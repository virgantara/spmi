<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\helpers\MyHelper;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\AsesmenDokumen;
use app\models\Auditor;
use app\models\Indikator;
use app\models\JenjangBobot;
use app\models\PersetujuanCek;
use app\models\UnitKerja;
use app\models\User;
use Mpdf\Shaper\Indic;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\httpclient\Client;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionDeleteIndikatorByAmiId($amiId)
    {
        $indikators = Indikator::findAll(['ami_id' => $amiId]);
        $jumlah = 0;

        $jumlahJenjang = 0;
        foreach ($indikators as $indikator) {
            $jenjang = JenjangBobot::findAll(['indikator_id' => $indikator->id]);
            foreach ($jenjang as $key => $value) {
                $jumlahJenjang++;
                $value->delete();
            }
            $indikator->delete();
            $jumlah++;
        }

        echo $jumlah . ' indikator berhasil dihapus' . "\n" . $jumlahJenjang . ' jenjang berhasil dihapus';

        return ExitCode::OK;
    }

    public function actionSyncPersetujuanSuratTugasByAmiId($amiId)
    {
        $amiUnit = AmiUnit::findAll(['ami_id' => $amiId]);

        $berhasil = 0;
        $gagal = 0;
        $skip = 0;
        foreach ($amiUnit as $unit) {
            $persetujuan = PersetujuanCek::inputDokumenTervalidasi($unit->id);
            if ($persetujuan == 'berhasil') $berhasil++;
            elseif ($persetujuan == 'skip') $skip++;
            else $gagal++;
        }

        echo "\n" . $berhasil . ' dokumen berhasil divalidasi' . "\n" . $gagal . ' dokumen gagal divalidasi' . "\n" . $skip . ' dokumen sudah ada';
        return ExitCode::OK;
    }

    public function actionUbahDataStandar()
    {
        $indikators17 = Indikator::findAll(['kriteria_id' => 19, 'ami_id' => 4]);

        $indikator = 0;
        $indikatordiubah = 0;
        $indikatordiubahgagal = 0;

        $count = 0;
        foreach ($indikators17 as $key => $i) {
            $count++;
            $i->kriteria_id = 6;
            if ($i->save()) {
                $indikatordiubah++;
            } else {
                echo MyHelper::logError($i);
                $indikatordiubahgagal++;
            }
        }
        echo "\n" . $indikator . ' indikator' . "\n" . $indikatordiubah . ' indikator berhasil diubah' . "\n" . $indikatordiubahgagal . ' indikator gagal diubah';
    }

    public function actionMigrasiDokumen()
    {
        $asesmen = Asesmen::find()->where(['IS NOT', 'dokumen_id', null])->all();

        $berhasil = 0;
        $gagal = 0;

        foreach ($asesmen as $a) {
            $dokumen = new AsesmenDokumen();
            $dokumen->asesmen_id = $a->id;
            $dokumen->dokumen_id = $a->dokumen_id;

            if ($dokumen->save()) {
                $berhasil++;
            } else {
                $gagal++;
            }
        }

        echo "Berhasil migrate dokumen: $berhasil asesmen\n";
        echo "Gagal migrate dokumen: $gagal asesmen\n";
    }


    public function actionSetKodeUnit()
    {
        $unitKerjas = UnitKerja::find()->all();

        $berhasil = 0;
        $gagal = 0;
        $skip = 0;

        $transaction = Yii::$app->db->beginTransaction();

        try {
            foreach ($unitKerjas as $unit) {
                if (empty($unit->kode_unit)) {
                    $unit->kode_unit = (string)$unit->id;

                    if ($unit->save()) {
                        $berhasil++;
                    } else {
                        $error = MyHelper::logError($unit);
                        Console::output("\n$error error");
                        $gagal++;
                    }
                } else {
                    $skip++;
                }
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        Console::output("\n$berhasil data berhasil diinput");
        Console::output("$gagal data gagal diinput");
        Console::output("$skip data sudah ada");

        return ExitCode::OK;
    }


    public function actionSetUserSso()
    {
        $auditors = Auditor::find()->all();
        $gagal = 0;
        $gagalEmail = 0;

        foreach ($auditors as $key => $auditor) {
            $api_baseurl = 'http://api.unida.gontor.ac.id:1926';
            $client = new Client(['baseUrl' => $api_baseurl]);
            $client_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IjE5MjYyMDE0IiwiY2xpZW50U2VjcmV0IjoiYmlzbWlsbGFoIiwiaWF0IjoxNTc5MTY2NDc3fQ.T5xuFl750KDGyblYBGDPpZ-fl4UDcOq4Rc6TWxux1VE';
            $headers = ['x-access-token' => $client_token];

            $response = $client->get('/sso/u/get', ['email' => $auditor->email], $headers)->send();
            $data_dosen = [];
            if ($response->isOk) {
                $hasil = $response->data['values'];

                if (empty($hasil['uuid'])) {
                    $gagal++;
                } else {

                    $uuid = $hasil['uuid'];

                    $user = User::findByEmail($auditor->email);
                    if (empty($user->email)) {
                        echo "GAGAL EMAIL: $auditor->email \n";
                        $gagalEmail++;
                    }

                    $user->uuid = $uuid;

                    if (!$user->save()) {
                        echo '<pre>';print_r(MyHelper::logError($user));exit;
                    }

                    echo "UUID: $uuid \n";
                }
            } else {

                echo "API GAGAL\n";
                exit;
            }
        }

        echo "Jumlah GAGAL: $gagal \n";
        echo "Jumlah GAGAL EMAIL: $gagalEmail \n";
        exit;
    }
}
