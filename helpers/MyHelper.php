<?php

namespace app\helpers;

use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Indikator;
use app\models\JenjangBobot;
use app\models\JenjangMap;
use app\models\PembagianKriteria;
use app\models\Periode;
use app\models\Persetujuan;
use app\models\PersetujuanCek;
use app\models\UnitKerja;
use Symfony\Component\Console\Helper\Dumper;
use Symfony\Component\Yaml\Dumper as YamlDumper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;


if (!defined('STDIN')) {
    define('STDIN', fopen('php://stdin', 'r'));
}

class MyHelper
{
    public static function listTingkat()
    {
        $list = [
            '1' => 'Nasional',
            '2' => 'ASEAN',
            '3' => 'Internasional'

        ];

        return $list;
    }

    public static function listAkreditasi()
    {
        $query = \app\models\StatusAkreditasi::find()->all();
        $list = \yii\helpers\ArrayHelper::map($query, 'kode', 'nama_id');
        // $list = [
        //     'U' => 'Unggul',
        //     'BS' => 'Baik Sekali',
        //     'BK' => 'Baik',
        //     'A' => 'A',
        //     'B' => 'B',
        //     'C' => 'C',
        //     'Accredited' => 'Accredited',
        //     'NotAccredited' => 'Not Accredited'

        // ];

        return $list;
    }

    // public static function setFlash($flag, $msg, $url, $arr)
    // {
    //     Yii::$app->session->setFlash('danger', "Silakan pilih mahasiswa");
    //     return $this->redirect(['master-input-capaian', ['periode_id' => $_GET['periode_id']]]);
    // }
    public static function convertToLetters($n)
    {
        $result = '';

        while ($n > 0) {
            $modulo = ($n - 1) % 26;
            $result = chr(65 + $modulo) . $result;
            $n = floor(($n - $modulo) / 26); // Menggunakan floor untuk memastikan nilai bilangan bulat
        }

        return $result;
    }


    public static function listDokumenAmi()
    {
        $list = [
            [
                'id' => '01', // ac = true done
                'nama' => 'Surat Tugas',
                'kode' => 'surat-tugas-ami',
                'link-cetak' => 'surat/tugas-amiunit',
                'visible' => false,
            ],
            [
                'id' => '02', // ac = false -
                'nama' => 'Rencana AMI',
                'kode' => 'rencana-ami',
                'link-cetak' => 'surat/rencana',
                'visible' => true,
            ],
            [
                'id' => '03', // -
                'nama' => 'Tilik',
                'kode' => 'tilik-ami',
                'link-cetak' => 'tilik/cetak',
                'visible' => false,
            ],
            [
                'id' => '04', // ac = false -
                'nama' => 'Temuan AMI',
                'kode' => 'temuan-ami',
                'link-cetak' => 'temuan/cetak-temuan',
                'visible' => true,
            ],
            [
                'id' => '05', // ac = false -
                'nama' => 'Log Temuan AMI',
                'kode' => 'log-temuan-ami',
                'link-cetak' => 'temuan/cetak-log-temuan',
                'visible' => true,
            ],
            [
                'id' => '06', // ac = false -
                'nama' => 'Berita Acara AMI',
                'kode' => 'berita-acara-ami',
                'link-cetak' => 'surat/cetak-berita',
                'visible' => true,
            ],
            [
                'id' => '07', // ac = false -
                'nama' => 'Laporan Audit Mutu Internal',
                'kode' => 'laporan-ami',
                'link-cetak' => 'asesmen/cetak-laporan-ami-per-auditee',
                'visible' => false,
            ],

        ];

        return $list;
    }

    public static function convertWaktu($waktu)
    {
        $timestamp = strtotime($waktu);

        $format = date("[H:i] d F Y", $timestamp);

        return $format;
    }

    public static function formatTextLowerDash($text)
    {
        $formatted = str_replace(' ', '-', $text);
        $formatted = strtolower($formatted);
        return $formatted;
    }

    public static function getJenisUnit()
    {
        $list = [
            'satker' => 'Satuan Kerja',
            'prodi' => 'Program Studi',
            'fakultas' => 'Fakultas'
        ];

        return $list;
    }

    public static function getJenisUnitCode($string)
    {
        $list = array(
            'fakultas' => 1,
            'prodi' => 2,
            'satker' => 3,
        );

        return $list[$string];
    }

    public static function getBidangRektor($id = null)
    {
        $list = [
            1 => 'Rektor',
            2 => 'Wakil Rektor 1',
            3 => 'Wakil Rektor 2',
            4 => 'Wakil Rektor 3',
            // 4 => 'Wakil Rektor 4',
        ];

        return $id == null ? $list : $list[$id];
    }

    public static function readMore($teks, $batas_kata)
    {
        $result = '';

        if (strlen($teks) >= $batas_kata) {
            $result = substr($teks, 0, $batas_kata) . "...";
        } else {
            $result = $teks;
        }

        return $result;
    }

    public static function getTerbilang($nilai)
    {
        $declare    = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

        if ($nilai < 12)                return " " . $declare[$nilai];
        elseif ($nilai < 20)            return MyHelper::getTerbilang($nilai - 10) . " belas";
        elseif ($nilai < 100)           return MyHelper::getTerbilang($nilai / 10) . " puluh" . MyHelper::getTerbilang($nilai % 10);
        elseif ($nilai < 200)           return " seratus" . MyHelper::getTerbilang($nilai - 100);
        elseif ($nilai < 1000)          return MyHelper::getTerbilang($nilai / 100) . " ratus" . MyHelper::getTerbilang($nilai % 100);
        elseif ($nilai < 2000)          return " seribu" . MyHelper::getTerbilang($nilai - 1000);
        elseif ($nilai < 1000000)       return MyHelper::getTerbilang($nilai / 1000) . " ribu" . MyHelper::getTerbilang($nilai % 1000);
        elseif ($nilai < 1000000000)    return MyHelper::getTerbilang($nilai / 1000000) . " juta" . MyHelper::getTerbilang($nilai % 1000000);
        elseif ($nilai < 1000000000000) return MyHelper::getTerbilang($nilai / 1000000000) . " milyar" . MyHelper::getTerbilang($nilai % 1000000000);
    }

    public static function getKriteriaAmiUnit($id)
    {
        $amiUnit = AmiUnit::findOne($id);

        // Filter Kriteria
        $jenisUnit = MyHelper::getJenisUnitCode($amiUnit->unit->jenis);
        $kriteria = PembagianKriteria::find()->where([
            'jenis_unit' => $jenisUnit
        ])->all();

        $results = [
            'kriteria'      => $kriteria,
            'jenisUnit'     => $jenisUnit,
            'unitId'        => $amiUnit->unit_id,
        ];
        return $results;
    }

    public static function printBarcode($data)
    {

        $persetujuan = Persetujuan::find()->where([
            'ami_unit_id' => $data['ami_unit_id'],
            // 'nama_dokumen' => $data['nama_dokumen'],
        ]);

        if ($data['is_auditee']) {
            $persetujuan->andWhere([
                'is_auditee'    => true
            ]);
            $persetujuan = $persetujuan->one();

            if (!empty($persetujuan)) return $persetujuan->amiUnit->unit->penanggung_jawab . '#kepala' . $persetujuan->amiUnit->unit->nama . '#accepted_at' . $persetujuan->datetime;
            else return false;
        } else {
            $persetujuan->andWhere([
                'auditor_id'    => $data['auditor_id'],
            ]);
            $persetujuan = $persetujuan->one();
            if (!empty($persetujuan)) return $persetujuan->auditor->nama . '#nidn' . $persetujuan->auditor->nidn . '#accepted_at' . $persetujuan->datetime;
            else return false;
        }
    }

    public static function printQrIsi($kodeKegiatan, $kodeDokumen, $kodeUnitKerja, $tahun, $keterangan = '')
    {
        $persetujuanCek = PersetujuanCek::findOne([
            'nama_kegiatan' => $kodeKegiatan,
            'nama_dokumen' => $kodeDokumen,
            'unit_kerja_id' => $kodeUnitKerja,
            'tahun' => $tahun
        ]);

        $kodeDokumen = $persetujuanCek->kode_dokumen ?? '';

        // return $kodeDokumen . $keterangan; // hanya kode dokumen

        return Yii::$app->urlManager->createAbsoluteUrl([
            'persetujuan-cek/cek-keaslian',
            'kode_dokumen' => $kodeDokumen,
            'keterangan' => urldecode($keterangan)
        ]);
    }

    public static function getSumBobotIndikator($condition, $jenjang_id)
    {
        //Akumulasi bobot total instrumen fakultas
        $query = Indikator::find();

        $pembagian = PembagianKriteria::find()->where(['jenis_unit' => $condition])->all();
        $pembagian = ArrayHelper::getColumn($pembagian, 'kriteria_id');
        $query->where(['kriteria_id' => $pembagian]);
        $query->andWhere(['jenis_indikator' => 0]);

        $query->joinWith(['jenjangBobots as j']);
        $query = $query->all();

        foreach ($query as $data) {
            $data = JenjangBobot::find()->select('bobot')->where(['indikator_id' => $data->id, 'jenjang_id' => $jenjang_id])->one();
            $bobot = [
                'bobot' => (isset($data->bobot) ? $data->bobot : 0),
            ];
            $bobots[] = $bobot['bobot'];
        }

        return array_sum($bobots);
    }

    public static function getSumSkorAsesmen($ami_unit_id, $column = null)
    {
        //Cek jumlah data skor
        $data    = Asesmen::find()->where([
            'ami_unit_id' => $ami_unit_id,
        ]);

        return ($column != null ? $data->sum($column) : $data->count());
    }

    public static function getSkorAsesmenAk($asesmen, $auditorke)
    {
        switch ($auditorke) {
            case 1:
                return $asesmen->skor_ak1 ?? '';
                break;
            case 2:
                return $asesmen->skor_ak2 ?? '';
                break;
            case 3:
                return $asesmen->skor_ak3 ?? '';
                break;

            default:
                break;
        }
    }

    public static function getSkorAsesmenAlAuto($asesmen, $auditorke)
    {
        switch ($auditorke) {
            case 1:
                if (empty($asesmen->skor_al1)) {
                    return $asesmen->skor_ak1;
                }
                return $asesmen->skor_al1;
                break;
            case 2:
                if (empty($asesmen->skor_al2)) {
                    return $asesmen->skor_ak2;
                }
                return $asesmen->skor_ak2;
                break;
            case 3:
                if (empty($asesmen->skor_al3)) {
                    return $asesmen->skor_ak3;
                }
                return $asesmen->skor_ak3;
                break;

            default:
                break;
        }
    }

    public static function getPaketPenilaian($ami_unit_id, $kriterias, $jenjang_id)
    {

        //Paket Total
        $asesmen = Asesmen::find()->where([
            'ami_unit_id' => $ami_unit_id,
        ]);

        $bobot = JenjangBobot::find()->where([
            'jenjang_id' => $jenjang_id,
        ])
            ->joinWith(['indikator as i'])
            ->andWhere([
                'i.kriteria_id'       => $kriterias,
            ]);

        $nAwal = MyHelper::getSumSkorAsesmen($ami_unit_id) * 3;
        $total_ed = MyHelper::getSumSkorAsesmen($ami_unit_id, 'skor_ed');
        $total_al1 = MyHelper::getSumSkorAsesmen($ami_unit_id, 'skor_al1');
        $total_al2 = MyHelper::getSumSkorAsesmen($ami_unit_id, 'skor_al2');
        $total_al3 = MyHelper::getSumSkorAsesmen($ami_unit_id, 'skor_al3');

        foreach ($asesmen->all() as $asesmen) {
            if ($asesmen->skor_al1 == null)  $nAwal--;
            if ($asesmen->skor_al2 == null)  $nAwal--;
            if ($asesmen->skor_al3 == null)  $nAwal--;
        }

        // $avrgAll    = null;
        $totalAl    = $total_al1 + $total_al2 + $total_al3;
        if ($totalAl != 0) $avrgAll = $totalAl / $nAwal;
        else $avrgAll = 0;

        $totalBobot = $bobot->sum('jenjang_bobot.bobot');
        $totalAll   = $avrgAll * $totalBobot;

        $data = [
            'totalEd'       => $total_ed,
            'totalAl1'      => $total_al1,
            'totalAl2'      => $total_al2,
            'totalAl3'      => $total_al3,
            'avrgAll'       => number_format((float)$avrgAll, 2, '.', ''),
            'totalAll'      => number_format((float)$totalAll, 2, '.', ''),
            'totalBobot'    => $totalBobot,
        ];

        return $data;
    }

    public static function getAsesmen($ami_unit_id, $indikator_id)
    {
        $asesmen = Asesmen::find()->where([
            'ami_unit_id'   => $ami_unit_id,
            'indikator_id'  => $indikator_id,
        ])->one();

        return $asesmen;
    }

    public static function setStatusDocMessage($asesmen, $text_false, $text_true)
    {
        return (isset($asesmen->link_bukti) || isset($asesmen->dokumen_id) || isset($asesmen->pengelompokan_id)  ? $text_true : $text_false);
    }

    public static function convertSkor($nilai)
    {
        if (empty($nilai)) return '-';
        elseif ($nilai > 99) return MyHelper::getStatusDokumenSatker()[$nilai];
        elseif ($nilai > 9) return MyHelper::getStatusDokumenProdiFakultas()[$nilai];
        return $nilai;
    }

    public static function setStatusAsesmen($code)
    {
        switch ($code) {

            case 0:
                $result = '❌';
                break;
            case 1:
                $result = '⚠️';
                break;
            case 2:
                $result = '✅';
                break;

            default:
                $result = 'ER';
                break;
        }
        return $result;
    }

    public static function getCountEvaluasi($kriteria, $amiUnit)
    {
        $instrumenEvaluasi = 0;
        $jenjang = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();
        foreach ($kriteria['kriteria'] as $v) {
            $instrumen = Indikator::find()->where([
                'kriteria_id' => $v->kriteria_id,
                'jenis_indikator' => '0',
            ])->joinWith(['jenjangBobots as jb', 'ami as a']);
            $instrumen->andWhere(['a.id' => $amiUnit->ami->id]);

            if ($amiUnit->unit->jenis == 'satker') {
                $instrumen = $instrumen->count();
            } else {
                $instrumen = $instrumen->andWhere(['jb.jenjang_id' => $jenjang->jenjang_id])->count();
            }

            $instrumenEvaluasi = $instrumenEvaluasi + $instrumen;

            if ($kriteria['jenisUnit'] == 3) {
                $instrumenKhusus = Indikator::find()->joinWith(['ami as a'])->where([
                    'kriteria_id' => $v->kriteria_id,
                    'jenis_indikator' => '2',
                    'unit_id' => $kriteria['unitId'],
                    'a.id' => $amiUnit->ami->id
                ])->count();
                $instrumenEvaluasi = $instrumenEvaluasi + $instrumenKhusus;
            }
            // echo '<pre>';print_r($v->kriteria->nama . ' -> ' . $instrumen . ' -> ' . $instrumenKhusus . ' - ' . $instrumenEvaluasi);
        }
        // echo '<pre>';print_r($instrumenEvaluasi);exit;


        return $instrumenEvaluasi;
    }

    public static function getAsesmenSkor($id, $column)
    {
        $totalData = 0;
        $data = Asesmen::find()->where([
            'ami_unit_id' => $id
        ]);
        $data->andWhere(['not', [$column => null]]);
        $data      = $data->count();
        $totalData = $totalData + $data;

        return $totalData;
    }

    public static function getAuditor($id, $jabatan)
    {
        $auditor = AmiAuditor::find()->where([
            'ami_id'    => $id,
            'status_id' => $jabatan,
        ])->one();
        $listJabatan = ['', 'Ketua Auditor', 'Anggota Auditor', 'Sekertaris Auditor'];


        if ($auditor != null) {
            # code...
            $result['id']               = $auditor->auditor->id;
            $result['nomor_registrasi'] = $auditor->auditor->nomor_registrasi;
            $result['nama']             = $auditor->auditor->nama;
            $result['email']            = $auditor->auditor->email;
            $result['prodi']            = $auditor->auditor->prodi;
            $result['niy']              = $auditor->auditor->niy;
            $result['nidn']             = $auditor->auditor->nidn;
            $result['pangkat']          = $auditor->auditor->pangkat;
            $result['jabfung']          = $auditor->auditor->jabfung;
            $result['status_aktif']     = $auditor->auditor->status_aktif;
            $result['jabatan']          = $listJabatan[$jabatan];
        }

        return (isset($auditor) ? $result : "-");
    }

    public static function getAuditee($id)
    {
        $auditee = UnitKerja::findOne($id);

        return (isset($auditee) ? $auditee->penanggung_jawab : "-");
    }

    public static function convertPersen($nilai, $total)
    {
        if ($total != 0) {
            $persen = $nilai / $total * 100;
            return number_format((float)$persen, 2, '.', '');
        } else {
            return "Error: Division by zero";
        }
    }

    public static function listJenjangStudi()
    {
        $list_kriteria = [
            'Profesi' => [
                'jenjang' => 'Profesi',
                'singkatan' => 'Profesi',
            ],
            'D' => [
                'jenjang' => 'Diploma 4',
                'singkatan' => 'D4',
            ],
            'C' => [
                'jenjang' => 'Sarjana',
                'singkatan' => 'S1',
            ],
            'B' => [
                'jenjang' => 'Magister',
                'singkatan' => 'S2',
            ],
            'A' => [
                'jenjang' => 'Doktoral',
                'singkatan' => 'S3',
            ],
        ];

        return $list_kriteria;
    }

    public static function getStatusAktif()
    {
        $roles = [
            '1' => 'Aktif',
            '0' => 'Non-Aktif'
        ];
        return $roles;
    }

    public static function getStatusKhusus()
    {
        $roles = [
            '1' => 'Khusus',
            '0' => 'Umum'
        ];
        return $roles;
    }

    public static function setStatusAktif($status)
    {
        switch ($status) {
            case 1:
                $status = 'Aktif';
                break;
            case 0:
                $status = 'Non-Aktif';
                break;
            default:
                break;
        }

        return $status;
    }

    public static function getLabel($skor)
    {
        switch ($skor) {
            case 100:
                $skor = 'blue';
                break;
            case 101:
                $skor = 'orange';
                break;
            case 102:
                $skor = 'red';
                break;
            case 0:
                $skor = 'red';
                break;

            case 12:
                $skor = 'blue';
                break;
            case 13:
                $skor = 'orange';
                break;
            case 11:
                $skor = 'blue';
                break;
            case 10:
                $skor = 'red';
                break;

            default:
                break;
        }
        return $skor;
    }

    public static function getSkorPdf($skor)
    {
        switch ($skor) {
            case 100:
                $skor = '<td style="text-align: center;background-color: #74992e;">✓</td><td></td><td></td>';
                break;
            case 101:
                $skor = '<td></td><td style="text-align: center;background-color: orange;">✓</td><td></td>';
                break;
            case 102:
                $skor = '<td></td><td></td><td style="text-align: center;background-color: red;">✓</td>';
                break;
            case 0:
                $skor = '<td></td><td></td><td></td>';
                break;

            default:
                break;
        }

        return $skor;
    }

    public static function getJenisPenilaianCode($string)
    {
        $list = array(
            'led' => 'Laporan Evaluasi Diri',
            'ak' => 'Asesmen Kecukupan',
            'al' => 'Asesmen Lapangan',
        );

        return $list[$string];
    }

    public static function getTingkatRtm()
    {
        $list = [
            [
                'prodi' => 'Program Studi',
                'fakultas' => 'Fakultas',
                'satker' => 'Satuan Kerja',
            ],
            [
                'fakultas' => 'Fakultas',
                'koordinator' => 'Wakil Rektor',
                'satker' => 'Non Wakil Rektor',
                'universitas' => 'Universitas',
            ],
            [
                'koordinator' => 'Wakil Rektor',
                // 'satker' => 'Non Wakil Rektor',
                'universitas' => 'Universitas',
            ]
        ];

        return $list;
    }


    public static function getLabelAuditor($urutan)
    {
        switch ($urutan) {
            case 1:
                $urutan = 'a. Ketua';
                break;
            case 2:
                $urutan = 'b. Anggota 1';
                break;
            case 3:
                $urutan = 'c. Anggota 2';
                break;

            default:
                break;
        }

        return $urutan;
    }


    public static function getKategoriKts()
    {
        $roles = [
            '0' => 'Observasi',
            '1' => 'KTS/Minor',
            '2' => 'KTS/Major',
            '3' => 'Memenuhi',
            '4' => 'Melampaui',
        ];

        return $roles;
    }

    public static function getStatusEfektif()
    {
        $roles = [
            '0' => 'Tidak Efektif',
            '1' => 'Efektif',
        ];

        return $roles;
    }

    public static function getStatusRtl()
    {
        $roles = [
            'close' => 'Close',
            'open' => 'Open',
            'toleran' => 'Toleran',
        ];

        return $roles;
    }

    public static function getJenisKriteria()
    {
        $roles = [
            '0' => 'Fakultas-prodi',
            '1' => 'Satuan Kerja',
            // '2' => 'KTS/Major',
        ];

        return $roles;
    }

    public static function getKhusus()
    {
        $roles = [
            '0' => 'Umum',
            '1' => 'Khusus',
            // '2' => 'KTS/Major',
        ];

        return $roles;
    }

    public static function setJenisKriteria($status)
    {
        switch ($status) {

            case 0:
                $status = 'Fakultas - Prodi';
                break;
            case 1:
                $status = 'Satuan Kerja';
                break;
            default:
                break;
        }

        return $status;
    }

    public static function listJenisUnit()
    {

        $unit = [
            'fakultas' => 1,
            'prodi' => 2,
            'satker' => 3,
        ];

        return $unit;
    }

    public static function getStatusDokumenSatker()
    {
        $roles = [
            100 => 'Sesuai',
            101 => 'Tidak Sesuai',
            102 => 'Tidak Ada',
        ];

        return $roles;
    }

    public static function getStatusDokumenProdiFakultas()
    {
        $roles = [
            10 => 'Belum Memenuhi',
            11 => 'Memenuhi',
            12 => 'Melampaui',
            13 => 'Menyimpang',
        ];

        return $roles;
    }

    public static function getYears()
    {
        return array_combine(range(date("Y", strtotime("+1 year")), 2018), range(date("Y", strtotime("+1 year")), 2018));
    }

    public static function getJabatan()
    {
        $jabatan = [
            '0' => 'Ka Prodi',
            '1' => 'Sek Prodi',
            '2' => 'Sek Fakultas',
            '3' => 'Ka TU',
        ];

        return $jabatan;
    }

    public static function getJenisIndikator()
    {
        $roles = [
            '0' => 'Umum',
            '1' => 'Khusus',
        ];


        return $roles;
    }

    public static function listStatusBAP()
    {
        return [
            '0' => [
                'label' => 'Menunggu persetujuan Kaprodi',
                'color' => 'warning'
            ],
            '1' => [
                'label' => 'Sudah disetujui',
                'color' => 'success'
            ],
        ];
    }

    public static function getJenisHasilPustaka()
    {
        return ['0' => 'Bukan Hasil Riset/Abdimas Sendiri', '1' => 'Hasil Penelitian', '2' => 'Hasil Pengabdian kepada Masyarakat'];
    }

    public static function cleanSpecialChars($string)
    {
        // $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    }

    public static function getStatusHapus()
    {
        return [
            '1' => 'Hapus',
            '0' => 'Aktif'
        ];
    }

    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function split_name($name)
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name));
        return array($first_name, $last_name);
    }
    public static function getJenisMBKM()
    {
        return [
            '1' => 'Dalam PT',
            '2' => 'PT Lain',
            '3' => 'Non-PT',
            '0' => 'Non-MBKM'
        ];
    }
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (!$length) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }

    public static function logDebug($error)
    {
        echo '<pre>';
        print_r($error);
        echo '</pre>';
        die();
    }

    public static function getJenisFileClassroom()
    {
        return [
            'driveFile' => 'Google Drive',
            // 'youtubeVideo' => 'YouTube',
            'link' => 'Link',
            // 'form' => 'Form'
        ];
    }

    public static function getSisterToken()
    {
        $tokenPath = Yii::getAlias('@webroot') . '/credentials/sister_token.json';
        $sisterToken = '';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $sisterToken = $accessToken['id_token'];
            $created_at = $accessToken['created_at'];
            $date     = new \DateTime(date('Y-m-d H:i:s', strtotime($created_at)));
            $current  = new \DateTime(date('Y-m-d H:i:s'));
            $interval = $date->diff($current);
            // $inv = $interval->format('%I');
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            if ($minutes > 5) {
                if (!MyHelper::wsSisterLogin()) {
                    throw new \Exception("Error Creating SISTER Token", 1);
                } else {
                    $accessToken = json_decode(file_get_contents($tokenPath), true);
                    $sisterToken = $accessToken['id_token'];
                }
            } else {

                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $sisterToken = $accessToken['id_token'];
            }
        } else {
            if (!MyHelper::wsSisterLogin()) {
                throw new \Exception("Error Creating SISTER Token", 1);
            } else {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $sisterToken = $accessToken['id_token'];
            }
        }

        return $sisterToken;
    }

    public static function wsSisterLogin()
    {
        try {
            $sister_baseurl = Yii::$app->params['sister']['baseurl'];
            $sister_id_pengguna = Yii::$app->params['sister']['id_pengguna'];
            $sister_username = Yii::$app->params['sister']['username'];
            $sister_password = Yii::$app->params['sister']['password'];
            $headers = ['content-type' => 'application/json'];
            $client = new \GuzzleHttp\Client([
                'timeout'  => 10.0,
                'headers' => $headers,
                // 'base_uri' => 'http://sister.unida.gontor.ac.id/api.php/0.1'
            ]);
            // $full_url = $sister_baseurl.'/Login';
            $id_token = '';
            $full_url = $sister_baseurl . '/authorize';

            $response = $client->post($full_url, [
                'body' => json_encode([
                    'username' => $sister_username,
                    'password' => $sister_password,
                    'id_pengguna' => $sister_id_pengguna
                ]),
                'headers' => ['Content-type' => 'application/json']

            ]);

            $response = json_decode($response->getBody());

            $data = [
                'id_token' => $response->token,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $tokenPath = Yii::getAlias('@webroot') . '/credentials/sister_token.json';


            file_put_contents($tokenPath, json_encode($data));
            return true;
        } catch (\Exception $e) {
            print_r($e->getMessage());
            exit;
            return false;
        }
    }

    public static function getStatusAktifDosen()
    {
        $roles = [
            'aktif' => 'AKTIF',
            'cuti' => 'CUTI',
            'tugasbelajar' => 'Tugas Belajar',
            'resign' => 'Resign',
            'nonaktif' => 'Non-Aktif'
        ];


        return $roles;
    }

    public static function getListHariEn()
    {
        $list_hari = [
            'SABTU' => 'Saturday',
            'AHAD' => 'Sunday',
            'SENIN' => 'Monday',
            'SELASA' => 'Tuesday',
            'RABU' => 'Wednesday',
            'KAMIS' => 'Thursday'
        ];

        return $list_hari;
    }

    public static function getBagian()
    {
        $list_bagian = [
            '1' => 'Auditee',
            '2' => 'Auditor',
            '3' => 'Admin',
        ];

        return $list_bagian;
    }

    public static function getListHari()
    {
        $list_hari = [
            'SABTU' => 'SABTU',
            'AHAD' => 'AHAD',
            'SENIN' => 'SENIN',
            'SELASA' => 'SELASA',
            'RABU' => 'RABU',
            'KAMIS' => 'KAMIS'
        ];

        return $list_hari;
    }

    public static function getMatkulKurikulum($id_kurikulum, $kode_mata_kuliah)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetMatkulKurikulum',
            'token'   => $token,
            'filter' => "id_kurikulum='" . $id_kurikulum . "' AND kode_mata_kuliah = '" . $kode_mata_kuliah . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            if (count($resp->data) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $resp->data[0],
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null,
                ];
            }
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null,
            ];
        }

        return $result;
    }

    public static function insertMatkulKurikulum($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertMatkulKurikulum',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_matkul' => $resp->data->id_matkul,
                'id_kurikulum' => $resp->data->id_kurikulum
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_matkul' => '',
                'id_kurikulum' => ''
            ];
        }

        return $result;
    }

    public static function insertKurikulum($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertKurikulum',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kurikulum' => $resp->data->id_kurikulum,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Kurikulum: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kurikulum' => '',
            ];
        }

        return $result;
    }


    public static function getDetailKurikulum($id_prodi, $nama_kurikulum)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDetailKurikulum',
            'token'   => $token,
            'filter' => "id_prodi='" . $id_prodi . "' AND nama_kurikulum = '" . $nama_kurikulum . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            if (count($resp->data) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $resp->data[0],
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null,
                ];
            }
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null,
            ];
        }

        return $result;
    }

    public static function getKelasKuliah($id_prodi, $id_semester, $id_dosen, $id_matkul, $kelas)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDetailKelasKuliah',
            'token'   => $token,
            'filter' => "id_prodi='" . $id_prodi . "' AND id_semester='" . $id_semester . "' AND id_matkul = '" . $id_matkul . "' AND nama_kelas_kuliah = '" . $kelas . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function insertKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kelas_kuliah' => $resp->data->id_kelas_kuliah,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function getDosenPengajarKelasKuliah($id_kelas_kuliah, $id_registrasi_dosen)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDosenPengajarKelasKuliah',
            'token'   => $token,
            'filter' => "id_kelas_kuliah='" . $id_kelas_kuliah . "' AND id_registrasi_dosen='" . $id_registrasi_dosen . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Dosen Pengajar Belum Ditemukan',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function insertDosenPengajarKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertDosenPengajarKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_aktivitas_mengajar' => $resp->data->id_aktivitas_mengajar,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Dosen Pengajar Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_aktivitas_mengajar' => '',
            ];
        }

        return $result;
    }

    public static function getPesertaKelasKuliah($id_kelas_kuliah, $id_registrasi_mahasiswa)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetPesertaKelasKuliah',
            'token'   => $token,
            'filter' => "id_kelas_kuliah='" . $id_kelas_kuliah . "' AND id_registrasi_mahasiswa='" . $id_registrasi_mahasiswa . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Mahasiswa Belum Ditemukan',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function deletePesertaKelasKuliah($keys, $params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'DeletePesertaKelasKuliah',
            'token'   => $token,
            'key' => $keys,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kelas_kuliah' => '',
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Delete Peserta Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function insertPesertaKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertPesertaKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_registrasi_mahasiswa' => $resp->data->id_registrasi_mahasiswa,
                'id_kelas_kuliah' => $resp->data->id_kelas_kuliah,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Peserta Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_registrasi_mahasiswa' => '',
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function getFeederToken()
    {
        $feederToken = '';
        $tokenPath = Yii::getAlias('@webroot') . '/credentials/feeder_token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $feederToken = $accessToken['id_token'];
        } else {
            if (!MyHelper::wsFeederLogin()) {
                throw new \Exception("Error Creating FEEDER Token", 1);
            } else {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $feederToken = $accessToken['id_token'];
            }
        }

        return $feederToken;
    }

    public static function wsFeederLogin()
    {
        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $token = '';
        $errors = '';
        try {
            $headers = ['Content-Type' => 'application/json'];

            $params = [
                'act' => 'GetToken',
                'username'   => $feeder_username,
                'password'   => $feeder_password,
            ];


            $response = $client->request('POST', '/ws/live2.php', [
                'headers' => $headers,
                'body' => json_encode($params)
            ]);

            $results = $response->getBody()->getContents();
            $results = json_decode($results);
            if ($results->error_code == 0) {
                $data = [
                    'id_token' => $results->data->token,
                    'created_at' => date('Y-m-d H:i:s')
                ];

                $tokenPath = Yii::getAlias('@webroot') . '/credentials/feeder_token.json';


                file_put_contents($tokenPath, json_encode($data));
            } else {
                $errors .= $results->error_desc;
                throw new \Exception;
            }

            return true;
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            print_r($errors);
            exit;
            return false;
        }
    }


    public static function getStatusBeasiswa()
    {
        return ['0' => 'Belum Diproses', '1' => 'Diterima', '2' => 'Ditolak'];
    }

    public static function getJenisBeasiswa()
    {
        return ['1' => 'Pemerintah', '2' => 'Universitas', '3' => 'Pondok'];
    }

    public static function getListJenjang()
    {
        return ['A' => 'Ph.D - S3', 'B' => 'Magister - S2', 'C' => 'Bachelor - S1', 'D' => 'Diploma 4 - D4'];
    }

    public static function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }

    public static function getStatusSkripsi()
    {
        return [
            '1' => 'Belum Sidang Akhir',
            '2' => 'Sudah Sidang Akhir Belum Revisi',
            '3' => 'Sudah Sidang Akhir Sudah Revisi',
            '4' => 'Ganti Topik'
        ];
    }

    public static function getRubrikCatatanHarian()
    {
        return ['1' => 'Tidak ada catatan', '2' => 'Sudah ada catatan, namun hanya laporan', '3' => 'Sudah ada catatan, namun hanya laporan dan evaluasi', '4' => 'Sudah ada catatan dan lengkap, namun kemajuan tidak/kurang signifikan', '5' => 'Sudah ada catatan, lengkap, dan ada kemajuan signifikan'];
    }

    public static function getStatusProposal()
    {
        return [
            '1' => 'Belum Sidang',
            '2' => 'Sudah Sidang Belum Revisi',
            '3' => 'Sudah Sidang Sudah Revisi',
            '4' => 'Ganti Topik'
        ];
    }

    public static function getLamaHari()
    {
        return [
            '1' => '1 hari',
            '3' => '3 hari',
            '7' => '1 minggu',
            '14' => '2 minggu',
            '21' => '3 minggu',
            '30' => '1 bulan'
        ];
    }

    public static function appendZeros($str, $charlength = 6)
    {

        return str_pad($str, $charlength, '0', STR_PAD_LEFT);
    }

    public static function konversiEkdAngkaHuruf($skor)
    {
        $huruf = 'F';
        $ket  = '-';
        $color = 'default';
        if ($skor >= 126) {
            $huruf = 'A';
            $color = 'success';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi reward sertifikat dan insentif tertentu penambah semangat kerja.';
        } else if ($skor >= 101) {

            $huruf = 'B';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi reward sertifikat.';
        } else if ($skor >= 76) {
            $huruf = 'C';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor bahwa yang bersangkutan telah mencukupi kinerjanya.';
        } else if ($skor >= 51) {
            $huruf = 'D';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi peringatan.';
        } else if ($skor >= 30) {
            $huruf = 'E';
            $color = 'danger';
            $ket  = 'Dilaporkan kepada Rektor untuk diberikan sanksi tertentu yang mendukung peningkatan kinerjanya.';
        }

        return ['huruf' => $huruf, 'ket' => $ket, 'color' => $color];
    }

    public static function numberToAlphabet($index)
    {
        $alphabet = range('A', 'Z');

        return $alphabet[$index]; // returns D
    }

    public static function getListAbsensiFull()
    {
        $list = [
            '1' => 'Hadir',
            '2' => 'Izin',
            '3' => 'Saki',
            '4' => 'Ghoib'

        ];

        return $list;
    }

    public static function getListAbsensi()
    {
        $list = [
            '1' => 'H',
            '2' => 'I',
            '3' => 'S',
            '4' => 'G'

        ];

        return $list;
    }

    public static function getStatusPersetujuan()
    {
        $list = [
            '1' => 'Disetujui',
            '0' => 'Ditolak'
        ];

        return $list;
    }

    public static function getListIzin()
    {
        $list = [
            '1' => 'Belum Disetujui',
            '2' => 'Disetujui',
            '3' => 'Ditolak'
        ];

        return $list;
    }

    public static function getTargetJawaban($id = null)
    {
        $list = [
            1 => '( >  ) Lebih besar',
            2 => '( >= ) Lebih besar / sama dengan',
            3 => '( <  ) Lebih kecil',
            4 => '( <= ) Lebih kecil / sama dengan',
            5 => '( == ) Sama dengan',
            // 6 => '( -  ) Tidak ada..',
        ];

        return $id == null ? $list : $list[$id];
    }

    public static function getColorLevel($val = null)
    {
        if (isset($val)) {
            $val = (int)$val;
            if ($val >= 1460) return '#006B3D'; // tahun pertama 
            if ($val >= 1095) return '#069C56'; // tahun kedua
            if ($val >= 730) return '#FF980E'; // tahun ketiga
            if ($val >= 365) return '#FF681E'; // tahun keempat
            if ($val >= 1) return '#D3212C'; // tahun kelima
            else return '#000000'; // kadaluarsa
        }
    }

    public static function getCekKadaluarsa($tanggal_kadaluarsa, $jumlah_hari = null)
    {
        $tanggal = new \DateTime($tanggal_kadaluarsa);

        $today = new \DateTime('today');

        if ($tanggal < $today) return "Sudah Kadaluarsa";

        if ($jumlah_hari == true) return $today->diff($tanggal)->days;

        $perbedaan = $today->diff($tanggal);

        $perbedaan_tahun = $perbedaan->y;
        $perbedaan_bulan = $perbedaan->m;
        $perbedaan_hari = $perbedaan->d;


        return $perbedaan_tahun . " tahun, " . $perbedaan_bulan . " bulan, " . $perbedaan_hari . " hari.";
    }

    public static function cekCapaian($params)
    {
        $result = [
            'nilai' => $params ? 1 : 0,
            'result' => $params ? '<button class="btn btn-sm btn-success">sudah tercapai</button>' : '<button class="btn btn-sm btn-danger">belum tercapai</button>'
        ];
        return $result;
    }

    public static function setJawaban($paramsTarget, $paramsReal, $option)
    {
        $result = [];
        switch ($option) {

            case 1:
                return MyHelper::cekCapaian($paramsTarget > $paramsReal);
                break;

            case 2:
                return MyHelper::cekCapaian($paramsTarget >= $paramsReal);
                break;

            case 3:
                return MyHelper::cekCapaian($paramsTarget < $paramsReal);
                break;

            case 4:
                return MyHelper::cekCapaian($paramsTarget <= $paramsReal);
                break;

            case 5:
                return MyHelper::cekCapaian($paramsTarget == $paramsReal);
                break;

            default:
                return "-";
                break;
        }
    }

    public static function adminOnly($paramsDep, $paramsDev)
    {
        return Yii::$app->user->identity->access_role == 'theCreator' ? $paramsDev : $paramsDep;
    }

    public static function adminOnlyUnit($paramsDep, $paramsDev)
    {
        return Yii::$app->user->identity->access_role == 'theCreator' ? UnitKerja::findOne($paramsDev) : UnitKerja::findOne($paramsDep);
    }

    public static function sendEmail($userMail)
    {
        $developMode = Yii::$app->params['develop-mode'] ?? false;
        if (Yii::$app->user->identity->access_role == 'theCreator' && $developMode ?? false)
            $userMail = 'nashehannafii@unida.gontor.ac.id';

        return $userMail;
    }

    public static function emailBpm()
    {
        $emailBpm = 'qa@unida.gontor.ac.id';
        if (Yii::$app->user->identity->access_role == 'theCreator' && Yii::$app->params['develop-mode'] ?? false)
            $emailBpm = 'nashehannafii@unida.gontor.ac.id';

        return $emailBpm;
    }

    public static function getKodePersetujuanDokumen($id, $kode = 'AMI', $kegiatan = 'surat-tugas-ami')
    {
        if ($kode == 'AMI') {
            $unit = AmiUnit::findOne($id);
        }

        $kodeKegiatan = array_column(MyHelper::listDokumenAmi(), 'id', 'kode')[$kegiatan];
        $kodeUnit = trim($unit->unit_id ?? $unit->id);
        $kodeUnit = str_pad($kodeUnit, 4, '0', STR_PAD_LEFT);
        return $kode . $kodeKegiatan . $kodeUnit . $unit->ami->tahun ?? $unit->tahun;
    }

    public static function getListSemester()
    {
        $list_semester = [
            1 => 'Semester 1',
            2 => 'Semester 2',
            3 => 'Semester 3',
            4 => 'Semester 4',
            5 => 'Semester 5',
            6 => 'Semester 6',
            7 => 'Semester 7',
            8 => 'Semester 8',
            9 => 'Semester 9 ke atas',
        ];

        return $list_semester;
    }

    public static function getSemester()
    {
        $list_semester = [
            0 => [1 => 1, 2 => 2],
            1 => [3 => 3, 4 => 4],
            2 => [5 => 5, 6 => 6],
            3 => [7 => 7, 8 => 8],
        ];

        return $list_semester;
    }

    public static function convertTanggalIndo($date)
    {
        if ($date == null) {
            return '-';
        }
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $date);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        if (!empty($pecahkan[2]) && !empty($pecahkan[1]) && !empty($pecahkan[0]))
            return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
        else
            return 'invalid date format';
    }

    public static function getStatusAktivitas()
    {
        $roles = [
            'A' => 'AKTIF',
            'C' => 'CUTI',
            'D' => 'DO',
            'K' => 'KELUAR',
            'L' => 'LULUS',
            'N' => 'NON-AKTIF',
            'G' => 'DOUBLE DEGREE',
            'M' => 'MUTASI'
        ];


        return $roles;
    }

    public static function getStatusAmi()
    {
        $roles = [
            '0' => 'PROSES',
            '1' => 'SELESAI'
        ];


        return $roles;
    }

    public static function getJenisAmi()
    {
        $roles = [
            '1' => 'Fakultas',
            '2' => 'Program Studi',
            '3' => 'Satuan Kerja',
        ];

        return $roles;
    }

    public static function getListAras()
    {
        $roles = [
            'universitas' => 'Universitas',
            'satker' => 'Satuan Kerja',
            'upps' => 'UPPS',
            'prodi' => 'Program Studi',
        ];

        return $roles;
    }

    public static function isBetween($sd, $ed)
    {
        return (($sd >= $ed) && ($sd <= $ed));
    }

    public static function dmYtoYmd($tgl)
    {
        $date = str_replace('/', '-', $tgl);
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function YmdtodmY($tgl)
    {
        return date('d-m-Y H:i:s', strtotime($tgl));
    }


    public static function hitungDurasi($date1, $date2)
    {
        $date1 = new \DateTime($date1);
        $date2 = new \DateTime($date2);
        $interval = $date1->diff($date2);

        $elapsed = '';
        if ($interval->d > 0)
            $elapsed = $interval->format('%a hari %h jam %i menit %s detik');
        else if ($interval->h > 0)
            $elapsed = $interval->format('%h jam %i menit %s detik');
        else
            $elapsed = $interval->format('%i menit %s detik');


        return $elapsed;
    }

    public static function hitungUmur($tgl_lahir)
    {
        $tanggal = new \DateTime($tgl_lahir);

        $today = new \DateTime('today');

        // tahun
        $y = $today->diff($tanggal)->y;

        // bulan
        $m = $today->diff($tanggal)->m;

        // hari
        $d = $today->diff($tanggal)->d;
        return $y;
    }

    public static function logError($model)
    {
        $errors = '';
        foreach ($model->getErrors() as $attribute) {
            foreach ($attribute as $error) {
                $errors .= $error . ' ';
            }
        }

        return $errors;
    }

    public static function statusRkat()
    {
        $list = [
            -1 => 'Belum Diproses',
            1  => 'Sedang Diproses',
            2  => 'Diperiksa ADM',
            3  => 'Diperiksa Kepala BAK',
            5  => 'Disetujui',
            4  => 'Diperiksa BPM',
        ];

        return $list;
    }


    public static function statusBiaya()
    {
        $list = [
            1  => 'Lebih',
            2  => 'Sesuai',
            3  => 'Kurang',
        ];

        return $list;
    }


    public static function statusKetepatanWaktu()
    {
        $list = [
            1  => 'Maju',
            2  => 'Tepat',
            3  => 'Mundur',
        ];

        return $list;
    }


    public static function formatRupiah($value, $decimal = 0)
    {
        return number_format($value, $decimal, ',', '.');
    }

    public static function getSelisihHariInap($old, $new)
    {
        $date1 = strtotime($old);
        $date2 = strtotime($new);
        $interval = $date2 - $date1;
        return round($interval / (60 * 60 * 24)) + 1;
    }

    public static function getRandomString($minlength = 12, $maxlength = 12, $useupper = true, $usespecial = false, $usenumbers = true)
    {
        $key = '';
        $charset = "abcdefghijklmnopqrstuvwxyz";

        if ($useupper) $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        if ($usenumbers) $charset .= "0123456789";

        if ($usespecial) $charset .= "~@#$%^*()_±={}|][";

        for ($i = 0; $i < $maxlength; $i++) $key .= $charset[(mt_rand(0, (strlen($charset) - 1)))];

        return $key;
    }
}
