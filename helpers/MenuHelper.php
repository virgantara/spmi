<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

/**
 * Css helper class.
 */
class MenuHelper
{
	public static function getMenuItems()
	{

		// if(empty(Yii::$app->user->identity))
		// {
		// 	$session = Yii::$app->session;
		//     $session->remove('token');
		//     Yii::$app->user->logout();
		//     $url = Yii::$app->params['sso_logout'];

		//     return [];
		// }
		// $userRole = Yii::$app->user->identity->access_role;
		$menuItems = [];

		$currentRoute = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

		$menuItems[] = [
			'template' => '<a href="{url}">{label}</a>',
			'label' => '<i class="fa fa-home"></i> <span>Dashboard</span>',
			'url' => ['site/dashboard'],
		];

		if (
			Yii::$app->user->can('auditor') || Yii::$app->user->can('auditee')
		) {

			$menuItems[] = [
				'label' => '<i class="fa fa-users"></i><span> Auditor</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'visible' => Yii::$app->user->can('admin'),
				'items' => [
					[
						'label' => '<span> Input Auditor</span>',
						'url' => ['auditor/index'],
					],
					[
						'label' => '<span> Penugasan Auditor</span>',
						'url' => ['ami-auditor/index'],
					],
					[
						'label' => '<span> Rekap Auditor</span>',
						'url' => ['ami-auditor/rekap'],
					],
					[
						'label' => '<span> Rekap Penugasan Auditor</span>',
						'url' => ['ami-auditor/rekap-auditor'],
					],
					[
						'label' => '<span> Surat Tugas Auditor</span>',
						'url' => ['/surat/index'],
					],

				]
			];

			// $menuItems[] = [
			// 	'template' => '<a href="{url}">{label}</a>',
			// 	'label' => '<i class="fa fa-book"></i> <span>Dokumen Umum</span>',
			// 	'url' => ['dokumen/umum'],
			// ];

			$menuItems[] = [
				'label' => '<i class="fa fa-file-text"></i><span> Penetapan</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'items' => [

					[
						'label' => '<span> Tagihan Dokumen</span>',
						'url' => 'javascript:void(0)',
					],
					[
						'label' => '<span> Standar Mutu</span>',
						'url' => ['indikator/index'],
					],
					[
						'label' => '<span> Indikator Khusus</span>',
						'url' => ['indikator/ami'],
						'visible' => Yii::$app->user->can('admin'),
					],
				]
			];

			$menuItems[] = [
				'label' => '<i class="fa fa-file-text"></i><span> Pelaksanaan</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'items' => [
					// [
					// 	'label' => '<span> Tagihan Dokumen</span>',
					// 	'url' => 'javascript:void(0)',
					// ],
					// [
					// 	'label' => '<span> Pelaksanaan Standar</span>',
					// 	'url' => 'javascript:void(0)',
					// ],
					[
						'label' => '<span> Dokumen Umum</span>',
						'url' => ['dokumen/umum'],
					],
					[
						'label' => '<span> Dokumen Personal</span>',
						'url' => ['dokumen/personal'],
						'visible' => Yii::$app->user->identity->access_role == 'auditee',
					],
					[
						'label' => '<span> Master Dokumen</span>',
						'url' => ['dokumen/personal'],
						'visible' => Yii::$app->user->can('admin'),
					],
					[
						'label' => '<span> Kinerja</span>',
						'url' => ['site/kinerja'],
						// 'visible' => Yii::$app->user->can('admin'),
					],
				]
			];

			$menuItems[] = [
				'label' => '<i class="fa fa-file-text"></i><span> Evaluasi</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'items' => [
					[
						'label' => '<span> Audit Mutu Internal</span>',
						'url' => ['asesmen/ami'],
						// 'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Monev</span>',
						'url' => ['monev/index']
					],
					[
						'label' => '<span> Amanah Kinerja</span>',
						'url' => ['amanah-kinerja/index']
					],
				]
			];

			$menuItems[] = [
				'label' => '<i class="fa fa-file-text"></i><span> Pengendalian</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'items' => [
					[
						'label' => '<span> RTM</span>',
						'url' => ['rtm/index'],
						'visible' => Yii::$app->user->can('auditee')
					],
					[
						'label' => '<span> Bahan RTM</span>',
						'url' => ['rtm/bahan'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Review Standar</span>',
						'url' => ['indikator/review'],
						// 'visible' => Yii::$app->user->can('admin')
					],
				]
			];

			$menuItems[] = [
				'label' => '<i class="fa fa-file-text"></i><span> Peningkatan</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'items' => [

					[
						'label' => '<span> Laporan Peningkatan</span>',
						'url' => ['peningkatan/index'],
						'visible' => Yii::$app->user->can('auditee')
					],

				]
			];
		}
		
		$menuItems[] = [
			'label' => '<i class="fa fa-bar-chart-o"></i><span> SPME</span><span class="fa fa-chevron-down"></span>',
			'url' => 'javascript:void(0)',
			'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
			'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
			// 'visible' => Yii::$app->user->can('admin'),
			'items' => [

				[
					'label' => '<span> Akreditas Prodi</span>',
					'url' => ['prodi/index'],
					// 'visible' => Yii::$app->user->can('admin')
				],


			]
		];

		if (
			Yii::$app->user->can('auditor') || Yii::$app->user->can('auditee')
		) {

			// $menuItems[] = [
			// 	'template' => '<a href="{url}">{label}</a>',
			// 	'label' => '<i class="fa fa-book"></i> <span>Survey</span>',
			// 	'url' => ['survey/index'],
			// ];

			// $menuItems[] = [
			// 	'label' => '<i class="fa fa-tasks"></i><span> Capaian</span><span class="fa fa-chevron-down"></span>',
			// 	'url' => 'javascript:void(0)',
			// 	'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
			// 	'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
			// 	'items' => [

			// 		[
			// 			'label' => '<span> Master Capaian</span>',
			// 			'url' => ['capaian-kinerja/index'],
			// 			'visible' => Yii::$app->user->can('admin')
			// 		],
			// 		[
			// 			'label' => '<span> Master Periode Capaian</span>',
			// 			'url' => ['periode/index'],
			// 			'visible' => Yii::$app->user->can('admin')
			// 		],
			// 		[
			// 			'label' => '<span> Input Capaian</span>',
			// 			'url' => ['capaian-hasil/input'],
			// 			'visible' => Yii::$app->user->can('auditee')
			// 		],
			// 		[
			// 			'label' => '<span> Survey</span>',
			// 			'url' => ['survey/index'],
			// 		],
			// 		// [
			// 		// 	'label' => '<span> Capaian Kinerja</span>',
			// 		// 	'url' => ['capaian-hasil/show'],
			// 		// 	'visible' => Yii::$app->user->can('auditee')
			// 		// ],


			// 	]
			// ];


			$menuItems[] = [
				'label' => '<i class="fa fa-tasks"></i><span> Utility</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'visible' => Yii::$app->user->can('admin'),
				'items' => [

					[
						'label' => '<span> Master Ruang Lingkup</span>',
						'url' => ['ruang-lingkup/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Master Temuan</span>',
						'url' => ['/temuan/master-temuan'],
						'visible' => Yii::$app->user->can('admin')
					],
					// [
					// 	'label' => '<span> Map Hasil AMI</span>',
					// 	'url' => ['surat/hasil-ami'],
					// ],
					[
						'label' => '<span> Pembagian Kriteria</span>',
						'url' => ['/pembagian-kriteria/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Pembagian RTM</span>',
						'url' => ['/pembagian-rtm/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Penanggung Jawab RTM</span>',
						'url' => ['/penanggung-jawab-rtm/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Tujuan Audit Mutu Internal</span>',
						'url' => ['tujuan-audit/index'],
						'visible' => Yii::$app->user->can('admin')
					],


				]
			];

			$menuItems[] = [
				'label' => '<i class="fa fa-cog"></i><span>Master</span><span class="fa fa-chevron-down"></span>',
				'url' => 'javascript:void(0)',
				'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
				'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
				'visible' => Yii::$app->user->can('admin'),
				'items' => [
					[
						'label' => '<span> Amanah Kinerja Template</span>',
						'url' => ['amanah-kinerja-master/index'],
						'visible' => Yii::$app->user->can('admin'),
					],
					// [
					// 	'label' => '<span> AMI</span>',
					// 	'url' => ['ami-unit/index'],
					// 	'visible' => Yii::$app->user->can('admin'),
					// ],
					[
						'label' => '<span> Aspek</span>',
						'url' => ['/aspek/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Auditee</span>',
						'url' => ['unit-kerja/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Auth Item</span>',
						'url' => ['auth-item/index'],
						'visible' => Yii::$app->user->can('theCreator')
					],
					[
						'label' => '<span> Auth Item Child</span>',
						'url' => ['auth-item-child/index'],
						'visible' => Yii::$app->user->can('theCreator')
					],
					[
						'label' => '<span> Bpm</span>',
						'url' => ['bpm/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Jenis</span>',
						'url' => ['/master-jenis/index'],
						'visible' => Yii::$app->user->can('theCreator')
					],
					[
						'label' => '<span> Jenis Dokumen</span>',
						'url' => ['master-jenis/dokumen'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Jenis Peningkatan</span>',
						'url' => ['master-jenis/peningkatan'],
						'visible' => Yii::$app->user->can('admin')
					],
					// [
					// 	'label' => '<span> Jenis RTM</span>',
					// 	'url' => ['/master-jenis/rtm'],
					// 	'visible' => Yii::$app->user->can('admin')
					// ],
					[
						'label' => '<span> Jenjang</span>',
						'url' => ['/jenjang/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Jenjang Map</span>',
						'url' => ['/jenjang-map/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Koordinator RTM</span>',
						'url' => ['unit-kerja/koordinator-rtm'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Kriteria</span>',
						'url' => ['/kriteria/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Lembaga Akreditasi</span>',
						'url' => ['/lembaga-akreditasi/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Lingkup Bahasan</span>',
						'url' => ['/lingkup-bahasan/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Monev</span>',
						'url' => ['monev/master'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Periode AMI</span>',
						'url' => ['ami/index'],
					],
					[
						'label' => '<span> Prodi</span>',
						'url' => ['/pembagian-prodi/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Status Akreditasi</span>',
						'url' => ['status-akreditasi/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Tujuan Audit</span>',
						'url' => ['tujuan-audit/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					[
						'label' => '<span> Users</span>',
						'url' => ['/user/index'],
						'visible' => Yii::$app->user->can('admin')
					],
					// [
					// 	'label' => '<span> Variabel Kategori</span>',
					// 	'url' => ['/variable-kategori/index'],
					// 	'visible' => Yii::$app->user->can('theCreator')
					// ],

				]
			];
		}



		if (Yii::$app->user->isGuest) {
			$menuItems[] = [
				'template' => '<a href="{url}" data-method="POST">{label}</a>',
				'label' => '<i class="fa fa-sign-in"></i>Login',
				'url' => ['site/login']
			];
		}

		return $menuItems;
	}



	public static function getTopMenus()
	{
		$menuItems = [];
		$list_apps = [];
		if (!Yii::$app->user->isGuest) {
			$key = Yii::$app->params['jwt_key'];
			$session = Yii::$app->session;
			if ($session->has('token')) {
				$token = $session->get('token');
				try {
					$decoded = \Firebase\JWT\JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
					foreach ($decoded->apps as $d) {
						$list_apps[] = [
							'template' => '<a target="_blank" href="{url}">{label}</a>',
							'label' => $d->app_name,
							'url' => $d->app_url . $token
						];
					}
				} catch (\Exception $e) {
					// return Yii::$app->response->redirect(Yii::$app->params['sso_login']);
				}
			}
		}

		if (!Yii::$app->user->isGuest) {



			// $menuItems[] = [
			// 	'template' => '<a id="access_role" href="{url}" class="icon-menu">{label}</a>',
			// 	'label' => ' Peran: <strong>' . Yii::$app->user->identity->access_role . '</strong>',
			// 	'url' => ['user/change']
			// 	// 'submenuTemplate' => "\n<ul class='dropdown-menu notifications'>\n{items}\n</ul>\n",
			// 	// 'items' => $list_apps

			// ];

			$menuItems[] = [
				'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span>{label}</span> <i class="icon-submenu fa fa-angle-down"></i></a>',
				'label' => Yii::$app->user->identity->nama,
				'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
				'items' => [
					[
						'template' => '<a href="{url}">{label}</a>',
						'label' => '<i class="fa fa-user pull-right"></i> My Profile',
						'url' => ['user/profil']
					],
					// [
					// 	'template' => '<a href="{url}">{label}</a>',
					// 	'label' => '<i class="fa fa-users pull-right"></i> Change Role',
					// 	'url' => ['user/change']
					// ],
					[
						'template' => '<a href="{url}" data-method="POST">{label}</a>',
						'label' => '<i class="fa fa-sign-out  pull-right"></i>Sign Out',
						'url' => ['site/logout']
					]
				]

			];



			$menuItems[] = [
				'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-layers"></i> <span>{label}</span> <i class="icon-submenu fa fa-angle-down"></i></a>',
				'label' => 'Your apps',
				'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
				'items' => $list_apps

			];

			$menuItems[] = [
				'template' => '<a id="list_notif" href="#" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expandable="false"><i class="fa fa-bell-o"></i> <span class="badge bg-red" id="count-notif"></span></a>',
				'label' => '',
				// 'submenuTemplate' => "\n<ul class='dropdown-menu notifications'>\n{items}\n</ul>\n",
				// 'items' => $list_apps

			];


			// $menuItems[] = [
			// 	'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <span>{label}</span> <i class="icon-submenu fa fa-angle-down"></i></a>',
			// 	'label' => '2023',
			// 	'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
			// 	'items' => [
			// 		[
			// 			'template' => '<a href="{url}">{label}</a>',
			// 			'label' => '2021',
			// 			'url' => ['user/profil']
			// 		],
			// 		[
			// 			'template' => '<a href="{url}">{label}</a>',
			// 			'label' => '2022',
			// 			'url' => ['user/profil']
			// 		]
			// 	]

			// ];
		}


		return $menuItems;
	}

	public static function getUserMenus()
	{
		$menuItems = [];

		if (!Yii::$app->user->isGuest) {


			$menuItems[] = [
				'template' => '<a data-widget="pushmenu" href="{url}" role="button" class="nav-link">{label}</a>',
				'label' => '<i class="fas fa-bars"></i>',
				'url' => 'javascript:void(0)'
			];
		}


		return $menuItems;
	}
}
