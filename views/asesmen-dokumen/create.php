<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AsesmenDokumen */

$this->title = Yii::t('app', 'Create Asesmen Dokumen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asesmen Dokumens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    	   </div>
        </div>
    </div>
</div>