<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Buat Audit Mutu Internal';
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// echo '<pre>';print_r($id);exit;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



    <?= $this->render('_form', [
        'model' => $model,
        // 'id' => $id,
        // 'ami' => $ami,
    ]) ?>
    	   </div>
        </div>
    </div>
</div>