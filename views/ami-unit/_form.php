<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\UnitKerja;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */
/* @var $form yii\widgets\ActiveForm */


$listAmi1 = Ami::find()->where(['status_aktif' => 1])->orderBy(['nama' => SORT_ASC])->all();
$listAmi = ArrayHelper::map($listAmi1, 'id', 'nama');
$listUnit1 = UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all();
$listUnit = ArrayHelper::map($listUnit1, 'id', 'nama');
// $list_jenis_unit = MyHelper::getJenisAmi();

?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>


    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Periode AMI</label>
        <div class="col-sm-9">

            <?= $form->field($model, 'ami_id')->widget(Select2::classname(), [
                'data' => $listAmi,

                'options' => ['placeholder' => Yii::t('app', '- Pilih Periode AMI -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Unit</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'unit_id')->widget(Select2::classname(), [
                'data' => $listUnit,

                'options' => ['placeholder' => Yii::t('app', '- Pilih Unit Kerja -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(false) ?>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Lokasi</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'lokasi')->textInput(['placeholder' => Yii::t('app', 'Masukkan lokasi')])->label(false) ?>
        </div>
    </div>


    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>