<?php

use app\helpers\MyHelper;
use app\models\Auditor;
use app\models\StatusAuditor;
use kartik\date\DatePicker;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = $model->ami->nama . ' - ' . $model->unit->nama;
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();

?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?php // Html::a('<i class="fa fa-plus"></i> Tambah Auditor', ['ami-auditor/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'btn-add', 'data-ami_unit' => $model->id]) 
                ?>
                <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['index'], ['class' => 'btn btn-info']) ?>

            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        // 'unit_id',
                        // 'ami_id',
                        [
                            'attribute' => 'ami_id',
                            'filter' => $list_periode,
                            'value' => function ($data) {
                                return (!empty($data) ? $data->ami->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'unit_id',
                            'filter' => $list_unit,
                            'value' => function ($data) {
                                return (!empty($data) ? $data->unit->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'status_ami',
                            'filter' => $list_ami,
                            'value' => function ($data) {
                                // return (!empty($data) ? $data->unit->nama : null);
                                $list_ami = MyHelper::getStatusAmi();
                                return (!empty($list_ami[$data->status_ami]) ? $list_ami[$data->status_ami] : null);
                            }
                        ],
                        [
                            'attribute' => 'lokasi',
                            // 'filter' => $list_ami,
                            'value' => function ($data) {
                                // return (!empty($data) ? $data->unit->nama : null);
                                // $list_ami = MyHelper::getStatusAmi();
                                return (!empty($data->lokasi) ? $data->lokasi : 'Belum Ditentukan');
                            }
                        ],

                    ],
                ]) ?>

                <?php // Html::a('<i class="fa fa-plus"></i> Set Auditee', ['ami-auditor/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'btn-set-auditee', 'data-ami_unit' => $model->id]) 
                ?>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th style="text-align: center" class="alert alert-info" colspan="10"><strong>Nama Petugas AMI</strong></th>
                        </tr>
                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>No</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Jenis Petugas</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Nama Petugas</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Keterangan</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Aksi</strong></th>

                        </tr>
                        <tr>
                            <th style="text-align: center">1</th>
                            <th style="text-align: center">Auditee</th>
                            <?php
                            // echo '<pre>';print_r($model->unit->penanggung_jawab);die;
                            ?>
                            <th><?= (isset($model->unit->penanggung_jawab) ? $model->unit->penanggung_jawab : 'Belum ditentukan') ?></th>
                            <th style="text-align: center;color: red;">
                                <?= (isset($model->unit->penanggung_jawab) ? $model->unit->penanggung_jawab : 'Belum ditentukan') ?>
                            </th>
                        </tr>

                        <?php


                        $i = 2;
                        foreach ($list_auditor as $a) :
                            // echo '<pre>';print_r($a);exit;
                            $auditor = Auditor::find()->where([
                                'id' => $a->auditor_id,
                            ])->one();

                            $status = StatusAuditor::find()->where([
                                'id' => $a->status_id,
                            ])->one();
                        ?>

                            <tr>
                                <td style="text-align: center"><?= $i; ?></td>
                                <td style="text-align: center"><?= $status->nama ?></td>
                                <td><?= $auditor->nama ?></td>
                                <td style="text-align: center;color: red;">
                                    <?= $auditor->nama ?>
                                </td>
                                <td style="text-align: center">
                                    <?= Html::a('', ['ami-auditor/view', 'id' => $a->id], ['class' => 'glyphicon glyphicon-eye-open']) ?>
                                    <?= Html::a('', ['ami-auditor/update', 'id' => $a->id], ['class' => 'glyphicon glyphicon-pencil']) ?>
                                    <?= Html::a('', ['ami-auditor/deletee', 'id' => $a->id, 'ami_id' => $model->id], ['class' => 'glyphicon glyphicon-trash']) ?>
                                </td>

                            </tr>
                        <?php
                            $i++;
                        endforeach;
                        ?>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>


<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);

$list_petugas = StatusAuditor::find()->orderBy(['nama' => SORT_ASC])->all();
?>
<form action="" id="form-krs">
    <div class="form-group">
        <label for="">Nama Petugas</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control', 'id' => 'nama_auditor', 'placeholder' => 'Ketik nama dosen', 'autocomplete' => 'off']) ?>
        <?php // Html::hiddenInput('email', '', ['id' => 'email']) 
        ?>
        <?= Html::hiddenInput('niy', '', ['id' => 'niy']) ?>
        <?= Html::hiddenInput('nidn', '', ['id' => 'nidn']) ?>
        <?= Html::hiddenInput('ami_unit_id', '', ['id' => 'ami_unit_id']) ?>
    </div>
    <div class="form-group">
        <label for="">Status Petugas</label>
        <?= Html::dropDownList('status_id', '', ArrayHelper::map($list_petugas, 'id', 'nama'), ['class' => 'form-control', 'prompt' => '- Jenis Petugas -']) ?>
    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>



<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-auditee',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-auditee">
    <div class="form-group">
        <label for="">Nama Auditee</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control', 'id' => 'nama_auditee', 'placeholder' => 'Ketik nama dosen', 'autocomplete' => 'off']) ?>
        <?php // Html::hiddenInput('email', '', ['id' => 'email']) 
        ?>
        <?= Html::hiddenInput('niy', '', ['id' => 'niy_auditee']) ?>
        <?= Html::hiddenInput('nidn', '', ['id' => 'nidn_auditee']) ?>
        <?= Html::hiddenInput('email', '', ['id' => 'email_auditee']) ?>
        <?= Html::hiddenInput('ami_unit_id', '', ['id' => 'ami_unit_id_auditee']) ?>
    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan-auditee']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>


<!-- Modal Dokumen -->

<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-dokumen',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-dokumen">
    <div class="form-group">
        <label for="">Kode Cetak Berita</label>
        <?= Html::textInput('dokumen_berita_acara', '', ['class' => 'form-control', 'id' => 'dokumen_berita_acara']) ?>
        <label for="">Kode Cetak Tilik</label>
        <?= Html::textInput('dokumen_tilik', '', ['class' => 'form-control', 'id' => 'dokumen_tilik']) ?>
        <label for="">Kode Cetak Temuan</label>
        <?= Html::textInput('dokumen_temuan', '', ['class' => 'form-control', 'id' => 'dokumen_temuan']) ?>
        <?= Html::hiddenInput('ami_unit_id', '', ['id' => 'ami_unit_id_dokumen']) ?>


    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan-dokumen']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).bind("keyup.autocomplete", function() {

        $('#nama_auditor').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).bind("keyup.autocomplete", function() {

        $('#nama_auditee').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy_auditee").val(ui.item.items.NIY)
                $("#nidn_auditee").val(ui.item.items.NIDN)
                $("#email_auditee").val(ui.item.items.email)
            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy_auditee").val(ui.item.items.NIY)
                $("#nidn_auditee").val(ui.item.items.NIDN)
                $("#email_auditee").val(ui.item.items.email)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-krs").serialize()

        $.ajax({
            url: "/ami-auditor/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Sending Email..",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-simpan-auditee", function(e) {
        e.preventDefault();

        var obj = $("#form-auditee").serialize()

        $.ajax({
            url: "/ami-unit/ajax-add-auditee",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-simpan-dokumen", function(e) {
        e.preventDefault();

        var obj = $("#form-dokumen").serialize()

        $.ajax({
            url: "/ami-unit/ajax-dokumen",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-dokumen").modal("hide")
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        var ami_unit_id = $(this).data("ami_unit")

        $("#ami_unit_id").val(ami_unit_id)
        $("#modal").modal("show")

    });

    $(document).on("click", "#btn-set-auditee", function(e) {
        e.preventDefault();
        var ami_unit_id = $(this).data("ami_unit")

        $("#ami_unit_id_auditee").val(ami_unit_id)
        $("#modal-auditee").modal("show")

    });

    $(document).on("click", "#btn-dokumen", function(e) {
        e.preventDefault();
        var ami_unit_id = $(this).data("ami_unit")

        $("#ami_unit_id_dokumen").val(ami_unit_id)
        $("#modal-dokumen").modal("show")

    });
</script>
<?php JSRegister::end(); ?>