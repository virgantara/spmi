<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ami-unit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'unit_id') ?>

    <?= $form->field($model, 'ami_id') ?>

    <?= $form->field($model, 'status_ami') ?>

    <?= $form->field($model, 'jenis_unit') ?>

    <?php // echo $form->field($model, 'auditee') ?>

    <?php // echo $form->field($model, 'email_auditee') ?>

    <?php // echo $form->field($model, 'lokasi') ?>

    <?php // echo $form->field($model, 'wakil_auditee') ?>

    <?php // echo $form->field($model, 'penanggung_jawab') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
