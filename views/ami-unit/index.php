<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmiUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Audit Mutu Internal';
$this->params['breadcrumbs'][] = $this->title;

$list_periode = ArrayHelper::map(\app\models\Ami::find()->all(), 'id', 'nama');
$list_unit = ArrayHelper::map(\app\models\UnitKerja::find()->all(), 'id', 'nama');
$list_ami = MyHelper::getStatusAmi();
$list_jenis_unit = MyHelper::getJenisAmi();

$statusData = ['0' => 'Proses', '1' => 'Selesai'];
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <p>
                    <?php Html::a('<i class="fa fa-plus"></i> Buat AMI', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'attribute' => 'ami_id',
                        'vAlign' => 'middle',
                        'label' => 'AMI',
                        'filter' => $list_periode,
                        'value' => function ($data) {
                            return (!empty($data) ? $data->ami->nama : null);
                        }
                    ],
                    [
                        'attribute' => 'unit_id',
                        'vAlign' => 'middle',
                        'label' => 'Unit',
                        'filter' => $list_unit,
                        'value' => function ($data) {
                            return (!empty($data) ? $data->unit->nama : null);
                        }
                    ],
                    [
                        'attribute' => 'jenis_unit',
                        'vAlign' => 'middle',
                        'filter' => $list_jenis_unit,
                        'value' => function ($data) {

                            $list_jenis_unit = MyHelper::getJenisAmi();
                            return (!empty($list_jenis_unit[$data->jenis_unit]) ? $list_jenis_unit[$data->jenis_unit] : null);
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'lokasi',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('theCreator'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    [
                        'attribute' => 'status_ami',
                        'vAlign' => 'middle',
                        // 'contentOptions'    => [
                        //     'width' => '5%',
                        // ],
                        'format' => 'raw',
                        'filter' => $statusData,
                        'hAlign' => 'center',
                        'value' => function ($data) use ($list_ami) {

                            $status = 'warning';
                            if ($data->status_ami == 1) {
                                $status = 'success';
                            }
                            $data = (!empty($list_ami[$data->status_ami]) ? $list_ami[$data->status_ami] : null);

                            return Html::a('<i class="fa fa-refresh"></i> ' . $data, '', ['class' => 'btn btn-sm btn-' . $status . ' btn-xs']);

                            // return !empty($statusData[$data->status_ami]) ? $statusData[$data->status_ami] : null;
                        }
                    ],
                    [
                        'label' => 'Rencana',
                        'format' => 'raw',
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'value' => function ($data) {

                            return Html::a(
                                '<i class="fa fa-cog"></i> Rencana',
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-sm btn-primary btn-xs',
                                    'onclick' => "window.open('" . Yii::$app->urlManager->createUrl(['asesmen/rencana', 'id' => $data->id]) . "', '_blank')"
                                ]
                            );
                        }
                    ],
                    // 'status_ami',
                    // 'jenis_ami',

                    // 'unit_id',
                    // 'ami_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        // Ganti 'lokasi' dengan atribut yang ingin Anda cek
                        if (empty($model->lokasi)) {
                            return ['class' => 'danger']; // Menambahkan kelas CSS 'danger' untuk baris kosong pada kolom 'lokasi'
                        }
                    },
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>