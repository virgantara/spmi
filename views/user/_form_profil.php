<?php

use app\models\Prodi;
use app\models\UnitKerja;
use app\rbac\models\AuthItem;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */

$list_roles = ArrayHelper::map(AuthItem::getRoles(), 'name', 'name');

?>
<?php foreach (AuthItem::getRoles() as $item_name) : ?>
    <?php $roles[$item_name->name] = $item_name->name ?>
<?php endforeach ?>
<?php foreach (UnitKerja::find()->all() as $item_name) : ?>
    <?php $unit_kerja[$item_name->id] = $item_name->nama ?>
<?php endforeach ?>


<div class="body">

    <?php $form = ActiveForm::begin(['id' => 'form-user']); ?>
    <?= $form->errorSummary($user, ['header' => '<div class="alert alert-danger">', 'footer' => '</div>']); ?>

    <?= $form->field($user, 'nama')->input('nama', ['placeholder' => Yii::t('app', 'Enter e-mail')]) ?>

    <?= $form->field($user, 'username')->textInput(
        ['placeholder' => Yii::t('app', 'Create username'), 'autofocus' => true]
    ) ?>
    
    <?= $form->field($user, 'email')->input('email', ['placeholder' => Yii::t('app', 'Enter e-mail')]) ?>

    <?php if ($user->scenario === 'create') : ?>

        <?= $form->field($user, 'password')->widget(
            PasswordInput::classname(),
            ['options' => ['placeholder' => Yii::t('app', 'Create password')]]
        ) ?>

    <?php else : ?>

        <?= $form->field($user, 'password')->widget(
            PasswordInput::classname(),
            ['options' => ['placeholder' => Yii::t('app', 'Change password ( if you want )')]]
        ) ?>

    <?php endif ?>

    <div class="form-group">
        <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'), ['class' => $user->isNewRecord
            ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Cancel'), ['user/profil'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-add-role", function(e) {
        e.preventDefault()

        $("#exampleModal").modal("show");
    })

    $(document).on('change', '#dosen', function(e) {
        e.preventDefault();
        $("#nim").val($(this).val());
    });

    $(document).on("click", "#btn-save", function(e) {
        e.preventDefault()

        var obj = new Object;
        obj.user_id = $("#user_id").val();
        obj.item_name = $("#item_name").val();

        $.ajax({
            type: 'POST',
            url: "/user/ajax-add-role",
            data: {
                dataPost: obj
            },
            async: true,
            error: function(e) {
                Swal.hideLoading();

            },
            beforeSend: function() {
                Swal.showLoading();
            },
            success: function(data) {
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {

                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                }
            }
        })
    })

    $(document).on("click", ".btn-remove-role", function(e) {
        e.preventDefault()

        var obj = new Object;
        obj.user_id = $(this).data("user");
        obj.item_name = $(this).data("item");

        $.ajax({
            type: 'POST',
            url: "/user/ajax-delete-role",
            data: {
                dataPost: obj
            },
            async: true,
            error: function(e) {
                Swal.hideLoading();
            },
            beforeSend: function() {
                Swal.showLoading();
            },
            success: function(data) {
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {

                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                }
            }
        })
    })
</script>
<?php JSRegister::end(); ?>