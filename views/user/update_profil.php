<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

$this->title = 'Update Profile Akun: ' . $user->nama;
$this->params['breadcrumbs'][] = ['label' => 'Profil', 'url' => ['user/profil']];
$this->params['breadcrumbs'][] = 'Update Profil Akun';
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?= $this->render('_form_profil', [
                    'user' => $user,
                ]) ?>
            </div>
        </div>
    </div>
</div>