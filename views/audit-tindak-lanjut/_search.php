<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuditTindakLanjutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audit-tindak-lanjut-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'temuan_id') ?>

    <?= $form->field($model, 'rencana_tindak_lanjut') ?>

    <?= $form->field($model, 'jadwal_rtl') ?>

    <?= $form->field($model, 'realisasi_tindak_lanjut') ?>

    <?php // echo $form->field($model, 'efektifitas') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
