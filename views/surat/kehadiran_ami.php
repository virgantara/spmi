<?php

use app\helpers\MyHelper;
use app\models\Bpm;
?>

<table border="1" width="100%" style="padding: 4px;">
    <tr>
        <td width="15%" rowspan="4"></td>

        <td width="40%" style="text-align: center;" rowspan="4">
            <br><br>
            <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
            <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
            <br>
            <br>
        </td>
        <td width="20%">Kode Formulir</td>
        <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_kehadiran_kode) ? $amiUnit->ami->form_kehadiran_kode : '-') ?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Pembuatan</td>
        <td width="25%"><?= (isset($amiUnit->ami->form_kehadiran_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_kehadiran_tanggal_pembuatan) : '-') ?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Revisi</td>
        <td width="25%"><?= (isset($amiUnit->ami->form_kehadiran_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_kehadiran_tanggal_revisi) : '-') ?></td>
    </tr>
    <tr>
        <td width="20%">Tanggal Efektif</td>
        <td width="25%"><?= (isset($amiUnit->ami->form_kehadiran_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_kehadiran_tanggal_efektif) : '-') ?></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center;">DAFTAR KEHADIRAN AUDIT MUTU INTERNAL</td>
    </tr>
</table>
<br><br><br>
<table width="100%" style="padding: 4px;">
    <tr>
        <td width="20%">Tahun</td>
        <td width="80%">: <?= (isset($amiUnit->ami->tahun) ? $amiUnit->ami->tahun : '-') ?></td>
    </tr>
    <tr>
        <td>Auditee</td>
        <td>: <?= (isset($amiUnit->unit->nama) ? $amiUnit->unit->nama : '-') ?></td>
    </tr>
    <tr>
        <td>Kategori</td>
        <td>: <?= (isset($amiUnit->unit->jenis) ? MyHelper::getJenisUnit()[$amiUnit->unit->jenis] : '-') ?></td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>: <?= $jenjang ?></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>: <?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
    </tr>
    <?php
    for ($i = 1; $i < $jumlahAuditor; $i++) :
    ?>
        <tr>
            <?php
            if ($i == 1) :
            ?>
                <td>Auditor</td>
            <?php
            else :
            ?>
                <td></td>
            <?php
            endif;
            ?>
            <td>: <?= $i ?>. <?= MyHelper::getAuditor($amiUnit->id, $i)['nama'] ?? ''; ?></td>
        </tr>
    <?php
    endfor;
    ?>
</table>
<br><br>
<?php
$jumlahBaris = 25;
?>
<table>
    <tr>
        <td width="80%">
            <table border="1" width="100%" style="padding: 5px;">
                <tr style="text-align: center;">
                    <td width="8%">No.</td>
                    <td width="60%">Nama</td>
                    <td width="32%">Unit Kerja/Bagian</td>
                </tr>
                <?php
                for ($i = 1; $i <= $jumlahBaris; $i++) :
                    if ($i < 4) {
                ?>

                        <tr style="text-align: center;">
                            <td><?= $i ?>.</td>
                            <td style="text-align: left;"><?= MyHelper::getAuditor($amiUnit->id, $i)['nama'] ?? ''; ?></td>
                            <td></td>
                        </tr>
                    <?php
                    } else {
                    ?>

                        <tr style="text-align: center;">
                            <td><?= $i ?>.</td>
                            <td></td>
                            <td></td>
                        </tr>
                <?php
                    }
                endfor;
                ?>
            </table>
        </td>
        <td width="20%">
            <table border="1" width="100%" style="padding: 5px;">
                <tr style="text-align: center;">
                    <td colspan="2" width="100%">Tanda Tangan</td>
                </tr>
                <?php
                for ($i = 1; $i <= $jumlahBaris; $i++) :
                ?>
                    <tr>
                        <?php
                        if ($i % 2 == 0) :
                        ?>
                            <td style="background-color: #D3D3D3;text-align: center;"></td>
                            <td><small><?= $i ?></small></td>
                        <?php
                        else :
                        ?>
                            <td><small><?= $i ?></small></td>
                            <td style="background-color: #D3D3D3;text-align: center;"></td>
                        <?php
                        endif;
                        ?>
                    </tr>
                <?php
                endfor;
                ?>
            </table>
        </td>
    </tr>

</table>

<br><br><br><br><br>

<table>
    <tr>
        <td width="60%"></td>
        <td>Ponorogo, <?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Mengetahui,</td>
    </tr>
    <tr>
        <td></td>
        <td>Ketua Tim Auditor</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? ''; ?></td>
    </tr>
</table>