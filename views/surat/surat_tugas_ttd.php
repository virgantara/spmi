<?php

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Bpm;

date_default_timezone_set("Asia/Jakarta");
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
$ami = $amiUnit->ami;
?>

<table>
    <tr>
        <td width="5%"></td>
        <td colspan="2" width="90%">

            <table border="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>Ponorogo, <?= MyHelper::convertTanggalIndo($ami->tanggal_ami_mulai); ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>Ketua BPM,</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td><?= (isset($ami->ketua_bpm) ? $ami->ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>NIY. <?= (isset($ami->nidn_ketua_bpm) ? $ami->nidn_ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%">Tembusan:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">1. Rektor Universitas Darussalam Gontor</td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">2. Para Wakil Rektor Universitas Darussalam Gontor</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td colspan="4">
            <table border="0">
                <tr>
                    <td style="text-align: center;">Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh Badan Penjaminan Mutu (BPM), Universitas Darussalam Gontor</td>
                </tr>
            </table>
        </td>
    </tr>

</table>