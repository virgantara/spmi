<?php

use app\helpers\MyHelper;

?>

<table border="1" width="100%" style="padding: 4px;">

    <tr>
        <td width="17%" rowspan="2"></td>
        <td width="34%" style="text-align: center;">
            <span style="font-size: 1.1em"><strong>FORMULIR</strong></span><br>

        </td>
        <td width="49%" rowspan="2">
            <table width="100%" style="font-size: 1.1em">
                <tr>
                    <td width="30%">No. Dok</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->dokumen_berita_acara) ? $amiUnit->dokumen_berita_acara : 'Belum dibuat') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Berlaku</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_penilaian_selesai) ? $amiUnit->ami->tanggal_penilaian_selesai : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">No Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->no_revisi) ? $amiUnit->ami->no_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_revisi) ? $amiUnit->ami->tanggal_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Lembar</td>
                    <td width="5%">:</td>
                    <td width="60%">1</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <span style="font-size: 1.1em"><strong>HASIL AUDIT MUTU INTERNAL</strong></span><br>
            <span style="font-size: 1.1em"><strong>UPPS DAN PROGRAM STUDI</strong></span><br>
        </td>
    </tr>
</table>

<br><br>

<table width="100%" style="font-size: 1.1em">
    <tr>
        <td width="30%">UPPS/Fakultas</td>
        <td width="70%">: <?= ($amiUnit->unit->jenis == 'fakultas' || $amiUnit->unit->jenis == 'satker' ? $amiUnit->unit->nama : '-') ?></td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>: <?= ($amiUnit->unit->jenis == 'prodi' ? $amiUnit->unit->nama : '-') ?></td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>: <?= $jenjang ?></td>
    </tr>
    <tr>
        <td>Tanggal Penilaian</td>
        <td>: <?= $amiUnit->ami->tanggal_penilaian_selesai ?></td>
    </tr>

</table>

<br><br>

<table border="1" width="100%" style="padding: 4px;font-size: 1.2em">
    <tr style="text-align: center;">
        <td width="25%">Auditee</td>
        <td width="25%">Auditor 1</td>
        <td width="25%">Auditor 2</td>
        <td width="25%">Auditor 3</td>
    </tr>
    <tr>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
    </tr>
    <tr style="font-size: 0.7em;text-align: center;">
        <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
    </tr>

</table>


<?php
if ($amiUnit->unit->jenis != 'satker') :
?>
    <p>
        A. Penilaian Hasil AMI
    </p>

    <table border="1" width="100%" style="padding: 4px;font-size: 1.2em">
        <tr>
            <th width="39%" colspan="3" style="text-align: center;">Rentang Nilai</th>
            <th width="30%" style="text-align: center;">Peringkat</th>
            <th width="31%" style="text-align: center;">Apresiasi</th>
        </tr>
        <tr>
            <td width="17%" style="text-align: center;">0</td>
            <td width="5%" style="text-align: center;">-</td>
            <td width="17%" style="text-align: center;">199</td>
            <td width="30%">Tidak Layak</td>
            <td width="31%">Innalillahi</td>
        </tr>
        <tr>
            <td width="17%" style="text-align: center;">200</td>
            <td width="5%" style="text-align: center;">-</td>
            <td width="17%" style="text-align: center;">300</td>
            <td width="30%">Baik</td>
            <td width="31%">Astagfirullah</td>
        </tr>
        <tr>
            <td width="17%" style="text-align: center;">301</td>
            <td width="5%" style="text-align: center;">-</td>
            <td width="17%" style="text-align: center;">360</td>
            <td width="30%">Baik Sekali</td>
            <td width="31%">Alhamdulillah</td>
        </tr>
        <tr>
            <td width="17%" style="text-align: center;">361</td>
            <td width="5%" style="text-align: center;">-</td>
            <td width="17%" style="text-align: center;">400</td>
            <td width="30%">Unggul</td>
            <td width="31%">Tabarakallah</td>
        </tr>

    </table>

    <p style="text-align: center;">Nilai AMI Perkriteria</p>

    <table border="1" width="100%" style="padding: 4px;font-size: 1.2em">
        <?php
        foreach ($listKriteria as $kriteria) :
        ?>
            <tr>
                <td width="25%" style="text-align: center;"><?= $kriteria->nama ?></td>
                <td width="50%"><?= $kriteria->keterangan ?></td>
                <td width="25%" style="text-align: center;">?</td>
            </tr>
        <?php
        endforeach;
        ?>

        <tr>
            <td colspan="2" width="75%" style="text-align: center;">Total</td>
            <td width="25%" style="text-align: center;">400</td>
        </tr>

    </table>

    <br><br>

    <table border="1" width="100%" style="padding: 4px;">
        <tr style="text-align: center;">
            <td width="33%">Nilai AMI</td>
            <td width="33%">Peringkat</td>
            <td width="33%">Apresiasi</td>
        </tr>
        <tr>
            <td><br><br><br><br></td>
            <td><br><br><br><br></td>
            <td><br><br><br><br></td>
        </tr>

    </table>
<?php
endif;
?>