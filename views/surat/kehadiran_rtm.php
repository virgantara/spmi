<?php

use app\helpers\MyHelper;
use app\models\Bpm;
?>
<table border="1" width="100%" style="padding: 4px;">
    <tr>
        <td width="17%" rowspan="2"></td>
        <td width="34%" style="text-align: center;">
            <span style="font-size: 1.2em"><strong>FORMULIR</strong></span><br>

        </td>
        <td width="49%" rowspan="2">
            <table width="100%" style="font-size: 1.1em">
                <tr>
                    <td width="30%">No. Dok</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->form_kehadiran_kode) ? $amiUnit->ami->form_kehadiran_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Berlaku</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_penilaian_selesai) ? $amiUnit->ami->tanggal_penilaian_selesai : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">No Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->no_revisi) ? $amiUnit->ami->no_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_revisi) ? $amiUnit->ami->tanggal_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Lembar</td>
                    <td width="5%">:</td>
                    <td width="60%">1</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <span style="font-size: 1.1em">DAFTAR HADIR </span><br>
            <span style="font-size: 1em">RAPAT TINJAUAN MANAJEMEN</span>
        </td>
    </tr>
</table>
<br><br><br>
<table width="100%" style="padding: 4px;">
    <tr>
        <td width="20%">Siklus/Tahun</td>
        <td width="80%">: <?= (isset($amiUnit->ami->tahun) ? $amiUnit->ami->tahun : '-') ?></td>
    </tr>
    <tr>
        <td>Fakultas/Pengelola</td>
        <td>: <?= (isset($amiUnit->unit->nama) ? $amiUnit->unit->nama : '-') ?></td>
    </tr>
    <tr>
        <td>Prodi/Unit Kerja</td>
        <td>: <?= (isset($amiUnit->unit->jenis) ? MyHelper::getJenisUnit()[$amiUnit->unit->jenis] : '-') ?></td>
    </tr>
    <tr>
        <td>Tgl/Bln/Thn</td>
        <td>: <?= MyHelper::convertTanggalIndo(date("Y-m-d")); ?></td>
    </tr>
</table>
<br><br>
<?php
$jumlahBaris = 25;
?>
<table>
    <tr>
        <td width="80%">
            <table border="1" width="100%" style="padding: 5px;">
                <tr style="text-align: center;">
                    <td width="8%">No.</td>
                    <td width="60%">Nama</td>
                    <td width="32%">Unit Kerja/Bagian</td>
                </tr>
                <?php
                for ($i = 1; $i <= $jumlahBaris; $i++) :
                ?>
                    <tr style="text-align: center;">
                        <td><?= $i ?>.</td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php
                endfor;
                ?>
            </table>
        </td>
        <td width="20%">
            <table border="1" width="100%" style="padding: 5px;">
                <tr style="text-align: center;">
                    <td colspan="2" width="100%">Tanda Tangan</td>
                </tr>
                <?php
                for ($i = 1; $i <= $jumlahBaris; $i++) :
                ?>
                    <tr>
                        <?php
                        if ($i % 2 == 0) :
                        ?>
                            <td style="background-color: #D3D3D3;text-align: center;">x</td>
                            <td><small><?= $i ?></small></td>
                        <?php
                        else :
                        ?>
                            <td><small><?= $i ?></small></td>
                            <td style="background-color: #D3D3D3;text-align: center;">x</td>
                        <?php
                        endif;
                        ?>
                    </tr>
                <?php
                endfor;
                ?>
            </table>
        </td>
    </tr>

</table>

<br><br><br><br><br>

<table>
    <tr>
        <td width="60%"></td>
        <td>Ponorogo, <?= MyHelper::convertTanggalIndo(date("Y-m-d")); ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Mengetahui,</td>
    </tr>
    <tr>
        <td></td>
        <td>Ketua Tim Auditor</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? ''; ?></td>
    </tr>
</table>