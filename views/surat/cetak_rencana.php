<?php

use app\helpers\MyHelper;
?>
<table width="100%">
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">

                <tr>
                    <td width="15%" rowspan="4"></td>

                    <td width="40%" style="text-align: center;" rowspan="4">
                        <br><br>
                        <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
                        <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
                        <br>
                        <br>
                    </td>
                    <td width="20%">Kode Formulir</td>
                    <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_rencana_kode) ? $amiUnit->ami->form_rencana_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pembuatan</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_rencana_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_rencana_tanggal_pembuatan) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Revisi</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_rencana_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_rencana_tanggal_revisi) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Efektif</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_rencana_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_rencana_tanggal_efektif) : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>RENCANA AUDIT MUTU INTERNAL</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td width="25%">Tanggal Audit</td>
                    <td width="75%" colspan="2"><?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
                </tr>
                <tr>
                    <td>Tempat Audit</td>
                    <td colspan="2"><?= (isset($amiUnit->lokasi) ? $amiUnit->lokasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Auditee</td>
                    <td colspan="2"><?= $amiUnit->unit->nama ?></td>
                </tr>
                <tr>
                    <td>Perwakilan Auditee</td>
                    <td colspan="2"><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                </tr>
                <tr>
                    <td>Ketua Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 1)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Anggota Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 2)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Sekertatis Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 3)['nomor_registrasi'] ?? '' ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>PENGESAHAN</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td width="25%">Auditee</td>
                    <td width="25%">Auditor 1</td>
                    <td width="25%">Auditor 2</td>
                    <td width="25%">Auditor 3</td>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <tr style="font-size: 0.7em;text-align: center;">
                    <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;font-size: 0.9em;">
                <tr>
                    <td colspan="2" width="95%">Tujuan Audit</td>
                    <td width="5%">( x )</td>
                </tr>
                <?php
                if (!empty($tujuanAudit)) {
                    $no = 1;
                    foreach ($tujuanAudit as $key => $ta) : ?>
                        <tr>
                            <td width="5%" style="text-align: center;"><?= $no ?>.</td>
                            <td width="90%"><?= $ta->tujuanAudit->tujuan_audit ?></td>
                            <td width="5%"></td>
                        </tr>
                    <?php
                        $no++;
                    endforeach;
                } else {
                    ?>
                    <tr>
                        <td colspan="3">data tidak tersedia</td>
                    </tr>
                <?php
                }

                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;font-size: 0.9em;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="3">Ruang lingkup:</td>
                            </tr>
                            <?php
                            if (!empty($ruangLingkup)) {
                                $no = 1;
                                foreach ($ruangLingkup as $key => $rl) : ?>
                                    <tr>
                                        <td width="5%"></td>
                                        <td width="2%"><?= $no ?>.</td>
                                        <td width="93%"><?= $rl->ruang_lingkup ?></td>
                                    </tr>
                                <?php
                                    $no++;
                                endforeach;
                            } else {
                                ?>
                                <tr>
                                    <td colspan="3">data tidak tersedia</td>
                                </tr>
                            <?php
                            }

                            ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;font-size: 0.85em;">
                <tr>
                    <td colspan="3" style="text-align: center;">Jadwal Kegiatan Audit</td>
                </tr>
                <tr style="text-align: center;">
                    <td width="15%">Waktu</td>
                    <td width="53%">Acara</td>
                    <td width="32%">PIC</td>
                </tr>
                <?php
                foreach ($listSusunan as $susunan) :
                ?>
                    <tr>
                        <td style="text-align: center;"><?= $susunan->waktu_mulai ?> - <?= $susunan->waktu_selesai ?></td>
                        <td><?= $susunan->acara ?></td>
                        <td><?= $susunan->pelaksana ?></td>
                    </tr>
                <?php
                endforeach;
                ?>
            </table>
        </td>
    </tr>


</table>