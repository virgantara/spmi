<?php

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Bpm;

date_default_timezone_set("Asia/Jakarta");
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

?>
<br><br><br><br><br><br><br><br><br>
<h3 style="text-align: center;">SURAT TUGAS</h3>
<p style="text-align: center;">Nomor: <?= (isset($ami->nomor_surat_tugas) ? $ami->nomor_surat_tugas : '-') ?></p>
<table>
    <tr>
        <td width="5%"></td>
        <td colspan="2">Saya yang bertandatangan di bawah ini,</td>
        <td width="5%"></td>
    </tr>
    <tr>
        <td></td>
        <td width="20%">Nama</td>
        <td width="70%">: <?= (isset($ami->ketua_bpm) ? $ami->ketua_bpm : "-") ?></td>
    </tr>
    <tr>
        <td></td>
        <td>NIY</td>
        <td>: <?= (isset($ami->nidn_ketua_bpm) ? $ami->nidn_ketua_bpm : "-") ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Jabatan</td>
        <td>: Ketua Badan Penjaminan Mutu UNIDA Gontor</td>
    </tr>
    <tr>
        <td></td>
        <td>Alamat</td>
        <td>: Gedung Terpadu Lantai 1</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Memberikan tugas kepada:</td>
    </tr>

    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table border="1" style="padding: 6px;">
                <tr>
                    <th width="53%" style="text-align: center;">Nama</th>
                    <th width="15%" style="text-align: center;">NIY/NIDN</th>
                    <th width="20%" style="text-align: center;">Pangkat/Gol.</th>
                    <th width="12%" style="text-align: center;">Jabatan</th>
                </tr>
                <tr>
                    <td><?= (isset($auditor->nama) ? $auditor->nama : "-") ?></td>
                    <td><?= (isset($auditor->nidn) ? $auditor->nidn : $auditor->niy) ?></td>
                    <td><?= (isset($auditor->jabfung) ? $auditor->jabfung : "-") ?></td>
                    <td style="text-align: center;">Auditor</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4">
            <div style="text-align: justify;text-justify: inter-word;">
                Sebagai auditor dalam kegiatan <?= (isset($ami->nama) ? $ami->nama : "-") ?> pada Satuan Kerja/Auditee sebagai berikut:
            </div>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table border="1" style="padding: 6px;">
                <tr>
                    <th width="8%" style="text-align: center;">No</th>
                    <th width="72%" style="text-align: center;">Satuan Kerja/Auditee</th>
                    <th width="20%" style="text-align: center;">Keterangan</th>
                </tr>
                <?php
                $i = 1;
                foreach ($unitAudit as $unit) :
                ?>
                    <tr>
                        <td style="text-align: center;"><?= $i ?></td>
                        <td><?= (isset($unit->ami->unit->nama) ? $unit->ami->unit->nama : "-") ?></td>
                        <td style="text-align: center;"><?= (isset($unit->status->nama) ? $unit->status->nama : "-") ?></td>
                    </tr>
                <?php
                    $i++;
                endforeach;
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4">Demikian, surat tugas ini diterbitkan untuk dipergunakan sebagaimana mestinya.</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table style="padding: 6px;">
                <?php
                $kurang = 6 - $i;
                for ($t = 0; $t < $kurang; $t++) :
                ?>
                    <tr>
                        <th width="8%" style="text-align: center;"></th>
                        <th width="72%" style="text-align: center;"></th>
                        <th width="20%" style="text-align: center;"></th>
                    </tr>
                <?php
                    $i++;
                endfor;
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table border="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="55%"></td>
                    <td width="45%">Gontor, <?= MyHelper::convertTanggalIndo($ami->tanggal_ami_mulai); ?> </td>
                </tr>
                <tr>
                    <td width="55%"></td>
                    <td width="45%">Ketua BPM,</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="55%"></td>
                    <td width="45%"><?= (isset($ami->ketua_bpm) ? $ami->ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td width="55%"></td>
                    <td width="45%">NIY. <?= (isset($ami->nidn_ketua_bpm) ? $ami->nidn_ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="55%">Tebusan:</td>
                    <td width="45%"></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">1. Rektor Universitas Darussalam Gontor.</td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">2. Pimpinan Satuan Kerja/Auditee di Universitas Darussalam Gontor</td>
                </tr>
            </table>
        </td>
    </tr>

</table>