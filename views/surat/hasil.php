<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Auditor;
use app\models\Kriteria;
use app\models\PembagianProdi;
use app\models\Temuan;
use app\models\Tilik;
use app\models\UnitKerja;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Surat Tugas Auditor';
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();



$ami_id     = !empty($_GET['ami_id']) ? $_GET['ami_id'] : '';
$auditor_id = !empty($_GET['auditor_id']) ? $_GET['auditor_id'] : '';



?>


<div class="panel col-lg-12">

    <div class="panel-body">
        <div class="x_content">

            <span class="badge bg-red mr-2" style="margin-right: .5rem">Maping Hasil AMI</span>
            <h3>Hasil AMI Universitas Darussalam Gontor</h3>
            <br>


            <div class="x_content">


                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['surat/hasil-ami'],
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]);

                ?>
                <div class="col-md-5 col-lg-5 col-xs-12">

                    <div class="form-group">
                        <label class="control-label ">Periode AMI</label>
                        <?= Select2::widget([
                            'id'        => 'ami',
                            'name'      => 'ami_id',
                            'data'      => ArrayHelper::map(Ami::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'nama'),
                            'value'     => $ami_id,
                            'options'   => ['placeholder' => Yii::t('app', '- Pilih kriteria -')],
                            'pluginOptions'     => [
                                'allowClear'    => true,
                                // 'multiple'      => true,
                            ],
                        ]); ?>
                    </div>


                    <div class="form-group clearfix">
                        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                            Cari</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>


                <div class="panel-body ">

                    <?php

                    if (!empty($_GET['ami_id'])) :
                    ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th style="text-align: center" class="alert alert-info" colspan="20"><strong>Hasil AMI - <?= $ami->nama ?></strong></th>
                                </tr>
                                <tr>

                                    <th width="8%" style="text-align: center"><strong>No</strong></th>
                                    <th width="22%" style="text-align: center"><strong>Nama Fakultas</strong></th>
                                    <th width="12%" style="text-align: center"><strong>Skor</strong></th>
                                    <th width="8%" style="text-align: center"><strong>No</strong></th>
                                    <th width="22%" style="text-align: center"><strong>Nama Prodi</strong></th>
                                    <th width="12%" style="text-align: center"><strong>Skor</strong></th>
                                    <th width="16%" style="text-align: center"><strong>Total Skor</strong></th>

                                </tr>
                                <?php
                                $i = 1;
                                $ii = 1;
                                foreach (UnitKerja::find()->where(['jenis' => 'fakultas'])->orderBy(['nama' => SORT_ASC])->all() as $fakultas) :
                                    $pembagianProdi = PembagianProdi::find()->where(['fakultas_id' => $fakultas->id]);
                                    $countprodi     = $pembagianProdi->count() + 1;

                                    $pembagian          = $pembagianProdi->one();
                                    $amiUnitFakultas    = AmiUnit::find()->where(['ami_id' => $ami_id, 'unit_id' => $pembagian->fakultas_id])->one();

                                    $kriterias = Kriteria::find()
                                        ->select('kriteria.id')
                                        ->joinWith([
                                            'pembagianKriterias as pk'
                                        ])
                                        ->where([
                                            'pk.jenis_unit' => MyHelper::getJenisUnitCode($amiUnitFakultas->unit->jenis)
                                        ])
                                        ->all();

                                    foreach ($kriterias as $kriteria) {
                                        $kriteria_fakultas['id'][] = $kriteria->id;
                                    }

                                    $paketPenilaianFakultas = MyHelper::getPaketPenilaian($amiUnitFakultas->id, $kriteria_fakultas['id'], $amiUnitFakultas->unit->jenjangMap->jenjang_id);
                                ?>

                                    <tr>
                                        <td rowspan="<?= $countprodi ?>" style="text-align: center;padding-top: 30px;"><?= $i ?></td>
                                        <td rowspan="<?= $countprodi ?>" style="padding-top: 30px;">
                                            <?= $fakultas->nama ?>
                                        </td>
                                        <td rowspan="<?= $countprodi ?>" style="text-align: center;padding-top: 30px;font-weight: bold;">
                                            <?= $paketPenilaianFakultas['totalAll'] ?>
                                        </td>
                                    </tr>

                                    <?php
                                    $prodi = $pembagianProdi->all();
                                    foreach ($prodi as $data) :

                                        $amiUnitProdi           = AmiUnit::find()->where(['ami_id' => $ami_id, 'unit_id' => $data->prodi->id])->one();

                                        $kriterias = Kriteria::find()
                                            ->select('kriteria.id')
                                            ->joinWith([
                                                'pembagianKriterias as pk'
                                            ])
                                            ->where([
                                                'pk.jenis_unit' => MyHelper::getJenisUnitCode($amiUnitProdi->unit->jenis)
                                            ])
                                            ->all();

                                        foreach ($kriterias as $kriteria) {
                                            $kriteria_prodi['id'][] = $kriteria->id;
                                        }

                                        $paketPenilaianProdi    = MyHelper::getPaketPenilaian($amiUnitProdi->id, $kriteria_prodi['id'], $amiUnitProdi->unit->jenjangMap->jenjang_id);

                                    ?>

                                        <tr>

                                            <td style="text-align: center;"><?= $ii ?></td>
                                            <td><?= $data->prodi->nama ?></td>
                                            <td style="text-align: center;font-weight: bold;">
                                                <?= $paketPenilaianProdi['totalAll'] ?>
                                            </td>
                                            <td style="text-align: center;font-weight: bolder;color: green;">
                                                <?= $paketPenilaianFakultas['totalAll'] + $paketPenilaianProdi['totalAll'] ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $ii++;
                                    endforeach;
                                    ?>

                                <?php
                                    $i++;
                                endforeach;
                                ?>


                            </table>
                        </div>
                    <?php

                    endif;
                    ?>


                </div>

            </div>

        </div>
    </div>


</div>