<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Auditor;
use app\models\Kriteria;
use app\models\Temuan;
use app\models\Tilik;
use app\models\UnitKerja;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Surat Tugas Auditor';
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();



$ami_id     = !empty($_GET['ami_id']) ? $_GET['ami_id'] : '';
$auditor_id = !empty($_GET['auditor_id']) ? $_GET['auditor_id'] : '';



?>


<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title"><?= $this->title; ?> </h3>
        <div class="custom-tabs-line tabs-line-bottom left-aligned">
            <?php

            echo \yii\widgets\Menu::widget([
                'options' => [
                    'class' => 'nav nav-tabs',
                    'role' => 'tablist'
                ],
                'items' => [
                    [
                        'label'     => 'Surat Tugas', 'url' => ['surat/index'],
                        'options'   => ['class' => 'active'],
                        'visible'   => Yii::$app->user->can('admin')
                    ],
                    [
                        'label' => 'Asesmen', 'url' => ['asesmen/proses'],
                    ],
                ],
            ]);
            ?>
        </div>

    </div>




    <div class="panel-body">
        <div class="x_content">

            <span class="badge bg-red mr-2" style="margin-right: .5rem">Surat Tugas</span>
            <h3>Surat Tugas Auditor</h3>
            <h4>Audit Mutu Internal</h4>
            <br>


            <div class="x_content">


                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['surat/index'],
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]);

                ?>
                <div class="col-md-5 col-lg-5 col-xs-12">

                    <div class="form-group">
                        <label class="control-label ">Periode AMI</label>
                        <?= Select2::widget([
                            'id'        => 'ami',
                            'name'      => 'ami_id',
                            'data'      => ArrayHelper::map(Ami::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'nama'),
                            'value'     => $ami_id,
                            'options'   => ['placeholder' => Yii::t('app', '- Pilih kriteria -')],
                            'pluginOptions'     => [
                                'allowClear'    => true,
                                'multiple'      => true,
                            ],
                        ]); ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label ">Auditor</label>
                        <?= Select2::widget([
                            'id'        => 'auditor',
                            'name'      => 'auditor_id',
                            'data'      => ArrayHelper::map(Auditor::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'nama'),
                            'value'     => $auditor_id,
                            'options'   => ['placeholder' => Yii::t('app', '- Pilih auditor -')],
                            'pluginOptions'     => [
                                'allowClear'    => true,
                                'multiple'      => true,
                            ],
                        ]); ?>
                    </div>


                    <div class="form-group clearfix">
                        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                            Cari</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>


                <div class="panel-body ">

                    <?php

                    if (!empty($list_auditor)) :
                    ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th style="text-align: center" class="alert alert-info" colspan="20"><strong>Master Input AMI - </strong></th>
                                </tr>
                                <tr>

                                    <th width="10%" style="text-align: center"><strong>No</strong></th>
                                    <th width="40%" style="text-align: center"><strong>Nama Auditor</strong></th>
                                    <th width="40%" style="text-align: center"><strong>Nama AMI</strong></th>
                                    <th width="10%" style="text-align: center"><strong>Cetak</strong></th>

                                </tr>

                                <?php

                                $i = 1;
                                foreach ($list_auditor as $k => $v) :

                                    $auditor    = Auditor::findOne($v['auditor_id']);
                                    $ami        = Ami::findOne($v['ami_id']);

                                ?>
                                    <tr>
                                        <td style="text-align: center"><?= $i ?></td>
                                        <td><?= $auditor->nama ?></td>
                                        <td><?= $ami->nama ?></td>
                                        <td style="text-align: center">
                                            <?=
                                            Html::a(
                                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                                [
                                                    'tugas-auditor',
                                                    'auditor_id'    => $v['auditor_id'],
                                                    'ami_id'        => $v['ami_id']
                                                ],
                                                [
                                                    'class'     => 'label label-danger',
                                                    'style'     => 'font-size:100%',
                                                    'target'    => '_blank'
                                                ]
                                            );
                                            ?>
                                        </td>

                                    </tr>
                                <?php
                                    $i++;
                                endforeach;
                                ?>

                            </table>
                        </div>
                    <?php

                    endif;
                    ?>


                </div>

            </div>

        </div>
    </div>


</div>