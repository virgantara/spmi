<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PenanggungJawabBahanRtm */

$this->title = 'Create Penanggung Jawab Bahan Rtm';
$this->params['breadcrumbs'][] = ['label' => 'Penanggung Jawab Bahan Rtms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    	   </div>
        </div>
    </div>
</div>