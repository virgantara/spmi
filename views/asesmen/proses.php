<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asesmen';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['ami']];
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>

<style>
    .projects {
        border-collapse: collapse;
        width: 100%;
    }

    .projects td,
    .projects th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        vertical-align: middle;
        /* Center the content vertically */
    }
</style>
<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body col-md-4">
        <div class="x_content">

            <h3>Persentasi Proses Input</h3>


            <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['asesmen/ami'], ['class' => 'btn btn-sm btn-info']) ?>
            <br>
            <table width="100%">
                <tr>
                    <td width="35%">
                        <label class="control-label ">
                            Laporan Evaluasi Diri :
                        </label>
                    </td>
                    <td width="15%" style="text-align: right;">
                        <label class="control-label ">
                            <?= $persentaseEvaluasi ?> %
                        </label>
                    </td>
                    <td width="15%" style="text-align: center;">
                        <label class="control-label ">
                            <?= $dataEvaluasi ?>
                            /
                            <?= $instrumenEvaluasi ?>
                        </label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="progress progress_md">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?= $instrumenEvaluasi ?>" style="width: <?= $persentaseEvaluasi ?>%;" aria-valuenow="<?= $dataEvaluasi - 1 ?>"></div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label class="control-label ">
                            Proses Asesmen :
                        </label>
                    </td>
                    <td style="text-align: right;">
                        <label class="control-label ">
                            <?= $persentaseKecukupan ?> %
                        </label>
                    </td>
                    <td style="text-align: center;">
                        <label class="control-label ">
                            <?= $dataKecukupan ?>
                            /
                            <?= $instrumenKecukupan ?>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="progress progress_md">
                            <div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="<?= $instrumenKecukupan ?>" style="width: <?= $persentaseKecukupan ?>%;" aria-valuenow="<?= $dataKecukupan - 1 ?>"></div>
                        </div>
                    </td>
                </tr>


            </table>


            <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'form_validation',
                ]
            ]); ?>

            <label class="control-label "> Mulai Asesmen</label>

            <div class="input-group">
                <select class="form-control" name="kriteria_id" id="kriteria_id">
                    <option value="" disabled selected>Pilih opsi</option>
                    <?php if (Yii::$app->user->can('auditee')) : ?>
                        <option value="led">Laporan Evaluasi Diri</option>
                    <?php endif;
                    if (Yii::$app->user->can('auditor')) : ?>
                        <option value="ak">Asesmen Kecukupan</option>
                        <option value="al">Asesmen Lapangan</option>
                    <?php endif; ?>
                </select>
                <span class="input-group-btn">
                    <?= Html::submitButton('Mulai!', ['class' => 'btn btn-primary waves-effect', 'disabled' => 'true', 'id' => 'btn-mulai']) ?>
                </span>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>

    <div class="panel-body col-md-4">
        <div class="x_content">

            <h3>Dokumen Audit Mutu Internal</h3>

            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th style="width: 30%">Nama Dokumen</th>
                        <th style="text-align: center;">Cetak</th>
                        <th style="width: 30%">Nama Dokumen</th>
                        <th style="text-align: center;">Cetak</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="vertical-align: middle;"> Surat Tugas </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'surat/tugas-amiunit',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class' => 'btn btn-danger btn-sm',
                                    'target' => "_blank"
                                ]
                            ) ?>
                        </td>
                        <td style="vertical-align: middle;"> Rencana AMI </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'surat/rencana',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class' => 'btn btn-danger btn-sm',
                                    'target' => "_blank"
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;"> Template Kehadiran </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'surat/kehadiran-ami',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class'     => 'btn btn-danger btn-sm',
                                    'target'    => "_blank"
                                ]
                            ) ?>
                        </td>
                        <td style="vertical-align: middle;"> Cetak Tilik </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                (Yii::$app->user->identity->access_role != 'auditee' ? [
                                    'tilik/cetak',
                                    'ami_unit_id' => $amiUnit->id
                                ] : 'javascript:void(0)'),
                                [
                                    'class' => 'btn btn-danger btn-sm',

                                    'target' => (Yii::$app->user->identity->access_role != 'auditee' ? '_blank' : ''),
                                    'disabled' => (Yii::$app->user->identity->access_role == 'auditee'),
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;"> Cetak Temuan </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'temuan/cetak-temuan',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class' => 'btn btn-danger btn-sm',
                                    'target' => "_blank"
                                ]
                            ) ?>
                        </td>
                        <td style="vertical-align: middle;"> Berita Acara </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'surat/cetak-berita',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class' => 'btn btn-danger btn-sm',
                                    'target' => "_blank"
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <!-- <td> Hasil AMI </td>
                        <td style="text-align: center;"> -->
                        <?php Html::a(
                            '<i class="fa fa-file-pdf-o"></i> Cetak',
                            [
                                'surat/cetak-hasil-ami',
                                'ami_unit_id' => $amiUnit->id
                            ],
                            [
                                'class' => 'btn btn-danger btn-sm',
                                'target' => "_blank"
                            ]
                        ) ?>
                        <!-- </td> -->
                        <td style="vertical-align: middle;"> Cetak Log Temuan </td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'temuan/cetak-log-temuan',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class' => 'btn btn-danger btn-sm',
                                    'target' => "_blank"
                                ]
                            ) ?>
                        </td>
                        <td style="vertical-align: middle;"><strong> Laporan AMI </strong></td>
                        <td style="text-align: center;">
                            <?= Html::a(
                                '<i class="fa fa-file-pdf-o"></i> Cetak',
                                [
                                    'asesmen/cetak-laporan-ami-per-auditee',
                                    'ami_unit_id' => $amiUnit->id
                                ],
                                [
                                    'class'     => 'btn btn-danger btn-sm',
                                    'target'    => "_blank"
                                ]
                            ) ?>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("change", "#kriteria_id", function(e) {
        e.preventDefault();

        $("#btn-mulai").prop("disabled", false)

    });
</script>
<?php JSRegister::end(); ?>