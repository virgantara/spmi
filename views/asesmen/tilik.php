<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Daftar Tilik';
?>

<?= Html::a('<i class="fa fa-print"></i> Cetak Tilik', ['asesmen/cetak-tilik', 'ami_unit_id' => $id], ['class' => 'btn btn-warning']) ?>


<div class="panel-body ">

    <?php // Html::a('<i class="fa fa-mail-reply"></i> Back', ['lkps/ami'], ['class' => 'btn btn-sm btn-info']) 
    ?>

    <?php
    
    $gridColumns = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'contentOptions' => ['class' => 'kartik-sheet-style'],
            'width' => '36px',
            'pageSummary' => 'Total',
            'pageSummaryOptions' => ['colspan' => 6],
            'header' => '',
            'headerOptions' => ['class' => 'kartik-sheet-style']
        ],
        // 'id',
        // 'asesmen_id',
        [
            'attribute' => 'asesmen_id',
            
            // 'filter' => ArrayHelper::map(\app\models\Indikator::find()->all(), 'id', 'nama'),
            'value' => function ($data) {
                return $data->asesmen->indikator->nama;
            },

            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'asesmen_id',
                'data' => ArrayHelper::map(\app\models\Indikator::find()->all(), 'id', 'nama'),
                'options' => ['placeholder' => 'Select asesmen...']
            ]),
        ],
        'pertanyaan',
        // 'status',
        [
            'attribute' => 'auditor_id',
            'value' => function ($data) {
                return $data->auditor->nama;
            }
        ],
        'catatan',
        [
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a('<i class="fa fa-trash"></i>', ['tilik/delete', 'id'=>$data->id], [
                    // 'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]);
            }
        ]
        //'created_at',
        //'updated_at',
        // ['class' => 'yii\grid\ActionColumn']
    ]; ?>
    <?= GridView::widget([
        'pager' => [
            'options' => ['class' => 'pagination'],
            'activePageCssClass' => 'active paginate_button page-item',
            'disabledPageCssClass' => 'disabled paginate_button',
            'prevPageLabel' => 'Previous',
            'nextPageLabel' => 'Next',
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'nextPageCssClass' => 'paginate_button next page-item',
            'prevPageCssClass' => 'paginate_button previous page-item',
            'firstPageCssClass' => 'first paginate_button page-item',
            'lastPageCssClass' => 'last paginate_button page-item',
            'maxButtonCount' => 10,
            'linkOptions' => [
                'class' => 'page-link'
            ]
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style' => 'overflow: auto'],
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                ],
                'options' => ['class' => 'skip-export']
            ]
        ],
        'exportConfig' => [
            GridView::PDF => ['label' => 'Save as PDF'],
            GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
            GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
            GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
        ],

        'toolbar' =>  [
            '{export}',

            '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => 'pjax-container',
            ]
        ],
        'id' => 'my-grid',
        'bordered' => true,
        'striped' => true,
        // 'condensed' => false,
        // 'responsive' => false,
        'hover' => true,
        // 'floatHeader' => true,
        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>

</div>