<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Kriteria;
use app\models\Temuan;
use app\models\Tilik;
use app\models\UnitKerja;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Master';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['asesmen/ami']];
$this->params['breadcrumbs'][] = $this->title;

$list_ami = MyHelper::getStatusAmi();

$ami_unit = AmiUnit::findOne($id);
$ami = Ami::findOne($ami_unit->ami_id);
$unit = UnitKerja::findOne($ami_unit->unit_id);
// echo '<pre>';print_r($ami_unit);exit;

$asesmen_nama = "$ami->nama - $unit->nama";
// echo '<pre>';print_r($model->id);die;
$kriteria_id = !empty($_GET['kriteria_id']) ? $_GET['kriteria_id'] : '';
$kki = !empty($_GET['kriteria_khusus_id']) ? $_GET['kriteria_khusus_id'] : '';



?>


<div class="panel col-lg-12">

    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>




    <div class="panel-body">
        <div class="x_content">

            <span class="badge bg-red mr-2" style="margin-right: .5rem">Master Input</span>
            <h3>Audit Mutu Internal - <?= $amiUnit->unit->nama ?> </h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <br>


            <div class="x_content">


                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['asesmen/master-input', 'id' => $id],
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]);

                ?>
                <div class="col-md-5 col-lg-5 col-xs-12">

                    <div class="form-group">
                        <label class="control-label ">Master kriteria</label>
                        <?= Select2::widget([
                            'id'        => 'kriteria_umum',
                            'name'      => 'kriteria_id',
                            'data'      => ArrayHelper::map(
                                Kriteria::find()->joinWith([
                                    'pembagianKriterias as pk'
                                ])
                                    ->where([
                                        'pk.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)
                                    ])
                                    ->asArray()
                                    ->all(),
                                'id',
                                'nama'
                            ),
                            'value'     => $kriteria_id,
                            'options'   => ['placeholder' => Yii::t('app', '- Pilih kriteria -')],
                            'pluginOptions'     => [
                                'allowClear'    => true,
                                'multiple'      => true,
                            ],
                        ]); ?>
                    </div>


                    <div class="form-group clearfix">
                        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                            Cari</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>


                <div class="panel-body ">

                    <?php
                    if (!empty($list_indikator)) :
                    ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th style="text-align: center" class="alert alert-info" colspan="20"><strong>Master Input AMI - <?= $unit->nama ?></strong></th>
                                </tr>
                                <tr>

                                    <th width="3%" style="text-align: center"><strong>No</strong></th>
                                    <th width="10%" style="text-align: center"><strong>Kriteria</strong></th>
                                    <th width="60%" style="text-align: center"><strong>Nama Instrumen</strong></th>
                                    <th width="3%" style="text-align: center"><strong>LED</strong></th>
                                    <th width="9%" colspan="3" style="text-align: center"><strong>AK</strong></th>
                                    <th width="9%" colspan="3" style="text-align: center"><strong>AL</strong></th>
                                    <th width="6%" style="text-align: center"><strong>Link Bukti</strong></th>
                                    <th width="6%" style="text-align: center"><strong>Audit</strong></th>

                                </tr>

                                <?php

                                $i = 1;

                                foreach ($list_indikator as $key => $indikator) :

                                    $kriteria = Kriteria::findOne($indikator->kriteria_id);
                                    $asesmen  = Asesmen::find()->where([
                                        'indikator_id'  => $indikator,
                                        'ami_unit_id'   => $amiUnit->id,
                                    ])->one();
                                    $status_link    = 'btn btn-sm btn-info';
                                    $class_link     = 'btn btn-sm btn-info';
                                    $class          = '<i class="fa fa-link"></i> View Document';

                                ?>

                                    <tr>
                                        <td style="text-align: center"><?= $i; ?></td>
                                        <td style="text-align: center"><?= $kriteria->nama ?></td>
                                        <td><?= $indikator->nama ?></td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_ed) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_ak1) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_ak2) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_ak3) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_al1) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_al2) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= MyHelper::convertSkor($asesmen->skor_al3) ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= (isset($asesmen->link_bukti) ?
                                                Html::a('<i class="fa fa-link"></i>', (isset($asesmen->link_bukti) ? $asesmen->link_bukti : ''), ['target' => '_blank', 'class' => "btn btn-sm btn-primary btn-xs"])
                                                : "-") ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= Html::a(
                                                '<i class="fa fa-upload"></i>',
                                                [''],
                                                [
                                                    'class'         => 'btn btn-sm btn-success btn-xs btn-audit',
                                                    'data-id'       => $indikator->id,
                                                    'data-nama'     => $indikator->nama,
                                                    'data-amiunit'  => $amiUnit->id,
                                                    'data-skor1'    => $indikator->skor1,
                                                    'data-skor2'    => $indikator->skor2,
                                                    'data-skor3'    => $indikator->skor3,
                                                    'data-skor4'    => $indikator->skor4,
                                                ]
                                            ); ?>
                                        </td>
                                    </tr>
                                <?php
                                    $i++;
                                endforeach;
                                ?>

                            </table>
                        </div>
                    <?php

                    endif;
                    ?>


                </div>

            </div>

        </div>
    </div>


</div>





<!-- Modal Master Input -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => [
        'id'        => 'modalHeader',
    ],
    'header'        => '<h3>Master Input</h3>',
    'id'            => 'modal-master',
    'size'          => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'id'        => 'form-master',
        'enctype'   => "multipart/form-data"
    ]
]); ?>
<div class="form-group">
    <label for="">Nama Instrumen</label>
    <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_id']) ?>
    <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'ami_unit_id']) ?>
    <?= Html::textArea('indikator_nama', '', ['class' => 'form-control', 'id' => 'indikator_nama', 'disabled' => true]) ?>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 1</label>
            <?= Html::textArea('skor1', '', ['class' => 'form-control', 'id' => 'skor1', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 2</label>
            <?= Html::textArea('skor2', '', ['class' => 'form-control', 'id' => 'skor2', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 3</label>
            <?= Html::textArea('skor3', '', ['class' => 'form-control', 'id' => 'skor3', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 4</label>
            <?= Html::textArea('skor4', '', ['class' => 'form-control', 'id' => 'skor4', 'disabled' => true]) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="">Evaluasi Diri</label>
    <?= Html::textInput('skor_ed', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ed', 'placeholder' => 'Auditee']) ?>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Skor Asesmen Kecukupan</label>
            <?= Html::textInput('skor_ak1', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak1', 'placeholder' => 'Auditor 1']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Skor Asesmen Lapangan</label>
            <?= Html::textInput('skor_al1', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al1', 'placeholder' => 'Auditor 1']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_ak2', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak2', 'placeholder' => 'Auditor 2']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_al2', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al2', 'placeholder' => 'Auditor 2']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_ak3', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak3', 'placeholder' => 'Auditor 3']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_al3', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al3', 'placeholder' => 'Auditor 3']) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="">Link Bukti</label>
    <?= Html::textInput('link_bukti', '', ['class' => 'form-control', 'id' => 'link_bukti', 'placeholder' => 'Masukkan link bukti']) ?>
</div>


<div class="form-group">
    <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
</div>
<?php ActiveForm::end() ?>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var data = $("#form-master").serialize()

        console.log(data)

        $.ajax({
            url: "/asesmen/ajax-input-master",
            type: "POST",
            async: true,
            data: data,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-master").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    // Modal ED

    $(document).on("click", ".btn-audit", function(e) {
        e.preventDefault();
        var instrumen_id = $(this).data("id")
        var instrumen_nama = $(this).data("nama")
        var ami_unit_id = $(this).data("amiunit")
        var skor1 = $(this).data("skor1")
        var skor2 = $(this).data("skor2")
        var skor3 = $(this).data("skor3")
        var skor4 = $(this).data("skor4")

        $("#indikator_id").val(instrumen_id)
        $("#indikator_nama").val(instrumen_nama)
        $("#ami_unit_id").val(ami_unit_id)
        $("#skor1").val(skor1)
        $("#skor2").val(skor2)
        $("#skor3").val(skor3)
        $("#skor4").val(skor4)

        $("#modal-master").modal("show")

    });
</script>
<?php JSRegister::end(); ?>