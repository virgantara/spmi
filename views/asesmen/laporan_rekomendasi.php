<?php

use app\helpers\MyHelper;

?>

<table border="1" width="100%" style="padding: 4px;">

    <tr>
        <td width="17%" rowspan="2"></td>
        <td width="34%" style="text-align: center;">
            <span style="font-size: 1.1em"><strong>FORMULIR</strong></span><br>

        </td>
        <td width="49%" rowspan="2">
            <table width="100%" style="font-size: 1.1em">
                <tr>
                    <td width="30%">No. Dok</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->dokumen_berita_acara) ? $amiUnit->dokumen_berita_acara : 'Belum dibuat') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Berlaku</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_penilaian_selesai) ? $amiUnit->ami->tanggal_penilaian_selesai : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">No Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->no_revisi) ? $amiUnit->ami->no_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Tgl Revisi</td>
                    <td width="5%">:</td>
                    <td width="60%"><?= (isset($amiUnit->ami->tanggal_revisi) ? $amiUnit->ami->tanggal_revisi : '-') ?></td>
                </tr>
                <tr>
                    <td width="30%">Lembar</td>
                    <td width="5%">:</td>
                    <td width="60%">1</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <span style="font-size: 1.1em"><strong>REKOMENDASI MUTU</strong></span>
        </td>
    </tr>
</table>

<br><br>

<table width="100%">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td width="10%"></td>
        <td width="80%" style="font-size: 1.35em;text-align: center;">REKOMENDASI MUTU</td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td></td>
        <td style="font-size: 1.35em;text-align: center;">AUDIT MUTU INTERNAL</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td style="font-size: 1.35em;text-align: center;">UNIVERSITAS DARUSSALAM GONTOR</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td style="font-size: 1.35em;text-align: center;"><?= $amiUnit->ami->tahun ?></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <table border="1" width="100%" style="padding: 4px;font-size: 1.2em">
                <tr>
                    <td width="30%">Auditee</td>
                    <td width="70%">: <?= $amiUnit->unit->nama ?></td>
                </tr>
                <?php
                for ($i = 1; $i < $jumlahAuditor; $i++) :
                ?>
                    <tr>
                        <td>Auditor <?= $i ?></td>
                        <td>: <?= MyHelper::getAuditor($amiUnit->id, $i)['nama']  ?? '' ?></td>
                    </tr>
                <?php
                endfor;
                ?>
                <tr>
                    <td>Tanggal Penilaian</td>
                    <td>: <?= $amiUnit->ami->tanggal_penilaian_selesai ?></td>
                </tr>

            </table>
        </td>
        <td></td>
    </tr>
</table>


<br><br><br>

<table border="1" width="100%" style="padding: 4px;font-size: 1.2em">
    <tr>
        <td colspan="4" style="text-align: center;">
            Disetujui Oleh:
        </td>
    </tr>
    <tr style="text-align: center;">
        <td width="25%">Auditee</td>
        <td width="25%">Auditor 1</td>
        <td width="25%">Auditor 2</td>
        <td width="25%">Auditor 3</td>
    </tr>
    <tr>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
        <td><br><br><br><br><br></td>
    </tr>
    <tr style="font-size: 0.65em;text-align: center;">
        <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
        <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
    </tr>

</table>

<table width="100%" style="padding: 4px;font-size: 1.2em">
    <tr>
        <td>
            <div style="text-align: justify;text-justify: inter-word;font-size: 1em;line-height: 1.8;">
                Berdasarkan hasil asesmen lapangan, penilaian untuk setiap indikator, kami selaku tim auditor
                memberikan rekomendasi mutu sebagai pengendalian, perbaikan, dan peningkatan mutu sebagai
                berikut:
            </div>
        </td>
    </tr>
</table>