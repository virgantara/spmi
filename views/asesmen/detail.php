<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Ami;
use app\models\Jenjang;
use app\models\ProdiJenjang;
use app\models\UnitKerja;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['ami']];
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>
<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body col-md-6">
        <div class="x_content">

            <h3><?= $amiUnit->unit->nama ?> </h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <br>

            <div class="x_content ">

                <span class="btn btn-success btn-xs"><i class="fa fa-folder"></i> Laporan Evaluasi Diri </span>
                <span class="btn btn-warning btn-xs"><i class="fa fa-folder"></i> Asesmen Kecukupan </span>
                <span class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Asesmen Lapangan </span>

                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 45%">Nama Petugas</th>
                            <th>Progress</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <a><?= MyHelper::getAuditee($amiUnit->unit_id) ?></a>
                                <br>
                                <small><b>Auditee</b></small>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataEvaluasi, $instrumenEvaluasi) ?>" style="width: <?= MyHelper::convertPersen($dataEvaluasi, $instrumenEvaluasi) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataEvaluasi, $instrumenEvaluasi) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataEvaluasi, $instrumenEvaluasi) ?>% Complete -
                                    <?= $dataEvaluasi ?> / <?= $instrumenEvaluasi ?></small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a><?= (isset(MyHelper::getAuditor($amiUnit->id, '1')['nama']) ? MyHelper::getAuditor($amiUnit->id, '1')['nama'] : "-") ?></a>
                                <br>
                                <small><b>Ketua Auditor</b></small>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataKecukupanAuditor1, $tugasKecukupanAuditor1) ?>" style="width: <?= MyHelper::convertPersen($dataKecukupanAuditor1, $tugasKecukupanAuditor1) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataKecukupanAuditor1, $tugasKecukupanAuditor1) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataKecukupanAuditor1, $tugasKecukupanAuditor1) ?>%
                                    Complete -
                                    <?= $dataKecukupanAuditor1 ?> / <?= $tugasKecukupanAuditor1 ?></small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataLapanganAuditor1, $tugasLapanganAuditor1) ?>" style="width: <?= MyHelper::convertPersen($dataLapanganAuditor1, $tugasLapanganAuditor1) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataLapanganAuditor1, $tugasLapanganAuditor1) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataLapanganAuditor1, $tugasLapanganAuditor1) ?>%
                                    Complete -
                                    <?= $dataLapanganAuditor1 ?> / <?= $tugasLapanganAuditor1 ?></small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a><?= (isset(MyHelper::getAuditor($amiUnit->id, '2')['nama']) ? MyHelper::getAuditor($amiUnit->id, '2')['nama'] : "-") ?></a>
                                <br>
                                <small><b>Anggota Auditor</b></small>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataKecukupanAuditor2, $tugasKecukupanAuditor2) ?>" style="width: <?= MyHelper::convertPersen($dataKecukupanAuditor2, $tugasKecukupanAuditor2) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataKecukupanAuditor2, $tugasKecukupanAuditor2) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataKecukupanAuditor2, $tugasKecukupanAuditor2) ?>%
                                    Complete -
                                    <?= $dataKecukupanAuditor2 ?> / <?= $tugasKecukupanAuditor2 ?></small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataLapanganAuditor2, $tugasLapanganAuditor2) ?>" style="width: <?= MyHelper::convertPersen($dataLapanganAuditor2, $tugasLapanganAuditor2) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataLapanganAuditor2, $tugasLapanganAuditor2) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataLapanganAuditor2, $tugasLapanganAuditor2) ?>%
                                    Complete -
                                    <?= $dataLapanganAuditor2 ?> / <?= $tugasLapanganAuditor2 ?></small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a><?= (isset(MyHelper::getAuditor($amiUnit->id, '3')['nama']) ? MyHelper::getAuditor($amiUnit->id, '3')['nama'] : "-") ?></a>
                                <br>
                                <small><b>Anggota Auditor</b></small>
                            </td>
                            <td class="project_progress">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-orange" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataKecukupanAuditor3, $tugasKecukupanAuditor3) ?>" style="width: <?= MyHelper::convertPersen($dataKecukupanAuditor3, $tugasKecukupanAuditor3) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataKecukupanAuditor3, $tugasKecukupanAuditor3) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataKecukupanAuditor3, $tugasKecukupanAuditor3) ?>%
                                    Complete -
                                    <?= $dataKecukupanAuditor3 ?> / <?= $tugasKecukupanAuditor3 ?></small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="<?= MyHelper::convertPersen($dataLapanganAuditor3, $tugasLapanganAuditor3) ?>" style="width: <?= MyHelper::convertPersen($dataLapanganAuditor3, $tugasLapanganAuditor3) ?>%;" aria-valuenow="<?= MyHelper::convertPersen($dataLapanganAuditor3, $tugasLapanganAuditor3) ?>">
                                    </div>
                                </div>
                                <small><?= MyHelper::convertPersen($dataLapanganAuditor3, $tugasLapanganAuditor3) ?>%
                                    Complete -
                                    <?= $dataLapanganAuditor3 ?> / <?= $tugasLapanganAuditor3 ?></small>
                            </td>
                        </tr>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>