<?php

use app\models\Indikator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Asesmen */
/* @var $form yii\widgets\ActiveForm */
// echo '<pre>';print_r($id);exit;
$indikator = Indikator::find()->where([
    'id' => $id_indikator,
])->one();
// echo '<pre>';print_r($indikator);exit;
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Nama Indikator</label>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->textInput(['value' => $indikator->nama, 'disabled' => true])->label(false) ?>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->hiddenInput(['value' => $id_indikator])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Skor 1</label>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->textInput(['value' => $indikator->skor1, 'disabled' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Skor 2</label>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->textInput(['value' => $indikator->skor2, 'disabled' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Skor 3</label>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->textInput(['value' => $indikator->skor3, 'disabled' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Skor 4</label>
        <?= $form->field($model, 'id_indikator', ['options' => ['tag' => false]])->textInput(['value' => $indikator->skor4, 'disabled' => true])->label(false) ?>
    </div>

    <?php
    if ($status == 'led') {
    ?>
        <div class="form-group">
            <label class="control-label">Link bukti</label>
            <?= $form->field($model, 'link_bukti', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
        </div>

        <div class="form-group">
            <label class="control-label">Evaluasi Diri</label>
            <?= $form->field($model, 'skor_ed', ['options' => ['tag' => false]])->textInput()->label(false) ?>
        </div>

    <?php
    } elseif ($status == 'ak') {
    ?>

        <div class="form-group">
            <label class="control-label">Evaluasi Diri</label>
            <?= $form->field($model, 'skor_ed', ['options' => ['tag' => false]])->textInput(['value' => $model->skor_ed, 'disabled' => true])->label(false) ?>
        </div>

        <div class="form-group">
            <label class="control-label">Asesmen Kecukupan</label>
            <?= $form->field($model, 'skor_ak', ['options' => ['tag' => false]])->textInput()->label(false) ?>
        </div>

        <div class="form-group">
            <label class="control-label">Tanggapan Auditor</label>
            <?= $form->field($model, 't_auditor', ['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>
        </div>

    <?php
    } elseif ($status == 'al') {
    ?>
        <div class="form-group">
            <label class="control-label">Skor Asesmen Lapangan</label>
            <?= $form->field($model, 'skor_al', ['options' => ['tag' => false]])->textInput()->label(false) ?>


        </div>
    <?php
    }
    ?>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>