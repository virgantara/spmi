<?php

use app\models\Asesmen;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Indikator;

/* @var $this yii\web\View */
/* @var $model app\models\Asesmen */

$asesmen = Asesmen::find()->where(['id' => $model->id])->one();
$indikator = Indikator::find()->where(['id' => $asesmen->id_indikator])->one();

$this->title = $indikator->nama;
$this->params['breadcrumbs'][] = ['label' => 'Asesmens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id, 'id_indikator' => $model->id_indikator], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Back', ['asesmen/evaluasi-diri'], ['class' => 'btn btn-info']) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        // 'id_indikator',
                        [
                            'attribute' => 'id_indikator',
                            'label' => 'Nama Indikator',
                            'value' => function ($data) {
                                $indikator = Indikator::find()->where(['id' => $data->id_indikator])->one();
                                return $indikator->nama;
                            }
                        ],
                        [
                            'attribute' => 'id_indikator',
                            'label' => 'Skor 1',
                            'value' => function ($data) {
                                $indikator = Indikator::find()->where(['id' => $data->id_indikator])->one();
                                return $indikator->skor1;
                            }
                        ],
                        [
                            'attribute' => 'id_indikator',
                            'label' => 'Skor 2',
                            'value' => function ($data) {
                                $indikator = Indikator::find()->where(['id' => $data->id_indikator])->one();
                                return $indikator->skor2;
                            }
                        ],
                        [
                            'attribute' => 'id_indikator',
                            'label' => 'Skor 3',
                            'value' => function ($data) {
                                $indikator = Indikator::find()->where(['id' => $data->id_indikator])->one();
                                return $indikator->skor3;
                            }
                        ],
                        [
                            'attribute' => 'id_indikator',
                            'label' => 'Skor 4',
                            'value' => function ($data) {
                                $indikator = Indikator::find()->where(['id' => $data->id_indikator])->one();
                                return $indikator->skor4;
                            }
                        ],
                        // 'link_bukti',
                        [
                            'label' => 'Bukti',
                            'format' => 'raw',
                            'value' => function ($data) {
                                if (!isset($data->link_bukti)) {
                                    return html::a('<i class="fa fa-close" style="color: red;"> Tidak ada Bukti</i>');
                                } else {
                                    return html::a('<i class="fa fa-download"> Link Bukti</i>', (!empty($data->link_bukti) ? $data->link_bukti : null), ['target' => '_blank']);
                                }
                            },
                        ],
                        'skor_ed',
                        'skor_ak',
                        'skor_al',
                        't_auditor:ntext',
                        // 'created_at',
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>