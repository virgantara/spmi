<?php

use app\helpers\MyHelper;
use app\models\JenjangMap;

?>

<table border="0" width="100%" style="padding: 4px;">

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">
            <table border="0" width="100%" style="padding: 4px;font-size: 1.2em;">
                <tr>
                    <td width="100%" colspan="3">B. Peluang Perbaikan</td>
                </tr>
                <tr>
                    <td width="3%"></td>
                    <td width="90%">
                        <div style="text-align: justify;text-justify: inter-word;font-size: 1em;line-height: 1.8;">
                            Berdasarkan hasil asesmen lapangan, penilaian untuk setiap indikator, kami selaku tim auditor
                            memberikan rekomendasi mutu sebagai pengendalian, perbaikan, dan peningkatan mutu sebagai
                            berikut:
                        </div>
                    </td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;font-size: 1.2em;text-align: center;">
                <tr>
                    <td width="6%">No. </td>
                    <td width="25%">Indikator </td>
                    <td width="30%">Uraian Ketidaksesuaian </td>
                    <td width="14%">Kategori </td>
                    <td width="25%">Saran Perbaikan </td>
                </tr>
                <?php
                for ($i = 1; $i <= 5; $i++) :
                ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php
                endfor;
                ?>
            </table>
        </td>
    </tr>


</table>