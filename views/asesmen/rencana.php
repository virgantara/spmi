<?php

use yii\helpers\Html;
use app\helpers\MyHelper;
use app\models\AmiTujuanAudit;
use app\models\TujuanAudit;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\time\TimePicker;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rencana';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['ami']];
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
$amiTujuanAudit = AmiTujuanAudit::find();

?>
<style>
</style>
<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>
    </div>

    <div class="panel-body">
        <div class="x_content">

            <h3>Rencana Audit Mutu Internal - <?= $amiUnit->unit->nama ?> </h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <br>

            <div class="x_content">
                <?php if (Yii::$app->user->can('admin')) : ?>
                    <h4>Ruang Lingkup</h4>
                    <hr>
                    <?php
                    $gridColumns = [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'contentOptions' => ['class' => 'kartik-sheet-style'],
                            'width' => '36px',
                            'pageSummary' => 'Total',
                            'pageSummaryOptions' => ['colspan' => 6],
                            'header' => '',
                            'headerOptions' => ['class' => 'kartik-sheet-style']
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'ruang_lingkup',
                            'contentOptions' => [
                                'width' => '92%',
                            ],
                            'editableOptions' => [
                                'inputType' => Editable::INPUT_TIME,
                            ],
                        ],
                        [
                            'contentOptions' => [
                                'align' => 'center',
                                'width' => '5%'
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $html = Html::a('<i class="fa fa-trash"></i>', ['ruang-lingkup/rencana-delete', 'id' => $model->id], [
                                    // 'class' => 'btn btn-danger btn-sm',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                                return $html;
                            }
                        ]
                    ]; ?>
                    <?= GridView::widget([
                        'pager' => [
                            'options' => ['class' => 'pagination'],
                            'activePageCssClass' => 'active paginate_button page-item',
                            'disabledPageCssClass' => 'disabled paginate_button',
                            'prevPageLabel' => 'Previous',
                            'nextPageLabel' => 'Next',
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last',
                            'nextPageCssClass' => 'paginate_button next page-item',
                            'prevPageCssClass' => 'paginate_button previous page-item',
                            'firstPageCssClass' => 'first paginate_button page-item',
                            'lastPageCssClass' => 'last paginate_button page-item',
                            'maxButtonCount' => 10,
                            'linkOptions' => [
                                'class' => 'page-link'
                            ]
                        ],
                        'dataProvider' => $dataRuangLingkup,
                        'filterModel' => $ruangLingkupSearch,
                        'responsiveWrap' => false,
                        'columns' => $gridColumns,
                        'containerOptions' => ['style' => 'overflow: auto'],
                        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                        'containerOptions' => ['style' => 'overflow: auto'],
                        'beforeHeader' => [
                            [
                                'columns' => [
                                    ['content' => Yii::t('app', 'Tujuan Audit Mutu Internal'), 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                                ],
                                'options' => ['class' => 'skip-export']
                            ]
                        ],
                        'exportConfig' => [
                            GridView::PDF => ['label' => 'Save as PDF'],
                            GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                            GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                            GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                        ],

                        'toolbar' =>  [
                            [
                                'content' => Html::a(
                                    '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Ruang Lingkup'),
                                    // ['pertanyaan/create', 'id' => $bagian[$key]->id], 
                                    'javascript:void(0)',
                                    [
                                        'class' => 'btn btn-success add-ruang-lingkup',
                                        'data-amiunitid' => $id,
                                    ]
                                ),
                                'options' => ['class' => 'btn-group pull-left'],
                            ],

                            '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                        ],
                        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                        // set export properties
                        'export' => [
                            'fontAwesome' => true
                        ],
                        'pjax' => false,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                            'options' => [
                                'id' => 'pjax-container-ruang-lingkup',
                            ]
                        ],
                        'id' => 'ruang-lingkup-grid',
                        'bordered' => true,
                        'striped' => true,
                        // 'condensed' => false,
                        // 'responsive' => false,
                        'hover' => true,
                        // 'floatHeader' => true,
                        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY
                        ],
                    ]); ?>

                    <br>
                    <h4>Tujuan Audit</h4>
                    <hr>

                    <div class="main-layer">
                        <?php
                        $gridColumns = [
                            [
                                'class' => 'kartik\grid\SerialColumn',
                                'contentOptions' => ['class' => 'kartik-sheet-style'],
                                'width' => '36px',
                                'pageSummary' => 'Total',
                                'pageSummaryOptions' => ['colspan' => 6],
                                'header' => '',
                                'headerOptions' => ['class' => 'kartik-sheet-style']
                            ],
                            [
                                'attribute' => 'tujuan_audit_id',
                                'label' => 'Tujuan Audit',
                                'contentOptions' => [
                                    'width' => '92%',
                                ],
                                'filter' => ArrayHelper::map(TujuanAudit::find()->all(), 'id', 'tujuan_audit'),
                                'filterType' => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'value' => function ($model) {
                                    return $model->tujuanAudit->tujuan_audit;
                                }
                            ],
                            [
                                'contentOptions' => [
                                    'align' => 'center',
                                    'width' => '5%'
                                ],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $html = Html::a('<i class="fa fa-trash"></i>', ['ami-tujuan-audit/rencana-delete', 'id' => $model->id], [
                                        // 'class' => 'btn btn-danger btn-sm',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                    return $html;
                                }
                            ]
                        ]; ?>
                        <?= GridView::widget([
                            'pager' => [
                                'options' => ['class' => 'pagination'],
                                'activePageCssClass' => 'active paginate_button page-item',
                                'disabledPageCssClass' => 'disabled paginate_button',
                                'prevPageLabel' => 'Previous',
                                'nextPageLabel' => 'Next',
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last',
                                'nextPageCssClass' => 'paginate_button next page-item',
                                'prevPageCssClass' => 'paginate_button previous page-item',
                                'firstPageCssClass' => 'first paginate_button page-item',
                                'lastPageCssClass' => 'last paginate_button page-item',
                                'maxButtonCount' => 10,
                                'linkOptions' => [
                                    'class' => 'page-link'
                                ]
                            ],
                            'dataProvider' => $dataAmiTujuanAudit,
                            'filterModel' => $AmiTujuanAuditSearch,
                            'responsiveWrap' => false,
                            'columns' => $gridColumns,
                            'containerOptions' => ['style' => 'overflow: auto'],
                            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                            'containerOptions' => ['style' => 'overflow: auto'],
                            'beforeHeader' => [
                                [
                                    'columns' => [
                                        ['content' => Yii::t('app', 'Tujuan Audit Mutu Internal'), 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                                    ],
                                    'options' => ['class' => 'skip-export']
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => ['label' => 'Save as PDF'],
                                GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                                GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                                GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                            ],

                            'toolbar' =>  [
                                [
                                    'content' => Html::button('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Edit'), ['class' => 'btn btn-success btn-edit-tujuan-audit']),
                                    'options' => ['class' => 'btn-group pull-left'],
                                ],

                                '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                            ],
                            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                            // set export properties
                            'export' => [
                                'fontAwesome' => true
                            ],
                            'pjax' => false,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                                'options' => [
                                    'id' => 'pjax-container-tujuan-audit-main',
                                ]
                            ],
                            'id' => 'tujuan-audit-grid-main',
                            'bordered' => true,
                            'striped' => true,
                            // 'condensed' => false,
                            // 'responsive' => false,
                            'hover' => true,
                            // 'floatHeader' => true,
                            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                            'panel' => [
                                'type' => GridView::TYPE_PRIMARY
                            ],
                        ]); ?>
                    </div>
                    <div class="tujuan-audit-layer" style="display: none;">
                        <?php
                        $gridColumns = [
                            [
                                'class' => 'kartik\grid\SerialColumn',
                                'contentOptions' => ['class' => 'kartik-sheet-style'],
                                'width' => '36px',
                                'pageSummary' => 'Total',
                                'pageSummaryOptions' => ['colspan' => 6],
                                'header' => '',
                                'headerOptions' => ['class' => 'kartik-sheet-style']
                            ],
                            'tujuan_audit',
                            [
                                'contentOptions' => [
                                    'align' => 'center',
                                    'width' => '5%'
                                ],
                                'format' => 'raw',
                                'value' => function ($model) use ($amiUnit, $amiTujuanAudit) {
                                    $status = '';
                                    if ($amiTujuanAudit->where(['ami_unit_id' => $amiUnit->id, 'tujuan_audit_id' => $model->id])->one()) {
                                        $status = 'checked';
                                    }
                                    $html = Html::checkbox($model->id, $status, ['class' => 'cb-tujuan-audit', 'data-tujuanauditid' => $model->id, 'data-amiunitid' => $amiUnit->id]);
                                    return $html;
                                }
                            ]
                        ]; ?>
                        <?= GridView::widget([
                            'pager' => [
                                'options' => ['class' => 'pagination'],
                                'activePageCssClass' => 'active paginate_button page-item',
                                'disabledPageCssClass' => 'disabled paginate_button',
                                'prevPageLabel' => 'Previous',
                                'nextPageLabel' => 'Next',
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last',
                                'nextPageCssClass' => 'paginate_button next page-item',
                                'prevPageCssClass' => 'paginate_button previous page-item',
                                'firstPageCssClass' => 'first paginate_button page-item',
                                'lastPageCssClass' => 'last paginate_button page-item',
                                'maxButtonCount' => 10,
                                'linkOptions' => [
                                    'class' => 'page-link'
                                ]
                            ],
                            'dataProvider' => $dataTujuanAudit,
                            'filterModel' => $tujuanAuditSearch,
                            'responsiveWrap' => false,
                            'columns' => $gridColumns,
                            'containerOptions' => ['style' => 'overflow: auto'],
                            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                            'containerOptions' => ['style' => 'overflow: auto'],
                            'beforeHeader' => [
                                [
                                    'columns' => [
                                        ['content' => Yii::t('app', 'Tujuan Audit Mutu Internal'), 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                                    ],
                                    'options' => ['class' => 'skip-export']
                                ]
                            ],
                            'exportConfig' => [
                                GridView::PDF => ['label' => 'Save as PDF'],
                                GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                                GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                                GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                            ],

                            'toolbar' =>  [
                                [
                                    'content' => Html::button('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Tujuan'), ['class' => 'btn btn-info add-tujuan-audit']),
                                    'options' => ['class' => 'btn-group pull-left'],
                                ],
                                [
                                    'content' => Html::button('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Edit'), ['class' => 'btn btn-success btn-main']),
                                    'options' => ['class' => 'btn-group pull-left'],
                                ],

                                '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                            ],
                            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                            // set export properties
                            'export' => [
                                'fontAwesome' => true
                            ],
                            'pjax' => false,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                                'options' => [
                                    'id' => 'pjax-container-tujuan-audit-master',
                                ]
                            ],
                            'id' => 'tujuan-audit-grid-master',
                            'bordered' => true,
                            'striped' => true,
                            // 'condensed' => false,
                            // 'responsive' => false,
                            'hover' => true,
                            // 'floatHeader' => true,
                            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                            'panel' => [
                                'type' => GridView::TYPE_PRIMARY
                            ],
                        ]); ?>
                    </div>
                <?php endif; ?>
                <?php if (!Yii::$app->user->can('admin')) : ?>
                    <h4>Ruang Lingkup</h4>
                    <hr>
                    <div class="table-responsive">
                        <?php if (empty($ruangLingkup)) : ?>
                            <p>Tidak ada data yang tersedia.</p>
                        <?php else : ?>
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <td style="width: 5%; text-align: center;">No</td>
                                        <td>Ruang Lingkup</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($ruangLingkup as $key => $v) : ?>
                                        <tr>
                                            <td style="width: 5%; text-align: center;"><?= $no ?></td>
                                            <td><?= $v->ruang_lingkup ?></td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                    <br>

                    <h4>Tujuan Audit</h4>
                    <hr>
                    <div class="table-responsive">
                        <?php if (empty($tujuanAudit)) : ?>
                            <p>Tidak ada data yang tersedia.</p>
                        <?php else : ?>
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <td style="width: 5%; text-align: center;">No</td>
                                        <td>Tujuan Audit</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($tujuanAudit as $key => $v) : ?>
                                        <tr>
                                            <td style="width: 5%; text-align: center;"><?= $no ?></td>
                                            <td><?= $v->tujuanAudit->tujuan_audit ?></td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>



                <br>
                <h4>Jadwal Audit Mutu Internal</h4>
                <hr>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'urutan',
                        'hAlign' => 'center',
                        'refreshGrid' => true,
                        'contentOptions' => [
                            'width' => '5%',
                        ],
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'waktu_mulai',
                        'hAlign' => 'center',
                        'contentOptions' => [
                            'width' => '8%',
                        ],
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'waktu_selesai',
                        'hAlign' => 'center',
                        'contentOptions' => [
                            'width' => '8%',
                        ],
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'acara',
                        'contentOptions' => [
                            'width' => '38%',
                        ],
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'pelaksana',
                        'contentOptions' => [
                            'width' => '38%',
                        ],
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                        ],
                    ],
                    [
                        'contentOptions' => [
                            'align' => 'center',
                            'width' => '5%'
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            $html = Html::a('<i class="fa fa-trash"></i>', ['jadwal/delete-rencana', 'id' => $model->id], [
                                // 'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                            return $html;
                        }
                    ]
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataJadwal,
                    'filterModel' => $jadwalSearch,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => Yii::t('app', 'Jadwal Audit Mutu Internal'), 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        [
                            'content' => Html::label('Tanggal AMI', 'username', ['class' => 'form-control']),
                            'options' => ['class' => 'btn-group pull-left'],
                        ],
                        [
                            'content' => Html::textInput(
                                'tanggal_ami',
                                MyHelper::convertTanggalIndo($amiUnit->tanggal_ami),
                                ['class' => 'form-control', 'disabled' => 'disabled']
                            ),
                            'options' => ['class' => 'btn-group pull-left'],
                        ],
                        [
                            'content' => Html::a(
                                '<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Tanggal AMI'),
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-success change-tanggal-ami',
                                    'data-amiunitid' => $id,
                                    'data-tanggal' => $amiUnit->tanggal_ami,
                                ]
                            ),
                            'options' => ['class' => 'btn-group pull-left'],
                        ],
                        [
                            'content' => Html::a(
                                '<i class="fa fa-plus"></i> ' . Yii::t('app', 'Tambah Jadwal'),
                                // ['pertanyaan/create', 'id' => $bagian[$key]->id], 
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-success add-jadwal',
                                    'data-amiunitid' => $id,
                                ]
                            ),
                            'options' => ['class' => 'btn-group pull-left'],
                        ],
                        [
                            'content' => Html::a(
                                '<i class="fa fa-file-text"></i> ' . Yii::t('app', 'Template'),
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-primary',
                                    'id' => 'btn-set-normal',
                                    'data-amiunitid' => $id,
                                ]
                            ),
                            'options' => ['class' => 'btn-group pull-left'],
                        ],

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => false,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container-jadwal-audit',
                        ]
                    ],
                    'id' => 'jadwal-audit-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>

        </div>
    </div>
</div>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Tambah Tujuan Audit</h3>',
    'id' => 'modal-tujuan-audit',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-tujuan-audit">
    <div class="form-group">
        <label for=""><?= Yii::t('app', 'Tujuan Audit') ?></label>
        <?= Html::textarea('tujuan_audit', '', ['class' => 'form-control', 'id' => 'ruang_lingkup', 'placeholder' => 'Masukkan tujuan audit']); ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-plus"></i> Tambah', ['class' => 'btn btn-success btn-sm', 'id' => 'btn-save-tujuan-audit']) ?>
    </div>
</form>

<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Tambah Ruang Lingkup</h3>',
    'id' => 'modal-ruang-lingkup',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-ruang-lingkup">
    <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'ami_unit_id_ruang_lingkup']) ?>
    <div class="form-group">
        <label for=""><?= Yii::t('app', 'Ruang Lingkup') ?></label>
        <?= Html::textarea('ruang_lingkup', '', ['class' => 'form-control', 'id' => 'ruang_lingkup', 'placeholder' => 'Masukkan ruang lingkup']); ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-plus"></i> Tambah', ['class' => 'btn btn-success btn-sm', 'id' => 'btn-save-ruang-lingkup']) ?>
    </div>
</form>

<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Tambah Jadwal Acara</h3>',
    'id' => 'jadwal',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-jadwal">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Waktu Mulai</label>
                <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'ami_unit_id']) ?>
                <?= TimePicker::widget([
                    'name'          => 'waktu_mulai',
                    'readonly'      => true,
                    'pluginOptions' => [
                        // 'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        // 'secondStep' => 5,
                    ]
                ]) ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Waktu Selesai</label>
                <?= TimePicker::widget([
                    'name' => 'waktu_selesai',
                    'readonly' => true,
                    'pluginOptions' => [
                        // 'showSeconds' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        // 'secondStep' => 5,
                    ]
                ]) ?>

            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="">Acara</label>
        <?= Html::textInput('acara', '', ['class' => 'form-control', 'id' => 'jadwal_acara', 'placeholder' => 'Masukkan nama acara']); ?>
    </div>

    <div class="form-group">
        <label for="">Pelaksana</label>
        <?= Html::textInput('pelaksana', '', ['class' => 'form-control', 'id' => 'jadwal_pelaksana', 'placeholder' => 'Masukkan pelaksana']); ?>
    </div>

    <div class="form-group">
        <label for="">Urutan</label>
        <?= Html::textInput('urutan', '', ['class' => 'form-control', 'id' => 'jadwal_urutan', 'placeholder' => 'Masukkan urutan']); ?>
    </div>



    <div class="form-group">
        <?= Html::button('<i class="fa fa-plus"></i> Tambah', ['class' => 'btn btn-success btn-sm', 'id' => 'btn-save-jadwal']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Ubah Tanggal AMI</h3>',
    'id' => 'ubah_tanggal',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-ubah-tanggal">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Tanggal AMI</label>
                <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'ubah_tanggal_ami_unit_id']) ?>
                <?= DatePicker::widget([
                    'name' => 'tanggal_ami',
                    'id' => 'ubah_tanggal_tanggal_ami',
                    'readonly'  => true,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); ?>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-plus"></i> Simpan', ['class' => 'btn btn-success btn-sm', 'id' => 'btn-save-tanggal']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("change", ".cb-tujuan-audit", function(e) {
        e.preventDefault()
        var isChecked = $(this).is(':checked');

        var obj = new Object;

        obj.tujuan_audit_id = $(this).data('tujuanauditid');
        obj.ami_unit_id = $(this).data('amiunitid');

        $.ajax({
            url: "/ami-tujuan-audit/ajax-set-tujuan-audit",
            type: "POST",
            // async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {
                var hasil = $.parseJSON(data)
                console.log(hasil);
            }
        })

        // if (isChecked) {
        //     console.log('Checkbox dicentang');
        //     console.log('Tujuan Audit ID: ' + tujuanAuditID);
        //     console.log('AMI Unit ID: ' + amiUnitID);

        //     $.ajax({
        //         url: "/jadwal/ajax-set-normal",
        //         type: "POST",
        //         async: true,
        //         data: obj,
        //         error: function(e) {
        //             console.log(e.responseText)
        //         },
        //         beforeSend: function() {
        //             Swal.showLoading()
        //         },
        //         success: function(data) {
        //             Swal.close()
        //             var hasil = $.parseJSON(data)
        //             if (hasil.code == 200) {
        //                 Swal.fire({
        //                     title: 'Yeay!',
        //                     icon: 'success',
        //                     text: hasil.message
        //                 }).then(res => {
        //                     window.location.reload()
        //                 });
        //             } else {
        //                 Swal.fire({
        //                     title: 'Oops!',
        //                     icon: 'error',
        //                     text: hasil.message
        //                 })
        //             }
        //         }
        //     })
        // } else {
        //     console.log('Checkbox tidak dicentang');
        //     console.log('Tujuan Audit ID: ' + tujuanAuditID);
        //     console.log('AMI Unit ID: ' + amiUnitID);
        // }
    })

    $(document).on("click", ".btn-edit-tujuan-audit", function(e) {
        e.preventDefault()
        $('.main-layer').hide();
        $('.tujuan-audit-layer').show();
    })

    $(document).on("click", ".btn-main", function(e) {
        e.preventDefault()
        $('.main-layer').show();
        $('.tujuan-audit-layer').hide();
        window.location.reload()
    })

    $(document).on("click", ".add-jadwal", function(e) {
        e.preventDefault()

        $("#ami_unit_id").val($(this).data("amiunitid"));
        $("#jadwal").modal("show");
    })

    $(document).on("click", ".change-tanggal-ami", function(e) {
        e.preventDefault()

        $("#ubah_tanggal_ami_unit_id").val($(this).data("amiunitid"));
        $("#ubah_tanggal_tanggal_ami").val($(this).data("tanggal"));
        $("#ubah_tanggal").modal("show");
    })

    $(document).on("click", ".add-ruang-lingkup", function(e) {
        e.preventDefault()

        $("#ami_unit_id_ruang_lingkup").val($(this).data("amiunitid"));
        $("#modal-ruang-lingkup").modal("show");

    })

    $(document).on("click", ".add-tujuan-audit", function(e) {
        e.preventDefault()

        $("#modal-tujuan-audit").modal("show");
    })

    $(document).on("click", "#btn-set-normal", function(e) {
        e.preventDefault();

        var obj = new Object;
        obj.ami_unit_id = $(this).data('amiunitid');

        $.ajax({
            url: "/jadwal/ajax-set-normal",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });


    $(document).on("click", "#btn-save-tanggal", function(e) {
        e.preventDefault();

        var obj = $("#form-ubah-tanggal").serialize()

        $.ajax({
            url: "/ami-unit/ajax-update-tanggal",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#ubah_tanggal").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-save-jadwal", function(e) {
        e.preventDefault();

        var obj = $("#form-jadwal").serialize()

        $.ajax({
            url: "/jadwal/ajax-add-jadwal",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-al").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-save-ruang-lingkup", function(e) {
        e.preventDefault();

        var obj = $("#form-ruang-lingkup").serialize()

        $.ajax({
            url: "/ruang-lingkup/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-ruang-lingkup").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-save-tujuan-audit", function(e) {
        e.preventDefault();

        var obj = $("#form-tujuan-audit").serialize()

        $.ajax({
            url: "/tujuan-audit/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-tujuan-audit").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });
</script>
<?php JSRegister::end(); ?>