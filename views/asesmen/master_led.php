<?php

use app\models\Ami;
use app\models\Indikator;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AsesmenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asesmens';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="col-md-3 col-lg-3 col-xs-12">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['asesmen/master-led',],
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="form-group">
                    <label class="control-label ">Periode AMI</label>
                    <?= Select2::widget([
                        'name' => 'ami_id',
                        'data' => ArrayHelper::map(Ami::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                        // 'value' => $prodi_mk,
                        'options' => ['placeholder' => Yii::t('app', '- Pilih Periode AMI -'), 'id' => 'ami_id'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]); ?>

                </div>


                <div class="form-group">
                    <label class="control-label ">Unit Kerja</label>
                    <?= DepDrop::widget([
                        'name' => 'ami_unit_id',
                        // 'value' => '',
                        'type' => DepDrop::TYPE_SELECT2,
                        'options' => ['id' => 'ami_unit_id'],
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['ami_id'],
                            'initialize' => true,
                            'placeholder' => '- Pilih Unit Kerja -',
                            'url' => Url::to(['/ami/subamiunit'])
                        ]
                    ]); ?>

                </div>

                <div class="form-group clearfix">
                    <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                        Cari</button>
                    <?php ActiveForm::end(); ?>
                    <?php // Html::a('<i class="fa fa-plus"></i> Buat Penugasan', [''], ['class' => 'btn btn-success', 'id' => 'btn-add']) 
                    ?>
                </div>
            </div>


            <div class="x_content">



                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'label'     => 'Unit',
                        'format'    => 'raw',
                        'value'     => function ($data) {
                            return $data->amiUnit->unit->nama;
                        },
                    ],
                    [
                        'label'     => 'Kriteria',
                        'format'    => 'raw',
                        'value'     => function ($data) {
                            $indikator = Indikator::findOne($data->indikator_id);
                            return $indikator->kriteria->nama;
                        },
                    ],
                    [
                        'label'     => 'Jenis Instrumen',
                        'format'    => 'raw',
                        'value'     => function ($data) {
                            $indikator = Indikator::findOne($data->indikator_id);
                            return ($indikator->jenis_indikator == 0 ? 'Instrumen Umum' : 'Instrumen Khusus');
                        },
                    ],
                    [
                        'label'     => 'Nama Indikator / Instrumen',
                        'format'    => 'raw',
                        'value'     => function ($data) {
                            $indikator = Indikator::findOne($data->indikator_id);
                            return $indikator->nama;
                        },
                    ],
                    [
                        'attribute' => 'link_bukti',
                        'filter'    => '',
                    ],
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>