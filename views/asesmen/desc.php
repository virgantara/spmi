<?php


/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Auditor;
use app\models\Persetujuan;
use app\models\StatusAuditor;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = 'Audit Mutu Internal';
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();
?>
<style type="text/css">
    input[type=checkbox] {
        /* Double-sized Checkboxes */
        -ms-transform: scale(1.75);
        /* IE */
        -moz-transform: scale(1.75);
        /* FF */
        -webkit-transform: scale(1.75);
        /* Safari and Chrome */
        -o-transform: scale(1.75);
        /* Opera */
        padding: 10px;
    }
</style>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">

                <?= (!isset($jadwal) && Yii::$app->user->can('auditee') ? Html::a('<i class="fa fa-calendar"></i> Buat Jadwal', ['jadwal-asesmen/create', 'id' => $model->id], ['class' => 'btn btn-success']) : '') ?>
                <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['asesmen/ami'], ['class' => 'btn btn-sm btn-info']) ?>
                <?= (Yii::$app->user->can('admin') ? Html::a('<i class="fa fa-print"></i> Cetak Berita Acara', ['asesmen/cetak-berita', 'ami_unit_id' => $model->id], ['class' => 'btn btn-sm btn-success', 'target' => "_blank"]) : '')  ?>
                <?= (Yii::$app->user->can('auditee') ? Html::a('<i class="fa fa-edit"></i> Update Lokasi', ['ami-auditor/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-success', 'id' => 'btn-dokumen', 'data-ami_unit' => $model->id]) : '') ?>
                <?= (Yii::$app->user->can('auditee') ? Html::a('<i class="fa fa-save"></i> Input LKPS', ['lkps/desc', 'id' => $model->id], ['class' => 'btn btn-sm btn-success disabled']) : '') ?>


            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        // 'unit_id',
                        // 'ami_id',
                        [
                            'attribute' => 'ami_id',
                            'filter' => $list_periode,
                            'value' => function ($data) {
                                return (!empty($data) ? $data->ami->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'unit_id',
                            'filter' => $list_unit,
                            'value' => function ($data) {
                                return (!empty($data) ? $data->unit->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'status_ami',
                            'filter' => $list_ami,
                            'value' => function ($data) {
                                // return (!empty($data) ? $data->unit->nama : null);
                                $list_ami = MyHelper::getStatusAmi();
                                return (!empty($list_ami[$data->status_ami]) ? $list_ami[$data->status_ami] : null);
                            }
                        ],
                        [
                            'attribute' => 'lokasi',
                            // 'filter' => $list_ami,
                            'value' => function ($data) {
                                // return (!empty($data) ? $data->unit->nama : null);
                                // $list_ami = MyHelper::getStatusAmi();
                                return (!empty($data->lokasi) ? $data->lokasi : 'Belum Ditentukan');
                            }
                        ],
                    ],
                ]) ?>

                <?php
                if (isset($jadwal)) :
                ?>
                    <?= (Yii::$app->user->can('auditee') ? Html::a('<i class="fa fa-edit"></i> Update Jadwal', ['jadwal-asesmen/update', 'id' => $jadwal->id, 'ami_unit_id' => $model->id], ['class' => 'btn btn-sm btn-warning']) : '')  ?>
                    <?php // (Yii::$app->user->can('admin') ? Html::a('<i class="fa fa-eye"></i> Hasil Asesmen', ['asesmen/hasil', 'ami_id' => $model->ami_id], ['class' => 'btn btn-sm btn-success disabled']) : '')  
                    ?>

                    <?php

                    $button = false;
                    if (Yii::$app->user->identity->access_role == 'auditor') {
                        $auditor = AmiAuditor::find()->where(['auditor_id' => Yii::$app->user->identity->auditor_id])->one();
                        // echo '<pre>';print_r($auditor);die;
                        if ($auditor->status_id == 1) {
                            $button = true;
                        }
                    } elseif (Yii::$app->user->can('admin')) {
                        $button = true;
                    }

                    ?>
                    <?= ($button == true ? Html::a('<i class="fa fa-eye"></i> Set Selesai', ['ami-unit/set-selesai', 'id' => $model->id], ['class' => 'btn btn-sm btn-success']) : '')  ?>



                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th style="text-align: center" class="alert alert-info" colspan="10"><i class="fa fa-calendar"></i> <strong>JADWAL AUDIT MUTU INTERNAL</strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center" rowspan="2"><strong>Input Documen (Evaluasi Diri)</strong></th>
                                <th style="text-align: center"><strong>Mulai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_mulai_led) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_mulai_led)) : "Belum Dibuat") ?></strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center"><strong>Selesai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_selesai_led) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_selesai_led)) : "Belum Dibuat") ?></strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center" rowspan="2"><strong>Asesmen Kecukupan</strong></th>
                                <th style="text-align: center"><strong>Mulai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_mulai_ak) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_mulai_ak)) : "Belum Dibuat") ?></strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center"><strong>Selesai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_selesai_ak) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_selesai_ak)) : "Belum Dibuat") ?></strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center" rowspan="2"><strong>Asesmen Lapangan</strong></th>
                                <th style="text-align: center"><strong>Mulai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_mulai_al) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_mulai_al)) : "Belum Dibuat") ?></strong></th>
                            </tr>
                            <tr>
                                <th style="text-align: center"><strong>Selesai</strong></th>
                                <th style="text-align: center"><strong><?= (isset($jadwal->tanggal_selesai_al) ? strftime('%H:%M - %A, %d %B %Y', strtotime($jadwal->tanggal_selesai_al)) : "Belum Dibuat") ?></strong></th>
                            </tr>

                        </table>
                    </div>
                <?php
                endif;
                ?>

                <?= (Yii::$app->user->can('admin') ? Html::a('<i class="fa fa-print"></i> Cetak Surat Tugas', ['asesmen/cetak-berita', 'ami_unit_id' => $model->id], ['class' => 'btn btn-sm btn-success disabled', 'target' => "_blank"]) : '')  ?>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th style="text-align: center" class="alert alert-info" colspan="10"><i class="fa fa-users"></i> <strong>PETUGAS AMI</strong></th>
                        </tr>
                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>No</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Jenis Petugas</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Nama Petugas</strong></th>
                            <!-- <th style="text-align: center" rowspan="1"><strong>Keterangan</strong></th> -->
                            <!-- <th style="text-align: center" rowspan="1"><strong>Aksi</strong></th> -->

                        </tr>
                        <tr>
                            <th style="text-align: center">1</th>
                            <th style="text-align: center">Auditee</th>
                            <?php
                            // echo '<pre>';print_r($model->unit->penanggung_jawab);die;
                            ?>
                            <th><?= (isset($model->unit->penanggung_jawab) ? $model->unit->penanggung_jawab : 'Belum ditentukan') ?></th>
                            <!-- <th style="text-align: center;color: red;">
                                <?php // (isset($model->unit->penanggung_jawab) ? $model->unit->penanggung_jawab : 'Belum ditentukan') 
                                ?>
                            </th> -->
                        </tr>

                        <?php


                        $i = 2;
                        foreach ($list_auditor as $a) :
                            // echo '<pre>';print_r($a);exit;
                            $auditor = Auditor::find()->where([
                                'id' => $a->auditor_id,
                            ])->one();

                            $status = StatusAuditor::find()->where([
                                'id' => $a->status_id,
                            ])->one();
                        ?>

                            <tr>
                                <td style="text-align: center"><?= $i; ?></td>
                                <td style="text-align: center"><?= $status->nama ?></td>
                                <td><?= $auditor->nama ?></td>
                                <!-- <td style="text-align: center;color: red;">
                                    <?php // $auditor->nama 
                                    ?>
                                </td> -->

                            </tr>
                        <?php
                            $i++;
                        endforeach;
                        ?>

                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th style="text-align: center" class="alert alert-info" colspan="10"><i class="fa fa-file"></i> <strong>ASESMEN</strong></th>
                        </tr>
                        <tr>

                            <?php
                            // Yii::$app->user->identity->access_role
                            // echo '<pre>';print_r(Yii::$app->user->identity->access_role);die;
                            if (Yii::$app->user->can('auditee')) :
                            ?>
                        <tr>
                            <th style="text-align: center" rowspan="1"><strong>Pengisian Laporan Evaluasi Diri</strong></th>
                            <td style="text-align: center">
                                <?= Html::a('<i class="fa fa-edit"></i> Start', ['asesmen/penilaian', 'id' => $model->id], ['class' => 'btn btn-sm btn-success']) ?>
                            </td>
                        </tr>

                    <?php
                            endif;
                            if (Yii::$app->user->can('auditor')) :
                    ?>
                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>Pengisian Asesmen Kecukupan</strong></th>
                            <td style="text-align: center">
                                <?= Html::a('<i class="fa fa-edit"></i> Start', ['asesmen/penilaian', 'id' => $model->id], ['class' => 'btn btn-sm btn-success']) ?>
                            </td>
                        </tr>
                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>Pengisian Asesmen Lapangan</strong></th>
                            <td style="text-align: center">
                                <?= Html::a('<i class="fa fa-edit"></i> Start', ['asesmen/penilaian', 'id' => $model->id], ['class' => 'btn btn-sm btn-success']) ?>
                            </td>
                        </tr>
                    <?php
                            endif;
                    ?>
                    </tr>

                    </table>
                </div>


                <?php
                // if (isset($p)) :
                ?>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th style="text-align: center" class="alert alert-danger" colspan="10"><i class="fa fa-check-square-o"></i> <strong>PERSETUJUAN - FITUR BELUM SELESAI</strong></th>
                        </tr>
                        <tr>
                            <th style="text-align: center">NAMA DOKUMEN</th>
                            <th style="text-align: center">CEKLIS PERSETUJUAN</th>
                        </tr>

                        <tr>
                            <th style="text-align: center" rowspan="1"><strong>FORMULIR - PERTANYAAN AUDIT DAN CHECKLIST DOKUMEN</strong></th>
                            <td style="text-align: center">
                                <?php
                                $persetujuan1 = Persetujuan::find()->where([
                                    'auditor_id' => Yii::$app->user->identity->auditor_id,
                                    'ami_unit_id' => $model->id,
                                ])->andWhere(['dokumen_id' => 1])->one();

                                $persetujuan2 = Persetujuan::find()->where([
                                    'auditor_id' => Yii::$app->user->identity->auditor_id,
                                    'ami_unit_id' => $model->id,
                                ])->andWhere(['dokumen_id' => 2])->one();

                                $persetujuan3 = Persetujuan::find()->where([
                                    'auditor_id' => Yii::$app->user->identity->auditor_id,
                                    'ami_unit_id' => $model->id,
                                ])->andWhere(['dokumen_id' => 3])->one();

                                ?>
                                <input type="checkbox" class="mycheck" <?= (isset($persetujuan1) ? 'checked' : 'disabled') ?> data-unit="<?= $model->id; ?>" data-auditor="<?= Yii::$app->user->identity->auditor_id ?>" value="1" data-dokumen="1">

                            </td>
                        </tr>

                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>FORMULIR - DESKRIPSI TEMUAN AUDIT</strong></th>
                            <td style="text-align: center">
                                <input type="checkbox" class="mycheck" <?= (isset($persetujuan2) ? 'checked' : 'disabled') ?> data-unit="<?= $model->id; ?>" data-auditor="<?= Yii::$app->user->identity->auditor_id ?>" value="1" data-dokumen="2">

                            </td>
                        </tr>
                        <tr>

                            <th style="text-align: center" rowspan="1"><strong>FORMULIR - BERITA ACARA AMI DAN REKOMENDASI MUTU</strong></th>
                            <td style="text-align: center">
                                <input type="checkbox" class="mycheck" <?= (isset($persetujuan3) ? 'checked' : 'disabled') ?> data-unit="<?= $model->id; ?>" data-auditor="<?= Yii::$app->user->identity->auditor_id ?>" value="1" data-dokumen="3">

                            </td>
                        </tr>

                    </table>
                </div>

                <?php
                // endif;
                ?>

            </div>
        </div>

    </div>
</div>



<!-- Modal Dokumen -->

<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-dokumen',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-dokumen">
    <div class="form-group">
        <label for="">Lokasi</label>
        <?= Html::textInput('lokasi', '', ['class' => 'form-control', 'id' => 'dokumen_lokasi']) ?>
        <?= Html::hiddenInput('ami_unit_id', '', ['id' => 'ami_unit_id_dokumen']) ?>


    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan-dokumen']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-simpan-dokumen", function(e) {
        e.preventDefault();

        var obj = $("#form-dokumen").serialize()

        $.ajax({
            url: "/ami-unit/ajax-dokumen",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-dokumen").modal("hide")
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-dokumen", function(e) {
        e.preventDefault();
        var ami_unit_id = $(this).data("ami_unit")

        $("#ami_unit_id_dokumen").val(ami_unit_id)
        $("#modal-dokumen").modal("show")

    });

    $(".mycheck").change(function() {
        var unit = $(this).data("unit");
        var auditor = $(this).data("auditor");
        var dokumen = $(this).data("dokumen");
        var obj = new Object;
        obj.ami_unit_id = unit;
        obj.auditor_id = auditor;
        obj.dokumen_id = dokumen;
        obj.checked = $(this).prop("checked") ? "1" : "0";

        $.ajax({
            type: "POST",
            url: "' . Url::to(['persetujuan/ajax-auth-app']) . '",
            data: {
                dataPost: obj
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        });
    });
</script>
<?php JSRegister::end(); ?>