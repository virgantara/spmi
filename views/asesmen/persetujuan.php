<?php

use yii\helpers\Html;
use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Persetujuan;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Persetujuan';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['ami']];
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table,
    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        vertical-align: middle;
        /* Center the content vertically */
    }
</style>
<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body col-md-8">

        <h3>Master Persetujuan Dokumen</h3>
        <br>
        <div class="x_content">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width: 32%; vertical-align: middle;">Nama Dokumen</th>
                        <th style="text-align: center; vertical-align: middle;">Status</th>
                        <th style="text-align: center; vertical-align: middle;">Dokumen</th>
                        <th style="text-align: center; vertical-align: middle;">Persetujuan</th>
                    </tr>
                </thead>

                <?php
                $auditorId = Yii::$app->user->identity->auditor_id;
                $checked = isset($persetujuan) ? 'checked' : '';

                foreach (MyHelper::listDokumenAmi() as $dokumen) :
                    if ($dokumen['visible'] === true) :
                ?>

                        <tr>
                            <td><?= $dokumen['nama'] ?? '' ?></td>
                            <td style="text-align: center;">
                                <?php if (isset($dataPersetujuan[$dokumen['kode']])) : ?>
                                    <?= Html::a(
                                        '<i class="fa fa-check"></i>',
                                        'javascript:void(0)',
                                        [
                                            'class' => 'btn btn-sm btn-success',
                                            'disabled' => 'disabled'
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td style="text-align: center;">
                                <?= Html::a(
                                    '<i class="fa fa-file-pdf-o"></i>',
                                    [
                                        $dokumen['link-cetak'],
                                        'ami_unit_id' => $amiUnit->id
                                    ],
                                    [
                                        'class' => 'btn btn-sm btn-danger',
                                        'target' => "_blank"
                                    ]
                                ) ?>
                            </td>
                            <td style="text-align: center;">
                                <?php if (isset($dataPersetujuan[$dokumen['kode']])) : ?>
                                    <?= Html::button(
                                        '<i class="fa fa-check-square-o"></i> Setujui',
                                        [
                                            'class' => 'btn btn-sm btn-primary',
                                            'disabled' => 'disabled'
                                        ]
                                    ) ?>
                                <?php else : ?>
                                    <?= Html::button(
                                        '<i class="fa fa-check-square-o"></i> Setujui',
                                        [
                                            'class' => 'btn btn-sm btn-primary btn-persetujuan',
                                            'id' => 'btn-setujui-' . $dokumen['kode'],
                                            'data-kode' => $dokumen['kode'],
                                            'data-nama' => $dokumen['nama'],
                                            'data-unit' => $amiUnit->unit->nama
                                        ]
                                    ) ?>
                                <?php endif; ?>

                            </td>
                        </tr>

                <?php
                    endif;
                endforeach;
                ?>

            </table>
        </div>
    </div>
</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", ".btn-persetujuan", function(e) {
        e.preventDefault();

        let btn = $(this);
        let amiUnitId = `<?= $amiUnit->id ?? null ?>` ?? null;
        let auditorId = `<?= Yii::$app->user->identity->auditor_id ?? null ?>` ?? null;
        let unitKerjaId = `<?= Yii::$app->user->identity->unit_kerja_id ?? null ?>` ?? null;
        let kode = btn.data("kode");

        // Using SweetAlert for confirmation
        Swal.fire({
            title: 'Konfirmasi',
            text: `Apakah Anda yakin ingin menyetujui dokumen ${btn.data("nama")} [ ${btn.data("unit")} ] ?`,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {

                var obj = new Object;
                // Creating the object with required data
                obj.ami_unit_id = amiUnitId ?? null;
                obj.auditor_id = auditorId ?? null;
                obj.unit_kerja_id = unitKerjaId ?? null;
                obj.kode_dokumen = kode;

                $.ajax({
                    url: "/persetujuan/ajax-add-persetujuan",
                    type: "POST",
                    async: true,
                    data: obj,
                    error: function(e) {
                        console.log(e.responseText);
                    },
                    beforeSend: function() {
                        Swal.showLoading();
                    },
                    success: function(data) {
                        Swal.close();
                        var hasil = $.parseJSON(data);
                        switch (hasil.code) {
                            case 200:
                                Swal.fire({
                                    title: 'Yeay!',
                                    icon: 'success',
                                    text: hasil.message
                                }).then(res => {
                                    $("#modal-tilik").modal("hide");
                                    window.location.reload();
                                });
                                break;
                            case 400:
                                Swal.fire({
                                    title: 'Oops!',
                                    icon: 'warning',
                                    text: hasil.message
                                }).then(res => {
                                    $("#modal-tilik").modal("hide");
                                    window.location.reload();
                                });
                                break;

                            default:
                                Swal.fire({
                                    title: 'Oops!',
                                    icon: 'error',
                                    text: hasil.message
                                });
                                break;
                        }
                    }
                });
            }
        });
    });
</script>
<?php JSRegister::end(); ?>