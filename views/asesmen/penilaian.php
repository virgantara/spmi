<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Dokumen;
use app\models\KelompokDokumen;
use app\models\Pengelompokan;
use app\models\Temuan;
use app\models\Tilik;
use app\models\UnitKerja;
use kartik\color\ColorInput;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Penilaian';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['ami']];
$this->params['breadcrumbs'][] = ['label' => 'Asesmen', 'url' => ['proses', 'id' => $id]];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();

$ami_unit = AmiUnit::findOne($id);
$ami = Ami::findOne($ami_unit->ami_id);
$unit = UnitKerja::findOne($ami_unit->unit_id);
// echo '<pre>';print_r($ami_unit);exit;

$asesmen_nama = "$ami->nama - $unit->nama";

// $kriteria_khusus_id = [
//     '100' => 'Kriteria Khusus'
// ];


?>

<style>
    .extra-super-large-modal .modal-dialog {
        max-width: 80%;
        width: 80%;
    }

    /* CSS untuk radio button yang diubah menjadi kotak checklist */
    input[type="radio"] {
        display: none;
    }

    input[type="radio"]+label {
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    input[type="radio"]+label::before {
        content: '';
        display: inline-block;
        width: 20px;
        /* Sesuaikan ukuran kotak sesuai kebutuhan */
        height: 20px;
        border: 1px solid #000;
        margin-right: 5px;
        /* Sesuaikan jarak antara kotak dan teks */
    }

    input[type="radio"]:checked+label::before {
        content: '\2713';
        font-size: 16px;
        color: #000;
    }

    /* Teks di samping ceklis */
    input[type="radio"]+label span {
        color: #000;
    }
</style>

<input type="hidden" name="kode" id="jenisPenilaian" value="<?= $_GET['jenisPenilaian'] ?>">
<input type="hidden" name="ami_unit_id" id="amiUnitId" value="<?= $id ?>">

<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body">
        <div class="x_content">

            <span class="badge bg-green mr-2" style="margin-right: .5rem"><?= MyHelper::getJenisPenilaianCode($jenisPenilaian) ?></span>
            <h3>Audit Mutu Internal - <?= $amiUnit->unit->nama ?> </h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <br>

            <div class="x_content">

                <div class="col-md-6 col-lg-6 col-xs-12">
                    <input type="hidden" name="ami_unit_id" id="ami_unit_id" value="<?= $id ?>">


                    <div class="form-group">
                        <label class="control-label ">Kriteria Standar Mutu</label>
                        <?= Select2::widget([
                            'name' => 'filter_kriteria_id',
                            'data' => ArrayHelper::map($filterKriteria, 'id', function ($data) {
                                $res = '';
                                if ($data->is_khusus) {
                                    $res .= '[KHUSUS] ';
                                } else {
                                    $res .= '[UMUM] ';
                                }
                                $res .= $data->nama;
                                $res .= !empty($data->keterangan) ? ' - ' . $data->keterangan : '';
                                return $res;
                            }),
                            'value' => $_GET['ami_id'] ?? [],
                            'options' => [
                                'placeholder' => Yii::t('app', 'Pilih Kriteria'),
                                'id' => 'filter_kriteria_id',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]) ?>
                    </div>

                    <div class="form-group clearfix">
                        <button class="btn btn-primary" id="btn-filter-standar"><i class="fa fa-search"></i>
                            Cari</button>
                    </div>
                </div>

                <div class="panel-body col-md-12">

                    <h4><strong>Penjelasan Indikator:</strong></h4>
                    <ul>
                        <li>❌ : tidak ada</li>
                        <li>⚠️ : belum lengkap</li>
                        <li>✅ : lengkap</li>
                        <?php if (Yii::$app->user->can('auditor')) : ?>


                            <li class="text-info">
                                <div class="label label-info">
                                    -
                                </div> : Terdapat tilik
                            </li>
                        <?php endif; ?>
                    </ul>


                    <div class="container">
                        <div class="content">
                            <div class="panel-group" id="dynamicPanels"></div>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </div>
</div>



<!-- Modal Input AK -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Asesmen Kecukupan</h3>',
    'id' => 'modal-ak',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-ak">
    <div class="form-group">
        <label for="">Nama Standar</label>
        <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_ak_id']) ?>
        <?= Html::hiddenInput('unit_id', '', ['class' => 'form-control', 'id' => 'indikator_ak_unit']) ?>
        <?= Html::textInput('indikator_name', '', ['class' => 'form-control', 'id' => 'indikator_ak_name', 'disabled' => true]) ?>

    </div>

    <div class="form-group">
        <label for="">Evaluasi Diri</label>
        <?= Html::dropDownList('skor_ed', '', \app\helpers\MyHelper::getStatusDokumenProdiFakultas(), ['class' => 'form-control', 'id' => 'indikator_ak_skor_ed', 'disabled' => true]) ?>
    </div>

    <div class="form-group">
        <label for="">Asesmen Kecukupan</label>
        <?= Html::radioList('skor_ak', '', \app\helpers\MyHelper::getStatusDokumenProdiFakultas()) ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-input-ak']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>



<!-- Modal Input AL -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Asesmen Lapangan</h3>',
    'id' => 'modal-al',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-al">
    <div class="form-group">
        <label for="">Nama Standar</label>
        <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_al_id']) ?>
        <?= Html::hiddenInput('unit_id', '', ['class' => 'form-control', 'id' => 'indikator_al_unit']) ?>
        <?= Html::textInput('indikator_name', '', ['class' => 'form-control', 'id' => 'indikator_al_name', 'disabled' => true]) ?>

    </div>


    <div class="form-group">
        <label for="">Evaluasi Diri</label>
        <?= Html::dropDownList('skor_ed', '', \app\helpers\MyHelper::getStatusDokumenProdiFakultas(), ['class' => 'form-control', 'id' => 'indikator_al_skor_ed', 'disabled' => true]) ?>
    </div>

    <div class="form-group">
        <label for="">Asesmen Lapangan</label>
        <?= Html::radioList('skor_al', '', \app\helpers\MyHelper::getStatusDokumenProdiFakultas()) ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-input-al']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>


<!-- Modal Tilik/Pertanyaan -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h3>Input Tilik / Pertanyaan</h3>',
    'id' => 'modal-tilik',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-tilik">
    <div class="form-group">
        <label for="">Nama Standar</label>
        <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'indikator_tilik_ami_unit_id']) ?>
        <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_tilik_id']) ?>
        <?= Html::textInput('indikator_nama', '', ['class' => 'form-control', 'id' => 'indikator_tilik_nama', 'disabled' => true]) ?>

    </div>

    <div class="form-group">
        <label for="">Pertanyaan/Catatan</label>
        <?= Html::textArea('pertanyaan', '', ['class' => 'form-control', 'id' => 'tilik_pertanyaan']); ?>
    </div>

    <div class="form-group">
        <label for="">Referensi</label>
        <?= Html::textArea('referensi', '', ['class' => 'form-control', 'id' => 'tilik_referensi']); ?>
    </div>

    <div class="form-group">
        <label for="">Bukti</label>
        <?= Html::textArea('bukti', '', ['class' => 'form-control', 'id' => 'tilik_bukti']); ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-input-tilik']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<!-- Modal Temuan -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header'        => '<h3><strong>Buat Temuan</strong></h3>',
    'options'       => [
        'class' => 'extra-super-large-modal'
    ],
    'id'            => 'modal-temuan',
    'size'          => 'modal-lg',

    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => true
    ]
]);
?>

<form action="" id="form-temuan">
    <div class="form-group">
        <label for="">Nama Standar</label>
        <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'indikator_temuan_ami_unit_id']) ?>
        <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_temuan_id']) ?>
        <?= Html::textInput('indikator_name', '', ['class' => 'form-control', 'id' => 'indikator_temuan_name', 'disabled' => true]) ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Kriteria</label>
                <?= Html::textInput('kriteria_temuan', '', ['class' => 'form-control', 'id' => 'indikator_temuan_kriteria', "placeholder" => "Masukkan kriteria", 'disabled' => true])  ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Uraian Ketidak Sesuaian</label>
                <?= Html::textArea('uraian_k', '', ['class' => 'form-control', 'id' => 'indikator_temuan_pertanyaan', "placeholder" => "Masukkan uraian ketidaksesuaian"]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Akar Penyebab</label>
                <?= Html::textArea('akar_penyebab', '', ['class' => 'form-control', 'id' => 'indikator_temuan_pertanyaan', "placeholder" => "Masukkan akar penyebab"]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Referensi</label>
                <?= Html::textArea('referensi', '', ['class' => 'form-control', 'id' => 'indikator_temuan_referensi', "placeholder" => "Masukkan referensi"]); ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="">Rekomendasi</label>
                <?= Html::textArea('rekomendasi', '', ['class' => 'form-control', 'id' => 'indikator_temuan_rekomendasi', "placeholder" => "Masukkan rekomendasi"]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Jadwal Perbaikan</label>
                <?= Html::textInput('jadwal_perbaikan', '', ['class' => 'form-control', 'id' => 'indikator_temuan_jadwal_perbaikan', "placeholder" => "Masukkan jadwal perbaikan"])  ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">kategori</label>
                <?= Html::dropDownList('kategori', '', \app\helpers\MyHelper::getKategoriKts(), ['class' => 'form-control', 'id' => 'indikator_temuan_kategori', 'prompt' => 'Pilih kategori']) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Jabatan Penanggung Jawab</label>
                <?= Html::textInput('jabatan_pj', '', ['class' => 'form-control', "placeholder" => "Masukkan jabatan penanggung jawab"])  ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Penanggung Jawab</label>
                <?= Html::textInput('penanggung_jawab', '', ['class' => 'form-control penanggung_jawab', "placeholder" => "Masukkan nama penanggung jawab"])  ?>
            </div>
        </div>
        <!-- <div class="col-md-6">
            <div class="form-group">
                <label for="">Temuan</label>
                <?php Html::textArea('temuan', '', ['class' => 'form-control', 'id' => 'indikator_temuan', "placeholder" => "Masukkan temuan"])  ?>
            </div>
        </div> -->
        <!-- <div class="col-md-6">
            <div class="form-group">
                <label for="">Ruang Lingkup</label>
                <?php Html::textInput('ruang_lingkup', '', ['class' => 'form-control', 'id' => 'indikator_temuan_skor_ed', "placeholder" => "Masukkan ruang lingkup"])  ?>
            </div>
        </div> -->
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-input-temuan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    function readMore(text, limit) {
        if (text.length > limit) {
            return text.substring(0, limit) + "...";
        } else {
            return text;
        }
    }

    function statusSign() {
        role = {
            '0': '❌',
            '1': '⚠️',
            '2': '✅',
        }
        return role;
    }

    function convertSkor(nilai) {
        var getStatusDokumenSatker = <?= json_encode(MyHelper::getStatusDokumenSatker()) ?>;
        var getStatusDokumenProdiFakultas = <?= json_encode(MyHelper::getStatusDokumenProdiFakultas()) ?>;

        if (!nilai) return '-';
        else if (nilai > 99) return getStatusDokumenSatker[nilai] || nilai;
        else if (nilai > 9) return getStatusDokumenProdiFakultas[nilai] || nilai;

        return nilai;
    }

    // standar manipusi html
    function detailStandar(data) {
        var html = `<div class="col-md-6">`;
        html += `<h4><strong>Nama Standar</strong></h4>`;
        html += `<p>${data.nama}</p>`;
        html += `<br>`;
        html += ``;
        html += `<h4><strong>Pernyataan</strong></h4>`;
        html += `<p>${data.pernyataan}</p>`;
        html += `<br>`;
        html += ``;
        html += `<h4><strong>Indikator</strong></h4>`;
        html += `<p>${data.indikator}</p>`;
        html += `<br>`;
        html += ``;
        html += `<h4><strong>Referensi</strong></h4>`;
        html += `<p>${data.referensi}</p>`;
        html += `<br>`;
        html += ``;
        html += `<h4><strong>Strategi</strong></h4>`;
        html += `<p>${data.strategi}</p>`;
        html += `<br>`;
        html += ``;
        html += `</div>`;

        return html;
    }

    function detailPenilaian(data) {
        var jenisPenilaian = $("#jenisPenilaian").val();
        var isAuditee = (`<?= Yii::$app->user->can('auditee') ?>` == `1` ? true : false);
        var isAuditor = (`<?= Yii::$app->user->can('auditor') ?>` == `1` ? true : false);

        var html = '<div class="col-md-6">';
        html += '<h4><strong>Dokumen Tunggal</strong></h4>';
        html += '<h5>';
        html += '<strong>Evaluasi Diri: ' + (data.skor_ed ? convertSkor(data.skor_ed) : 'Belum dievaluasi') + '</strong>';
        html += '</h5>';
        html += `<table width="100%" id="tabel-dokumen-${data.id}">`;
        if (data.dokumen.length != 0) {
            $.each(data.dokumen, function(i, dokumen) {
                html += '<tr>';
                html += '<td>' + dokumen.nama + '</td>';
                html += '<td>';
                html += '<a href="/dokumen/download?id=' + dokumen.id + '" class="btn btn-info btn-sm" target="_blank">';
                html += '<i class="fa fa-file-pdf-o"></i> lihat';
                html += '</a>';

                if (isAuditee) {
                    html += `<a href="javascript:void(0)" data-asesmen-dokumen-id="${dokumen.asesmen_dokumen_id}" class="btn btn-danger btn-sm btn-delete-dokumen">`;
                    html += '<i class="fa fa-trash"></i>';
                    html += '</a>';
                }
            })

            html += '</td>';
            html += '</tr>';
        } else {
            html += 'dokumen tidak tersedia';
        }
        html += '</table>';
        html += '<hr>';


        if (isAuditee && jenisPenilaian == 'led') {
            html += '<h4><strong>Input/Update Bukti & Evaluasi Diri</strong></h4>';
            html += '<h5><strong>Dokumen Bukti</strong></h5>';

            html += '<div class="row">';
            html += '<div class="col-md-10">';
            html += `<input type="hidden" name="dokumen_bukti_id" id="dokumen-bukti-${data.id}">`;
            html += `<input class="form-control dokumen_bukti" type="text" id="text-dokumen-${data.id}" name="nama_dokumen" data-indikator-id="${data.id}" placeholder="Ketik nama dokumen">`;
            html += '</div>';
            html += '<div class="col-md-2">';
            html += `<button class="btn btn-md btn-success btn-add-dokumen" data-indikator-id="${data.id}"><i class="fa fa-plus"></i></button>`;
            html += '</div>';
            html += '</div>';

            html += '<br>';
            html += `<table width="100%">`;
            html += '<tr>';
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ed == 10? 'checked' : '')} value="10" type="radio" name="ed-${data.id}" id="ed-${data.id}-1"><label for="ed-${data.id}-1"><span>Belum memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ed == 11? 'checked' : '')} value="11" type="radio" name="ed-${data.id}" id="ed-${data.id}-2"><label for="ed-${data.id}-2"><span>Memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ed == 12? 'checked' : '')} value="12" type="radio" name="ed-${data.id}" id="ed-${data.id}-3"><label for="ed-${data.id}-3"><span>Melampaui</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ed == 13? 'checked' : '')} value="13" type="radio" name="ed-${data.id}" id="ed-${data.id}-4"><label for="ed-${data.id}-4"><span>Menyimpang</span></label></td>`;
            html += '</tr>';
            html += '</table>';

            html += '<br>';
            html += `<button class="btn btn-sm btn-primary btn-save-ed" data-indikator-id="${data.id}"><i class="fa fa-save"></i> Simpan</button>`;
            html += '<hr>';
        }

        if (isAuditor && jenisPenilaian == 'ak') {
            html += '<h4><strong>Input/Update Skor Asesmen Kecukupan</strong></h4>';


            html += '<br>';
            html += '<table width="100%">';
            html += '<tr>';
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ak == 10? 'checked' : '')} value="10" type="radio" name="ak-${data.id}" id="ak-${data.id}-1"><label for="ak-${data.id}-1"><span>Belum memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ak == 11? 'checked' : '')} value="11" type="radio" name="ak-${data.id}" id="ak-${data.id}-2"><label for="ak-${data.id}-2"><span>Memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ak == 12? 'checked' : '')} value="12" type="radio" name="ak-${data.id}" id="ak-${data.id}-3"><label for="ak-${data.id}-3"><span>Melampaui</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_ak == 13? 'checked' : '')} value="13" type="radio" name="ak-${data.id}" id="ak-${data.id}-4"><label for="ak-${data.id}-4"><span>Menyimpang</span></label></td>`;
            html += '</tr>';
            html += '</table>';

            html += '<br>';
            html += `<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-save-ak" data-indikator-id="${data.id}"><i class="fa fa-save"></i> Simpan</a>`;
            html += '<hr>';
            html += '<h4><strong>Tilik</strong></h4>';

            html += '<br>';
            html += `<button class="btn btn-sm btn-primary" data="${data.id}" data-indikator="${data.id}" data-nindikator="${data.nama}" data-ami_unit_id="${data.ami_unit_id}" id="btn-tilik"><i class="fa fa-plus"></i> tambah</button>`;
            html += `<a href="/tilik/index?id=${data.ami_unit_id}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-database"></i> Daftar tilik</a>`;
        }
        if (isAuditor && jenisPenilaian == 'al') {
            html += '<h4><strong>Input/Update Skor Asesmen Lapangan</strong></h4>';

            html += '<br>';
            html += '<table width="100%">';
            html += '<tr>';
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_al == 10? 'checked' : '')} value="10" type="radio" name="al-${data.id}" id="al-${data.id}-1"><label for="al-${data.id}-1"><span>Belum memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_al == 11? 'checked' : '')} value="11" type="radio" name="al-${data.id}" id="al-${data.id}-2"><label for="al-${data.id}-2"><span>Memenuhi</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_al == 12? 'checked' : '')} value="12" type="radio" name="al-${data.id}" id="al-${data.id}-3"><label for="al-${data.id}-3"><span>Melampaui</span></label></td>`;
            html += `<td width="25%" style="text-align: center;"><input ${(data.skor_al == 13? 'checked' : '')} value="13" type="radio" name="al-${data.id}" id="al-${data.id}-4"><label for="al-${data.id}-4"><span>Menyimpang</span></label></td>`;
            html += '</tr>';
            html += '</table>';

            html += '<br>';
            html += `<button class="btn btn-sm btn-primary btn-save-al" data-indikator-id="${data.id}"><i class="fa fa-save"></i> Simpan</button>`;
            html += '<hr>';
            html += '<h4><strong>Temuan</strong></h4>';

            html += '<br>';
            html += `<button class="btn btn-sm btn-primary" id="btn-temuan" data="${data.id}" data-kriteria="${data.kriteria_nama}" data-indikator="${data.id}" data-nindikator="${data.nama}" data-ami_unit_id="${data.ami_unit_id}" id="btn-tilik"><i class="fa fa-plus"></i> tambah</button>`;
            html += `<a href="/temuan/index?id=${data.ami_unit_id}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-database"></i> Daftar temuan</a>`;

        }

        if (isAuditor) {
            html += '<hr>';

            html += '<h4><strong>Daftar Tilik</strong></h4>';
            html += '<table width="100%" class="">';
            html += '<tr>';
            html += `<td width="5%" style="text-align: center;vertical-align: middle;"><p>No</p></td>`;
            html += `<td width="45%" style="vertical-align: middle;"><p> Pertanyaan/Catatan</p></td>`;
            html += `<td width="25%" style="vertical-align: middle;"><p>Referensi</p></td>`;
            html += `<td width="25%" style="vertical-align: middle;"><p>Bukti</p></td>`;
            html += '</tr>';
            if (data.dataTilik && Object.keys(data.dataTilik).length > 0) {
                $.each(data.dataTilik, function(i, object) {
                    html += '<tr>';
                    html += `<td style="text-align: center;vertical-align: middle;"><p>${i + 1}.</p></td>`;
                    html += `<td style="vertical-align: middle;"> <p>${object.pertanyaan}</p></td>`;
                    html += `<td style="vertical-align: middle;"> <p>${object.referensi}</p></td>`;
                    html += `<td style="vertical-align: middle;"> <p>${object.bukti}</p></td>`;
                    html += '</tr>';
                });
            } else {
                html += '<tr>';
                html += '<td colspan="4" style="text-align: center;">Tilik tidak tersedia</td>';
                html += '</tr>';
            }

            html += '</table>';
        }
        html += '</div>';

        return html;
    }

    function panelThead(kriteria) {

        var html = '<div class="panel panel-default data-thead"><div class="panel-heading"><h3 class="panel-title" style="text-align: center;">';
        html += `<td width="3%" style="text-align: center;">`;
        html += `<strong>${kriteria.nama.toUpperCase()}${kriteria.keterangan ? ' - ' + kriteria.keterangan : ''}</strong>`;
        html += `</h3></div></div>`;
        html += ``;
        html += `<div class="panel panel-default data-thead"><div class="panel-heading"><table width="100%" style="padding: 4px;"><tr>`;
        html += `<td width="3%"><h4 class="panel-title"><strong>No</strong></h4></td>`;
        html += ``;
        html += `<td width="19%" style="text-align: center;"><h4 class="panel-title"><strong>Referensi</strong></h4></td>`;
        html += `<td width="65%" style="text-align: center;"><h4 class="panel-title"><strong>Standar Mutu - Indikator</strong></h4></td>`;
        html += `<td width="6%" style="text-align: center;"><h4 class="panel-title"><strong>Dokumen</strong></h4></td>`;
        html += `<td width="3%" style="text-align: center;"><h4 class="panel-title"><strong>ED</strong></h4></td>`;
        html += `<td width="3%" style="text-align: center;"><h4 class="panel-title"><strong>AK</strong></h4></td>`;
        html += `<td width="3%" style="text-align: center;"><h4 class="panel-title"><strong>AL</strong></h4></td>`;
        html += ``;
        html += `</tr></table></div></div>`;
        html += `<br>`;
        return html;
    }

    function panelTitle(i, data) {
        var html = '<tr>';
        html += `<td width="3%" style="text-align: center;">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}"><strong>${i+1}.</strong></a>`;
        html += `</h4></td>`;
        html += ``;
        html += `<td width="19%" style="text-align: center;">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}"><strong>${data.referensi}</strong></a>`;
        html += `</h4></td>`;
        html += ``;
        html += `<td width="65%">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}">${readMore(data.indikator, 115)}</a>`;
        html += `</h4></td>`;
        html += ``;
        html += `<td width="6%" style="text-align: center;">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}"><strong>${statusSign()[data.statusDokumen ?? 0]}</strong></a>`;
        html += `</h4></td>`;
        html += ``;
        html += `<td width="3%" style="text-align: center;">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}"><strong>${statusSign()[data.status_ed ?? 0]}</strong></a>`;
        html += `</h4></td>`;
        html += ``;
        html += `<td width="3%" style="text-align: center;">`;
        html += `<h4 class="panel-title">`;
        html += `<a data-toggle="collapse" data-parent="#dynamicPanels" href="#collapse${data.id}"><strong>${statusSign()[data.status_ak ?? 0]}</strong></a>`;
        html += `</h4></td>`;
        html += ``;
        html += '<td width="3%" style="text-align: center;">';
        html += '<h4 class="panel-title">';
        html += '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + (data.id) + '"><strong>' + (statusSign()[data.status_al ?? 0]) + '</strong></a>';
        html += '</h4></td></tr>';
        html += '';

        return html;
    }

    function searchStandarAsesmen() {

        var obj = {
            filter_kriteria_id: $('#filter_kriteria_id').val(),
            ami_unit_id: $('#ami_unit_id').val()
        };

        $.ajax({
            url: "/indikator/ajax-get-standar-by-kriteria",
            type: "POST",
            data: obj,
            error: function(e) {
                console.log(e.responseText);
            },
            success: function(data) {
                var parsedData = $.parseJSON(data);

                var isAuditor = (`<?= Yii::$app->user->can('auditor') ?>` == `1` ? true : false);
                $('#dynamicPanels').append(panelThead(parsedData['kriteria']));

                $.each(parsedData['standar'], function(i, object) {

                    if (object.statusTilik == 1 && isAuditor) {
                        var panelWarna = "panel-info";
                    } else {
                        var panelWarna = "";
                    }
                    var panel = $(`<div class="panel panel-default ${panelWarna} data-value"></div>`);
                    var panelHeading = $('<div class="panel-heading"></div>');
                    var tableTitle = $('<table width="100%" style="padding: 4px;"></table>');
                    var panelBody = $('<div id="collapse' + (object.id) + '" class="panel-collapse-standar collapse"></div>');

                    panelHeading.append(tableTitle.append(panelTitle(i, object)));
                    panel.append(panelHeading);
                    panel.append(panelBody);

                    $('#dynamicPanels').append(panel);
                });

                // Add event handler for dynamically created elements
                $('[data-toggle="collapse"]').unbind().click(function() {
                    $($(this).attr('href')).collapse('toggle');
                });
            }
        });
    }

    $(document).ready(function() {
        $('#btn-filter-standar').click(function() {
            $('.data-value').remove();
            $('.data-thead').remove();

            searchStandarAsesmen();
        });

        $('#dynamicPanels').on('show.bs.collapse', '.panel-collapse-standar', function() {
            var idStandar = $(this).attr('id').replace('collapse', '');
            var panelBody = $('<div class="panel-body"></div>');
            var collapseElement = $(this);

            var obj = {
                indikator_id: idStandar,
                ami_unit_id: $('#ami_unit_id').val()
            };

            $.ajax({
                url: "/asesmen/ajax-get-detail-asesmen",
                type: "POST",
                data: obj,
                error: function(e) {
                    console.log(e.responseText);
                },
                beforeSend: function() {
                    Swal.fire({
                        title: "Please wait",
                        html: "Processing your request..",
                        showConfirmButton: false, 
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },

                    })
                },
                success: function(data) {

                    var parsedData = $.parseJSON(data);

                    panelBody.append(detailStandar(parsedData));
                    panelBody.append(detailPenilaian(parsedData));

                    collapseElement.html(panelBody);

                    Swal.close()
                }
            });
        });

        $(document).bind("keyup.autocomplete", function() {
            var indikatorId; // Pindahkan deklarasi ke luar agar dapat diakses oleh fungsi lain

            $('.dokumen_bukti').autocomplete({
                minLength: 2,
                select: function(event, ui) {
                    $(this).next().val(ui.item.id);
                    $("#dokumen-bukti-" + indikatorId).val(ui.item.id);
                },
                focus: function(event, ui) {
                    $(this).next().val(ui.item.id);
                    $("#dokumen-bukti-" + indikatorId).val(ui.item.id);
                },
                source: function(request, response) {
                    indikatorId = $(this.element).data('indikator-id'); // Tetap di sini agar diakses oleh fungsi lain

                    setTimeout(function() {
                        $.ajax({
                            url: "/dokumen/ajax-get-dokumen-asesmen",
                            dataType: "json",
                            data: {
                                term: request.term,
                            },
                            success: function(data) {
                                response(data);
                            }
                        });
                    }, 500);
                },
            });
        });

    });

    $(document).on("click", ".btn-add-dokumen", function(e) {

        indikator_id = $(this).data('indikator-id');
        ami_unit_id = $('#amiUnitId').val();
        dokumen_id = $(`#dokumen-bukti-${indikator_id}`).val();

        if (indikator_id && ami_unit_id && dokumen_id) {
            $.ajax({
                url: "/asesmen-dokumen/ajax-add-dokumen",
                type: "POST",
                async: true,
                data: {
                    indikator_id,
                    ami_unit_id,
                    dokumen_id
                },
                error: function(e) {
                    console.log(e.responseText)
                },
                beforeSend: function() {
                    Swal.showLoading()
                },
                success: function(data) {
                    Swal.close()
                    var hasil = $.parseJSON(data)

                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        }).then(res => {

                            var html = '';
                            html += '<tr>';
                            html += '<td>' + hasil.data.nama + '</td>';
                            html += '<td>';
                            html += '<a href="/dokumen/download?id=' + hasil.data.id + '" class="btn btn-info btn-sm" target="_blank">';
                            html += '<i class="fa fa-file-pdf-o"></i> lihat';
                            html += '</a>';

                            var isAuditee = (`<?= Yii::$app->user->can('auditee') ?>` == `1` ? true : false);

                            if (isAuditee) {
                                html += `<a href="javascript:void(0)" data-asesmen-dokumen-id="${hasil.data.asesmen_dokumen_id}" class="btn btn-danger btn-sm btn-delete-dokumen">`;
                                html += '<i class="fa fa-trash"></i>';
                                html += '</a>';
                            }
                            html += '</td>';
                            html += '<tr>';

                            $(`#tabel-dokumen-${indikator_id}`).append(html);
                            $(`#text-dokumen-${indikator_id}`).val('');


                        });
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        })
                    }
                }
            })
        }
    });

    $(document).on('click', '.btn-delete-dokumen', function() {
        var asesmen_dokumen_id = $(this).data('asesmen-dokumen-id');

        // Tampilkan konfirmasi SweetAlert
        Swal.fire({
            title: "Perhatian!",
            html: "Anda yakin menghapus dokumen bukti ini dari standar?, <br> Menghapus bukti menghilangkan penilaian auditor!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete.isConfirmed) {
                console.log(asesmen_dokumen_id);
                $.ajax({
                    url: "/asesmen-dokumen/delete-by-id",
                    type: "POST",
                    data: {
                        id: asesmen_dokumen_id
                    },
                    success: function(response) {
                        Swal.close()
                        var data = $.parseJSON(response)
                        if (data.code == 200) {
                            Swal.fire({
                                title: 'Yeay!',
                                icon: 'success',
                                text: data.message
                            }).then(res => {
                                $(`.btn-delete-dokumen[data-asesmen-dokumen-id="${asesmen_dokumen_id}"]`).closest('tr').remove();
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: data.message
                            })
                        }
                    },
                    error: function() {
                        // Tampilkan notifikasi SweetAlert gagal jika diperlukan
                        swal("Gagal!", "Terjadi kesalahan saat menghapus.", "error");
                    }
                });
            }
        });
    });

    $(document).on("click", ".btn-save-ed", function(e) {

        indikator_id = $(this).data('indikator-id');
        ami_unit_id = $('#amiUnitId').val();
        skor_ed = $(`input[name="ed-${indikator_id}"]:checked`).val();

        if (indikator_id && ami_unit_id && skor_ed) {
            $.ajax({
                url: "/asesmen/ajax-save-skored",
                type: "POST",
                async: true,
                data: {
                    indikator_id,
                    ami_unit_id,
                    skor_ed
                },
                error: function(e) {
                    console.log(e.responseText)
                },
                beforeSend: function() {
                    Swal.showLoading()
                },
                success: function(data) {
                    Swal.close()
                    var hasil = $.parseJSON(data)

                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        });
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        })
                    }
                }
            })
        }
    });

    $(document).on("click", ".btn-save-ak", function(e) {

        indikator_id = $(this).data('indikator-id');
        ami_unit_id = $('#amiUnitId').val();
        skor_ak = $(`input[name="ak-${indikator_id}"]:checked`).val();

        if (indikator_id && ami_unit_id && skor_ak) {
            $.ajax({
                url: "/asesmen/ajax-save-skorak",
                type: "POST",
                async: true,
                data: {
                    indikator_id,
                    ami_unit_id,
                    skor_ak
                },
                error: function(e) {
                    console.log(e.responseText)
                },
                beforeSend: function() {
                    Swal.showLoading()
                },
                success: function(data) {
                    Swal.close()
                    var hasil = $.parseJSON(data)

                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        });
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        })
                    }
                }
            })
        }
    });

    $(document).on("click", ".btn-save-al", function(e) {

        indikator_id = $(this).data('indikator-id');
        ami_unit_id = $('#amiUnitId').val();
        skor_al = $(`input[name="al-${indikator_id}"]:checked`).val();

        if (indikator_id && ami_unit_id && skor_al) {
            $.ajax({
                url: "/asesmen/ajax-save-skoral",
                type: "POST",
                async: true,
                data: {
                    indikator_id,
                    ami_unit_id,
                    skor_al
                },
                error: function(e) {
                    console.log(e.responseText)
                },
                beforeSend: function() {
                    Swal.showLoading()
                },
                success: function(data) {
                    Swal.close()
                    var hasil = $.parseJSON(data)

                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        });
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        })
                    }
                }
            })
        }
    });




    // ===== //
    $(document).bind("keyup.autocomplete", function() {

        $('.penanggung_jawab').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy_auditee").val(ui.item.items.NIY)
                $("#nidn_auditee").val(ui.item.items.NIDN)
                $("#email_auditee").val(ui.item.items.email)
            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy_auditee").val(ui.item.items.NIY)
                $("#nidn_auditee").val(ui.item.items.NIDN)
                $("#email_auditee").val(ui.item.items.email)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).on("click", "#btn-input-tilik", function(e) {
        e.preventDefault();

        var obj = $("#form-tilik").serialize()


        $.ajax({
            url: "/asesmen/ajax-input-tilik",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-tilik").modal("hide");
                        $("#modal-tilik textarea").val("");
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-input-temuan", function(e) {
        e.preventDefault();

        var obj = $("#form-temuan").serialize()


        $.ajax({
            url: "/asesmen/ajax-input-temuan",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-temuan").modal("hide")
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });


    // Modal ED

    $(document).on("click", ".btn-add", function(e) {
        e.preventDefault();
        var indikator_id = $(this).data("indikator")
        var ami_unit_id = $(this).data("amiunitid")

        $("#indikator_id").val(indikator_id).focus()
        $(".dokumen_kelompok").hide()
        $("#dokumen-modal").modal("show")

    });

    // Modal AK

    $(document).on("click", ".btn-ak", function(e) {
        e.preventDefault();
        var indikator_id = $(this).data("indikator")
        var indikator_name = $(this).data("nindikator")
        var skor_ed = $(this).data("ed")
        var unit = $(this).data("unit")


        $("#indikator_ak_id").val(indikator_id).focus()
        $("#indikator_ak_name").val(indikator_name).focus()
        $("#indikator_ak_skor_ed").val(skor_ed)
        $("#indikator_ak_unit").val(unit)
        $("#modal-ak").modal("show")

    });


    // Modal AL

    $(document).on("click", "#btn-al", function(e) {
        e.preventDefault();
        var indikator_id = $(this).data("indikator")
        var indikator_name = $(this).data("nindikator")
        var skor_ed = $(this).data("ed")
        var unit = $(this).data("unit")


        $("#indikator_al_id").val(indikator_id).focus()
        $("#indikator_al_name").val(indikator_name).focus()
        $("#indikator_al_skor_ed").val(skor_ed)
        $("#indikator_al_unit").val(unit)
        $("#modal-al").modal("show")

    });

    // Modal Tilik

    $(document).on("click", "#btn-tilik", function(e) {
        e.preventDefault();

        var indikator_id = $(this).data("indikator")
        var indikator_nama = $(this).data("nindikator")
        var ami_unit_id = $(this).data("ami_unit_id")

        $("#indikator_tilik_id").val(indikator_id).focus()
        $("#indikator_tilik_nama").val(indikator_nama).focus()
        $("#indikator_tilik_ami_unit_id").val(ami_unit_id)

        $("#modal-tilik").modal("show")

    });

    // Modal Temuan
    $(document).on("click", "#btn-temuan", function(e) {
        e.preventDefault();
        var indikator_id = $(this).data("indikator")
        var indikator_name = $(this).data("nindikator")
        var ami_unit_id = $(this).data("ami_unit_id")
        var kriteria = $(this).data("kriteria")

        $("#indikator_temuan_id").val(indikator_id)
        $("#indikator_temuan_name").val(indikator_name)
        $("#indikator_temuan_ami_unit_id").val(ami_unit_id)
        $("#indikator_temuan_kriteria").val(kriteria)
        $("#modal-temuan").modal("show")

    });


    $(document).on("click", ".btn-tunggal", function(e) {
        e.preventDefault();

        $("#radio-kelompok").val("")
        $(".dokumen_kelompok").hide()
        $(".dokumen_tunggal").show()

    });

    $(document).on("click", ".btn-kelompok", function(e) {
        e.preventDefault();

        $("#radio-tunggal").val("")
        $(".dokumen_tunggal").hide()
        $(".dokumen_kelompok").show()

    });

    $(document).on("click", ".set-kelompok-hide", function(e) {
        e.preventDefault();

        $(".dokumen_kelompok").hide()

    });
</script>
<?php JSRegister::end(); ?>