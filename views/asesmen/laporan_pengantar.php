<?php

use app\helpers\MyHelper;
use app\models\JenjangMap;

?>

<table border="0" width="100%" style="padding: 4px;">

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td width="10%"></td>
        <td width="80%" style="text-align: center;">
            <div style="font-size: 1.6em;">Kata Pengantar</div>
        </td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <div style="text-align: justify;text-justify: inter-word;font-size: 1.2em;line-height: 1.8;">
                Puji syukur kepada Allah SWT yang senantiasa melimpahkan rahmat-Nya berupa
                kekuatan, kesehatan dan kemudahan bagi kita semua dalam melaksanakan siklus SPMI melalui
                kegiatan audit mutu internal pada tahun akademik <?= $amiUnit->ami->tahun ?>/<?= $amiUnit->ami->tahun + 1 ?> 
                dapat terlaksana dengan baik.
                Laporan pelaksanaan Audit Mutu Internal ini disusun oleh para auditor berdasarkan pada hasil
                asessmen lapangan yang telah dilaksanakan bersama auditee. Laporan audit mutu internal ini
                akan dijadikan sebagai sarana peningkatan kepatuhan terhadap standar yang ditetapkan. Hasil
                ini juga akan dijadikan informasi untuk menyusun kebijakan universitas dalam pencapaian visi
                misi di Universitas Darussalam Gontor. Demikianlah laporan ini disusun untuk disajikan sebagai
                dokumen pelaksanaan audit mutu internal. Ucapan terimakasih kami sampaikan pada seluruh
                pihak yang telah membantu terlaksananya audit mutu internal di Universitas Darussalam Gontor.
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td width="50%"></td>
                    <td style="font-size: 1.2em">Ponorogo, <?= MyHelper::convertTanggalIndo(date("Y-m-d")); ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-size: 1.2em"><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? ''; ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>


</table>