<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Asesmen */

$this->title = 'Update Asesmen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asesmens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'id_indikator' => $id_indikator,
        'status' => $status
    ]) ?>
           </div>
        </div>
    </div>
</div>
