<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AsesmenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asesmen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ami_unit_id') ?>

    <?= $form->field($model, 'indikator_id') ?>

    <?= $form->field($model, 'link_bukti') ?>

    <?= $form->field($model, 'skor_ed') ?>

    <?php // echo $form->field($model, 'skor_ak1') ?>

    <?php // echo $form->field($model, 'skor_ak2') ?>

    <?php // echo $form->field($model, 'skor_ak3') ?>

    <?php // echo $form->field($model, 'skor_al1') ?>

    <?php // echo $form->field($model, 'skor_al2') ?>

    <?php // echo $form->field($model, 'skor_al3') ?>

    <?php // echo $form->field($model, 't_auditor') ?>
    
    <?php // echo $form->field($model, 'ami_unit_id') ?> 
		 
    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'dokumen_id') ?> 

    <?php // echo $form->field($model, 'pengelompokan_id') ?> 

    <?php // echo $form->field($model, 'jenis_dokumen') ?> 

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
