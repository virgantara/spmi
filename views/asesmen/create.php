<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Asesmen */

$this->title = 'Create Asesmen';
$this->params['breadcrumbs'][] = ['label' => 'Asesmens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// $id = 2;
// $status = 'led';
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



    <?= $this->render('_form', [
        'model' => $model,
        'id_indikator' => $id,
        'status' => $status
    ]) ?>
    	   </div>
        </div>
    </div>
</div>