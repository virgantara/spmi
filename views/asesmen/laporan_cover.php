<?php

use app\helpers\MyHelper;
use app\models\JenjangMap;

?>

<table border="0" width="100%" style="padding: 4px;">

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="70%" style="text-align: center;">
            <span style="font-size: 1.6em;"><strong>LAPORAN AUDIT MUTU INTERNAL</strong></span>
        </td>
        <td width="15%"></td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: center;">
            <span style="font-size: 1.2em"><strong><?= strtoupper($amiUnit->unit->nama) ?> (<?= strtoupper($amiUnit->unit->singkatan) ?>)</strong></span>
        </td>
        <td></td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td>
            <table border="1" style="padding: 4px;">
                <tr>
                    <th width="40%">Nomor Dokumen</th>
                    <td width="60%">: FORM/UNIDA/MUTU-15A</td>
                </tr>
                <tr>
                    <th>Auditee</th>
                    <td>: <?= $amiUnit->unit->nama ?> <?= ($amiUnit->unit->jenis == 'prodi' ? '(' . $jenjang . ')' : '') ?></td>
                </tr>
                <tr>
                    <th>Perwakilan Auditee</th>
                    <td>: <?= $amiUnit->unit->penanggung_jawab ?></td>
                </tr>
                <tr>
                    <th>Ketua Tim Auditor</th>
                    <td>: <?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? ''; ?></td>
                </tr>
                <tr>
                    <th>Anggota Tim Auditor</th>
                    <td>: <?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? ''; ?></td>
                </tr>
                <tr>
                    <th>Sekertaris Tim Auditor</th>
                    <td>: <?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? ''; ?></td>
                </tr>
                <tr>
                    <th>Periode AMI</th>
                    <td>: <?= $amiUnit->ami->nama ?> (<?= $amiUnit->ami->tahun ?> - <?= $amiUnit->ami->tahun + 1 ?>)</td>
                </tr>

            </table>
        </td>
        <td></td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td style="text-align: center;">
            <span style="font-size: 1.6em"><strong>UNIVERSITAS DARUSSALAM GONTOR</strong></span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align: center;">
            <span style="font-size: 1.4em"><strong><?= strtoupper($amiUnit->ami->tahun) ?></strong></span>
        </td>
    </tr>

</table>