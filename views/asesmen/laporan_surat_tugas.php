<?php

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Bpm;

date_default_timezone_set("Asia/Jakarta");
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

?>
<br><br><br><br><br><br><br><br><br><br>
<h3 style="text-align: center;">SURAT TUGAS</h3>
<p style="text-align: center;">Nomor: <?= (isset($ami->surat_tugas) ? $ami->surat_tugas : "-") ?></p>
<table>
    <tr>
        <td width="5%"></td>
        <td colspan="2">Saya yang bertandatangan di bawah ini,</td>
        <td width="5%"></td>
    </tr>
    <tr>
        <td></td>
        <td width="20%">Nama</td>
        <td width="70%">: <?= (isset($ami->ketua_bpm) ? $ami->ketua_bpm : "-") ?></td>
    </tr>
    <tr>
        <td></td>
        <td>NIY</td>
        <td>: <?= (isset($ami->nidn_ketua_bpm) ? $ami->nidn_ketua_bpm : "-") ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Jabatan</td>
        <td>: Ketua Badan Penjaminan Mutu UNIDA Gontor</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">Memberikan tugas kepada:</td>
    </tr>

    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table border="1" style="padding: 6px;">
                <tr>
                    <th width="8%" style="text-align: center;">No</th>
                    <th width="45%" style="text-align: center;">Nama</th>
                    <th width="15%" style="text-align: center;">NIY/NIDN</th>
                    <th width="12%" style="text-align: center;">Jabatan</th>
                    <th width="20%" style="text-align: center;">Auditee</th>
                </tr>
                <?php
                for ($i = 1; $i <= $jumlahAuditor; $i++) :
                    $auditor    = MyHelper::getAuditor($amiUnit->id, $i);
                    $amiAuditor = AmiAuditor::find()->where([
                        'auditor_id'    => $auditor['id'],
                        'ami_id'        => $amiUnit,
                    ])->one();
                ?>
                    <tr>
                        <td style="text-align: center;"><?= $i ?></td>
                        <td><?= $auditor['nama']  ?? '' ?></td>
                        <td><?= (isset($auditor['nidn']) && $auditor['nidn'] != "" ? $auditor['nidn'] : $auditor['niy']) ?></td>
                        <td style="text-align: center;"><?= $amiAuditor->status->nama ?></td>
                        <?php
                        if ($i == 1) :
                        ?>
                            <td style="text-align: center;" rowspan="3"> <?= $amiUnit->unit->nama ?></td>
                        <?php
                        endif;
                        ?>
                    </tr>
                <?php
                endfor;
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <div style="text-align: justify;text-justify: inter-word;line-height: 1.5;">
                Sebagai auditor dalam kegiatan Audit Mutu Internal [AMI] pada Satuan Kerja/Auditee yang dilaksanakan dalam
                rentang tanggal <?= MyHelper::convertTanggalIndo($amiUnit->ami->tanggal_visitasi_mulai); ?> s/d <?= MyHelper::convertTanggalIndo($amiUnit->ami->tanggal_akhir_visitasi); ?>.

            </div>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4">Demikian, surat tugas ini diterbitkan untuk dipergunakan sebagaimana mestinya.</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2">

            <table border="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>Ponorogo, <?= MyHelper::convertTanggalIndo($ami->tanggal_ami_mulai); ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>Ketua BPM,</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td><?= (isset($ami->ketua_bpm) ? $ami->ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td>NIY. <?= (isset($ami->nidn_ketua_bpm) ? $ami->nidn_ketua_bpm : "-") ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td width="70%">Tembusan:</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">1. Rektor Universitas Darussalam Gontor</td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="95%">2. Para Wakil Rektor Universitas Darussalam Gontor</td>
                </tr>
            </table>
        </td>
    </tr>

</table>