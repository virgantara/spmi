<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\UnitKerja;
use Google\Service\CloudSearch\Value;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmiUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Audit Mutu Internal';
$this->params['breadcrumbs'][] = $this->title;

$listPeriode = ArrayHelper::map(Ami::find()->all(), 'id', 'nama');
$listAuditee = ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');

$list_ami = MyHelper::getStatusAmi();

$statusData = ['0' => 'Proses', '1' => 'Selesai'];
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="questionOne">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                                <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                            </a>
                        </h5>
                    </div>
                    <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                        <div class="panel-body">
                            <form action="<?= Yii::$app->urlManager->createUrl(['asesmen/ami']) ?>" method="get">
                                <div class="row">
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label for="">Periode</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-ami_id',
                                                'name' => 'ami_id[]',
                                                'data' => $listPeriode,
                                                'value' => $_GET['ami_id'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Periode')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Auditee</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-unit_id',
                                                'name' => 'unit_id[]',
                                                'data' => $listAuditee,
                                                'value' => $_GET['unit_id'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Auditee')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>

                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= Html::submitButton('Apply Filter', ['class' => 'btn btn-primary']) ?>
                                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-reset', 'type' => 'reset']) ?>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'attribute' => 'ami_id',
                        'vAlign' => 'middle',
                        'label' => 'Periode',
                        'value' => function ($data) {
                            return (!empty($data) ? $data->ami->nama : null);
                        }
                    ],
                    [
                        'label' => 'Auditee',
                        'vAlign' => 'middle',
                        'attribute' => 'unit_id',
                        'value' => function ($data) {
                            return (!empty($data) ? $data->unit->nama : null);
                        }
                    ],
                    [
                        // 'label' => 'Unit',
                        'contentOptions'    => [
                            'width' => '10%',
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'attribute' => 'tanggal_ami',
                        'value' => function ($data) {
                            return (!empty($data) ? MyHelper::convertTanggalIndo($data->tanggal_ami) : null);
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'status_ami',
                        // 'contentOptions'    => [
                        //     'width' => '5%',
                        // ],
                        'format' => 'raw',
                        'filter' => $statusData,
                        'hAlign' => 'center',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'refreshGrid' => true,
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'data' => $statusData,
                        ],
                        'value' => function ($data) use ($list_ami) {

                            $status = 'warning';
                            if ($data->status_ami == 1) {
                                $status = 'success';
                            }
                            $data = (!empty($list_ami[$data->status_ami]) ? $list_ami[$data->status_ami] : null);

                            return Html::a('<i class="fa fa-refresh"></i> ' . $data, '', ['class' => 'btn btn-sm btn-' . $status . ' btn-xs']);

                            // return !empty($statusData[$data->status_ami]) ? $statusData[$data->status_ami] : null;
                        }
                    ],
                    [
                        'label' => 'Rencana',
                        'format' => 'raw',
                        'hAlign' => 'center',
                        'value' => function ($data) {

                            return Html::a(
                                '<i class="fa fa-cog"></i> Rencana',
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-sm btn-primary btn-xs',
                                    'onclick' => "window.open('" . Yii::$app->urlManager->createUrl(['asesmen/rencana', 'id' => $data->id]) . "', '_blank')"
                                ]
                            );
                        }
                    ],
                    [
                        'label' => 'Asesmen',
                        'format' => 'raw',
                        'hAlign' => 'center',
                        'value' => function ($data) {

                            return Html::a(
                                '<i class="fa fa-file-text"></i> Asesmen',
                                'javascript:void(0)',
                                [
                                    'class' => 'btn btn-sm btn-success btn-xs',
                                    'onclick' => "window.open('" . Yii::$app->urlManager->createUrl(['asesmen/proses', 'id' => $data->id]) . "', '_blank')"
                                ]
                            );
                        }
                    ],
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        if (isset($model->tanggal_ami)) {
                            # code...
                            if (strtotime($model->tanggal_ami) < strtotime(date('Y-m-d'))) {
                                return ['style' => 'background-color: rgba(0, 255, 0, 0.2);'];
    
                                // return ['style' => 'background-color: #FFDAB9;'];
                            }
                        }
                    },

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>