<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\JenjangBobot;
use app\models\Kriteria;
use app\models\Temuan;
use app\models\Tilik;
use app\models\UnitKerja;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Hasil AMI';
$this->params['breadcrumbs'][] = ['label' => 'Ami Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$list_ami = MyHelper::getStatusAmi();

$ami_unit = AmiUnit::findOne($id);
$ami = Ami::findOne($ami_unit->ami_id);
$unit = UnitKerja::findOne($ami_unit->unit_id);
// echo '<pre>';print_r($ami_unit);exit;

$asesmen_nama = "$ami->nama - $unit->nama";
// echo '<pre>';print_r($model->id);die;
$kriteria_id = !empty($_GET['kriteria_id']) ? $_GET['kriteria_id'] : '';
$kki = !empty($_GET['kriteria_khusus_id']) ? $_GET['kriteria_khusus_id'] : '';



?>


<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title"><?= $this->title; ?> - <?= $amiUnit->unit->nama ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body">
        <div class="x_content">

            <span class="badge bg-red mr-2" style="margin-right: .5rem">Hasil AMI</span>
            <h3>Audit Mutu Internal - <?= $amiUnit->unit->nama ?> </h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <br>


            <div class="x_content">


                <div class="panel-body ">

                    <?php
                    if (!empty($listIndikator)) :
                    ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th style="text-align: center" class="alert alert-info" colspan="20"><strong><?= $ami->nama ?> - <?= $unit->nama ?></strong></th>
                                </tr>
                                <tr>

                                    <th width="3%" style="text-align: center"><strong>No</strong></th>
                                    <th width="10%" style="text-align: center"><strong>Kriteria</strong></th>
                                    <th width="50%" style="text-align: center"><strong>Nama Instrumen</strong></th>
                                    <th style="text-align: center"><strong>Evaluasi Diri</strong></th>
                                    <!-- <th width="9%" colspan="3" style="text-align: center"><strong>AK</strong></th> -->
                                    <th colspan="3" style="text-align: center"><strong>Asesmen Lapangan</strong></th>
                                    <!-- <th width="4%" style="text-align: center"><strong>Avg</strong></th>
                                    <th width="4%" style="text-align: center"><strong>Bobot</strong></th>
                                    <th width="6%" style="text-align: center"><strong>Hasil</strong></th> -->

                                </tr>

                                <?php

                                $i = 1;

                                foreach ($listIndikator as $key => $indikator) :

                                    $bobot = JenjangBobot::find()->where([
                                        'jenjang_id' => $amiUnit->unit->jenjangMap->jenjang_id,
                                        'indikator_id' => $indikator->id,
                                    ])->one();

                                    $kriteria = Kriteria::findOne($indikator->kriteria_id);

                                    $asesmen = Asesmen::find()->where([
                                        'indikator_id' => $indikator,
                                        'ami_unit_id' => $amiUnit->id,
                                    ])->one();
                                    $status_link = 'btn btn-sm btn-info';
                                    $class_link = 'btn btn-sm btn-info';
                                    $class = '<i class="fa fa-link"></i> View Document';

                                ?>

                                    <tr>
                                        <td style="text-align: center"><?= $i; ?></td>
                                        <td style="text-align: center"><?= $kriteria->nama ?></td>
                                        <td><?= $indikator->nama ?></td>
                                        <td width="9%" style="text-align: center">
                                            <?php
                                            if (!empty($asesmen)) {
                                                echo MyHelper::convertSkor($asesmen->skor_ed) ?? '-';
                                            }
                                            ?>
                                        </td>
                                        <!-- <td style="text-align: center">
                                            <? // MyHelper::convertSkor($asesmen->skor_ed)) 
                                            ?>
                                        </td>
                                        <td style="text-align: center">
                                            <? // MyHelper::convertSkor($asesmen->skor_ed) 
                                            ?>
                                        </td>
                                        <td style="text-align: center">
                                            <? // MyHelper::convertSkor($asesmen->skor_ed)
                                            ?> -->
                                        </td>
                                        <td width="8%" style="text-align: center">
                                            <?php
                                            if (!empty($asesmen)) {
                                                echo MyHelper::convertSkor($asesmen->skor_al1) ?? '-';
                                            }
                                            ?>
                                        </td>
                                        <td width="8%" style="text-align: center">
                                            <?php
                                            if (!empty($asesmen)) {
                                                echo MyHelper::convertSkor($asesmen->skor_al2) ?? '-';
                                            }
                                            ?>
                                        </td>
                                        <td width="8%" style="text-align: center">
                                            <?php
                                            if (!empty($asesmen)) {
                                                echo MyHelper::convertSkor($asesmen->skor_al3) ?? '-';
                                            }
                                            ?>
                                        </td>
                                        <!-- <td style="text-align: center;font-weight: bold;"> -->
                                        <?php
                                        // if ($asesmen != null) {

                                        //     $pembagi = 3;
                                        //     if ($asesmen->skor_al1 == null)  $pembagi--;
                                        //     if ($asesmen->skor_al2 == null)  $pembagi--;
                                        //     if ($asesmen->skor_al3 == null)  $pembagi--;

                                        //     $hasil = ($asesmen->skor_al1 + $asesmen->skor_al2 + $asesmen->skor_al3) / $pembagi;
                                        //     echo number_format((float)$hasil, 2, '.', '');
                                        // } else {
                                        //     echo '-';
                                        // } 
                                        ?>
                                        <!-- </td> -->
                                        <!-- <td style="text-align: center;font-weight: bold;"> -->
                                        <?php
                                        //  (isset($bobot->bobot) ? $bobot->bobot : "-") 
                                        ?>
                                        <!-- </td> -->
                                        <!-- <td style="text-align: center;font-weight: bold;color: orange;"> -->
                                        <?php

                                        // if ($asesmen != null) {

                                        //     $pembagi = 3;
                                        //     if ($asesmen->skor_al1 == null)  $pembagi--;
                                        //     if ($asesmen->skor_al2 == null)  $pembagi--;
                                        //     if ($asesmen->skor_al3 == null)  $pembagi--;

                                        //     $hasil = ($asesmen->skor_al1 + $asesmen->skor_al2 + $asesmen->skor_al3) / $pembagi;
                                        //     $hasil = $hasil * $bobot->bobot;
                                        //     echo number_format((float)$hasil, 2, '.', '');
                                        // } else {
                                        //     echo '-';
                                        // }
                                        ?>
                                        <!-- </td> -->
                                    </tr>
                                <?php
                                    $i++;
                                endforeach;
                                ?>
                                <!-- <tr style="font-weight: bold;background-color: #eee;">

                                    <td colspan="3" style="text-align: center;">Total Nilai</td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['totalEd'] ?></td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['totalAl1'] ?></td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['totalAl2'] ?></td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['totalAl3'] ?></td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['avrgAll'] ?></td>
                                    <td style="text-align: center;color: blue;"><?php $paketPenilaian['totalBobot'] ?></td>
                                    <td style="font-weight: bolder;text-align: center;color: green;"><?php $paketPenilaian['totalAll'] ?></td>

                                </tr> -->

                            </table>
                        </div>
                    <?php

                    endif;
                    ?>


                </div>

            </div>

        </div>
    </div>


</div>





<!-- Modal Master Input -->

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => [
        'id' => 'modalHeader',
    ],
    'header' => '<h3>Master Input</h3>',
    'id' => 'modal-master',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'id'        => 'form-master',
        'enctype'   => "multipart/form-data"
    ]
]); ?>
<div class="form-group">
    <label for="">Nama Instrumen</label>
    <?= Html::hiddenInput('indikator_id', '', ['class' => 'form-control', 'id' => 'indikator_id']) ?>
    <?= Html::hiddenInput('ami_unit_id', '', ['class' => 'form-control', 'id' => 'ami_unit_id']) ?>
    <?= Html::textArea('indikator_nama', '', ['class' => 'form-control', 'id' => 'indikator_nama', 'disabled' => true]) ?>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 1</label>
            <?= Html::textArea('skor1', '', ['class' => 'form-control', 'id' => 'skor1', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 2</label>
            <?= Html::textArea('skor2', '', ['class' => 'form-control', 'id' => 'skor2', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 3</label>
            <?= Html::textArea('skor3', '', ['class' => 'form-control', 'id' => 'skor3', 'disabled' => true]) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Indikator Skor 4</label>
            <?= Html::textArea('skor4', '', ['class' => 'form-control', 'id' => 'skor4', 'disabled' => true]) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="">Evaluasi Diri</label>
    <?= Html::textInput('skor_ed', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ed', 'placeholder' => 'Auditee']) ?>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Skor Asesmen Kecukupan</label>
            <?= Html::textInput('skor_ak1', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak1', 'placeholder' => 'Auditor 1']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Skor Asesmen Lapangan</label>
            <?= Html::textInput('skor_al1', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al1', 'placeholder' => 'Auditor 1']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_ak2', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak2', 'placeholder' => 'Auditor 2']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_al2', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al2', 'placeholder' => 'Auditor 2']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_ak3', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_ak3', 'placeholder' => 'Auditor 3']) ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= Html::textInput('skor_al3', '', ['class' => 'form-control', 'type' => 'number', 'id' => 'skor_al3', 'placeholder' => 'Auditor 3']) ?>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="">Link Bukti</label>
    <?= Html::textInput('link_bukti', '', ['class' => 'form-control', 'id' => 'link_bukti', 'placeholder' => 'Masukkan link bukti']) ?>
</div>


<div class="form-group">
    <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
</div>
<?php ActiveForm::end() ?>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var data = $("#form-master").serialize()

        console.log(data)

        $.ajax({
            url: "/asesmen/ajax-input-master",
            type: "POST",
            async: true,
            data: data,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-master").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    // Modal ED

    $(document).on("click", ".btn-audit", function(e) {
        e.preventDefault();
        var instrumen_id = $(this).data("id")
        var instrumen_nama = $(this).data("nama")
        var ami_unit_id = $(this).data("amiunit")
        var skor1 = $(this).data("skor1")
        var skor2 = $(this).data("skor2")
        var skor3 = $(this).data("skor3")
        var skor4 = $(this).data("skor4")

        $("#indikator_id").val(instrumen_id)
        $("#indikator_nama").val(instrumen_nama)
        $("#ami_unit_id").val(ami_unit_id)
        $("#skor1").val(skor1)
        $("#skor2").val(skor2)
        $("#skor3").val(skor3)
        $("#skor4").val(skor4)

        $("#modal-master").modal("show")

    });
</script>
<?php JSRegister::end(); ?>