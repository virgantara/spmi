<?php

use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Temuan */
/* @var $form yii\widgets\ActiveForm */

$list_kriteria = ArrayHelper::map(\app\models\Kriteria::find()->all(), 'id', 'nama');
$list_indikator = ArrayHelper::map(\app\models\Indikator::find()->all(), 'id', 'nama');

?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Kriteria</label>
        <?= $form->field($model, 'kriteria', ['options' => ['tag' => false]])->dropDownList($list_kriteria, ['class' => 'form-control', 'prompt' => '- Pilih Kategori -'])->label(false) ?>
    </div>
    <div class="form-group">
        <label class="control-label">Indikator</label>
        <?= $form->field($model, 'indikator', ['options' => ['tag' => false]])->dropDownList($list_indikator, ['class' => 'form-control', 'prompt' => '- Pilih Indikator -'])->label(false) ?>
    </div>
    <div class="form-group">
        <label class="control-label">Temuan</label>
        <?= $form->field($model, 'temuan', ['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Kriteria</label>
        <?= $form->field($model, 'kriteria', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Indikator</label>
        <?= $form->field($model, 'indikator', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Temuan</label>
        <?= $form->field($model, 'temuan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditee</label>
        <?= $form->field($model, 'auditee', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Kriteria temuan</label>
        <?= $form->field($model, 'kriteria_temuan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Lokasi</label>
        <?= $form->field($model, 'lokasi', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Ruang lingkup</label>
        <?= $form->field($model, 'ruang_lingkup', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Tanggal audit</label>
        <?= $form->field($model, 'tanggal_audit', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
            'readonly' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
            ]
        ])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Wakil auditee</label>
        <?= $form->field($model, 'wakil_auditee', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditor ketua</label>
        <?= $form->field($model, 'auditor_ketua', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditor anggota1</label>
        <?= $form->field($model, 'auditor_anggota1', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditor anggota2</label>
        <?= $form->field($model, 'auditor_anggota2', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditor anggota3</label>
        <?= $form->field($model, 'auditor_anggota3', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Auditor anggota4</label>
        <?= $form->field($model, 'auditor_anggota4', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Uraian k</label>
        <?= $form->field($model, 'uraian_k', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Referensi</label>
        <?= $form->field($model, 'referensi', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Akar penyebab</label>
        <?= $form->field($model, 'akar_penyebab', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Rekomendasi</label>
        <?= $form->field($model, 'rekomendasi', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Kategori</label>
        <?= $form->field($model, 'kategori', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Jadwal perbaikan</label>
        <?= $form->field($model, 'jadwal_perbaikan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <div class="form-group">
        <label class="control-label">Penanggung jawab</label>
        <?= $form->field($model, 'penanggung_jawab', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>