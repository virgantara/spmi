<?php

use app\helpers\MyHelper;
use app\models\Bpm;

$master_bpm = Bpm::find()->one();
?>
<table width="100%">
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">

                <tr>
                    <td width="15%" rowspan="4"></td>

                    <td width="40%" style="text-align: center;" rowspan="4">
                        <br><br>
                        <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
                        <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
                        <br>
                        <br>
                    </td>
                    <td width="20%">Kode Formulir</td>
                    <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_atl_kode) ? $amiUnit->ami->form_atl_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pembuatan</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_pembuatan) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Revisi</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_revisi) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Efektif</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_efektif) : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>AUDIT TINDAK LANJUT (ATL)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td width="25%">Tanggal Audit</td>
                    <td width="75%" colspan="2"><?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
                </tr>
                <tr>
                    <td>Tempat Audit</td>
                    <td colspan="2"><?= (isset($amiUnit->lokasi) ? $amiUnit->lokasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Auditee</td>
                    <td colspan="2"><?= $amiUnit->unit->nama ?></td>
                </tr>
                <tr>
                    <td>Perwakilan Auditee</td>
                    <td colspan="2"><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                </tr>
                <tr>
                    <td>Ketua Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 1)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Anggota Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 2)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Sekertatis Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 3)['nomor_registrasi'] ?? '' ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>PENGESAHAN</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td width="25%">Auditee</td>
                    <td width="25%">Auditor 1</td>
                    <td width="25%">Auditor 2</td>
                    <td width="25%">Auditor 3</td>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <tr style="font-size: 0.7em;text-align: center;">
                    <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>Setelah melaksanakan audit mutu internal para auditor mendapatkan temuan sebagai berikut:</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td colspan="5" style="text-align: center;">JUMLAH TEMUAN PERKATEGORI</td>
                </tr>
                <tr>
                    <td style="text-align: center;">MELAMPAUI</td>
                    <td style="text-align: center;">MEMENUHI</td>
                    <td style="text-align: center;">OBSERVASI</td>
                    <td style="text-align: center;">KTS MINOR</td>
                    <td style="text-align: center;">KTS MAYOR</td>
                </tr>
                <tr>
                    <!-- 
                    '4' => 'Melampaui', 
                    '3' => 'Memenuhi',
                    '0' => 'Observasi',
                    '1' => 'KTS/Minor',
                    '2' => 'KTS/Major',
                    -->
                    <td style="text-align: center;"><?= $dataJumlahPerkategori[4] ?></td>
                    <td style="text-align: center;"><?= $dataJumlahPerkategori[3] ?></td>
                    <td style="text-align: center;"><?= $dataJumlahPerkategori[0] ?></td>
                    <td style="text-align: center;"><?= $dataJumlahPerkategori[1] ?></td>
                    <td style="text-align: center;"><?= $dataJumlahPerkategori[2] ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 5px; font-size: 0.9em">
                <tr>
                    <td style="width: 5%; text-align:center;">No</td>
                    <td style="width: 50%; text-align:center;">Temuan/ Uraian Ketidaksesuaian</td>
                    <td style="width: 9%; text-align:center;">MP</td>
                    <td style="width: 9%; text-align:center;">MM</td>
                    <td style="width: 9%; text-align:center;">OB</td>
                    <td style="width: 9%; text-align:center;">KTS-MN</td>
                    <td style="width: 9%; text-align:center;">KTS-MR</td>
                </tr>
                <?php if (!empty($listTemuan)) { ?>
                    <?php
                    $no = 1;
                    foreach ($listTemuan as $key => $temuan) : ?>
                        <tr>
                            <td style="text-align:center;"><?= $no ?></td>
                            <td><?= (isset($temuan->uraian_k) ? $temuan->uraian_k : '-') ?></td>
                            <td style="text-align:center;"><?= (isset($temuan->kategori) ? ($temuan->kategori == '4' ? 'x' : '-') : '-') ?></td>
                            <td style="text-align:center;"><?= (isset($temuan->kategori) ? ($temuan->kategori == '3' ? 'x' : '-') : '-') ?></td>
                            <td style="text-align:center;"><?= (isset($temuan->kategori) ? ($temuan->kategori == '0' ? 'x' : '-') : '-') ?></td>
                            <td style="text-align:center;"><?= (isset($temuan->kategori) ? ($temuan->kategori == '1' ? 'x' : '-') : '-') ?></td>
                            <td style="text-align:center;"><?= (isset($temuan->kategori) ? ($temuan->kategori == '2' ? 'x' : '-') : '-') ?></td>


                            <!-- 
                            '4' => 'Melampaui', 
                            '3' => 'Memenuhi',
                            '0' => 'Observasi',
                            '1' => 'KTS/Minor',
                            '2' => 'KTS/Major',
                            -->
                        </tr>
                    <?php
                        $no++;
                    endforeach; ?>
                <?php } else { ?>

                    <tr>
                        <td colspan="7">Tidak ada tindak lanjut</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>



</table>