<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\Indikator;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AsesmenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Temuan';
$this->params['breadcrumbs'][] = $this->title;

$show = '';
if (!empty($_GET['ami_unit_id']) || !empty($_GET['ami_id']) || !empty($_GET['kriteria_id'])) {
    $show = 'show';
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="questionOne">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                            <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                        </a>
                    </h5>
                </div>
                <div id="answerOne" class="panel-collapse collapse <?= $show ?>" role="tabpanel" aria-labelledby="questionOne">
                    <div class="panel-body">

                        <?php $form = ActiveForm::begin([
                            'method' => 'GET',
                            'action' => ['temuan/master-temuan',],
                            'options' => [
                                'id' => 'form_validation',
                            ]
                        ]); ?>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="control-label ">Periode AMI</label>
                                    <?= Select2::widget([
                                        'name' => 'ami_id',
                                        'data' => ArrayHelper::map(Ami::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                                        'value' => $_GET['ami_id'] ?? null,
                                        'options' => ['placeholder' => Yii::t('app', '- Pilih Periode AMI -'), 'id' => 'ami_id'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]); ?>
                                </div>

                                <div class="form-group">
                                    <label class="control-label ">Kriteria</label>
                                    <?= Select2::widget([
                                        'name' => 'kriteria_id',
                                        'data' => $listKriteria,
                                        'value' => $_GET['kriteria_id'] ?? null,
                                        'options' => ['placeholder' => Yii::t('app', '- Pilih Kriteria -'), 'id' => 'kriteria_id'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]); ?>
                                </div>

                            </div>

                            <div class="col-md-6 col-lg-6 col-xs-12">

                                <div class="form-group">
                                    <label class="control-label ">Unit Kerja</label>
                                    <?= DepDrop::widget([
                                        'name' => 'ami_unit_id',
                                        'type' => DepDrop::TYPE_SELECT2,
                                        'options' => ['id' => 'ami_unit_id'],
                                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                        'value' => $_GET['ami_unit_id'] ?? null,
                                        'pluginOptions' => [
                                            'depends' => ['ami_id'],
                                            'initialize' => true,
                                            'placeholder' => '- Pilih Auditee -',
                                            'url' => Url::to(['/ami/subamiunit'])
                                        ]
                                    ]); ?>
                                </div>

                                <div class="form-group">
                                    <label class="control-label ">Kategori</label>
                                    <?= Select2::widget([
                                        'name' => 'kategori',
                                        'data' => MyHelper::getKategoriKts(),
                                        'value' => $_GET['kategori'] ?? null,
                                        'options' => ['placeholder' => Yii::t('app', '- Pilih Kategori -'), 'id' => 'kategori'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]); ?>

                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <?= Html::submitButton('<i class="fa fa-search"></i> Tampilkan', ['class' => 'btn btn-primary']) ?>
                            <?php Html::resetButton('Reset', ['class' => 'btn btn-default btn-reset', 'type' => 'reset']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>



            <div class="x_content">



                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'label' => 'Auditee',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->amiUnit->unit->nama;
                        },
                    ],
                    [
                        'attribute' => 'kriteria',
                        'value' => function ($data) {
                            return $data->indikator->kriteria->nama ?? '';
                        },
                    ],
                    [
                        'label' => 'Uraian Ketidaksesuaian',
                        'attribute' => 'uraian_k'
                    ],
                    'akar_penyebab',
                    'rekomendasi',
                    'rencana_perbaikan',
                    'referensi',
                    'jadwal_perbaikan',
                    [
                        'attribute' => 'kategori',
                        'value'     => function ($data) {
                            return $data->kategori == null ? '' : MyHelper::getKategoriKts()[$data->kategori];
                        }
                    ],
                    'penanggung_jawab',
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>