<?php

use app\helpers\MyHelper;
use app\models\Bpm;

$master_bpm = Bpm::find()->one();
?>
<table width="100%">
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">

                <tr>
                    <td width="15%" rowspan="4"></td>

                    <td width="40%" style="text-align: center;" rowspan="4">
                        <br><br>
                        <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
                        <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
                        <br>
                        <br>
                    </td>
                    <td width="20%">Kode Formulir</td>
                    <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_atl_kode) ? $amiUnit->ami->form_atl_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pembuatan</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_pembuatan) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Revisi</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_revisi) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Efektif</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_atl_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_atl_tanggal_efektif) : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>AUDIT TINDAK LANJUT (ATL)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td width="25%">Tanggal Audit</td>
                    <td width="75%" colspan="2"><?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
                </tr>
                <tr>
                    <td>Tempat Audit</td>
                    <td colspan="2"><?= (isset($amiUnit->lokasi) ? $amiUnit->lokasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Auditee</td>
                    <td colspan="2"><?= $amiUnit->unit->nama ?></td>
                </tr>
                <tr>
                    <td>Perwakilan Auditee</td>
                    <td colspan="2"><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                </tr>
                <tr>
                    <td>Ketua Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 1)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Anggota Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 2)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Sekertatis Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 3)['nomor_registrasi'] ?? '' ?></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>PENGESAHAN</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td width="25%">Auditee</td>
                    <td width="25%">Auditor 1</td>
                    <td width="25%">Auditor 2</td>
                    <td width="25%">Auditor 3</td>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <tr style="font-size: 0.7em;text-align: center;">
                    <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <?php
    $no = 1;
    if (!empty($atl)) { ?>
        <?php foreach ($atl as $key => $temuan) : ?>
            <tr>
                <td>
                    <table border="1" style="padding: 5px;">
                        <tr>
                            <td>No. PTK</td>
                            <td colspan="3"><?= $no ?></td>
                        </tr>
                        <tr>
                            <td>Deskripsi Kondisi</td>
                            <td colspan="3"><?= (isset($temuan->uraian_k) ? $temuan->uraian_k : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Rencana Tindak Lanjut (RTL)</td>
                            <td colspan="3"><?= (isset($temuan->rencana_tindak_lanjut) ? $temuan->rencana_tindak_lanjut : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Jadwal RTL</td>
                            <td colspan="3"><?= (isset($temuan->jadwal_rtl) ? $temuan->jadwal_rtl : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Realisasi Tindak Lanjut</td>
                            <td colspan="3"><?= (isset($temuan->realisasi_tindak_lanjut) ? $temuan->realisasi_tindak_lanjut : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Efektifitas</td>
                            <td colspan="3"><?= (isset($temuan->efektifitas) ? MyHelper::getStatusEfektif()[$temuan->efektifitas] : '-') ?></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td width="33%">[<?= (isset($temuan->status) ? ($temuan->status == 'close' ? 'x' : '-') : '-') ?>] Close</td>
                                        <td width="33%">[<?= (isset($temuan->status) ? ($temuan->status == 'open' ? 'x' : '-') : '-') ?>] Open</td>
                                        <td width="33%">[<?= (isset($temuan->status) ? ($temuan->status == 'toleran' ? 'x' : '-') : '-') ?>] Toleran</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>

        <?php
            $no++;
        endforeach; ?>
    <?php } else { ?>

        <tr>
            <td>Tidak ada tindak lanjut</td>
        </tr>
    <?php } ?>


</table>