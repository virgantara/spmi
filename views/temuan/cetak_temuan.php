<?php

use app\helpers\MyHelper;
use app\models\Bpm;

$master_bpm = Bpm::find()->one();
?>
<table width="100%">
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">

                <tr>
                    <td width="15%" rowspan="4"></td>

                    <td width="40%" style="text-align: center;" rowspan="4">
                        <br><br>
                        <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
                        <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
                        <br>
                        <br>
                    </td>
                    <td width="20%">Kode Formulir</td>
                    <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_temuan_kode) ? $amiUnit->ami->form_temuan_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pembuatan</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_temuan_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_temuan_tanggal_pembuatan) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Revisi</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_temuan_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_temuan_tanggal_revisi) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Efektif</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_temuan_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_temuan_tanggal_efektif) : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>TEMUAN AUDIT & PERMINTAAN TINDAKAN KOREKSI (PTK)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td width="25%">Tanggal Audit</td>
                    <td width="75%" colspan="2"><?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
                </tr>
                <tr>
                    <td>Tempat Audit</td>
                    <td colspan="2"><?= (isset($amiUnit->lokasi) ? $amiUnit->lokasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Auditee</td>
                    <td colspan="2"><?= $amiUnit->unit->nama ?></td>
                </tr>
                <tr>
                    <td>Perwakilan Auditee</td>
                    <td colspan="2"><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                </tr>
                <tr>
                    <td>Ketua Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 1)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Anggota Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 2)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Sekertatis Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 3)['nomor_registrasi'] ?? '' ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>PENGESAHAN</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td width="25%">Auditee</td>
                    <td width="25%">Auditor 1</td>
                    <td width="25%">Auditor 2</td>
                    <td width="25%">Auditor 3</td>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <tr style="font-size: 0.7em;text-align: center;">
                    <td><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td>Kriteria</td>
                    <td colspan="3"><?= (isset($temuan->asesmen_id) ? $temuan->asesmen->indikator->kriteria->nama . (isset($temuan->asesmen->indikator->kriteria->keterangan) ? ' - ' . $temuan->asesmen->indikator->kriteria->keterangan : '')  : '-') ?></td>
                </tr>
                <tr>
                    <td>Indikator/Instrumen</td>
                    <td colspan="3"><?= (isset($temuan->asesmen_id) ? $temuan->asesmen->indikator->nama : '-') ?></td>
                </tr>
                <tr>
                    <td>Deskripsi Kondisi/ Uraian Ketidaksesuaian</td>
                    <td colspan="3"><?= (isset($temuan->uraian_k) ? $temuan->uraian_k : '-') ?></td>
                </tr>
                <tr>
                    <td width="25%">Referensi</td>
                    <td width="75%" colspan="3"><?= (isset($temuan->referensi) ? $temuan->referensi : '-') ?></td>
                </tr>
                <tr>
                    <td>Akar Penyebab</td>
                    <td colspan="3" style="font-size: 0.75em"><?= (isset($temuan->akar_penyebab) ? $temuan->akar_penyebab : '-') ?></td>
                </tr>
                <tr>
                    <td>Rekomendasi</td>
                    <td colspan="3" style="font-size: 0.75em"><?= (isset($temuan->rekomendasi) ? $temuan->rekomendasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Kategori</td>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td width="20%">[<?= (isset($temuan->kategori) ? ($temuan->kategori == 0 ? 'x' : '-') : '-') ?>] Obervasi</td>
                                <td width="20%">[<?= (isset($temuan->kategori) ? ($temuan->kategori == 1 ? 'x' : '-') : '-') ?>] KTS/Minor</td>
                                <td width="20%">[<?= (isset($temuan->kategori) ? ($temuan->kategori == 2 ? 'x' : '-') : '-') ?>] KTS/Major</td>
                                <td width="20%">[<?= (isset($temuan->kategori) ? ($temuan->kategori == 3 ? 'x' : '-') : '-') ?>] Memenuhi</td>
                                <td width="20%">[<?= (isset($temuan->kategori) ? ($temuan->kategori == 4 ? 'x' : '-') : '-') ?>] Melampaui</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>

        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td>No. PTK</td>
                    <td colspan="3"><?= $no ?></td>
                </tr>
                <tr>
                    <td>Rencana Perbaikan</td>
                    <td colspan="3" style="font-size: 0.75em"><?= (isset($temuan->rencana_tindak_lanjut) ? $temuan->rencana_tindak_lanjut : '-') ?></td>
                </tr>
                <tr>
                    <td>Jadwal Perbaikan</td>
                    <td><?= (isset($temuan->jadwal_perbaikan) ? $temuan->jadwal_perbaikan : '-') ?></td>
                    <td>Penanggung Jawab</td>
                    <td><?= (isset($temuan->penanggung_jawab) ? '<strong>' . $temuan->jabatan_pj . '</strong><br><small>' . $temuan->penanggung_jawab . '</small>' : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <!-- <tr>
        <td>

            <table border="0">
                <tr>
                    <td style="text-align: center;">Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh Badan Penjaminan Mutu (BPM), Universitas Darussalam Gontor</td>
                </tr>
            </table>
        </td>
    </tr> -->
</table>