<?php

use app\helpers\MyHelper;
use app\models\Asesmen;
use app\models\Indikator;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Kriteria;
use app\models\Temuan;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use Symfony\Component\Console\Helper\Dumper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Temuan';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['asesmen/ami']];
$this->params['breadcrumbs'][] = $this->title;

$listAsesmen = ArrayHelper::map(Asesmen::find()->where(['ami_unit_id' => $amiUnit->id])->all(), 'id', function ($data) {
    return $data->indikator->nama;
});

?>

<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>

    <div class="panel-body">
        <div class="x_content">

            <h3><?= $amiUnit->unit->nama ?></h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <?php if (Yii::$app->user->can("admin")) :
                Html::a(
                    '<i class="fa fa-plus"></i> Tambah',
                    [
                        'asesmen/ami'
                    ],
                    [
                        'class'     => 'btn btn-success btn-sm btn-add-temuan',
                    ]
                );
            endif;
            ?>
            <?= Html::a(
                '<i class="fa fa-file-pdf-o"></i> Cetak',
                [
                    'temuan/cetak-temuan',
                    'ami_unit_id' => $amiUnit->id
                ],
                [
                    'class'     => 'btn btn-danger btn-sm',
                    'target'    => "_blank"
                ]
            ) ?>
            <?= Html::a(
                '<i class="fa fa-file-pdf-o"></i> Log Temuan',
                [
                    'temuan/cetak-log-temuan',
                    'id' => $amiUnit->id
                ],
                [
                    'class'     => 'btn btn-primary btn-sm',
                    'target'    => "_blank"
                ]
            ) ?>
            <br>
            <br>

            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'contentOptions' => ['class' => 'kartik-sheet-style'],
                    'width' => '36px',
                    'pageSummary' => 'Total',
                    'pageSummaryOptions' => ['colspan' => 6],
                    'header' => '',
                    'headerOptions' => ['class' => 'kartik-sheet-style']
                ],
                [
                    'attribute' => 'asesmen_id',
                    'filter' => $listAsesmen,
                    'filterType' => GridView::FILTER_SELECT2,
                    'label' => 'Nama Indikator',
                    'width' => '30%',
                    'value' => function ($data) {
                        return $data->asesmen->indikator->nama ?? '';
                    },
                    'filterWidgetOptions' => [
                        'options' => ['placeholder' => 'Pilih Indikator', 'allowClear' => true],
                        'pluginOptions' => ['allowClear' => true]
                    ],
                ],
                // [
                //     'class' => 'kartik\grid\EditableColumn',
                //     'attribute' => 'temuan',
                //     'label' => 'Temuan',
                //     'readonly' => !Yii::$app->user->can('auditor'),
                //     'editableOptions' => [
                //         'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                //     ],
                //     'value' => function ($data) {
                //         return ($data->temuan == null ? '' : $data->temuan);
                //     }
                // ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'uraian_k',
                    'label' => 'Uraian Ketidaksesuaian',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->uraian_k == null ? '' : $data->uraian_k);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'referensi',
                    'label' => 'Referensi',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'value' => function ($data) {
                        return ($data->referensi == null ? '' : $data->referensi);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'akar_penyebab',
                    'label' => 'Akar Penyebab',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->akar_penyebab == null ? '' : $data->akar_penyebab);
                    }
                ],
                // 'rencana_perbaikan',
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'rekomendasi',
                    'label' => 'Rekomendasi',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->rekomendasi == null ? '' : $data->rekomendasi);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'kategori',
                    'refreshGrid' => true,
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'filter' => MyHelper::getKategoriKts(),
                    'value' => function ($data) {
                        return $data->kategori == null ? '' : MyHelper::getKategoriKts()[$data->kategori];
                    },
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => MyHelper::getKategoriKts(),

                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'jadwal_perbaikan',
                    'label' => 'Jadwal Perbaikan',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    'value' => function ($data) {
                        return ($data->jadwal_perbaikan == null ? '' : $data->jadwal_perbaikan);
                    }
                ],
                [
                    'attribute' => 'penanggung_jawab',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $label  = '<strong>' . $data->jabatan_pj . '</strong><br><small>' . $data->penanggung_jawab . '</small>';
                        $label  .= ' ' . Html::a('<i class="fa fa-edit"></i>', 'javascript:void(0)', ['class' => 'btn-edit-dosen', 'title' => 'Ubah Dosen', 'data-item' => $data->id]);
                        return $label;
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['style' => 'width:100px;'],
                    // 'buttonOptions' => ['class' => 'btn btn-default'],
                    'visible' => Yii::$app->user->can('auditor'),
                    'header' => 'Aksi',
                    'template' => '{update} {hapus} ',
                    'buttons' => [
                        'update'    => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '', [
                                'title' => Yii::t('app', 'Update Temuan'),
                                'data-pjax' =>  0,
                                'data-temuanid' =>  $model->id,
                                'class' => 'btn-update-temuan'
                            ]);
                        },
                        'hapus'    => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                [
                                    'temuan/delete', 'id' => $model->id,
                                    'ami_unit_id' => $model->ami_unit_id
                                ],
                                [
                                    'title' => Yii::t('app', 'Delete Temuan'),
                                    'data'  => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        },
                    ]
                ],
            ]; ?>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>

        </div>
    </div>
</div>



<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-temuan',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-temuan">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Kriteria</label>
                <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id"]) ?>
                <?= Html::hiddenInput('ami_unit_id', $id, ['class' => 'form-control', "id" => "ami_unit_id"]) ?>
                <?= Html::textInput('kriteria_temuan', '', ['class' => 'form-control', "id" => "kriteria", "placeholder" => "Masukkan kriteria temuan"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Indikator/Instrumen</label>
                <?= Html::textInput('indikator_id', '', ['class' => 'form-control', "id" => "indikator_id", "placeholder" => "Masukkan indikator"]) ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="">Temuan</label>
        <?= Html::textInput('temuan', '', ['class' => 'form-control', "id" => "temuan", "placeholder" => "Masukkan temuan"]) ?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Lokasi</label>
                <?= Html::textInput('lokasi', '', ['class' => 'form-control', "id" => "lokasi", "placeholder" => "Masukkan lokasi"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Ruang lingkup</label>
                <?= Html::textInput('ruang_lingkup', '', ['class' => 'form-control', "id" => "ruang_lingkup", "placeholder" => "Masukkan ruang lingkup"]) ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="">Uraian ketidaksesuaian</label>
        <?= Html::textInput('uraian_k', '', ['class' => 'form-control', "id" => "uraian_k", "placeholder" => "Masukkan uraian ketidaksesuaian"]) ?>
    </div>
    <div class="form-group">
        <label for="">Referensi</label>
        <?= Html::textInput('referensi', '', ['class' => 'form-control', "id" => "referensi", "placeholder" => "Masukkan referensi"]) ?>
    </div>
    <div class="form-group">
        <label for="">Akar penyebab</label>
        <?= Html::textInput('akar_penyebab', '', ['class' => 'form-control', "id" => "akar_penyebab", "placeholder" => "Masukkan akar penyebab"]) ?>
    </div>
    <div class="form-group">
        <label for="">Rekomendasi</label>
        <?= Html::textInput('rekomendasi', '', ['class' => 'form-control', "id" => "rekomendasi", "placeholder" => "Masukkan rekomendasi"]) ?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Kategori</label>
                <?= Html::radioList('kategori', '', MyHelper::getKategoriKts(), ['class' => 'form-control', "id" => "kategori"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Jadwal perbaikan</label>
                <?= Html::textInput('jadwal_perbaikan', '', ['class' => 'form-control', "id" => "jadwal_perbaikan", "placeholder" => "Masukkan jadwal perbaikan"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Penanggung jawab</label>
                <?= Html::textInput('jabatan_pj', '', ['class' => 'form-control', "id" => "jabatan_pj", "placeholder" => "Masukkan jabatan penanggung jawab"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">.</label>
                <?= Html::textInput('penanggung_jawab', '', ['class' => 'form-control penanggung_jawab', "id" => "penanggung_jawab", "placeholder" => "Masukkan nama penanggung jawab"]) ?>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Penanggung Jawab</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="temuan_id">
                <div class="col-md-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label for="">Jabatan</label>
                            <?= Html::textInput('jabatan', '', ['class' => 'form-control', 'id' => 'jabatan', 'placeholder' => 'Ketik Jabatan']) ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label for="">Nama Penanggung Jawab</label>
                            <?= Html::textInput('nama_pejabat', '', ['class' => 'form-control', 'id' => 'nama_pejabat', 'placeholder' => 'Ketik Nama Dosen']) ?>
                        </div>

                    </div>
                </div>
                <small>*pastikan kebenaran data</small>
                <?php
                AutoComplete::widget([
                    'name' => 'nama_pejabat',
                    'id' => 'nama_pejabat',
                    'clientOptions' => [
                        'source' => Url::to(['auditor/ajax-cari-dosen']),
                        'autoFill' => true,
                        'minLength' => '1',
                        'select' => new JsExpression("function( event, ui ) {
                            $('#dosen_id').val(ui.item.id);
                        }")
                    ],
                    'options' => [
                        // 'size' => '40'
                    ]
                ]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-update-pj">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).bind("keyup.autocomplete", function() {

        $('#penanggung_jawab').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });

        $('#indikator-temuan').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-temuan").serialize()

        $.ajax({
            url: "/temuan/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-temuan").modal("hide")
                        $(":input", "#modal-temuan").val("")
                        window.location.reload()
                    });


                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", ".btn-edit-dosen", function(e) {
        e.preventDefault()

        $("#exampleModal").modal("show");

        $("#temuan_id").val($(this).data("item"));
    })

    $(document).on("click", "#btn-update-pj", function(e) {
        e.preventDefault();

        var obj = new Object
        obj.id = $('#temuan_id').val();
        obj.jabatan_pj = $('#jabatan').val();
        obj.penanggung_jawab = $('#nama_pejabat').val();

        $.ajax({
            url: "/temuan/ajax-update-pj",
            type: "POST",
            async: true,
            data: {
                dataPost: obj
            },
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close();
                var hasil = $.parseJSON(data);
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#exampleModal").modal("hide");
                        $.pjax.reload({
                            container: '#my-grid',
                            async: false
                        });
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    });
                }
            }

        })
    })

    $(document).on("click", ".btn-add-temuan", function(e) {
        e.preventDefault();

        var ami_unit_id = $(this).data("amiUnit")

        $("#modal-temuan").modal("show")
    });

    $(document).on("click", ".btn-update-temuan", function(e) {
        e.preventDefault();

        var temuan_id = $(this).data("temuanid")

        $.ajax({
            url: "/temuan/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: temuan_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                $("#modal-temuan").modal("show")

                $("#id").val(hasil.id)
                $("#kriteria").val(hasil.kriteria)
                $("#temuan").val(hasil.temuan)
                $("#lokasi").val(hasil.lokasi)
                $("#ruang_lingkup").val(hasil.ruang_lingkup)
                $("#uraian_k").val(hasil.uraian_k)
                $("#referensi").val(hasil.referensi)
                $("#akar_penyebab").val(hasil.akar_penyebab)
                $("#rekomendasi").val(hasil.rekomendasi)
                $("#jadwal_perbaikan").val(hasil.jadwal_perbaikan)
                $("#jabatan_pj").val(hasil.jabatan_pj)
                $("#penanggung_jawab").val(hasil.penanggung_jawab)

                $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })

    });
</script>
<?php JSRegister::end() ?>