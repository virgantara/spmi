<?php

use app\helpers\MyHelper;
use app\models\Asesmen;
use app\models\Indikator;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Kriteria;
use app\models\Temuan;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use Symfony\Component\Console\Helper\Dumper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Audit Tindak Lanjut';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['asesmen/ami']];
$this->params['breadcrumbs'][] = $this->title;

$listTemuan = ArrayHelper::map(Temuan::find()->where(['ami_unit_id' => $amiUnit->id])->all(), 'id', 'uraian_k');
$listStatusEfektif = MyHelper::getStatusEfektif();
$listStatusRtl = MyHelper::getStatusRtl();
?>

<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>

    </div>
    <div class="panel-body">
        <div class="x_content">
            <h3><?= $amiUnit->unit->nama ?></h3>
            <h4><?= $amiUnit->ami->nama ?></h4>

            <?= Html::a('<i class="fa fa-file-pdf-o"></i> ' . Yii::t('app', 'Cetak'), ['cetak-atl', 'id' => $id], ['class' => 'btn btn-danger btn-sm', 'target' => '_blank']) ?>

            <br>
            <br>
            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'contentOptions' => ['class' => 'kartik-sheet-style'],
                    'width' => '36px',
                    'pageSummary' => 'Total',
                    'pageSummaryOptions' => ['colspan' => 6],
                    'header' => '',
                    'headerOptions' => ['class' => 'kartik-sheet-style']
                ],
                'uraian_k',
                'rekomendasi',
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'rencana_tindak_lanjut',
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'jadwal_rtl',
                    'editableOptions'   => [
                        'inputType' => \kartik\editable\Editable::INPUT_DATE,
                        // 'asPopover' => false,
                        'options' => [
                            'size' => 'md',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ],
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'realisasi_tindak_lanjut',
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'efektifitas',
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => $listStatusEfektif
                    ],
                    'value' => function ($model) use ($listStatusEfektif) {
                        return (isset($model->efektifitas) ? $listStatusEfektif[$model->efektifitas] : null);
                    },
                    'filter' => $listStatusEfektif,
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Efektifitas'), 'allowClear' => true],
                        'pluginOptions' => ['allowClear' => true]
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'status',
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => $listStatusRtl
                    ],
                    'value' => function ($model) use ($listStatusRtl) {
                        return (isset($model->status) ? $listStatusRtl[$model->status] : null);
                    },
                    'filter' => $listStatusRtl,
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options' => ['placeholder' => Yii::t('app', 'Pilih Status'), 'allowClear' => true],
                        'pluginOptions' => ['allowClear' => true]
                    ],
                ]
            ]; ?>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    $status = $model->status; // Ambil nilai status dari model
                    if ($status === null || $status === 'open') {
                        return ['class' => 'danger']; // Tambahkan kelas 'danger' untuk baris yang memenuhi kondisi
                    }
                },
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>

        </div>
    </div>
</div>


<?php

yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-atl',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-temuan">

    <div class="form-group">
        <label for="temuan_id">Temuan</label>
        <?= Select2::widget([
            'name' => 'temuan_id',
            'data' => $listTemuan,
            'options' => ['placeholder' => 'Pilih Temuan', 'id' => 'temuan_id'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <label for="">Rencana Tindak Lanjut (RTL)</label>
        <?= Html::textInput('rencana_tindak_lanjut', '', ['class' => 'form-control', "id" => "rencana_tindak_lanjut", "placeholder" => "Masukkan Rencana Tindak Lanjut"]) ?>
    </div>
    <div class="form-group">
        <label for="">Jadwal RTL</label>
        <?= Html::textInput('jadwal_rtl', '', ['class' => 'form-control', "id" => "jadwal_rtl", "placeholder" => "Masukkan Jadwal RTL"]) ?>
    </div>
    <div class="form-group">
        <label for="">Realisasi Tindak Lanjut</label>
        <?= Html::textInput('realisasi_tindak_lanjut', '', ['class' => 'form-control', "id" => "realisasi_tindak_lanjut", "placeholder" => "Masukkan Realisasi Tindak Lanjut"]) ?>
    </div>
    <div class="form-group">
        <label for="">Efektifitas</label>
        <?= Html::textInput('efektifitas', '', ['class' => 'form-control', "id" => "efektifitas", "placeholder" => "Masukkan efektifitas"]) ?>
    </div>
    <div class="form-group">
        <label for="">Status</label>
        <?= Html::radioList('status', '', MyHelper::getStatusRtl(), ['class' => 'form-control', "id" => "status"]) ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>


<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-temuan").serialize()

        $.ajax({
            url: "/audit-tindak-lanjut/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-atl").modal("hide")
                        $(":input", "#modal-atl").val("")
                        window.location.reload()
                    });


                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", ".btn-add-atl", function(e) {
        e.preventDefault();

        $("#modal-atl").modal("show")
    });

    $(document).on("click", ".btn-update-temuan", function(e) {
        e.preventDefault();

        var temuan_id = $(this).data("temuanid")

        $.ajax({
            url: "/audit-tindak-lanjut/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: temuan_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                $("#modal-atl").modal("show")

                $("#id").val(hasil.id)
                $("#kriteria").val(hasil.kriteria)
                $("#temuan").val(hasil.temuan)
                $("#lokasi").val(hasil.lokasi)
                $("#ruang_lingkup").val(hasil.ruang_lingkup)
                $("#uraian_k").val(hasil.uraian_k)
                $("#referensi").val(hasil.referensi)
                $("#akar_penyebab").val(hasil.akar_penyebab)
                $("#rekomendasi").val(hasil.rekomendasi)
                $("#jadwal_perbaikan").val(hasil.jadwal_perbaikan)
                $("#jabatan_pj").val(hasil.jabatan_pj)
                $("#penanggung_jawab").val(hasil.penanggung_jawab)

                $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })

    });
</script>
<?php JSRegister::end() ?>