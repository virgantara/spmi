<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemuanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="temuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uraian_k') ?>

    <?= $form->field($model, 'referensi') ?>

    <?= $form->field($model, 'akar_penyebab') ?>

    <?= $form->field($model, 'rekomendasi') ?>

    <?php // echo $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'jadwal_perbaikan') ?>

    <?php // echo $form->field($model, 'penanggung_jawab') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'ami_unit_id') ?>

    <?php // echo $form->field($model, 'jabatan_pj') ?>

    <?php // echo $form->field($model, 'asesmen_id') ?>

    <?php // echo $form->field($model, 'indikator_id') ?>

    <?php // echo $form->field($model, 'rencana_perbaikan') ?>

    <?php // echo $form->field($model, 'rencana_tindak_lanjut') ?>

    <?php // echo $form->field($model, 'jadwal_rtl') ?>

    <?php // echo $form->field($model, 'realisasi_tindak_lanjut') ?>

    <?php // echo $form->field($model, 'efektifitas') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
