<?php

use app\models\Indikator;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Kriteria;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Temuan Audit';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <p>
                    <?= Html::a('<i class="fa fa-plus"></i> Buat Temuan Audit', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    // 'kriteria',
                    [
                        'label'     => 'Unit',
                        'format'    => 'raw',
                        'value'     => function ($data) {
                            return $data->asesmen->amiUnit->unit->nama;
                        },
                    ],
                    [
                        'attribute' => 'kriteria',
                        'label' => 'Kriteria',
                        'value' => function ($data) {
                            $kriteria = Kriteria::find()->where(['id' => $data->kriteria])->one();
                            return (isset($kriteria->nama) ? $kriteria->nama : '');
                        }
                    ],
                    // 'indikator',
                    [
                        'attribute' => 'indikator',
                        'label' => 'Indikator',
                        'value' => function ($data) {
                            $indikator = Indikator::find()->where(['id' => $data->indikator])->one();
                            return (isset($indikator->nama) ? $indikator->nama : "");
                        }
                    ],
                    'temuan',
                    'kriteria_temuan',
                    'lokasi',
                    //'ruang_lingkup',
                    //'tanggal_audit',
                    //'wakil_auditee',
                    //'auditor_ketua',
                    //'auditor_anggota1',
                    //'auditor_anggota2',
                    //'auditor_anggota3',
                    //'auditor_anggota4',
                    //'uraian_k',
                    //'referensi',
                    //'akar_penyebab',
                    //'rekomendasi',
                    //'kategori',
                    //'jadwal_perbaikan',
                    //'penanggung_jawab',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:100px;'],
                        // 'buttonOptions' => ['class' => 'btn btn-default'],
                        'header' => 'Aksi',
                        'template' => '{view} {update} {delete} ',
                        'buttons' => [
                            'tambah' => function ($url, $model) {
                                $url = Yii::$app->urlManager->createUrl(['temuan/create-deskripsi', 'id' => $model->id]);
                                return Html::a('<i class="glyphicon glyphicon-plus"></i>', $url, ['title' => 'Tambah Deskripsi']);
                            }
                        ]
                    ],
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>