<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitKerjaProdi */

$this->title = 'Create Unit Kerja Prodi';
$this->params['breadcrumbs'][] = ['label' => 'Unit Kerja Prodis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    	   </div>
        </div>
    </div>
</div>