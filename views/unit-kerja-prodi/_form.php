<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerjaProdi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Unit kerja</label>
            <?= $form->field($model, 'unit_kerja_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Prodi</label>
            <?= $form->field($model, 'prodi_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
