<?php

use app\helpers\MyHelper;
use app\models\Prodi;
use app\models\UnitKerja;
use app\models\UnitKerjaProdi;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnitKerjaProdiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unit Kerja Prodis';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $listUnitKerja = UnitKerja::find()->where(['jenis' => 'prodi'])->orderBy('singkatan ASC')->all();
                            foreach ($listUnitKerja as $unitKerja) {
                            ?>
                            <th><?= $unitKerja->singkatan ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        
                    <?php
                        $listProdi = Prodi::find()->orderBy('nama_prodi ASC')->all();
                        foreach ($listProdi as $prodi) {
                        ?>
                        <tr>
                            <th><?= $prodi->nama_prodi; ?></th>

                            <?php
                            $x = 1;
                                foreach ($listUnitKerja as $unitKerja) {
                                    $m = UnitKerjaProdi::find()->where(['prodi_id' => $prodi->id, 'unit_kerja_id' => $unitKerja->id])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                            <td style="text-align: center"><input type="checkbox" <?= $checked; ?> class="mycheck"
                                    data-prodi="<?= $prodi->id; ?>" data-unitkerja="<?= $unitKerja->id; ?>" value="1">
                            </td>
                            <?php
                            $x++;
                                }
                                ?>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php 


$script = ' 
$(".mycheck").change(function(){
    var unit_kerja      = $(this).data("unitkerja");
    var prodi           = $(this).data("prodi");
    var obj             = new Object;
    obj.unit_kerja_id   = unit_kerja;
    obj.prodi_id        = prodi;
    obj.checked         = $(this).prop("checked") ? "1" : "0";

    $.ajax({
        type    : "POST",
        url     : "'.Url::to(['unit-kerja-prodi/ajax-prodi-satker']).'",
        data    : {
            dataPost: obj
        },
        success : function(data){
            console.log(data)
        }
    });
});
';


$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

?>