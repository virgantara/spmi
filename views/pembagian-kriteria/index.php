<?php

use app\helpers\MyHelper;
use app\models\Kriteria;
use app\models\PembagianKriteria;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PembagianKriteriaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Pembagian Kriteria';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $listJenis = MyHelper::listJenisUnit();
                            foreach ($listJenis as $jenis => $value) {
                            ?>
                            <th><?= MyHelper::getJenisUnit()[$jenis]; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $listKriteria = Kriteria::find()->orderBy('urutan ASC')->all();
                        foreach ($listKriteria as $kriteria) {
                        ?>
                        <tr>
                            <th><?= $kriteria->nama; ?></th>

                            <?php
                            $x = 1;
                                foreach ($listJenis as $jenis) {
                                    $m = PembagianKriteria::find()->where(['kriteria_id' => $kriteria->id, 'jenis_unit' => $x])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                            <td><input type="checkbox" <?= $checked; ?> class="mycheck"
                                    data-kriteria="<?= $kriteria->id; ?>" data-jenis="<?= $jenis; ?>" value="1">
                            </td>
                            <?php
                            $x++;
                                }
                                ?>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php 


$script = ' 
$(".mycheck").change(function(){
    var kriteria    = $(this).data("kriteria");
    var jenis       = $(this).data("jenis");
    var obj         = new Object;
    obj.kriteria_id = kriteria;
    obj.jenis_unit  = jenis;
    obj.checked     = $(this).prop("checked") ? "1" : "0";

    $.ajax({
        type    : "POST",
        url     : "'.Url::to(['pembagian-kriteria/ajax-pembagian-kriteria']).'",
        data    : {
            dataPost: obj
        },
        success : function(data){
            console.log(data)
        }
    });
});
';


$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

?>