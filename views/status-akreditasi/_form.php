<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StatusAkreditasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>


    <?= $form->field($model, 'kode')->textInput(['class'=>'form-control','maxlength' => true]) ?>
    <?= $form->field($model, 'nama_id')->textInput(['class'=>'form-control','maxlength' => true]) ?>
    <?= $form->field($model, 'nama_en')->textInput(['class'=>'form-control','maxlength' => true]) ?>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
