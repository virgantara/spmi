<?php

use app\models\CapaianKinerjaSearch;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Periode */

$this->title = 'Buat Periode Input Capaian';
$this->params['breadcrumbs'][] = ['label' => 'Periode Input Capaian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
