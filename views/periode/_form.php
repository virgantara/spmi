<?php

use app\helpers\MyHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Periode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>


    <div class="form-group">
        <label class="control-label">Nama Periode</label>
        <?= $form->field($model, 'periode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Masukkan nama periode tahun! ')])->label(false) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'tahun')->widget(Select2::classname(), [
            'data'    => MyHelper::getYears(),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Tahun -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label('Tahun') ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status aktif</label>
        <?= $form->field($model, 'status_aktif', ['options' => ['tag' => false]])->radioList(MyHelper::getStatusAktif())->label(false) ?>
    </div>


    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>

<div class="help-block"></div>