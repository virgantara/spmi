<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\CapaianKinerja;
use app\models\CapaianPeriode;
use richardfan\widget\JSRegister;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Periode */

$this->title = $model->periode;
$this->params['breadcrumbs'][] = ['label' => 'Periodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$capaian_kinerja = CapaianKinerja::find()->orderBy(['no_indikator' => SORT_ASC])->all();
?>

<style type="text/css">
    input[type=checkbox] {
        /* Double-sized Checkboxes */
        -ms-transform: scale(1.75);
        /* IE */
        -moz-transform: scale(1.75);
        /* FF */
        -webkit-transform: scale(1.75);
        /* Safari and Chrome */
        -o-transform: scale(1.75);
        /* Opera */
        padding: 10px;
    }
</style>

<div class="block-header">
    <h3><strong>Nama Periode: <?= Html::encode($this->title) ?></strong></h3>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'tahun',
                        'periode',
                        [
                            'attribute' => 'status_aktif',
                            'value' => function ($data) {
                                return isset($data->status_aktif) ? MyHelper::setStatusAktif($data->status_aktif) : "-";
                            }
                        ],
                        // 'created_at',
                        // 'updated_at',
                    ],
                ]) ?>

            </div>


        </div>

    </div>

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tr>

                                <th style="text-align: center" rowspan="1"><strong>Sasaran strategis</strong></th>
                                <th style="text-align: center" rowspan="1"><strong>No Indikator</strong></th>
                                <th style="text-align: center" rowspan="1"><strong>Target Unida</strong></th>
                                <th style="text-align: center" rowspan="1"><strong>Bidang WR</strong></th>
                                <th style="text-align: center" rowspan="1"><strong>Ceklis</strong></th>

                            </tr>

                            <?php
                            foreach ($capaian_kinerja as $capaian) :

                                $list_capaian = CapaianPeriode::find()->where([
                                    'periode_id' => $model->id,
                                    'capaian_kinerja_id' => $capaian->id,
                                ])->one();

                                if (isset($list_capaian)) {
                                    $checked = 'checked';
                                } else {
                                    $checked = '';
                                }
                            ?>
                                <tr>
                                    <td><?= $capaian->sasaran_stategis ?></td>
                                    <td style="text-align: center"><?= $capaian->no_indikator ?></td>
                                    <td><?= $capaian->target_unida ?></td>
                                    <td style="text-align: center"><?= isset($capaian->bidang_wr) ? MyHelper::getBidangRektor($capaian->bidang_wr) : "-" ?></td>
                                    <td style="text-align: center">
                                        <input type="checkbox" name="periode_id[]" data-capaian="<?= $capaian->id; ?>" class="cb_periode" value="<?= $capaian->id; ?>" <?= $checked ?>>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).on("click", ".cb_periode", function() {

        let capaian_id = $(this).data("capaian")
        var obj = new Object
        obj.capaian_kinerja_id = capaian_id
        obj.periode_id = `<?= $model->id ?>`
        var selector = $(this)

        if ($(this).is(":checked")) {
            obj.keperluan = 'input'
        } else {
            obj.keperluan = 'hapus'
        }
        // console.log(obj);

        $.ajax({
            url: "/periode/ajax-periode",
            type: "POST",
            data: obj,
            beforeSend: function() {
                selector.next().show()
            },
            error: function(e) {
                console.log(e.responseText)
                selector.next().hide()
            },
            success: function(data) {
                // console.log(data);
                selector.next().hide()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    })

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })


    })
</script>
<?php JSRegister::end() ?>