<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Auditor */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Auditors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>

                <?= Html::button('<i class="fa fa-send"></i> Create & Send Account', ['class' => 'btn btn-primary', 'id' => 'btn-send', 'data-unit' => $model->id]) ?>

                <form action="" id="form-unit">
                    <?= Html::hiddenInput('id', $model->id, ['id' => 'id_unit']) ?>
                </form>

            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nomor_registrasi',
                        'nama',
                        'kode_unik',
                        'nomor_sk',
                        'tanggal_sk',
                        'email:email',
                        'niy',
                        'prodi',
                        'nidn',
                        'pangkat',
                        'jabfung',
                        'status_aktif',
                        // 'is_sent',
                        [
                            'label' => 'Konfirmasi Akun',
                            'format' => 'raw',
                            'value' => function ($data) {

                                $sent = 'belum terkirim';
                                $label = 'label label-danger';
                                if ($data->is_sent != null) {
                                    $sent = 'sudah terkirim';
                                    $label = 'label label-primary';
                                }

                                return Html::tag('span', $sent, ['class' => $label]);
                            }
                        ],
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-send", function(e) {
        e.preventDefault();

        var obj = $("#form-unit").serialize()

        $.ajax({
            url: "/auditor/ajax-send",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Sending Email..",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        window.location.reload()
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });
</script>
<?php JSRegister::end(); ?>