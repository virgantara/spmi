<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $model app\models\Auditor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <?= $form->field($model, 'nomor_registrasi')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'nama')->textInput(['class' => 'form-control', 'id' => 'nama_auditor']) ?>
    <?= $form->field($model, 'email')->textInput(['class' => 'form-control', 'id' => 'email']) ?>
    <?= $form->field($model, 'nidn')->textInput(['class' => 'form-control', 'id' => 'niy']) ?>
    <?= $form->field($model, 'niy')->textInput(['class' => 'form-control', 'id' => 'niy']) ?>

    <?= $form->field($model, 'nomor_sk')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'prodi')->textInput(['class' => 'form-control', 'maxlength' => true, 'id' => 'prodi']) ?>
    <?= $form->field($model, 'tanggal_sk')->widget(DatePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>
    <?= $form->field($model, 'status_aktif')->radioList(\app\helpers\MyHelper::getStatusAktif()) ?>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    <?php ActiveForm::end(); ?>

</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).bind("keyup.autocomplete", function() {

        $('#nama_auditor').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });
</script>
<?php JSRegister::end(); ?>