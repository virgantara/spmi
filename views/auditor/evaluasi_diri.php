<?php

?>

<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Asesmen Kecukupan</h2>
            <!-- <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul> -->
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <p>Pada bagian ini dapat diisi mengenai penjelasan untuk pengisian asesmen kecukupan</p>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Kelompok </th>
                            <th class="column-title">Nomor </th>
                            <th class="column-title">Indikator </th>
                            <th class="column-title">Skor 1 </th>
                            <th class="column-title">Skor 2 </th>
                            <th class="column-title">Skor 3 </th>
                            <th class="column-title">Skor 4 </th>
                            <th class="column-title">Link Bukti Penunjang </th>
                            <th class="column-title">Skor ED </th>
                            <!-- <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td>
                                <input type="text" class="flat" name="table_records">
                                <button>Input</button>
                            </td>
                            <td>
                                <input type="text">
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>