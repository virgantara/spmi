<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuditorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auditor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomor_registrasi') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kode_unik') ?>

    <?= $form->field($model, 'nomor_sk') ?>

    <?php // echo $form->field($model, 'tanggal_sk') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'niy') ?>

    <?php // echo $form->field($model, 'prodi') ?>

    <?php // echo $form->field($model, 'nidn') ?>

    <?php // echo $form->field($model, 'pangkat') ?>

    <?php // echo $form->field($model, 'jabfung') ?>

    <?php // echo $form->field($model, 'status_aktif') ?>

    <?php // echo $form->field($model, 'is_sent') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
