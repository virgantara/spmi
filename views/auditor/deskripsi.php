<div class="col-md-6 ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Deskripsi Temuan</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form class="form-label-left">
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Auditee </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Ka Prodi">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Kriteria </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Keuangan">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Lokasi </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Hall Lt.2">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Ruang Lingkup </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Keuangan">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Tanggal Audit </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Hari, 10 November 2022">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Wakil Auditee </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Bambang">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Auditor Ketua </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Febrian Arif Wicaksana">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Auditor Anggota </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Veri Setiawan, M.I.Kom.">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 "></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Dr. Agus Budiman, M.Pd.">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 "></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Ahmad Farid Saifuddin, M.Ag.">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 "></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Mufti Afif, M.A.">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Uraian Ketidaksesuaian (KTS) </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Jabatan akademik pembimbing dan penguji skripsi belum memenuhi standar minimal (Asisten Ahli)">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Referensi </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Standar penelitian dan PkM ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Kriteria </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Pendidikan">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Akar Penyebab </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Sebagian besar dosen adalah dosen baru yang belum mempunyai jabatan akademik">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Rekomendasi </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Memfasilitasi bagi dosen baru untuk mengurus kepangkatan">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Kategori </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Observasi">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Jadwal Perbaikan </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="1 Tahun">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 ">Penanggung Jawab </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" class="form-control" disabled="disabled" placeholder="Kaprodi Ilmu Komunikasi">
                    </div>
                </div>
        </div>
    </div>
</div>