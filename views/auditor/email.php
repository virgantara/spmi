<?php 
use yii\helpers\Html;
use yii\helpers\Url;
 ?>
<!doctype html>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="initial-scale=1.0" />
  <meta name="format-detection" content="telephone=no" />
  <title></title>
<body class="bg-light">
  <div class="container">
<!--     <img class="ax-center my-10 w-24" src="https://assets.bootstrapemail.com/logos/light/square.png" /> -->
    <div class="card p-6 p-lg-10 space-y-4">
      <h1 class="h3 fw-700">
        Auditor Account Activation
      </h1>
      <p>
        Dear, <b><?=$user->nama;?></b><br>
        <br>
        You have been assigned as <b>AUDITOR</b> for Sistem Penjaminan Mutu Internal (SIMUDA) Universitas Darussalam Gontor. 
        <br>
        <br>
        Here is your account information:<br>
        Username: <?=$user->email?><br>
        Password: <b><?=$password?></b>
      </p>
        <?php

        $loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
        ?>

        <?= Html::a('Please, click here to log into your account.', $loginLink) ?><br>
        Note: Please don't share your account to anyone.
    </div>
    <div class="text-muted text-center my-6">
      UPT PPTIK<br>
      Universitas Darussalam Gontor
    </div>
  </div>
</body>

 </html>