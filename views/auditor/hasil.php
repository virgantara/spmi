<div class="col-md-6 col-sm-6 text-center">
    <div class="x_panel">
        <div class="x_title text-center">
            <h3>Rekap Asesmen Kecukupan</h3>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix">
                <div class="x_content">
                    <h4>Pendoman Nilai Asesmen Lapangan</h4>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>0 - 191</td>
                                <td>Tidak Layak</td>
                            </tr>
                            <tr>
                                <td>191 - 380</td>
                                <td>Layak Asesmen Lapangan</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="x_content">
            <table class="table table-striped ">
                <thead class="text-center">
                    <tr>
                        <th class="text-center">Kriteria</th>
                        <th class="text-center">Auditor 1</th>
                        <th class="text-center">Auditor 2</th>
                        <th class="text-center">Auditor 3</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>0</td>
                        <td>200</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><strong>Total Skor</strong></td>
                        <td><strong>0</strong></td>
                        <td><strong>300</strong></td>
                        <td><strong>400</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Nilai AK (Rata-rata)</strong></td>
                        <td><strong>233.33</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-6 col-sm-6 text-center">
    <div class="x_panel">
        <div class="x_title text-center">
            <h3>Rekap Asesmen Lapangan</h3>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix">
                <div class="x_content">
                    <h4>Pendoman Nilai Asesmen Lapangan</h4>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>0 - 95</td>
                                <td>Tidak Layak</td>
                            </tr>
                            <tr>
                                <td>96 - 190</td>
                                <td>Baik</td>
                            </tr>
                            <tr>
                                <td>191 - 285</td>
                                <td>Sangat Baik</td>
                            </tr>
                            <tr>
                                <td>286 - 380</td>
                                <td>Unggul</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="x_content">
            <table class="table table-striped ">
                <thead class="text-center">
                    <tr>
                        <th class="text-center">Kriteria</th>
                        <th class="text-center">Auditor 1</th>
                        <th class="text-center">Auditor 2</th>
                        <th class="text-center">Auditor 3</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>0</td>
                        <td>200</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td><strong>Total Skor</strong></td>
                        <td><strong>0</strong></td>
                        <td><strong>300</strong></td>
                        <td><strong>400</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Nilai AK (Rata-rata)</strong></td>
                        <td><strong>233.33</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 text-center">
    <div class="x_panel">
        <div class="x_title text-center">
            <h3>Nilai Akhir</h3>
            <div class="clearfix">
                <div class="x_content">
                    <h4>Nilai Akhir Asesmen Lapangan</h4>
                    <table class="table table-striped ">
                        <thead class="text-center">
                            <tr>
                                <th class="text-center" colspan="3">AL = Nilai AMI</th>
                                <th class="text-center">Status Peringkat</th>
                                <th class="text-center">Apresiasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>0</td>
                                <td>s/d</td>
                                <td>95</td>
                                <td>Tidak Layak</td>
                                <td>Innalillahi</td>
                            </tr>
                            <tr>
                                <td>96</td>
                                <td>s/d</td>
                                <td>190</td>
                                <td>Baik</td>
                                <td>Astagfirullah</td>
                            </tr>
                            <tr>
                                <td>191</td>
                                <td>s/d</td>
                                <td>289</td>
                                <td>Sangat Baik</td>
                                <td>Alhamdulillah</td>
                            </tr>
                            <tr>
                                <td>286</td>
                                <td>s/d</td>
                                <td>380</td>
                                <td>Unggul</td>
                                <td>Tabarakallah</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="x_content">
            <table class="table table-striped ">
                <thead class="text-center">
                    <tr>
                        <th class="text-center">Nilai AMI</th>
                        <th class="text-center">Status Peringkat</th>
                        <th class="text-center">Apresiasi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>9</td>
                        <td>Tidak Layak</td>
                        <td>Innalillahi</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>