<?php

use yii\helpers\Html;

?>
<div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Default Example <small>Users</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <?= Html::a('<i class="fa fa-file"></i> Print PDF', ['#'], ['class' => 'btn btn-danger', 'id' => 'btn-add']) ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                        </p>
                        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Kriteria</th>
                                    <th>Indikator</th>
                                    <th>Temuan</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Kriteria 3</td>
                                    <td>Jurnal terakreditasi</td>                                    
                                    <td>ternyata ditemukan</td>                                    
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>