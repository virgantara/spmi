<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use kartik\date\DatePicker;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Auditee Internal';
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <p>
                    <button class="delete-selected btn btn-danger"><i class="fa fa-trash"></i> Delete Selected</button>
                    <?= Html::a('<i class="fa fa-plus"></i> Add Auditee', ['create'], ['class' => 'btn btn-success', 'id' => 'btn-add']) ?>
                </p>
                <?php
                $gridColumns = [
                    ['class' => '\kartik\grid\CheckboxColumn'],
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    'nama',
                    'nomor_registrasi',
                    'nomor_sk',
                    'tanggal_sk',
                    // 'email',
                    // 'pangkat',
                    // 'jabfung',
                    'prodi',
                    'niy',
                    'nidn',
                    [
                        'attribute' => 'status_aktif',
                        'filter' => $list_aktif,
                        'value' => function ($data) use ($list_aktif) {
                            return (!empty($list_aktif[$data->status_aktif]) ? $list_aktif[$data->status_aktif] : null);
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>



<?php


yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs">
    <div class="form-group">
        <label for="">Nama Auditee</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control', 'id' => 'nama_auditor', 'placeholder' => 'Ketik nama dosen', 'autocomplete' => 'off']) ?>
        <?= Html::hiddenInput('kode_unik', '', ['id' => 'kode_unik']) ?>
        <?= Html::hiddenInput('email', '', ['id' => 'email']) ?>
        <?= Html::hiddenInput('niy', '', ['id' => 'niy']) ?>
        <?= Html::hiddenInput('prodi', '', ['id' => 'prodi']) ?>
        <?= Html::hiddenInput('fakultas', '', ['id' => 'fakultas']) ?>
        <?= Html::hiddenInput('nidn', '', ['id' => 'nidn']) ?>
        <?= Html::hiddenInput('pangkat', '', ['id' => 'pangkat']) ?>
        <?= Html::hiddenInput('jabfung', '', ['id' => 'jabfung']) ?>
    </div>
    <div class="form-group">
        <label for="">Nomor Registrasi</label>
        <?= Html::textInput('nomor_registrasi', '', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <label for="">Nomor SK</label>
        <?= Html::textInput('nomor_sk', '', ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <label for="">Tgl SK</label>
        <?= DatePicker::widget([
            'name' => 'tanggal_sk',
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]) ?>
    </div>
    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(".delete-selected").click(function(e) {
        var keys = $('#my-grid').yiiGridView('getSelectedRows');
        e.preventDefault();

        Swal.fire({
            title: 'Penghapusan Auditor!',
            text: "Data Auditor yang dihapus tidak bisa dikembalikan. Anda yakin ingin menghapus data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                var obj = new Object;
                obj.keys = keys;
                $.ajax({

                    type: "POST",
                    url: "/auditor/delete-multiple",
                    data: {
                        dataPost: obj
                    },

                    beforeSend: function() {
                        Swal.fire({
                            title: "Please wait",
                            html: "Processing your request...",
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            },

                        })
                    },
                    error: function(e) {
                        Swal.close()
                    },
                    success: function(data) {
                        Swal.close()
                        var data = $.parseJSON(data)


                        if (data.code == 200) {
                            Swal.fire({
                                title: 'Yeay!',
                                icon: 'success',
                                text: data.message
                            });

                            $.pjax.reload({
                                container: '#pjax-container',
                                async: true
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: data.message
                            });

                        }
                    }
                })
            }
        });
    });

    $(document).bind("keyup.autocomplete", function() {

        $('#nama_auditor').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-krs").serialize()

        console.log(obj)

        $.ajax({
            url: "/auditor/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                    $("#nama_auditor").val("").focus()
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        $("#modal").modal("show")


    });
</script>
<?php JSRegister::end(); ?>