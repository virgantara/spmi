<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auditor */

$this->title = 'Update Profile Auditor: ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Profil', 'url' => ['user/profil']];
$this->params['breadcrumbs'][] = 'Update Profil Auditor';
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?= $this->render('_form_profil', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>