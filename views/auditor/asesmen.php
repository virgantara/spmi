<?php

?>

<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h2>Asesmen Kecukupan</h2>
            <!-- <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul> -->
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <p>Pada bagian ini dapat diisi mengenai penjelasan untuk pengisian asesmen kecukupan</p>
            <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title text-center">Kelompok </th>
                            <th class="column-title text-center">Nomor </th>
                            <th class="column-title text-center">Indikator </th>
                            <th class="column-title text-center">Skor 1 </th>
                            <th class="column-title text-center">Skor 2 </th>
                            <th class="column-title text-center">Skor 3 </th>
                            <th class="column-title text-center">Skor 4 </th>
                            <th class="column-title text-center">Link Bukti Penunjang </th>
                            <th class="column-title text-center">Skor ED </th>
                            <th class="column-title text-center">Tanggapan Auditor </th>
                            <th class="column-title text-center">Skor AK </th>
                            <th class="column-title text-center">Skor AL </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>
                        <tr class="even pointer">
                            <!-- <td class="a-center ">
                                <input type="checkbox" class="flat" name="table_records">
                            </td> -->
                            <td class=" ">Kriteria 1</td>
                            <td class=" ">1.1 </td>
                            <td class=" ">Sitasi karya ilmiah dosen </td>
                            <td class=" ">Sitasi karya ilmiah dosen 25%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 50%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 75%</td>
                            <td class=" ">Sitasi karya ilmiah dosen 100%</td>
                            <td class="text-center"> <a href="">View</a> </td>
                            <td class="text-center">3 </td>
                            <td> Ini sudah baik </td>
                            <td>2 </td>
                            <td>3 </td>

                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>