<?php

use app\models\Ami;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

$amiDatas = Ami::find()->all();

$items = [];
foreach ($amiDatas as $key => $ami) {
    $items[] = [
        'label' => $ami->nama,
        'url' => [
            'indikator/data',
            'ami_id' => $ami->id
        ],
        'linkOptions' => [
            'target' => '_blank'
        ],
    ];
}
?>

<div class="custom-tabs-line tabs-line-bottom left-aligned">

    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs',
            'role' => 'tablist'
        ],
        'items' => $items,
    ]);
    ?>

</div>