<?php

use yii\widgets\Menu;
?>

<div class="custom-tabs-line tabs-line-bottom left-aligned">

    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs',
            'role' => 'tablist'
        ],
        'items' => [
            [
                'label' => 'Rencana Operasional (RENOP)',
                'url' => ['monev-unit-kerja-objek/rencana-operasional', 'id' => $id],
            ],
            [
                'label' => 'Audit Tindak Lanjut',
                'url' => ['monev-unit-kerja-objek/audit-tindak-lanjut', 'id' => $id],
                // 'url' => 'javascript:void(0)',

            ],
            [
                'label' => 'Amanah Kinerja',
                'url' => ['monev-unit-kerja-objek/amanah-kinerja', 'id' => $id],

            ],
        ],
    ]);
    ?>

</div>