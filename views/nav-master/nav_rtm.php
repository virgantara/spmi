<?php

use yii\widgets\Menu;
?>

<div class="custom-tabs-line tabs-line-bottom left-aligned">

    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs',
            'role' => 'tablist'
        ],
        'items' => [
            [
                'label' => 'Bahan RTM', 'url' => ['rtm/view', 'id' => $id],
            ],
            [
                'label' => 'Rapat Tinjauan', 'url' =>
                'javascript:void(0)',
                // ['rtm/rapat-tinjauan', 'id' => $id],
            ],
            [
                'label' => 'Tindak Lanjut', 'url' =>
                'javascript:void(0)',
                // ['rtm/tindak-lanjut', 'id' => $id],
            ],
        ],
    ]);
    ?>

</div>