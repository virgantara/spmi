<?php

use yii\widgets\Menu;
?>

<div class="custom-tabs-line tabs-line-bottom left-aligned">

    <?= Menu::widget([
        'options' => [
            'class' => 'nav nav-tabs',
            'role' => 'tablist'
        ],
        'items' => [
            [
                'label' => 'Asesmen', 'url' => ['asesmen/proses', 'id' => $id],
            ],
            [
                'label' => 'Detail', 'url' => ['asesmen/detail', 'id' => $id],
            ],
            [
                'label' => 'Rencana', 'url' => ['asesmen/rencana', 'id' => $id],
            ],
            [
                'label' => 'Persetujuan', 'url' => ['asesmen/persetujuan', 'id' => $id],
            ],
            [
                'label' => 'Penilaian', 'url' => ['asesmen/penilaian', 'id' => $id],
            ],
            [
                'label' => 'Daftar Tilik', 'url' => ['tilik/index', 'id' => $id],
                'visible' => Yii::$app->user->can('auditor'),
            ],
            [
                'label' => 'Daftar Temuan', 'url' => ['temuan/index', 'id' => $id],
            ],
            [
                'label' => 'Audit Tindak Lanjut', 'url' => ['temuan/audit-tindak-lanjut', 'id' => $id],
            ],
            [
                'label' => 'Laporan', 'url' => ['ami-laporan/index', 'id' => $id],
            ],
            [
                'label' => 'Hasil Ami', 'url' => ['asesmen/hasil', 'id' => $id],
                'visible' => false
            ],
            [
                'label'     => 'Master', 'url' => ['asesmen/master-input', 'id' => $id],
                'visible'   => Yii::$app->user->can('admin')
            ],
        ],
    ]);
    ?>

</div>