<?php

use app\helpers\MyHelper;
use app\models\AmanahKinerjaUnitKerja;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmanahKinerja */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Amanah Kinerjas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'id' => 'btn-delete'
                ]) ?>
                <?= Html::a('<i class="fa fa-refresh"></i> ' . Yii::t('app', 'Sync'), 'javascript:void(0)', [
                    'class' => 'btn btn-primary btn-sync',
                    'data-amanah-id' => $model->id,
                    'title' => 'Sinkronkan dengan template amanah kinerja',
                ]) ?>

            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nama',
                        'tahun',
                        [
                            'attribute' => 'status_aktif',
                            'value' => function ($model) {
                                return MyHelper::getStatusAktif()[$model->status_aktif];
                            }
                        ],
                    ],
                ]) ?>

                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title text-center">No.</th>
                                <th class="column-title text-center">Nama Satuan Kerja/Fakultas</th>
                                <th class="column-title text-center">Jenis</th>
                                <th class="column-title text-center">Amanah Kinerja </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nomor = 1;
                            foreach ($unitKerja as $unit) :

                                $totalAmanah = AmanahKinerjaUnitKerja::find()->where(['unit_kerja_id' => $unit->id, 'amanah_kinerja_id' => $model->id])->count();
                                $totalIsian = AmanahKinerjaUnitKerja::find()->where(['unit_kerja_id' => $unit->id, 'amanah_kinerja_id' => $model->id])->andWhere(['<>', 'capaian', null])->count();
                            ?>
                                <tr class="even pointer">
                                    <td class="text-center"><?= $nomor; ?>.</td>
                                    <td class=" "><?= $unit->nama; ?> </td>
                                    <td class=" "><?= MyHelper::getJenisUnit()[$unit->jenis] ?> </td>
                                    <td class="text-center">
                                        <?= Html::a('<i class="fa fa-eye"></i> Lihat', [
                                            'amanah-kinerja-unit-kerja/detail',
                                            'unit_kerja_id' => $unit->id,
                                            'amanah_kinerja_id' => $model->id,
                                        ], [
                                            'class' => 'btn btn-sm btn-success',
                                            'title' => 'Lihat Amanah Kinerja',
                                            'data-pjax' => '0',
                                        ]); ?>
                                        <?= Html::a($totalIsian . '/' . $totalAmanah, 'javascript:void(0)', [
                                            'class' => 'btn btn-sm ' . ($totalAmanah == $totalIsian ? 'btn-success' : 'btn-danger'),
                                            'title' => 'Jumlah isian amanah kinerja',
                                            'data-pjax' => '0',
                                        ]); ?>

                                    </td>
                                </tr>
                            <?php
                                $nomor++;
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>


<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        $('#btn-delete').click(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin ingin menghapus item ini? Tindakan ini akan menghapus seluruh data amanah kinerja seluruh satuan kerja/fakultas.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: 'Konfirmasi',
                        text: 'Untuk melanjutkan, ketik "delete":',
                        input: 'text',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        cancelButtonText: 'Cancel',
                        inputValidator: (value) => {
                            if (value !== 'delete') {
                                return 'Input tidak valid. Ketik "delete" untuk melanjutkan.';
                            }
                        }
                    }).then((inputResult) => {
                        if (inputResult.isConfirmed) {
                            $.ajax({
                                type: "POST",
                                url: "<?= Url::to(['delete', 'id' => $model->id]) ?>",
                                data: {
                                    _csrf: yii.getCsrfToken()
                                },
                                success: function(data) {
                                    var parsedData = $.parseJSON(data);

                                    if (parsedData.code == 200) {
                                        Swal.fire({
                                            title: 'Berhasil!',
                                            icon: 'success',
                                            text: parsedData.message
                                        }).then(function() {
                                            window.location.href = '<?= Url::to(['index']) ?>';
                                        });

                                    } else {
                                        Swal.fire({
                                            title: 'Gagal!',
                                            icon: 'error',
                                            text: parsedData.message
                                        });
                                    }
                                },
                                error: function(xhr, status, error) {
                                    Swal.fire("Error!", "Gagal menghapus data!", "error");
                                },
                            });
                        }
                    });
                }
            });
        });
    });

    $(document).on("click", ".btn-sync", function(e) {

        amanah_id = $(this).data('amanah-id');

        $.ajax({
            url: "/amanah-kinerja/ajax-sync",
            type: "POST",
            async: true,
            data: {
                amanah_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)

                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        });

    });
</script>
<?php JSRegister::end() ?>