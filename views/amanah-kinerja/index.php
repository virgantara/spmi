<?php

use app\helpers\MyHelper;
use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmanahKinerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Amanah Kinerjas');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?php if (Yii::$app->user->can('admin')) : ?>
                    <p>
                        <?= Html::a(Yii::t('app', 'Create Amanah Kinerja'), ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                <?php endif; ?>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'nama',
                        'format' => 'ntext',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 3, // Jumlah baris dalam textarea
                                'placeholder' => 'Masukkan nama',
                            ],
                        ],
                    ],

                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'tahun',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'contentOptions' => [
                            'width' => '12%',
                        ],
                        'hAlign' => 'center',
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::getYears(),
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => 'Pilih tahun',
                            ],
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'filter' => MyHelper::getStatusAktif(),
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'contentOptions' => [
                            'width' => '12%',
                        ],
                        'hAlign' => 'center',
                        'attribute' => 'status_aktif',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::getStatusAktif(),
                            'options' => [
                                'placeholder' => 'Pilih status',
                            ],
                        ],
                        'value' => function ($data) {
                            return MyHelper::getStatusAktif()[$data->status_aktif];
                        }
                    ],
                    [
                        'label' => 'Amanah Kinerja',
                        'format' => 'raw',
                        'contentOptions' => [
                            'width' => '12%',
                        ],
                        'hAlign' => 'center',
                        'value' => function ($data) {
                            // Tentukan URL berdasarkan hak akses pengguna
                            $url = Yii::$app->user->can('admin')
                                ? ['view', 'id' => $data->id]
                                : ['amanah-kinerja-unit-kerja/detail', 'unit_kerja_id' => Yii::$app->user->identity->unit_kerja_id, 'amanah_kinerja_id' => $data->id];

                            // Kembalikan tautan dengan ikon dan teks
                            return Html::a('<i class="fa fa-eye"></i> Lihat', $url, [
                                'class' => 'btn btn-sm btn-success',
                                'title' => 'Lihat Amanah Kinerja',
                                'data-pjax' => '0',
                            ]);
                        }
                    ],

                    // 'created_at',
                    // 'updated_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'visible' => Yii::$app->user->can('theCreator'),
                    ],
                    [
                        'label' => '',
                        'visible' => !Yii::$app->user->can('theCreator'),
                        'value' => function ($model) {
                            return '';
                        },
                        'contentOptions' => [
                            'class' => 'text-center',
                            'style' => 'width: 1px;',
                        ],
                    ],

                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>