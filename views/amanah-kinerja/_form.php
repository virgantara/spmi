<?php

use app\helpers\MyHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmanahKinerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Tahun</label>
        <?= $form->field($model, 'tahun', ['options' => ['tag' => false]])->widget(Select2::classname(), [
            'data'    => MyHelper::getYears(),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Tahun -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status aktif</label>
        <?= $form->field($model, 'status_aktif')->radioList(MyHelper::getStatusAktif(), ['value' => 1])->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>