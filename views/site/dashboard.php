<?php

/** @var yii\web\View $this */

use app\helpers\MyHelper;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

$this->title = Yii::$app->name;

$listTingkatLembagaAkreditasi = MyHelper::listTingkat();
?>
<style>
    table.jambo_table > tbody > tr:nth-of-type(n+6) {
        display: none;
    }
</style>

<h2><strong>SIMUDA UNIDA Gontor</strong></h2>


<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h3>Informasi Akreditasi</h3>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Terakreditasi Unggul</span>
                    <div class="count "><a href="javascript:void(0)" id="count_U" data-item="U" class="list_prodi green">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-clock-o"></i> Terakreditasi A</span>
                    <div class="count "><a href="javascript:void(0)" id="count_A" data-item="A" class="list_prodi green">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Terakreditasi Baik Sekali</span>
                    <div class="count "><a href="javascript:void(0)" id="count_BS" data-item="BS" class="list_prodi orange">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Terakreditasi B</span>

                    <div class="count "><a href="javascript:void(0)" id="count_B" data-item="B" class="list_prodi orange">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Terakreditasi Baik</span>
                    <div class="count "><a href="javascript:void(0)" id="count_BK" data-item="BK" class="list_prodi red">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Terakreditasi C</span>
                    <div class="count "><a href="javascript:void(0)" id="count_C" data-item="C" class="list_prodi red">0</a></div>
                    <span class="count_bottom"><i class="green"></i> Prodi</span>
                </div>
            </div>

            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Persentase Terakreditasi A/Unggul</span>
                    <div class="count "><a href="javascript:void(0)" id="count_U_persen" class="blue">0</a></div>
                    <!-- <span class="count_bottom"><i class="green"></i> </span> -->
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Persentase Terakreditasi B/Baik Sekali</span>
                    <div class="count "><a href="javascript:void(0)" id="count_BS_persen" class="green">0</a></div>
                    <!-- <span class="count_bottom"><i class="green"></i> Prodi</span> -->
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Persentase Terakreditasi C/Baik</span>
                    <div class="count "><a href="javascript:void(0)" id="count_B_persen" class="yellow">0</a></div>
                    <!-- <span class="count_bottom"><i class="green"></i> Prodi</span> -->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
        <div class="x_title">
            <h3>Monitoring Akreditasi Program Studi</h3>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="table-responsive">
                <?= Html::button('Show All', ['class' => 'btn btn-xs btn-info', 'id' => 'toggleRowsButton']) ?>
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title text-center">No </th>
                            <th class="column-title">Program Studi</th>
                            <th class="column-title text-center">Jenjang </th>
                            <th class="column-title text-center">Tingkat Akreditasi </th>
                            <th class="column-title text-center">Lembaga Akreditasi </th>
                            <th class="column-title text-center">Akreditasi </th>
                            <th class="column-title text-center">Tanggal SK </th>
                            <th class="column-title">Nomor SK </th>
                            <th class="column-title text-center">Tanggal Kadaluarsa </th>
                            <th class="column-title text-center">Kadaluarsa dalam</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($akreditasi as $item) : ?>
                            <tr class="odd pointer">
                                <td class="text-center"><?= $no ?></td>
                                <td class=" "><?= $item->prodi->nama_prodi ?></td>
                                <td class="text-center"><?= $item->prodi->kode_jenjang ?></td>
                                <td class="text-center"><?= $listTingkatLembagaAkreditasi[$item->lembagaAkreditasi->tingkat] ?></td>
                                <td class="text-center"><?= $item->lembagaAkreditasi->singkatan_lembaga ?></td>
                                <td class="text-center"><strong><?= (isset($item->status_akreditasi) ? MyHelper::listAkreditasi()[$item->status_akreditasi] : null) ?></strong></td>
                                <td class="text-center"><?= $item->tanggal_sk ?></td>
                                <td class=" "><?= $item->nomor_sk ?></td>
                                <td class="text-center"><a href="#"><?= $item->tanggal_kadaluarsa ?></a>
                                <td class="text-center" style="color:<?= MyHelper::getColorLevel(MyHelper::getCekKadaluarsa($item->tanggal_kadaluarsa, true)) ?>;">
                                    <?= MyHelper::getCekKadaluarsa($item->tanggal_kadaluarsa) ?>
                                </td>
                            </tr>
                        <?php $no++;
                        endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    // 'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<div class="clearfix">
<table class="table table-striped table-hover" id="tabel_prodi">
    <thead>
        <tr>
            <th>No</th>
            <th>Prodi</th>
            <th>Akreditasi</th>
            <th>Tgl SK</th>
            <th>Nomor SK</th>
            <th>Tgl Kadaluarsa</th>
            <th>SK</th>
        </tr>
    </thead>
    <tbody>
       
    </tbody>
</table></div>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    getListAkreditasi()

    countUpcomingExpired(365)
    countUpcomingExpired(180)

    function getListAkreditasi() {

        $.ajax({
            url: "/prodi/ajax-count-akreditasi-prodi",
            type: "POST",
            async: true,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                let total_unggul = 0
                let total_baik_sekali = 0
                let total_baik = 0

                $.each(hasil, function(i, obj) {


                    if (obj.akreditasi == "A" || obj.akreditasi == "U") {
                        total_unggul += eval(obj.total)
                    } else if (obj.akreditasi == "B" || obj.akreditasi == "BS") {
                        total_baik_sekali += eval(obj.total)
                    } else if (obj.akreditasi == "C" || obj.akreditasi == "BK") {
                        total_baik += eval(obj.total)
                    }

                    $("#count_" + obj.akreditasi).html(obj.total)
                })

                let total_item = eval(total_unggul + total_baik_sekali + total_baik)

                let persen_u = Math.round(Math.round(total_unggul / total_item * 100) / 100 * 100,2)
                let persen_bs = Math.round(Math.round(total_baik_sekali / total_item * 100) / 100 * 100,2)
                let persen_b = Math.round(Math.round(total_baik / total_item * 100) / 100 * 100,2)

                $("#count_U_persen").html(persen_u + " %")
                $("#count_BS_persen").html(persen_bs + " %")
                $("#count_B_persen").html(persen_b + " %")
            }
        })
    }

    function countUpcomingExpired(jumlah_hari) {
        var obj = new Object
        obj.jumlah_hari = jumlah_hari
        $.ajax({
            url: "/prodi/ajax-count-upcoming-expired",
            type: "POST",
            data: {
                dataPost: obj
            },
            async: true,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)

            }
        })
    }

    $(document).on("click", ".list_prodi", function(e) {
        e.preventDefault();
        var obj = new Object
        obj.akreditasi = $(this).data("item")

        $.ajax({
            url: "/prodi/ajax-list-akreditasi-prodi",
            type: "POST",
            data: {
                dataPost: obj
            },
            async: true,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                $("#tabel_prodi > tbody").empty()
                let row = ""
                $.each(hasil, function(i, obj) {
                    row += "<tr>"
                    row += "<td>" + eval(i + 1) + "</td>"
                    row += "<td>" + obj.nama_prodi + "</td>"
                    row += "<td>" + obj.akreditasi + "</td>"
                    row += "<td>" + obj.tanggal_sk + "</td>"
                    row += "<td>" + obj.nomor_sk + "</td>"
                    row += "<td>" + obj.tanggal_kadaluarsa + "</td>"
                    row += "<td><a target='_blank' href='/akreditasi/download-sk?id="+obj.id+"'><i class='fa fa-download'></i> Unduh SK</a></td>"
                    row += "</tr>"
                    
                })

                $("#tabel_prodi > tbody").append(row)
                $("#modal").modal("show")
            }
        })
    });

    $(document).ready(function() {
        $('#toggleRowsButton').on('click', function() {
            const $tableRows = $('.table tbody tr:nth-of-type(n+6)');
            $tableRows.toggle(); // Toggle visibility of rows starting from 6th

            if ($(this).text() === 'Show All') {
                $(this).text('Show Less');
            } else {
                $(this).text('Show All');
            }
        });
    });
</script>
<?php JSRegister::end(); ?>