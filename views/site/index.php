<?php

/** @var yii\web\View $this */

use app\helpers\MyHelper;

$this->title = Yii::$app->name;
$theme = Yii::$app->view->theme->baseUrl;
?>
<style>
    .responsive {
        width: 100%;
        height: auto;
    }
</style>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-8 col-lg-offset-2 text-center">
        
        <img class="responsive" src="<?= $theme ?>/images/simuda.jpg" alt="Logo SIMUDA">
        <!-- Nomor Registrasi Hak Cipta: EC00202307063<br> 
        No Pencatatan: 000439985 -->
    </div>
</div>

