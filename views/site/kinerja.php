<?php

/** @var yii\web\View $this */

use app\helpers\MyHelper;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

$this->title = Yii::$app->name;
$theme = Yii::$app->view->theme->baseUrl;
?>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table,
    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        vertical-align: middle;
        /* Center the content vertically */
    }

    #searchInput {
        padding: 8px;
        /* Padding untuk input */
        margin-bottom: 10px;
        /* Jarak antara input dan konten sekitarnya */
        width: 100%;
        /* Lebar input mengisi seluruh area yang tersedia */
        box-sizing: border-box;
        /* Mengikutsertakan padding dalam perhitungan lebar */
        border: 1px solid #dddddd;
        /* Garis tepi input */
        border-radius: 4px;
        /* Sudut border input */
    }
</style>

<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h3>
                Kinerja Universitas Darussalam Gontor
            </h3>
        </div>
        <div class="col-md-8">
            <div class="x_content">
                <input type="text" id="searchInput" placeholder="Cari...">
                <table class="table table-striped" id="dataTable">
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Kinerja Universitas Darussalam Gontor - Bidang Penelitian</b></h5>
                        </td>
                        <td style="text-align: center; width: 22%; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/penelitian/index',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Kinerja Universitas Darussalam Gontor - Publikasi Ilmiah</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/publikasi',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Kinerja Universitas Darussalam Gontor - Bidang Pengabdian</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/pengabdian/index',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Kinerja Universitas Darussalam Gontor - Bidang Kerjasama</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/kerjasama/index',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Mahasiswa - Sebaran Mahasiswa [Perprodi, Pertahun]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/mhs/sb',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Kecukupan Dosen</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/kecukupan',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Rasio Dosen dan Mahasiswa [Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/rasio-mahasiswa',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Rasio DTPS dan DTT [Per Prodi]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/rasio-dtt',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Jabatan Akademik Dosen [Per Prodi]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/akademik',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Jabatan Fungsional Dosen [Per Prodi]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/jabfung',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;Sumber Daya Manusia - Bimbingan Mahasiswa Dosen [Per Prodi]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/dosen/bimbingan',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Mahasiswa Baru [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/maba',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Kecukupan Dosen Penghitung Rasio [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/dpr',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Batas Maksimal Dosen Tidak Tetap [Per Prodi]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/dtt',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Rasio Dosen dan Mahasiswa [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/rasio-mahasiswa',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Jumlah lulusan [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/lulusan',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Kualifikasi Akademik Dosen Penghitung Rasio [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/lulusan',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Jabatan Akademik Dosen Penghitung Rasio [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/dpr-jabfung',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Kelulusan Tepat Waktu [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/lulusan-tw',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;IPEPA - Keberhasilan Studi [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/ipepa/lulusan-pbs',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Tata Pamong, Tata Kelola, dan Kerjasama [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/kerjasama',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Mahasiswa [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/mahasiswa',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Sumber Daya Manusia [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/sdm',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Pendidikan [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/pendidikan',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Penelitian [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/penelitian',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Pengabdian [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/pengabdian',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">
                            <h5><b>&nbsp;&nbsp;&nbsp;&nbsp;LKPS - Luaran dan Capaian Tridharma [Per Prodi, Per Periode]</b></h5>
                        </td>
                        <td style="text-align: center; vertical-align: middle;">
                            <?=
                            Html::a(
                                '<i class="fa fa-link"></i>',
                                'https://ekinerja.unida.gontor.ac.id/lkps/luaran',
                                [
                                    'class' => 'btn btn-sm btn-primary',
                                    'target' => '_blank'
                                ]
                            ) ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        // Tangkap perubahan pada input pencarian
        $('#searchInput').on('keyup', function() {
            var searchText = $(this).val().toLowerCase();
            $('#dataTable tr:gt(0)').each(function() {
                var found = false;
                $(this).find('td').each(function() {
                    var cellText = $(this).text().toLowerCase();
                    if (cellText.indexOf(searchText) !== -1) {
                        found = true;
                        return false;
                    }
                });
                if (found) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    });
</script>
<?php JSRegister::end() ?>