<?php

use app\models\JabatanRtm;
use app\models\Jenjang;
use app\models\PembagianRtm;
use app\models\Prodi;
use app\models\ProdiJenjang;
use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdiJenjangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Pembagian RTM';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $jabatanRtm = JabatanRtm::find()->orderBy('urutan ASC')->all();
                            foreach ($jabatanRtm as $jabatan) {
                            ?>
                            <th><?= $jabatan->nama; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $listUnitKerja = UnitKerja::find()->where(['not', ['jenis' => 'prodi']])->orderBy('nama ASC')->all();
                        foreach ($listUnitKerja as $unitKerja) {
                        ?>
                        <tr>
                            <th><?= $unitKerja->nama; ?></th>

                            <?php
                                foreach ($jabatanRtm as $jabatan) {
                                    $m = PembagianRtm::find()->where(['unit_kerja_id' => $unitKerja->id, 'jabatan_id' => $jabatan->id])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                            <td><input name="<?= $unitKerja->id ?>" type="radio" <?= $checked; ?> class="mycheck"
                                    data-unit="<?= $unitKerja->id; ?>" data-jabatan="<?= $jabatan->id; ?>" value="1">
                            </td>
                            <?php
                                }
                                ?>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php 


$script = ' 
$(".mycheck").change(function(){
    var unit            = $(this).data("unit")
    var jabatan         = $(this).data("jabatan")
    var obj             = new Object;
    obj.jabatan_id      = jabatan;
    obj.unit_kerja_id   = unit;
    obj.checked         = $(this).prop("checked") ? "1" : "0";

    $.ajax({
        type    : "POST",
        url     : "'.Url::to(['pembagian-rtm/ajax-pembagian-rtm']).'",
        data    : {
            dataPost: obj
        },
        success : function(data){
            console.log(data)
        }
    });
});
';


$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

?>