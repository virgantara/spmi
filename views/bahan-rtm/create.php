<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BahanRtm */

$this->title = Yii::t('app', 'Create Bahan Rtm');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rtm'), 'url' => ['rtm/index']];
$this->params['breadcrumbs'][] = ['label' => $rtm->unitKerja->nama, 'url' => ['rtm/view', 'id' => $rtm->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">


                <?= $this->render('_form', [
                    'model' => $model,
                    'rtm' => $rtm,
                    'dataTemuan' => $dataTemuan,
                ]) ?>
            </div>
        </div>
    </div>
</div>