<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Jenjang;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $rtm->bidang . ' ' . $rtm->tahun;
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>
<div class="panel">
    <div class="panel-heading">
        <br>
        <h2>RTM - <?= $this->title; ?></h2>
        <h4>Capaian Kinerja</h4>
        <br>
        <div class="custom-tabs-line tabs-line-bottom left-aligned">
            <?php

            echo \yii\widgets\Menu::widget([
                'options' => [
                    'class' => 'nav nav-tabs',
                    'role' => 'tablist'
                ],
                'items' => [
                    [
                        'label' => 'Hasil AMI', 'url' => ['hasil-rapat', 'id' => $rtm->id],
                    ],
                    [
                        'label' => 'Survey', 'url' => ['survey', 'id' => $rtm->id],
                    ],
                    [
                        'label' => 'Capaian', 'url' => ['capaian', 'id' => $rtm->id],
                        'options' => ['class' => 'active']
                    ],
                    [
                        'label' => 'Tindak Lanjut', 'url' => ['tindak-lanjut', 'id' => $rtm->id],
                    ],
                    [
                        'label' => 'Saran Perbaikan (Internal)', 'url' => ['perbaikan-mutu-internal', 'id' => $rtm->id],
                    ],
                    [
                        'label' => 'Saran Perbaikan (Eksternal)', 'url' => ['perbaikan-mutu-eksternal', 'id' => $rtm->id],
                    ],
                ],
            ]);
            ?>
        </div>

    </div>

    <div class="panel-body">


        <div class="form-group">
            <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['rtm/view', 'id' => $rtm->id], ['class' => 'btn btn-info']) ?>
            <?= Html::a('<i class="fa fa-plus"></i> Input', ['create'], ['class' => 'btn btn-success', 'id' => 'btn-add']) ?>

        </div>
        <br>

        <?php
        $gridColumns = [
            [
                'class' => 'kartik\grid\SerialColumn',
                'contentOptions' => ['class' => 'kartik-sheet-style'],
                'width' => '36px',
                'pageSummary' => 'Total',
                'pageSummaryOptions' => ['colspan' => 6],
                'header' => '',
                'headerOptions' => ['class' => 'kartik-sheet-style']
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'deskripsi',
                'label' => 'Topik Diskusi',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'asPopover' => false,
                ],
                'value' => function ($data) {
                    return ($data->deskripsi == null ? '' : $data->deskripsi);
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'tindakan',
                'label' => 'Tindakan atau Keputusan',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'asPopover' => false,
                ],
                'value'             => function ($data) {
                    return ($data->tindakan == null ? '' : $data->tindakan);
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'target_waktu',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_DATE,
                    'asPopover' => false,
                    'options' => [
                        'size' => 'md',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ],
                ],
                'value'             => function ($data) {
                    return ($data->target_waktu == null ? '' : $data->target_waktu);
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'output',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'asPopover' => false,
                ],
                'value' => function ($data) {
                    return ($data->output == null ? '' : $data->output);
                }
            ],
            [
                'attribute' => 'penanggung_jawab',
                'value' => function ($data) {
                    return ($data->penanggung_jawab == null ? '' : $data->penanggung_jawab);
                }
            ],
            [
                'attribute' => 'pelaksana',
                'value' => function ($data) {
                    return ($data->pelaksana == null ? '' : $data->pelaksana);
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'aras',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'asPopover' => false,
                ],
                'value' => function ($data) {
                    return ($data->aras == null ? '' : $data->aras);
                }
            ],
            [
                'format' => 'raw',
                'value' => function ($data) {
                    // $eye = Html::a('', ['view', 'id' => $data->id], ['class' => 'glyphicon glyphicon-eye-open']);
                    $eye = "";
                    // $pencil = Html::a('', ['fakultas'], ['class' => 'glyphicon glyphicon-pencil update_indikator', 'data-instrumen' => $data->id]);
                    $pencil = "";
                    $trash = Html::a('', ['delete', 'id' => $data->id, 'posisi' => 'capaian', 'rtm_id' => $data->rtm_id], [
                        'class' => 'glyphicon glyphicon-trash',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                    return $eye . ' ' . $pencil . ' ' . $trash;
                },
            ],
            // ['class' => 'yii\grid\ActionColumn']
        ]; ?>
        <?= GridView::widget([
            'pager' => [
                'options' => ['class' => 'pagination'],
                'activePageCssClass' => 'active paginate_button page-item',
                'disabledPageCssClass' => 'disabled paginate_button',
                'prevPageLabel' => 'Previous',
                'nextPageLabel' => 'Next',
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'nextPageCssClass' => 'paginate_button next page-item',
                'prevPageCssClass' => 'paginate_button previous page-item',
                'firstPageCssClass' => 'first paginate_button page-item',
                'lastPageCssClass' => 'last paginate_button page-item',
                'maxButtonCount' => 10,
                'linkOptions' => [
                    'class' => 'page-link'
                ]
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsiveWrap' => false,
            'columns' => $gridColumns,
            'containerOptions' => ['style' => 'overflow: auto'],
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'containerOptions' => ['style' => 'overflow: auto'],
            'beforeHeader' => [
                [
                    'columns' => [
                        ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'exportConfig' => [
                GridView::PDF => ['label' => 'Save as PDF'],
                GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
            ],

            'toolbar' =>  [
                '{export}',

                '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
            ],
            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // set export properties
            'export' => [
                'fontAwesome' => true
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container',
                ]
            ],
            'id' => 'my-grid',
            'bordered' => true,
            'striped' => true,
            // 'condensed' => false,
            // 'responsive' => false,
            'hover' => true,
            // 'floatHeader' => true,
            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
            'panel' => [
                'type' => GridView::TYPE_PRIMARY
            ],
        ]); ?>
    </div>
</div>

<?php


yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs">
    <div class="form-group">
        <label for="">Topik</label>
        <?= Html::hiddenInput('id', '', ["id" => "id"]) ?>
        <?= Html::hiddenInput('rtm_id', $rtm->id, ["id" => "rtm_id"]) ?>
        <?= Html::hiddenInput('jenis_bahan', '3', ["id" => "jenis_bahan"]) ?>
        <?= Html::textInput('deskripsi', '', ['class' => 'form-control', "id" => "topik", 'placeholder' => "Masukkan topik"]) ?>
    </div>
    <div class="form-group">
        <label for="">Tindakan atau Keputusan</label>
        <?= Html::textInput('tindakan', '', ['class' => 'form-control', "id" => "tindakan", 'placeholder' => "Masukkan tindakan"]) ?>
    </div>
    <div class="form-group">
        <label for="">Target Waktu Selesai</label>
        <?php echo DatePicker::widget([
            'name'      => 'target_waktu',
            'readonly'  => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]); ?>
    </div>
    <div class="form-group">
        <label for="">Output</label>
        <?= Html::textInput('output', '', ['class' => 'form-control', "id" => "output", 'placeholder' => "Masukkan output"]) ?>
    </div>
    <div class="form-group">
        <label for="">Penanggung Jawab</label>
        <?= Html::textInput('penanggung_jawab', '', ['class' => 'form-control', "id" => "penanggung_jawab", 'placeholder' => "Masukkan penanggung jawab"]) ?>
    </div>
    <div class="form-group">
        <label for="">Pelaksana</label>
        <?= Html::textInput('pelaksana', '', ['class' => 'form-control', "id" => "pelaksana", 'placeholder' => "Masukkan pelaksana"]) ?>
    </div>
    <div class="form-group">
        <label for="">Aras</label>
        <?= Html::textInput('aras', '', ['class' => 'form-control', "id" => "aras", 'placeholder' => "Masukkan aras"]) ?>
    </div>


    <div class="form-group">
        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>

</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-krs").serialize()

        $.ajax({
            url: "/bahan-rtm/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                        $(":input", "#modal").val("")
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        $("#modal").modal("show")
        $(".bobot_beda").hide()
    });

    $(document).on("click", ".close", function(e) {
        e.preventDefault();
        $(":input", "#modal").val("")
    });


    $(document).bind("keyup.autocomplete", function() {

        $('#penanggung_jawab, #pelaksana').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });
</script>
<?php JSRegister::end(); ?>