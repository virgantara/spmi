<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BahanRtmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bahan-rtm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'unit_id') ?>

    <?= $form->field($model, 'temuan_id') ?>

    <?= $form->field($model, 'rtm_id') ?>

    <?= $form->field($model, 'aspek_id') ?>

    <?php // echo $form->field($model, 'lingkup_bahasan_id') ?>

    <?php // echo $form->field($model, 'jenis_bahan') ?>

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'tindakan') ?>

    <?php // echo $form->field($model, 'target_waktu') ?>

    <?php // echo $form->field($model, 'output') ?>

    <?php // echo $form->field($model, 'penanggung_jawab') ?>

    <?php // echo $form->field($model, 'pelaksana') ?>

    <?php // echo $form->field($model, 'aras') ?>

    <?php // echo $form->field($model, 'persetujuan') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
