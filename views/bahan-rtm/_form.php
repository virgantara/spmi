<?php

use app\helpers\MyHelper;
use app\models\Aspek;
use app\models\LingkupBahasan;
use app\models\UnitKerja;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BahanRtm */
/* @var $form yii\widgets\ActiveForm */

$identity =  Yii::$app->user->identity;
$unitKerja = UnitKerja::findOne($identity->unit_kerja_id);
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <?= $form->field($model, 'unit_id')->hiddenInput(['value' => $identity->unit_kerja_id])->label(false) ?>
    <?= $form->field($model, 'rtm_id')->hiddenInput(['value' => $rtm->id])->label(false) ?>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Unit</label>
                <input type="text" class="form-control" value="<?= $unitKerja->nama ?? null ?>" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Koordinator</label>
                <input type="text" class="form-control" value="<?= $rtm->unitKerja->nama ?? null ?>" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Aspek</label>
                <?= $form->field($model, 'aspek_id', ['options' => ['tag' => false]])
                    ->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Aspek::findAll(['status_aktif' => 1]), 'id', 'aspek'),
                        'options' => ['placeholder' => 'Pilih aspek...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])
                    ->label(false);
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Lingkup bahasan</label>
                <?= $form->field($model, 'lingkup_bahasan_id', ['options' => ['tag' => false]])
                    ->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(LingkupBahasan::findAll(['status_aktif' => 1]), 'id', 'lingkup_pembahasan'),
                        'options' => ['placeholder' => 'Pilih lingkup pembahasan...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])
                    ->label(false);
                ?>
            </div>
        </div>
        <div class="col-md-6 temuanFieldContainer" style="display: none;">
            <div class="form-group">
                <label class="control-label">Temuan</label>
                <?= $form->field($model, 'temuan_id', ['options' => ['tag' => false]])
                    ->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($dataTemuan, 'id', 'uraian_k'),
                        'options' => [
                            'placeholder' => 'Pilih uraian ketidaksesuaian...',
                            'id' => 'temuan_id',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])
                    ->label(false);
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Deskripsi Kondisi</label>
                <?= $form->field($model, 'deskripsi', [
                    'options' => ['tag' => false],
                ])->textarea([
                    'class' => 'form-control',
                    'maxlength' => true,
                    'id' => 'deskripsi_kondisi',
                ])->label(false) ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Rencana Tindak Lanjut</label>
                <?= $form->field($model, 'tindakan', [
                    'options' => ['tag' => false]
                ])->textarea([
                    'class' => 'form-control',
                    'maxlength' => true,
                    'id' => 'rencana_tindak_lanjut',
                ])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Penanggung jawab</label>
                <?= $form->field($model, 'penanggung_jawab', ['options' => ['tag' => false]])->textInput([
                    'class' => 'form-control',
                    'maxlength' => true,
                    'id' => 'penanggung_jawab'
                ])->label(false) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Pelaksana</label>
                <?= $form->field($model, 'pelaksana', ['options' => ['tag' => false]])->textInput([
                    'class' => 'form-control',
                    'maxlength' => true,
                    'id' => 'pelaksana'
                ])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Target Waktu Selesai</label>
                <?= $form->field($model, 'target_waktu', ['options' => ['tag' => false]])
                    ->widget(\kartik\date\DatePicker::class, [
                        'options' => ['placeholder' => 'Pilih tanggal', 'readonly' => true], // Add 'readonly' option
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
                        ],
                    ])
                    ->label(false);
                ?>
            </div>
        </div>
        <div class="col-md-3 temuanFieldContainer" style="display: none;">
            <div class="form-group">
                <label class="control-label">Jadwal Perbaikan AMI</label>
                <?= Html::textInput('jadwal_perbaikan', '', [
                    'class' => 'form-control',
                    'maxlength' => true,
                    'disabled' => 'disabled',
                    'id' => 'jadwal_perbaikan',
                ]) ?>

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Aras</label>
                <?= $form->field($model, 'aras', ['options' => ['tag' => false]])
                    ->radioList(MyHelper::getListAras(), [
                        'itemOptions' => ['class' => 'your-radio-class'], // Add your class here if needed
                    ])
                    ->label(false) ?>

            </div>
        </div>
    </div>


    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>

<?php JSRegister::begin() ?>
<script>
    $(document).bind("keyup.autocomplete", function() {

        $('#pelaksana, #penanggung_jawab').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,
                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });

        // $('#aras').autocomplete({
        //     minLength: 1,
        //     select: function(event, ui) {
        //         $(this).next().val(ui.item.id);
        //     },
        //     focus: function(event, ui) {
        //         $(this).next().val(ui.item.id);
        //     },
        //     source: function(request, response) {
        //         $.ajax({
        //             url: "/bahan-rtm/ajax-get-aras",
        //             dataType: "json",
        //             data: {
        //                 term: request.term,
        //             },
        //             success: function(data) {
        //                 response(data);
        //             }
        //         })
        //     },

        // });
    });

    $(document).ready(function() {

        checkLingkupBahasan();

        $('#<?= Html::getInputId($model, 'lingkup_bahasan_id') ?>').on('change', function() {
            checkLingkupBahasan();
        });

        $('#temuan_id').on('change', function() {
            var temuanId = $(this).val()

            $.ajax({
                url: "/temuan/ajax-get-by-id",
                type: "POST",
                dataType: "json",
                data: {
                    dataPost: temuanId,
                },
                success: function(data) {
                    $('#deskripsi_kondisi').val(data.uraian_k);
                    $('#rencana_tindak_lanjut').val(data.rencana_tindak_lanjut);
                    $('#penanggung_jawab').val(data.penanggung_jawab);
                    $('#jadwal_perbaikan').val(data.jadwal_perbaikan);
                }
            })
        })

        function checkLingkupBahasan() {
            var lingkupBahasanId = $('#<?= Html::getInputId($model, 'lingkup_bahasan_id') ?>').val();

            if (lingkupBahasanId == 1) {
                $('.temuanFieldContainer').show();
            } else {
                $('.temuanFieldContainer').hide()
            }
        }

    });
</script>
<?php JSRegister::end() ?>