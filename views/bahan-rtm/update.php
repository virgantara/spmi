<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BahanRtm */

$this->title = Yii::t('app', 'Update Bahan Rtm: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bahan Rtms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?= $this->render('_form', [
                    'model' => $model,
                    'rtm' => $rtm,
                    'dataTemuan' => $dataTemuan,
                ]) ?>
            </div>
        </div>
    </div>
</div>