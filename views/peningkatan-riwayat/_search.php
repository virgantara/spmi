<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PeningkatanRiwayatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peningkatan-riwayat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'peningkatan_id') ?>

    <?= $form->field($model, 'nama_dokumen') ?>

    <?= $form->field($model, 'indikator_id') ?>

    <?= $form->field($model, 'unit_kerja_id') ?>

    <?php // echo $form->field($model, 'dokumen_id') ?>

    <?php // echo $form->field($model, 'pernyataan_peningkatan') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
