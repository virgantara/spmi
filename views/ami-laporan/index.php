<?php

use app\helpers\MyHelper;
use app\models\Asesmen;
use app\models\Indikator;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Kriteria;
use app\models\Temuan;
use kartik\editable\Editable;
use kartik\form\ActiveForm;
use kartik\grid\EditableColumn;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use Symfony\Component\Console\Helper\Dumper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['asesmen/ami']];
$this->params['breadcrumbs'][] = $this->title;

$listTemuan = ArrayHelper::map(Temuan::find()->where(['ami_unit_id' => $amiUnit->id])->all(), 'id', 'uraian_k');
$listStatusEfektif = MyHelper::getStatusEfektif();
$listStatusRtl = MyHelper::getStatusRtl();
?>

<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $amiUnit->id]) ?>

    </div>
    <div class="panel-body">
        <div class="x_content">
            <h3><?= $amiUnit->unit->nama ?></h3>
            <h4><?= $amiUnit->ami->nama ?></h4>

            <?= Html::a('<i class="fa fa-upload"></i> ' . Yii::t('app', 'Unggah Bukti Kehadiran'), 'javascript:void(0)', ['class' => 'btn btn-success btn-sm', 'data-toggle' => 'modal', 'data-target' => '#uploadModal']) ?>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i> ' . Yii::t('app', 'Cetak Laporan'), ['asesmen/cetak-laporan-ami-per-auditee', 'ami_unit_id' => $amiUnit->id], ['class' => 'btn btn-danger btn-sm', 'target' => '_blank']) ?>

            <br>
            <br>
            <?php
            $gridColumns = [
                // 'ami_unit_id',
                [
                    'class' => EditableColumn::class,
                    'hAlign' => 'center',
                    'refreshGrid' => true,
                    'width' => '5%',
                    'attribute' => 'urutan',
                    'editableOptions' => [
                        'header' => 'Urutan',
                        'inputType' => Editable::INPUT_TEXT,
                        // Tambahkan opsi tambahan yang Anda perlukan
                    ],
                ],
                [
                    'class' => EditableColumn::class,
                    'attribute' => 'keterangan',
                    'editableOptions' => [
                        'header' => 'Keterangan',
                        'inputType' => Editable::INPUT_TEXTAREA,
                        // Tambahkan opsi tambahan yang Anda perlukan
                    ],
                ],
                [
                    'format' => 'raw',
                    'width' => '10%',
                    'hAlign' => 'center',
                    'label' => 'Unduh Bukti',
                    'value' => function ($data) {
                        if (isset($data->path_gambar)) {
                            return Html::a('<i class="fa fa-download"> </i> Unduh', ['download', 'id' => $data->id], ['target' => '_blank', 'data-pjax' => 0, 'class' => 'btn btn-xs btn-primary']);
                        } else {
                            return null;
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                            ]);
                        },
                    ],
                ],

            ]; ?>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => 'Bukti Kehadiran', 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>

        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Unggah Bukti Kehadiran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/ami-laporan/upload" method="post" id="form-upload" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <label for="">Urutan</label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="hidden" name="ami_unit_id" value="<?= $amiUnit->id ?>" required>
                                <input type="number" name="urutan" class="form-control" placeholder="1" required>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="">keterangan</label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <textarea name="keterangan" class="form-control" placeholder="Keterangan" rows="3" required></textarea>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="">File</label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="file" name="file" id="file" class="" required>
                            </div>

                        </div>
                        <small>Tipe File: jpg/png, max file size: 2MB </small>
                    </div>

                    <br><br>
                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'Pastikan kebenaran data!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>


<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-temuan").serialize()

        $.ajax({
            url: "/audit-tindak-lanjut/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-atl").modal("hide")
                        $(":input", "#modal-atl").val("")
                        window.location.reload()
                    });


                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", ".btn-add-atl", function(e) {
        e.preventDefault();

        $("#modal-atl").modal("show")
    });

    $(document).on("click", ".btn-update-temuan", function(e) {
        e.preventDefault();

        var temuan_id = $(this).data("temuanid")

        $.ajax({
            url: "/audit-tindak-lanjut/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: temuan_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                $("#modal-atl").modal("show")

                $("#id").val(hasil.id)
                $("#kriteria").val(hasil.kriteria)
                $("#temuan").val(hasil.temuan)
                $("#lokasi").val(hasil.lokasi)
                $("#ruang_lingkup").val(hasil.ruang_lingkup)
                $("#uraian_k").val(hasil.uraian_k)
                $("#referensi").val(hasil.referensi)
                $("#akar_penyebab").val(hasil.akar_penyebab)
                $("#rekomendasi").val(hasil.rekomendasi)
                $("#jadwal_perbaikan").val(hasil.jadwal_perbaikan)
                $("#jabatan_pj").val(hasil.jabatan_pj)
                $("#penanggung_jawab").val(hasil.penanggung_jawab)

                $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })

    });
</script>
<?php JSRegister::end() ?>