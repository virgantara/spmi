<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Persetujuan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Ami unit</label>
            <?= $form->field($model, 'ami_unit_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Auditor</label>
            <?= $form->field($model, 'auditor_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Dokumen</label>
            <?= $form->field($model, 'dokumen_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Datetime</label>
            <?= $form->field($model, 'datetime',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Is auditee</label>
            <?= $form->field($model, 'is_auditee',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
