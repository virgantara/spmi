<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterJenis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Urutan</label>
        <?= $form->field($model, 'urutan', ['options' => ['tag' => false]])->textInput()->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Value</label>
        <?= $form->field($model, 'value', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Set to</label>
        <?= $form->field($model, 'set_to', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status khusus</label>
        <?= $form->field($model, 'is_khusus', ['options' => ['tag' => false]])->radioList(\app\helpers\MyHelper::getStatusKhusus())->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status aktif</label>
        <?= $form->field($model, 'status_aktif', ['options' => ['tag' => false]])->radioList(\app\helpers\MyHelper::getStatusAktif())->label(false) ?>
    </div>
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>