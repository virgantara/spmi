<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnitAuditor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Id</label>
            <?= $form->field($model, 'id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Ami unit</label>
            <?= $form->field($model, 'ami_unit_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Asesor</label>
            <?= $form->field($model, 'asesor_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Asesor ke</label>
            <?= $form->field($model, 'asesor_ke',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
