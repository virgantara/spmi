<?php

use app\helpers\MyHelper;
use app\models\Asesmen;
use app\models\Indikator;

date_default_timezone_set("Asia/Jakarta");
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
?>

<table width="100%" style="margin-left: 4px;" border="0">
    <tr>
        <td width="10%"></td>
        <td width="80%" colspan="3" style="text-align: center;">
            <h1><b>LAPORAN</b></h1>
            <h1><b>MONITORING DAN EVALUASI KINERJA</b></h1>
            <h1><b>UPT PPTIK</b></h1>
        </td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td colspan="5">
            <br><br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br><br>
        </td>
    </tr>
    <tr>
        <td></td>
        <td width="30%"><p>Nomor Dokumen</p></td>
        <td width="2%" style="text-align: center;">:</td>
        <td width="48%"><?= $nomor_dokumen ?></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td width="30%"><p>Satuan Kerja</p></td>
        <td width="2%" style="text-align: center;">:</td>
        <td width="48%"><?= $satuan_kerja ?></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td width="30%"><p>Nama Kepala Satuan Kerja</p></td>
        <td width="2%" style="text-align: center;">:</td>
        <td width="48%"><?= $kepala_satker ?></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td width="30%"><p>Koordinator</p></td>
        <td width="2%" style="text-align: center;">:</td>
        <td width="48%"><?= $koordinator ?></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td width="30%"><p>Nama Wakil Rekor</p></td>
        <td width="2%" style="text-align: center;">:</td>
        <td width="48%"><?= $nama_warek ?></td>
        <td></td>
    </tr>
    <tr>
        <td><br><br><br><br></td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: center;">
            <h2><b>UNIVERSITAS DARUSSALAM GONTOR</b></h2>
            <h2><b><?= $tahun ?></b></h2>
        </td>
    </tr>
</table>