<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kata Pengantar</title>
    <style>
        body {
            font-family: 'pala';
            line-height: 1.6;
            margin: 20px;
        }
        .header, .footer {
            text-align: center;
            margin-bottom: 40px;
        }
        .content {
            text-align: justify;
        }
        .red-text {
            color: red;
        }
        .signature {
            text-align: right;
            margin-top: 40px;
        }
    </style>
</head>
<body>

<div class="header">
    <h2>KATA PENGANTAR</h2>
</div>

<div class="content">
    <p>Assalamu’alaikum warahmatullahi wabarakatuh</p>

    <p>
        Puji syukur senantiasa kami panjatkan ke hadirat Allah SWT atas limpahan rahmat, taufiq, dan hidayah-Nya, sehingga kami dapat menjalankan peran dan tanggung jawab kami sebagaimana amanah yang kami terima sehingga mampu menyelesaikan Laporan Monitoring dan Evaluasi Kinerja Satuan Kerja tepat waktu.
    </p>

    <p>
        Laporan Monitoring dan Evaluasi Kinerja Satuan Kerja menjadi sarana pertanggungjawaban kami dalam menunaikan amanah sejak proses perencanaan, pelaksanaan dan pencapaian sasaran program/kegiatan pada Semester I tahun 2023.
    </p>

    <p>
        Besar harapan kami agar laporan kinerja ini dapat memberikan manfaat bagi seluruh pihak terkait dan digunakan sebagai bahan pertimbangan dalam peningkatan mutu kinerja pada periode berikutnya di lingkungan UNIVERSITAS DARUSSALAM GONTOR. Menyadari bahwa kinerja kami belum sepenuhnya sempurna maka kami menerima segala saran masukan untuk meningkatkan mutu kinerja kami.
    </p>

    <p>
        Ucapan terimakasih yang sebesar-besarnya kami haturkan kepada keluarga besar UNIVERSITAS DARUSSALAM GONTOR dan semua pihak yang telah turut serta membantu penyusunan laporan ini
    </p>

    <p>Wassalamu’alaikum warahmatullahi wabarakatuh.</p>
</div>

<div class="signature">
    <p>Ponorogo, <span class="red-text">28 Februari 2022</span></p>
    <p>Kepala Satuan Kerja</p>
    <p class="red-text">Nama Kepala Satuan Kerja</p>
</div>

</body>
</html>
