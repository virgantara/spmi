<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MonevUnitKerja */

$this->title = $model->monev->nama_monev;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoring dan Evaluasi'), 'url' => ['monev/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
            </div>

            <div class="panel-body ">
                <?= $this->render('../monev-unit-kerja/_view', ['model' => $model]) ?>
            </div>

            <div class="panel-heading">
                <?= $this->render('../nav-master/nav_monev', ['id' => $model->id]) ?>
            </div>

        </div>
    </div>
</div>