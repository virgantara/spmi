<?php

use yii\widgets\DetailView;
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        // 'id',
        [
            'label' => 'Monev',
            'value' => function ($model) {
                return $model->monev->nama_monev;
            }
        ],
        [
            'label' => 'Tahun',
            'value' => function ($model) {
                return $model->monev->tahun;
            }
        ],
        [
            'label' => 'Periode',
            'value' => function ($model) {
                return $model->monev->periode;
            }
        ],
        [
            'label' => 'Satuan Kerja',
            'value' => function ($model) {
                return $model->unitKerja->nama;
            }
        ],
    ],
]) ?>