<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rtm */

$this->title = Yii::t('app', 'Rapat Tinjauan Manajemen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rtm'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    input[type="checkbox"] {
        transform: scale(1.5);
        margin-right: 5px;
    }

    table {
        width: 40%;
        border-collapse: collapse;
    }

    tr {
        margin-bottom: 10px;
    }

    th,
    td {
        padding: 8px;
        text-align: left;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?> | Siklus: <?= $ami->tahun ?></h3>
            </div>
            <div class="x_content">

                <table>
                    <tr>
                        <th colspan="3">
                            Tingkat UPPS
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <?php foreach ($fakultas as $key => $value) : ?>
                        <tr>
                            <td><?= $value->nama ?></td>
                            <td>
                                <input <?= (isset($dataRtm[$value->id]) ? 'checked' : '') ?> value="<?= $value->id ?>" type="checkbox" class="add-rtm" name="rtm-<?= $value->id ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="3">
                            <hr>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            Tingkat Wakil Rektor
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <?php foreach ($warek as $key => $value) : ?>
                        <tr>
                            <td><?= $value->nama ?></td>
                            <td>
                                <input <?= (isset($dataRtm[$value->id]) ? 'checked' : '') ?> value="<?= $value->id ?>" type="checkbox" class="add-rtm" name="rtm-<?= $value->id ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="3">
                            <hr>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            Tingkat Non Wakil Rektor
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <?php foreach ($satker as $key => $value) : ?>
                        <tr>
                            <td><?= $value->nama ?></td>
                            <td>
                                <input <?= (isset($dataRtm[$value->id]) ? 'checked' : '') ?> value="<?= $value->id ?>" type="checkbox" class="add-rtm" name="rtm-<?= $value->id ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="3">
                            <hr>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3">
                            Tingkat Universitas
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <?php foreach ($universitas as $key => $value) : ?>
                        <tr>
                            <td><?= $value->nama ?></td>
                            <td>
                                <input <?= (isset($dataRtm[$value->id]) ? 'checked' : '') ?> value="<?= $value->id ?>" type="checkbox" class="add-rtm" name="rtm-<?= $value->id ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        $('.add-rtm').on('change', function() {

            var obj = new Object;
            obj.unit_id = $(this).val();
            obj.isChecked = $(this).is(':checked');
            console.log(obj);

            $.ajax({
                url: '/rtm/ajax-add',
                method: 'POST',
                data: {
                    dataPost: obj
                },
                success: function(response) {
                    Swal.close()
                    var hasil = $.parseJSON(response)
                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        }).then(res => {
                            $("#modal").modal("hide")
                        });

                        // $.pjax.reload({
                        //     container: "#pjax-container"
                        // });
                        // $("#nama_auditor").val("").focus()
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        })
                    }
                },
                error: function(xhr, status, error) {
                    console.error(status + ': ' + error);
                }
            });
        });
    });
</script>
<?php JSRegister::end() ?>