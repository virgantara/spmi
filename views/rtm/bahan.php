<?php

use app\helpers\MyHelper;
use app\models\Periode;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RtmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rtm';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3>Rapat Tinjauan Manajemen</h3>
            </div>
            <div class="x_content">

                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'label' => 'Siklus',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->ami->tahun ?? '';
                        },
                    ],
                    [
                        'label' => 'Koordinator',
                        'vAlign' => 'middle',
                        'value' => function ($model) {
                            return $model->unitKerja->nama ?? '';
                        },
                    ],
                    [
                        'attribute' => 'nama_kepala',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'ketua_panitia',
                        'vAlign' => 'middle',
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'vAlign' => 'middle',
                        'attribute' => 'tema',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                        'value' => function ($data) {
                            return ($data->tema == null ? '' : $data->tema);
                        }
                    ],
                    [
                        'label' => 'Detil',
                        'vAlign' => 'middle',
                        'contentOptions' => [
                            'width' => '6%',
                        ],
                        'format' => 'raw',
                        'hAlign' => 'center',
                        'value' => function ($data) {
                            return Html::a('<i class="fa fa-search"></i>', ['view', 'id' => $data->id], ['class' => 'btn btn-info btn-sm']);
                        },
                    ],
                    // [
                    //     'format' => 'raw',
                    //     'contentOptions' => [
                    //         'width' => '5%',
                    //     ],
                    //     'hAlign' => 'center',
                    //     'value' => function ($data) {
                    //         $tingkat = 'dekanat';
                    //         $pencil = Html::a('', '', ['class' => 'glyphicon glyphicon-pencil ' . $tingkat, 'data-rtm' => $data->id]);
                    //         $trash = Html::a('', ['delete', 'id' => $data->id, 'posisi' => 'index'], [
                    //             'class' => 'glyphicon glyphicon-trash',
                    //             'data' => [
                    //                 'confirm' => 'Are you sure you want to delete this item?',
                    //                 'method' => 'post',
                    //             ],
                    //         ]);
                    //         return $pencil . ' ' . $trash;
                    //     },
                    // ],
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>


<!-- MODAL PILIH -->

<?php Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h4 class="modal-title">Pilih Tingkatan RTM</h4>',
    'id' => 'modal',
    // 'size' => 'modal-md',
    'size' => Modal::SIZE_DEFAULT, // or Modal::SIZE_LARGE or Modal::SIZE_SMALL
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs">

    <div class="form-group">

        <?= Html::button('<i class="fa fa-file"></i> Dekanat', ['class' => 'btn btn-primary', 'id' => 'btn-dekanat']) ?>
        <?= Html::button('<i class="fa fa-file"></i> Wakil Rektor', ['class' => 'btn btn-primary', 'id' => 'btn-warek']) ?>
        <?= Html::button('<i class="fa fa-file"></i> Universitas', ['class' => 'btn btn-primary', 'id' => 'btn-universitas']) ?>
    </div>
</form>
<?php Modal::end(); ?>





<!-- MODAL DEKANAT -->

<?php Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h4 class="modal-title">FORM RTM DEKANAT</h4>',
    'id' => 'modal-dekanat',
    // 'size' => 'modal-md',
    'size' => Modal::SIZE_DEFAULT, // or Modal::SIZE_LARGE or Modal::SIZE_SMALL
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-dekanat">

    <div class="form-group">
        <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id_dekanat"]) ?>
        <?= Html::hiddenInput('jenis_rtm', '1', ['class' => 'form-control', "id" => "jenis_rtm_dekanat"]) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua RTM</label>
        <?= Html::textInput('nama_kepala', '', ['class' => 'form-control', "id" => "nama_kepala_dekanat", 'placeholder' => "Masukkan nama ketua RTM", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua Panitia <small>(Jika ada)</small></label>
        <?= Html::textInput('ketua_panitia', '', ['class' => 'form-control', "id" => "ketua_panitia_dekanat", 'placeholder' => "Masukkan nama ketua panitia", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Tahun</label>
        <?= Select2::widget([
            'id' => 'tahun_dekanat',
            'name' => 'tahun',
            'value' => '',
            'data' => MyHelper::getYears(),
            'options' => [
                'required' => true,
                'placeholder' => 'Masukkan tahun RTM ...'
            ]
        ]); ?>

    </div>
    <div class="form-group">
        <label for="">Periode</label>
        <?= Select2::widget([
            'id' => 'periode_id_dekanat',
            'name' => 'periode_id',
            'value' => '',
            'data' => ArrayHelper::map(Periode::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'periode'),
            'options' => [
                'required' => true,
                'placeholder' => 'Masukkan tahun RTM ...'
            ]
        ]); ?>

    </div>
    <div class="form-group">
        <label for="">Tema</label>
        <?= Html::textInput('tema', '', ['class' => 'form-control', "id" => "tema_dekanat", 'placeholder' => "Masukkan tema RTM", 'required' => 'required']) ?>
    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-file"></i> Simpan', ['class' => 'btn btn-success', 'id' => 'btn-save-dekanat']) ?>
    </div>
</form>
<?php Modal::end(); ?>

<!-- MODAL WAREK -->

<?php Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h4 class="modal-title">FORM RTM WAKIL REKTOR</h4>',
    'id' => 'modal-warek',
    // 'size' => 'modal-md',
    'size' => Modal::SIZE_DEFAULT, // or Modal::SIZE_LARGE or Modal::SIZE_SMALL
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-warek">

    <div class="form-group">
        <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id"]) ?>
        <?= Html::hiddenInput('jenis_rtm', '2', ['class' => 'form-control', "id" => "jenis_rtm_warek"]) ?>
    </div>

    <div class="form-group">
        <label for="">Bidang</label>
        <?= Html::textInput('bidang', '', ['class' => 'form-control', "id" => "bidang_warek", 'placeholder' => "Masukkan bidang", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua RTM</label>
        <?= Html::textInput('nama_kepala', '', ['class' => 'form-control', "id" => "nama_kepala_warek", 'placeholder' => "Masukkan nama ketua RTM", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua Panitia <small>(Jika ada)</small></label>
        <?= Html::textInput('ketua_panitia', '', ['class' => 'form-control', "id" => "ketua_panitia_warek", 'placeholder' => "Masukkan nama ketua panitia", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Tahun</label>
        <?= Select2::widget([
            'name' => 'tahun',
            'id' => 'tahun_warek',
            'value' => '',
            'data' => MyHelper::getYears(),
            'options' => [
                'placeholder' => 'Masukkan tahun RTM ...',
                'required' => true
            ]
        ]); ?>

    </div>
    <div class="form-group">
        <label for="">Tema</label>
        <?= Html::textInput('tema', '', ['class' => 'form-control', "id" => "tema_warek", 'placeholder' => "Masukkan tema RTM", 'required' => 'required']) ?>
    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-file"></i> Simpan', ['class' => 'btn btn-success', 'id' => 'btn-save-warek']) ?>
    </div>
</form>
<?php Modal::end(); ?>

<!-- MODAL UNIVERSITAS -->

<?php Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '<h4 class="modal-title">FORM RTM UNIVERSITAS</h4>',
    'id' => 'modal-universitas',
    // 'size' => 'modal-md',
    'size' => Modal::SIZE_DEFAULT, // or Modal::SIZE_LARGE or Modal::SIZE_SMALL
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-universitas">

    <div class="form-group">
        <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id"]) ?>
        <?= Html::hiddenInput('jenis_rtm', '3', ['class' => 'form-control', "id" => "jenis_rtm_universitas"]) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua RTM</label>
        <?= Html::textInput('nama_kepala', '', ['class' => 'form-control', "id" => "nama_kepala_universitas", 'placeholder' => "Masukkan nama ketua RTM", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Ketua Panitia <small>(Jika ada)</small></label>
        <?= Html::textInput('ketua_panitia', '', ['class' => 'form-control', "id" => "ketua_panitia_universitas", 'placeholder' => "Masukkan nama ketua panitia", 'required' => 'required']) ?>
    </div>
    <div class="form-group">
        <label for="">Tahun</label>
        <?= Select2::widget([
            'name' => 'tahun',
            'id' => "tahun_universitas",
            'value' => '',
            'data' => MyHelper::getYears(),
            'options' => [
                'placeholder' => 'Masukkan tahun RTM ...',
                'required' => true
            ]
        ]); ?>
    </div>
    <div class="form-group">
        <label for="">Tema</label>
        <?= Html::textInput('tema', '', ['class' => 'form-control', "id" => "tema_universitas", 'placeholder' => "Masukkan tema RTM", 'required' => 'required']) ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-file"></i> Simpan', ['class' => 'btn btn-success', 'id' => 'btn-save-universitas']) ?>
    </div>
</form>
<?php Modal::end(); ?>


<?php JSRegister::begin() ?>
<script>
    $(document).on("click", ".close", function(e) {
        e.preventDefault();
        $(":input", "#modal").val("")
    });

    $(document).bind("keyup.autocomplete", function() {

        $('#nama_kepala_dekanat,#ketua_panitia_dekanat,#nama_kepala_warek,#ketua_panitia_warek,#nama_kepala_universitas,#ketua_panitia_universitas').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#niy").val(ui.item.items.NIY)
                $("#nidn").val(ui.item.items.NIDN)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    // click pilih
    $(document).on("click", "#btn-add-rtm", function(e) {
        e.preventDefault();
        $("#modal").modal("show")
    });

    // pilih dekanat
    $(document).on("click", "#btn-dekanat", function(e) {
        e.preventDefault();
        $("#modal").modal("hide")
        $("#modal-dekanat").modal("show")
    });

    // pilih warek
    $(document).on("click", "#btn-warek", function(e) {
        e.preventDefault();
        $("#modal").modal("hide")
        $("#modal-warek").modal("show")
    });

    // pilih universitas
    $(document).on("click", "#btn-universitas", function(e) {
        e.preventDefault();
        $("#modal").modal("hide")
        $("#modal-universitas").modal("show")
    });

    // click back
    $(document).on("click", ".btn-back", function(e) {
        e.preventDefault();
        $("#modal").modal("show")
        $("#modal-dekanat").modal("hide")
        $("#form-dekanat")[0].reset()
        $("#modal-warek").modal("hide")
        $("#form-warek")[0].reset()
        $("#modal-universitas").modal("hide")
        $("#form-universitas")[0].reset()
    });

    // Validasi Input
    function validateInput(formId) {

        var allAreFilled = true;

        $("#" + formId + " [required]").each(function() {
            if (!allAreFilled) return;

            var elementType = $(this).attr("type");

            if (elementType === "radio" || elementType === "checkbox") {
                var inputName = $(this).attr("name");
                var inputChecked = $("input[name='" + inputName + "']:checked").length > 0;

                allAreFilled = inputChecked;
            } else {
                var inputValue = $(this).val();

                if (!inputValue) {
                    allAreFilled = false;
                }
            }
        });

        if (!allAreFilled) {
            alert('Mohon untuk melengkapi data terlebih dahulu !!');
        } else {
            return true;
        }
    }

    // Insert AJAX
    function insertAjax(formId, modalId) {

        console.log('berhasil');

        var obj = $("#" + formId).serialize()

        console.log(obj);

        $.ajax({
            url: "/rtm/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#" + modalId).modal("hide")
                        $(":input", "#modal").val("")
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })

    }

    // On Click Save
    $(document).on("click", "#btn-save-dekanat", function(e) {
        e.preventDefault();

        validateInput('form-dekanat') ? insertAjax('form-dekanat', 'modal-dekanat') : ''
    });
    $(document).on("click", "#btn-save-warek", function(e) {
        e.preventDefault();

        validateInput('form-warek') ? insertAjax('form-warek', 'modal-warek') : ''
    });
    $(document).on("click", "#btn-save-universitas", function(e) {
        e.preventDefault();

        validateInput('form-universitas') ? insertAjax('form-universitas', 'modal-universitas') : ''
    });

    // On Click Edit
    $(document).on("click", ".dekanat", function(e) {
        e.preventDefault();

        var rtm_id = $(this).data("rtm")

        $.ajax({
            url: "/rtm/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: rtm_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                $("#modal-dekanat").modal("show")

                $("#id_dekanat").val(hasil.id)
                $("#nama_kepala_dekanat").val(hasil.nama_kepala)
                $("#tahun_dekanat").val(hasil.tahun).trigger("change")
                $("#ketua_panitia_dekanat").val(hasil.ketua_panitia)
                $("#tema_dekanat").val(hasil.tema)
                $("#periode_id_dekanat").val(hasil.periode_id).trigger("change")

                // $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })

    });
    $(document).on("click", ".warek", function(e) {
        e.preventDefault();

        var rtm_id = $(this).data("rtm")

        $.ajax({
            url: "/rtm/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: rtm_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                $("#modal-warek").modal("show")

                $("#id_warek").val(hasil.id)
                $("#bidang_warek").val(hasil.bidang)
                $("#nama_kepala_warek").val(hasil.nama_kepala)
                $("#tahun_warek").val(hasil.tahun).trigger("change")
                $("#ketua_panitia_warek").val(hasil.ketua_panitia)
                $("#tema_warek").val(hasil.tema)

                // $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })
    });
    $(document).on("click", ".universitas", function(e) {
        e.preventDefault();

        var rtm_id = $(this).data("rtm")

        $.ajax({
            url: "/rtm/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: rtm_id
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {

                var hasil = $.parseJSON(data)

                console.log(hasil);

                $("#modal-universitas").modal("show")

                $("#id_universitas").val(hasil.id)
                $("#nama_kepala_universitas").val(hasil.nama_kepala)
                $("#tahun_universitas").val(hasil.tahun).trigger("change")
                $("#ketua_panitia_universitas").val(hasil.ketua_panitia)
                $("#tema_universitas").val(hasil.tema)

                // $("input[name=kategori][value=" + hasil.kategori + "]").prop("checked", true)

            }
        })
    });

    // Reload page then close modal
    $("#modal-dekanat,#modal-warek,#modal-universitas").on("hidden.bs.modal", function() {
        location.reload();
    });
</script>
<?php JSRegister::end() ?>