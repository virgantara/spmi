<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rtm */

$this->title = 'Tindak Lanjut';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rtm'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->unitKerja->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">

                <?= $this->render('../nav-master/nav_rtm', ['id' => $model->id]) ?>
                <br>
                <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Bahan'), ['bahan-rtm/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<i class="fa fa-upload"></i> ' . Yii::t('app', 'Upload'), ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete Selected'), 'javascript:void(0)', ['class' => 'btn btn-danger']) ?>
                | Download Template File <?= Html::a(Yii::t('app', 'Disini'), ['bahan-rtm/template-download', 'id' => $model->id], ['target' => '_blank']) ?>
            </div>


            <div class="panel-body ">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="questionOne">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                                <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                            </a>
                        </h5>
                    </div>
                    <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                        <div class="panel-body">
                            <form action="<?= Yii::$app->urlManager->createUrl(['indikator/index']) ?>" method="get">
                                <div class="row">
                                    <div class="col-lg-3">

                                        <div class="form-group">
                                            <label for="">Lingkup Pembahasan</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-lingkup_pembahasan_id',
                                                'name' => 'lingkup_pembahasan_id[]',
                                                'data' => [1, 2],
                                                'value' => $_GET['lingkup_pembahasan_id'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Lingkup Pembahasan')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">

                                        <div class="form-group">
                                            <label for="">Aspek</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-aspek_id',
                                                'name' => 'aspek_id[]',
                                                'data' => [1, 2],
                                                'value' => $_GET['aspek_id'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Aspek')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">

                                        <div class="form-group">
                                            <label for="">Aras</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-aras',
                                                'name' => 'aras[]',
                                                'data' => [1, 2],
                                                'value' => $_GET['aras'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Aras')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">

                                        <div class="form-group">
                                            <label for="">Persetujuan</label>
                                            <?= Select2::widget([
                                                'id' => 'select2-persetujuan',
                                                'name' => 'persetujuan[]',
                                                'data' => [1, 2],
                                                'value' => $_GET['persetujuan'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Persetujuan')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= Html::submitButton('Apply Filter', ['class' => 'btn btn-primary']) ?>
                                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-reset', 'type' => 'reset']) ?>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    'unit_id',
                    'jenis_bahan',
                    // 'temuan_id',
                    'deskripsi',
                    'tindakan',
                    //'target_waktu',
                    //'output',
                    //'penanggung_jawab',
                    //'pelaksana',
                    //'aras',
                    //'rtm_id',
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataBahanProvider,
                    'filterModel' => $searchBahanModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container-bahan-rtm',
                        ]
                    ],
                    'id' => 'my-grid-bahan-rtm',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>

    </div>
</div>