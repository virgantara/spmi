<?php

use app\models\LingkupBahasan;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rtm */

$this->title = 'Rapat Tinjauan';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rtm'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->unitKerja->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="panel">
            <div class="panel-heading">

                <?= $this->render('../nav-master/nav_rtm', ['id' => $model->id]) ?>

            </div>

            <div class="panel-body">

                <div class="x_content">
                    <div class="col-md-5 col-lg-5 col-xs-12">
                        <input type="hidden" name="ami_unit_id" id="ami_unit_id" value="<?= $model->id ?>">

                        <div class="form-group">
                            <label class="control-label ">Lingkup Pembahasan</label>
                            <?= Html::dropDownList(
                                'lingkup_bahasan_id',
                                null,
                                ArrayHelper::map(LingkupBahasan::findAll(['status_aktif' => 1]), 'id', 'lingkup_pembahasan'),
                                [
                                    'id' => 'lingkup_bahasan_id',
                                    'class' => "form-control",
                                    'prompt' => 'Pilih Lingkup Pembahasan'
                                ]

                            ) ?>
                        </div>

                        <div class="form-group clearfix">
                            <button class="btn btn-primary" id="btn-filter-standar"><i class="fa fa-search"></i>
                                Filter</button>
                        </div>
                    </div>

                    <div class="panel-body col-md-12">

                        <div class="container">
                            <div class="content">
                                <div class="panel-group" id="dynamicPanels">

                                    <div class="panel panel-default data-thead">
                                        <div class="panel-heading">
                                            <h3 class="panel-title" style="text-align: center;"><strong>PENETAPAN</strong></h3>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>