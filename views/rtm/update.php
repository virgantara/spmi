<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rtm */

$this->title = Yii::t('app', 'Update Rtm: ' . $model->unitKerja->nama, [
    'nameAttribute' => '' . $model->unitKerja->nama,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rtm'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->unitKerja->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?= $this->render('_form', [
                    'model' => $model,
                    'ami' => $ami,
                ]) ?>
            </div>
        </div>
    </div>
</div>