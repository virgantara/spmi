<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rtm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Siklus</label>
        <input class="form-control" type="text" value="<?= $model->ami->tahun ?>" readonly="true">
    </div>

    <div class="form-group">
        <label class="control-label">Unit kerja</label>
        <input class="form-control" type="text" value="<?= $model->unitKerja->nama ?>" readonly="true">
    </div>

    <div class="form-group">
        <label class="control-label">Nama kepala</label>
        <?= $form->field($model, 'nama_kepala', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'id' => 'nama_kepala', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Ketua panitia</label>
        <?= $form->field($model, 'ketua_panitia', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'id' => 'ketua_panitia', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Tema</label>
        <?= $form->field($model, 'tema', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>

<?php JSRegister::begin() ?>
<script>
    $(document).bind("keyup.autocomplete", function() {

        $('#nama_kepala').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });

        $('#ketua_panitia').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });

    });
</script>
<?php JSRegister::end() ?>