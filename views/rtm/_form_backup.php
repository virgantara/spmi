<?php

use app\helpers\MyHelper;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rtm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <form action="" id="form-rtm">

        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'form_validation',
            ]
        ]); ?>
        <div class="form-group">
            <?= Html::hiddenInput('Rtm[ami_id]', $ami->id) ?>
            <label class="control-label">Siklus</label>
            <?= Html::textInput('Rtm[ami_nama]', $ami->nama, ['class' => 'form-control', 'readonly' => true]) ?>
        </div>


        <div class="form-group">
            <label class="control-label">Tingkatan</label>
            <?= Select2::widget([
                'name' => 'Rtm[tingkatan]', // Ganti dengan nama atribut model yang sesuai jika perlu
                'data' => MyHelper::getTingkatRtm()[1],
                'id' => 'tingkatan_id',
                'options' => ['placeholder' => 'Pilih Tingkatan', 'class' => 'form-control'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'hideSearch' => true,
            ]); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Koordinator</label>
            <?= DepDrop::widget([
                'name' => 'Rtm[unit_kerja_id]', // Ganti dengan nama atribut model yang sesuai jika perlu
                'options' => ['id' => 'koodinator_id'],
                'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions' => [
                    'depends' => ['tingkatan_id'],
                    'initialize' => true,
                    'placeholder' => 'Pilih Koordinator',
                    'url' => Url::to(['/unit-kerja/subkoordinator']),
                ],
            ]); ?>
        </div>


        <div class="form-group">
            <label class="control-label">Nama kepala</label>
            <?= Html::textInput('Rtm[nama_kepala]', $model->nama_kepala, ['class' => 'form-control', 'id' => 'nama_kepala', 'readonly' => true]) ?>
        </div>

        <div class="form-group">
            <label class="control-label">Ketua panitia</label>
            <?= Html::textInput('Rtm[ketua_panitia]', $model->ketua_panitia, ['class' => 'form-control', 'id' => 'ketua_panitia']) ?>
        </div>

        <div class="form-group">
            <label class="control-label">Tema</label>
            <?= Html::textInput('Rtm[tema]', $model->tema, ['class' => 'form-control']) ?>
        </div>


        <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>

        <?php ActiveForm::end(); ?>
    </form>

</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        $("#koodinator_id").change(function() {

            var koordinatorId = $(this).val();

            if (koordinatorId !== null) {
                $.ajax({
                    type: "POST",
                    url: "/unit-kerja/ajax-get-pj",
                    data: {
                        dataPost: koordinatorId
                    },
                    success: function(data) {
                        var hasil = $.parseJSON(data)

                        if (hasil.data) {
                            $("#nama_kepala").val(hasil.data.nama_kepala);
                        } else {
                            // Ganti dengan nilai default atau sesuai kebutuhan Anda
                            $("#nama_kepala").val("");
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error("Error: " + error);
                    }
                });
            }


        });
    });

    $(document).bind("keyup.autocomplete", function() {

        $('#ketua_panitia').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });


    $(document).on("click", "#saveButton", function(e) {
        e.preventDefault();

        var obj = $("#form-rtm").serialize()

        $.ajax({
            url: "/rtm/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        window.location('/rtm')
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });
</script>
<?php JSRegister::end() ?>