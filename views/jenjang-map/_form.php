<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenjangMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Unit kerja</label>
            <?= $form->field($model, 'unit_kerja_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Jenjang</label>
            <?= $form->field($model, 'jenjang_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
