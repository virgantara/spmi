<?php

use app\models\Jenjang;
use app\models\JenjangMap;
use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JenjangMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenjang Maps';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $listJenjang = Jenjang::find()->orderBy('urutan ASC')->all();
                            foreach ($listJenjang as $jenjang) {
                            ?>
                                <th><?= $jenjang->nama; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $listUnit = UnitKerja::find()->orderBy('nama ASC')->where(['not', ['jenis' => 'satker']])->all();
                        foreach ($listUnit as $unit) {
                        ?>
                            <tr>
                                <th><?= $unit->nama; ?></th>

                                <?php
                                foreach ($listJenjang as $jenjang) {
                                    $m = JenjangMap::find()->where(['unit_kerja_id' => $unit->id, 'jenjang_id' => $jenjang->id])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                                    <td><input name="<?= $unit->id ?>" type="radio" <?= $checked; ?> class="mycheck" data-unit="<?= $unit->id; ?>" data-jenjang="<?= $jenjang->id; ?>" value="1">
                                    </td>
                                <?php
                                }
                                ?>

                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php


$script = ' 
$(".mycheck").change(function(){
    var unit            = $(this).data("unit");
    var jenjang         = $(this).data("jenjang");
    var obj             = new Object;
    obj.unit_kerja_id   = unit;
    obj.jenjang_id      = jenjang;
    obj.checked         = $(this).prop("checked") ? "1" : "0";

    $.ajax({
        type : "POST",
        url : "' . Url::to(['jenjang-map/ajax-jenjang-map']) . '",
        data : {
            dataPost: obj
        },
        success : function(data){
            console.log(data)
        }
    });
});
';


$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

?>