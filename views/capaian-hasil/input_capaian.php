<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CapaianHasilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Input Capaian Hasil Periode ' . $periode->periode;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><strong><?= Html::encode($this->title) ?></strong></h3>
            </div>
            <div class="x_content">
                <?= Html::a('<i class="fa fa-reply"></i> Back', ['input'], ['class' => 'btn btn-sm btn-info']) ?>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped" id="tabel-capaian">

                            <thead>
                                <tr>
                                    <th>Sasaran strategis</th>
                                    <th class="text-center">No Indikator</th>
                                    <th>Indikator Kinerja</th>
                                    <th class="text-center">Satuan</th>
                                    <th>Target UNIDA</th>
                                    <th>Bidang WR</th>
                                    <th colspan="2">Target Capaian</th>
                                    <th colspan="2">Realisasi Target</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>


                </div>

                <button id="btn_update_nilai_dosen" class="btn btn-success"><i class="fa fa-pencil"></i> Update</button>

            </div>
        </div>
    </div>

</div>
<?php JSRegister::begin() ?>
<script>
    getKompetensi(<?= $_GET['periode_id'] ?>)

    var list_kompetensi = []

    function getKompetensi(periode_id) {

        var obj = new Object;
        obj.periode_id = periode_id;
        var ajax_url = "/capaian-hasil/ajax-get-capaian-kinerja";
        $(this).ajaxDataPost(ajax_url, obj, function(err, hasils) {

            // console.log(hasils);
            var row = '';
            $('#tabel-capaian > tbody').empty();

            var counter = 0;
            var list_kategori = []
            var list_values = []

            $.each(hasils, function(i, objects) {

                var counter_label = "";
                if (!list_kategori.includes(objects.induk)) {
                    counter++;
                    counter_label = counter
                }

                // console.log(objects.sasaran_strategis);

                row += '<tr>';
                row += '<td>' + objects.sasaran_strategis + '</td>';
                row += '<td>' + objects.no_indikator + '</td>';
                row += '<td>' + objects.indikator_kinerja + '</td>';
                row += '<td>' + objects.satuan + '</td>';

                row += '<td>' + objects.target_unida + '</td>';
                row += '<td>' + objects.bidang_wr + '</td>';
                row += '<td><input type="number" data-unit_kerja_id=' + objects.unit_kerja_id + ' data-periode_id=' + objects.periode_id + ' data-capaian_id=' + i + '  class="capaian_target" value=' + objects.capaian_target + '></td>';
                row += '<td>' + objects.capaian_target + '</td>';
                row += '<td><input type="number" data-realisasi_id=' + i + ' class="realisasi_target" value=' + objects.realisasi_target + '></td>';
                row += '<td>' + objects.realisasi_target + '</td>';
                row += '</tr>';

            });

            $('#tabel-capaian > tbody').append(row);

        });

    }

    $(document).on("click", "#btn_update_nilai_dosen", function(e) {
        // var nilai_realisasi = "";
        // var realisasi_id = "";

        var objArray = []
        var objData = []
        var objArrayRealisasi = []

        $(".realisasi_target").each(function(i, item) {
            var obj = new Object;

            obj.nilai_realisasi = item.value

            objArrayRealisasi.push(obj)

        });

        $(".capaian_target").each(function(i, item) {
            var obj = new Object;

            obj.unit_kerja_id = $(this).data("unit_kerja_id")
            obj.capaian_kinerja_id = $(this).data("capaian_id")
            obj.periode_id = $(this).data("periode_id")
            obj.capaian_target = item.value
            obj.realisasi_target = objArrayRealisasi[i].nilai_realisasi

            objData.push(obj)

        });
        
        var data = new Object;
        data.master = 0

        objArray.push(objData)
        objArray.push(data)

        console.log(objArray);

        $.ajax({
            type: "POST",
            data: {
                dataPost: objArray
            },
            async: true,
            url: "/capaian-hasil/ajax-realisasi-capaian",
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Processing your request...",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },

            success: function(data) {
                var res = $.parseJSON(data);

                console.log(res);
                if (res.code != 200) {
                    alert(res.message)
                } else {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: res.message
                    }).then(() => {
                        location.reload()
                    })

                }

            }
        });

    });
</script>
<?php JSRegister::end() ?>