<?php


/* @var $this yii\web\View */
/* @var $model app\models\CapaianHasil */

use app\helpers\MyHelper;
use richardfan\widget\JSRegister;
use yii\helpers\Html;

$this->title = $periode->periode . " - " . $periode->tahun;
$this->params['breadcrumbs'][] = ['label' => 'Capaian Hasils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$unitKerja = MyHelper::adminOnlyUnit(Yii::$app->user->identity->unit_kerja_id, 23);
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><strong><?= Html::encode($this->title) ?></strong></h3>
            </div>
            <div class="x_content">
                <?= Html::a('<i class="fa fa-reply"></i> Back', ['input'], ['class' => 'btn btn-sm btn-info']) ?>

                <br><br>
                <table>
                    <tr>
                        <td>Nama Satuan</td>
                        <td>:</td>
                        <td><?= $unitKerja ? $unitKerja->nama . " - " . $unitKerja->singkatan : "-" ?></td>
                    </tr>
                    <tr>
                        <td>Sudah tercapai</td>
                        <td>:</td>
                        <td>
                            <div class="sudah_tercapai"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Belum tercapai</td>
                        <td>:</td>
                        <td>
                            <div class="belum_tercapai"></div>
                        </td>
                    </tr>
                </table>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped" id="tabel-capaian">

                            <thead>
                                <tr>
                                    <th class="text-center">Sasaran strategis</th>
                                    <th class="text-center">No Indikator</th>
                                    <th class="text-center">Indikator Kinerja</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Target UNIDA</th>
                                    <th class="text-center">Bidang WR</th>
                                    <th class="text-center">Target Capaian</th>
                                    <th></th>
                                    <th class="text-center">Realisasi Target</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // echo '<pre>';print_r('tes');die;
                                $sudah_tercapai = 0;
                                $total = 0;
                                foreach ($capaian as $data) :

                                    $capaian = $data['capaian'];
                                    $capaianHasil = $data['capaianHasil'];
                                    
                                    $total++;
                                    $sudah_tercapai = $sudah_tercapai + MyHelper::setJawaban($capaianHasil['capaian_target'], $capaianHasil['realisasi_target'], $capaian['ket_jawaban'])['nilai'];
                                ?>
                                    <tr>
                                        <td><?= $capaian['sasaran_stategis'] ?></td>
                                        <td><?= $capaian['no_indikator'] ?></td>
                                        <td><?= $capaian['indikator_kinerja'] ?></td>
                                        <td><?= $capaian['satuan'] ?></td>
                                        <td><?= $capaian['target_unida'] ?></td>
                                        <td><?= MyHelper::getBidangRektor($capaian['bidang_wr']) ?></td>
                                        <td class="text-center"><?= $capaianHasil['realisasi_target'] ?></td>
                                        <td class="text-center"><?= MyHelper::getTargetJawaban($capaian['ket_jawaban']) ?></td>
                                        <td class="text-center"><strong><?= $capaianHasil['capaian_target'] ?></strong></td>
                                        <td class="text-center"><?= MyHelper::setJawaban($capaianHasil['capaian_target'], $capaianHasil['realisasi_target'], $capaian['ket_jawaban'])['result'] ?></td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>

                    </div>


                </div>

            </div>
        </div>
    </div>

</div>

<?php JSRegister::begin() ?>
<script>
    $(".sudah_tercapai").text(<?= $sudah_tercapai ?>);
    $(".belum_tercapai").text(<?= $total - $sudah_tercapai ?>);
</script>
<?php JSRegister::end() ?>