<?php

use app\models\UnitKerja;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CapaianHasilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Input Capaian Hasil Periode ' . $periode->periode;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><strong><?= Html::encode($this->title) ?></strong></h3>
            </div>
            <div class="x_content">


                <div class="row">
                    <div class="col-md-5 col-lg-5 col-xs-12">

                        <div class="form-group">
                            <label class="control-label ">Satuan Kerja</label>
                            <?= Select2::widget([
                                // 'id'        => 'satker_id',
                                'name'      => 'satker_id',
                                'data'      => ArrayHelper::map(UnitKerja::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'nama'),
                                // 'value'     => $unitKerja,
                                'options'   => ['placeholder' => Yii::t('app', '- Pilih Unit Kerja -')],
                                'pluginOptions'     => [
                                    'allowClear'    => true,
                                    // 'multiple'      => true,
                                ],
                            ]); ?>
                        </div>


                        <div class="form-group clearfix">
                            <button type="submit" class="btn btn-sm btn-primary" name="btn-cari" value="1" disabled><i class="fa fa-search"></i>
                                Cari</button>
                            <?= Html::a('<i class="fa fa-reply"></i> Back', ['input'], ['class' => 'btn btn-sm btn-info']) ?>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped" id="tabel-capaian">

                            <thead>
                                <tr>
                                    <th>Sasaran strategis</th>
                                    <th class="text-center">No Indikator</th>
                                    <th>Indikator Kinerja</th>
                                    <th class="text-center">Satuan</th>
                                    <th>Target UNIDA</th>
                                    <th>Bidang WR</th>
                                    <th colspan="2">Target Capaian</th>
                                    <th colspan="2">Realisasi Target</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>

                <button id="btn_update_nilai_dosen" class="btn btn-success"><i class="fa fa-pencil"></i> Update</button>

            </div>
        </div>
    </div>

</div>
<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "[name='btn-cari']", function(e) {
        satker_id = $("[name='satker_id']").val()
        periode_id = <?= $_GET['periode_id'] ?>

        getKompetensi(periode_id, satker_id)
    })

    var list_kompetensi = []

    function getKompetensi(periode_id, satker_id) {

        var obj = new Object;
        obj.periode_id = periode_id;
        obj.satker_id = satker_id;
        var ajax_url = "/capaian-hasil/ajax-get-master-capaian";

        $(this).ajaxDataPost(ajax_url, obj, function(err, hasils) {

            // console.log(hasils);
            var row = '';
            $('#tabel-capaian > tbody').empty();

            var counter = 0;
            var list_kategori = []
            var list_values = []

            $.each(hasils, function(i, objects) {

                var counter_label = "";
                if (!list_kategori.includes(objects.induk)) {
                    counter++;
                    counter_label = counter
                }

                // console.log(objects.sasaran_strategis);

                row += '<tr>';
                row += '<td>' + objects.sasaran_strategis + '</td>';
                row += '<td>' + objects.no_indikator + '</td>';
                row += '<td>' + objects.indikator_kinerja + '</td>';
                row += '<td>' + objects.satuan + '</td>';

                row += '<td>' + objects.target_unida + '</td>';
                row += '<td>' + objects.bidang_wr + '</td>';
                row += '<td><input type="number" data-unit_kerja_id=' + objects.unit_kerja_id + ' data-periode_id=' + objects.periode_id + ' data-capaian_id=' + i + '  class="capaian_target" value=' + objects.capaian_target + '></td>';
                row += '<td>' + objects.capaian_target + '</td>';
                row += '<td><input type="number" data-realisasi_id=' + i + ' class="realisasi_target" value=' + objects.realisasi_target + '></td>';
                row += '<td>' + objects.realisasi_target + '</td>';
                // row += '<button id="btn_update_nilai_dosen" class="btn btn-success"><i class="fa fa-pencil"></i> Update</button>';
                row += '</tr>';

            });

            $('#tabel-capaian > tbody').append(row);

        });

    }

    $(document).on("click", "#btn_update_nilai_dosen", function(e) {
        // var nilai_realisasi = "";
        // var realisasi_id = "";

        var objArray = []
        var objData = []
        var objArrayRealisasi = []

        $(".realisasi_target").each(function(i, item) {
            var obj = new Object;

            obj.nilai_realisasi = item.value

            objArrayRealisasi.push(obj)

        });

        $(".capaian_target").each(function(i, item) {
            var obj = new Object;

            obj.unit_kerja_id = $(this).data("unit_kerja_id")
            obj.capaian_kinerja_id = $(this).data("capaian_id")
            obj.periode_id = $(this).data("periode_id")
            obj.capaian_target = item.value
            obj.realisasi_target = objArrayRealisasi[i].nilai_realisasi

            objData.push(obj)

        });
        
        var data = new Object;
        data.master = 1

        objArray.push(objData)
        objArray.push(data)

        $.ajax({
            type: "POST",
            data: {
                dataPost: objArray
            },
            async: true,
            url: "/capaian-hasil/ajax-realisasi-capaian",
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Processing your request...",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },

            success: function(data) {
                var res = $.parseJSON(data);

                console.log(res);
                if (res.code != 200) {
                    alert(res.message)
                } else {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: res.message
                    }).then(() => {
                        location.reload()
                    })

                }

            }
        });

    });

    $(document).on("change", "[name='satker_id']", function(e) {
        var selectedValue = $(this).val();
        if (selectedValue === null || selectedValue === '') {
          $("[name='btn-cari']").prop('disabled', true);
        } else {
          $("[name='btn-cari']").prop('disabled', false);
        }
      });
</script>
<?php JSRegister::end() ?>