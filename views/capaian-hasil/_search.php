<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CapaianHasilSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="capaian-hasil-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'unit_kerja_id') ?>

    <?= $form->field($model, 'capaian_kinerja_id') ?>

    <?= $form->field($model, 'periode_id') ?>

    <?= $form->field($model, 'capaian_target') ?>

    <?php // echo $form->field($model, 'realisasi_target') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
