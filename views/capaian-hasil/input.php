<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CapaianHasilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Input Hasil Capaian';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .index-content a:hover {
        color: black;
        text-decoration: none;
    }

    .index-content {
        margin-bottom: 20px;
        padding: 50px 0px;

    }

    .index-content .row {
        margin-top: 20px;
    }

    .index-content a {
        color: black;
    }

    .index-content .card {
        background-color: #FFFFFF;
        padding: 0;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);

    }

    .index-content .card:hover {
        box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3);
        color: black;
    }

    .index-content .card img {
        width: 100%;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .index-content .card h4 {
        margin: 20px;
    }

    .index-content .card p {
        margin: 20px;
        opacity: 0.65;
    }

    .index-content .blue-button {
        width: 100px;
        -webkit-transition: background-color 1s, color 1s;
        /* For Safari 3.1 to 6.0 */
        transition: background-color 1s, color 1s;
        min-height: 20px;
        background-color: #002E5B;
        color: #ffffff;
        border-radius: 4px;
        text-align: center;
        font-weight: lighter;
        margin: 0px 20px 15px 20px;
        padding: 5px 0px;
        display: inline-block;
    }

    .index-content .blue-button:hover {
        background-color: #dadada;
        color: #002E5B;
    }

    @media (max-width: 768px) {

        .index-content .col-lg-4 {
            margin-top: 20px;
        }
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><strong><?= Html::encode($this->title) ?></strong></h3>
            </div>
            <div class="x_content">

                <div class="row">

                    <?php
                    foreach ($dataPeriode as $data) :
                    ?>
                        <div class="col-md-3 col-sm-3  ">
                            <div class="x_panel ui-ribbon-container">
                                <?php
                                $disabled = 'disabled';
                                if ($data->status_aktif == 1) :
                                    $disabled = '';
                                ?>
                                    <div class="ui-ribbon-wrapper">
                                        <div class="ui-ribbon">
                                            Active
                                        </div>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <div class="x_title">
                                    <h3><strong><?= $data->periode ?></strong></h3>
                                    <div class="clearfix"></div>
                                    <p><?= $data->tahun ?></p>
                                </div>
                                <div class="x_content">
                                    <table>
                                        <tr>
                                            <td>
                                                <?= Html::a('<i class="fa fa-pencil"></i> Input Capaian', ['input-capaian', 'periode_id' => $data->id], ['class' => 'btn btn-sm ' . $disabled . ' btn-success']) ?>
                                            </td>
                                            <td>
                                                <?= Html::a('<i class="fa fa-eye"></i> Lihat Capaian', ['view', 'periode_id' => $data->id], ['class' => 'btn btn-sm btn-primary']) ?>
                                            </td>
                                            <?php
                                            if ($data->status_aktif == 1 && Yii::$app->user->can('admin')) :
                                            ?>
                                                <td>
                                                    <?= Html::a('<i class="fa fa-gear"></i> Master Input', ['master-input-capaian', 'periode_id' => $data->id], ['class' => 'btn btn-sm btn-info']) ?>
                                                </td>
                                            <?php
                                            endif;
                                            ?>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>

                </div>


            </div>
        </div>
    </div>

</div>