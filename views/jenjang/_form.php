<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jenjang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Nama</label>
        <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>
    <div class="form-group">
        <label class="control-label">Urutan</label>
        <?= $form->field($model, 'urutan', ['options' => ['tag' => false]])->textInput()->label(false) ?>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>