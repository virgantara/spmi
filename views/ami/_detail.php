`<table 00000000id="w3" class="table table-striped table-bordered detail-view">
    <thead>
        <tr>
            <th><?= Yii::t('app', 'Tanggal Mulai AMI') ?></th>
            <th><?= Yii::t('app', 'Tanggal Selesai AMI') ?></th>
            <th><?= Yii::t('app', 'Tanggal Mulai Visitasi') ?></th>
            <th><?= Yii::t('app', 'Tanggal Selesai Visitasi') ?></th>
            <th><?= Yii::t('app', 'Tanggal Mulai Penilaian') ?></th>
            <th><?= Yii::t('app', 'Tanggal Selesai Penilaian') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $model->tanggal_ami_mulai ?></td>
            <td><?= $model->tanggal_ami_selesai ?></td>
            <td><?= $model->tanggal_visitasi_mulai ?></td>
            <td><?= $model->tanggal_visitasi_selesai ?></td>
            <td><?= $model->tanggal_penilaian_mulai ?></td>
            <td><?= $model->tanggal_penilaian_selesai ?></td>
        </tr>
    </tbody>
</table>