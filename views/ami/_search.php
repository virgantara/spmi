<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ami-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'tahun') ?>

    <?= $form->field($model, 'tanggal_ami_mulai') ?>

    <?= $form->field($model, 'tanggal_ami_selesai') ?>

    <?php // echo $form->field($model, 'tanggal_visitasi_mulai') ?>

    <?php // echo $form->field($model, 'tanggal_visitasi_selesai') ?>

    <?php // echo $form->field($model, 'tanggal_penilaian_mulai') ?>

    <?php // echo $form->field($model, 'tanggal_penilaian_selesai') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'status_aktif') ?>

    <?php // echo $form->field($model, 'periode_id') ?>

    <?php // echo $form->field($model, 'form_rencana_kode') ?>

    <?php // echo $form->field($model, 'form_rencana_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_rencana_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_rencana_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_tilik_kode') ?>

    <?php // echo $form->field($model, 'form_tilik_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_tilik_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_tilik_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_temuan_kode') ?>

    <?php // echo $form->field($model, 'form_temuan_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_temuan_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_temuan_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_stemuan_kode') ?>

    <?php // echo $form->field($model, 'form_stemuan_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_stemuan_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_stemuan_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_berita_kode') ?>

    <?php // echo $form->field($model, 'form_berita_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_berita_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_berita_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_atl_kode') ?>

    <?php // echo $form->field($model, 'form_atl_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_atl_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_atl_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'form_kehadiran_kode') ?>

    <?php // echo $form->field($model, 'form_kehadiran_tanggal_pembuatan') ?>

    <?php // echo $form->field($model, 'form_kehadiran_tanggal_revisi') ?>

    <?php // echo $form->field($model, 'form_kehadiran_tanggal_efektif') ?>

    <?php // echo $form->field($model, 'nomor_surat_tugas') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'ketua_bpm') ?>

    <?php // echo $form->field($model, 'nidn_ketua_bpm') ?>

    <?php // echo $form->field($model, 'path_cover') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
