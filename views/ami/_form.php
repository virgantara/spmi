<?php

use app\helpers\MyHelper;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ami */
/* @var $form yii\widgets\ActiveForm */

$years = MyHelper::getYears();

?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <label class="control-label">Nama Periode</label>
                <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'placeholder' => 'Masukkan nama periode', 'maxlength' => true])->label(false) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'tahun')->widget(Select2::classname(), [
                    'data'    => MyHelper::getYears(),
                    'options' => ['placeholder' => Yii::t('app', '- Pilih Tahun -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label('Tahun') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_ami_mulai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Awal Penyusunan LED') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_visitasi_mulai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Awal Asesmen Kecukupan') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_penilaian_mulai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Awal Asesmen Lapangan') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_ami_selesai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Akhir Penyusunan LED') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_visitasi_selesai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Akhir Asesmen Kecukupan') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'tanggal_penilaian_selesai', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])->label('Tanggal Akhir Asesmen Lapangan') ?>
            </div>
            <div class="col-md-12">
                <h4>HEADER FORMULIR</h4>
                <hr>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_rencana_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_tilik_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_temuan_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_stemuan_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_berita_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_atl_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_rencana_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_tilik_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_temuan_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_stemuan_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_berita_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_atl_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_rencana_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_tilik_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_temuan_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_stemuan_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_berita_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_atl_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_rencana_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_tilik_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_temuan_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_stemuan_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_berita_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'form_atl_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                    'readonly' => true,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                        'tag' => false,
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ]) ?>
            </div>
            <div class="col-md-12">
                <hr>
                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'form_kehadiran_kode', ['options' => ['tag' => false]])->textInput(['placeholder' => Yii::t('app', 'Kode formulir')]) ?>

                        <?= $form->field($model, 'form_kehadiran_tanggal_pembuatan', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                            'readonly' => true,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                                'tag' => false,
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                            ]
                        ]) ?>
                        <?= $form->field($model, 'form_kehadiran_tanggal_revisi', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                            'readonly' => true,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                                'tag' => false,
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                            ]
                        ]) ?>
                        <?= $form->field($model, 'form_kehadiran_tanggal_efektif', ['options' => ['tag' => false]])->widget(DatePicker::className(), [
                            'readonly' => true,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Tanggal kegiatan'),
                                'tag' => false,
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                            ]
                        ]) ?>
                    </div>
                    <div class="col-md-10">
                        <label class="control-label">Nomor Surat Tugas</label>
                        <?= $form->field($model, 'nomor_surat_tugas', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'placeholder' => 'Masukkan nomor surat tugas', 'maxlength' => true])->label(false) ?>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <label class="control-label">Keterangan</label>
                <?= $form->field($model, 'keterangan', ['options' => ['tag' => false]])->textarea(['rows' => 3])->label(false) ?>
            </div>
            <div class="col-md-12">
                <label class="control-label">Status aktif</label>
                <?= $form->field($model, 'status_aktif', ['options' => ['tag' => false]])->radioList(\app\helpers\MyHelper::getStatusAktif())->label(false) ?>
            </div>

        </div>
    </div>


    <div class="form-group">

    </div>


    <div class="form-group">

    </div>




    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>