<?php

use kartik\form\ActiveForm;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ami */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Audit Mutu Internal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$listAmiUnit = ArrayHelper::map($amiUnit, 'unit_id', 'status_ami');
?>

<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="green"><i class="fa fa-leaf"></i> <?= $model->nama ?? '' ?> </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3  profile_left">
                    <div class="profile_img">
                        <div id="crop-avatar">
                            <!-- <img class="img-responsive avatar-view" src="<?= \yii\helpers\Url::to(['ami/cover', 'id' => $model->id]); ?>" /> -->
                            <img class="img-responsive avatar-view" width="300px" height="450px" src="<?= $model->path_cover ?>" />
                        </div>
                    </div>
                    <h3><?= $model->nama ?? '' ?></h3>
                    <ul class="list-unstyled user_data">
                        <li><i class="fa fa-calendar"></i> <?= $model->tahun ?? '' ?>
                        </li>
                    </ul>
                    <?= Html::a('<i class="fa fa-edit m-right-xs"></i> ' . Yii::t('app', 'Edit Cover'), 'javascript:void(0)', ['class' => 'btn btn-success btn-xs', 'data-toggle' => 'modal', 'data-target' => '#uploadModal']) ?>

                    <br />

                    <!-- <h4>Skills</h4>
                    <ul class="list-unstyled user_data">
                        <li>
                            <p>Web Applications</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                            </div>
                        </li>
                        <li>
                            <p>Website Design</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                            </div>
                        </li>
                        <li>
                            <p>Automation & Testing</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                            </div>
                        </li>
                        <li>
                            <p>UI / UX</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                            </div>
                        </li>
                    </ul> -->

                </div>
                <div class="col-md-9 col-sm-9 ">
                    <div class="profile_title">
                        <div class="col-md-6">
                            <h2>AMI Report</h2>
                        </div>
                    </div>

                    <div id="graph_bar" style="width:100%; height:280px;"></div>

                    <div class role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Auditee</a>
                            </li>
                            <li role="presentation" class="disabled custom-tab-item"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Standar Mutu</a>
                            </li>
                            <li role="presentation" class="disabled custom-tab-item"><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Rangkuman Hasil AMI</a>
                            </li>
                            <li role="presentation" class="disabled custom-tab-item"><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Laporan</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane active " id="tab_content1" aria-labelledby="home-tab">

                                <table class="data table table-striped no-margin">
                                    <thead>
                                        <tr>
                                            <?php if ($model->status_aktif == 1) : ?>
                                                <th style="text-align: center;">
                                                    <input data-unit="all" type="checkbox" <?= $checkedAll ?> id="cb-ami-unit-all">
                                                </th>
                                            <?php endif; ?>
                                            <th>
                                                No
                                            </th>
                                            <th>Auditee</th>
                                            <th>Singkatan</th>
                                            <th>Penanggung Jawab</th>
                                            <th style="text-align: center;">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($auditee as $key => $a) :

                                            $checked = '';
                                            $label = '-';
                                            if (isset($listAmiUnit[$a->id])) {
                                                $checked = 'checked';

                                                if ($listAmiUnit[$a->id] == 1) $label = Html::tag('label', 'Selesai', ['class' => 'btn btn-success btn-xs']);
                                                else $label = Html::tag('label', 'Proses', ['class' => 'btn btn-warning btn-xs']);
                                            }
                                        ?>
                                            <tr>
                                                <?php if ($model->status_aktif == 1) : ?>
                                                    <td style="text-align: center;"><input data-unit="<?= $a->id ?>" type="checkbox" <?= $checked ?> class="cb-ami-unit"></td> <!-- Checkbox pada setiap baris -->
                                                <?php endif; ?>
                                                <td><?= $no ?></td>
                                                <td><?= $a->nama ?></td>
                                                <td><?= $a->singkatan ?></td>
                                                <td><?= $a->penanggung_jawab ?></td>
                                                <td style="text-align: center;"><?= $label ?></td>
                                            </tr>
                                        <?php
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>


                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                <ul class="messages">
                                    <li>
                                        <div class="message_wrapper">
                                            <h4 class="heading">Desmond Davison</h4>
                                            <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                            <br />
                                            <p class="url">
                                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="message_wrapper">
                                            <h4 class="heading">Desmond Davison</h4>
                                            <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                            <br />
                                            <p class="url">
                                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="message_wrapper">
                                            <h4 class="heading">Desmond Davison</h4>
                                            <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                            <br />
                                            <p class="url">
                                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                            </p>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                    photo booth letterpress, commodo enim craft beer mlkshk </p>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                    photo booth letterpress, commodo enim craft beer mlkshk </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Unggah Bukti Kehadiran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/ami/upload-cover" method="post" id="form-upload" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <label for="">Cover Laporan</label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="hidden" name="id" value="<?= $model->id ?>" required>
                                <input type="file" name="file" id="file" class="" required>
                            </div>

                        </div>
                        <small>Tipe File: jpg/png, max file size: 2MB </small>
                    </div>

                    <br><br>
                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'Pastikan kebenaran data!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $('ul.nav-tabs li.disabled a').removeAttr('data-toggle');

    // Menonaktifkan tab yang dinonaktifkan
    $('ul.nav-tabs li.disabled a').on('click', function(e) {
        e.preventDefault();
        return false;
    });
    $(document).ready(function() {
        $('.cb-ami-unit').on('change', function() {
            var obj = new Object;
            obj.unit_id = $(this).data('unit');
            obj.isChecked = $(this).is(':checked');

            if (!obj.isChecked) {
                Swal.fire({
                    title: 'Apakah Anda yakin?',
                    text: "Data ini akan dihapus!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Lakukan AJAX jika user mengonfirmasi penghapusan
                        ajaxUpdate(obj);
                    } else {
                        // Kembalikan checkbox ke state sebelumnya
                        $(this).prop('checked', true);
                    }
                });
            } else {
                // Lakukan AJAX untuk aksi "checked"
                ajaxUpdate(obj);
            }
        });

        $('#cb-ami-unit-all').on('change', function() {
            var obj = {};
            obj.isChecked = $(this).is(':checked');

            if (!obj.isChecked) {
                Swal.fire({
                    title: 'Apakah Anda yakin?',
                    text: "Semua data ini akan dihapus! Ketik 'hapus' untuk mengonfirmasi.",
                    icon: 'warning',
                    input: 'text',
                    inputPlaceholder: 'Ketik "hapus"',
                    inputValidator: (value) => {
                        if (value.toLowerCase() !== 'hapus') {
                            return 'Anda harus mengetik "hapus" untuk melanjutkan!';
                        }
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        ajaxUpdateAll(obj);
                    } else {
                        $(this).prop('checked', true);
                    }
                });
            } else {
                ajaxUpdateAll(obj);
            }
        });


        function ajaxUpdate(obj) {
            $.ajax({
                url: '/ami-unit/ajax-add',
                method: 'POST',
                data: {
                    dataPost: obj
                },
                success: function(response) {
                    Swal.close();
                    var hasil = $.parseJSON(response);
                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        }).then(res => {
                            $("#modal").modal("hide");
                        });

                        $.pjax.reload({
                            container: "#pjax-container"
                        });
                        $("#nama_auditor").val("").focus();
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.error(status + ': ' + error);
                }
            });
        }

        function ajaxUpdateAll(obj) {
            $.ajax({
                url: '/ami-unit/ajax-add-all',
                method: 'POST',
                data: {
                    dataPost: obj
                },
                success: function(response) {
                    Swal.close();
                    var hasil = $.parseJSON(response);
                    if (hasil.code == 200) {
                        Swal.fire({
                            title: 'Yeay!',
                            icon: 'success',
                            text: hasil.message
                        }).then(res => {
                            $("#modal").modal("hide");
                            window.location.reload();
                        });

                        $.pjax.reload({
                            container: "#pjax-container"
                        });
                        $("#nama_auditor").val("").focus();
                    } else {
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        }).then(res => {
                            window.location.reload();
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.error(status + ': ' + error);
                }
            });
        }
    });
</script>
<?php JSRegister::end() ?>