<?php

use app\helpers\MyHelper;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Monev */

$this->title = $model->nama_monev;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoring dan Evaluasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nama_monev',
                        'tahun',
                        'periode',
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return MyHelper::getStatusAktif()[$data->status];
                            }
                        ]
                    ],
                ]) ?>

                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title text-center">No.</th>
                                <th class="column-title text-center">Nama Satuan Kerja/Fakultas</th>
                                <th class="column-title text-center">Jenis</th>
                                <th class="column-title text-center">Monev </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nomor = 1;
                            foreach ($unitKerja as $unit) : ?>
                                <tr class="even pointer">
                                    <td class="text-center"><?= $nomor; ?>.</td>
                                    <td class=" "><?= $unit->nama; ?> </td>
                                    <td class=" "><?= MyHelper::getJenisUnit()[$unit->jenis] ?> </td>
                                    <td class="text-center">
                                        <?= Html::a('<i class="fa fa-eye"></i> Lihat', [
                                            'monev-unit-kerja/to-view',
                                            'monev_id' => $model->id,
                                            'unit_kerja_id' => $unit->id,
                                        ], [
                                            'class' => 'btn btn-sm btn-success',
                                            'title' => 'Lihat Monev',
                                            'data-pjax' => '0',
                                        ]); ?>

                                        <?= Html::a(
                                            '<i class="fa fa-file-pdf-o"></i> Laporan',
                                            // [
                                            //     'amanah-kinerja-unit-kerja/detail',
                                            //     'unit_kerja_id' => $unit->id,
                                            //     'monev_id' => $model->id,
                                            // ],
                                            ['monev-unit-kerja/laporan-download'],
                                            [
                                                'class' => 'btn btn-sm btn-danger',
                                                'title' => 'Lihat Monev',
                                                'target' => '_blank',
                                            ]
                                        ); ?>
                                    </td>
                                </tr>
                            <?php
                                $nomor++;
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>


<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        $('#btn-delete').click(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Peringatan!',
                text: "Apakah Anda yakin ingin menghapus item ini? Tindakan ini akan menghapus seluruh data amanah kinerja seluruh satuan kerja/fakultas.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: 'Konfirmasi',
                        text: 'Untuk melanjutkan, ketik "delete":',
                        input: 'text',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        cancelButtonText: 'Cancel',
                        inputValidator: (value) => {
                            if (value !== 'delete') {
                                return 'Input tidak valid. Ketik "delete" untuk melanjutkan.';
                            }
                        }
                    }).then((inputResult) => {
                        if (inputResult.isConfirmed) {
                            $.ajax({
                                type: "POST",
                                url: "<?= Url::to(['delete', 'id' => $model->id]) ?>",
                                data: {
                                    _csrf: yii.getCsrfToken()
                                },
                                success: function(data) {
                                    var parsedData = $.parseJSON(data);

                                    if (parsedData.code == 200) {
                                        Swal.fire({
                                            title: 'Berhasil!',
                                            icon: 'success',
                                            text: parsedData.message
                                        }).then(function() {
                                            window.location.href = '<?= Url::to(['index']) ?>';
                                        });

                                    } else {
                                        Swal.fire({
                                            title: 'Gagal!',
                                            icon: 'error',
                                            text: parsedData.message
                                        });
                                    }
                                },
                                error: function(xhr, status, error) {
                                    Swal.fire("Error!", "Gagal menghapus data!", "error");
                                },
                            });
                        }
                    });
                }
            });
        });
    })
</script>
<?php JSRegister::end() ?>