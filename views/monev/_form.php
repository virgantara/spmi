<?php

use app\helpers\MyHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Monev */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <?= $form->field($model, 'id', ['options' => ['tag' => false]])
        ->hiddenInput([
            'class' => 'form-control',
            'readonly' => true,
            'value' => $model->id === null ? MyHelper::gen_uuid() : $model->id
        ])
        ->label(false) ?>

    <div class="form-group">
        <label class="control-label">Nama monev</label>
        <?= $form->field($model, 'nama_monev', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Tahun</label>
        <?= $form->field($model, 'tahun', ['options' => ['tag' => false]])->widget(Select2::classname(), [
            'data'    => MyHelper::getYears(),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Tahun -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Periode</label>
        <?= $form->field($model, 'periode', ['options' => ['tag' => false]])
            ->input('number')
            ->label(false)
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status</label>
        <?= $form->field($model, 'status', ['options' => ['tag' => false]])->radioList(
            MyHelper::getStatusAktif(),
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $checked = $value == 1 ? 'checked' : '';
                    return "<label><input type='radio' name='{$name}' value='{$value}' {$checked}> {$label}</label>";
                }
            ]
        )->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>