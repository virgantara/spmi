<?php

use app\helpers\MyHelper;
use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmanahKinerjaMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Amanah Kinerja Template');
$this->params['breadcrumbs'][] = $this->title;
$jenisAmanah = [
    'satker' => 'Satuan kerja',
    'fakultas' => 'Fakultas'
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <p>
                    <?= Html::a(Yii::t('app', 'Create Amanah Kinerja Master'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'filter' => $jenisAmanah,
                        'refreshGrid' => true,
                        'contentOptions'    => [
                            'width' => '8%',
                        ],
                        'hAlign' => 'center',
                        'attribute' => 'jenis_amanah_kinerja',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => $jenisAmanah,
                            'options' => [
                                'placeholder' => 'Pilih jenis',
                            ],
                        ],
                        'value' => function ($data) use ($jenisAmanah) {
                            return $jenisAmanah[$data->jenis_amanah_kinerja];
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'label' => 'Nama Satuan Kerja',
                        'filter' => $listUnitKerja,
                        'contentOptions'    => [
                            'width' => '25%',
                        ],
                        'refreshGrid' => true,
                        'attribute' => 'unit_kerja_id',
                        'readonly' => function ($data) {
                            if ($data->jenis_amanah_kinerja == 'satker')
                                return !Yii::$app->user->can('admin');

                            return true;
                        },
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => $listUnitKerja,
                            'options' => [
                                'placeholder' => 'Pilih status',
                            ],
                        ],
                        'value' => function ($data) use ($listUnitKerja) {
                            return $listUnitKerja[$data->unit_kerja_id] ?? '-';
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'amanah_kinerja',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '40%',
                        ],
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA, // Atau sesuai dengan kebutuhan
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 4, // Jumlah baris dalam textarea
                                'placeholder' => 'Masukkan amanah kinerja',
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->amanah_kinerja ?? '-';
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'target_umum',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '10%',
                        ],
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA, // Atau sesuai dengan kebutuhan
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 4, // Jumlah baris dalam textarea
                                'placeholder' => 'Masukkan amanah kinerja',
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->target_umum ?? '-';
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'filter' => MyHelper::getStatusAktif(),
                        'refreshGrid' => true,
                        'contentOptions'    => [
                            'width' => '8%',
                        ],
                        'hAlign' => 'center',
                        'attribute' => 'status_aktif',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::getStatusAktif(),
                            'options' => [
                                'placeholder' => 'Pilih status',
                            ],
                        ],
                        'value' => function ($data) {
                            return MyHelper::getStatusAktif()[$data->status_aktif];
                        }
                    ],

                    //'created_at',
                    //'updated_at',
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        if ($model->jenis_amanah_kinerja == 'satker' && $model->unit_kerja_id == null) {
                            return ['style' => 'background-color: rgba(255, 0, 0, 0.2);'];
                            // return ['style' => 'background-color: #FFDAB9;'];
                        }
                    },
                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>