<?php

use app\helpers\MyHelper;
use app\models\UnitKerja;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmanahKinerjaMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Satuan Kerja <small>*wajib jika jenis amanah kinerja satuan kerja</small></label>
        <?= $form->field($model, 'unit_kerja_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(UnitKerja::find()->where(['jenis' => 'satker'])->all(), 'id', 'nama'),
            'options' => ['placeholder' => Yii::t('app', 'Pilih Satuan Kerja')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>

    </div>

    <div class="form-group">
        <label class="control-label">Jenis amanah kinerja <small>*wajib</small></label>
        <?= $form->field($model, 'jenis_amanah_kinerja')->widget(Select2::classname(), [
            'data' => [
                'fakultas' => 'Fakultas',
                'satker' => 'Satker'
            ],
            'options' => ['placeholder' => Yii::t('app', 'Pilih Jenis Amanah Kinerja')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
        <small>*jika satuan kerja, maka wajib memilih satuan kerja</small>
    </div>

    <div class="form-group">
        <label class="control-label">Amanah kinerja</label>
        <?= $form->field($model, 'amanah_kinerja', ['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Target Umum <small>*kosongkan jika tidak ada</small></label>
        <?= $form->field($model, 'target_umum', ['options' => ['tag' => false]])->textInput(['placeholder' => 'Ketik target umum'])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status aktif</label>
        <?= $form->field($model, 'status_aktif')->radioList(MyHelper::getStatusAktif(), ['value' => 1])->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>