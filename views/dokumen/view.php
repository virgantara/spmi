<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dokumen */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Dokumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <div class="block-header">
                    <h2><strong>Dokumen: <?= Html::encode($this->title) ?></strong></h2>
                </div>
                <?= Html::a('<i class="fa fa-share"></i> Back', ['personal'], ['class' => 'btn btn-info btn-sm']) ?>
                <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nama',
                        [
                            'label'     => 'Pemilik dokumen',
                            'value'     => function ($data) {
                                return (!empty($data->unitKerja) ? $data->unitKerja->singkatan : null);
                            }
                        ],
                        // 'jenis_id',
                        [
                            'attribute' => 'jenis_id',
                            'label'     => 'Jenis',
                            'value'     => function ($data) {
                                return $data->jenis->nama;
                            }
                        ],
                        [

                            'format'    => 'raw',
                            'label'     => 'Unduh Dokumen',
                            'value'     => function ($data) {
                                if (!empty($data->s3)) {
                                    // return html::a('<i class="fa fa-close" style="color: red;"> Tidak ada Dokumen</i>');
                                    if (!empty($data->link)) {
                                        return html::a('<i class="fa fa-close" style="color: red;"> Tidak ada Dokumen</i>');
                                    } else {

                                        return html::a('<i class="fa fa-download"> </i> Unduh', (!empty($data->link) ? $data->link : null), ['target' => '_blank', 'class' => 'btn btn-xs btn-primary']);
                                    }
                                } else {
                                    return html::a('<i class="fa fa-download"> </i> Unduh', ['download', 'id' => $data->id], ['target' => '_blank', 'data-pjax' => 0, 'class' => 'btn btn-xs btn-primary']);
                                }
                            },
                        ],
                        'keterangan:ntext',
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>