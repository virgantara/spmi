<?php

use app\models\MasterJenis;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumen';
$this->params['breadcrumbs'][] = $this->title;
$list_jenis = MasterJenis::find()->where([
    'set_to' => 'dokumen',
    'status_aktif' => 1,
])->all();

$list_unit = ArrayHelper::map(\app\models\UnitKerja::find()->all(), 'id', 'singkatan');
?>
<style>
    td.my-grid {
        color: #2B2C2F !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                 
                <p>
                    <?= Html::a('<i class="fa fa-plus"></i> Tambah Dokumen', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
                </p>

                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'attribute' => 'nama',
                        'contentOptions' => ['width' => '30%'],
                        'label' => 'Nama Dokumen',
                        'value' => function ($data) {
                            return $data->nama;
                        },
                    ],
                    [
                        'attribute' => 'jenis_id',
                        'label' => 'Jenis Dokumen',
                        'filter' => ArrayHelper::map($list_jenis, 'id', 'nama'),
                        'value' => function ($data) {
                            return (!empty($data->jenis) ? $data->jenis->nama : null);
                        },
                    ],
                    // [
                    //     'attribute' => 'link',
                    //     'format' => 'raw',
                    //     'label' => 'Link Dokumen',
                    //     'value' => function ($data) {
                    //         if (!isset($data->link)) {
                    //             return html::a('<i class="fa fa-close" style="color: red;"></i> Tidak ada Dokumen');
                    //         } else {
                    //             return html::a('<i class="fa fa-link"></i> link dokumen', (!empty($data->link) ? $data->link : null), ['target' => '_blank']);
                    //         }
                    //     },
                    // ],

                    [
                        'attribute' => 'unit_kerja_id',
                        'label' => 'Owner',
                        'contentOptions' => ['width' => '20px'],
                        'filter' => $list_unit,
                        'value' => function ($data) {
                            return (!empty($data->unitKerja) ? $data->unitKerja->singkatan : null);
                        },
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => ['1' => 'Publik', '2' => 'Private'],
                        'value' => function ($data) {
                            return ($data->status == '1' ? 'Publik' : 'Private');
                        }
                    ],
                    [

                        'format' => 'raw',
                        'label' => 'Unduh Dokumen',
                        'value' => function ($data) {
                            if (isset($data->s3)) {
                                if ($data->status == '1') {
                                    return Html::a('<i class="fa fa-download"> </i> Unduh', ['download', 'id' => $data->id], ['target' => '_blank', 'data-pjax' => 0, 'class' => 'btn btn-sm btn-primary']);
                                } else {
                                    return 'This document is private';
                                }
                            } else {
                                return null;
                            }
                        },
                    ],
                    // 'created_at',
                    //'updated_at',
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>

<script>
    function test() {
        window.open('http://www.google.com', '_blank');
        window.open('http://www.youtube.com', '_blank');
    }
</script>