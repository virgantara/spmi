<?php

use app\helpers\MyHelper;
use app\models\MasterJenis;
use app\models\Pengelompokan;
use app\models\Prodi;
use app\models\UnitKerja;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dokumen */
/* @var $form yii\widgets\ActiveForm */

$jenis = MasterJenis::find()->where([
    'set_to' => 'dokumen',
    'status_aktif' => 1,
])->all();
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
            'enctype' => "multipart/form-data"
        ]
    ]); ?>

    <?= $form->errorSummary($model, ['header' => '<div class="alert alert-danger">', 'footer' => '</div>']); ?>

    <div class="form-group">
        <div class="col-md-12">
            <?= $form->field($model, 'jenis_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($jenis, 'id', 'nama'),
                'options' => ['placeholder' => Yii::t('app', '- Pilih Jenis Dokumen -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <?= $form->field($model, 'nama')->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => 'Masukkan nama dokumen'])->label('Nama Dokumen') ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label class="control-label">Upload Dokumen</label>
            <?= $form->field($model, 's3', ['options' => ['tag' => false]])->fileInput(['accept' => 'application/pdf', 'class' => 'form-control'])->label(false) ?>
            <small>Tipe File: pdf, max file size: 10MB</small>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <?= $form->field($model, 'status')->radioList(['1' => 'Publik', '2' => 'Private']) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?php if (Yii::$app->user->can('admin')) : ?>
        <div class="form-group">
            <div class="col-md-12">
                <label class="control-label">Unit Kerja</label>
                <?= $form->field($model, 'unit_kerja_id', ['options' => ['tag' => false]])
                    ->widget(Select2::class, [
                        'data' => ArrayHelper::map(UnitKerja::find()->all(), 'id', 'nama'),
                        'options' => ['placeholder' => '- Pilih Unit Kerja -'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])
                    ->label(false); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <div class="col-md-12">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>