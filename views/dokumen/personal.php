<?php

use app\models\Ami;
use app\models\Auditor;
use app\models\Dokumen;
use app\models\KelompokDokumen;
use app\models\MasterJenis;
use app\models\Pengelompokan;
use app\models\UnitKerja;
use kartik\color\ColorInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dokumen Dokumen AMI - PERSONAL';
$this->params['breadcrumbs'][] = $this->title;
$listJenis = MasterJenis::find()->where([
    'set_to' => 'dokumen',
    'status_aktif' => 1,
])->all();
$statusData = ['1' => 'Publik', '2' => 'Private'];

?>
<style>
    td.my-grid {
        color: #2B2C2F !important;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><strong><?= Html::encode($this->title) ?></strong></h3>
            </div>
            <div class="col-md-12">

                <div class="col-md-9">
                    <?= Html::a(
                        '<i class="fa fa-plus"> </i> Dokumen',
                        [
                            'create',
                        ],
                        [
                            'class' => 'btn btn-sm btn-success'
                        ]
                    ); ?>
                    <?php Html::a(
                        '<i class="fa fa-plus"> </i> Kelompok',
                        '',
                        [
                            'class' => 'btn btn-sm btn-success btn-tambah-kelompok'
                        ]
                    ); ?>
                    <?php Html::a(
                        '<i class="fa fa-gear"> </i> Pengelompokan',
                        '',
                        [
                            'class' => 'btn btn-sm btn-success btn-tambah-pengelompokan'
                        ]
                    ); ?>

                </div>
                <div class="col-md-3">

                </div>
            </div>


            <div class="col-md-12">
                <div class="x_content">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="questionOne">
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                                    <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                                </a>
                            </h5>
                        </div>
                        <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                            <div class="panel-body">

                                <?php $form = ActiveForm::begin([
                                    'method' => 'GET',
                                    'action' => ['dokumen/personal'],
                                    'options' => [
                                        'id' => 'form_validation',
                                    ]
                                ]); ?>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label ">Nama Dokumen</label>
                                            <?= Html::textInput('nama', $_GET['nama'] ?? '', ['class' => 'form-control', 'placeholder' => 'Ketik nama dokumen']) ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label ">Jenis Dokumen</label>
                                            <?= Select2::widget([
                                                'name' => 'jenis_id[]',
                                                'data' => ArrayHelper::map($listJenis, 'id', 'nama'),
                                                'value' => $_GET['jenis_id'] ?? [],
                                                'options' => ['placeholder' => Yii::t('app', 'Pilih Auditee')],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>

                                    <?php if (Yii::$app->user->can('admin')) : ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label ">Auditee</label>
                                                <?= Select2::widget([
                                                    'name' => 'unit_kerja_id[]',
                                                    'data' => ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                                                    'value' => $_GET['unit_kerja_id'] ?? [],
                                                    'options' => ['placeholder' => Yii::t('app', 'Pilih Auditee')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true,
                                                        'multiple' => true,
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                                        Cari</button>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>



                    <div class="alert alert-info">
                        <b><?= Yii::t('app', 'Note:') ?></b>
                        <ul>
                            <li>Dokumen yang diinputkan akan menjadi bahan untuk asesmen</li>
                            <li><?= Html::a('<b>Dokumen yang dihapus akan menghapus penilaian indikator</b>', 'javascript:void(0)', ['style' => 'color:yellow;']) ?></li>
                        </ul>
                    </div>
                    <?php
                    $gridColumns = [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'contentOptions' => ['class' => 'kartik-sheet-style'],
                            'width' => '36px',
                            'pageSummary' => 'Total',
                            'pageSummaryOptions' => ['colspan' => 6],
                            'header' => '',
                            'headerOptions' => ['class' => 'kartik-sheet-style']
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'nama',
                            'contentOptions'    => [
                                'width' => '53%',
                            ],
                            'label' => 'Nama Dokumen',
                            'filterInputOptions' => [
                                'class' => 'form-control',
                                'placeholder' => 'Filter by Nama Dokumen...',
                            ],
                            'readonly' => !Yii::$app->user->can('auditee'),
                            'editableOptions' => [
                                'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                            ],
                            'value' => function ($data) {
                                return (!empty($data->nama) ? $data->nama : null);
                            }
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'jenis_id',
                            'contentOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => ArrayHelper::map($listJenis, 'id', 'nama'),
                            'label' => 'Jenis Dokumen',
                            'refreshGrid' => true,
                            'readonly' => !Yii::$app->user->can('auditee'),
                            'editableOptions' => [
                                'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                                'data' => ArrayHelper::map($listJenis, 'id', 'nama'),
                            ],
                            'value' => function ($data) {
                                return (!empty($data->jenis) ? $data->jenis->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'unit_kerja_id',
                            'contentOptions' => [
                                'width' => '10%',
                            ],
                            'value' => function ($data) {
                                return (!empty($data->unitKerja->singkatan) ? $data->unitKerja->singkatan : null);
                            },
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'status',
                            'contentOptions'    => [
                                'width' => '5%',
                            ],
                            'filter' => $statusData,
                            'hAlign' => 'center',
                            'readonly' => !Yii::$app->user->can('auditee'),
                            'refreshGrid' => true,
                            'editableOptions' => [
                                'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                                'data' => $statusData,
                            ],
                            'value' => function ($data) use ($statusData) {

                                return !empty($statusData[$data->status]) ? $statusData[$data->status] : null;
                            }
                        ],
                        [
                            'format' => 'raw',
                            'label' => 'Unduh',
                            'hAlign' => 'center',
                            'contentOptions' => [
                                'width' => '10%',
                            ],
                            'value' => function ($data) {
                                if (!empty($data->s3)) {
                                    return Html::a(
                                        '<i class="fa fa-download"> </i> Unduh',
                                        [
                                            'download',
                                            'id' => $data->id
                                        ],
                                        [
                                            'target' => '_blank',
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-xs btn-primary'
                                        ]
                                    );
                                }
                            },
                        ],
                        // ['class' => 'yii\grid\ActionColumn']
                        [
                            'format' => 'raw',
                            'value' => function ($data) {
                                $eye = "";
                                $eye .= Html::a('', ['view', 'id' => $data->id], ['class' => 'glyphicon glyphicon-eye-open']);
                                $pencil = "";
                                $pencil .= Html::a('', ['update', 'id' => $data->id], ['class' => 'glyphicon glyphicon-pencil']);
                                $trash = Html::a('', ['delete', 'id' => $data->id], [
                                    'class' => 'glyphicon glyphicon-trash',
                                    'data' => [
                                        'confirm' => 'Menghapus dokumen akan menghapus penilaian asesmen!, Apakah Anda yakin ingin menghapus item ini?',
                                        'method' => 'post',
                                    ],
                                ]);
                                return $eye . ' ' . $pencil . ' ' . $trash;
                            },
                        ],
                    ]; ?>

                    <?= GridView::widget([
                        'pager' => [
                            'options' => ['class' => 'pagination'],
                            'activePageCssClass' => 'active paginate_button page-item',
                            'disabledPageCssClass' => 'disabled paginate_button',
                            'prevPageLabel' => 'Previous',
                            'nextPageLabel' => 'Next',
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last',
                            'nextPageCssClass' => 'paginate_button next page-item',
                            'prevPageCssClass' => 'paginate_button previous page-item',
                            'firstPageCssClass' => 'first paginate_button page-item',
                            'lastPageCssClass' => 'last paginate_button page-item',
                            'maxButtonCount' => 10,
                            'linkOptions' => [
                                'class' => 'page-link'
                            ]
                        ],
                        'dataProvider' => $dataProviderDokumen,
                        'responsiveWrap' => false,
                        'columns' => $gridColumns,
                        'containerOptions' => ['style' => 'overflow: auto'],
                        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                        'containerOptions' => ['style' => 'overflow: auto'],
                        'beforeHeader' => [
                            [
                                'columns' => [
                                    ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                                ],
                                'options' => ['class' => 'skip-export']
                            ]
                        ],
                        'exportConfig' => [
                            GridView::PDF => ['label' => 'Save as PDF'],
                            GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                            GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                            GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                        ],

                        'toolbar' =>  [
                            '{export}',

                            '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                        ],
                        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                        // set export properties
                        'export' => [
                            'fontAwesome' => true
                        ],
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                            'options' => [
                                'id' => 'pjax-container-1',
                            ]
                        ],
                        'id' => 'my-grid-1',
                        'bordered' => true,
                        'striped' => true,
                        // 'condensed' => false,
                        // 'responsive' => false,
                        'hover' => true,
                        // 'floatHeader' => true,
                        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY
                        ],
                    ]); ?>


                </div>
            </div>
        </div>
    </div>

</div>


<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-save-kelompok", function(e) {
        e.preventDefault();

        obj = $("#form-kelompok").serialize()

        $.ajax({
            url: "/pengelompokan/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-led").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-save-kelompok-dokumen", function(e) {
        e.preventDefault();

        obj = $("#form-pengelompokan").serialize()

        $.ajax({
            url: "/kelompok-dokumen/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-led").modal("hide")
                        window.location.reload()
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    // Modal Kelompok

    $(document).on("click", ".btn-tambah-kelompok, .btn-view-kelompok, .btn-update-kelompok", function(e) {
        e.preventDefault();
        var kelompok_id = $(this).data("kelompokid")

        $("#pengelompokan_id").val(kelompok_id)
        $("#pengelompokan_init_id").val(kelompok_id)

        $("#kelompok-modal").modal("show")

    });

    // Modal Pengelompokan

    $(document).on("click", ".btn-tambah-pengelompokan", function(e) {
        e.preventDefault();
        var indikator_id = $(this).data("indikator")
        var ami_unit_id = $(this).data("amiunitid")

        $("#indikator_id").val(indikator_id).focus()
        $("#pengelompokan-modal").modal("show")

    });
</script>
<?php JSRegister::end(); ?>