<?php

use app\helpers\MyHelper;
use app\models\MasterJenis;
use kartik\editable\Editable;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MonevUnitKerjaObjek */

$url = Yii::$app->user->can('admin')
    ? ['monev/view', 'id' => $model->monev->id]
    : ['monev-unit-kerja/view', 'monev_id' => $model->monev_id];

$this->title = $model->unitKerja->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoring dan Evaluasi'), 'url' => ['monev/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->monev->nama_monev), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->pagination = false;
?>

<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
            </div>

            <div class="panel-body">
                <?= $this->render('../monev-unit-kerja/_view', ['model' => $model]) ?>
            </div>

            <div class="panel-heading">
                <?= $this->render('../nav-master/nav_monev', ['id' => $model->id]) ?>
            </div>

            <div class="panel-body">
                <div class="accordion" id="accordionMain" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingImport" data-toggle="collapse" data-parent="#accordionMain" href="#collapseImport" aria-expanded="false" aria-controls="collapseImport">
                            <h4 class="panel-title">Import Data Amanah Kinerja</h4>
                        </a>
                        <div id="collapseImport" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingImport">
                            <div class="panel-body">
                                <form action="" id="form-import-amkin">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <?= Html::hiddenInput('jenis_objek', 'amanah-kinerja', ['readonly' => true]) ?>
                                            <?= Html::hiddenInput('monev_unit_kerja_id', $model->id, ['readonly' => true]) ?>
                                            <?= Html::hiddenInput('unit_kerja_id', $model->unit_kerja_id, ['readonly' => true]) ?>
                                            <?= Html::dropDownList('amanah_kinerja_id', null, $dataAmanahKinerja, ['class' => 'form-control']) ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= Html::submitButton('Import Data', ['class' => 'btn btn-primary', 'id' => 'btn-import']) ?>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'attribute' => 'nama_program',
                        'label' => 'Amanah Kinerja',
                        'contentOptions' => [
                            'width' => '42%',
                        ],
                    ],
                    [
                        'attribute' => 'target',
                        'label' => 'Target',
                        'contentOptions' => [
                            'width' => '20%',
                        ],
                    ],
                    [
                        'attribute' => 'capaian',
                        'label' => 'Capaian',
                        'contentOptions' => [
                            'width' => '20%',
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'status_waktu',
                        'label' => 'Ketepatan Waktu',
                        'contentOptions'    => [
                            'width' => '8%',
                        ],
                        'format' => 'raw',
                        'filter' => MyHelper::statusKetepatanWaktu(),
                        'hAlign' => 'center',
                        'refreshGrid' => true,
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::statusKetepatanWaktu(),
                            'asPopover' => false,
                        ],
                        'value' => function ($data) {
                            return MyHelper::statusKetepatanWaktu()[$data->status_waktu] ?? "Belum dipilih";
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'evaluasi',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '15%',
                        ],
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 3,
                                'placeholder' => 'Ketik evaluasi',
                            ],
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'perbaikan',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '15%',
                        ],
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 3,
                                'placeholder' => 'Ketik perbaikan',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Bukti Pelaksanaan',
                        'format' => 'raw',
                        'hAlign' => 'center',
                        'value' => function ($model) {
                            $html = '';
                            $html .= Html::a('<i class="fa fa-cog"> </i> Edit', 'javascript:void(0)', ['data-objek' => $model->id, 'id' => 'btn-pilihan']);
                            $html .= '<br>';

                            if (!empty($model->getBukti())) {
                                foreach ($model->getBukti() as $bukti) {
                                    $html .= Html::a('<i class="fa fa-file-archive-o"></i>', [
                                        'dokumen/download',
                                        'id' => $bukti->dokumen_id,
                                        'data-pjax' => 0,
                                    ], ['target' => '_blank']);
                                    $html .= ' ';
                                }
                            }
                            return $html;
                        },
                    ],
                    [
                        'label' => '',
                        'value' => function ($model) {
                            return ''; // Kosongkan value
                        },
                        'contentOptions' => [
                            'class' => 'text-center',
                            'style' => 'width: 1px;', // Atur lebar kolom sesuai kebutuhan
                        ],
                    ],
                    //'created_at',
                    //'updated_at',
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        if (empty($model->status_waktu)) {
                            return ['style' => 'background-color: rgba(255, 0, 0, 0.2);'];
                            // return ['style' => 'background-color: #FFDAB9;'];
                        }
                    },
                    'toolbar' =>  [
                        '{export}',
                        '{toggleData}' // Uncomment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => false,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    'hover' => true,
                    'showPageSummary' => false, // Show page summary
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>
            </div>
        </div>

    </div>
</div>



<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-pilihan',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true],
    'header' => '<h4 class="modal-title">Bukti Pelaksanaan</h4>',
]);
?>

<table width="100%" id="table-dokumen">
    <thead>
        <tr>
            <th class="text-center" width="5%">No</th>
            <th>Nama Dokumen</th>
            <th class="text-center" width="5%">File</th>
            <th class="text-center" width="5%">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center" width="5%">1.</td>
            <td>Contoh Dokumen</td>
            <td class="text-center" width="5%"><?= Html::a('<i class="fa fa-file-archive-o"></i>', 'javascript:void(0)') ?></td>
            <td class="text-center" width="5%"><?= Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0)') ?></td>
        </tr>
    </tbody>
</table>
<hr>

<div class="accordion" id="accordionMain" role="tablist" aria-multiselectable="true">
    <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingImport" data-toggle="collapse" data-parent="#accordionMain" href="#collapseTambahDokumen" aria-expanded="false" aria-controls="collapseImport1">
            <h4 class="panel-title">Tambah Dokumen</h4>
        </a>
        <div id="collapseTambahDokumen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingImport">
            <div class="panel-body">
                <form action="" id="form-import-rkat">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Apakah dokumen sudah pernah diinput kedalam SIMUDA ?</label>
                        </div>
                        <div class="col-md-12">
                            <?= Html::button('<i class="fa fa-check-square-o"></i> Sudah', ['class' => 'btn btn-success', 'id' => 'btn-pilih-sudah']) ?>
                            <?= Html::button('<i class="fa fa-times"></i> Belum', ['class' => 'btn btn-danger', 'id' => 'btn-pilih-belum']) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
Modal::end();
?>

<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-dokumen-pilih',
    'size' => 'modal-md',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true],
    'header' => '<h4 class="modal-title">Bukti Pelaksanaan</h4>',
]);
?>

<form action="" id="form-set-dokumen">

    <div class="form-group">
        <label for="">Nama Dokumen</label>
        <?= Html::hiddenInput('dokumen_id', '', ['id' => 'set_bukti_dokumen_id']) ?>
        <?= Html::hiddenInput('monev_unit_kerja_objek_id', '', ['id' => 'monev_unit_kerja_objek_id']) ?>
        <?= Html::textInput('nama_dokumen', '', ['class' => 'form-control dokumen_bukti', 'id' => 'dokumen_lokasi', 'placeholder' => "Ketik nama dokumen"]) ?>
    </div>

    <div class="form-group">
        <?= Html::button('<i class="fa fa-plus"></i> Tambah Dokumen', ['class' => 'btn btn-success', 'id' => 'btn-set-dokumen']) ?>
        <?= Html::button('<i class="fa fa-reply"></i> Kembali', ['class' => 'btn btn-primary', 'id' => 'btn-back-pilihan']) ?>
    </div>

</form>

<?php
Modal::end();
?>

<div class="modal fade" id="modal-dokumen-upload" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Upload Bukti Pelaksanaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/dokumen/upload-bukti-monev" method="post" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Nama Dokumen</label>
                            <?= Html::textInput('nama', '', ['class' => 'form-control', 'id' => 'nama_auditor', 'placeholder' => 'Ketik nama dokumen']) ?>
                            <?= Html::hiddenInput('monev_unit_kerja_objek_id', '', ['id' => 'upload-file-monev-satker-bukti-id']) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Jenis Dokumen</label>
                            <?= Html::dropDownList('jenis_id', '', ArrayHelper::map(MasterJenis::find()->where([
                                'set_to' => 'dokumen',
                                'status_aktif' => 1,
                            ])->all(), 'id', 'nama'), ['class' => 'form-control', 'prompt' => 'Pilih Jenis Dokumen', 'required' => true]) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <label for="">File</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="file" name="file" id="file" class="" required>
                            </div>
                            <small>Tipe File: pdf, max file size: 10MB</small>

                        </div>
                    </div>

                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'File yang di upload akan tersimpan pada fitur "Pelaksanaan -> Dokumen Personal"!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-success waves-effect" id="btn-upload-file"><i class="fa fa-save"></i> Upload File</button>
                    <button type="button" class="btn btn-primary" id='btn-back-pilihan'><i class="fa fa-reply"></i> Back</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function() {
        $('#btn-upload-file').click(function(event) {
            event.preventDefault();

            Swal.fire({
                title: 'Caution !',
                text: 'Dokumen akan di upload kedalam fitur "Pelasanaan -> Dokumen Personal"! Apakah anda ingin melanjutkan proses upload?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#btn-upload-file').removeAttr('disabled');
                    $('#btn-upload-file').closest('form').submit();
                }
                $('#uploadModal').modal('hide');
                $('#file').reset();
            });
        });

        $(document).bind("keyup.autocomplete", function() {
            var indikatorId; // Pindahkan deklarasi ke luar agar dapat diakses oleh fungsi lain

            $('.dokumen_bukti').autocomplete({
                minLength: 2,
                select: function(event, ui) {
                    $(this).next().val(ui.item.id);
                    $("#set_bukti_dokumen_id").val(ui.item.id);
                },
                focus: function(event, ui) {
                    $(this).next().val(ui.item.id);
                    $("#set_bukti_dokumen_id").val(ui.item.id);
                },
                source: function(request, response) {
                    indikatorId = $(this.element).data('indikator-id');

                    setTimeout(function() {
                        $.ajax({
                            url: "/dokumen/ajax-get-dokumen-asesmen",
                            dataType: "json",
                            data: {
                                term: request.term,
                            },
                            success: function(data) {
                                response(data);
                            }
                        });
                    }, 500);
                },
            });
        });

    });

    $(document).on("click", "#btn-pilihan", function(e) {
        let objek_id = $(this).data("objek");
        $("#upload-file-monev-satker-bukti-id").val(objek_id);
        var obj = {
            'objek_id': objek_id
        };

        $.ajax({
            url: "/monev-unit-kerja-objek/ajax-get-bukti",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)

                $('#table-dokumen tbody').empty();

                let dataDokumen = '';
                let nomor = 1;

                // Misalkan `hasil.data` adalah array
                if (hasil.data) {
                    hasil.data.forEach(function(data) {
                        dataDokumen += '<tr>' +
                            `<td class="text-center" width="5%">${nomor}.</td>` +
                            `<td>${data.nama_dokumen}</td>` +
                            `<td class="text-center" width="5%"><a href="/dokumen/download?id=${data.dokumen_id}" target="_blank"><i class="fa fa-file-archive-o"></i></a></td>` +
                            `<td class="text-center" width="5%"><a class="fa fa-trash" href="/monev-bukti/delete?id=${data.id}" data-confirm="Are you sure you want to delete this item?" data-method="post"></a></td>` +
                            '</tr>';
                        nomor++; // Increment nomor untuk setiap iterasi
                    });

                } else {
                    dataDokumen += '<tr>' +
                        `<td class="text-center" colspan="4" width="100%">Data tidak ditemukan</td>` +
                        '</tr>';
                }
                $('#table-dokumen tbody').append(dataDokumen);

                $("#monev_unit_kerja_objek_id").val(objek_id);
                $("#modal-pilihan").modal("show");
            }
        })
    });

    $(document).on("click", "#btn-pilih-sudah", function(e) {
        $("#modal-pilihan").modal("hide")
        $("#modal-dokumen-pilih").modal("show")
    });

    $(document).on("click", "#btn-pilih-belum", function(e) {
        $("#modal-pilihan").modal("hide")
        $("#modal-dokumen-upload").modal("show")
    });

    $(document).on("click", "#btn-back-pilihan", function(e) {
        $("#modal-dokumen-pilih").modal("hide")
        $("#modal-dokumen-upload").modal("hide")
        $("#modal-pilihan").modal("show")
    });

    $(document).on("click", "#btn-set-dokumen", function(e) {
        e.preventDefault();

        var obj = $("#form-set-dokumen").serialize()

        $.ajax({
            url: "/monev-bukti/ajax-set-bukti",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                        window.location.reload()
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-import", function(e) {
        e.preventDefault();

        var obj = $("#form-import-amkin").serialize()

        Swal.fire({
            title: 'Perhatian!',
            text: 'Apabila data yang diimport berbeda periode dengan data yang sudah ada maka data akan ditimpa. Apakah Anda ingin melanjutkan import data?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url: "/monev-unit-kerja-objek/ajax-add-objek-amkin",
                    type: "POST",
                    async: true,
                    data: obj,
                    error: function(e) {
                        console.log(e.responseText)
                    },
                    beforeSend: function() {
                        Swal.showLoading()
                    },
                    success: function(data) {
                        Swal.close()
                        var hasil = $.parseJSON(data)
                        if (hasil.code == 200) {
                            Swal.fire({
                                title: 'Yeay!',
                                icon: 'success',
                                text: hasil.message
                            }).then(res => {
                                $("#modal").modal("hide")
                                window.location.reload()
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: hasil.message
                            })
                        }
                    }
                });

            }
        });


    });
</script>
<?php JSRegister::end(); ?>