<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MonevUnitKerjaObjekSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monev-unit-kerja-objek-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'monev_unit_kerja_id') ?>

    <?= $form->field($model, 'jenis_objek') ?>

    <?= $form->field($model, 'import_id') ?>

    <?= $form->field($model, 'kode') ?>

    <?php // echo $form->field($model, 'nama_kode') ?>

    <?php // echo $form->field($model, 'nama_sub_kode') ?>

    <?php // echo $form->field($model, 'nama_program') ?>

    <?php // echo $form->field($model, 'sumber_dana') ?>

    <?php // echo $form->field($model, 'biaya') ?>

    <?php // echo $form->field($model, 'waktu') ?>

    <?php // echo $form->field($model, 'status_biaya') ?>

    <?php // echo $form->field($model, 'status_waktu') ?>

    <?php // echo $form->field($model, 'status_program') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
