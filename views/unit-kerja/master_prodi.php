<?php

use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnitkerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Hirarki Prodi';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $listFakultas = UnitKerja::find()->where(['jenis' => 'fakultas'])->orderBy('nama ASC')->all();
                            foreach ($listFakultas as $fakultas) {
                            ?>
                            <th><?= $fakultas->nama; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $listProdi = UnitKerja::find()->where(['jenis'=>'prodi'])->orderBy('nama ASC')->all();
                        foreach ($listProdi as $kriteria) {
                        ?>
                        <tr>
                            <th><?= $kriteria->nama; ?></th>

                            <?php
                            $x = 1;
                                foreach ($listFakultas as $fakultas) {
                                    // $m = PembagianKriteria::find()->where(['kriteria_id' => $kriteria->id, 'jenis_unit' => $x])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                            <td><input type="checkbox" <?= $checked; ?> class="mycheck"
                                    data-kriteria="<?= $kriteria->id; ?>" data-fakultas="<?= $fakultas; ?>" value="1">
                            </td>
                            <?php
                            $x++;
                                }
                                ?>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>