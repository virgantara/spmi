<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="initial-scale=1.0" />
  <meta name="format-detection" content="telephone=no" />
  <title></title>

<body class="bg-light">
  <div class="container">
    <!--     <img class="ax-center my-10 w-24" src="https://assets.bootstrapemail.com/logos/light/square.png" /> -->
    <div class="card p-6 p-lg-10 space-y-4">
      <h1 class="h3 fw-700">
        Aktivasi Akun Koordinator RTM
      </h1>
      <p>
        Hai, <b><?= $user->nama; ?></b><br>
        <br>
        Anda telah ditetapkan sebagai <b>Koordinator RTM</b> untuk Sistem Penjaminan Mutu Internal (SIMUDA) Universitas Darussalam Gontor.
        <br>
        <br>
        Berikut adalah informasi akun Anda:<br>
        Username: <?= $user->username ?><br>
        Password: <b><?= $password ?></b>
      </p>
      <?php
      $loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
      ?>
      <?= Html::a('Silakan klik di sini untuk masuk ke akun Anda.', $loginLink) ?><br>
      Catatan: Mohon jangan bagikan informasi akun Anda kepada siapapun.
    </div>

    <div class="text-muted text-center my-6">
      UPT PPTIK<br>
      Universitas Darussalam Gontor
    </div>
  </div>
</body>

</html>