<?php

use app\helpers\MyHelper;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */

$this->title = 'Create Unit Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Unit Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



                <?= $this->render('_form', [
                    'model' => $model,
                    'tingkatRtm' => MyHelper::getTingkatRtm()[0],
                ]) ?>
            </div>
        </div>
    </div>
</div>