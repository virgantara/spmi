<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Unit Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>

                <?= Html::button('<i class="fa fa-plus"></i> Create Account', ['class' => 'btn btn-success', 'id' => 'btn-send', 'data-unit' => $model->id]) ?>

                <form action="" id="form-unit">
                    <?= Html::hiddenInput('id', $model->id, ['id' => 'id_unit']) ?>
                </form>

            </div>

            <div class="panel-body ">

            <h4>NB : Please complete the data before sending the account to the email</h4>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'jenis',
                        'nama',
                        'singkatan',
                        'kode_prodi',
                        'email:email',
                        'penanggung_jawab',
                        [
                            'attribute' => 'is_sent',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $sent = 'belum dikirim';
                                $label = 'label label-danger';
                                if ($data->is_sent == 1) {
                                    $sent = 'sudah dikirim';
                                    $label = 'label label-primary';
                                }
                                return Html::tag('span', $sent, ['class' => $label]);
                            }
                        ]
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>


<?php

$this->registerJs('


$(document).on("click", "#btn-send", function(e){
    e.preventDefault();
    
    var obj = $("#form-unit").serialize()
    
    $.ajax({
        url: "/unit-kerja/ajax-send",
        type : "POST",
        async : true,
        data: obj,
        error : function(e){
            console.log(e.responseText)
        },
        beforeSend: function(){
            Swal.fire({
                title : "Please wait",
                html: "Sending Email..",
                
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
                
            })
        },
        success: function (data) {
            Swal.close()
            var hasil = $.parseJSON(data)
            if(hasil.code == 200){
                Swal.fire({
                    title: \'Yeay!\',
                    icon: \'success\',
                    text: hasil.message
                }).then(res=>{
                    window.location.reload()
                });
                
                $.pjax.reload({container: "#pjax-container"});
            }

            else{
                Swal.fire({
                    title: \'Oops!\',
                    icon: \'error\',
                    text: hasil.message
                })
            }
        }
    })
});

', \yii\web\View::POS_READY)

?>