<?php

use app\helpers\MyHelper;
use app\models\UnitKerja;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Singkatan</label>
        <?= $form->field($model, 'singkatan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Jenis Auditee</label>
        <input type="text" class="form-control" value="<?= MyHelper::getTingkatRtm()[0][$model->jenis] ?>" readonly>
    </div>

    <div class="form-group">
        <label class="control-label">Dibawah</label>
        <input type="text" class="form-control" value="<?= UnitKerja::findOne($model->parent_id)->nama ?? null ?>" readonly>
    </div>

    <div class="form-group">
        <label class="control-label">Email</label>
        <?= $form->field($model, 'email', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>


    <div class="form-group">
        <label class="control-label">Penanggung Jawab</label>
        <?= $form->field($model, 'penanggung_jawab', ['options' => ['tag' => false]])->textInput(['id' => 'penanggung_jawab', 'class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?= Html::a(Yii::t('app', 'Cancel'), ['user/profil'], ['class' => 'btn btn-default']) ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

$this->registerJs(' 



$(document).bind("keyup.autocomplete",function(){

    $(\'#penanggung_jawab\').autocomplete({
        minLength:3,
        select:function(event, ui){
            $(this).next().val(ui.item.id);
            $("#niy").val(ui.item.items.NIY)
            $("#nidn").val(ui.item.items.NIDN)

        },
        focus: function (event, ui) {
            $(this).next().val(ui.item.id);
            $("#niy").val(ui.item.items.NIY)
            $("#nidn").val(ui.item.items.NIDN)
        },
        source:function(request, response) {
            $.ajax({
                url: "/auditor/ajax-cari-dosen",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },

    }); 
});


', \yii\web\View::POS_READY);

?>