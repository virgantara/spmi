<?php

use app\helpers\MyHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Nama</label>
        <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'jenis')->widget(Select2::classname(), [
              'data' => $tingkatRtm,
              'options'=>['placeholder'=>Yii::t('app','- Pilih Jenis -')],
              'pluginOptions' => [
                  'allowClear' => true,
              ],
          ])->label('Jenis')?>
    </div>
    
    <div class="form-group">
        <label class="control-label">Singkatan</label>
        <?= $form->field($model, 'singkatan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Surat</label>
        <?= $form->field($model, 'surat', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Kode prodi</label>
        <?= $form->field($model, 'kode_prodi', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Email</label>
        <?= $form->field($model, 'email', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>


    <div class="form-group">
        <label class="control-label">Penanggung Jawab</label>
        <?= $form->field($model, 'penanggung_jawab', ['options' => ['tag' => false]])->textInput(['id'=>'penanggung_jawab', 'class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>


<?php

$this->registerJs(' 



$(document).bind("keyup.autocomplete",function(){

    $(\'#penanggung_jawab\').autocomplete({
        minLength:3,
        select:function(event, ui){
            $(this).next().val(ui.item.id);
            $("#niy").val(ui.item.items.NIY)
            $("#nidn").val(ui.item.items.NIDN)

        },
        focus: function (event, ui) {
            $(this).next().val(ui.item.id);
            $("#niy").val(ui.item.items.NIY)
            $("#nidn").val(ui.item.items.NIDN)
        },
        source:function(request, response) {
            $.ajax({
                url: "/auditor/ajax-cari-dosen",
                dataType: "json",
                data: {
                    term: request.term,
                    
                },
                success: function (data) {
                    response(data);
                }
            })
        },

    }); 
});


', \yii\web\View::POS_READY);

?>