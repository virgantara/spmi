<?php

use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnitkerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditee';
$this->params['breadcrumbs'][] = $this->title;

$list_jenis = [
    'prodi' => 'Program Studi',
    'fakultas' => 'Fakultas',
    'satker' => 'Satuan Kerja',
];

$listUnitKerja = ArrayHelper::map(UnitKerja::find()->all(), 'id', 'nama');
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <p>
                    <?= Html::a('<i class="fa fa-plus"></i> Tambah Unit Kerja', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'attribute' => 'id',
                        'width' => '5%',
                        'vAlign' => 'middle'
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'kode_unit',
                        'width' => '5%',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    [
                        'attribute' => 'nama',
                        'vAlign' => 'middle',
                        'width' => '18%',
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'singkatan',
                        'width' => '8%',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'parent_id',
                        // 'width' => '8%',
                        'filter' => $listUnitKerja,
                        // 'filterType' => GridView::FILTER_SELECT2,
                        // 'filterWidgetOptions' => [
                        //     'pluginOptions' => ['allowClear' => true],
                        // ],
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'data' => $listUnitKerja
                        ],
                        'value' => function ($model) use ($listUnitKerja) {
                            return $listUnitKerja[$model->parent_id] ?? '';
                        }
                    ],
                    // [
                    //     'attribute' => 'jenis',
                    //     'filter' => $list_jenis,
                    //     'value' => function ($data) {

                    //         $list_jenis = [
                    //             'prodi' => 'Program Studi',
                    //             'fakultas' => 'Fakultas',
                    //             'satker' => 'Satuan Kerja',
                    //         ];

                    //         return (!empty($list_jenis[$data->jenis]) ? $list_jenis[$data->jenis] : null);
                    //     }
                    // ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'email',
                        'width' => '18%',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'penanggung_jawab',
                        'width' => '18%',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    // [
                    //     'attribute' => 'is_sent',
                    //     'format' => 'raw',
                    //     'hAlign' => 'center',
                    //     'width' => '8%',
                    //     'value' => function ($data) {
                    //         $sent = 'belum dikirim';
                    //         $label = 'label label-danger';
                    //         if ($data->is_sent == 1) {
                    //             $sent = 'sudah dikirim';
                    //             $label = 'label label-primary';
                    //         }
                    //         return Html::tag('span', $sent, ['class' => $label]);
                    //     },
                    // ],
                    ['class' => 'yii\grid\ActionColumn'],
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>