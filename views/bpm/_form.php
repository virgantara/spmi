<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bpm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Kepala bpm</label>
            <?= $form->field($model, 'kepala_bpm',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Niy</label>
            <?= $form->field($model, 'niy',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Daftar hadir ami</label>
            <?= $form->field($model, 'daftar_hadir_ami',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
