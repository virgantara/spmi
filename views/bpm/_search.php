<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BpmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bpm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kepala_bpm') ?>

    <?= $form->field($model, 'surat_tugas') ?>

    <?= $form->field($model, 'berita_acara') ?>

    <?= $form->field($model, 'susunan_acara') ?>

    <?php // echo $form->field($model, 'daftar_hadir') ?>

    <?php // echo $form->field($model, 'hasil_audit_fakultas_prodi') ?>

    <?php // echo $form->field($model, 'hasil_audit_satuan_kerja') ?>

    <?php // echo $form->field($model, 'temuan') ?>

    <?php // echo $form->field($model, 'rekomendasi_mutu') ?>

    <?php // echo $form->field($model, 'niy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
