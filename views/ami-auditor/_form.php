<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\Auditor;
use app\models\StatusAuditor;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiAuditor */
/* @var $form yii\widgets\ActiveForm */

$listAuditor1 = Auditor::find()->orderBy(['nama' => SORT_ASC])->all();
$listAuditor = ArrayHelper::map($listAuditor1, 'id', 'nama');

$listStatus1 = StatusAuditor::find()->orderBy(['nama' => SORT_ASC])->all();
$listStatus = ArrayHelper::map($listStatus1, 'id', 'nama');
?>

<div class="body">


    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="panel-heading">
        <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['index'], ['class' => 'btn btn-info']) ?>
    </div>



    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Periode</label>
        <div class="col-sm-9">
            <?= Select2::widget([
                'name' => 'periode_id',
                'data' => ArrayHelper::map(Ami::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                // 'value' => $prodi_mk,
                'options' => ['placeholder' => Yii::t('app', '- Pilih Periode AMI -'), 'id' => 'periode_id'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Unit Kerja</label>
        <div class="col-sm-9">
            <?= DepDrop::widget([
                'type' => DepDrop::TYPE_SELECT2,
                'name' => 'unit_kerja_id',
                'options' => ['id' => 'unit_kerja_id'],
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'pluginOptions' => [
                    'depends' => ['periode_id'],
                    'initialize' => true,
                    'placeholder' => '- Pilih Unit Kerja -',
                    'url' => Url::to(['/ami/subamiunit'])
                ]
            ]); ?>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Nama Petugas</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'auditor_id', ['options' => ['tag' => false]])->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Auditor::find()->where(['status_aktif' => 1])->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                'options' => ['placeholder' => Yii::t('app', '- Pilih Auditor -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Peran</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'status_id', ['options' => ['tag' => false]])->widget(Select2::classname(), [
                'data' => ArrayHelper::map(StatusAuditor::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                'options' => ['placeholder' => Yii::t('app', '- Pilih Peran -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label(false) ?>
        </div>
    </div>


    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>