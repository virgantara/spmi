<?php

use app\models\Ami;
use app\models\AmiUnit;
use app\models\Auditor;
use app\models\StatusAuditor;
use app\models\UnitKerja;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmiAuditorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penugasan Auditor';
$this->params['breadcrumbs'][] = $this->title;


$listUnit1 = UnitKerja::find()->orderBy(['nama' => SORT_ASC]);
$listUnit1->joinWith(['amiUnits.ami as aa']);
$listUnit1->where(['aa.status_aktif' => 1]);
$listUnit1 = $listUnit1->all();

$listUnit = ArrayHelper::map($listUnit1, 'id', 'nama');


$amiId = !empty($_GET['ami_id']) ? $_GET['ami_id'] : '';
$amiUnitId = !empty($_GET['ami_unit_id']) ? $_GET['ami_unit_id'] : '';
// echo '<pre>';print_r($model);die;
// $prodi_mk = (!$model->isNewRecord ? $model->mk->prodi0->kode_prodi : null);


?>
<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="x_content">

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal"><i class="fa fa-upload"></i> Upload Data</button>
                <?= Html::a('<i class="fa fa-plus"></i> Buat Penugasan', ['create'], ['class' => 'btn btn-success']) ?>
                | Download Template File <?= Html::a(Yii::t('app', 'Disini'), ['template-download'], ['target' => '_blank']) ?>

                <br><br>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="questionOne">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                                <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                            </a>
                        </h5>
                    </div>
                    <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                        <div class="panel-body">

                            <?php $form = ActiveForm::begin([
                                'method' => 'GET',
                                'action' => ['ami-auditor/index',],
                                'options' => [
                                    'id' => 'form_validation',
                                ]
                            ]); ?>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label ">Periode AMI</label>
                                        <?= Select2::widget([
                                            'name' => 'ami_id[]',
                                            'data' => ArrayHelper::map(Ami::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'nama'),
                                            'value' => $_GET['ami_id'] ?? [],
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Periode'), 'id' => 'ami_id', 'multiple' => true], // Set the 'multiple' option here
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label ">Auditee</label>
                                        <?= Select2::widget([
                                            'name' => 'unit_id[]',
                                            'data' => ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                                            'value' => $_GET['unit_id'] ?? [],
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Auditee')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]);
                                        ?>



                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label ">Auditor</label>
                                        <?= Select2::widget([
                                            'name' => 'auditor_id[]',
                                            'data' => ArrayHelper::map(Auditor::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama'),
                                            'value' => $_GET['auditor_id'] ?? [],
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Auditor')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i>
                                    Cari</button>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>

                <div class="alert alert-info">
                    <b><?= Yii::t('app', 'Note:') ?></b>
                    <ul>
                        <li>Penugasan auditor dapat melalui 2 cara: input satuan dan excel</li>
                        <li>Input satuan melalui [Buat Penugasan]</li>
                        <li>Input excel melalui [Upload Data] sesuai dengan template yang disediakan</li>
                    </ul>
                </div>


                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'attribute' => 'ami_id',
                        'label' => 'Nama Periode AMI',
                        'value' => function ($data) {
                            return (!empty($data->ami) && !empty($data->ami->ami) ? $data->ami->ami->nama : '');
                        }
                    ],
                    [
                        'attribute' => 'ami_id',
                        'label' => 'Nama Unit',
                        'value' => function ($data) {
                            return (!empty($data->ami) && !empty($data->ami->unit) ? $data->ami->unit->nama : '');
                        }
                    ],
                    [
                        'attribute' => 'auditor_id',
                        'label' => 'Nama Auditor',
                        'value' => function ($data) {
                            return (!empty($data->auditor) ? $data->auditor->nama : '');
                        }
                    ],
                    [
                        'attribute' => 'status_id',
                        'label' => 'Peran',
                        'value' => function ($data) {
                            return (!empty($data->status) ? $data->status->nama : null);
                        }
                    ],
                    // [
                    //     'label' => 'Status Akun',
                    //     'format' => 'raw',
                    //     'value' => function ($data) {

                    //         $sent   = 'Belum terkirim';
                    //         $label  = 'label label-danger';
                    //         if (!empty($data->auditor)) {
                    //             if ($data->auditor->is_sent != null) {
                    //                 $sent   = 'sudah terkirim';
                    //                 $label  = 'label label-primary';
                    //             }
                    //         }

                    //         return Html::tag('span', $sent, ['class' => $label, 'style' => 'font-size:100%']);
                    //     }
                    // ],
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Import Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/ami-auditor/upload" method="post" id="form-upload" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="file" name="file" id="file" class="" required>
                            </div>

                        </div>
                    </div>

                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'keseluruhan data yang ada pada file akan di upload!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
$list_petugas = StatusAuditor::find()->orderBy(['nama' => SORT_ASC])->all();
$list_auditor = Auditor::find()->orderBy(['nama' => SORT_ASC])->all();
$list_ami = Ami::find()->orderBy(['nama' => SORT_ASC])->all();
// echo '<pre>';print_r($list_ami);die;

$unit_id = !empty($_GET['unit_id']) ? $_GET['unit_id'] : null;

?>
<form action="" id="form-krs">

    <div class="form-group">
        <label for="">Nama Auditor</label>
        <?= Select2::widget([
            'name' => 'nama_auditor',
            'data' => ArrayHelper::map($list_auditor, 'id', 'nama'),
            'options' => ['placeholder' => Yii::t('app', '- Cari Nama Auditor -')],
            'pluginOptions' => [
                // 'allowClear' => true,
            ],
        ]); ?>
    </div>

    <div class="form-group">
        <label for="">Periode AMI</label>
        <?= Select2::widget([
            'name' => 'ami_id',
            'data' => ArrayHelper::map($list_ami, 'id', 'nama'),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Periode -')],
            'pluginOptions' => [
                // 'allowClear' => true,
            ],
        ]); ?>
    </div>

    <?php // 
    ?>

    <div class="form-group">
        <label for="">Unit Kerja</label>
        <?= DepDrop::widget([
            'name' => 'unit_id',
            // 'value' => '',
            'type' => DepDrop::TYPE_SELECT2,
            'options' => ['id' => 'unit_id'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['ami_id'],
                'initialize' => true,
                'placeholder' => '- Pilih Unit -',
                'url' => Url::to(['/ami/subamiunit'])
            ]
        ]); ?>
    </div>

    <div class="form-group">
        <label for="">Peran</label>
        <?= Select2::widget([
            'name' => 'status_id',
            'data' => ArrayHelper::map($list_petugas, 'id', 'nama'),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Peran -')],
            'pluginOptions' => [
                // 'allowClear' => true,
            ],
        ]); ?>
    </div>
    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>


<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin(); ?>
<script>
    $(document).ready(function() {
        $('#form-upload').submit(function(e) {
            e.preventDefault();

            // Tampilkan pesan konfirmasi SweetAlert
            Swal.fire({
                title: 'Apakah data sudah sesuai?',
                text: 'Pastikan Anda telah memeriksa data dengan benar sebelum mengirimkan!',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya, Kirim!',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    // Jika dikonfirmasi, submit form secara manual
                    $(this).off("submit").submit();
                }
            });
        });
    });

    $(".delete-selected").click(function(e) {
        var keys = $('#my-grid').yiiGridView('getSelectedRows');
        e.preventDefault();

        Swal.fire({
            title: 'Penghapusan Auditor!',
            text: "Data Auditor yang dihapus tidak bisa dikembalikan. Anda yakin ingin menghapus data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                var obj = new Object;
                obj.keys = keys;
                $.ajax({

                    type: "POST",
                    url: "/auditor/delete-multiple",
                    data: {
                        dataPost: obj
                    },

                    beforeSend: function() {
                        Swal.fire({
                            title: "Please wait",
                            html: "Processing your request...",
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            },

                        })
                    },
                    error: function(e) {
                        Swal.close()
                    },
                    success: function(data) {
                        Swal.close()
                        var data = $.parseJSON(data)


                        if (data.code == 200) {
                            Swal.fire({
                                title: 'Yeay!',
                                icon: 'success',
                                text: data.message
                            });

                            $.pjax.reload({
                                container: '#pjax-container',
                                async: true
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: data.message
                            });

                        }
                    }
                })
            }
        });
    });

    $(document).bind("keyup.autocomplete", function() {

        $('#nama_auditor').autocomplete({
            minLength: 3,
            select: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)

            },
            focus: function(event, ui) {
                $(this).next().val(ui.item.id);
                $("#kode_unik").val(ui.item.items.kode_unik)
                $("#email").val(ui.item.items.email)
                $("#niy").val(ui.item.items.NIY)
                $("#pangkat").val(ui.item.items.pangkat + " " + ui.item.items.golongan)
                $("#prodi").val(ui.item.items.nama_prodi)
                $("#nidn").val(ui.item.items.NIDN)
                $("#jabfung").val(ui.item.items.jabfung)
            },
            source: function(request, response) {
                $.ajax({
                    url: "/auditor/ajax-cari-dosen",
                    dataType: "json",
                    data: {
                        term: request.term,

                    },
                    success: function(data) {
                        response(data);
                    }
                })
            },

        });
    });

    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-krs").serialize()


        $.ajax({
            url: "/ami-auditor/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Sending Email..",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                    $("#nama_auditor").val("").focus()
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        $("#modal").modal("show")
    });
</script>
<?php JSRegister::end(); ?>