<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmiUnitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Auditor By Unit Kerja';
$this->params['breadcrumbs'][] = $this->title;

$listPeriode = ArrayHelper::map(Ami::find()->all(), 'id', 'nama');
$listUnit = ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');
$list_ami = MyHelper::getStatusAmi();
$list_jenis_unit = MyHelper::getJenisAmi();

?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'attribute' => 'ami_id',
                        'label' => 'Nama Periode AMI',
                        'filter' => $listPeriode,
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Jenis', 'id' => 'grid-bahan-search-periode_id'],
                        'value' => function ($data) {
                            return (!empty($data) ? $data->ami->nama : null);
                        }
                    ],
                    [
                        'attribute' => 'unit_id',
                        'label' => 'Nama Unit',
                        'filter' => $listUnit,
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Jenis', 'id' => 'grid-bahan-search-unit_id'],
                        'value' => function ($data) {
                            return (!empty($data) ? $data->unit->nama : null);
                        }
                    ],
                    // [
                    //     'attribute' => 'jenis_unit',
                    //     'filter' => $list_jenis_unit,
                    //     'value' => function ($data) {

                    //         $list_jenis_unit = MyHelper::getJenisAmi();
                    //         return (!empty($list_jenis_unit[$data->jenis_unit]) ? $list_jenis_unit[$data->jenis_unit] : null);
                    //     }
                    // ],
                    // [
                    //     'attribute' => 'status_ami',
                    //     'filter' => $list_ami,
                    //     'value' => function ($data) {
                    //         // return (!empty($data) ? $data->unit->nama : null);
                    //         $list_ami = MyHelper::getStatusAmi();
                    //         return (!empty($list_ami[$data->status_ami]) ? $list_ami[$data->status_ami] : null);
                    //     }
                    // ],
                    [
                        'label' => 'Auditor 1',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $amiAuditor = AmiAuditor::find()->where([
                                'ami_id' => $data->id,
                                'status_id' => 1,
                            ])->one();

                            $sent = 'belum terisi';
                            $label = 'label label-danger';
                            if ($amiAuditor) {
                                $sent = 'sudah terisi';
                                $label = 'label label-primary';
                            }

                            return Html::tag('span', $sent, ['class' => $label]);
                        }
                    ],
                    [
                        'label' => 'Auditor 2',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $amiAuditor = AmiAuditor::find()->where([
                                'ami_id' => $data->id,
                                'status_id' => 2,
                            ])->one();

                            $sent = 'belum terisi';
                            $label = 'label label-danger';
                            if ($amiAuditor) {
                                $sent = 'sudah terisi';
                                $label = 'label label-primary';
                            }

                            return Html::tag('span', $sent, ['class' => $label]);
                        }
                    ],
                    [
                        'label' => 'Auditor 3',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $amiAuditor = AmiAuditor::find()->where([
                                'ami_id' => $data->id,
                                'status_id' => 3,
                            ])->one();

                            $sent = 'belum terisi';
                            $label = 'label label-danger';
                            if ($amiAuditor) {
                                $sent = 'sudah terisi';
                                $label = 'label label-primary';
                            }

                            return Html::tag('span', $sent, ['class' => $label]);
                        }
                    ],
                    // 'status_ami',
                    // 'jenis_ami',

                    // 'unit_id',
                    // 'ami_id',
                    // ['class' => 'yii\grid\ActionColumn'],
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>