<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiAuditor */

$this->title = $model->auditor->nama;
$this->params['breadcrumbs'][] = ['label' => 'Ami Auditors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?php Html::button('<i class="fa fa-send"></i> Create & Send Account', ['class' => 'btn btn-primary', 'id' => 'btn-send', 'data-unit' => $model->id]) ?>

                <form action="" id="form-unit">
                    <?= Html::hiddenInput('id', $model->auditor_id, ['id' => 'ami_auditor_id']) ?>
                </form>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        // 'ami_id',
                        [
                            'label' => 'Nama Audit Mutu Internal',
                            'value' => function ($data) {
                                return (isset($data->ami->ami->nama) ? $data->ami->ami->nama : '');
                            }
                        ],
                        [
                            'label' => 'Nama Unit Kerja',
                            'value' => function ($data) {
                                return (isset($data->ami->unit->nama) ? $data->ami->unit->nama : '');
                            }
                        ],
                        [
                            'label' => 'Nama Auditor',
                            'value' => function ($data) {
                                return (isset($data->auditor->nama) ? $data->auditor->nama : '');
                            }
                        ],
                        [
                            'label' => 'Peran Auditor',
                            'value' => function ($data) {
                                return (isset($data->status->nama) ? $data->status->nama : '');
                            }
                        ],
                        [
                            'label' => 'Status Akun',
                            'format' => 'raw',
                            'value' => function ($data) {

                                $sent = 'belum terkirim';
                                $label = 'label label-danger';
                                if ($data->auditor->is_sent != null) {
                                    $sent = 'sudah terkirim';
                                    $label = 'label label-primary';
                                }

                                return Html::tag('span', $sent, ['class' => $label]);
                            }
                        ],
                        // 'auditor_id',
                        // 'status_id',
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin(); ?>
<script>
    $(document).on("click", "#btn-send", function(e) {
        e.preventDefault();

        var obj = $("#form-unit").serialize()

        $.ajax({
            url: "/auditor/ajax-send",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Sending Email..",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        window.location.reload()
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });
</script>
<?php JSRegister::end(); ?>