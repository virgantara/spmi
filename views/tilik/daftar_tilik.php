<?php

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Indikator;
use app\models\Kriteria;
use app\models\UnitKerja;
use Google\Service\PeopleService\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Audit Mutu Internal';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Tilik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// $list_ami = MyHelper::getStatusAmi();

$ami_asesmen = AmiUnit::findOne($id);
// echo '<pre>';print_r($ami_asesmen);exit;
$ami_nama = Ami::findOne($ami_asesmen->ami_id);
$unit_nama = UnitKerja::findOne($ami_asesmen->unit_id);
// echo '<pre>';print_r($ami_asesmen);exit;

$asesmen_nama = "$ami_nama->nama - $unit_nama->nama";
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3><?= $asesmen_nama ?></h3>

                <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['asesmen/kecukupan', 'id' => $id], ['class' => 'btn btn-info']) ?>

            </div>

            <div class="panel-body ">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <tr>
                            <th style="text-align: center" class="alert alert-info" colspan="10"><strong>ASESMEN</strong></th>
                        </tr>
                        <tr>

                            <!-- <th style="text-align: center" rowspan="1"><strong>No</strong></th> -->
                            <th style="text-align: center" rowspan="1"><strong>No</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Pertanyaan</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Nomor</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Status</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Catatan</strong></th>
                            <!-- <th style="text-align: center" rowspan="1"><strong>Skor ED</strong></th>
                            <th style="text-align: center" rowspan="1"><strong>Input Skor</strong></th> -->
                            <th style="text-align: center" rowspan="1"><strong>Aksi</strong></th>
                            <!-- <th style="text-align: center" rowspan="1"><strong>Aksi</strong></th> -->

                        </tr>

                        <?php
                        // echo '<pre>';print_r($asesmen);exit;
                        $i = 1;
                        foreach ($tilik as $t) :

                            $asesmen = Asesmen::findOne($t->asesmen_id);
                            $indikator = Indikator::findOne($asesmen->indikator_id);
                            // $kriteria = Kriteria::findOne($indikator->kriteria_id);
                            // echo '<pre>';print_r($kriteria->nomor);exit;
                            // echo '<pre>';print_r($a);exit;
                            // if (!$a->link_bukti) {
                            //     $status_link = 'btn btn-sm btn-danger disabled';
                            //     $class_link = 'btn btn-sm btn-danger disabled';
                            //     $class = '<i class="fa fa-close"></i>';
                            // } else {
                            //     $status_link = 'btn btn-sm btn-info';
                            //     $class_link = 'btn btn-sm btn-info';
                            //     $class = '<i class="fa fa-link"></i>';
                            // }
                        ?>

                            <tr>
                                <td style="text-align: center"><?= $i ?></td>
                                <td style="text-align: center"><?= (!empty($t->pertanyaan) ? $t->pertanyaan : null) ?></td>
                                <td style="text-align: center"><?= (!empty($indikator->nomor) ? $indikator->nomor : null) ?></td>
                                <td><?= (!empty($t->status) ? $t->status : null) ?></td>
                                <td><?= (!empty($t->catatan) ? $t->catatan : null) ?></td>
                                <td style="text-align: center">
                                    <?= Html::a('', ['tilik/view', 'id' => $t->id], ['class' => 'glyphicon glyphicon-eye-open']) ?>
                                    <?= Html::a('', ['tilik/update', 'id' => $t->id], ['class' => 'glyphicon glyphicon-pencil']) ?>
                                    <?= Html::a('', ['tilik/deletee', 'id' => $t->id,], ['class' => 'glyphicon glyphicon-trash']) ?>
                                </td>

                            </tr>

                        <?php
                            $i++;
                        endforeach;
                        ?>

                    </table>
                </div>

            </div>
        </div>

    </div>
</div>