<?php

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\Asesmen;
use yii\helpers\Html;
use app\models\Tilik;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemuanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Tilik';
$this->params['breadcrumbs'][] = ['label' => 'Audit Mutu Internal', 'url' => ['asesmen/ami']];
$this->params['breadcrumbs'][] = $this->title;

$listAsesmen = ArrayHelper::map(Asesmen::find()->where(['ami_unit_id' => $amiUnit->id])->all(), 'id', function ($data) {
    return $data->indikator->nama;
});

?>

<div class="panel col-lg-12">
    <div class="panel-heading">
        <h3 class="page-title">[<?= $amiUnit->ami->nama ?>] <br> <?= $amiUnit->unit->nama ?> - <?= $this->title; ?></h3>
        <?= $this->render('../nav-master/nav_asesmen', ['id' => $id]) ?>
    </div>

    <div class="panel-body">
        <div class="x_content">

            <h3><?= $amiUnit->unit->nama ?></h3>
            <h4><?= $amiUnit->ami->nama ?></h4>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i> Cetak', ['cetak', 'ami_unit_id' => $id], ['class' => 'btn btn-danger btn-sm', 'target' => '_blank']) ?>
            <br>
            <br>

            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'contentOptions' => ['class' => 'kartik-sheet-style'],
                    'width' => '36px',
                    'pageSummary' => 'Total',
                    'pageSummaryOptions' => ['colspan' => 6],
                    'header' => '',
                    'headerOptions' => ['class' => 'kartik-sheet-style']
                ],
                // 'id',
                // 'asesmen_id',
                [
                    'attribute' => 'asesmen_id',
                    'filter' => $listAsesmen,
                    'filterType' => GridView::FILTER_SELECT2,
                    'label' => 'Nama Indikator',
                    'width' => '30%',
                    'value' => function ($data) {
                        return $data->asesmen->indikator->nama;
                    },
                    'filterWidgetOptions' => [
                        'options' => ['placeholder' => 'Pilih Indikator', 'allowClear' => true],
                        'pluginOptions' => ['allowClear' => true]
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'pertanyaan',
                    'label' => 'Pertanyaan',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->pertanyaan == null ? '' : $data->pertanyaan);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'referensi',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->referensi == null ? '' : $data->referensi);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'bukti',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    ],
                    'value' => function ($data) {
                        return ($data->bukti == null ? '' : $data->bukti);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'auditor_id',
                    'filter' => ArrayHelper::map($auditorData, 'id', 'nama'),
                    'refreshGrid' => true,
                    'label' => 'Auditor',
                    'readonly' => !Yii::$app->user->can('auditor'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => ArrayHelper::map($auditorData, 'id', 'nama')
                    ],
                    'value' => function ($data) {
                        return ($data->auditor_id == null ? '' : $data->auditor->nama);
                    }
                ],
                //'created_at',
                //'updated_at',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'visible' => Yii::$app->user->can('auditor'), // Kolom hanya akan terlihat jika user memiliki izin 'auditor'
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                ['delete', 'id' => $model->id],
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ]
                            );
                        },
                    ],
                    'template' => '{delete}', // Hanya menampilkan tombol delete
                ],

            ]; ?>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="skripsi_id">
                <input type="hidden" id="dosen_id">
                <?= Html::textInput('nama_pejabat', '', ['class' => 'form-control', 'id' => 'nama_pejabat', 'placeholder' => 'Ketik Nama Dosen']) ?>
                <?php
                AutoComplete::widget([
                    'name' => 'nama_pejabat',
                    'id' => 'nama_pejabat',
                    'clientOptions' => [
                        'source' => Url::to(['auditor/ajax-cari-dosen']),
                        'autoFill' => true,
                        'minLength' => '1',
                        'select' => new JsExpression("function( event, ui ) {
                            $('#dosen_id').val(ui.item.id);
                        }")
                    ],
                    'options' => [
                        // 'size' => '40'
                    ]
                ]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<?php

$this->registerJs(' 

$(document).on("click",".btn-edit-dosen",function(e){
    e.preventDefault()
  
    $("#exampleModal").modal("show");
  
    $("#skripsi_id").val($(this).data("item"));
})



', \yii\web\View::POS_READY);

?>