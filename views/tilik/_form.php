<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tilik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Instrumen Audit</label>
        <?= $form->field($model, 'asesmen_id', ['options' => ['tag' => false]])->textInput(['value' => $label, 'disabled' => true])->label(false) ?>
        <?= $form->field($model, 'asesmen_id', ['options' => ['tag' => false]])->hiddenInput(['value' => $id])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Pertanyaan</label>
        <?= $form->field($model, 'pertanyaan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status Pertanyaan</label>
        <?= $form->field($model, 'status', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Catatan</label>
        <?= $form->field($model, 'catatan', ['options' => ['tag' => false]])->textarea(['rows' => 2])->label(false) ?>
    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>


</div>