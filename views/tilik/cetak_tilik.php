<?php

use app\helpers\MyHelper;
use app\models\Asesmen;
use app\models\Indikator;

date_default_timezone_set("Asia/Jakarta");
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
?>


<table width="100%">
    <tr>
        <td>
            <table border="1" width="100%" style="padding: 4px;">

                <tr>
                    <td width="15%" rowspan="4"></td>

                    <td width="40%" style="text-align: center;" rowspan="4">
                        <br><br>
                        <span style="font-size: 1em;">UNIVERSITAS DARUSSALAM GONTOR</span><br>
                        <span style="font-size: 0.9em"><br><strong>BADAN PENJAMINAN MUTU</strong></span>
                        <br>
                        <br>
                    </td>
                    <td width="20%">Kode Formulir</td>
                    <td width="25%" style="font-size: 0.9em"><?= (isset($amiUnit->ami->form_tilik_kode) ? $amiUnit->ami->form_tilik_kode : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Pembuatan</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_tilik_tanggal_pembuatan) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_tilik_tanggal_pembuatan) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Revisi</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_tilik_tanggal_revisi) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_tilik_tanggal_revisi) : '-') ?></td>
                </tr>
                <tr>
                    <td width="20%">Tanggal Efektif</td>
                    <td width="25%"><?= (isset($amiUnit->ami->form_tilik_tanggal_efektif) ? MyHelper::convertTanggalIndo($amiUnit->ami->form_tilik_tanggal_efektif) : '-') ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr style="text-align: center;">
                    <td>PERTANYAAN DAN CHECKLIST AUDIT</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>
        <td>
            <table border="1" style="padding: 5px;">
                <tr>
                    <td width="25%">Tanggal Audit</td>
                    <td width="75%" colspan="2"><?= MyHelper::convertTanggalIndo($amiUnit->tanggal_ami); ?></td>
                </tr>
                <tr>
                    <td>Tempat Audit</td>
                    <td colspan="2"><?= (isset($amiUnit->lokasi) ? $amiUnit->lokasi : '-') ?></td>
                </tr>
                <tr>
                    <td>Auditee</td>
                    <td colspan="2"><?= $amiUnit->unit->nama ?></td>
                </tr>
                <tr>
                    <td>Perwakilan Auditee</td>
                    <td colspan="2"><?= MyHelper::getAuditee($amiUnit->unit_id) ?></td>
                </tr>
                <tr>
                    <td>Ketua Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 1)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 1)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Anggota Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 2)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 2)['nomor_registrasi'] ?? '' ?></td>
                </tr>
                <tr>
                    <td>Sekertatis Auditor</td>
                    <td><?= MyHelper::getAuditor($amiUnit->id, 3)['nama'] ?? '' ?></td>
                    <td>NRA: <?= MyHelper::getAuditor($amiUnit->id, 3)['nomor_registrasi'] ?? '' ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr>

        <td>
            <table border="1" style="padding: 5px; font-size: 0.8em;">
                <tr>
                    <td style="text-align: center;" width="5%">No</td>
                    <td style="text-align: center;" width="30%">Indikator/Dokumen</td>
                    <td style="text-align: center;" width="20%">Referensi</td>
                    <td style="text-align: center;" width="30%">Pertanyaan/Catatan</td>
                    <td style="text-align: center;" style="text-align: center;" d width="15%">Bukti</td>
                </tr>
                <?php
                if (!empty($tilik)) {
                    $no = 1;
                    foreach ($tilik as $t) : ?>
                        <tr>
                            <td style="text-align: center;"><?= $no ?></td>
                            <td><?= $t->asesmen->indikator->nama ?></td>
                            <td><?= $t->referensi ?></td>
                            <td><?= $t->pertanyaan ?></td>
                            <td><?= $t->bukti ?></td>
                        </tr>
                    <?php
                        $no++;
                    endforeach;
                } else {
                    // Jika $tilik kosong, tampilkan satu baris dengan pesan '-'
                    ?>
                    <tr>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">-</td>
                    </tr>
                <?php } ?>
            </table>
        </td>
    </tr>
</table>

<p>Laporan ini diproses secara otomaatis oleh komputer. Tidak memerlukan tanda tangan.</p>
<p><i>This statement is automatically generated by system. It does not require any signature</i></p>