<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $model app\models\Prodi */

$this->title = $model->nama_prodi;
$this->params['breadcrumbs'][] = ['label' => 'Prodi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$listAkreditasi = \app\helpers\MyHelper::listAkreditasi();
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <?php
            if (Yii::$app->user->can('admin')) {
            ?>
                <div class="panel-heading">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            <?php } ?>
            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'kode_prodi',
                        'nama_prodi',
                        'kode_jenjang',
                        // [
                        //     'attribute' => 'akreditasi',
                        //     'value' => function($data) use ($listAkreditasi){
                        //         return $listAkreditasi[$data->akreditasi];
                        //     }
                        // ],
                        // 'tanggal_sk',
                        // 'nomor_sk',
                        // 'tanggal_kadaluarsa',
                        'updated_at',
                        'created_at',
                    ],
                ]) ?>
                <?php
                if (Yii::$app->user->can('admin')) {
                ?>
                    <p>
                        <?= Html::a('Create Akreditasi', ['create'], ['class' => 'btn btn-success', 'id' => 'btn-add']) ?>
                    </p>

                <?php
                }
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'urutan',
                        'contentOptions'    => [
                            'width' => '5%',
                        ],
                        // 'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    'namaLembagaAkreditasi',
                    [
                        'attribute' => 'status_akreditasi',
                        'value' => function ($data) use ($listAkreditasi) {
                            return $listAkreditasi[$data->status_akreditasi];
                        }
                    ],
                    'tanggal_sk',
                    'tanggal_kadaluarsa',
                    'nomor_sk',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{download-sk} {upload-sk} {delete}',
                        'visibleButtons' => [
                            'upload-sk' => function ($model, $key, $index) {
                                return !Yii::$app->user->isGuest;
                            },
                            'delete' => function ($model, $key, $index) {
                                return !Yii::$app->user->isGuest;
                            },
                            'download-sk' => function ($model, $key, $index) {
                                return !empty($model->file_path);
                            }
                        ],
                        'buttons' => [
                            'download-sk' => function ($url, $model) {
                                return Html::a('<span class="fa fa-download"></span> Download SK', ['akreditasi/download-sk', 'id' => $model->id], [
                                    'aria-label' => 'Download SK',
                                    'data-pjax' => '0',
                                    'target' => '_blank',
                                    'title' => 'Download SK',
                                    'class' => 'btn btn-success',
                                ]);
                            },
                            'upload-sk' => function ($url, $model) {
                                return Html::a('<span class="fa fa-upload"></span> Upload SK', ['akreditasi/upload-sk', 'id' => $model->id], [
                                    'aria-label' => 'Upload SK',
                                    'data-pjax' => '0',
                                    'title' => 'Upload SK',
                                    'class' => 'btn btn-info',
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-trash"></i> Remove', ['akreditasi/delete', 'id' => $model->id], [
                                    'title' => Yii::t('app', 'Remove Akreditasi'),
                                    'class' => 'btn btn-danger',
                                    'data-pjax' => 0,
                                    'data' => [
                                        'confirm' => 'Are you sure you want to remove this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                            },

                        ]
                    ]
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>
            </div>
        </div>

    </div>
</div>


<?php


yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-akreditasi">
    <div class="form-group">
        <label for="">Urutan</label>
        <?= Html::textInput('urutan', $getLastAkreditasi, ['class' => 'form-control', 'id' => 'urutan', 'readonly' => true]) ?>
    </div>
    <div class="form-group">
        <label for="">Lembaga Akreditasi</label>
        <?= Html::dropDownList('lembaga_akreditasi_id', '', ArrayHelper::map($listLembagaAkreditasi, 'id', 'singkatan_lembaga'), ['class' => 'form-control', 'id' => 'lembaga_akreditasi_id', 'prompt' => '- Pilih Lembaga Akreditasi -']) ?>
        <?= Html::hiddenInput('prodi_id', $model->id, ['id' => 'prodi_id']) ?>
    </div>
    <div class="form-group">
        <label for="">Tanggal SK Akreditasi</label>
        <?= DatePicker::widget([
            'name' => 'tanggal_sk',
            'options' => ['placeholder' => 'Pilih Tanggal SK ...'],
            'convertFormat' => true,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'php:Y-m-d',
                'autoclose' => true,
            ]
        ]) ?>

    </div>
    <div class="form-group">
        <label for="">Tanggal Kadaluarsa</label>
        <?= DatePicker::widget([
            'name' => 'tanggal_kadaluarsa',
            'options' => ['placeholder' => 'Pilih Tanggal Kadaluarsa ...'],
            'convertFormat' => true,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'php:Y-m-d',
                'autoclose' => true,
            ]
        ]) ?>

    </div>
    <div class="form-group">
        <label for="">Nomor SK Akreditasi</label>
        <?= Html::textInput('nomor_sk', '', ['class' => 'form-control', 'id' => 'nomor_sk']) ?>
    </div>
    <div class="form-group">
        <label for="">Status Akreitasi</label>
        <?= Html::dropDownList('status_akreditasi', '', $listAkreditasi, ['class' => 'form-control', 'id' => 'status_akreditasi', 'prompt' => '- Pilih Status Akreditasi -']) ?>

    </div>
    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-akreditasi").serialize()

        $.ajax({
            url: "/akreditasi/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title:  'Yeay!',
                        icon:  'success',
                        timer: 700,
                        text: hasil.message
                    }).then((res) => {
                        $("#modal").modal("hide");
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                } else {
                    Swal.fire({
                        title:  'Oops!',
                        icon:  'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        $("#modal").modal("show")

    });
</script>
<?php JSRegister::end() ?>
