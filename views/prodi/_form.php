<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Prodi */
/* @var $form yii\widgets\ActiveForm */

$list_akreditasi = MyHelper::listAkreditasi();
?>

<div class="col-md-6">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>
        <?= $form->field($model, 'kode_prodi')->textInput(['class'=>'form-control','readonly' => 'readonly']) ?>
        <?= $form->field($model, 'nama_prodi')->textInput(['class'=>'form-control','readonly' => 'readonly']) ?>
        <?= $form->field($model, 'kode_jenjang')->textInput(['class'=>'form-control','readonly' => 'readonly']) ?>
        
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>


</div>