<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Program Studi';
$this->params['breadcrumbs'][] = $this->title;

$list_akreditasi = MyHelper::listAkreditasi();
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">
                <?php 
                if(!Yii::$app->user->isGuest){
                 ?>
                
                <p>
                    <?= Html::button('<i class="fa fa-download"></i> Import from SIMPEG/SIAKAD', ['class' => 'btn btn-success', 'id' => 'btn-import']) ?>
                </p>

                <?php
            }
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    'nama_prodi',
                    'kode_jenjang',
                    [
                        'header' => 'Akreditasi',
                        'format' => 'raw',
                        'value' => function($data){
                            $html = '<b>Akreditasi Nasional</b>:<br>';
                            foreach($data->listAkreditasi['list_akreditasi_nasional'] as $item){
                                $html .= 'Terakreditasi: '.$item['status_akreditasi'].' - '.$item['lembaga'].' - Nomor: '.$item['nomor_sk'];
                                
                                if(!empty($item['link_download']))
                                    $html .= '&nbsp;<a data-pjax="0" title="Unduh Sertifikat Akreditasi" target="_blank" href="'.$item['link_download'].'"><i class="fa fa-download"></i></a>';
                                
                                $html .= '<br>';
                            }

                            $html .= '<br><b>Akreditasi Internasional</b>:<br>';
                            foreach($data->listAkreditasi['list_akreditasi_internasional'] as $item){
                                $html .= 'Terakreditasi: '.$item['status_akreditasi'].' - '.$item['lembaga'].' - Nomor: '.$item['nomor_sk'];
                                
                                if(!empty($item['link_download']))
                                    $html .= '&nbsp;<a data-pjax="0" title="Unduh Sertifikat Akreditasi" target="_blank" href="'.$item['link_download'].'"><i class="fa fa-download"></i></a>';
                                
                                $html .= '<br>';
                            }

                            return $html;
                        }
                    ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'refreshGrid' => true,
                    //     'attribute' => 'akreditasi',
                    //     'filter' => $list_akreditasi,
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    //         'data' => $list_akreditasi,
                    //         'asPopover' => false,
                    //     ],
                    //     'value' => function ($data) use ($list_akreditasi) {
                    //         return (!empty($list_akreditasi[$data->akreditasi]) ? $list_akreditasi[$data->akreditasi] : null);
                    //     }
                    // ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'refreshGrid' => true,
                    //     'attribute' => 'lembaga_akreditasi_id',
                    //     'filter' => ArrayHelper::map($list_lembaga_akreditasi,'id','singkatan_lembaga'),
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    //         'data' => ArrayHelper::map($list_lembaga_akreditasi,'id','singkatan_lembaga'),
                    //         'asPopover' => false,
                    //     ],
                    //     'value' => function ($data) {
                    //         return (!empty($data->lembagaAkreditasi) ? $data->lembagaAkreditasi->singkatan_lembaga : null);
                    //     }
                    // ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'attribute' => 'tanggal_sk',
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => \kartik\editable\Editable::INPUT_DATE,
                    //         'options' => [
                    //             'convertFormat' => true, // autoconvert PHP format to JS format
                    //             'pluginOptions' => [
                    //                 'format' => 'php:Y-m-d',
                    //                 'todayHighlight' => true
                    //             ] // php date format
                    //         ]

                    //     ],

                    // ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'attribute' => 'nomor_sk',
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    //         'asPopover' => false,
                    //     ],
                    // ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'attribute' => 'tanggal_kadaluarsa',
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => \kartik\editable\Editable::INPUT_DATE,
                    //         'options' => [
                    //             'convertFormat' => true, // autoconvert PHP format to JS format
                    //             'pluginOptions' => [
                    //                 'format' => 'php:Y-m-d',
                    //                 'todayHighlight' => true
                    //             ] // php date format
                    //         ]

                    //     ],

                    // ],
                    //'updated_at',
                    //'created_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons'=>[
                            'view'=>function ($url, $model) {
                                return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'class' => 'btn btn-success',
                                    'data-pjax' => 0
                                ]);
                            },
                            'update'=>function ($url, $model) {
                                return Html::a('<span class="fa fa-edit"></span> Update', $url, [
                                    'class' => 'btn btn-primary',
                                    'data-pjax' => 0
                                ]);
                            },
                         
                            'delete' => function ($url, $model) {
                                return Html::a('<i class="fa fa-trash"></i> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete Prodi'),
                                    'class' => 'btn btn-danger',
                                    'data-pjax' => 0,
                                    'data' => [
                                        'confirm' => 'Are you sure you want to remove this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        
                        ],
                        'visibleButtons' => [
                            'update' => function ($model, $key, $index) {
                                return !Yii::$app->user->isGuest;
                            },
                            'delete' => function ($model, $key, $index) {
                                return !Yii::$app->user->isGuest;
                            }
                        ],
                    ]
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>


<?php

$this->registerJs(' 


$(document).on("click", "#btn-import", function(e){
    e.preventDefault();
    
    $.ajax({
        url: "/prodi/ajax-import",
        type : "POST",
        async : true,
        error : function(e){
            console.log(e.responseText)
        },
        beforeSend: function(){
            Swal.showLoading()
        },
        success: function (data) {
            Swal.close()
            var hasil = $.parseJSON(data)
            if(hasil.code == 200){
                Swal.fire({
                    title: \'Yeay!\',
                    icon: \'success\',
                    text: hasil.message
                });
                
                $.pjax.reload({container: "#pjax-container"});
                
            }

            else{
                Swal.fire({
                    title: \'Oops!\',
                    icon: \'error\',
                    text: hasil.message
                })
            }
        }
    })
});



', \yii\web\View::POS_READY);

?>