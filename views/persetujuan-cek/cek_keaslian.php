<?php

use app\helpers\MyHelper;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersetujuanCekSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cek Keaslian Dokumen');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    body {
        font-family: Arial, sans-serif;
    }

    table {
        width: 100%;
        border-collapse: separate;
        border-spacing: 10px;
        color: black;
        /* Mengatur jarak antar sel */
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ddd;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    /* Menambahkan kelas untuk baris warna merah */
    .red-row td {
        background-color: rgba(255, 153, 153, 0.5);
        /* Merah dengan tingkat transparansi */
    }

    /* Menambahkan kelas untuk baris warna hijau dengan transparansi */
    .green-row td {
        background-color: rgba(153, 255, 153, 0.5);
        /* Hijau dengan tingkat transparansi */
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">


                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['persetujuan-cek/cek-keaslian'],
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>
                <div class="row">


                    <div class="col-md-offset-3 col-md-6">
                        <h3 class="page-title"><?= $this->title; ?></h3>
                        <div class="form-group">
                            <label class="control-label ">Kode Dokumen</label>
                            <?= Html::textInput('kode_dokumen', $_GET['kode_dokumen'] ?? '', ['id' => 'kode_dokumen', 'class' => 'form-control', 'placeholder' => 'Ketik kode dokumen']) ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Verification Code</label>
                            <?php echo $form->field($model, 'captcha')->widget(Captcha::className(), [
                                'options' => ['placeholder' => 'Please type above captcha',],
                                'imageOptions' => [
                                    'id' => 'my-captcha-image'
                                ]
                            ])->label(FALSE); ?>
                        </div>
                        <div class="form-group clearfix">
                            <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>


                        </div>

                    </div>

                </div>
                <?php ActiveForm::end(); ?>



                <div class="row">

                    <?php  ?>



                    <div class="col-md-offset-3 col-md-6">

                        <?php if ($statusDokumen == 'asli') : ?>
                            <h3 class="page-title"><?= Yii::t('app', 'Detail Dokumen') ?></h3>
                            <div class="alert alert-success">
                                <b><?= Yii::t('app', 'Dokumen Surat Tugas') ?></b>
                                <ul>
                                    <?php foreach ($data['disetujui'] as $d) : ?>
                                        <li>Ditandatangani oleh <b><?= $d['nama'] ?></b> pada <b><?= MyHelper::convertWaktu($d['waktu']) ?></b> sebagai <b><?= $d['jabatan'] ?></b></li>
                                    <?php endforeach; ?>
                                </ul>
                                <br>
                                <?= Html::a('<i class="fa fa-file-pdf-o"></i> Dokumen', $data['link'], ['class' => 'btn btn-danger btn-xs', 'target' => '_blank']) ?>
                            </div>
                        <?php elseif ($statusDokumen == 'palsu') : ?>
                            <h3 class="page-title"><?= Yii::t('app', 'Detail Dokumen') ?></h3>
                            <div class="alert alert-danger">
                                <b><?= Yii::t('app', 'Dokumen tidak ditemukan') ?></b>
                            </div>
                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<?php JSRegister::begin() ?>
<script>
    $('.btn-refresh-captcha').on('click', function() {
        $.ajax({
            url: '/persetujuan-cek/cek-keaslian', // Ganti dengan URL yang sesuai
            dataType: 'json',
            success: function(data) {
                $('#my-captcha-image').attr('src', data['url']);
            }
        });
    });
</script>
<?php JSRegister::end() ?>