<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersetujuanCek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Kode dokumen</label>
            <?= $form->field($model, 'kode_dokumen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Nama kegiatan</label>
            <?= $form->field($model, 'nama_kegiatan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Nama dokumen</label>
            <?= $form->field($model, 'nama_dokumen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Acara</label>
            <?= $form->field($model, 'acara',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Tahun</label>
            <?= $form->field($model, 'tahun',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Created at</label>
            <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Updated at</label>
            <?= $form->field($model, 'updated_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
