<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Aspek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Aspek</label>
        <?= $form->field($model, 'aspek', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Status aktif</label>
        <?= $form->field($model, 'status_aktif', ['options' => ['tag' => false]])->radioList(\app\helpers\MyHelper::getStatusAktif())->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>