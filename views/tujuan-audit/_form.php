<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TujuanAudit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>

    <div class="form-group">
        <label class="control-label">Tujuan audit</label>
        <?= $form->field($model, 'tujuan_audit', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Keterangan</label>
        <?= $form->field($model, 'keterangan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>