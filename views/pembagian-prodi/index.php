<?php

use app\models\PembagianProdi;
use app\models\UnitKerja;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PembagianProdiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pembagian Prodis';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <?php
                            $listFakultas = UnitKerja::find()->where(['jenis'=>'fakultas'])->orderBy('nama ASC')->all();
                            foreach ($listFakultas as $fakultas) {
                            ?>
                            <th><?= $fakultas->nama; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $listProdi = UnitKerja::find()->where(['jenis'=>'prodi'])->orderBy('nama ASC')->all();
                        foreach ($listProdi as $prodi) {
                        ?>
                        <tr>
                            <th><?= $prodi->nama; ?></th>

                            <?php
                                foreach ($listFakultas as $fakultas) {
                                    $m = PembagianProdi::find()->where(['prodi_id' => $prodi->id, 'fakultas_id' => $fakultas->id])->one();

                                    $checked = !empty($m) ? 'checked' : '';
                                ?>
                            <td><input name="<?= $prodi->id ?>" type="radio" <?= $checked; ?> class="mycheck"
                                    data-prodi="<?= $prodi->id; ?>" data-fakultas="<?= $fakultas->id; ?>" m value="1">
                            </td>
                            <?php
                                }
                                ?>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php 

$script = ' 
$(".mycheck").change(function(){
    var prodi       = $(this).data("prodi");
    var fakultas    = $(this).data("fakultas");
    var obj         = new Object;
    obj.prodi_id    = prodi;
    obj.fakultas_id = fakultas;
    obj.checked     = $(this).prop("checked") ? "1" : "0";

    $.ajax({
        type : "POST",
        url : "'.Url::to(['pembagian-prodi/ajax-prodi-fakultas']).'",
        data : {
            dataPost: obj
        },
        success : function(data){
            console.log(data)
        }
    });
});
';


$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);

?>