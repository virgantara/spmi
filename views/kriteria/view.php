<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kriteria */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kriterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$setStatus = MyHelper::getStatusAktif();
$setKhusus = MyHelper::getKhusus();
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'nama',
                        // 'status_aktif',
                        [
                            'attribute' => 'status_aktif',
                            'value' => function ($data) use ($setStatus) {
                                return (isset($data->status_aktif) ? $setStatus[$data->status_aktif] : null);
                            }
                        ],
                        'urutan',
                        // 'jenis',
                        [
                            'attribute' => 'jenis',
                            'value' => function ($data) {
                                return MyHelper::setJenisKriteria($data->jenis);
                            }
                        ],
                        [
                            // 'attribute' => 'status_aktif',
                            'label' => 'Jenis Kriteria',
                            'value' => function ($data) use ($setKhusus) {
                                return (isset($data->is_khusus) ? $setKhusus[$data->is_khusus] : null);
                            }
                        ],
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>