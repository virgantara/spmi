<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kriteria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>


        <?= $form->field($model, 'nama')->textInput(['class'=>'form-control','maxlength' => true])->label('Nama Kriteria') ?>
        <?= $form->field($model, 'status_aktif')->radioList(\app\helpers\MyHelper::getStatusAktif()) ?>
        <?= $form->field($model, 'urutan')->textInput(['class'=>'form-control','maxlength' => true]) ?>
        <?= $form->field($model, 'jenis')->dropDownList(MyHelper::getJenisKriteria(), ['prompt' => '- Pilih Jenis -']) ?>
        <?= $form->field($model, 'is_khusus')->dropDownList(MyHelper::getKhusus(), ['prompt' => '- Jenis Instrumen -'])->label('Jenis Instrumen') ?>

        <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
