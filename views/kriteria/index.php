<?php

use yii\helpers\Html;
use kartik\grid\GridView;

use app\helpers\MyHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KriteriaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kriterias';
$this->params['breadcrumbs'][] = $this->title;
$list_aktif = MyHelper::getStatusAktif();
$setKhusus = MyHelper::getKhusus();
$listJenisStandar = [
    'standar-mutu' => 'Standar Mutu',
    'non-standar-mutu' => 'Non Standar Mutu'
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <div class="alert alert-warning">
                    <b><?= Yii::t('app', 'Note:') ?></b>
                    <ul>
                        <li>Kolom kode hanya digunakan untuk input template standar mutu</li>
                        <li>Kriteria yang dinonaktifkan tidak muncul pada Audit Mutu Internal yang sedang aktif</li>
                    </ul>
                </div>
                <p>
                    <?= Html::a('Create Kriteria', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'urutan',
                        'contentOptions' => [
                            'width' => '5%',
                        ],
                        'hAlign' => 'center',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'kode',
                        'contentOptions' => [
                            'width' => '5%',
                        ],
                        'hAlign' => 'center',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions'   => [
                            'inputType'     => \kartik\editable\Editable::INPUT_TEXT,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'nama',
                        'label' => 'nama',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                            'asPopover' => false,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'keterangan',
                        'label' => 'Keterangan',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                            'asPopover' => false,
                        ],
                        'value' => function ($data) {
                            return ($data->keterangan == null ? '' : $data->keterangan);
                        }
                    ],
                    [
                        'attribute' => 'jenis',
                        'contentOptions' => [
                            'width' => '10%',
                        ],
                        'hAlign' => 'center',
                        'value' => function ($data) {
                            return MyHelper::setJenisKriteria($data->jenis);
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'jenis_standar',
                        'label' => 'jenis_standar',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'data' => $listJenisStandar,
                            // 'asPopover' => false,
                        ],
                        'contentOptions' => [
                            'width' => '10%',
                        ],
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'is_khusus',
                        'filter' => $setKhusus,
                        'contentOptions' => [
                            'width' => '10%',
                        ],
                        'hAlign' => 'center',
                        'label' => 'Jenis Kriteria',
                        'value' => function ($data) use ($setKhusus) {
                            return (!empty($setKhusus[$data->is_khusus]) ? $setKhusus[$data->is_khusus] : null);
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'status_aktif',
                        'contentOptions' => [
                            'width' => '10%',
                        ],
                        'hAlign' => 'center',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'refreshGrid' => true,
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::getStatusAktif(),
                        ],
                        'value' => function ($data) {
                            return (isset($data->status_aktif) ? MyHelper::setStatusAktif($data->status_aktif) : '-');
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>