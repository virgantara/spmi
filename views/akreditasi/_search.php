<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkreditasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akreditasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'lembaga_akreditasi_id') ?>

    <?= $form->field($model, 'prodi_id') ?>

    <?= $form->field($model, 'status_akreditasi') ?>

    <?= $form->field($model, 'tanggal_sk') ?>

    <?php // echo $form->field($model, 'tanggal_kadaluarsa') ?>

    <?php // echo $form->field($model, 'nomor_sk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
