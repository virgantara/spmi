<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Akreditasi */
/* @var $form yii\widgets\ActiveForm */
$listAkreditasi = \app\helpers\MyHelper::listAkreditasi();
?>

<div class="body">

     <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
            'enctype' => 'multipart/form-data'
        ]
    ]); 

     echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    ?>
        <?= $form->field($model, 'lembaga_akreditasi_id')->dropDownList(ArrayHelper::map($list_lembaga_akreditasi,'id','singkatan_lembaga'),['class'=>'form-control','maxlength' => true]) ?>
        <?= $form->field($model, 'status_akreditasi')->dropDownList($listAkreditasi,['class'=>'form-control','maxlength' => true]) ?>
        <?= $form->field($model, 'tanggal_sk')->widget(DatePicker::classname(),[
                'options' => ['placeholder' => 'Pilih Tanggal SK ...'],
                'convertFormat' => true,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'php:Y-m-d',
                    'autoclose' => true,
                ]
            ]) ?>
        <?= $form->field($model, 'tanggal_kadaluarsa')->widget(DatePicker::classname(),[
                'options' => ['placeholder' => 'Pilih Tanggal Kadaluarsa ...'],
                'convertFormat' => true,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'format' => 'php:Y-m-d',
                    'autoclose' => true,
                ]
            ]) ?>
        <?= $form->field($model, 'nomor_sk')->textInput(['class'=>'form-control','maxlength' => true]) ?>
    
        <?= $form->field($model, 'file_path')->fileInput(['accept'=>'application/pdf']) ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
