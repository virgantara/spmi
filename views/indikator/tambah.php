<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Indikator */

$this->title = 'Create Indikator Khusus';
$this->params['breadcrumbs'][] = ['label' => 'Standar Mutu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">



                <?= $this->render('_tambah', [
                    'model' => $model,
                    'ami_unit' => $ami_unit,
                ]) ?>
            </div>
        </div>
    </div>
</div>