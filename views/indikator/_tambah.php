<?php

use app\models\Ami;
use app\models\AmiUnit;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Indikator */
/* @var $form yii\widgets\ActiveForm */

$list_kriteria = ArrayHelper::map(\app\models\Kriteria::find()->where(['jenis' => 1, 'is_khusus' => 1])->all(), 'id', 'nama');



// echo '<pre>';print_r($ami_unit);die;
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>
</div>
<?= $form->field($model, 'kriteria_id')->widget(Select2::classname(), [
    'data' => $list_kriteria,

    'options' => ['placeholder' => Yii::t('app', '- Pilih Jenis Dokumen-')],
    'pluginOptions' => [
        'allowClear' => true,
    ],
])->label('Jenis Dokumen') ?>
<?= $form->field($model, 'nomor')->hiddenInput(['class' => 'form-control', 'maxlength' => true, 'value' => 1])->label(false) ?>
<?= $form->field($model, 'ami_unit')->hiddenInput(['value' => $ami_unit])->label(false) ?>
<?= $form->field($model, 'nama')->textInput(['class' => 'form-control', 'maxlength' => true])->label('Nama Standar') ?>
<?= $form->field($model, 'pernyataan')->textInput(['class' => 'form-control', 'maxlength' => true])->label('Pernyataan Standar') ?>
<?= $form->field($model, 'indikator')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
<?= $form->field($model, 'referensi')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
<?= $form->field($model, 'strategi')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
<?= $form->field($model, 'bobot')->hiddenInput(['class' => 'form-control', 'maxlength' => true, 'value' => 1])->label(false) ?>
<?= $form->field($model, 'status_aktif')->hiddenInput(['value' => '1'])->label(false) ?>

<?= $form->field($model, 'jenis_indikator')->hiddenInput(['value' => '2'])->label(false) ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

<?php ActiveForm::end(); ?>

</div>