<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Ami;
use app\models\UnitKerja;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Instrumen Audit - Satuan Kerja';
$this->params['breadcrumbs'][] = $this->title;

$list_aktif = MyHelper::getStatusAktif();
?>
<div class="panel">
    <div class="panel-heading">
        <h3 class="page-title"><?= $this->title; ?></h3>
        <div class="custom-tabs-line tabs-line-bottom left-aligned">
            <?php

            echo \yii\widgets\Menu::widget([
                'options' => [
                    'class' => 'nav nav-tabs',
                    'role' => 'tablist'
                ],
                'items' => [
                    [
                        'label' => 'Fakultas', 'url' => ['indikator/fakultas'],
                    ],
                    [
                        'label' => 'Program Studi', 'url' => ['indikator/program-studi'],
                    ],
                    [
                        'label' => 'Satuan Kerja', 'url' => ['indikator/satuan-kerja'],
                        'options' => ['class' => 'active']
                    ],
                ],
            ]);
            ?>
        </div>

    </div>

    <div class="panel-body">


        <div class="form-group">
            <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Indikator'), ['create'], ['class' => 'btn btn-success', 'id' => 'btn-add-satker']) ?>

        </div>
        <br>

        <p>
        </p>
        <?php
        $gridColumns = [
            [
                'class' => 'kartik\grid\SerialColumn',
                'contentOptions' => ['class' => 'kartik-sheet-style'],
                'width' => '36px',
                'pageSummary' => 'Total',
                'pageSummaryOptions' => ['colspan' => 6],
                'header' => '',
                'headerOptions' => ['class' => 'kartik-sheet-style']
            ],
            [
                'attribute' => 'kriteria_id',
                'label' => 'Kriteria',
                'filter' => $listKriteria,
                'value' => function ($data) {
                    return (!empty($data) ? $data->kriteria->nama : null);
                }
            ],
            [
                'attribute' => 'nama',
                'label' => 'Nama Dokumen',
                'value' => function ($data) {
                    return $data->nama;
                }
            ],
            [
                'attribute' => 'status_aktif',
                'filter' => $list_aktif,
                'value' => function ($data) use ($list_aktif) {
                    return (!empty($list_aktif[$data->status_aktif]) ? $list_aktif[$data->status_aktif] : null);
                }
            ],
            ['class' => 'yii\grid\ActionColumn']
        ]; ?>
        <?= GridView::widget([
            'pager' => [
                'options' => ['class' => 'pagination'],
                'activePageCssClass' => 'active paginate_button page-item',
                'disabledPageCssClass' => 'disabled paginate_button',
                'prevPageLabel' => 'Previous',
                'nextPageLabel' => 'Next',
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'nextPageCssClass' => 'paginate_button next page-item',
                'prevPageCssClass' => 'paginate_button previous page-item',
                'firstPageCssClass' => 'first paginate_button page-item',
                'lastPageCssClass' => 'last paginate_button page-item',
                'maxButtonCount' => 10,
                'linkOptions' => [
                    'class' => 'page-link'
                ]
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsiveWrap' => false,
            'columns' => $gridColumns,
            'containerOptions' => ['style' => 'overflow: auto'],
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'containerOptions' => ['style' => 'overflow: auto'],
            'beforeHeader' => [
                [
                    'columns' => [
                        ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'exportConfig' => [
                GridView::PDF => ['label' => 'Save as PDF'],
                GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
            ],

            'toolbar' =>  [
                '{export}',

                '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
            ],
            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // set export properties
            'export' => [
                'fontAwesome' => true
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container',
                ]
            ],
            'id' => 'my-grid',
            'bordered' => true,
            'striped' => true,
            // 'condensed' => false,
            // 'responsive' => false,
            'hover' => true,
            // 'floatHeader' => true,
            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
            'panel' => [
                'type' => GridView::TYPE_PRIMARY
            ],
        ]); ?>
    </div>
</div>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal-satker',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs-satker">
    <div class="form-group">
        <label for="">Kriteria</label>
        <?= Html::dropDownList('kriteria_id', '', $listKriteria, ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('skor1', '-', ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('skor2', '-', ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('skor4', '-', ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('skor3', '-', ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('bobot', 1, ['class' => 'form-control']) ?>
        <?= Html::hiddenInput('nomor', 1, ['class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <label for="">Nama Dokumen</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control']) ?>
    </div>


    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan-satker']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>

<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "#btn-simpan-satker", function(e) {
        e.preventDefault();

        var obj = $("#form-krs-satker").serialize()

        $.ajax({
            url: "/indikator/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal-satker").modal("hide")
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    })

    $(document).on("click", "#btn-add-satker", function(e) {
        e.preventDefault();
        $("#modal-satker").modal("show")

    })
</script>
<?php JSRegister::end() ?>
