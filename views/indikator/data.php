<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Jenjang;
use app\models\JenjangBobot;
use app\models\UnitKerja;
use kartik\export\ExportMenu;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'History Standar Mutu');
$this->params['breadcrumbs'][] = $this->title;
$listJenjang = ArrayHelper::map(Jenjang::find()->all(), 'id', 'nama');
$listAuditee = ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');
?>

<!-- <div class="row"> -->
<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>
        <?= $this->render('../nav-master/nav_indikator') ?>
    </div>

    <div class="panel-body">
        <div class="x_content">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="questionOne">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                            <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                        </a>
                    </h5>
                </div>
                <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                    <div class="panel-body">
                        <form action="<?= Yii::$app->urlManager->createUrl(['indikator/data']) ?>" method="get">
                            <div class="row">
                                <div class="col-lg-4">
                                    <input type="hidden" name="ami_id" value="<?= $_GET['ami_id'] ?? '' ?>">
                                    <div class="form-group">
                                        <label for="">Kriteria</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-kriteria_id',
                                            'name' => 'kriteria_id[]',
                                            'data' => $listKriteria,
                                            'value' => $_GET['kriteria_id'] ?? [],
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Kriteria')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Satuan Kerja</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-unit_id',
                                            'name' => 'unit_id[]',
                                            'data' => $listAuditee,
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Satuan kerja')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>

                                    </div>
                                </div>

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label for="">Jenjang</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-jenjang_id',
                                            'name' => 'jenjang_id[]',
                                            'data' => $listJenjang,
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Jenjang')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <?= Html::submitButton('Apply Filter', ['class' => 'btn btn-primary']) ?>
                                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-reset', 'type' => 'reset']) ?>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <?php if (Yii::$app->user->can('admin')) : ?>
                <div class="alert alert-info">
                    <b><?= Yii::t('app', 'Note:') ?></b>
                    <ul>
                        <li>Sebelum melakukan upload pastikan seluruh <?= Html::a('kode kriteria', ['kriteria/index'], ['style' => 'color:yellow;', 'target' => '_blank']) ?> telah terisi</li>
                        <li>Menginputkan dokumen melalui upload data excel sesuai dengan template yang telah disediakan</li>
                        <li>Jika instrumen memiliki bobot yang sama, maka dapat di ubah langsung tanpa klik update (pencil)</li>
                    </ul>
                </div>
            <?php endif; ?>

            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'contentOptions' => ['class' => 'kartik-sheet-style'],
                    'width' => '36px',
                    'pageSummary' => 'Total',
                    'pageSummaryOptions' => ['colspan' => 6],
                    'header' => '',
                    'headerOptions' => ['class' => 'kartik-sheet-style']
                ],
                [
                    'attribute' => 'kriteria_id',
                    'label' => 'Kriteria',
                    'filter' => $listKriteria,
                    'value' => function ($data) {
                        return (!empty($data) ? $data->kriteria->nama : null);
                    }
                ],
                [
                    'attribute' => 'nama',
                    'label' => 'Nama Standar',
                ],
                [
                    'attribute' => 'pernyataan',
                ],
                [
                    'attribute' => 'indikator',
                ],
                [
                    'attribute' => 'referensi',
                ],
                [
                    'attribute' => 'strategi',
                ],
                [
                    'label' => 'Jenjang',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $jenjang = '';

                        $data = JenjangBobot::find()->where([
                            'indikator_id' => $data->id,
                        ])->all();

                        foreach ($data as $k => $v) {
                            $jenjang .= ' ' . Html::tag('span', $v->jenjang->nama, ['class' => 'label label-primary']);
                        }

                        return $jenjang;
                    }
                ],
                [
                    'attribute' => 'unit_id',
                    'value' => function ($model) {
                        return $model->unit->nama ?? null;
                    }
                ],
                [
                    'filter' => MyHelper::getStatusAktif(),
                    'attribute' => 'status_aktif',
                    'value' => function ($data) {
                        return MyHelper::setStatusAktif($data->status_aktif) ?? null;
                    }
                ]
            ]; ?>
            <p>
                <?php
                // Renders a export dropdown menu
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'clearBuffers' => true, //optional
                ]);
                ?>
            </p>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>
        </div>
    </div>
</div>