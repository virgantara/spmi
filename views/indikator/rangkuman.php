<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Jenjang;
use app\models\JenjangBobot;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Instrumen Audit - Fakultas';
$this->params['breadcrumbs'][] = $this->title;


$list_aktif = MyHelper::getStatusAktif();
?>
<div class="panel">
    <div class="panel-heading">
        <h3 class="page-title">Tes</h3>

    </div>

    <div class="panel-body">

        <?php
        $data = !empty($_GET['jenjang_id']) ? $_GET['jenjang_id'] : '';

        $form = ActiveForm::begin([
            'method' => 'GET',
            'action' => ['indikator/fakultas'],
            'options' => [
                'id' => 'form_validation',
            ]
        ]);

        ?>

        <div class="col-md-5">

            <div class="form-group">
                <label class="control-label ">Filter Jenjang</label>
                <?= Select2::widget([
                    'id' => 'filter_jenjang',
                    'name' => 'filter_jenjang',
                    'value' => $data,
                    'data' => ArrayHelper::map(Jenjang::find()->asArray()->all(), 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('app', '- Pilih jenjang -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true,
                    ],
                ]); ?>
            </div>

            <div class="form-group clearfix">
                <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-filter"></i>
                    Filter</button>
                <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Indikator'), ['create'], ['class' => 'btn btn-success', 'id' => 'btn-add']) ?>
            </div>

        </div>

        <div class="col-md-5">

            <div class="x_content">

                <?= Html::a('<i class="fa fa-refresh"> </i> Refresh', ['fakultas'], ['class' => 'btn btn-info btn-sm']) ?>

                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="text-align: center;width: 23%">Jenjang</th>
                            <th style="text-align: center;width: 2%"></th>
                            <th style="width: 25%">Total fakultas</th>
                            <th style="width: 25%">Total prodi</th>
                            <th style="width: 25%">Total Bobot</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center;">D4 </td>
                            <td>:</td>
                            <td><?= $total_d4 ?></td>
                            <td><?= MyHelper::getSumBobotIndikator(2, 1) ?></td>
                            <td><?= $total_d4 + MyHelper::getSumBobotIndikator(2, 1) ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">S1 </td>
                            <td>:</td>
                            <td><?= $total_s1 ?></td>
                            <td><?= MyHelper::getSumBobotIndikator(2, 2) ?></td>
                            <td><?= $total_s1 + MyHelper::getSumBobotIndikator(2, 2) ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">S2 </td>
                            <td>:</td>
                            <td><?= $total_s2 ?></td>
                            <td><?= MyHelper::getSumBobotIndikator(2, 3) ?></td>
                            <td><?= $total_s2 + MyHelper::getSumBobotIndikator(2, 3) ?></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">S3 </td>
                            <td>:</td>
                            <td><?= $total_s3 ?></td>
                            <td><?= MyHelper::getSumBobotIndikator(2, 4) ?></td>
                            <td><?= $total_s3 + MyHelper::getSumBobotIndikator(2, 4) ?></td>
                        </tr>

                    </tbody>
                </table>

            </div>

        </div>

        <?php ActiveForm::end(); ?>

        <br>
        <p>* Jika instrumen memiliki bobot yang sama, maka dapat di ubah langsung tanpa klik update (pencil)</p>

        <?php
        $gridColumns = [
            [
                'class' => 'kartik\grid\SerialColumn',
                'contentOptions' => ['class' => 'kartik-sheet-style'],
                'width' => '36px',
                'pageSummary' => 'Total',
                'pageSummaryOptions' => ['colspan' => 6],
                'header' => '',
                'headerOptions' => ['class' => 'kartik-sheet-style']
            ],
            [
                'attribute' => 'kriteria_id',
                'label' => 'Kriteria',
                'filter' => $listKriteria,
                'value' => function ($data) {
                    return (!empty($data) ? $data->kriteria->nama : null);
                }
            ],
            [
                'attribute' => 'nama',
                'label' => 'Nama Instrumen',
                'value' => function ($data) {
                    return $data->nama;
                }
            ],
            [
                'label' => 'Jenjang',
                'format' => 'raw',
                'value' => function ($data) {
                    $jenjang = '';

                    $data = JenjangBobot::find()->where([
                        'indikator_id' => $data->id,
                    ])->all();

                    foreach ($data as $k => $v) {
                        $jenjang .= ' ' . Html::tag('span', $v->jenjang->nama, ['class' => 'label label-primary']);
                    }

                    return $jenjang;
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'bobot',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    'asPopover' => false,
                ],
                'value' => function ($data) {
                    return ($data->bobot == null ? 'Bobot berbeda' : $data->bobot);
                }
            ],
            [
                'label' => 'D4',
                'value' => function ($data) {

                    $data = JenjangBobot::find()->select('bobot')->where(['indikator_id' => $data->id, 'jenjang_id' => 1])->one();

                    return (isset($data->bobot) ? $data->bobot : '');
                }
            ],
            [
                'label' => 'S1',
                'value' => function ($data) {

                    $data = JenjangBobot::find()->select('bobot')->where(['indikator_id' => $data->id, 'jenjang_id' => 2])->one();

                    return (isset($data->bobot) ? $data->bobot : '');
                }
            ],
            [
                'label' => 'S2',
                'value' => function ($data) {

                    $data = JenjangBobot::find()->select('bobot')->where(['indikator_id' => $data->id, 'jenjang_id' => 3])->one();

                    return (isset($data->bobot) ? $data->bobot : '');
                }
            ],
            [
                'label' => 'S3',
                'value' => function ($data) {

                    $data = JenjangBobot::find()->select('bobot')->where(['indikator_id' => $data->id, 'jenjang_id' => 4])->one();

                    return (isset($data->bobot) ? $data->bobot : '');
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'filter' => $list_aktif,
                'refreshGrid' => true,
                'attribute' => 'status_aktif',
                'readonly' => !Yii::$app->user->can('admin'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => $list_aktif,
                ],
                'value' => function ($data) use ($list_aktif) {
                    return !empty($list_aktif[$data->status_aktif]) ? MyHelper::setStatusAktif($list_aktif[$data->status_aktif]) : null;
                }
            ],
            [
                'format' => 'raw',
                'value' => function ($data) {
                    $eye = Html::a('', ['view', 'id' => $data->id], ['class' => 'glyphicon glyphicon-eye-open']);
                    $pencil = Html::a('', ['fakultas'], ['class' => 'glyphicon glyphicon-pencil update_indikator', 'data-instrumen' => $data->id]);
                    $trash = Html::a('', ['delete', 'id' => $data->id], [
                        'class' => 'glyphicon glyphicon-trash',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                    return $eye . ' ' . $pencil . ' ' . $trash;
                },
            ],
            // ['class' => 'yii\grid\ActionColumn']
        ]; ?>
        <?= GridView::widget([
            'pager' => [
                'options' => ['class' => 'pagination'],
                'activePageCssClass' => 'active paginate_button page-item',
                'disabledPageCssClass' => 'disabled paginate_button',
                'prevPageLabel' => 'Previous',
                'nextPageLabel' => 'Next',
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'nextPageCssClass' => 'paginate_button next page-item',
                'prevPageCssClass' => 'paginate_button previous page-item',
                'firstPageCssClass' => 'first paginate_button page-item',
                'lastPageCssClass' => 'last paginate_button page-item',
                'maxButtonCount' => 10,
                'linkOptions' => [
                    'class' => 'page-link'
                ]
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsiveWrap' => false,
            'columns' => $gridColumns,
            'containerOptions' => ['style' => 'overflow: auto'],
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'containerOptions' => ['style' => 'overflow: auto'],
            'beforeHeader' => [
                [
                    'columns' => [
                        ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                    ],
                    'options' => ['class' => 'skip-export']
                ]
            ],
            'exportConfig' => [
                GridView::PDF => ['label' => 'Save as PDF'],
                GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
            ],

            'toolbar' =>  [
                '{export}',

                '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
            ],
            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // set export properties
            'export' => [
                'fontAwesome' => true
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'pjax-container',
                ]
            ],
            'id' => 'my-grid',
            'bordered' => true,
            'striped' => true,
            // 'condensed' => false,
            // 'responsive' => false,
            'hover' => true,
            // 'floatHeader' => true,
            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
            'panel' => [
                'type' => GridView::TYPE_PRIMARY
            ],
        ]); ?>
    </div>
</div>

<?php


yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs">
    <div class="form-group">
        <label for="">Kriteria</label>
        <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id"]) ?>
        <?= Html::dropDownList('kriteria_id', '', $listKriteria, ['class' => 'form-control', "id" => "kriteria_id"]) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Instrumen</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control', "id" => "nama"]) ?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Indikator Skor 1</label>
                <?= Html::textArea('skor1', '', ['class' => 'form-control', "id" => "skor1"]) ?>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Indikator Skor 2</label>
                <?= Html::textArea('skor2', '', ['class' => 'form-control', "id" => "skor2"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Indikator Skor 3</label>
                <?= Html::textArea('skor3', '', ['class' => 'form-control', "id" => "skor3"]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Indikator Skor 4</label>
                <?= Html::textArea('skor4', '', ['class' => 'form-control', "id" => "skor4"]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="">Jenjang</label>
        <?php
        $listJenjang = Jenjang::find()->all();
        ?>
        <?= Select2::widget([
            'id' => 'pilih_jenjang',
            'name' => 'jenjang_id[]',
            'data' => ArrayHelper::map($listJenjang, 'id', 'nama'),
            'options' => ['placeholder' => Yii::t('app', '- Pilih Jenjang -')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ]); ?>
    </div>


    <div class="form-group">
        <label for="">Apakah nilai bobot antar jenjang sama ?</label>

    </div>
    <?= Html::button('<i class="fa fa-file"></i> Sama', ['class' => 'btn btn-success', 'disabled' => true, 'id' => 'btn-sama']) ?>
    <?= Html::button('<i class="fa fa-clone"></i> Berbeda', ['class' => 'btn btn-success', 'disabled' => true, 'id' => 'btn-beda']) ?>


    <div class="form-group bobot_sama">
        <label for="">Bobot</label>
        <?= Html::textInput('bobot', '', ['class' => 'form-control', "id" => "bobot"]) ?>
    </div>

    <div class="bobot_beda">
        <label for="">Bobot</label>
        <div class="row">
            <?php
            $i = 1;
            $listJenjang = Jenjang::find()->all();
            foreach ($listJenjang as $key) :
            ?>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Html::textInput('nilai_bobot[' . $key->id . ']', '', ['class' => 'form-control', 'placeholder' => $key->nama, "id" => "nilai_bobot" . $i]) ?>
                    </div>
                </div>
            <?php
                $i++;
            endforeach;
            ?>
        </div>
    </div>

    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>



<?php

$this->registerJs(' 

$(document).on("click", "#btn-simpan", function(e){
    e.preventDefault();
    
    var obj = $("#form-krs").serialize()
    
    $.ajax({
        url: "/indikator/ajax-add",
        type : "POST",
        async : true,
        data: obj,
        error : function(e){
            console.log(e.responseText)
        },
        beforeSend: function(){
            Swal.showLoading()
        },
        success: function (data) {
            Swal.close()
            var hasil = $.parseJSON(data)
            if(hasil.code == 200){
                Swal.fire({
                    title: \'Yeay!\',
                    icon: \'success\',
                    text: hasil.message
                }).then(res=>{
                    $("#modal").modal("hide")
                    $(":input","#modal").val("")
                });
                
                $.pjax.reload({container: "#pjax-container"});
                
            }
            else {
                Swal.fire({
                    title: \'Oops!\',
                    icon: \'error\',
                    text: hasil.message
                })
            }
        }
    })
});

$(document).on("click", "#btn-add", function(e){
    e.preventDefault();
    $("#modal").modal("show")
    $(".bobot_beda").hide()
});

$(document).on("click", ".update_indikator", function(e){
    e.preventDefault();
    var data_instrumen = $(this).data("instrumen")
        
    $.ajax({
        url: "/indikator/ajax-get-data",
        type    : "POST",
        async   : true,
        data    : {
            id: data_instrumen
        },
        error   : function(e){
            console.log(e.responseText)
        },
        success : function (data) {
            var hasil = $.parseJSON(data)
			var jenjangId = new Array();
            $.each(hasil.jenjang, (k, v) => {
                jenjangId.push(v.id)
                $("#nilai_bobot"+v.id).val(v.bobot)
            })
            
            $("#modal").modal("show")
            $("#id").val(hasil.id)
            $("#kriteria_id").val(hasil.kriteria_id)
            $("#nama").val(hasil.nama)
            $("#skor1").val(hasil.skor1)
            $("#skor2").val(hasil.skor2)
            $("#skor3").val(hasil.skor3)
            $("#skor4").val(hasil.skor4)
            $("#pilih_jenjang").val(jenjangId).trigger("change")
            
            console.log(hasil)
            if (hasil.bobot == null) {
                $(".bobot_beda").show()
                $(".bobot_sama").hide()
            }else {
                $("#bobot").val(hasil.bobot)
                $(".bobot_sama").show()
                $(".bobot_beda").hide()
            }
        }
    })
    
});

$(document).on("click", "#btn-sama", function(e) {
    e.preventDefault();
    $(".bobot_sama").show()
    $(".bobot_beda").hide()
});

$(document).on("click", "#btn-beda", function(e) {
    e.preventDefault();
    $(".bobot_beda").show()
    $(".bobot_sama").hide()
});

$(document).on("change", "#pilih_jenjang", function(e) {
    e.preventDefault();

    $("#btn-sama").prop("disabled", false);
    $("#btn-beda").prop("disabled", false);
});

$(document).on("click", ".close", function(e) {
    e.preventDefault();
    $(":input","#modal").val("")
});

', \yii\web\View::POS_READY);

?>