<?php

use app\helpers\MyHelper;
use app\models\Auditor;
use app\models\Indikator;
use app\models\Kriteria;
use app\models\StatusAuditor;
use Google\Service\PeopleService\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AmiUnit */

$this->title = 'Indikator Khusus SATKER - Audit Mutu Internal UNIDA';
$this->params['breadcrumbs'][] = ['label' => 'Penetapan Standar Khusus Satuan Kerja', 'url' => ['ami']];
$this->params['breadcrumbs'][] = $this->title;
$list_ami = MyHelper::getStatusAmi();
// echo '<pre>';print_r($model);die;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">

                <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['indikator/tambah', 'id_ami_unit' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<i class="fa fa-mail-reply"></i> Back', ['indikator/ami'], ['class' => 'btn btn-info']) ?>

            </div>

            <div class="panel-body ">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'unit_id',
                            'label' => 'Unit Kerja',
                            'value' => function ($data) {
                                return (!empty($data) ? $data->nama : null);
                            }
                        ],
                        [
                            'attribute' => 'penanggung_jawab',
                            'label'     => 'Ketua / Penanggung Jawab',
                            'value'     => function ($data) {
                                return (!empty($data) ? $data->penanggung_jawab : null);
                            }
                        ],
                    ],
                ]) ?>

                <?php
                if (!empty($indikator_khusus)) :
                ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th style="text-align: center" class="alert alert-info" colspan="4"><strong>Instrumen Khusus Satuan Kerja - <?= $model->nama ?> </strong></th>
                            </tr>
                            <tr>

                                <th width="5%" style="text-align: center"><strong>No</strong></th>
                                <th width="10%" style="text-align: center"><strong>Kriteria</strong></th>
                                <th width="80%" style="text-align: center"><strong>Nama Indikator</strong></th>
                                <th width="5%" style="text-align: center"><strong>Aksi</strong></th>

                            </tr>

                            <?php

                            $i = 1;

                            foreach ($indikator_khusus as $key => $a) :

                                $kriteria = Kriteria::findOne($a->kriteria_id);

                            ?>

                                <tr>
                                    <td style="text-align: center"><?= $i; ?></td>
                                    <td style="text-align: center"><?= $kriteria->nama ?></td>
                                    <td><?= $a->nama ?></td>
                                    <td style="text-align: center">
                                        <?= Html::a('', ['update', 'id' => $a->id], ['class' => 'glyphicon glyphicon-pencil']) ?>
                                        <?= Html::a('', ['deletee', 'id' => $a->id], [
                                            'class' => 'glyphicon glyphicon-trash',
                                            'data' => [
                                                'confirm' => 'Are you sure you want to delete this item?',
                                                'method' => 'post',
                                            ],
                                        ]); ?>
                                    </td>
                                </tr>
                            <?php
                                $i++;
                            endforeach;
                            ?>

                        </table>
                    </div>
                <?php

                endif;
                ?>

            </div>
        </div>

    </div>
</div>