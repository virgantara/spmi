<?php

use app\helpers\MyHelper;
use app\models\JenjangBobot;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Indikator */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Indikators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Back', ['fakultas'], ['class' => 'btn btn-info']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label'     => 'Kriteria',
                            'attribute' => 'kriteria_id',
                            'value'     => function ($data) {
                                return $data->kriteria->nama;
                            }
                        ],
                        [
                            'label'     => 'Nama Indikator',
                            'attribute' => 'nama',
                        ],
                        [
                            'label'     => 'Indikator Skor 1',
                            'attribute' => 'skor1',
                        ],
                        [
                            'label'     => 'Indikator Skor 2',
                            'attribute' => 'skor2',
                        ],
                        [
                            'label'     => 'Indikator Skor 3',
                            'attribute' => 'skor3',
                        ],
                        [
                            'label'     => 'Indikator Skor 4',
                            'attribute' => 'skor4',
                        ],
                        [
                            'label'     => 'Status Aktif',
                            'value'     => function ($data) {
                                return MyHelper::setStatusAktif($data->status_aktif);
                            },
                        ],
                        [
                            'label'     => 'Jenis Bobot',
                            'format'    => 'raw',
                            'value'     => function ($data) {
                                return ($data->bobot == null ? "<strong>Berbeda</strong>" : "<strong>Sama</strong>");
                            }
                        ]
                    ],
                ]) ?>

            </div>
        </div>

    </div>
    <div class="col-md-4">

        <div class="panel">

            <div class="panel-body ">

                <table id="w1" class="table table-striped table-bordered detail-view">
                    <tr>
                        <th>Jenjang</th>
                        <th>Bobot</th>
                    </tr>

                    <?php 
                    
                    $bobot = JenjangBobot::find()->where([
                        'indikator_id' => $model->id,
                    ])->all();
                    
                    foreach ($bobot as $k => $v) :
                    ?>
                    <tr>
                        <td><?= $v->jenjang->nama ?></td>
                        <td><?= $v->bobot ?></td>
                    </tr>
                    <?php 
                    endforeach;
                    ?>
                </table>

            </div>
        </div>
    </div>
</div>