<?php

use app\helpers\MyHelper;
use app\models\JenjangBobot;
use kartik\select2\Select2;
use Mpdf\Tag\Select;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Indikator */

$this->title = 'Review Standar';
$this->params['breadcrumbs'][] = ['label' => 'Indikators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .hiddenRow {
        padding: 0 !important;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    table,
    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 10px;
        vertical-align: middle;
        /* Center the content vertically */
    }

    /* Menghilangkan default radio button */
    input[type="radio"] {
        display: none;
    }

    /* Mengganti tampilan radio button menjadi kotak checklist */
    input[type="radio"]+label {
        display: inline-block;
        width: 20px;
        height: 20px;
        border: 1px solid #000;
        cursor: pointer;
    }

    /* Memberikan tampilan centang pada radio button yang dipilih */
    input[type="radio"]:checked+label::before {
        content: '\2713';
        display: block;
        text-align: center;
        line-height: 20px;
        font-size: 16px;
        color: #000;
    }
</style>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <label for="">Filter Kriteria</label>
                <?= Select2::widget([
                    'name' => 'kriteria',
                    'id' => 'filter-kriteria',
                    'data' => ArrayHelper::map($listKriteria, 'id', 'nama'),
                    'options' => ['placeholder' => Yii::t('app', 'pilih kriteria')],
                    'pluginOptions' => [
                        'multiple' => true, // Menjadikan pilihan ganda (multiple)
                        // 'allowClear' => true,
                    ],
                ]); ?>

            </div>

            <div class="panel-body">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center;">No</th>
                            <th style="text-align: center;">Detil</th>
                            <th>Kriteria Standar</th>
                            <th>Indikator</th>
                            <th style="text-align: center;">Dipertahankan</th>
                            <th style="text-align: center;">Diturunkan</th>
                            <th style="text-align: center;">Ditingkatan</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($indikators as $indikator) : ?>
                            <tr>
                                <td style="vertical-align: middle;text-align: center;">
                                    <h5><b><?= $no ?></b></h5>
                                </td>
                                <td style="text-align: center;">
                                    <?= Html::button('<i class="fa fa-expand"></i>', ['data-toggle' => "collapse", 'data-target' => "#demo" . $indikator->id, 'class' => "accordion-toggle", 'class' => "btn btn-primary btn-sm"]) ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <h5><b><?= $indikator->kriteria->keterangan ?? $indikator->kriteria->nama ?></b></h5>
                                </td>
                                <td style="vertical-align: middle;">
                                    <b><?= MyHelper::readMore($indikator->indikator, 125) ?></b>
                                </td>
                                <td style="text-align: center;vertical-align: middle;"><input type="radio" name="tes" id="tes1"><label for="tes1"></label></td>
                                <td style="text-align: center;vertical-align: middle;"><input type="radio" name="tes" id="tes2"><label for="tes2"></label></td>
                                <td style="text-align: center;vertical-align: middle;"><input type="radio" name="tes" id="tes3"><label for="tes3"></label></td>
                            </tr>

                            <tr>
                                <td colspan="12" class="hiddenRow">
                                    <div class="accordian-body collapse" id="demo<?= $indikator->id ?>">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr class="info">
                                                    <th>Job</th>
                                                    <th>Company</th>
                                                    <th>Salary</th>
                                                    <th>Date On</th>
                                                    <th>Date off</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <tr data-toggle="collapse" class="accordion-toggle" data-target="#demo10">
                                                    <td> <a href="#">Enginner Software</a></td>
                                                    <td>Google</td>
                                                    <td>U$8.00000 </td>
                                                    <td> 2016/09/27</td>
                                                    <td> 2017/09/27</td>
                                                    <td>
                                                        <a href="#" class="btn btn-default btn-sm">
                                                            <i class="glyphicon glyphicon-cog"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="12" class="hiddenRow">
                                                        <div class="accordian-body collapse" id="demo10">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <td><a href="#"> XPTO 1</a></td>
                                                                        <td>XPTO 2</td>
                                                                        <td>Obs</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>item 1</th>
                                                                        <th>item 2</th>
                                                                        <th>item 3 </th>
                                                                        <th>item 4</th>
                                                                        <th>item 5</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>item 1</td>
                                                                        <td>item 2</td>
                                                                        <td>item 3</td>
                                                                        <td>item 4</td>
                                                                        <td>item 5</td>
                                                                        <td>
                                                                            <a href="#" class="btn btn-default btn-sm">
                                                                                <i class="glyphicon glyphicon-cog"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Scrum Master</td>
                                                    <td>Google</td>
                                                    <td>U$8.00000 </td>
                                                    <td> 2016/09/27</td>
                                                    <td> 2017/09/27</td>
                                                    <td> <a href="#" class="btn btn-default btn-sm">
                                                            <i class="glyphicon glyphicon-cog"></i>
                                                        </a>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>Back-end</td>
                                                    <td>Google</td>
                                                    <td>U$8.00000 </td>
                                                    <td> 2016/09/27</td>
                                                    <td> 2017/09/27</td>
                                                    <td> <a href="#" class="btn btn-default btn-sm">
                                                            <i class="glyphicon glyphicon-cog"></i>
                                                        </a>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td>Front-end</td>
                                                    <td>Google</td>
                                                    <td>U$8.00000 </td>
                                                    <td> 2016/09/27</td>
                                                    <td> 2017/09/27</td>
                                                    <td> <a href="#" class="btn btn-default btn-sm">
                                                            <i class="glyphicon glyphicon-cog"></i>
                                                        </a>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </div>
                                </td>
                            </tr>
                        <?php
                            $no++;
                        endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin() ?>

<script>
    $(document).ready(function() {
        $('#filter-kriteria').on('change', function() {
            var selectedValues = $(this).val();

            console.log(selectedValues);
            // $.ajax({
            //     url: 'indikator/review',
            //     type: 'GET',
            //     data: {
            //         selectedValues: selectedValues
            //     },
            //     success: function(data) {
            //         // console.log(data);
            //     },
            //     error: function(error) {
            //         console.log('Error:', error);
            //     }
            // });
        });
    });
</script>
<?php JSRegister::end() ?>