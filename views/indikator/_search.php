<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndikatorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="indikator-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomor') ?>

    <?= $form->field($model, 'kriteria_id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'skor1') ?>

    <?php // echo $form->field($model, 'skor2') ?>

    <?php // echo $form->field($model, 'skor3') ?>

    <?php // echo $form->field($model, 'skor4') ?>

    <?php // echo $form->field($model, 'bobot') ?>

    <?php // echo $form->field($model, 'status_aktif') ?>

    <?php // echo $form->field($model, 'ami_id') ?>

    <?php // echo $form->field($model, 'jenis_indikator') ?>

    <?php // echo $form->field($model, 'ami_unit') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
