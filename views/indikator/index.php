<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\helpers\MyHelper;
use app\models\Jenjang;
use app\models\JenjangBobot;
use app\models\UnitKerja;
use kartik\export\ExportMenu;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Form Input Standar Mutu');
$this->params['breadcrumbs'][] = $this->title;
$listJenjang = ArrayHelper::map(Jenjang::find()->all(), 'id', 'nama');
$listAuditee = ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');
?>

<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<!-- <div class="row"> -->
<div class="panel">
    <div class="panel-heading">
        <h3><?= Html::encode($this->title) ?></h3>
        <?= $this->render('../nav-master/nav_indikator') ?>
    </div>

    <div class="panel-body">
        <div class="x_content">

            <?php if (Yii::$app->user->can('admin')) : ?>

                <?= Html::button(
                    '<i class="fa fa-clone"></i> Clone Dari',
                    [
                        'class' => 'btn btn-success',
                        'data-toggle' => 'modal',
                        'data-target' => '#cloneModal'
                    ]
                ) ?>
                <?= Html::button(
                    '<i class="fa fa-upload"></i> Upload Data',
                    [
                        'class' => 'btn btn-primary',
                        'data-toggle' => 'modal',
                        'data-target' => '#uploadModal'
                    ]
                ) ?>

                | Download Template File <?= Html::a(Yii::t('app', 'Disini'), ['template-download'], ['target' => '_blank']) ?>

                <br><br>
            <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="questionOne">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="false" aria-controls="answerOne">
                            <i class="fa fa-filter"></i> Filter <small style="color:blue">* Klik di sini untuk filter data</small>
                        </a>
                    </h5>
                </div>
                <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                    <div class="panel-body">
                        <form action="<?= Yii::$app->urlManager->createUrl(['indikator/index']) ?>" method="get">
                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label for="">Kriteria</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-kriteria_id',
                                            'name' => 'kriteria_id[]',
                                            'data' => $listKriteria,
                                            'value' => $_GET['kriteria_id'] ?? [],
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Kriteria')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="">Satuan Kerja</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-unit_id',
                                            'name' => 'unit_id[]',
                                            'data' => $listAuditee,
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Satuan kerja')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>

                                    </div>
                                </div>

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label for="">Jenjang</label>
                                        <?= Select2::widget([
                                            'id' => 'select2-jenjang_id',
                                            'name' => 'jenjang_id[]',
                                            'data' => $listJenjang,
                                            'options' => ['placeholder' => Yii::t('app', 'Pilih Jenjang')],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'multiple' => true,
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <?= Html::submitButton('Apply Filter', ['class' => 'btn btn-primary']) ?>
                                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-reset', 'type' => 'reset']) ?>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <?php if (Yii::$app->user->can('admin')) : ?>
                <div class="alert alert-info">
                    <b><?= Yii::t('app', 'Note:') ?></b>
                    <ul>
                        <li>Hanya instrumen atau indikator yang periode AMI-nya berstatus aktif yang dapat ditambahkan, diinput, atau diedit.</li>
                        <li>Sebelum mengunggah dokumen, pastikan seluruh <?= Html::a('kode kriteria', ['kriteria/index'], ['style' => 'color:yellow;', 'target' => '_blank']) ?> telah terisi dengan lengkap.</li>
                        <li>Dokumen dapat diinputkan melalui unggahan data Excel yang sesuai dengan template yang telah disediakan.</li>
                        <li>Jika instrumen memiliki bobot yang sama, perubahan dapat dilakukan langsung tanpa perlu mengklik tombol update (ikon pensil).</li>
                    </ul>
                </div>
            <?php endif; ?>

            <?php
            $gridColumns = [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'contentOptions' => ['class' => 'kartik-sheet-style'],
                    'width' => '36px',
                    'pageSummary' => 'Total',
                    'pageSummaryOptions' => ['colspan' => 6],
                    'header' => '',
                    'headerOptions' => ['class' => 'kartik-sheet-style']
                ],
                [
                    'attribute' => 'kriteria_id',
                    'label' => 'Kriteria',
                    'filter' => $listKriteria,
                    'value' => function ($data) {
                        return (!empty($data) ? $data->kriteria->nama : null);
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'nama',
                    'label' => 'Nama Standar',
                    'readonly' => !Yii::$app->user->can('admin'),
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'pernyataan',
                    'readonly' => !Yii::$app->user->can('admin'),
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'indikator',
                    'refreshGrid' => true,
                    'readonly' => !Yii::$app->user->can('admin'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'referensi',
                    'readonly' => !Yii::$app->user->can('admin'),
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'strategi',
                    'refreshGrid' => true,
                    'readonly' => !Yii::$app->user->can('admin'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'label' => 'Jenjang',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $jenjang = '';

                        $data = JenjangBobot::find()->where([
                            'indikator_id' => $data->id,
                        ])->all();

                        foreach ($data as $k => $v) {
                            $jenjang .= ' ' . Html::tag('span', $v->jenjang->nama, ['class' => 'label label-primary']);
                        }

                        return $jenjang;
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'unit_id',
                    'refreshGrid' => true,
                    'readonly' => !Yii::$app->user->can('admin'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => $listAuditee,
                    ],
                    'value' => function ($model) {
                        return $model->unit->nama ?? null;
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'filter' => MyHelper::getStatusAktif(),
                    'refreshGrid' => true,
                    'attribute' => 'status_aktif',
                    'readonly' => !Yii::$app->user->can('admin'),
                    'editableOptions' => [
                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data' => MyHelper::getStatusAktif(),
                    ],
                    'value' => function ($data) {
                        return MyHelper::setStatusAktif($data->status_aktif) ?? null;
                    }
                ],
                [
                    'format' => 'raw',
                    'visible' => Yii::$app->user->can('admin'),
                    'value' => function ($data) {
                        $eye = Html::a('', ['view', 'id' => $data->id], ['class' => 'glyphicon glyphicon-eye-open']);
                        $pencil = Html::a('', 'javascript:void(0)', ['class' => 'glyphicon glyphicon-pencil update_indikator', 'data-instrumen' => $data->id]);
                        $trash = Html::a('', ['delete', 'id' => $data->id], [
                            'class' => 'glyphicon glyphicon-trash',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                        return $eye . ' ' . $pencil . ' ' . $trash;
                    },
                ],
                // ['class' => 'yii\grid\ActionColumn']
            ]; ?>
            <p>
                <?php
                // Renders a export dropdown menu
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'clearBuffers' => true, //optional
                ]);
                ?>
            </p>
            <?= GridView::widget([
                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'activePageCssClass' => 'active paginate_button page-item',
                    'disabledPageCssClass' => 'disabled paginate_button',
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'nextPageCssClass' => 'paginate_button next page-item',
                    'prevPageCssClass' => 'paginate_button previous page-item',
                    'firstPageCssClass' => 'first paginate_button page-item',
                    'lastPageCssClass' => 'last paginate_button page-item',
                    'maxButtonCount' => 10,
                    'linkOptions' => [
                        'class' => 'page-link'
                    ]
                ],
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'containerOptions' => ['style' => 'overflow: auto'],
                'beforeHeader' => [
                    [
                        'columns' => [
                            ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                        ],
                        'options' => ['class' => 'skip-export']
                    ]
                ],
                'exportConfig' => [
                    GridView::PDF => ['label' => 'Save as PDF'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                    GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                    GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                ],

                'toolbar' =>  [
                    '{export}',

                    '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                // set export properties
                'export' => [
                    'fontAwesome' => true
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                    'options' => [
                        'id' => 'pjax-container',
                    ]
                ],
                'id' => 'my-grid',
                'bordered' => true,
                'striped' => true,
                // 'condensed' => false,
                // 'responsive' => false,
                'hover' => true,
                // 'floatHeader' => true,
                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],
            ]); ?>
        </div>
    </div>
</div>

<!-- </div> -->


<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Import Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/indikator/upload" method="post" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="file" name="file" id="file" class="" required>
                            </div>

                        </div>
                    </div>

                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'keseluruhan data yang ada pada file akan di upload!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="cloneModal" tabindex="-1" role="dialog" aria-labelledby="cloneModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cloneModalLabel">Clone dari Tahun</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <form action="" method="post" id="form-clone">
                <div class="modal-body">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="">Tahun Audit Mutu Internal</label>
                            <?= Select2::widget([
                                'id' => 'ami_id',
                                'name' => 'ami_id',
                                'data' => $listAmi,
                                'options' => ['placeholder' => Yii::t('app', 'Pilih Tahun Ami')],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ]); ?>
                        </div>

                    </div>

                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'keseluruhan indikator aktif pada tahun yang dipilih akan di cloning!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="cloneButton"><i class="fa fa-clone"></i> Clone Data</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>
<form action="" id="form-krs">
    <div class="form-group">
        <label for="">Kriteria</label>
        <?= Html::hiddenInput('id', '', ['class' => 'form-control', "id" => "id"]) ?>
        <?= Html::dropDownList('kriteria_id', '', $listKriteria, ['class' => 'form-control', "id" => "kriteria_id"]) ?>
    </div>
    <div class="form-group">
        <label for="">Nama Standar</label>
        <?= Html::textInput('nama', '', ['class' => 'form-control', "id" => "nama"]) ?>
    </div>

    <div class="form-group">
        <label for="">Pernyataan standar</label>
        <?= Html::textarea('pernyataan', '', ['class' => 'form-control', "id" => "pernyataan"]) ?>
    </div>

    <div class="form-group">
        <label for="">Indikator</label>
        <?= Html::textarea('indikator', '', ['class' => 'form-control', "id" => "indikator"]) ?>
    </div>

    <div class="form-group">
        <label for="">Referensi</label>
        <?= Html::textarea('referensi', '', ['class' => 'form-control', "id" => "referensi"]) ?>
    </div>

    <div class="form-group">
        <label for="">Strategi</label>
        <?= Html::textarea('strategi', '', ['class' => 'form-control', "id" => "strategi"]) ?>
    </div>


    <div class="form-group">
        <label for="">Jenjang</label>
        <?= Select2::widget([
            'id' => 'pilih_jenjang',
            'name' => 'jenjang_id[]',
            'data' => $listJenjang,
            'options' => ['placeholder' => Yii::t('app', 'Pilih Jenjang')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ]); ?>
    </div>


    <div class="form-group">

        <?= Html::button('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'btn-simpan']) ?>
    </div>
</form>
<?php
yii\bootstrap\Modal::end();
?>


<?php JSRegister::begin() ?>
<script>
    $('#select2-kriteria_id').val(<?php echo json_encode($_GET['kriteria_id'] ?? [], JSON_NUMERIC_CHECK); ?>).trigger('change');
    $('#select2-unit_id').val(<?php echo json_encode($_GET['unit_id'] ?? [], JSON_NUMERIC_CHECK); ?>).trigger('change');
    $('#select2-jenjang_id').val(<?php echo json_encode($_GET['jenjang_id'] ?? [], JSON_NUMERIC_CHECK); ?>).trigger('change');

    $(document).ready(function() {
        $('#saveButton').click(function(event) {
            event.preventDefault();

            Swal.fire({
                title: 'Caution !',
                text: 'Keseluruhan data pada file akan di upload kedalam sistem! Apakah anda ingin melanjutkan proses input?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#saveButton').removeAttr('disabled');
                    $('#saveButton').closest('form').submit();
                }
                $('#uploadModal').modal('hide');
                $('#file').reset();
            });
        });

        $(".delete-selected").click(function(e) {
            var keys = $('#alat-grid').yiiGridView('getSelectedRows');
            e.preventDefault();

            Swal.fire({
                title: 'Penghapusan Inventaris Aset!',
                text: "Data Inventaris Aset yang dihapus tidak bisa dikembalikan. Anda yakin ingin menghapus data ini?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var obj = new Object;
                    obj.keys = keys;
                    $.ajax({

                        type: "POST",
                        url: "/alat-aset/delete-multiple",
                        data: {
                            dataPost: obj
                        },

                        beforeSend: function() {
                            Swal.fire({
                                title: "Please wait",
                                html: "Processing your request...",
                                allowOutsideClick: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },

                            })
                        },
                        error: function(e) {
                            Swal.close()
                        },
                        success: function(data) {
                            Swal.close()
                            var data = $.parseJSON(data)


                            if (data.code == 200) {
                                Swal.fire({
                                    title: 'Yeay!',
                                    icon: 'success',
                                    text: data.message
                                });

                                $.pjax.reload({
                                    container: '#pjax-container',
                                    async: true
                                });
                            } else {
                                Swal.fire({
                                    title: 'Oops!',
                                    icon: 'error',
                                    text: data.message
                                });

                            }
                        }
                    })
                }
            });
        });
    });

    $(document).on("click", "#cloneButton", function(e) {
        e.preventDefault();

        Swal.fire({
            title: 'Caution !',
            text: 'Data saat ini akan dihapus dan digantikan oleh hasil cloning!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
        }).then((result) => {
            if (result.isConfirmed) {

                Swal.fire({
                    title: 'Mohon Tunggu...',
                    text: 'Data sedang di cloning',
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading();
                    }
                });

                var obj = $("#form-clone").serialize();
                $.ajax({
                    url: "/indikator/ajax-cloning",
                    type: "POST",
                    async: true,
                    data: obj,
                    error: function(e) {
                        Swal.close(); // Menutup animasi loading jika terjadi error
                        console.log(e.responseText);
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: 'Terjadi kesalahan saat melakukan cloning!'
                        });
                    },
                    success: function(data) {
                        Swal.close(); // Menutup animasi loading setelah selesai
                        var hasil = $.parseJSON(data);
                        if (hasil.code == 200) {
                            Swal.fire({
                                title: 'Yeay!',
                                icon: 'success',
                                text: hasil.message
                            }).then(res => {
                                $("#modal-atl").modal("hide");
                                $(":input", "#modal-atl").val("");
                                window.location.reload();
                            });
                        } else {
                            Swal.fire({
                                title: 'Oops!',
                                icon: 'error',
                                text: hasil.message
                            });
                        }
                    }
                });
            }
            $('#uploadModal').modal('hide');
            $('#file').reset();
        });
    });


    $(document).on("click", "#btn-simpan", function(e) {
        e.preventDefault();

        var obj = $("#form-krs").serialize()

        $.ajax({
            url: "/indikator/ajax-add",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.showLoading()
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        $("#modal").modal("hide")
                        $(":input", "#modal").val("")
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });

    $(document).on("click", "#btn-add", function(e) {
        e.preventDefault();
        $("#modal").modal("show")
        $(".bobot_beda").hide()
    });

    $(document).on("click", ".update_indikator", function(e) {
        e.preventDefault();
        var data_instrumen = $(this).data("instrumen")

        $.ajax({
            url: "/indikator/ajax-get-data",
            type: "POST",
            async: true,
            data: {
                id: data_instrumen
            },
            error: function(e) {
                console.log(e.responseText)
            },
            success: function(data) {
                var hasil = $.parseJSON(data)
                var jenjangId = new Array();
                $.each(hasil.jenjang, (k, v) => {
                    jenjangId.push(v.id)
                    $("#nilai_bobot" + v.id).val(v.bobot)
                })

                $("#modal").modal("show")
                $("#id").val(hasil.id)
                $("#kriteria_id").val(hasil.kriteria_id)
                $("#nama").val(hasil.nama)
                $("#pernyataan").val(hasil.pernyataan)
                $("#indikator").val(hasil.indikator)
                $("#referensi").val(hasil.referensi)
                $("#strategi").val(hasil.strategi)

                $("#pilih_jenjang").val(jenjangId).trigger("change")
            }
        })

    });

    $(document).on("click", "#btn-sama", function(e) {
        e.preventDefault();
        $(".bobot_sama").show()
        $(".bobot_beda").hide()
    });

    $(document).on("click", "#btn-beda", function(e) {
        e.preventDefault();
        $(".bobot_beda").show()
        $(".bobot_sama").hide()
    });

    $(document).on("change", "#pilih_jenjang", function(e) {
        e.preventDefault();

        $("#btn-sama").prop("disabled", false);
        $("#btn-beda").prop("disabled", false);
    });

    $(document).on("click", ".close", function(e) {
        e.preventDefault();
        $(":input", "#modal").val("")
    });
</script>
<?php JSRegister::end() ?>

<?php

$this->registerJs(' 


', \yii\web\View::POS_READY);

?>