<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Indikator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>


    <?= $form->field($model, 'kriteria_id')->textInput(['readonly' => true]) ?>
    <?= $form->field($model, 'nama')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'skor1')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'skor2')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'skor3')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'skor4')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'bobot')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
    <?= $form->field($model, 'status_aktif')->radioList(\app\helpers\MyHelper::getStatusAktif()) ?>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>