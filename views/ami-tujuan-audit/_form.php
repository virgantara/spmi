<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmiTujuanAudit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Tujuan audit</label>
            <?= $form->field($model, 'tujuan_audit_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Ami unit</label>
            <?= $form->field($model, 'ami_unit_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Created at</label>
            <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Updated at</label>
            <?= $form->field($model, 'updated_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
