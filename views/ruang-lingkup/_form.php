<?php

use app\models\AmiUnit;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RuangLingkup */
/* @var $form yii\widgets\ActiveForm */


$amiUnit = AmiUnit::find()->joinWith(['ami as a'])->where(['a.status_aktif' => 1])->all();
$listAmiUnit = ArrayHelper::map($amiUnit, 'id', function ($data) {
    return $data->unit->nama;
});
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Ruang lingkup</label>
        <?= $form->field($model, 'ruang_lingkup', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Ami unit</label>
        <?= $form->field($model, 'ami_unit_id', ['options' => ['tag' => false]])
            ->dropDownList(
                $listAmiUnit, 
                ['prompt' => 'Pilih Ami Unit'] 
            )
            ->label(false) ?>
    </div>


    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>