<?php

use app\models\AmiUnit;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RuangLingkupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ruang Lingkups');
$this->params['breadcrumbs'][] = $this->title;

$amiUnit = AmiUnit::find()->joinWith(['ami as a'])->where(['a.status_aktif' => 1])->all();
$listAmiUnit = ArrayHelper::map($amiUnit, 'id', function ($data) {
    return $data->unit->nama;
});
?>
<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Ruang Lingkup'), ['create'], ['class' => 'btn btn-success']) ?>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal"><i class="fa fa-upload"></i> Upload Data</button>
                | Download master file <b><?= Html::a(Yii::t('app', 'disini'), ['download-master'], ['target' => '_blank']) ?></b>

                <br><br>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'ruang_lingkup',
                        'vAlign' => 'middle',
                        'refreshGrid' => true,
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_TEXT,

                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'ami_unit_id',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'refreshGrid' => true,
                        'editableOptions' => [
                            'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                            'data' => $listAmiUnit,
                        ],
                        'value' => function ($model) use ($listAmiUnit) {
                            return $listAmiUnit[$model->ami_unit_id];
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Import Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form action="/ruang-lingkup/upload" method="post" id="form-upload" enctype="multipart/form-data">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form_validation',
                    ]
                ]); ?>

                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="file" name="file" id="file" class="" required>
                            </div>

                        </div>
                    </div>

                    <p style="padding-left: 3%;"><strong><?= Yii::t('app', 'keseluruhan data yang ada pada file akan di upload!') ?></strong></p>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary waves-effect" id="saveButton"><i class="fa fa-save"></i> Submit File</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>

                <?php ActiveForm::end(); ?>
            </form>
        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).ready(function() {
        $('#form-upload').submit(function(e) {
            e.preventDefault();

            // Tampilkan pesan konfirmasi SweetAlert
            Swal.fire({
                title: 'Apakah data sudah sesuai?',
                text: 'Pastikan Anda telah memeriksa data dengan benar sebelum mengirimkan!',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya, Kirim!',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    // Jika dikonfirmasi, submit form secara manual
                    $(this).off("submit").submit();
                }
            });
        });
    });
</script>
<?php JSRegister::end() ?>