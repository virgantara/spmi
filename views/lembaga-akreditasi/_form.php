<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LembagaAkreditasi */
/* @var $form yii\widgets\ActiveForm */
$list_tingkat = \app\helpers\MyHelper::listTingkat();
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>

            <?= $form->field($model, 'tingkat')->radioList($list_tingkat) ?>

            <?= $form->field($model, 'nama_lembaga')->textInput(['class'=>'form-control','maxlength' => true]) ?>
            <?= $form->field($model, 'singkatan_lembaga')->textInput(['class'=>'form-control','maxlength' => true]) ?>
            <?= $form->field($model, 'status_aktif')->radioList(['1' => 'Aktif','2' => 'Non-Aktif']) ?>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
