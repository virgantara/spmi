<?php

use app\helpers\MyHelper;
use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmanahKinerjaUnitKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $amanahKinerja->nama . ' - ' . $unitKerja->nama ?? '';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="x_content">

                <p>
                    <?php

                    $url = Yii::$app->user->can('admin')
                        ? ['amanah-kinerja/view', 'id' => $amanahKinerja->id]
                        : ['amanah-kinerja/index'];

                    echo Html::a('<i class="fa fa-reply"></i> ' . Yii::t('app', 'Kembali'), $url, ['class' => 'btn btn-primary']);
                    ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    // 'amanah_kinerja_id',
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'label' => 'Nama Satuan Kerja',
                    //     'filter' => $listUnitKerja,
                    //     'contentOptions'    => [
                    //         'width' => '30%',
                    //     ],
                    //     'refreshGrid' => true,
                    //     'attribute' => 'unit_kerja_id',
                    //     'readonly' => function ($data) {
                    //         if ($data->jenis_amanah_kinerja == 'satker')
                    //             return !Yii::$app->user->can('admin');

                    //         return true;
                    //     },
                    //     'editableOptions' => [
                    //         'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    //         'data' => $listUnitKerja,
                    //         'options' => [
                    //             'placeholder' => 'Pilih status',
                    //         ],
                    //     ],
                    //     'value' => function ($data) use ($listUnitKerja) {
                    //         return $listUnitKerja[$data->unit_kerja_id] ?? '-';
                    //     }
                    // ],
                    [
                        'attribute' => 'amanah_kinerja_master_id',
                        'label' => 'Amanah Kinerja',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '60%',
                        ],
                        'value' => function ($model) {
                            return $model->amanahKinerjaMaster->amanah_kinerja ?? '-';
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'target',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '15%',
                        ],
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 4,
                                'placeholder' => 'Masukkan target',
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->target ?? 'Belum ada';
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'capaian',
                        'format' => 'ntext',
                        'contentOptions'    => [
                            'width' => '15%',
                        ],
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                            'options' => [
                                'class' => 'form-control',
                                'rows' => 4,
                                'placeholder' => 'Masukkan target',
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->capaian ?? 'Belum ada';
                        }
                    ],
                    // [
                    //     'class' => 'kartik\grid\EditableColumn',
                    //     'filter' => MyHelper::getStatusAktif(),
                    //     'refreshGrid' => true,
                    //     'contentOptions'    => [
                    //         'width' => '8%',
                    //     ],
                    //     'hAlign' => 'center',
                    //     'attribute' => 'status',
                    //     'readonly' => !Yii::$app->user->can('admin'),
                    //     'editableOptions' => [
                    //         'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    //         'data' => MyHelper::getStatusAktif(),
                    //         'options' => [
                    //             'placeholder' => 'Pilih status',
                    //         ],
                    //     ],
                    //     'value' => function ($data) {
                    //         return MyHelper::getStatusAktif()[$data->status] ?? '-';
                    //     }
                    // ],
                    //'status',
                    //'created_at',
                    //'updated_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'visible' => Yii::$app->user->can('admin')
                    ],
                    [
                        'label' => '',
                        'value' => function ($model) {
                            return ''; // Kosongkan value
                        },
                        'contentOptions' => [
                            'class' => 'text-center',
                            'style' => 'width: 1px;', // Atur lebar kolom sesuai kebutuhan
                        ],
                    ],
                ]; ?>
                <?= GridView::widget([
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'activePageCssClass' => 'active paginate_button page-item',
                        'disabledPageCssClass' => 'disabled paginate_button',
                        'prevPageLabel' => 'Previous',
                        'nextPageLabel' => 'Next',
                        'firstPageLabel' => 'First',
                        'lastPageLabel' => 'Last',
                        'nextPageCssClass' => 'paginate_button next page-item',
                        'prevPageCssClass' => 'paginate_button previous page-item',
                        'firstPageCssClass' => 'first paginate_button page-item',
                        'lastPageCssClass' => 'last paginate_button page-item',
                        'maxButtonCount' => 10,
                        'linkOptions' => [
                            'class' => 'page-link'
                        ]
                    ],
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                        'options' => [
                            'id' => 'pjax-container',
                        ]
                    ],
                    'id' => 'my-grid',
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>