<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AmanahKinerjaUnitKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>



        <div class="form-group">
            <label class="control-label">Id</label>
            <?= $form->field($model, 'id',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Amanah kinerja</label>
            <?= $form->field($model, 'amanah_kinerja_id',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Amanah kinerja master</label>
            <?= $form->field($model, 'amanah_kinerja_master_id',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Unit kerja</label>
            <?= $form->field($model, 'unit_kerja_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Target</label>
            <?= $form->field($model, 'target',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Capaian</label>
            <?= $form->field($model, 'capaian',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Status</label>
            <?= $form->field($model, 'status',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Created at</label>
            <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <div class="form-group">
            <label class="control-label">Updated at</label>
            <?= $form->field($model, 'updated_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
        </div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
