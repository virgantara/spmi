<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CapaianKinerjaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="capaian-kinerja-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sasaran_stategis') ?>

    <?= $form->field($model, 'no_indikator') ?>

    <?= $form->field($model, 'indikator_kinerja') ?>

    <?= $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'target_unida') ?>

    <?php // echo $form->field($model, 'bidang_wr') ?>
    
    <?php // echo $form->field($model, 'status_aktif') ?> 

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
