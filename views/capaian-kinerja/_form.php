<?php

use app\helpers\MyHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CapaianKinerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="control-label">Sasaran stategis *</label>
        <?= $form->field($model, 'sasaran_stategis', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'required' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Masukkan sasaran strategis ')])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Nomor indikator *</label>
        <?= $form->field($model, 'no_indikator', ['options' => ['tag' => false]])->textInput(['type' => 'number', 'min' => 1, 'required' => true, 'placeholder' => Yii::t('app', 'Masukkan nomor indikator ')])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Indikator kinerja *</label>
        <?= $form->field($model, 'indikator_kinerja', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'required' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Masukkan indikator kinerja ')])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Satuan *</label>
        <?= $form->field($model, 'satuan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'required' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Masukkan satuan ')])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Target unida *</label>
        <?= $form->field($model, 'target_unida', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'required' => true, 'maxlength' => true, 'placeholder' => Yii::t('app', 'Masukkan target unida ')])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Target Jawaban *</label>
        <?= $form->field($model, 'ket_jawaban')->widget(Select2::classname(), [
            'data'    => MyHelper::getTargetJawaban(),
            'options' => ['placeholder' => Yii::t('app', '- Target Jawaban -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
    </div>

    <div class="form-group">
        <label class="control-label">Bidang wr *</label>
        <?= $form->field($model, 'bidang_wr')->widget(Select2::classname(), [
            'data'    => MyHelper::getBidangRektor(),
            'options' => ['placeholder' => Yii::t('app', '- Bidang WR -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
    </div>
    <small>* Harap melengkapi keseluruhan data</small> <br>

    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>