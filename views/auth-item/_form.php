<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\rbac\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Name</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'name', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>


        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Type</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'type', ['options' => ['tag' => false]])->textInput()->label(false) ?>


        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Description</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'description', ['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>


        </div>
    </div>
    <?= $form->field($model, 'rule_name', ['options' => ['tag' => false]])->hiddenInput(['class' => 'form-control', 'maxlength' => true])->label(false) ?>
    <?= $form->field($model, 'data', ['options' => ['tag' => false]])->hiddenInput(['rows' => 6])->label(false) ?>
    <?= $form->field($model, 'created_at', ['options' => ['tag' => false]])->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'updated_at', ['options' => ['tag' => false]])->hiddenInput()->label(false) ?>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>