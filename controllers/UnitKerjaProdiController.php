<?php

namespace app\controllers;

use Yii;
use app\models\UnitKerjaProdi;
use app\models\UnitKerjaProdiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UnitKerjaProdiController implements the CRUD actions for UnitKerjaProdi model.
 */
class UnitKerjaProdiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UnitKerjaProdi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnitKerjaProdiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionAjaxProdiSatker()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $prodiSatker = UnitKerjaProdi::find()->where([
            'unit_kerja_id' => $dataPost['unit_kerja_id'],
            'prodi_id'      => $dataPost['prodi_id']    ,
        ])->one();
        
        if (empty($prodiSatker)) {
            $prodiSatker = new UnitKerjaProdi();
            $prodiSatker->attributes = $dataPost;
            if ($prodiSatker->save()) {
                $results = [
                    'code'      => 200,
                    'message'   => 'data berhasil ditambah',
                ];
            }else {
                $error = \app\helpers\MyHelper::logError($prodiSatker);
                $results = [
                    'code'      => 500,
                    'message'   => $error,
                ];
            }
        }else {
            if($dataPost['checked'] == '0')
            {
                $prodiSatker->delete();
                $results = [
                    'code' => 200,
                    'message' => 'Bagian berhasil dihapus'
                ];
            }else {
                $error = \app\helpers\MyHelper::logError($prodiSatker);
                $results = [
                    'code'      => 500,
                    'message'   => $error,
                ];
            }
        }

        echo json_encode($results);
        exit;
    }


    /**
     * Displays a single UnitKerjaProdi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UnitKerjaProdi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UnitKerjaProdi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UnitKerjaProdi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UnitKerjaProdi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UnitKerjaProdi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UnitKerjaProdi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UnitKerjaProdi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
