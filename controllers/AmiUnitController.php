<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use Yii;
use app\models\AmiUnit;
use app\models\AmiUnitSearch;
use app\models\Auditor;
use app\models\AuthAssignment;
use app\models\Persetujuan;
use app\models\PersetujuanCek;
use app\models\UnitKerja;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * AmiUnitController implements the CRUD actions for AmiUnit model.
 */
class AmiUnitController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'set-selesai', 'ajax-add-auditee', 'ajax-dokumen',],
                'rules' => [

                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete',
                            'ajax-add-auditee',
                            'set-selesai',
                            'ajax-dokumen'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin'],
                    ],
                    [
                        'actions' => [
                            'set-selesai'
                        ],
                        'allow' => true,
                        'roles' => ['auditor'],
                    ],
                    [
                        'actions' => [
                            'ajax-dokumen'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmiUnit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmiUnitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmiUnit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $list_periode = ArrayHelper::map(Ami::find()->all(), 'id', 'nama');
        $list_unit = ArrayHelper::map(UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');

        $list_auditor = AmiAuditor::findAll(['ami_id' => $id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'list_periode' => $list_periode,
            'list_unit' => $list_unit,
            'list_auditor' => $list_auditor,
        ]);
    }

    public function actionSetSelesai($id)
    {
        $amiUnit = AmiUnit::findOne($id);
        $amiUnit->status_ami = 1;
        $amiUnit->save();
        Yii::$app->session->setFlash('success', "Data tersimpan");
        return $this->redirect(['asesmen/ami', 'id' => $amiUnit->id]);
    }

    /**
     * Creates a new AmiUnit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmiUnit();

        if ($model->load(Yii::$app->request->post())) {
            $ami = AmiUnit::find()->all();
            if (!empty($ami)) {
                $ami = AmiUnit::find()->orderBy(['id' => SORT_DESC])->one();
                $model->id = $ami->id + 1;
            } else {
                $model->id = 1;
            }

            $model->status_ami = 0;

            $check_unit = AmiUnit::find()->where([
                'ami_id' => $model->ami_id,
                'unit_id' => $model->unit_id,
            ])->one();

            $n_ami = Ami::findOne($model->ami_id);

            $n_unit = UnitKerja::findOne($model->unit_id);

            $jenis_unit = MyHelper::listJenisUnit();

            $model->jenis_unit = $jenis_unit[$n_unit->jenis];

            if (empty($check_unit)) {
                $model->save();
                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', "$n_unit->nama telah terdaftar pada $n_ami->nama");
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionAjaxAddAuditee()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            // echo '<pre>';print_r($dataPost);die;
            if (!empty($dataPost)) {

                $ami_unit = AmiUnit::findOne($dataPost['ami_unit_id']);

                $ami_unit->auditee = $dataPost['nama'];
                $ami_unit->email_auditee = $dataPost['email'];
                $ami_unit->save();

                $user = User::findOne([
                    'unit_kerja_id' => $ami_unit->unit->id,
                ]);

                if (empty($user)) {
                    $user = new User;
                    // $user->auditor_id = $model->id;
                }

                $password = MyHelper::getRandomString(8, 8, true, false, false);

                $user->username = $ami_unit->id . "_" . $dataPost['email'];
                // echo '<pre>';print_r($user->username);die;
                $user->email = $ami_unit->unit->email;
                $user->nama = $ami_unit->unit->nama;
                $user->unit_kerja_id = $ami_unit->unit->id;

                $user->setPassword($password);
                $user->status = User::STATUS_ACTIVE;
                $user->access_role = 'auditee';

                $auth = Yii::$app->authManager;
                $oldRole = $auth->getRole('auditee');

                $user->access_role = $oldRole->name;
                // echo '<pre>';print_r($user);die;


                if ($user->save()) {
                    $auth->assign($oldRole, $user->id);

                    $to = MyHelper::sendEmail($user->email);

                    $emailTemplate = $this->renderPartial('email', [
                        'user' => $user,
                        'password' => $password
                    ]);

                    Yii::$app->mailer->compose()
                        ->setTo($to)
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                        ->setSubject('[SIMUDA] Account Information')
                        ->setHtmlBody($emailTemplate)
                        ->send();


                    $toBpm = MyHelper::emailBpm();
                    Yii::$app->mailer->compose()
                        ->setTo($toBpm)
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                        ->setSubject('[SIMUDA] Account Information')
                        ->setHtmlBody($emailTemplate)
                        ->send();
                } else {
                    throw new \Exception(MyHelper::logError($user));
                }


                $transaction->commit();
                $results = [
                    'code' => 200,
                    'message' => 'Data successfully added'
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxAdd()
    {
        $results = [
            'code' => 500,
            'message' => 'Gagal memproses permintaan'
        ];

        if (Yii::$app->request->isAjax) {
            $dataPost = Yii::$app->request->post('dataPost');
            if ($dataPost && isset($dataPost['unit_id'])) {
                $ami = Ami::findOne(['status_aktif' => 1]);
                $auditee = UnitKerja::findOne($dataPost['unit_id']);
                $jenisUnit = MyHelper::listJenisUnit();

                if ($ami) {
                    $amiUnit = AmiUnit::findOne(['ami_id' => $ami->id, 'unit_id' => $dataPost['unit_id']]);

                    try {
                        if ($amiUnit) {
                            if ($amiUnit->delete()) {
                                $results = [
                                    'code' => 200,
                                    'message' => $amiUnit->unit->nama . ' berhasil dihapus dari ' . $amiUnit->ami->nama
                                ];
                            }
                        } else {
                            $model = new AmiUnit();
                            $model->ami_id = $ami->id;
                            $model->unit_id = $dataPost['unit_id'];
                            $model->jenis_unit = $jenisUnit[$auditee->jenis];
                            $model->wakil_auditee = $auditee->penanggung_jawab;
                            $model->penanggung_jawab = $auditee->penanggung_jawab;
                            $model->status_ami = 0;
                            if ($model->save()) {

                                PersetujuanCek::inputDokumenTervalidasi($model->id);
                                $results = [
                                    'code' => 200,
                                    'message' => $model->unit->nama . ' berhasil ditambahkan ke ' . $model->ami->nama
                                ];
                            } else {
                                $results['code'] = 500;
                                $results['message'] = 'Gagal menambahkan data';
                                $results['errors'] = $model->getErrors(); // Ambil pesan kesalahan dari model
                            }
                        }
                    } catch (\Exception $e) {
                        $results['message'] = 'Terjadi kesalahan: ' . $e->getMessage();
                    } catch (\Throwable $e) {
                        $results['message'] = 'Terjadi kesalahan: ' . $e->getMessage();
                    }
                } else {
                    $results['message'] = 'AMI tidak ditemukan';
                }
            } else {
                $results['message'] = 'Data unit tidak valid';
            }
        } else {
            $results['message'] = 'Permintaan bukan merupakan permintaan Ajax';
        }

        echo json_encode($results);
        exit;
        // Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // return $results;
    }


    public function actionAjaxAddAll()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $results = [
            'code' => 500,
            'message' => 'Gagal memproses permintaan'
        ];

        if (!Yii::$app->request->isAjax) {
            $results['message'] = 'Permintaan bukan merupakan permintaan Ajax';
            return $results;
        }

        $dataPost = Yii::$app->request->post('dataPost');
        if (!$dataPost) {
            $results['message'] = 'Data unit tidak valid';
            return $results;
        }

        $ami = Ami::findOne(['status_aktif' => 1]);
        if (!$ami) {
            $results['message'] = 'AMI tidak ditemukan';
            return $results;
        }

        try {
            if ($dataPost['isChecked'] === 'false') {
                $amiUnits = AmiUnit::findAll(['ami_id' => $ami->id]);
                foreach ($amiUnits as $amiUnit) {
                    if (!$amiUnit->delete()) {
                        $results['message'] = 'Gagal menghapus unit: ' . $amiUnit->unit->nama;
                        return $results;
                    }
                }
                $results = [
                    'code' => 200,
                    'message' => 'Semua unit berhasil dihapus dari ' . $ami->nama
                ];
            } else {
                $auditees = UnitKerja::find()->where(['jenis' => ['satker', 'prodi', 'fakultas']])->all();
                $jenisUnit = MyHelper::listJenisUnit();
                $countData = 0;

                foreach ($auditees as $auditee) {
                    $model = AmiUnit::findOne(['ami_id' => $ami->id, 'unit_id' => $auditee->id]);
                    if (empty($model)) {
                        $model = new AmiUnit();
                    } else
                        $countData--;
                        
                    $model->ami_id = $ami->id;
                    $model->unit_id = $auditee->id;
                    $model->jenis_unit = $jenisUnit[$auditee->jenis] ?? null;
                    $model->wakil_auditee = $auditee->penanggung_jawab;
                    $model->penanggung_jawab = $auditee->penanggung_jawab;
                    $model->status_ami = 0;

                    if ($model->save()) {
                        PersetujuanCek::inputDokumenTervalidasi($model->id);
                        $countData++;
                    } else {
                        $results['message'] = 'Gagal menambahkan data untuk unit: ' . $auditee->nama;
                        $results['errors'] = $model->getErrors();
                        return $results;
                    }
                }

                $results = [
                    'code' => 200,
                    'message' => $countData . ' data berhasil dibuat'
                ];
            }
        } catch (\Exception $e) {
            $results['message'] = 'Terjadi kesalahan: ' . $e->getMessage();
        } catch (\Throwable $e) {
            $results['message'] = 'Terjadi kesalahan: ' . $e->getMessage();
        }

        echo json_encode($results);
        exit;
    }


    public function actionAjaxUpdateTanggal()
    {
        $results = [];
        $errors = '';
        $dataPost = $_POST;

        $amiUnit = AmiUnit::findOne($dataPost['ami_unit_id']);

        if ($amiUnit) {
            $amiUnit->tanggal_ami = $dataPost['tanggal_ami'];
            if ($amiUnit->save()) {
                $results = [
                    'code' => 200,
                    'message' => 'Tanggal berhasil diupdate'
                ];
            } else {
                $errors .= MyHelper::logError($amiUnit);
                $results = [
                    'code' => 500,
                    'message' => $errors
                ];
            }
        } else {
            $results = [
                'code' => 500,
                'message' => 'Ami Unit tidak ditemukan'
            ];
        }

        echo json_encode($results);
        exit;
    }


    public function actionAjaxDokumen()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $dataPost = $_POST;

        try {
            // echo '<pre>';print_r($dataPost);die;

            if (!empty($dataPost)) {

                $ami_unit = AmiUnit::findOne($dataPost['ami_unit_id']);

                if (isset($ami_unit)) {
                    $ami_unit->attributes = $dataPost;

                    if ($ami_unit->save()) {
                        # code...
                        $transaction->commit();

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully updated'
                        ];
                    } else {
                        $errors .= MyHelper::logError($ami_unit);
                        throw new \Exception;
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }


    /**
     * Updates an existing AmiUnit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AmiUnit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AmiUnit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AmiUnit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmiUnit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelAuthAssignment($id)
    {
        if (($model = AuthAssignment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
