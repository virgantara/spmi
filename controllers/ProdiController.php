<?php

namespace app\controllers;

use Yii;
use app\helpers\MyHelper;
use app\models\Prodi;
use app\models\ProdiSearch;
use app\models\Akreditasi;
use app\models\AkreditasiSearch;
use app\models\StatusAkreditasi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;


/**
 * ProdiController implements the CRUD actions for Prodi model.
 */
class ProdiController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'ajax-import'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['fakultas', 'tatausaha', 'prodi', 'operatorCabang', 'auditor']
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'view', 'ajax-import'],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }




    /**
     * Lists all Prodi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProdiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Prodi::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Prodi']);
            $post = ['Prodi' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        $list_lembaga_akreditasi = \app\models\LembagaAkreditasi::find()->where(['status_aktif' => '1'])->orderBy(['tingkat' => SORT_ASC])->all();

        return $this->render('index', [
            'list_lembaga_akreditasi' => $list_lembaga_akreditasi,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Prodi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new AkreditasiSearch();
        $searchModel->namaProdi = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listLembagaAkreditasi = \app\models\LembagaAkreditasi::find()->where(['status_aktif' => '1'])->orderBy(['tingkat' => SORT_ASC])->all();

        $getLastAkreditasi = Akreditasi::find()->where(['prodi_id' => $id])->orderBy(['urutan' => SORT_DESC])->one();

        if (empty($getLastAkreditasi)) $getLastAkreditasi = 1;
        else $getLastAkreditasi = (int)$getLastAkreditasi->urutan + 1;

        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');
            $model = Akreditasi::findOne($id);
            $out = json_encode(['output' => '', 'message' => '']);
            $posted = current($_POST['Akreditasi']);
            $post = ['Akreditasi' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listLembagaAkreditasi' => $listLembagaAkreditasi,
            'getLastAkreditasi' => $getLastAkreditasi,
        ]);
    }


    /**
     * Updates an existing Prodi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionAjaxCountUpcomingExpired()
    {
        $results = [];
        if (!empty($_POST['dataPost'])) {
            $dataPost = $_POST['dataPost'];
            $query = new \yii\db\Query();
            $query->select(['COUNT(*) as total']);
            $query->from(['prodi p']);
            $query->join('JOIN', 'akreditasi a', 'a.prodi_id = p.id');
            $query->join('JOIN', 'lembaga_akreditasi la', 'a.lembaga_akreditasi_id = la.id');
            $query->where(['la.tingkat' => '1']);
            $query->where('a.tanggal_kadaluarsa BETWEEN CURDATE() AND CURDATE() + INTERVAL ' . $dataPost['jumlah_hari'] . ' DAY');
            $results = $query->one();
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxCountAkreditasiProdi()
    {
        $results = [];

        $status_akreditasi = StatusAkreditasi::find()->all();
        foreach ($status_akreditasi as $status) {
            $results[$status->kode] = 0;
        }

        $prodis = Prodi::find()->all();

        foreach ($prodis as $prodi) {
            $akreditasi = Akreditasi::find()
                ->andWhere(['prodi_id' => $prodi->id])
                ->andWhere(['<=', 'tanggal_sk', new \yii\db\Expression('NOW()')])
                ->andWhere(['>=', 'tanggal_kadaluarsa', new \yii\db\Expression('NOW()')])
                ->orderBy(['urutan' => SORT_DESC])
                ->one();

            if (isset($akreditasi)) {
                $results[$akreditasi->status_akreditasi]++;
            }
        }

        $hasil = [];
        foreach ($results as $key => $value) {
            $hasil[] = [
                'akreditasi' => $key,
                'total' => $value
            ];
        }

        echo json_encode($hasil);
        exit;
    }

    public function actionAjaxListAkreditasiProdi()
    {
        $results = [];
        if (!empty($_POST['dataPost'])) {
            $dataPost = $_POST['dataPost'];

            $prodis = Prodi::find()->all();
            $status_akreditasi = ArrayHelper::map(StatusAkreditasi::find()->all(), 'kode', 'nama_id');

            foreach ($prodis as $prodi) {
                $akreditasi = Akreditasi::find()->where(['prodi_id' => $prodi->id])->orderBy(['urutan' => SORT_DESC])->one();
                if (!empty($akreditasi)) {
                    if ($akreditasi->status_akreditasi == $dataPost['akreditasi']) {
                        $results[] = [
                            'id' => $prodi->id,
                            'nama_prodi' => $prodi->nama_prodi,
                            'nomor_sk' => $akreditasi->nomor_sk,
                            'tanggal_sk' => $akreditasi->tanggal_sk,
                            'tanggal_kadaluarsa' => $akreditasi->tanggal_kadaluarsa,
                            'akreditasi' => $status_akreditasi[$akreditasi->status_akreditasi],
                        ];
                    }
                }
            }
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxImport()
    {
        $results = [];
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token' => $client_token];

        $response = $client->get('/f/prodi/list', [], $headers)->send();
        $list_dosen = [];
        $list_target_rasio = [];
        $list_jenjang = MyHelper::listJenjangStudi();
        if ($response->isOk) {
            $counter = 0;
            $list_fak = $response->data['values'];
            foreach ($list_fak as $fak) {

                foreach ($fak['items'] as $p) {

                    $m = Prodi::findOne([
                        'kode_prodi' => $p['kode_prodi']
                    ]);

                    if (empty($m)) {
                        $m = new Prodi;
                        $m->kode_prodi = $p['kode_prodi'];
                    }

                    $jenjang = $list_jenjang[$p['kode_jenjang_studi']]['singkatan'];

                    $m->nama_prodi = $p['nama_prodi'];
                    $m->kode_jenjang = $jenjang;
                    $m->fakultas = $fak['nama_fakultas'];

                    if ($m->save()) {
                        $counter++;
                    } else {
                        $err = MyHelper::logError($m);
                        print_r($err);
                        exit;
                    }
                }
            }

            $results = [
                'code' => 200,
                'message' => $counter . ' data successfully imported'
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Finds the Prodi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prodi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prodi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
