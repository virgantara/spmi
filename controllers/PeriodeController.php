<?php

namespace app\controllers;

use app\models\Asesmen;
use app\models\CapaianHasil;
use app\models\CapaianKinerjaSearch;
use app\models\CapaianPeriode;
use Yii;
use app\models\Periode;
use app\models\PeriodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PeriodeController implements the CRUD actions for Periode model.
 */
class PeriodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Periode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeriodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Periode::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Periode']);
            $post = ['Periode' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Periode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Periode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Periode();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Periode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Periode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAjaxPeriode()
    {
        $results = [];

        $errors = '';
        $dataPost = $_POST;

        try {

            if (!empty($dataPost)) {

                $model = CapaianPeriode::findOne([
                    'capaian_kinerja_id' => $dataPost['capaian_kinerja_id'],
                    'periode_id' => $dataPost['periode_id']
                ]);

                if ($dataPost['keperluan'] == 'input') {
                    if (empty($model))
                        $model = new CapaianPeriode();

                    $model->attributes = $dataPost;

                    if ($model->save()) {

                        $results = [
                            'code' => 200,
                            'message' =>  'List capaian berhasil ditambahkan'
                        ];
                    } else {
                        $errors = \app\helpers\MyHelper::logError($model);
                        // throw new \Exception;
                        $results = [
                            'code' => 500,
                            'message' =>  $errors
                        ];
                    }
                } elseif ($dataPost['keperluan'] == 'hapus') {

                    if (!empty($model)) {

                        if ($model->delete()) {
                            $results = [
                                'code' => 200,
                                'message' =>  'Data list capaian berhasil di hapus dari periode'
                            ];
                        } else {
                            $errors = \app\helpers\MyHelper::logError($model);
                            // throw new \Exception;
                            $results = [
                                'code' => 500,
                                'message' =>  $errors
                            ];
                        }
                    }
                }
            }

            echo json_encode($results);
            exit;
        } catch (\Throwable $th) {
            // throw $th;
        }
    }


    /**
     * Finds the Periode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Periode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Periode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
