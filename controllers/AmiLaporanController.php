<?php

namespace app\controllers;

use Yii;
use app\models\AmiLaporan;
use app\models\AmiLaporanSearch;
use app\models\AmiUnit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * AmiLaporanController implements the CRUD actions for AmiLaporan model.
 */
class AmiLaporanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmiLaporan models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $amiUnit = AmiUnit::findOne($id);
        $filters = [
            'ami_unit_id' => $amiUnit->id
        ];
        $searchModel = new AmiLaporanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        
        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = AmiLaporan::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['AmiLaporan']);
            $post = ['AmiLaporan' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }


        return $this->render('index', [
            'amiUnit' => $amiUnit,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpload()
    {
        $errors = [];
        $connection = \Yii::$app->db;
        $transcation = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];

        $s3new = new \Aws\S3\S3Client($s3config);
        $errors = [];

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if (!empty($file)) {
                $amiUnit = AmiUnit::findOne($_POST['ami_unit_id']);

                $model = new AmiLaporan();
                $model->path_gambar = $file;
                $model->attributes = $_POST;

                if ($model->path_gambar) {
                    $curdate = date('d-m-y');

                    $file_name = $curdate . '-' . $file->name;
                    $s3path = $file->tempName;
                    $s3type = $file->type;
                    $key = 'laporan' . '/' . $amiUnit->ami->nama . '/' . $amiUnit->unit->nama . '/' . $file_name;

                    $insert = $s3new->putObject([
                        'Bucket' => 'spmi',
                        'Key' => $key,
                        'Body' => 'Body',
                        'SourceFile' => $s3path,
                        'ContenType' => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('spmi', $key);
                    $model->path_gambar = $plainUrl;
                }

                if ($model->save()) {
                    $transcation->commit();

                    Yii::$app->session->setFlash('success', "Data dipebarui");
                    return $this->redirect(['index', 'id' => $amiUnit->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    /**
     * Displays a single AmiLaporan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AmiLaporan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmiLaporan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AmiLaporan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AmiLaporan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect([
            'index',
            'id' => $model->ami_unit_id
        ]);
    }

    public function actionDownload($id)
    {
        $model = AmiLaporan::findOne($id);
        $file_path = $model->path_gambar;

        // Mendapatkan ekstensi file
        $file_extension = pathinfo($file_path, PATHINFO_EXTENSION);

        // Set jenis konten sesuai dengan ekstensi file
        $content_type = '';
        switch ($file_extension) {
            case 'png':
                $content_type = 'image/png';
                break;
            case 'jpg':
            case 'jpeg':
                $content_type = 'image/jpeg';
                break;
            default:
                // Handle jika jenis file tidak sesuai
                // Misalnya, tampilkan pesan kesalahan atau lakukan tindakan lain
                break;
        }

        if ($content_type !== '') {
            $file = file_get_contents($file_path);
            $filename = basename(parse_url($model->path_gambar, PHP_URL_PATH));

            header('Content-type: ' . $content_type);
            header('Content-Disposition: inline; filename="' . $filename . '"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . strlen($file));
            header('Accept-Ranges: bytes');
            echo $file;
            exit;
        } else {
            // Handle jika jenis file tidak didukung (bukan PNG atau JPG)
            // Misalnya, tampilkan pesan kesalahan atau lakukan tindakan lain
        }
    }

    /**
     * Finds the AmiLaporan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AmiLaporan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmiLaporan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
