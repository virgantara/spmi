<?php

namespace app\controllers;

use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Asesmen;
use app\models\Indikator;
use Yii;
use app\models\Tilik;
use app\models\TilikSearch;
use app\models\UnitKerja;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TilikController implements the CRUD actions for Tilik model.
 */
class TilikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'deletee'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'view', 'deletee'],
                        'allow' => true,
                        'roles' => ['auditor', 'auditee']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tilik models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $amiUnit = AmiUnit::findOne($id);

        $searchModel    = new TilikSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->queryParams, $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Tilik::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Tilik']);
            $post = ['Tilik' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        $auditors = AmiAuditor::find()->where(['ami_id' => $_GET['id']])->all();
        $auditorData = [];
        foreach ($auditors as $value) {
            // $data = $value->auditor_id;
            $data = [
                'id'    => $value->auditor_id,
                'nama'  => $value->auditor->nama,
            ];
            $auditorData[] = $data;
        }

        return $this->render('index', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'auditorData' => $auditorData,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCetak($ami_unit_id)
    {
        $results = [];
        $ami_unit = AmiUnit::findOne($ami_unit_id);

        try {
            ob_start();
            $this->layout = '';
            ob_start();
            $auditor_ketua = AmiAuditor::find()->orderBy(['status_id' => SORT_ASC])->where([
                'ami_id' => $ami_unit_id,
                'status_id' => 1,
            ])->one();

            $auditor = AmiAuditor::find()->orderBy(['status_id' => SORT_ASC])->where([
                'ami_id' => $ami_unit_id,
            ])->all();

            $tilik = Tilik::find();
            $tilik->joinWith(['asesmen.amiUnit as au', 'asesmen.amiUnit.ami as aaa']);
            $tilik->where([
                'au.unit_id' => $ami_unit->unit_id,
                'aaa.status_aktif' => 1
            ]);
            $tilik = $tilik->all();

            $indikator = Indikator::find()->where([
                'jenis_indikator' => 1,
            ])->all();

            echo $this->renderPartial('cetak_tilik', [
                'results' => $results,
                'nama_unit' => $ami_unit->unit->nama,
                'tahun' => $ami_unit->ami->tahun,
                'auditor' => $auditor,
                'amiUnit' => $ami_unit,
                'tilik' => $tilik,
                'indikator' => $indikator,
                'auditor_ketua' => $auditor_ketua,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            // echo '<pre>';print_r($fontpath);die;
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $pdf->Image($imgdata, 20, 13, 14);
            $pdf->writeHTML($data);
            $pdf->Output('Daftar tilik_' . $ami_unit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }


    public function actionDaftarTilik($id)
    {
        $tilik = Tilik::find()->all();

        // echo '<pre>';print_r($tilik);exit;

        return $this->render('daftar_tilik', [
            'tilik' => $tilik,
            'id' => $id,
        ]);
    }

    /**
     * Displays a single Tilik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tilik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {

        $model = new Tilik();
        // echo '<pre>';print_r($id);exit;

        $asesmen = Asesmen::findOne($id);

        $ami_unit = AmiUnit::findOne($asesmen->ami_unit_id);
        $ami = Ami::findOne($ami_unit->ami_id);
        $satker = UnitKerja::findOne($ami_unit->unit_id);
        $indikator = Indikator::findOne($asesmen->indikator_id);


        $label = "$ami->nama - $satker->nama | $indikator->nama";

        if ($model->load(Yii::$app->request->post())) {

            // $model->asesmen_id = $id;
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['asesmen/link-ak', 'id' => $id]);
        }

        return $this->render('create', [
            'model' => $model,
            'id' => $id,
            'label' => $label,
        ]);
    }

    /**
     * Updates an existing Tilik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $asesmen = Asesmen::findOne($id);

        $ami_unit = AmiUnit::findOne($asesmen->ami_unit_id);
        $ami = Ami::findOne($ami_unit->ami_id);
        $satker = UnitKerja::findOne($ami_unit->unit_id);
        $indikator = Indikator::findOne($asesmen->indikator_id);


        $label = "$ami->nama - $satker->nama | $indikator->nama";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // echo '<pre>';print_r($label);exit;

        return $this->render('update', [
            'model' => $model,
            'id' => $id,
            'label' => $label,
        ]);
    }

    /**
     * Deletes an existing Tilik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletee($id, $asesmen_id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['asesmen/link-ak', 'id' => $asesmen_id]);
    }

    /**
     * Finds the Tilik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tilik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tilik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
