<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\BahanRtm;
use app\models\BahanRtmSearch;
use app\models\Kriteria;
use Yii;
use app\models\Rtm;
use app\models\RtmSearch;
use app\models\UnitKerja;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * RtmController implements the CRUD actions for Rtm model.
 */
class RtmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rtm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Rtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);
            // echo '<pre>';print_r($_POST);

            $posted = current($_POST['Rtm']);
            $post = ['Rtm' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionBahan()
    {
        $searchModel = new RtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bahan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rtm model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $filters = ['rtm_id' => $id];
        $searchBahanModel = new BahanRtmSearch();
        $dataBahanProvider = $searchBahanModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchBahanModel' => $searchBahanModel,
            'dataBahanProvider' => $dataBahanProvider,
        ]);
    }

    public function actionRapatTinjauan($id)
    {
        $searchBahanModel = new BahanRtmSearch();
        $dataBahanProvider = $searchBahanModel->search(Yii::$app->request->queryParams);

        return $this->render('rapat_tinjauan', [
            'model' => $this->findModel($id),
            'searchBahanModel' => $searchBahanModel,
            'dataBahanProvider' => $dataBahanProvider,
        ]);
    }

    public function actionTindakLanjut($id)
    {
        $searchBahanModel = new BahanRtmSearch();
        $dataBahanProvider = $searchBahanModel->search(Yii::$app->request->queryParams);

        return $this->render('tindak_lanjut', [
            'model' => $this->findModel($id),
            'searchBahanModel' => $searchBahanModel,
            'dataBahanProvider' => $dataBahanProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Rtm();

        $ami = Ami::findOne(['status_aktif' => 1]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'ami' => $ami,
        ]);
    }

    public function actionCreateRtm()
    {
        $ami = Ami::findOne(['status_aktif' => 1]);

        if (empty($ami)) {
            Yii::$app->session->setFlash('danger', "Tidak ada siklus periode AMI yang aktif");
            return $this->redirect(['index']);
        }

        $fakultas = UnitKerja::findAll(['jenis' => 'fakultas']);
        $warek = UnitKerja::findAll(['jenis' => 'koordinator']);
        $satker = UnitKerja::findAll(['singkatan' => ['bpm', 'senat', 'yptd', 'rektorat']]);
        $universitas = UnitKerja::findAll(['jenis' => 'universitas']);
        $rtm = Rtm::find()->joinWith(['ami as a'])->where(['a.status_aktif' => 1])->all();
        $dataRtm = [];
        foreach ($rtm as $value) {
            // $dataRtm[$value->unit_kerja_id] = $value;
            $dataRtm[$value->unit_kerja_id] = $value->unit_kerja_id;
        }

        return $this->render('create_rtm', [
            'dataRtm' => $dataRtm,
            'ami' => $ami,
            'fakultas' => $fakultas,
            'warek' => $warek,
            'satker' => $satker,
            'universitas' => $universitas,
        ]);
    }


    public function actionAjaxAdd()
    {
        $results = [
            'code' => 500,
            'message' => 'Gagal memproses permintaan'
        ];

        if (Yii::$app->request->isAjax) {
            $dataPost = Yii::$app->request->post('dataPost');

            if (isset($dataPost['unit_id'])) {
                $ami = Ami::findOne(['status_aktif' => 1]);
                $unitKerja = UnitKerja::findOne($dataPost['unit_id']);

                if ($dataPost['isChecked'] == 'true') {
                    $model = new Rtm();

                    $model->ami_id = $ami->id;
                    $model->unit_kerja_id = $unitKerja->id;
                    $model->nama_kepala = $unitKerja->penanggung_jawab;

                    if ($model->save()) {
                        $results = [
                            'code' => 200,
                            'message' => 'RTM ' . $unitKerja->nama . ' berhasil ditambahkan ke sikus ' . $ami->nama
                        ];
                    } else {
                        $results['code'] = 500;
                        $results['message'] = 'Gagal menambahkan data';
                        $results['errors'] = $model->getErrors();
                    }
                } else {
                    $model = Rtm::deleteAll(['ami_id' => $ami->id, 'unit_kerja_id' => $dataPost['unit_id']]);
                    if ($model) {
                        $results = [
                            'code' => 200,
                            'message' => 'RTM ' . $unitKerja->nama . ' berhasil dihapus ke sikus ' . $ami->nama
                        ];
                    }
                }
            } else {
                $results['message'] = 'Data unit tidak valid';
            }
        } else {
            $results['message'] = 'Permintaan bukan merupakan permintaan Ajax';
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing Rtm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $ami = Ami::findOne(['status_aktif' => 1]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'ami' => $ami,
        ]);
    }


    public function actionDekan()
    {
        $searchModel = new RtmSearch();
        $jenis = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Rtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('dekan', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWakilRektor()
    {
        $searchModel = new RtmSearch();
        $jenis = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Rtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('wakil_rektor', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUniversitas()
    {
        $searchModel = new RtmSearch();
        $jenis = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Rtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('universitas', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'dataProvider' => $dataProvider,
        ]);
    }

    // public function actionAjaxAdd()
    // {

    //     $results = [];

    //     $errors = '';
    //     $connection = \Yii::$app->db;
    //     $transaction = $connection->beginTransaction();

    //     try {
    //         $dataPost = $_POST['Rtm'];

    //         if (!empty($dataPost)) {

    //             if (empty($dataPost['id'])) $model = new Rtm;
    //             else $model = Rtm::findOne($dataPost['id']);

    //             $model->attributes = $dataPost;

    //             if ($model->save()) {

    //                 $transaction->commit();

    //                 $results = [
    //                     'code' => 200,
    //                     'message' => 'Data successfully added/updated'
    //                 ];
    //             } else {
    //                 $errors .= MyHelper::logError($model);
    //                 throw new \Exception;
    //             }
    //         } else {
    //             $errors .= 'Oops, you cannot POST empty data';
    //             throw new \Exception;
    //         }
    //     } catch (\Exception $e) {
    //         $transaction->rollBack();
    //         $errors .= $e->getMessage();
    //         $results = [
    //             'code' => 500,
    //             'message' => $errors
    //         ];
    //     } catch (\Throwable $e) {
    //         $transaction->rollBack();
    //         $errors .= $e->getMessage();
    //         $results = [
    //             'code' => 500,
    //             'message' => $errors
    //         ];
    //     }

    //     echo json_encode($results);
    //     exit;
    // }

    public function actionAjaxGetData()
    {
        $dataPost = $_POST;
        $out = [];

        $rtm = Rtm::findOne($dataPost['id']);

        // $out['id'] = $rtm->id;
        // $out['bidang'] = $rtm->bidang;
        // $out['nama_kepala'] = $rtm->nama_kepala;
        // $out['tahun'] = $rtm->tahun;
        // $out['ketua_panitia'] = $rtm->ketua_panitia;
        // $out['tema'] = $rtm->tema;

        $out = $rtm;

        echo \yii\helpers\Json::encode($out);

        die();
    }


    /**
     * Deletes an existing Rtm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        BahanRtm::deleteAll(['rtm_id'  => $id]);
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', "Data RTM berhasil dihapus");
        return $this->redirect(['index']);
    }

    /**
     * Finds the Rtm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rtm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rtm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
