<?php

namespace app\controllers;

use Yii;
use app\models\AmanahKinerjaMaster;
use app\models\AmanahKinerjaMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\UnitKerja;
use yii\helpers\ArrayHelper;

/**
 * AmanahKinerjaMasterController implements the CRUD actions for AmanahKinerjaMaster model.
 */
class AmanahKinerjaMasterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete'],
                'rules' => [
                    [
                        'actions' => [
                            'view', 'index', 'delete', 'create', 'update'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmanahKinerjaMaster models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmanahKinerjaMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listUnitKerja = ArrayHelper::map(UnitKerja::find(['jenis' => 'satker'])->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            $id = Yii::$app->request->post('editableKey');
            $model = AmanahKinerjaMaster::findOne($id);

            $out = json_encode(['output' => '', 'message' => '']);
            $posted = current($_POST['AmanahKinerjaMaster']);
            $post = ['AmanahKinerjaMaster' => $posted];

            if ($model->load($post)) {
                if ($model->jenis_amanah_kinerja == 'fakultas')
                    $model->unit_kerja_id = null;

                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listUnitKerja' => $listUnitKerja,
        ]);
    }

    /**
     * Displays a single AmanahKinerjaMaster model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AmanahKinerjaMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmanahKinerjaMaster();

        if ($model->load(Yii::$app->request->post())) {
            $model->id = MyHelper::gen_uuid();
            if ($model->jenis_amanah_kinerja == 'satker' && $model->unit_kerja_id == null) {
                Yii::$app->session->setFlash('danger', "Satuan kerja belum dipilih");
                return $this->redirect(['index']);
            }
            if ($model->jenis_amanah_kinerja == 'fakultas')
                $model->unit_kerja_id = null;


            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AmanahKinerjaMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AmanahKinerjaMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AmanahKinerjaMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AmanahKinerjaMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmanahKinerjaMaster::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
