<?php

namespace app\controllers;

use Yii;
use app\models\JenjangMap;
use app\models\JenjangMapSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * JenjangMapController implements the CRUD actions for JenjangMap model.
 */
class JenjangMapController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JenjangMap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JenjangMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JenjangMap model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JenjangMap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JenjangMap();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxJenjangMap()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $jenjangMap = JenjangMap::find()->where([
            'unit_kerja_id' => $dataPost['unit_kerja_id']
        ])->one();
        
        #check data
        // echo '<pre>';print_r(!empty($jenjangMap));die;
        if (!empty($jenjangMap)) {
            $jenjangMap->attributes = $dataPost;
            if($jenjangMap->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Jenjang Prodi Berhasil di Update'
                ];
            }
        }else {
            $jenjangMap = new JenjangMap();
            $jenjangMap->attributes = $dataPost;
            if($jenjangMap->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'App Role Added'
                ];
            }

            else
            {
                $error = \app\helpers\MyHelper::logError($jenjangMap);
                $results = [
                    'code' => 500,
                    'message' => $error
                ];
            }
        }

        echo json_encode($results);
        
        exit;
    }

    /**
     * Updates an existing JenjangMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JenjangMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JenjangMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JenjangMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JenjangMap::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
