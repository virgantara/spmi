<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Prodi;
use app\models\Akreditasi;
use Firebase\JWT\JWT;

class SiteController extends Controller
{

    public $successUrl = '';
    /**
     * Returns a list of behaviors that this component should behave as.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['logout', 'signup', 'testing', 'kompetensi'],
                'rules' => [
                    [
                        'actions' => [
                            'testing',
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'baakdata'],
                    ],
                    [
                        'actions' => ['signup', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Declares external actions for the controller.
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'successUrl' => $this->successUrl

            ],
        ];
    }

    public function beforeAction($action)
    {

        return true; // or false to not run the action
    }

    // public function actionAuthCallback()
    // {
    //     $results = [];

    //     try {
    //         $request_method = $_SERVER["REQUEST_METHOD"];

    //         switch ($request_method) {
    //             case 'POST':
    //                 $token = $_SERVER['HTTP_X_JWT_TOKEN'];
    //                 $key = Yii::$app->params['jwt_key'];
    //                 $decoded = JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
    //                 $results = [
    //                     'code' => 200,
    //                     'message' => 'Valid'
    //                 ];
    //                 break;
    //             default:
    //                 header("HTTP/1.0 405 Method Not Allowed");
    //                 exit;
    //                 break;
    //         }
    //     } catch (\Exception $e) {

    //         $results = [
    //             'code' => 500,
    //             'message' => $e->getMessage()
    //         ];
    //     }

    //     header('Content-Type: application/json');
    //     echo json_encode($results);

    //     die();
    // }

    public function actionLogoutSessionCallback()
    {

        // $input = json_decode(file_get_contents('php://input'),true);
        // header('Content-type:application/json;charset=utf-8');

        $results = [];

        try {
            $session = Yii::$app->session;
            $session->remove('token');
            Yii::$app->user->logout();

            $results = [
                'code' => 200,
                'message' => 'Valid SIAKAD ' . $session->get('token')
            ];
        } catch (\Exception $e) {

            $results = [
                'code' => 500,
                'message' => $e->getMessage()
            ];
        }

        echo json_encode($results);

        die();
    }

    public function actionAuthCallback()
    {
        try {
            $accessToken = Yii::$app->request->get('access_token');
            $refreshToken = Yii::$app->request->get('refresh_token');

            Yii::$app->tokenService->handleAuthCallback($accessToken, $refreshToken);

            return $this->redirect(['site/index']);
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    protected function handleException($e)
    {
        Yii::$app->session->setFlash('danger', $e->getMessage());
        return $this->redirect(['site/index']);
    }

    public function actionCallback()
    {
        try {
            $receivedJwt = Yii::$app->request->get('state');
            $authCode = Yii::$app->request->get('code');

            Yii::$app->tokenService->handleCallback($receivedJwt, $authCode);

            return $this->redirect(['site/index']);
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    public function actionLoginSso($token)
    {
        try {
            $key = Yii::$app->params['jwt_key'];
            $decoded = JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);

            $uuid = $decoded->uuid;
            $user = \app\models\User::find()
                ->where(['uuid' => $uuid])
                ->one();

            if ($user !== null) {
                $user->otp = null;
                // $user->otp_expire = null;
                $user->save(false);

                Yii::$app->session->set('token', $token);
                Yii::$app->user->login($user);

                return $this->redirect(['site/index']);
            } else {
                return $this->redirect($decoded->iss . '/site/sso-callback?code=302')->send();
            }
        } catch (\Exception $e) {
            Yii::error("Error during SSO login: " . $e->getMessage());
            throw new \yii\web\UnauthorizedHttpException("Invalid SSO token.");
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionKinerja()
    {
        return $this->render('kinerja');
    }
    public function actionDashboard()
    {
        $query = Akreditasi::find();
        $query->where('tanggal_sk <= NOW() AND tanggal_kadaluarsa >= NOW()');
        $query->orderBy(['tanggal_sk' => SORT_ASC]);
        $akreditasi = $query->all();

        $list_akreditasi = [];

        foreach ($akreditasi as $item) {
            $list_akreditasi[$item->lembaga_akreditasi_id . $item->prodi_id] = $item;
        }

        usort($list_akreditasi, function ($a, $b) {
            $tanggalA = strtotime($a->tanggal_kadaluarsa);
            $tanggalB = strtotime($b->tanggal_kadaluarsa);

            if ($tanggalA == $tanggalB) {
                return 0;
            }

            return ($tanggalA < $tanggalB) ? -1 : 1;
        });

        return $this->render('dashboard', [
            'akreditasi' => $list_akreditasi,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */

    public function actionLogout()
    {

        $session = Yii::$app->session;
        $session->remove('token');
        Yii::$app->user->logout();
        $url = Yii::$app->params['sso_logout'];
        return $this->redirect($url);
        // return $this->goHome();
    }

    public function actionAuthSso()
    {
        $session = Yii::$app->session;
        
        $state = bin2hex(random_bytes(16));
        
        $secretKey = Yii::$app->params['jwt_key'];

        $payload = [
            'iss' => Yii::$app->params['oauth']['redirectUri'],         // Issuer
            'iat' => time(),                   // Issued at
            'exp' => time() + 300,             // Expiration (5 minutes)
            'csrf_token' => bin2hex(random_bytes(16)) // Unique CSRF token
        ];

        $jwt = JWT::encode($payload, $secretKey, 'HS256');
        $session->set('oauth_state',$jwt);

        // print_r($_SESSION['oauth_state']);
        $clientId = Yii::$app->params['oauth']['client_id'];
        $redirectUri = urlencode(Yii::$app->params['oauth']['redirectUri']);
        $scope = urlencode('read write');

        $authUrl = Yii::$app->params['oauth']['baseurl']."/oauth/login?"
            . "client_id={$clientId}"
            . "&redirect_uri={$redirectUri}"
            . "&response_type=code"
            . "&scope={$scope}"
            . "&grant_type=password"
            . "&state={$jwt}";

        return $this->redirect($authUrl);
    }
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
