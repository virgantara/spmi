<?php

namespace app\controllers;

use Yii;
use app\models\Monev;
use app\models\MonevSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\UnitKerja;

/**
 * MonevController implements the CRUD actions for Monev model.
 */
class MonevController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'master'],
                'rules' => [
                    [
                        'actions' => [
                            'detail',
                            'master',
                            'index'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete',
                            'detail',
                            'master'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monev models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMaster()
    {
        $searchModel = new MonevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            $id = Yii::$app->request->post('editableKey');
            $model = Monev::findOne($id);

            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Monev']);
            $post = ['Monev' => $posted];

            if ($model->load($post)) {

                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('master', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Monev model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $unitKerja = UnitKerja::find()->where(['jenis' => ['satker', 'fakultas']])->orderBy(['nama' => SORT_ASC])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'unitKerja' => $unitKerja,
        ]);
    }

    /**
     * Creates a new Monev model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monev();

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $unitKerjas = UnitKerja::find()->all();

                $listUnitKerja = [];
                foreach ($unitKerjas as $unitKerja) {
                    $listUnitKerja[] = [
                        'id' => MyHelper::gen_uuid(),
                        'unit_kerja_id' => $unitKerja->id,
                        'monev_id' => $model->id,
                    ];
                }
                // echo '<pre>';print_r($listUnitKerja);exit;

                if (!empty($listUnitKerja)) {
                    Yii::$app->db->createCommand()->batchInsert('monev_unit_kerja', [
                        'id',
                        'unit_kerja_id',
                        'monev_id',
                    ], $listUnitKerja)->execute();
                }

                $transaction->commit();

                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['master']);
            }
        } catch (\Throwable $th) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Monev model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Monev model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Monev model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Monev the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monev::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
