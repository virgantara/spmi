<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiLaporan;
use app\models\AmiRuangLingkup;
use app\models\AmiRuangLingkupSearch;
use app\models\AmiTujuanAudit;
use app\models\AmiTujuanAuditSearch;
use app\models\AmiUnit;
use Yii;
use app\models\Asesmen;
use app\models\AsesmenSearch;
use app\models\AmiUnitSearch;
use app\models\AsesmenDokumen;
use app\models\Auditor;
use app\models\Indikator;
use app\models\Jadwal;
use app\models\JadwalSearch;
use app\models\JenjangMap;
use app\models\Kriteria;
use app\models\Persetujuan;
use app\models\RuangLingkup;
use app\models\RuangLingkupSearch;
use app\models\Temuan;
use app\models\TemuanSearch;
use app\models\Tilik;
use app\models\TilikSearch;
use app\models\TujuanAuditSearch;
use yii\debug\panels\DumpPanel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AsesmenController implements the CRUD actions for Asesmen model.
 */
class AsesmenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'ami', 'hasil', 'desc', 'tilik', 'temuan', 'cetak-berita', 'cetak-tilik', 'cetak-temuan', 'penilaian', 'ajax-input-led', 'ajax-input-ak', 'ajax-input-al', 'ajax-input', 'ajax-input-tilik', 'ajax-input-temuan', 'link-led', 'link-ak', 'link-al'],
                'rules' => [

                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete',
                            'ajax-add-auditee',
                            'set-selesai',
                            'ajax-dokumen'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin'],
                    ],
                    [
                        'actions' => [
                            'ami',
                            'hasil',
                            'desc',
                            'tilik',
                            'temuan',
                            'cetak-berita',
                            'cetak-tilik',
                            'cetak-temuan',
                            'penilaian',
                            'ajax-input-led',
                            'ajax-input-ak',
                            'ajax-input-al',
                            'ajax-input',
                            'ajax-input-tilik',
                            'ajax-input-temuan',
                            'link-led',
                            'link-ak',
                            'link-al',
                        ],
                        'allow' => true,
                        'roles' => ['auditor'],
                    ],
                    [
                        'actions' => [
                            'ami',
                            'hasil',
                            'desc',
                            'tilik',
                            'temuan',
                            'cetak-berita',
                            'cetak-tilik',
                            'cetak-temuan',
                            'penilaian',
                            'ajax-input-led',
                            'ajax-input',
                            'link-led',
                            'link-ak',
                            'link-al',
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Asesmen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AsesmenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMasterInput($id, $jenisPenilaian = null)
    {

        $amiUnit = AmiUnit::findOne($id);

        $asesmen = Asesmen::find()->where([
            'ami_unit_id' => $amiUnit->id,
        ])->one();

        $list_indikator =  [];
        $list_indikator_khusus =  [];

        if (!empty($_GET['kriteria_id'])) {

            $list_indikator = Indikator::find()->where([
                'kriteria_id'       => $_GET['kriteria_id'],
                'status_aktif'      => 1,
                'jenis_indikator'   => 0,
            ]);
            if ($amiUnit->unit->jenis != 'satker') $list_indikator->joinWith(['jenjangBobots as jb'])->andWhere(['jb.jenjang_id' => $amiUnit->unit->jenjangMap->jenjang_id]);
            else $list_indikator->orWhere(['kriteria_id' => $_GET['kriteria_id'], 'jenis_indikator' => 2, 'unit_id' => $amiUnit->unit_id]);

            $list_indikator = $list_indikator->orderBy(['kriteria_id' => SORT_ASC])->all();
        }

        return $this->render('master_input', [
            'jenisPenilaian'        => $jenisPenilaian,
            'asesmen'               => $asesmen,
            'list_indikator'        => $list_indikator,
            'list_indikator_khusus' => $list_indikator_khusus,
            'id'                    => $id,
            'amiUnit'               => $amiUnit,
        ]);
    }

    public function actionAjaxInputMaster()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;

        $dataPost = $_POST;

        try {

            if (!empty($dataPost)) {

                $asesmen = Asesmen::find()->where([
                    'indikator_id' => $dataPost['indikator_id'],
                    'ami_unit_id' => $dataPost['ami_unit_id'],
                ])->one();

                if (!isset($asesmen)) {

                    $model = new Asesmen();
                    $model->attributes = $dataPost;

                    if ($model->save()) {

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                } else {
                    $model = $this->findModel($asesmen->id);
                    $model->attributes = $dataPost;

                    if ($model->save()) {

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully updated'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAmi()
    {
        $filters = [];
        if (!empty($_GET)) {
            $filters = $_GET;
        }
        $searchModel = new AmiUnitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = AmiUnit::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['AmiUnit']);
            $post = ['AmiUnit' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('ami', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMasterLed()
    {
        $amiUnitId = [];
        $amiId = [];

        if (!empty($_GET['ami_unit_id']))
            $amiUnitId = $_GET['ami_unit_id'];

        if (!empty($_GET['ami_id']))
            $amiId = $_GET['ami_id'];

        $searchModel = new AsesmenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $amiUnitId, $amiId);

        return $this->render('master_led', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHasil($id)
    {

        $amiUnit = AmiUnit::findOne($id);

        if ($amiUnit->unit->jenis == "satker") {
            return $this->redirect(['hasil-satker', 'id' => $id]);
        }

        $kriterias = Kriteria::find()
            ->select('kriteria.id')
            ->joinWith([
                'pembagianKriterias as pk'
            ])
            ->where([
                'pk.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)
            ])
            ->all();

        foreach ($kriterias as $kriteria) {
            $datas['id'][] = $kriteria->id;
        }

        $listIndikator = Indikator::find()->where([
            'kriteria_id' => $datas['id'],
            'indikator.status_aktif' => 1,
            'jenis_indikator' => 0,
        ]);
        $listIndikator->joinWith(['ami as a']);
        $listIndikator->andWhere(['a.id' => $amiUnit->ami->id]);

        if ($amiUnit->unit->jenis != 'satker') $listIndikator->joinWith(['jenjangBobots as jb'])->andWhere(['jb.jenjang_id' => $amiUnit->unit->jenjangMap->jenjang_id]);
        else {
            $listIndikator->orWhere(['kriteria_id' => $_GET['kriteria_id'], 'jenis_indikator' => 2, 'unit_id' => $amiUnit->unit_id]);
        }

        $listIndikator = $listIndikator->orderBy(['kriteria_id' => SORT_ASC])->all();

        $paketPenilaian = MyHelper::getPaketPenilaian($amiUnit->id, $datas['id'], $amiUnit->unit->jenjangMap->jenjang_id);

        return $this->render('hasil', [
            'paketPenilaian' => $paketPenilaian,
            'listIndikator' => $listIndikator,
            'id' => $id,
            'amiUnit' => $amiUnit,
        ]);
    }


    public function actionHasilSatker($id)
    {

        echo '<pre>';
        print_r('Dalam proses pengerjaan');
        die;
        $amiUnit = AmiUnit::findOne($id);

        $kriterias = Kriteria::find()
            ->select('kriteria.id')
            ->joinWith([
                'pembagianKriterias as pk'
            ])
            ->where([
                'pk.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)
            ])
            ->all();

        foreach ($kriterias as $kriteria) {
            $datas['id'][] = $kriteria->id;
        }

        $list_indikator = Indikator::find()->where([
            'kriteria_id'       => $datas['id'],
            'status_aktif'      => 1,
            'jenis_indikator'   => 0,
        ]);

        $list_indikator = Indikator::find()->where([
            'kriteria_id'       => $datas['id'],
            'status_aktif'      => 1,
            'jenis_indikator'   => 0,
        ]);

        if ($amiUnit->unit->jenis != 'satker') $list_indikator->joinWith(['jenjangBobots as jb'])->andWhere(['jb.jenjang_id' => $amiUnit->unit->jenjangMap->jenjang_id]);
        else $list_indikator->orWhere(['kriteria_id' => $_GET['kriteria_id'], 'jenis_indikator' => 2, 'unit_id' => $amiUnit->unit_id]);

        $list_indikator = $list_indikator->orderBy(['kriteria_id' => SORT_ASC])->all();

        $paketPenilaian = MyHelper::getPaketPenilaian($amiUnit->id, $datas['id'], $amiUnit->unit->jenjangMap->jenjang_id);

        return $this->render('hasil', [
            'paket_penilaian'       => $paketPenilaian,
            'list_indikator'        => $list_indikator,
            'id'                    => $id,
            'amiUnit'               => $amiUnit,
        ]);
    }

    public function actionTilik($id)
    {
        $ami_unit = AmiUnit::findOne($id);

        $searchModel = new TilikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ami_unit->unit_id);

        return $this->render('tilik', [
            'id' => $id,
            // 'tilik' => $tilik,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTemuan($id)
    {
        $ami_unit = AmiUnit::findOne($id);

        $searchModel = new TemuanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ami_unit->unit_id);

        return $this->render('temuan', [
            'id' => $id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCetakLaporanAmiPerAuditee($ami_unit_id)
    {
        $ami_unit = AmiUnit::findOne($ami_unit_id);

        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SOURCE
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $listTemuan = Temuan::findAll(['ami_unit_id' => $ami_unit_id]);
            $auditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
            ])->andWhere([
                'not',
                ['status_id' => 1]
            ]);
            $listAuditor = $auditor->all();
            $jumlahAuditor = $auditor->count();
            $ketuaAuditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
                'status_id' => 1,
            ])->one();
            $jenjang = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();
            $listSusunan = Jadwal::find()->where(['ami_unit_id' => $ami_unit_id])->orderBy(['urutan' => SORT_ASC])->all();
            $ruangLingkup = RuangLingkup::findAll(['ami_unit_id' => $ami_unit_id]);
            $tujuanAudit = AmiTujuanAudit::findAll(['ami_unit_id' => $ami_unit_id]);
            $listKriteria = Kriteria::find()->where(['is_khusus' => 0])->orderBy(['urutan' => SORT_ASC]);
            $amiBuktiKehadiran = AmiLaporan::find()->orderBy(['urutan' => SORT_ASC])->where(['ami_unit_id' => $amiUnit->id])->all();
            $listKriteria->joinWith(['pembagianKriterias as p'])->andWhere(['p.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)]);

            $dataTemuan = [];
            $temuan = Temuan::find();
            $temuan->joinWith(['asesmen.indikator as ai']);
            foreach ($listKriteria->all() as $key => $kriteria) {
                $data = $temuan->where([
                    'ai.kriteria_id' => $kriteria->id,
                    'ai.ami_id' => $amiUnit->ami_id,
                ])->all();
                $dataRekomendasi = [];
                foreach ($data as $key => $d) {
                    $dataRekomendasi[] = $d;
                }
                $dataTemuan[$kriteria->id] = $dataRekomendasi;
            }

            $atl = Temuan::find();
            $atl->where([
                'ami_unit_id' => $amiUnit->id,
            ]);
            $atl->andWhere(['IS NOT', 'rencana_tindak_lanjut', null])
                ->andWhere(['IS NOT', 'realisasi_tindak_lanjut', null])
                ->andWhere(['IS NOT', 'jadwal_rtl', null]);
            $atl = $atl->all();



            //-- CONFIG
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $logoFull = Yii::getAlias('@webroot') . '/gentela/images/logo-full.png';
            $style = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
            );

            // COVER
            echo $this->renderPartial('laporan_cover', [
                'ketuaAuditor' => $ketuaAuditor,
                'amiUnit' => $amiUnit,
                'jenjang' => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
                'jumlahAuditor' => $jumlahAuditor + 1
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($imgdata, 80, 60, 45);
            $pdf->writeHTML($data);

            // RENCANA AMI
            echo $this->renderPartial('//surat/cetak_rencana', [
                'tujuanAudit' => $tujuanAudit,
                'ruangLingkup' => $ruangLingkup,
                'amiUnit' => $amiUnit,
                'jumlahAuditor' => $jumlahAuditor + 1,
                'listSusunan' => $listSusunan,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($imgdata, 20, 13, 14);
            $pdf->writeHTML($data);

            // Tambahkan gambar dari dokumen PDF eksternal

            // Tentukan koordinat gambar
            $x = 1;
            $y = 10;
            $width = 1050;
            $height = 1350;

            // Tambahkan gambar dari dokumen PDF eksternal ke file PDF yang sedang dibuat

            if (!empty($amiBuktiKehadiran)) {
                foreach ($amiBuktiKehadiran as $key => $bukti) {
                    $pdf->AddPage();
                    $pdf->Image($bukti->path_gambar, $x, $y, $width, $height);
                }
            } else {
                $pdf->AddPage();
                $pdf->SetFont('pala', 'B', 16);
                $pdf->Cell(0, 10, 'Bukti kehadiran tidak tersedia', 0, 1, 'C');
                // Anda dapat menambahkan pesan atau tindakan lain yang sesuai di sini
            }


            // BERITA ACARA
            echo $this->renderPartial('//surat/cetak_berita', [
                'jumlahAuditor' => $jumlahAuditor + 1,
                'listKriteria' => $listKriteria->all(),
                'dataTemuan' => $dataTemuan,
                'amiUnit' => $amiUnit,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($imgdata, 20, 13, 14);
            $pdf->writeHTML($data);

            $lembar = 1;
            $no = 1;
            // TEMUAN
            foreach ($listTemuan as $temuan) {

                echo $this->renderPartial('//temuan/cetak_temuan', [
                    'listTemuan' => $listTemuan,
                    'lembar' => $lembar,
                    'temuan' => $temuan,
                    'amiUnit' => $amiUnit,
                    'jumlahAuditor' => $jumlahAuditor + 1,
                    'no' => $no,
                ]);
                $no++;

                $data = ob_get_clean();
                ob_start();
                $pdf->AddPage();
                if (!empty($temuan)) {
                    # code...
                    $pdf->Image($imgdata, 20, 13, 14);
                    $pdf->writeHTML($data);
                    $style = array(
                        'border' => false,
                        'padding' => 0,
                        'fgcolor' => array(0, 0, 0),
                        'bgcolor' => false, //array(255,255,255)
                    );
                }

                #DATA
                $mulai = 70;
                $jarak = 48;
                $data = [
                    'ami_unit_id' => $amiUnit->id,
                    'dokumen_id'  => 1,
                ];

                ##BARCODE PENANGGUNGJAWAB
                $pj['is_auditee'] = true;
                $dataPj = $data + $pj;
                if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, 105, 0, 22, $style, 'N');

                ##BARCODE KETUA AUDITOR
                $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
                $kAuditor['is_auditee'] = false;
                $dataKetua = $data + $kAuditor;
                if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, 105, 0, 22, $style, 'N');

                ##BARCODE ANGGOTA AUDITOR
                if (isset($listAuditor)) {
                    $position = $mulai + $jarak;
                    foreach ($listAuditor as $auditor) {
                        $aAuditor['auditor_id'] = $auditor->auditor_id;
                        $aAuditor['is_auditee'] = false;
                        $dataAuditor = $data + $aAuditor;
                        if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, 105, 0, 22, $style, 'N');
                        $position = $position + $jarak;
                    }
                }

                $lembar++;
            }

            // AUDIT TINDAK LANJUT

            // echo $this->renderPartial('//temuan/cetak_atl', [
            //     'results' => $results = [],
            //     'nama_unit' => $amiUnit->unit->nama,
            //     'tahun' => $amiUnit->ami->tahun,
            //     'auditor' => $auditor,
            //     'amiUnit' => $amiUnit,
            //     'atl' => $atl,
            //     'auditor_ketua' => $ketuaAuditor,
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);

            $pdf->Output('LaporanAMI_' . $ami_unit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }


    public function actionCetakLaporanAmiPerPeriode($ami_unit_id)
    {
        $ami_unit = AmiUnit::findOne($ami_unit_id);

        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $listTemuan = Temuan::findAll(['ami_unit_id' => $ami_unit_id]);
            $auditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
            ])->andWhere([
                'not',
                ['status_id' => 1]
            ]);
            $listAuditor = $auditor->all();
            $jumlahAuditor = $auditor->count();
            $ketuaAuditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
                'status_id' => 1,
            ])->one();
            $jenjang = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();
            $listTemuan = Temuan::find()->where(['ami_unit_id' => $ami_unit_id])->all();
            $lembar = 1;
            $listKriteria = Kriteria::find();
            $listSusunan = Jadwal::find()->where(['ami_unit_id' => $ami_unit_id])->orderBy(['urutan' => SORT_ASC])->all();
            $listKriteria->joinWith(['pembagianKriterias as pk']);

            if ($amiUnit->unit->jenis == "prodi") {
                $listKriteria
                    ->where(['jenis_unit' => 2])
                    ->orWhere([
                        'jenis_unit' => 1
                    ])
                    ->orderBy(['urutan' => SORT_ASC]);
            }


            //-- SOURCE
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $logoFull = Yii::getAlias('@webroot') . '/gentela/images/logo-full.png';
            $style = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
            );

            //00. COVER
            echo $this->renderPartial('laporan_cover', [
                'ketuaAuditor' => $ketuaAuditor,
                'listTemuan' => $listTemuan,
                'amiUnit' => $amiUnit,
                'jenjang' => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
                'jumlahAuditor' => $jumlahAuditor + 1
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($imgdata, 80, 60, 45);
            $pdf->writeHTML($data);


            //01. PENGANTAR
            echo $this->renderPartial('laporan_pengantar', [
                'amiUnit' => $amiUnit,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->writeHTML($data);


            //02. SURAT TUGAS
            echo $this->renderPartial('//surat/surat_tugas', [
                'amiUnit' => $amiUnit,
                'jumlahAuditor' => $jumlahAuditor + 1
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($logoFull, 78, 10, 50);
            $pdf->writeHTML($data);


            //03. BERITA ACARA
            echo $this->renderPartial('//surat/cetak_berita', [
                'ketuaAuditor' => $ketuaAuditor,
                'listKriteria' => $listKriteria,
                'listTemuan' => $listTemuan,
                'amiUnit' => $amiUnit,
                'jumlahAuditor' => $jumlahAuditor + 1
            ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);
            // #DATA
            // $mulai  = 70;
            // $jarak  = 48;
            // $tinggi = 150;
            // $data = [
            //     'ami_unit_id' => $amiUnit->id,
            //     'dokumen_id'  => 0,
            // ];
            // ##BARCODE PENANGGUNGJAWAB
            // $pj['is_auditee'] = true;
            // $dataPj = $data + $pj;
            // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE KETUA AUDITOR
            // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
            // $kAuditor['is_auditee'] = false;
            // $dataKetua = $data + $kAuditor;
            // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE ANGGOTA AUDITOR
            // if (isset($listAuditor)) {
            //     $position = $mulai + $jarak;
            //     foreach ($listAuditor as $auditor) {
            //         $aAuditor['auditor_id'] = $auditor->auditor_id;
            //         $aAuditor['is_auditee'] = false;
            //         $dataAuditor = $data + $aAuditor;
            //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
            //         $position = $position + $jarak;
            //     }
            // }


            // //04. SUSUNAN ACARA ----
            // echo $this->renderPartial('//jadwal/susunan_acara', [
            //     'amiUnit'       => $amiUnit,
            //     'jumlahAuditor' => $jumlahAuditor + 2,
            //     'listSusunan'   => $listSusunan,
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);


            // //05. DAFTAR HADIR ----
            // echo $this->renderPartial('//surat/kehadiran', [
            //     'amiUnit'       => $amiUnit,
            //     'jumlahAuditor' => $jumlahAuditor + 1,
            //     'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);


            // //06. HASIL AMI ----
            // echo $this->renderPartial('//surat/cetak_hasil_ami', [
            //     'listTemuan'    => $listTemuan,
            //     'amiUnit'       => $amiUnit,
            //     'listKriteria'  => $listKriteria->all(),
            //     'jumlahAuditor' => $jumlahAuditor + 1,
            //     'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 9);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);
            // #DATA
            // $mulai  = 70;
            // $jarak  = 48;
            // $tinggi = 73;
            // $data = [
            //     'ami_unit_id' => $amiUnit->id,
            //     'dokumen_id'  => 1,
            // ];
            // ##BARCODE PENANGGUNGJAWAB
            // $pj['is_auditee'] = true;
            // $dataPj = $data + $pj;
            // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE KETUA AUDITOR
            // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
            // $kAuditor['is_auditee'] = false;
            // $dataKetua = $data + $kAuditor;
            // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE ANGGOTA AUDITOR
            // if (isset($listAuditor)) {
            //     $position = $mulai + $jarak;
            //     foreach ($listAuditor as $auditor) {
            //         $aAuditor['auditor_id'] = $auditor->auditor_id;
            //         $aAuditor['is_auditee'] = false;
            //         $dataAuditor = $data + $aAuditor;
            //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
            //         $position = $position + $jarak;
            //     }
            // }


            // //07. PELUANG PERBAIKAN ----
            // echo $this->renderPartial('laporan_peluang_perbaikan', [
            //     'amiUnit'       => $amiUnit,
            //     'jumlahAuditor' => $jumlahAuditor + 1
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->writeHTML($data);


            // //08. DESKRIPSI TEMUAN ----
            // foreach ($listTemuan as $temuan) {

            //     echo $this->renderPartial('//temuan/cetak_temuan', [
            //         'listTemuan'    => $listTemuan,
            //         'lembar'        => $lembar,
            //         'temuan'        => $temuan,
            //         'amiUnit'       => $amiUnit,
            //         'jumlahAuditor' => $jumlahAuditor + 1
            //     ]);

            //     $data = ob_get_clean();
            //     ob_start();
            //     $pdf->AddPage();
            //     $pdf->Image($imgdata, 20, 13, 14);
            //     $pdf->writeHTML($data);
            //     $style = array(
            //         'border' => false,
            //         'padding' => 0,
            //         'fgcolor' => array(0, 0, 0),
            //         'bgcolor' => false, //array(255,255,255)
            //     );
            //     #DATA
            //     $mulai  = 70;
            //     $jarak  = 48;
            //     $tinggi = 105;
            //     $data = [
            //         'ami_unit_id' => $amiUnit->id,
            //         'dokumen_id'  => 2,
            //     ];
            //     ##BARCODE PENANGGUNGJAWAB
            //     $pj['is_auditee'] = true;
            //     $dataPj = $data + $pj;
            //     if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
            //     ##BARCODE KETUA AUDITOR
            //     $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
            //     $kAuditor['is_auditee'] = false;
            //     $dataKetua = $data + $kAuditor;
            //     if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
            //     ##BARCODE ANGGOTA AUDITOR
            //     if (isset($listAuditor)) {
            //         $position = $mulai + $jarak;
            //         foreach ($listAuditor as $auditor) {
            //             $aAuditor['auditor_id'] = $auditor->auditor_id;
            //             $aAuditor['is_auditee'] = false;
            //             $dataAuditor = $data + $aAuditor;
            //             if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
            //             $position = $position + $jarak;
            //         }
            //     }

            //     $lembar++;
            // }


            // //09. REKOMENDASI MUTU ----
            // echo $this->renderPartial('laporan_rekomendasi', [
            //     'amiUnit'       => $amiUnit,
            //     'listKriteria'  => $listKriteria->all(),
            //     'jumlahAuditor' => $jumlahAuditor + 2
            // ]);

            // $data = ob_get_clean();
            // ob_start();
            // $pdf->SetFont($fontreg, '', 10);
            // $pdf->AddPage();
            // $pdf->Image($imgdata, 20, 13, 14);
            // $pdf->writeHTML($data);
            // #DATA
            // $mulai  = 70;
            // $jarak  = 48;
            // $tinggi = 144;
            // $data = [
            //     'ami_unit_id' => $amiUnit->id,
            //     'dokumen_id'  => 3,
            // ];
            // ##BARCODE PENANGGUNGJAWAB
            // $pj['is_auditee'] = true;
            // $dataPj = $data + $pj;
            // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE KETUA AUDITOR
            // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
            // $kAuditor['is_auditee'] = false;
            // $dataKetua = $data + $kAuditor;
            // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
            // ##BARCODE ANGGOTA AUDITOR
            // if (isset($listAuditor)) {
            //     $position = $mulai + $jarak;
            //     foreach ($listAuditor as $auditor) {
            //         $aAuditor['auditor_id'] = $auditor->auditor_id;
            //         $aAuditor['is_auditee'] = false;
            //         $dataAuditor = $data + $aAuditor;
            //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
            //         $position = $position + $jarak;
            //     }
            // }


            // //09. REKOMENDASI MUTU (ISI) ----
            // echo $this->renderPartial('laporan_rekomendasi_isi', [
            //     'listKriteria'  => $listKriteria->all(),
            // ]);

            $data = ob_get_clean();
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->writeHTML($data);


            $pdf->Output('LaporanAMI_' . $ami_unit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionPenilaian($id, $jenisPenilaian = null)
    {
        if (empty($jenisPenilaian)) {
            Yii::$app->session->setFlash('warning', "Agar memilih opsi input asesmen");
            return $this->redirect(['proses', 'id' => $id]);
        }

        $amiUnit = AmiUnit::findOne($id);

        $filterKriteria = Kriteria::find();

        if ($amiUnit->jenis_unit != 3) {
            $filterKriteria->andWhere(['NOT', ['is_khusus' => true]]);
        }

        if ($amiUnit->ami->status_aktif == true) {
            $filterKriteria->andWhere([
                'status_aktif' => 1
            ]);
        }

        $filterKriteria->joinWith(['pembagianKriterias as p'])
            ->andWhere(['p.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)]);

        $filterKriteria = $filterKriteria->all();

        $asesmen = Asesmen::find()->where([
            'ami_unit_id' => $amiUnit->id,
        ])->one();

        // Show Per Kriteria
        $list_indikator =  [];
        $list_indikator_khusus =  [];

        if (!empty($_GET['kriteria_id'])) {

            $list_indikator = Indikator::find()->where([
                'kriteria_id' => $_GET['kriteria_id'],
                'jenis_indikator' => 0,
                'indikator.status_aktif' => 1,
            ]);
            $list_indikator->joinWith(['ami as a']);
            $list_indikator->andWhere(['a.id' => $amiUnit->ami->id]);
            if ($amiUnit->unit->jenis != 'satker') $list_indikator->joinWith(['jenjangBobots as jb'])->andWhere(['jb.jenjang_id' => $amiUnit->unit->jenjangMap->jenjang_id]);
            $list_indikator = $list_indikator->all();
        }

        if (!empty($_GET['kriteria_khusus_id']) && !empty($amiUnit->unit)) {
            // echo '<pre>';print_r($_GET['kriteria_khusus_id']);die;
            $list_indikator = Indikator::find()->where([
                'unit_id' => $amiUnit->unit->id,
                'kriteria_id' => $_GET['kriteria_khusus_id'],
                'jenis_indikator' => 2,
                'indikator.status_aktif' => 1,
            ]);
            $list_indikator->joinWith(['ami as a']);
            $list_indikator->andWhere(['a.id' => $amiUnit->ami->id]);
            $list_indikator = $list_indikator->all();
        }

        $model = new Asesmen();

        $amiAuditor = AmiAuditor::find()->where([
            'ami_id' => $id,
            'auditor_id' => Yii::$app->user->identity->auditor_id,
        ])->one();

        return $this->render('penilaian', [
            'jenisPenilaian' => $jenisPenilaian,
            'asesmen' => $asesmen,
            'list_indikator' => $list_indikator,
            'list_indikator_khusus' => $list_indikator_khusus,
            'filterKriteria' => $filterKriteria,
            'id' => $id,
            'amiUnit' => $amiUnit,
            'model' => $model,
            'amiAuditor' => $amiAuditor
        ]);
    }

    public function actionInputLed()
    {
        $dataPost   = $_POST;

        //Check Asesmen
        $asesmen    = MyHelper::getAsesmen($dataPost['ami_unit_id'], $dataPost['indikator_id']);

        if (empty($asesmen)) {
            $asesmen    = new Asesmen();
        }

        $dataPost['jenis_dokumen'] == 1 ? $asesmen->dokumen_id  = null : $asesmen->pengelompokan_id  = null;
        $dataPost['jenis_dokumen'] == 1 ? $asesmen->dokumen_id  = $dataPost['link_bukti_tunggal'] : $asesmen->pengelompokan_id  = $dataPost['link_bukti_kelompok'];

        $asesmen->attributes = $dataPost;

        if ($asesmen->save()) {

            Yii::$app->session->setFlash('success', "Data ED berhasil diinputkan");
            return $this->redirect([
                'penilaian',
                'id' => $dataPost['ami_unit_id'],
                'jenisPenilaian' => $dataPost['jenis_penilaian'],
                'kriteria_id' => $dataPost['kriteria_id'],
                'btn-cari' => 1,
            ]);
        }
    }

    public function actionInputAk()
    {
        $dataPost   = $_POST;

        //Check Asesmen
        $asesmen = MyHelper::getAsesmen($dataPost['ami_unit_id'], $dataPost['indikator_id']);

        $ami_auditor = AmiAuditor::find()->where([
            'ami_id' => $dataPost['ami_unit_id'],
            'auditor_id' => Yii::$app->user->identity->auditor_id,
        ])->one();

        switch ($ami_auditor->status_id) {
            case 1:
                $asesmen->skor_ak1 = $dataPost['skor_ak'];
                $asesmen->skor_al1 = $dataPost['skor_ak'];
                break;
            case 2:
                $asesmen->skor_ak2 = $dataPost['skor_ak'];
                $asesmen->skor_al2 = $dataPost['skor_ak'];
                break;
            case 3:
                $asesmen->skor_ak3 = $dataPost['skor_ak'];
                $asesmen->skor_al3 = $dataPost['skor_ak'];
                break;

            default:
                break;
        }

        if ($asesmen->save()) {

            Yii::$app->session->setFlash('success', "Skor Asesmen Kecukupan berhasil diinputkan");
            return $this->redirect([
                'penilaian',
                'id' => $dataPost['ami_unit_id'],
                'jenisPenilaian' => $dataPost['jenis_penilaian'],
                'kriteria_id' => $dataPost['kriteria_id'],
                'btn-cari' => 1,
            ]);
        }
    }

    public function actionInputAl()
    {
        $dataPost   = $_POST;

        //Check Asesmen
        $asesmen    = MyHelper::getAsesmen($dataPost['ami_unit_id'], $dataPost['indikator_id']);

        $ami_auditor = AmiAuditor::find()->where([
            'ami_id'        => $dataPost['ami_unit_id'],
            'auditor_id'    => Yii::$app->user->identity->auditor_id,
        ])->one();

        switch ($ami_auditor->status_id) {
            case 1:
                $asesmen->skor_al1 = $dataPost['skor_al'];
                break;
            case 2:
                $asesmen->skor_al2 = $dataPost['skor_al'];
                break;
            case 3:
                $asesmen->skor_al3 = $dataPost['skor_al'];
                break;

            default:
                break;
        }

        if ($asesmen->save()) {

            Yii::$app->session->setFlash('success', "Skor Asesmen Kecukupan berhasil diinputkan");
            return $this->redirect([
                'penilaian',
                'id'                => $dataPost['ami_unit_id'],
                'jenisPenilaian'    => $dataPost['jenis_penilaian'],
                'kriteria_id'       => $dataPost['kriteria_id'],
                'btn-cari'          => 1,
            ]);
        }
    }

    public function actionAjaxInputTilik()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $dataPost = $_POST;

        try {

            if (!empty($dataPost)) {

                $asesmen = Asesmen::find()->where([
                    'indikator_id' => $dataPost['indikator_id'],
                    'ami_unit_id' => $dataPost['ami_unit_id'],
                ])->one();

                if (!isset($asesmen)) {
                    $asesmen = new Asesmen();
                    $asesmen->indikator_id = $dataPost['indikator_id'];
                    $asesmen->ami_unit_id = $dataPost['ami_unit_id'];

                    if ($asesmen->save()) {
                        # code...
                    } else {
                        $errors .= MyHelper::logError($asesmen);
                        throw new \Exception;
                    }
                }

                $tilik = new Tilik();

                $tilik->attributes = $dataPost;
                $tilik->auditor_id = Yii::$app->user->identity->auditor_id;
                $tilik->asesmen_id = $asesmen->id;


                if ($tilik->save()) {
                    # code...
                    $transaction->commit();

                    $results = [
                        'code' => 200,
                        'message' => 'Data successfully added'
                    ];
                } else {
                    $errors .= MyHelper::logError($tilik);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxInputTemuan()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $dataPost = $_POST;

        try {
            // echo '<pre>';print_r($dataPost);die;

            if (!empty($dataPost)) {

                $asesmen = Asesmen::find()->where([
                    'indikator_id'  => $dataPost['indikator_id'],
                    'ami_unit_id'   => $dataPost['ami_unit_id'],
                ])->one();

                if (empty($asesmen)) {
                    # code...
                    $asesmen = new Asesmen();
                    $asesmen->ami_unit_id   = $dataPost['ami_unit_id'];

                    if ($asesmen->save()) {
                        # code...

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($asesmen);
                        throw new \Exception;
                    }
                }

                $temuan = new Temuan();

                $temuan->attributes = $dataPost;
                $temuan->asesmen_id = $asesmen->id;
                $temuan->rencana_tindak_lanjut = $dataPost['rekomendasi'];

                // echo '<pre>';print_r($temuan);die;


                if ($temuan->save()) {
                    # code...
                    $transaction->commit();

                    $results = [
                        'code' => 200,
                        'message' => 'Data successfully added'
                    ];
                } else {
                    $errors .= MyHelper::logError($temuan);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxGetDetailAsesmen()
    {
        $dataPost = $_POST;
        $out = [];

        $asesmen = Asesmen::findOne(['indikator_id' => $dataPost['indikator_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);
        $dokumen = [];
        $dataTilik = [];

        if (isset($asesmen)) {
            $dokumen = AsesmenDokumen::findAll(['asesmen_id' => $asesmen->id]);
            $dataTilik = Tilik::findAll(['asesmen_id' => $asesmen->id]);
        }

        $indikator = ($asesmen) ? $asesmen->indikator : Indikator::findOne($dataPost['indikator_id']);

        $amiAuditor = AmiAuditor::findOne(['auditor_id' => Yii::$app->user->identity->auditor_id, 'ami_id' => $dataPost['ami_unit_id']]);


        $dataDokumen = [];

        if (!empty($dokumen)) {
            foreach ($dokumen as $d) {
                $dataDokumen[] = [
                    'id' => $d->dokumen->id,
                    'nama' => $d->dokumen->nama,
                    'asesmen_dokumen_id' => $d->id,
                ];
            }
        }

        $skorAk = 'skor_ak' . ($amiAuditor->status_id ?? '1');
        $skorAl = 'skor_al' . ($amiAuditor->status_id ?? '1');
        $out = [
            'id' => $indikator->id,
            'kriteria_id' => $indikator->kriteria_id,
            'kriteria_nama' => $indikator->kriteria->nama . (isset($indikator->kriteria->keterangan) ? ' - ' . $indikator->kriteria->keterangan : ''),
            'nama' => $indikator->nama,
            'ami_unit_id' => $asesmen->ami_unit_id ?? '',
            'pernyataan' => $indikator->pernyataan,
            'indikator' => $indikator->indikator,
            'referensi' => $indikator->referensi,
            'strategi' => $indikator->strategi,
            'bobot' => $indikator->bobot,
            'dokumen' => $dataDokumen,
            'dataTilik' => $dataTilik,
            'skor_ed' => $asesmen->skor_ed ?? '',
            'skor_ak' => $asesmen->$skorAk  ?? '',
            'skor_al' => $asesmen->$skorAl  ?? '',
            'skor_ak1' => $asesmen->skor_ak1 ?? '',
            'skor_ak2' => $asesmen->skor_ak2 ?? '',
            'skor_ak3' => $asesmen->skor_ak3 ?? '',
            'skor_al1' => $asesmen->skor_al1 ?? '',
            'skor_al2' => $asesmen->skor_al2 ?? '',
            'skor_al3' => $asesmen->skor_al3 ?? '',
        ];

        echo \yii\helpers\Json::encode($out);

        die();
    }



    /**
     * Displays a single Asesmen model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Asesmen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Asesmen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSaveSkored()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {
                $asesmen = Asesmen::findOne(['indikator_id' => $dataPost['indikator_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);

                if (empty($asesmen)) {
                    $asesmen = new Asesmen();
                    $asesmen->indikator_id = $dataPost['indikator_id'];
                    $asesmen->ami_unit_id = $dataPost['ami_unit_id'];
                }

                $asesmen->skor_ed = $dataPost['skor_ed'];

                if ($asesmen->save()) {
                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' => 'Evaluasi diri berhasil diperbarui',
                    ];
                } else {
                    $errors .= MyHelper::logError($asesmen);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxSaveSkorak()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {
                $asesmen = Asesmen::findOne(['indikator_id' => $dataPost['indikator_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);

                $amiAuditor = AmiAuditor::findOne(['auditor_id' => Yii::$app->user->identity->auditor_id, 'ami_id' => $asesmen->ami_unit_id]);

                if (empty($asesmen)) {
                    $asesmen = new Asesmen();
                    $asesmen->indikator_id = $dataPost['indikator_id'];
                    $asesmen->ami_unit_id = $dataPost['ami_unit_id'];
                }

                $skor_ak = 'skor_ak' . $amiAuditor->status_id;
                $skor_al = 'skor_al' . $amiAuditor->status_id;

                $asesmen->$skor_ak = $dataPost['skor_ak'];
                $asesmen->$skor_al = $dataPost['skor_ak'];

                if ($asesmen->save()) {
                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' => 'Asesmen kecukupan berhasil diperbarui',
                    ];
                } else {
                    $errors .= MyHelper::logError($asesmen);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxSaveSkoral()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {
                $asesmen = Asesmen::findOne(['indikator_id' => $dataPost['indikator_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);

                $amiAuditor = AmiAuditor::findOne(['auditor_id' => Yii::$app->user->identity->auditor_id, 'ami_id' => $asesmen->ami_unit_id]);

                if (empty($asesmen)) {
                    $asesmen = new Asesmen();
                    $asesmen->indikator_id = $dataPost['indikator_id'];
                    $asesmen->ami_unit_id = $dataPost['ami_unit_id'];
                }

                $skor_al = 'skor_al' . $amiAuditor->status_id;

                $asesmen->$skor_al = $dataPost['skor_al'];

                if ($asesmen->save()) {
                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' => 'Asesmen lapangan berhasil diperbarui',
                    ];
                } else {
                    $errors .= MyHelper::logError($asesmen);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionProses($id)
    {
        if ($_POST) {
            $dataPost = $_POST;

            switch ($dataPost['kriteria_id']) {
                case 'led':
                    return $this->redirect(['laporan-evaluasi-diri', 'id' => $id]);
                    break;

                case 'ak':
                    return $this->redirect(['asesmen-kecukupan', 'id' => $id]);
                    break;

                case 'al':
                    return $this->redirect(['asesmen-lapangan', 'id' => $id]);
                    break;

                default:
                    # code...
                    break;
            }
        }

        //Kriteria
        $amiUnit = AmiUnit::findOne($id);
        $kriteria = MyHelper::getKriteriaAmiUnit($id);

        $instrumenEvaluasi = MyHelper::getCountEvaluasi($kriteria, $amiUnit); // Instrumen Evaluasi Diri 
        // echo '<pre>';print_r($instrumenEvaluasi);die;
        $instrumenKecukupan = $instrumenEvaluasi * 3; // Instrumen Kecukupan   
        $instrumenLapangan = $instrumenEvaluasi * 3; // Instrumen Lapangan

        // Data Evaluasi Diri
        $dataEvaluasi = Asesmen::find()->where([
            'ami_unit_id' => $id
        ])->count();

        // Data Kecukupan
        $totalDataKecukupan = 0;
        for ($i = 1; $i <= 3; $i++) {
            $skor_ak = MyHelper::getAsesmenSkor($id, 'skor_ak' . $i);
            $totalDataKecukupan = $totalDataKecukupan + $skor_ak;
        }

        // Data Lapangan
        $totalDataLapangan = 0;
        for ($i = 1; $i <= 3; $i++) {
            $skor_al = MyHelper::getAsesmenSkor($id, 'skor_al' . $i);
            $totalDataLapangan = $totalDataLapangan + $skor_al;
        }

        return $this->render('proses', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'instrumenEvaluasi' => $instrumenEvaluasi,
            'dataEvaluasi' => $dataEvaluasi,
            'instrumenKecukupan' => $instrumenKecukupan,
            'dataKecukupan' => $totalDataKecukupan,
            'instrumenLapangan' => $instrumenLapangan,
            'dataLapangan' => $totalDataLapangan,
            'persentaseEvaluasi' => MyHelper::convertPersen($dataEvaluasi, $instrumenEvaluasi),
            'persentaseKecukupan' => MyHelper::convertPersen($totalDataKecukupan, $instrumenKecukupan),
            'persentaseLapangan' => MyHelper::convertPersen($totalDataLapangan, $instrumenLapangan),
        ]);
    }

    public function actionDetail($id)
    {
        $amiUnit = AmiUnit::findOne($id);

        $kriteria = MyHelper::getKriteriaAmiUnit($id);                      // Get Kriteria
        $instrumenEvaluasi  = MyHelper::getCountEvaluasi($kriteria, $amiUnit);        // Instrumen Evaluasi Diri 

        // Data Evaluasi Diri
        $dataEvaluasi = Asesmen::find()->where([
            'ami_unit_id' => $id
        ])->count();

        // Tugas Auditor 1
        $tugasKecukupanAuditor1 = $instrumenEvaluasi;             // - Kecukupan 
        $tugasLapanganAuditor1 = $instrumenEvaluasi;             // - Lapangan 

        // Data Auditor 1
        $dataKecukupanAuditor1 = MyHelper::getAsesmenSkor($id, 'skor_ak1');
        $dataLapanganAuditor1 = MyHelper::getAsesmenSkor($id, 'skor_al1');


        // Tugas Auditor 2
        $tugasKecukupanAuditor2 = $instrumenEvaluasi;             // - Kecukupan 
        $tugasLapanganAuditor2 = $instrumenEvaluasi;             // - Lapangan 

        // Data Auditor 2
        $dataKecukupanAuditor2 = MyHelper::getAsesmenSkor($id, 'skor_ak2');
        $dataLapanganAuditor2 = MyHelper::getAsesmenSkor($id, 'skor_al2');


        // Tugas Auditor 3
        $tugasKecukupanAuditor3 = $instrumenEvaluasi;             // - Kecukupan 
        $tugasLapanganAuditor3 = $instrumenEvaluasi;             // - Lapangan 

        // Data Auditor 3
        $dataKecukupanAuditor3 = MyHelper::getAsesmenSkor($id, 'skor_ak3');
        $dataLapanganAuditor3 = MyHelper::getAsesmenSkor($id, 'skor_al3');


        return $this->render('detail', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'instrumenEvaluasi' => $instrumenEvaluasi,
            'dataEvaluasi' => $dataEvaluasi,
            'tugasKecukupanAuditor1' => $tugasKecukupanAuditor1,
            'tugasLapanganAuditor1' => $tugasLapanganAuditor1,
            'dataKecukupanAuditor1' => $dataKecukupanAuditor1,
            'dataLapanganAuditor1' => $dataLapanganAuditor1,
            'tugasKecukupanAuditor2' => $tugasKecukupanAuditor2,
            'tugasLapanganAuditor2' => $tugasLapanganAuditor2,
            'dataKecukupanAuditor2' => $dataKecukupanAuditor2,
            'dataLapanganAuditor2' => $dataLapanganAuditor2,
            'tugasKecukupanAuditor3' => $tugasKecukupanAuditor3,
            'tugasLapanganAuditor3' => $tugasLapanganAuditor3,
            'dataKecukupanAuditor3' => $dataKecukupanAuditor3,
            'dataLapanganAuditor3' => $dataLapanganAuditor3,
        ]);
    }

    public function actionPersetujuan($id)
    {
        $amiUnit = AmiUnit::findOne($id);
        $persetujuan = Persetujuan::find();
        $persetujuan->where(['ami_unit_id' => $amiUnit->id]);
        if (Yii::$app->user->identity->access_role == 'auditee') $persetujuan->andWhere(['is_auditee' => 1]);
        if (Yii::$app->user->identity->access_role == 'auditor') $persetujuan->andWhere(['auditor_id' => Yii::$app->user->identity->auditor_id]);

        $dataPersetujuan = [];

        foreach ($persetujuan->all() as $key => $persetujuan) {
            $dataPersetujuan[$persetujuan->kode_dokumen] = 1;
        }

        return $this->render('persetujuan', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'dataPersetujuan' => $dataPersetujuan
        ]);
    }

    public function actionRencana($id)
    {
        $amiUnit = AmiUnit::findOne($id);
        $listJadwal = Jadwal::find()->where(['ami_unit_id' => $id])->orderBy(['urutan' => SORT_ASC])->all();

        $filters = [
            'ami_unit_id' => $id
        ];
        $jadwalSearch = new JadwalSearch();
        $dataJadwal = $jadwalSearch->search(Yii::$app->request->queryParams, $filters);
        $tujuanAuditSearch = new TujuanAuditSearch();
        $dataTujuanAudit = $tujuanAuditSearch->search(Yii::$app->request->queryParams, $filters);
        $amiTujuanAuditSearch = new AmiTujuanAuditSearch();
        $dataAmiTujuanAudit = $amiTujuanAuditSearch->search(Yii::$app->request->queryParams, $filters);
        $ruangLingkupSearch = new RuangLingkupSearch();
        $dataRuangLingkup = $ruangLingkupSearch->search(Yii::$app->request->queryParams, $filters);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Jadwal::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Jadwal']);
            $post = ['Jadwal' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        // echo '<pre>';print_r($amiUnit->id);die;
        return $this->render('rencana', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'listJadwal' => $listJadwal,
            'jadwalSearch' => $jadwalSearch,
            'dataJadwal' => $dataJadwal,
            'tujuanAuditSearch' => $tujuanAuditSearch,
            'dataTujuanAudit' => $dataTujuanAudit,
            'ruangLingkupSearch' => $ruangLingkupSearch,
            'dataRuangLingkup' => $dataRuangLingkup,
            'AmiTujuanAuditSearch' => $amiTujuanAuditSearch,
            'dataAmiTujuanAudit' => $dataAmiTujuanAudit,
            'ruangLingkup' => RuangLingkup::findAll(['ami_unit_id' => $amiUnit->id]),
            'tujuanAudit' => AmiTujuanAudit::findAll(['ami_unit_id' => $amiUnit->id]),
        ]);
    }

    public function actionLaporanEvaluasiDiri($id)
    {
        $jenisPenilaian = 'led';

        return $this->redirect(['penilaian', 'id' => $id, 'jenisPenilaian' => $jenisPenilaian]);
    }

    public function actionAsesmenKecukupan($id)
    {
        $jenisPenilaian = 'ak';

        return $this->redirect(['penilaian', 'id' => $id, 'jenisPenilaian' => $jenisPenilaian]);
    }

    public function actionAsesmenLapangan($id)
    {
        $jenisPenilaian = 'al';

        return $this->redirect(['penilaian', 'id' => $id, 'jenisPenilaian' => $jenisPenilaian]);
    }

    public function actionLinkLed($indikator_id, $ami_unit_id)
    {

        $asesmen = Asesmen::find()->where([
            // 'id' =>
            'indikator_id' => $indikator_id,
            'ami_unit_id' => $ami_unit_id,
        ])->one();

        // echo '<pre>';print_r($asesmen);die;
        if (empty($asesmen)) {
            $model = new Asesmen();
            $model->indikator_id = $indikator_id;
            $model->ami_unit_id = $ami_unit_id;
            $model->save();
        }

        // echo '<pre>';print_r($asesmen->id);die;
        $model = $this->findModel($asesmen->id);

        $indikator = Indikator::findOne($model->indikator_id);

        if ($model->load(Yii::$app->request->post())) {
            // echo '<pre>';print_r($model);exit;
            $skor_ed = (int) $model->skor_ed;
            $model->skor_ed = $skor_ed;
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");

            $ami_unit = AmiUnit::findOne($model->ami_unit_id);
            return $this->redirect(['penilaian', 'id' => $ami_unit->id]);
        }

        return $this->render('link_led', [
            'model' => $model,
            'indikator' => $indikator,
        ]);
    }

    public function actionLinkAk($id)
    {
        // $model = Asesmen::findOne($id);
        $model = $this->findModel($id);

        // echo '<pre>';print_r($id);exit;

        $indikator = Indikator::findOne($model->indikator_id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");

            $ami_unit = AmiUnit::findOne($model->ami_unit_id);
            return $this->redirect(['penilaian', 'id' => $ami_unit->id]);
        }

        return $this->render('link_ak', [
            'model' => $model,
            'indikator' => $indikator,
        ]);
    }

    public function actionLinkAl($id)
    {
        // $model = Asesmen::findOne($id);
        $model = $this->findModel($id);

        $indikator = Indikator::findOne($model->indikator_id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");

            $ami_unit = AmiUnit::findOne($model->ami_unit_id);
            return $this->redirect(['penilaian', 'id' => $ami_unit->id]);
        }

        return $this->render('link_al', [
            'model' => $model,
            'indikator' => $indikator,
        ]);
    }

    /**
     * Updates an existing Asesmen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $id_indikator)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model'         => $model,
            'id'            => $id,
            'id_indikator'  => $id_indikator,
            'status'        => 'led'
        ]);
    }

    /**
     * Deletes an existing Asesmen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Asesmen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Asesmen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Asesmen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelAmiUnit($id)
    {
        if (($model = AmiUnit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
