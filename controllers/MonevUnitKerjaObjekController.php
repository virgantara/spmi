<?php

namespace app\controllers;

use Yii;
use app\models\MonevUnitKerjaObjek;
use app\models\MonevUnitKerjaObjekSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\AmanahKinerja;
use app\models\AmanahKinerjaUnitKerja;
use app\models\AmiUnit;
use app\models\MonevBukti;
use app\models\MonevUnitKerja;
use app\models\Temuan;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * MonevUnitKerjaObjekController implements the CRUD actions for MonevUnitKerjaObjek model.
 */
class MonevUnitKerjaObjekController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'amanah-kinerja', 'rencana-operasional', 'audit-tindak-lanjut', 'ajax-add-objek-renop', 'ajax-add-objek-atl', 'ajax-add-objek-amkin', 'ajax-get-bukti'],
                'rules' => [
                    [
                        'actions' => [
                            'update',
                            'amanah-kinerja',
                            'rencana-operasional',
                            'audit-tindak-lanjut',
                            'ajax-add-objek-renop',
                            'ajax-add-objek-atl',
                            'ajax-add-objek-amkin',
                            'ajax-get-bukti'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete',
                            'amanah-kinerja',
                            'rencana-operasional',
                            'audit-tindak-lanjut',
                            'ajax-add-objek-renop',
                            'ajax-add-objek-atl',
                            'ajax-add-objek-amkin',
                            'ajax-get-bukti'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MonevUnitKerjaObjek models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonevUnitKerjaObjekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MonevUnitKerjaObjek model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAmanahKinerja($id)
    {
        $monevUnit = MonevUnitKerja::findOne($id);

        $dataAmanahKinerja = AmanahKinerja::find()->orderBy(['nama' => SORT_ASC])->all();

        $dataAmanahKinerja = ArrayHelper::map($dataAmanahKinerja, 'id', function ($data) {
            return '[' . $data->nama . '] [' . $data->tahun . '] [' . MyHelper::getStatusAktif()[$data->status_aktif] . ']';
        });

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = MonevUnitKerjaObjek::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['MonevUnitKerjaObjek']);
            $post = ['MonevUnitKerjaObjek' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        $filters = [
            'monev_unit_kerja_id' => $id,
            'jenis_objek' => 'amanah-kinerja'
        ];
        $searchModel = new MonevUnitKerjaObjekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('amanah_kinerja', [
            'model' => $monevUnit,
            'dataAmanahKinerja' => $dataAmanahKinerja,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAuditTindakLanjut($id)
    {
        $monevUnit = MonevUnitKerja::findOne($id);

        $dataAtl = AmiUnit::find()->where(['unit_id' => $monevUnit->unit_kerja_id])->orderBy(['id' => SORT_DESC])->all();

        $dataAtl = ArrayHelper::map($dataAtl, 'id', function ($data) {
            return '[' . $data->ami->nama . '] [' . $data->unit->nama . '] [' . $data->tanggal_ami . ']';
        });

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = MonevUnitKerjaObjek::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['MonevUnitKerjaObjek']);
            $post = ['MonevUnitKerjaObjek' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        $filters = [
            'monev_unit_kerja_id' => $id,
            'jenis_objek' => 'audit-tindak-lanjut'
        ];
        $searchModel = new MonevUnitKerjaObjekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('audit_tindak_lanjut', [
            'model' => $monevUnit,
            'dataAtl' => $dataAtl,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRencanaOperasional($id)
    {
        $monevUnit = MonevUnitKerja::findOne($id);
        $dataRkat = [];

        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token' => $client_token];
        $params = [
            'unit_kerja_id' => $monevUnit->unitKerja->kode_unit
        ];

        $response = $client->get('/rkat/unitkerja/list', $params, $headers)->send();
        if ($response->isOk) {
            $result = $response->data['values'];

            if (!empty($result)) {
                foreach ($result as $d) {

                    $dataRkat[$d['id']] = '[' . $monevUnit->unitKerja->nama . '] [' . $d['nama_tahun'] . '] [' . MyHelper::statusRkat()[$d['status']] . ']';
                }
                $dataRkat = array_reverse($dataRkat, true);
            } else {
                $dataRkat[0] = 'Data rkat tidak ditemukan';
            }
        }

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = MonevUnitKerjaObjek::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['MonevUnitKerjaObjek']);
            $post = ['MonevUnitKerjaObjek' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        $filters = [
            'monev_unit_kerja_id' => $id,
            'jenis_objek' => 'rencana-operasional'
        ];
        $searchModel = new MonevUnitKerjaObjekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('rencana_operasional', [
            'model' => $monevUnit,
            'dataRkat' => $dataRkat,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MonevUnitKerjaObjek model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MonevUnitKerjaObjek();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxAddObjekAmkin()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            $monevUnitKerja = MonevUnitKerja::findOne($dataPost['monev_unit_kerja_id']);
            if (isset($monevUnitKerja) && $monevUnitKerja->monev->status == 0) {
                $results = [
                    'code' => 500,
                    'message' => 'Status Monev Non-Aktif, Tidak dapat melakukan import data!, silakan menghubungi BPM untuk merubah status monev menjadi aktif!'
                ];
                echo json_encode($results);
                exit;
            }

            if (!empty($dataPost)) {
                $successCount = 0;
                $failCount = 0;
                $dataImportId = [];

                $amanahKinerja = AmanahKinerjaUnitKerja::findAll(['unit_kerja_id' => $dataPost['unit_kerja_id'], 'amanah_kinerja_id' => $dataPost['amanah_kinerja_id']]);

                if (!empty($amanahKinerja)) {

                    foreach ($amanahKinerja as $amanah) {
                        $dataImportId[] = $amanah->id;

                        $model = MonevUnitKerjaObjek::findOne(['import_id' => $amanah->id]);
                        if (empty($model)) {
                            $model = new MonevUnitKerjaObjek();
                            $model->id = MyHelper::gen_uuid();
                        }

                        $model->monev_unit_kerja_id = $dataPost['monev_unit_kerja_id'];
                        $model->import_id = $amanah->id;
                        $model->nama_program =  $amanah->amanahKinerjaMaster->amanah_kinerja;
                        $model->target =  $amanah->target;
                        $model->capaian =  $amanah->capaian;
                        $model->jenis_objek = $dataPost['jenis_objek'];

                        if ($model->save()) {
                            $successCount++;
                        } else {
                            $failCount++;
                            $errors .= MyHelper::logError($model);
                            throw new \Exception;
                        }
                    }

                    if (!empty($dataImportId)) {
                        MonevUnitKerjaObjek::deleteAll([
                            'and',
                            ['monev_unit_kerja_id' => $dataPost['monev_unit_kerja_id']],
                            ['not in', 'import_id', $dataImportId],
                            ['jenis_objek' => 'amanah-kinerja']
                        ]);
                    }

                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' =>  $successCount . ' data successfully imported & ' . $failCount . ' failed imported'
                    ];
                } else {
                    $results = [
                        'code' => 500,
                        'message' => $errors
                    ];
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxAddObjekRenop()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {

                $monevUnitKerja = MonevUnitKerja::findOne($dataPost['monev_unit_kerja_id']);
                if (isset($monevUnitKerja) && $monevUnitKerja->monev->status == 0) {
                    $results = [
                        'code' => 500,
                        'message' => 'Status Monev Non-Aktif, Tidak dapat melakukan import data!, silakan menghubungi BPM untuk merubah status monev menjadi aktif!'
                    ];
                    echo json_encode($results);
                    exit;
                }

                $dataRkat = [];
                $api_baseurl = Yii::$app->params['api_baseurl'];
                $client = new Client(['baseUrl' => $api_baseurl]);
                $client_token = Yii::$app->params['client_token'];
                $headers = ['x-access-token' => $client_token];
                $params = [
                    'rkat_id' => $dataPost['rkat_id']
                ];

                $response = $client->get('/rkat/get', $params, $headers)->send();
                if ($response->isOk) {
                    $result = $response->data['values'];

                    if (!empty($result)) {
                        $successCount = 0;
                        $failCount = 0;
                        $dataImportId = [];
                        foreach ($result as $indukKegiatan) {
                            foreach ($indukKegiatan['list_program_kerja'] as $proker) {
                                foreach ($proker['list_rencana_kegiatan'] as $rencanaKegiatan) {
                                    $dataImportId[] = $rencanaKegiatan['rkat_item']['id'];

                                    // Temukan atau buat model baru
                                    $model = MonevUnitKerjaObjek::findOne(['import_id' => $rencanaKegiatan['rkat_item']['id']]);
                                    if (!$model) {
                                        $model = new MonevUnitKerjaObjek();
                                        $model->id = MyHelper::gen_uuid();
                                    }

                                    // Update data model
                                    $model->monev_unit_kerja_id = $dataPost['monev_unit_kerja_id'];
                                    $model->import_id = (string)$rencanaKegiatan['rkat_item']['id'];
                                    $model->kode = $indukKegiatan['kode'];
                                    $model->nama_kode = $indukKegiatan['nama'];
                                    $model->nama_sub_kode = $proker['nama'];
                                    $model->jenis_objek = $dataPost['jenis_objek'];
                                    $model->nama_program = $rencanaKegiatan['nama'];
                                    $model->biaya = $rencanaKegiatan['rkat_item']['nominal_ajuan'];
                                    $model->waktu = $rencanaKegiatan['rkat_item']['waktu_pelaksanaan'];

                                    if ($model->save()) {
                                        $successCount++;
                                    } else {
                                        $failCount++;
                                        $errors .= MyHelper::logError($model);
                                    }
                                }
                            }
                        }

                        if (!empty($dataImportId)) {
                            MonevUnitKerjaObjek::deleteAll([
                                'and',
                                ['monev_unit_kerja_id' => $dataPost['monev_unit_kerja_id']],
                                ['not in', 'import_id', $dataImportId],
                                ['jenis_objek' => 'rencana-operasional']
                            ]);
                        }

                        $transaction->commit();
                        $results = [
                            'code' => 200,
                            'message' =>  $successCount . ' data successfully imported & ' . $failCount . ' failed imported'
                        ];
                    } else {
                        $results = [
                            'code' => 500,
                            'message' => $errors
                        ];
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxAddObjekAtl()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            $monevUnitKerja = MonevUnitKerja::findOne($dataPost['monev_unit_kerja_id']);
            if (isset($monevUnitKerja) && $monevUnitKerja->monev->status == 0) {
                $results = [
                    'code' => 500,
                    'message' => 'Status Monev Non-Aktif, Tidak dapat melakukan import data!, silakan menghubungi BPM untuk merubah status monev menjadi aktif!'
                ];
                echo json_encode($results);
                exit;
            }

            if (!empty($dataPost)) {
                $successCount = 0;
                $failCount = 0;
                $dataImportId = [];

                $temuans = Temuan::findAll(['ami_unit_id' => $dataPost['ami_unit_id']]);

                if (!empty($temuans)) {

                    foreach ($temuans as $temuan) {
                        $dataImportId[] = $temuan->id;
                        $model = MonevUnitKerjaObjek::findOne(['import_id' => $temuan->id]);
                        if (empty($model)) {
                            $model = new MonevUnitKerjaObjek();
                            $model->id = MyHelper::gen_uuid();
                        }

                        $model->monev_unit_kerja_id = $dataPost['monev_unit_kerja_id'];
                        $model->import_id = (string)$temuan->id;
                        $model->referensi =  $temuan->referensi;
                        $model->ketidaksesuaian =  $temuan->uraian_k;
                        $model->nama_sub_kode =  $temuan->akar_penyebab;
                        $model->jenis_objek = $dataPost['jenis_objek'];
                        $model->rencana_tindak_lanjut =  $temuan->rencana_tindak_lanjut;
                        $model->waktu = $temuan->jadwal_rtl;

                        if ($model->save()) {
                            $successCount++;
                        } else {
                            $failCount++;
                            $errors .= MyHelper::logError($model);
                            throw new \Exception;
                        }
                    }

                    if (!empty($dataImportId)) {
                        MonevUnitKerjaObjek::deleteAll([
                            'and',
                            ['monev_unit_kerja_id' => $dataPost['monev_unit_kerja_id']],
                            ['not in', 'import_id', $dataImportId],
                            ['jenis_objek' => 'audit-tindak-lanjut']
                        ]);
                    }

                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' =>  $successCount . ' data successfully imported & ' . $failCount . ' failed imported'
                    ];
                } else {
                    $results = [
                        'code' => 500,
                        'message' => $errors
                    ];
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxGetBukti()
    {

        $results = [];
        $data = [];

        $errors = '';

        $dataPost = $_POST;
        if (!empty($dataPost)) {

            $buktis = MonevBukti::findAll(['monev_unit_kerja_objek_id' => $dataPost['objek_id']]);

            if (!empty($buktis)) {

                foreach ($buktis as $bukti) {
                    $singkatan = isset($bukti->dokumen->unitKerja->singkatan) ? $bukti->dokumen->unitKerja->singkatan : '-';
                    $data[] = [
                        'id' => $bukti->id,
                        'dokumen_id' => $bukti->dokumen->id,
                        'nama_dokumen' => '[' . $singkatan . '] ' . $bukti->dokumen->nama,
                    ];
                }

                $results = [
                    'code' => 200,
                    'message' => 'Data ditemukan',
                    'data' => $data
                ];
            } else {
                $results = [
                    'code' => 200,
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            $errors .= 'Oops, you cannot POST empty data';
            throw new \Exception;
        }
        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing MonevUnitKerjaObjek model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MonevUnitKerjaObjek model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MonevUnitKerjaObjek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MonevUnitKerjaObjek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MonevUnitKerjaObjek::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findObjek($monevUnitId, $jenisObjek)
    {
        if (($objeks = MonevUnitKerjaObjek::find()->where([
            'monev_unit_kerja_id' => $monevUnitId,
            'jenis_objek' => $jenisObjek
        ])->orderBy([
            'kode' => SORT_ASC,
            // 'import_id' => SORT_ASC
        ])->all()) !== null) {
            return $objeks;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
