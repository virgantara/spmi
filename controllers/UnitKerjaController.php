<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\AuthAssignment;
use Yii;
use app\models\UnitKerja;
use app\models\UnitKerjaSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * UnitKerjaController implements the CRUD actions for UnitKerja model.
 */
class UnitKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'auditee', 'send', 'ajax-send'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'view', 'auditee', 'send', 'ajax-send'],
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UnitKerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnitKerjaSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['jenis' => ['satker', 'prodi', 'fakultas']]);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = UnitKerja::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['UnitKerja']);
            $post = ['UnitKerja' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKoordinatorRtm()
    {
        $searchModel = new UnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['jenis' => ['koordinator', 'universitas']]);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = UnitKerja::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['UnitKerja']);
            $post = ['UnitKerja' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('koordinator_rtm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMasterProdi()
    {
        $searchModel = new UnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('master_prodi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAuditee()
    {
        $searchModel = new UnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('auditee', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UnitKerja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UnitKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UnitKerja();

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        if ($model->load(Yii::$app->request->post())) {
            $model->is_sent = 1;
            $model->save();

            // Add User Auditee...

            $user = User::findOne([
                'unit_kerja_id' => $model->id,
            ]);

            // echo '<pre>';print_r($user);die;

            if (empty($user)) {
                $user = new User;
            }

            $password = MyHelper::getRandomString(8, 8, true, false, false);

            $user->username = $model->email;
            $user->email = $model->email;
            $user->nama = $model->nama;
            $user->unit_kerja_id = $model->id;

            $user->setPassword($password);
            $user->status = User::STATUS_ACTIVE;
            $user->access_role = 'auditee';

            $auth = Yii::$app->authManager;
            $oldRole = $auth->getRole('auditee');

            $user->access_role = $oldRole->name;

            if ($user->save()) {
                $auth->assign($oldRole, $user->id);
                $to = MyHelper::sendEmail($user->email);
                $emailTemplate = $this->renderPartial('email', [
                    'user' => $user,
                    'password' => $password
                ]);
                // echo '<pre>';print_r($emailTemplate);die;

                Yii::$app->mailer->compose()
                    ->setTo($to)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    ->setSubject('[SIMUDA] Account Information')
                    ->setHtmlBody($emailTemplate)
                    ->send();

                $toBpm = MyHelper::emailBpm();
                Yii::$app->mailer->compose()
                    ->setTo($toBpm)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    ->setSubject('[SIMUDA] Account Information')
                    ->setHtmlBody($emailTemplate)
                    ->send();
            } else {
                throw new \Exception(MyHelper::logError($user));
            }

            $transaction->commit();
            $results = [
                'code' => 200,
                'message' => 'Data successfully added'
            ];

            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateKoordinatorRtm()
    {
        $model = new UnitKerja();

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        if ($model->load(Yii::$app->request->post())) {
            $model->is_sent = 1;
            $model->save();

            $user = User::findOne([
                'unit_kerja_id' => $model->id,
            ]);

            if (empty($user)) {
                $user = new User;
            }

            $password = MyHelper::getRandomString(8, 8, true, false, false);

            $user->username = $model->email;
            $user->email = $model->email;
            $user->nama = $model->nama;
            $user->unit_kerja_id = $model->id;

            $user->setPassword($password);
            $user->status = User::STATUS_ACTIVE;
            $user->access_role = 'auditee';

            $auth = Yii::$app->authManager;
            $oldRole = $auth->getRole('auditee');

            $user->access_role = $oldRole->name;

            if ($user->save()) {
                $auth->assign($oldRole, $user->id);
                $to = MyHelper::sendEmail($user->email);
                $emailTemplate = $this->renderPartial('email_koordinator', [
                    'user' => $user,
                    'password' => $password
                ]);
                Yii::$app->mailer->compose()
                    ->setTo($to)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    ->setSubject('[SIMUDA] Account Information')
                    ->setHtmlBody($emailTemplate)
                    ->send();

                $toBpm = MyHelper::emailBpm();
                Yii::$app->mailer->compose()
                    ->setTo($toBpm)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    ->setSubject('[SIMUDA] Account Information')
                    ->setHtmlBody($emailTemplate)
                    ->send();
            } else {
                throw new \Exception(MyHelper::logError($user));
            }

            $transaction->commit();
            $results = [
                'code' => 200,
                'message' => 'Data successfully added'
            ];

            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['koordinator-rtm']);
        }

        return $this->render('create_koordinator', [
            'model' => $model,
        ]);
    }


    public function actionSend($id)
    {
        // $model = new UnitKerja();
        $model = $this->findModel($id);

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        // if ($model->load(Yii::$app->request->post())) {
        $model->is_sent = 1;
        $model->save();

        // Add User Auditee...

        $user = User::findOne([
            'unit_kerja_id' => $model->id,
        ]);

        if (empty($user)) {
            $user = new User;
        }

        $password = MyHelper::getRandomString(8, 8, true, false, false);

        $user->username = $model->email;
        $user->email = $model->email;
        $user->nama = $model->nama;
        $user->unit_kerja_id = $model->id;

        $user->setPassword($password);
        $user->status = User::STATUS_ACTIVE;
        $user->access_role = 'auditee';

        $auth = Yii::$app->authManager;
        $oldRole = $auth->getRole('auditee');

        $user->access_role = $oldRole->name;

        if ($user->save()) {
            $auth->assign($oldRole, $user->id);
            $to = MyHelper::sendEmail($user->email);

            $emailTemplate = $this->renderPartial('email', [
                'user' => $user,
                'password' => $password
            ]);
            // echo '<pre>';print_r($emailTemplate);die;

            Yii::$app->mailer->compose()
                ->setTo($to)
                ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                ->setSubject('[SIMUDA] Account Information')
                ->setHtmlBody($emailTemplate)
                ->send();

            $toBpm = MyHelper::emailBpm();
            Yii::$app->mailer->compose()
                ->setTo($toBpm)
                ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                ->setSubject('[SIMUDA] Account Information')
                ->setHtmlBody($emailTemplate)
                ->send();
        } else {
            throw new \Exception(MyHelper::logError($user));
        }

        $transaction->commit();
        $results = [
            'code' => 200,
            'message' => 'Data successfully added'
        ];

        Yii::$app->session->setFlash('success', "Data tersimpan");
        return $this->redirect(['view', 'id' => $model->id]);
        // }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing UnitKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data diupdate & Akun terkirim melalui email");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateProfil()
    {
        $id = Yii::$app->user->identity->unit_kerja_id;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data berhasil diupdate");
            return $this->redirect(['user/profil']);
        }

        return $this->render('update_profil', [
            'model' => $model,
        ]);
    }

    public function actionSubkoordinator()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $jenis = $parents[0];
                $query = UnitKerja::find();
                $query->andWhere([
                    'jenis' => $jenis,
                ]);
                $list_unit = $query->all();
                foreach ($list_unit as $item) {
                    $out[$item->id] = [
                        'id' => $item->id,
                        'name' => $item->nama
                    ];
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                die();
            }
        }
    }

    public function actionAjaxSend()
    {
        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $dataPost = $_POST;

        $model = $this->findModel($dataPost['id']);


        try {
            if (!empty($dataPost)) {


                $model->is_sent = 1;
                $model->save();

                // Add User Auditee...

                $user = User::findOne([
                    'unit_kerja_id' => $model->id,
                ]);

                // echo '<pre>';print_r($user);die;
                $is_user = false;
                if (empty($user)) {
                    $user = new User;
                    $is_user = true;
                }

                $password = MyHelper::getRandomString(8, 8, true, false, false);

                $user->username = $model->email;
                $user->email = $model->email;
                $user->nama = $model->nama;
                $user->unit_kerja_id = $model->id;

                $user->setPassword($password);
                $user->status = User::STATUS_ACTIVE;
                $user->access_role = 'auditee';

                $auth = Yii::$app->authManager;
                $oldRole = $auth->getRole('auditee');

                $user->access_role = $oldRole->name;

                // echo '<pre>';print_r($user);die;
                if ($user->save()) {

                    if ($is_user == true)
                        $auth->assign($oldRole, $user->id);

                    // $to = MyHelper::sendEmail($user->email);

                    // $emailTemplate = $this->renderPartial('email', [
                    //     'user' => $user,
                    //     'password' => $password
                    // ]);

                    // Yii::$app->mailer->compose()
                    //     ->setTo($to)
                    //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    //     ->setSubject('[SIMUDA] Account Information')
                    //     ->setHtmlBody($emailTemplate)
                    //     ->send();


                    // $toBpm = MyHelper::emailBpm();
                    // Yii::$app->mailer->compose()
                    //     ->setTo($toBpm)
                    //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    //     ->setSubject('[SIMUDA] Account Information')
                    //     ->setHtmlBody($emailTemplate)
                    //     ->send();
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }

                $transaction->commit();
                $results = [
                    'code' => 200,
                    'message' => 'Data successfully updated'
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            // $errors .= 'Oops, your data is incomplete, please complete the data';

            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }


    public function actionAjaxGetPj()
    {

        $results = [];

        $errors = '';

        try {
            $dataPost = $_POST['dataPost'];

            if (!empty($dataPost)) {

                $unitKerja = $this->findModel($dataPost);
                $results = [
                    'code' => 200,
                    'data' => [
                        'nama_kepala' => trim($unitKerja->penanggung_jawab),
                    ]
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Deletes an existing UnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UnitKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
