<?php

namespace app\controllers;

use app\models\AmiUnit;
use Yii;
use app\models\PersetujuanCek;
use app\models\PersetujuanCekSearch;
use app\models\UnitKerja;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PersetujuanCekController implements the CRUD actions for PersetujuanCek model.
 */
class PersetujuanCekController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
    
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }
    
    // public function actionIndex()
    // {
    //     $searchModel = new PersetujuanCekSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    // public function actionCreate()
    // {
    //     $model = new PersetujuanCek();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         Yii::$app->session->setFlash('success', "Data tersimpan");
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionCekKeaslian()
    {
        $model = new PersetujuanCek();
        $scenario = 'captchaRequired';
        $model->scenario = $scenario;

        $statusDokumen = '';
        $data = [];

        if (!empty($_GET['btn-cari'])) {
            $persetujuan = PersetujuanCek::findOne(['kode_dokumen' => $_GET['kode_dokumen']]);

            if (isset($persetujuan)) {
                $statusDokumen = 'asli';
                switch ($persetujuan->nama_kegiatan) {
                    case 'AMI':
                        $data = $this->persetujuanAMI($persetujuan);
                        break;

                    default:
                        break;
                }
            } else {
                $statusDokumen = 'palsu';
            }
        }
        return $this->render('cek_keaslian', [
            'model' => $model,
            'statusDokumen' => $statusDokumen,
            'data' => $data,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PersetujuanCek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PersetujuanCek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function persetujuanAMI($persetujuan)
    {
        $amiUnit = UnitKerja::findOne($persetujuan->unit_kerja_id);
        $amiUnit = AmiUnit::findOne([
            $persetujuan->unit_kerja_id
        ]);
        switch ($persetujuan->nama_dokumen) {
            case 'surat-tugas-ami':
                $data['disetujui'][] = [
                    'nama' => $amiUnit->ami->ketua_bpm,
                    'waktu' => $amiUnit->created_at,
                    'jabatan' => 'Ketua BPM'
                ];
                $data['link'] = [
                    'surat/tugas-amiunit',
                    'ami_unit_id' => $persetujuan->unit_kerja_id
                ];
                return $data;
                break;

            default:
                # code...
                break;
        }
    }
    protected function findModel($id)
    {
        if (($model = PersetujuanCek::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
