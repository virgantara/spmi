<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\Kriteria;
use app\models\Persetujuan;
use Yii;
use app\models\Temuan;
use app\models\TemuanSearch;
use Google\Service\HangoutsChat\Resource\Dms;
use Symfony\Component\Console\Helper\Dumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * TemuanController implements the CRUD actions for Temuan model.
 */
class TemuanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'view'],
                        'allow' => true,
                        'roles' => ['admin', 'auditor', 'auditee']
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Temuan models.
     * @return mixed
     */
    public function actionIndex($id)
    {

        $amiUnit = AmiUnit::findOne($id);

        $searchModel = new TemuanSearch();
        $filter = ['ami_unit_id' => $id];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filter);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Temuan::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Temuan']);
            $post = ['Temuan' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'id'            => $id,
            'amiUnit'       => $amiUnit,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionAuditTindakLanjut($id)
    {

        $amiUnit = AmiUnit::findOne($id);

        $searchModel = new TemuanSearch();
        $filter = ['ami_unit_id' => $id];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filter);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Temuan::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Temuan']);
            $post = ['Temuan' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('audit_tindak_lanjut', [
            'id' => $id,
            'amiUnit' => $amiUnit,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMasterTemuan()
    {

        $searchModel = new TemuanSearch();

        $filter = [];
        $ami_id = null;
        $ami_unit_id = null;
        $kriteria_id = null;

        if (!empty($_GET)) {
            $ami_id = $_GET['ami_id'] ?? null;
            $ami_unit_id = $_GET['ami_unit_id'] ?? null;
            $kriteria_id = $_GET['kriteria_id'] ?? null;
            $kategori = $_GET['kategori'] ?? null;

            $filter = [
                'ami_id' => $ami_id,
                'ami_unit_id' => $ami_unit_id,
                'kriteria_id' => $kriteria_id,
                'kategori' => $kategori
            ];
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filter);

        $listKriteria = ArrayHelper::map(Kriteria::find()->orderBy(['urutan' => SORT_ASC])->all(), 'id', function ($model) {
            return $model->nama . (isset($model->keterangan) ? ' - ' . $model->keterangan : '');
        });

        return $this->render('master_temuan', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'listKriteria'   => $listKriteria,
        ]);
    }

    public function actionCetakTemuan($ami_unit_id)
    {

        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $listTemuan = Temuan::find()->where(['ami_unit_id' => $ami_unit_id])->all();
            $auditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
            ])->andWhere([
                'not', ['status_id' => 1]
            ])->orderBy([
                'status_id' => SORT_ASC
            ]);
            $listAuditor = $auditor->all();
            $jumlahAuditor = $auditor->count();
            $ketuaAuditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
                'status_id' => 1,
            ])->one();
            $imgData = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';

            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetFont($fontreg, '', 10);

            $lembar = 1;

            $pdf->Image($imgData, 20, 13, 14);

            $no = 1;

            if (empty($listTemuan)) {
                // Tampilkan pesan "Data tidak tersedia"
                $pdf->AddPage();
                $pdf->writeHTML('<h1>Tidak ada temuan</h1>');
            } else {

                $listPersetujuan = ArrayHelper::map(Persetujuan::findAll([
                    'ami_unit_id' => $amiUnit->id,
                    'kode_dokumen' => 'temuan-ami'
                ]), function ($data) {
                    $auditorId = $data->auditor_id ?? 0;
                    $isAuditee = $data->is_auditee ?? 0;
                    return $auditorId . '-' . $isAuditee;
                }, 'ami_unit_id');

                foreach ($listTemuan as $temuan) {

                    echo $this->renderPartial('cetak_temuan', [
                        'listTemuan' => $listTemuan,
                        'lembar' => $lembar,
                        'temuan' => $temuan,
                        'amiUnit' => $amiUnit,
                        'jumlahAuditor' => $jumlahAuditor + 1,
                        'no' => $no,
                        'statusPersetujuan' => !empty($listPersetujuan) ? true : false,
                    ]);
                    $no++;

                    # code...
                    $data = ob_get_clean();
                    ob_start();
                    $pdf->AddPage();
                    $pdf->Image($imgData, 20, 13, 14);
                    $pdf->writeHTML($data);
                    $style = array(
                        'border' => false,
                        'padding' => 0,
                        'fgcolor' => array(0, 0, 0),
                        'bgcolor' => false, //array(255,255,255)
                    );

                    #DATA
                    $mulai = 70;
                    $jarak = 48;
                    $data = [
                        'ami_unit_id' => $amiUnit->id,
                    ];

                    ##BARCODE PENANGGUNGJAWAB
                    if (isset($listPersetujuan['0-1']))
                        $pdf->write2DBarcode(
                            MyHelper::printQrIsi(
                                'AMI',
                                'temuan-ami',
                                $amiUnit->unit->id,
                                $amiUnit->ami->tahun,
                                '#auditee'
                            ),
                            'QRCODE,Q',
                            23,
                            128,
                            0,
                            22,
                            $style,
                            'N'
                        );

                    ##BARCODE KETUA AUDITOR
                    if (isset($listPersetujuan[$ketuaAuditor->auditor_id . '-0']))
                        $pdf->write2DBarcode(
                            MyHelper::printQrIsi(
                                'AMI',
                                'temuan-ami',
                                $amiUnit->unit->id,
                                $amiUnit->ami->tahun,
                                '#auditor1'
                            ),
                            'QRCODE,Q',
                            $mulai,
                            128,
                            0,
                            22,
                            $style,
                            'N'
                        );

                    ##BARCODE ANGGOTA AUDITOR
                    if (isset($listAuditor)) {
                        $position = $mulai + $jarak;

                        foreach ($listAuditor as $auditor) {
                            $aAuditor['auditor_id'] = $auditor->auditor_id;
                            $aAuditor['is_auditee'] = false;
                            $dataAuditor = $data + $aAuditor;

                            if (isset($listPersetujuan[$auditor->auditor_id . '-0']))
                                $pdf->write2DBarcode(
                                    MyHelper::printQrIsi(
                                        'AMI',
                                        'temuan-ami',
                                        $amiUnit->unit->id,
                                        $amiUnit->ami->tahun,
                                        '#auditor' . $auditor->status_id
                                    ),
                                    'QRCODE,Q',
                                    $position,
                                    128,
                                    0,
                                    22,
                                    $style,
                                    'N'
                                );
                            $position = $position + $jarak;
                        }
                    }

                    $lembar++;
                }
            }

            $pdf->Output('Temuan-' . $amiUnit->ami->nama . '-' . $amiUnit->unit->singkatan . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionCetakLogTemuan($ami_unit_id)
    {
        $results = [];
        $amiUnit = AmiUnit::findOne($ami_unit_id);

        try {
            ob_start();
            $this->layout = '';
            ob_start();
            $auditor = AmiAuditor::find()
                ->orderBy(['status_id' => SORT_ASC])
                ->where([
                    'ami_id' => $amiUnit->id,
                ]);

            $listTemuan = Temuan::find();
            $listTemuan->where(['ami_unit_id' => $amiUnit->id]);
            $listTemuan = $listTemuan->all();

            $dataJumlahPerkategori = [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
            ];

            foreach ($listTemuan as $key => $value) {
                $dataJumlahPerkategori[$value->kategori]++;
            }

            echo $this->renderPartial('cetak_log_temuan', [
                'results' => $results,
                'nama_unit' => $amiUnit->unit->nama,
                'tahun' => $amiUnit->ami->tahun,
                'auditor' => $auditor,
                'amiUnit' => $amiUnit,
                'dataJumlahPerkategori' => $dataJumlahPerkategori,
                'listTemuan' => $listTemuan,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);

            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            // echo '<pre>';print_r($fontpath);die;
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $pdf->Image($imgdata, 20, 13, 14);

            $listPersetujuan = ArrayHelper::map(Persetujuan::findAll([
                'ami_unit_id' => $amiUnit->id,
                'kode_dokumen' => 'log-temuan-ami'
            ]), function ($data) {
                $auditorId = $data->auditor_id ?? 0;
                $isAuditee = $data->is_auditee ?? 0;
                return $auditorId . '-' . $isAuditee;
            }, 'ami_unit_id');

            $style = [
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => false,
            ];
            if (isset($listPersetujuan['0-1']))
                $pdf->write2DBarcode(
                    MyHelper::printQrIsi(
                        'AMI',
                        'temuan-ami',
                        $amiUnit->unit->id,
                        $amiUnit->ami->tahun,
                        '#auditee'
                    ),
                    'QRCODE,Q',
                    25,
                    128,
                    0,
                    22,
                    $style,
                    'N',
                    false
                );

            $mulai = 70;
            $jarak = 48;

            foreach ($auditor->all() as $key => $value) {
                $position = $mulai + $jarak;
                if (isset($listPersetujuan[$value->auditor_id . '-0']))
                    $pdf->write2DBarcode(
                        MyHelper::printQrIsi(
                            'AMI',
                            'temuan-ami',
                            $amiUnit->unit->id,
                            $amiUnit->ami->tahun,
                            '#auditor' . $value->status_id
                        ),
                        'QRCODE,Q',
                        $position,
                        128,
                        0,
                        22,
                        $style,
                        'N'
                    );
            }

            $pdf->SetXY(12, 10);

            $pdf->writeHTML($data);

            $pdf->Output('Audit Tindak Lanjut ' . $amiUnit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }


    public function actionCetakAtl($id)
    {
        $results = [];
        $amiUnit = AmiUnit::findOne($id);

        try {
            ob_start();
            $this->layout = '';
            ob_start();
            $auditor_ketua = AmiAuditor::find()->orderBy(['status_id' => SORT_ASC])->where([
                'ami_id' => $amiUnit->id,
                'status_id' => 1,
            ])->one();

            $auditor = AmiAuditor::find()->orderBy(['status_id' => SORT_ASC])->where([
                'ami_id' => $amiUnit->id,
            ])->all();

            // echo '<pre>';print_r($amiUnit);die;

            $atl = Temuan::find();
            $atl->where([
                'ami_unit_id' => $amiUnit->id,
            ]);
            $atl->andWhere(['IS NOT', 'rencana_tindak_lanjut', null])
                ->andWhere(['IS NOT', 'realisasi_tindak_lanjut', null])
                ->andWhere(['IS NOT', 'jadwal_rtl', null]);
            $atl = $atl->all();

            echo $this->renderPartial('cetak_atl', [
                'results' => $results,
                'nama_unit' => $amiUnit->unit->nama,
                'tahun' => $amiUnit->ami->tahun,
                'auditor' => $auditor,
                'amiUnit' => $amiUnit,
                'atl' => $atl,
                'auditor_ketua' => $auditor_ketua,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            // echo '<pre>';print_r($fontpath);die;
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $pdf->Image($imgdata, 20, 13, 14);
            $pdf->writeHTML($data);
            $pdf->Output('Audit Tindak Lanjut ' . $amiUnit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    /**
     * Displays a single Temuan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Temuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Temuan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxGetById()
    {
        $dataPost = $_POST['dataPost'];
        $out = [];

        $temuan = Temuan::findOne($dataPost);

        if ($temuan) {
            $out = $temuan->attributes;
        }

        echo \yii\helpers\Json::encode($out);

        die();
    }

    public function actionAjaxAdd()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                if ($dataPost['id'] != null) {
                    $model = $this->findModel($dataPost['id']);
                } else {
                    $model = new Temuan;
                }

                $model->attributes = $dataPost;

                if ($model->save()) {
                    $transaction->commit();

                    $results = [
                        'code'      => 200,
                        'message'   => 'Data saved successfully'
                    ];
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxUpdatePj()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST['dataPost'];

            if (!empty($dataPost)) {

                $model = $this->findModel($dataPost['id']);

                $model->attributes = $dataPost;

                if ($model->save()) {
                    $transaction->commit();

                    $results = [
                        'code' => 200,
                        'message' => 'Data saved successfully'
                    ];
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxGetData()
    {
        $dataPost = $_POST;
        $out = [];

        $temuan = Temuan::findOne($dataPost['id']);

        $out['id']                  = $temuan->id;
        $out['kriteria']            = $temuan->kriteria;
        $out['temuan']              = $temuan->temuan;
        $out['lokasi']              = $temuan->lokasi;
        $out['ruang_lingkup']       = $temuan->ruang_lingkup;
        $out['uraian_k']            = $temuan->uraian_k;
        $out['referensi']           = $temuan->referensi;
        $out['akar_penyebab']       = $temuan->akar_penyebab;
        $out['rekomendasi']         = $temuan->rekomendasi;
        $out['kategori']            = $temuan->kategori;
        $out['jadwal_perbaikan']    = $temuan->jadwal_perbaikan;
        $out['jabatan_pj']          = $temuan->jabatan_pj;
        $out['penanggung_jawab']    = $temuan->penanggung_jawab;

        echo \yii\helpers\Json::encode($out);

        die();
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id, $ami_unit_id = null)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['temuan/index', 'id' => $ami_unit_id]);
    }

    protected function findModel($id)
    {
        if (($model = Temuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
