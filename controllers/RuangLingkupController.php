<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiUnit;
use Yii;
use app\models\RuangLingkup;
use app\models\RuangLingkupSearch;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * RuangLingkupController implements the CRUD actions for RuangLingkup model.
 */
class RuangLingkupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RuangLingkup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RuangLingkupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = RuangLingkup::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['RuangLingkup']);
            $post = ['RuangLingkup' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpload()
    {
        $errors = '';
        $counter = 0;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if ($file != null) {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    $inputFileType = IOFactory::identify($file->tempName);
                    $reader = IOFactory::createReader($inputFileType);
                    $spreadsheet = $reader->load($file->tempName);
                    $worksheet = $spreadsheet->getSheetByName('Master Ruang Lingkup');
                    $highestRow = $worksheet->getHighestRow();

                    // Prepare arrays for mapping

                    $ami = Ami::findOne(['status_aktif' => 1]);

                    $arrAmiUnit = [];
                    $amiUnit = ArrayHelper::map(AmiUnit::find()->where(['ami_id' => $ami->id])->all(), function ($data) {
                        return $data->unit->singkatan;
                    }, 'id');
                    foreach ($amiUnit as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAmiUnit[$newKey] = $value;
                    }

                    $jumlahb = 0;
                    $jumlahg = 0;

                    $mulaiKolom = 1;
                    $dataRuangLingkup = [];
                    foreach ($arrAmiUnit as $key => $value) {
                        $mulaiBaris = 4;
                        for ($jmlTujuan = 0; $jmlTujuan < $highestRow; $jmlTujuan++) {
                            $ruangLingkup = $worksheet->getCell(MyHelper::convertToLetters($mulaiKolom) . $mulaiBaris)->getValue();
                            
                            if (!empty($ruangLingkup)) {
                                $dataRuangLingkup[] = [
                                    'ruang_lingkup' => $ruangLingkup,
                                    'ami_unit_id' => $value
                                ];
                                $jumlahb++;
                            }
                            $mulaiBaris++;
                        }
                        $mulaiKolom++;
                    }

                    // Batch insert after the loop
                    if (!empty($dataRuangLingkup)) {
                        Yii::$app->db->createCommand()->batchInsert('ruang_lingkup', [
                            'ruang_lingkup',
                            'ami_unit_id',
                        ], $dataRuangLingkup)->execute();
                    }

                    // Committing the transaction after batch insert
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', $jumlahb . ' data ruang lingkup berhasil diinput');
                    return $this->redirect(['index']);
                } catch (\Throwable $th) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    public function actionDownloadMaster()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $ami = Ami::findOne(['status_aktif' => 1]);
        $amiUnit = AmiUnit::find()->joinWith(['ami as a']);
        $amiUnit->where(['a.status_aktif' => 1]);

        // Add the first sheet
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Master Ruang Lingkup');
        $sheet1->setCellValue('A1', 'Master Ruang Lingkup');

        $sheet1->setCellValue('A2', 'Periode');
        $sheet1->setCellValue('B2', $ami->nama);

        $mulai = 1;
        foreach ($amiUnit->all() as $au) {
            $sheet1->setCellValue(MyHelper::convertToLetters($mulai) . '3', $au->unit->singkatan);
            $mulai++;
        }

        $sheet1->mergeCells('A1:M1');

        $style = $sheet1->getStyle('A1:M3');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet1->getStyle('A1:' . MyHelper::convertToLetters($mulai) . '3')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $spreadsheet->setActiveSheetIndex(0);

        // Configure the Excel output
        $filename = 'data-master-ruang-lingkup-ami-' . date('Y') . '.xlsx';

        // Render Excel to memory
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Set the response headers and send the file as a download
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        Yii::$app->response->headers->add('Content-Disposition', 'attachment;filename=' . $filename);
        Yii::$app->response->headers->add('Cache-Control', 'max-age=0');
        Yii::$app->response->content = $content;
        Yii::$app->response->send();
        Yii::$app->end();
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RuangLingkup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RuangLingkup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxAdd()
    {
        $results   = [];
        $errors    = '';
        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $model = new RuangLingkup();
                $model->attributes = $dataPost;

                if ($model->save()) {
                    $results   = [
                        'code'     => 200,
                        'message'  => "Yeay, Data berhasil ditambahkan",
                    ];
                } else {
                    $errors    = MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Opps, data yang dikirim kosong !';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'     => 500,
                'message'  => $errors,
            ];
        } catch (\Throwable $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'     => 500,
                'message'  => $errors,
            ];
        }

        echo json_encode($results);

        die();
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RuangLingkup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRencanaDelete($id)
    {
        $model = $this->findModel($id);
        // echo '<pre>';print_r($model);die;
        $model->delete();

        return $this->redirect(['asesmen/rencana', 'id' => $model->ami_unit_id]);
    }

    /**
     * Finds the RuangLingkup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RuangLingkup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RuangLingkup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
