<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Asesmen;
use Yii;
use app\models\Dokumen;
use app\models\DokumenSearch;
use app\models\MasterJenis;
use app\models\MonevBukti;
use app\models\MonevUnitKerjaObjek;
use app\models\Pengelompokan;
use app\models\PengelompokanSearch;
use app\models\UnitKerja;
use Symfony\Component\Console\Helper\Dumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * DokumenController implements the CRUD actions for Dokumen model.
 */
class DokumenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['update', 'view', 'index', 'delete'],
                'rules' => [

                    [
                        'actions' => [
                            'update',
                            'view',
                            'index',
                            'delete',
                            'upload-bukti-monev'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin', 'auditee', 'auditor'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dokumen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionUmum()
    {
        $searchModel = new DokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('umum', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionPersonal()
    {
        $searchModelDokumen = new DokumenSearch();
        $filters = [];
        if (!empty($_GET['nama'])) $filters['nama'] = $_GET['nama'];
        if (!empty($_GET['jenis_id'])) $filters['jenis_id'] = $_GET['jenis_id'];
        if (!empty($_GET['unit_kerja_id'])) $filters['unit_kerja_id'] = $_GET['unit_kerja_id'];

        $dataProviderDokumen = $searchModelDokumen->search(Yii::$app->request->queryParams, 'personal', $filters);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');

            $mode = array_key_last($_POST);

            $model = Dokumen::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST[$mode]);
            $post = [$mode => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('personal', [
            'searchModelDokumen' => $searchModelDokumen,
            'dataProviderDokumen' => $dataProviderDokumen,
        ]);
    }

    public function actionSearch()
    {
        $searchModel = new DokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dokumen model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dokumen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new Dokumen;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];

        $s3new = new \Aws\S3\S3Client($s3config);
        $errors = '';


        try {

            // echo '<pre>';print_r(Yii::$app->request->post());die;
            $dataPost   = $_POST;
            if (Yii::$app->request->post()) {

                $update = false;

                if (isset($dataPost['Dokumen']['id'])) {
                    $model = $this->findModel($dataPost['Dokumen']['id']);
                    $update = true;
                } else {

                    $model = new Dokumen;
                    if (!Yii::$app->user->can('admin')) {
                        $unitKerja = UnitKerja::findOne(Yii::$app->user->identity->unit_kerja_id);
                        $namaUnitKerja = $unitKerja->nama;
                        $model->unit_kerja_id = Yii::$app->user->identity->unit_kerja_id;
                    }
                }

                $model->attributes = $dataPost['Dokumen'];

                if (Yii::$app->user->can('admin')) {
                    $namaUnitKerja = UnitKerja::findOne($model->unit_kerja_id)->nama;
                }
                $model->s3 = UploadedFile::getInstance($model, 's3');
                $jenis = MasterJenis::findOne($model->jenis_id);

                if ($model->s3) {
                    $curdate = date('d-m-y');

                    $file_name = $model->nama . '-' . $curdate;
                    $s3path = $model->s3->tempName;
                    $s3type = $model->s3->type;
                    $key = 'dokumen' . '/' . $namaUnitKerja . '/' . $jenis->nama . '/' . $file_name . '.pdf';

                    $insert = $s3new->putObject([
                        'Bucket' => 'spmi',
                        'Key' => $key,
                        'Body' => 'Body',
                        'SourceFile' => $s3path,
                        'ContenType' => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('spmi', $key);
                    $model->s3 = $plainUrl;

                    if ($model->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Data tersimpan");
                        return $this->redirect(['personal']);
                    }
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dokumen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionDownload($id)
    {

        $model = Dokumen::findOne($id);

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash("danger", "please login first");
            return $this->redirect(['site/login']);
        }

        // if ($model->status == '2') {
        //     Yii::$app->session->setFlash("danger", "Sorry, this document is private");
        //     return $this->redirect(['search']);
        // }

        $filePath = $model->s3;
        if (empty($filePath)) {
            return 'dokumen tidak tersedia';
        }
        $file = file_get_contents($filePath);
        $curdate = date('d-m-y');
        $filename = $model->nama . '-' . $curdate;
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . strlen($file));
        header('Accept-Ranges: bytes');
        echo $file;
        exit;
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->unit_kerja_id != Yii::$app->user->identity->unit_kerja_id) {
            Yii::$app->session->setFlash('danger', "You are not allowed to update this document");
            return $this->redirect(['index']);
        }

        $connection = \Yii::$app->db;
        $transcation = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];

        $s3new = new \Aws\S3\S3Client($s3config);
        $errors = [];
        // echo '<pre>';print_r($model);die;
        $s3Lama = $model->s3;

        try {

            $dataPost = $_POST;

            if ($model->load(Yii::$app->request->post())) {
                $dokumenFile = UploadedFile::getInstance($model, 's3');
                $model->nama = $dataPost['Dokumen']['nama'];
                $model->jenis_id = $dataPost['Dokumen']['jenis_id'];
                $model->status = $dataPost['Dokumen']['status'];
                $model->keterangan = $dataPost['Dokumen']['keterangan'];

                if (isset($dokumenFile)) {
                    $model->s3 = $dokumenFile;
                    $jenis = MasterJenis::findOne($model->jenis_id);

                    if (!Yii::$app->user->can('admin')) {
                        $unitKerja = UnitKerja::findOne(Yii::$app->user->identity->unit_kerja_id);
                        $namaUnitKerja = $unitKerja->nama;
                    } else {
                        $namaUnitKerja = UnitKerja::findOne($model->unit_kerja_id)->nama;
                    }

                    if ($model->s3) {
                        $curdate    = date('d-m-y');

                        $file_name = $model->nama . '-' . $curdate;
                        $s3path = $model->s3->tempName;
                        $s3type = $model->s3->type;
                        $key = 'dokumen' . '/' . $namaUnitKerja . '/' . $jenis->nama . '/' . $file_name . '.pdf';

                        $insert = $s3new->putObject([
                            'Bucket'        => 'spmi',
                            'Key'           => $key,
                            'Body'          => 'Body',
                            'SourceFile'    => $s3path,
                            'ContenType'    => $s3type,
                        ]);

                        $plainUrl = $s3new->getObjectUrl('spmi', $key);
                        $model->s3 = $plainUrl;
                    }
                } else {
                    $model->s3 = $s3Lama;
                }

                if ($model->save()) {
                    $transcation->commit();

                    Yii::$app->session->setFlash('success', "Data dipebarui");
                    return $this->redirect(['personal']);
                }
            }
        } catch (\Exception $e) {
            $transcation->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transcation->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dokumen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->unit_kerja_id == Yii::$app->user->identity->unit_kerja_id) {
            $asesmen = Asesmen::findAll(['dokumen_id' => $id]);
            foreach ($asesmen as $a) {
                // $asesmen->dokumen_id = null;
                $a->delete();
            }
            $model->delete();
            Yii::$app->session->setFlash('success', "Data successfully deleted");
        } else {
            Yii::$app->session->setFlash('danger', "You are not allowed to delete this document");
        }

        return $this->redirect(['personal']);
    }

    public function actionUploadBuktiMonev()
    {
        $connection = \Yii::$app->db;
        $transcation = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];

        $s3new = new \Aws\S3\S3Client($s3config);

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            $dataPost = $_POST;

            $model = new Dokumen();
            $model->attributes = $dataPost;
            $jenis = MasterJenis::findOne($model->jenis_id);
            $monevObjek = MonevUnitKerjaObjek::findOne($dataPost['monev_unit_kerja_objek_id']);

            $modelMonevBukti = new MonevBukti();
            $modelMonevBukti->id = MyHelper::gen_uuid();
            $modelMonevBukti->attributes = $dataPost;
            $modelMonevBukti->monev_unit_kerja_id = $monevObjek->monev_unit_kerja_id;

            if (!empty($file)) {

                $curdate = date('d-m-y');

                $file_name = $curdate . '-' . MyHelper::formatTextLowerDash($dataPost['nama']);
                $s3path = $file->tempName;
                $s3type = $file->type;
                $key = 'dokumen' . '/' . $monevObjek->monevUnitKerja->unitKerja->nama . '/' . $jenis->nama . '/' . $file_name . '.pdf';

                $insert = $s3new->putObject([
                    'Bucket' => 'spmi',
                    'Key' => $key,
                    'Body' => 'Body',
                    'SourceFile' => $s3path,
                    'ContenType' => $s3type,
                ]);

                $plainUrl = $s3new->getObjectUrl('spmi', $key);
                $model->s3 = $plainUrl;
                $model->unit_kerja_id = Yii::$app->user->identity->unit_kerja_id;
                $model->status = 2;

                if ($model->save()) {
                    $modelMonevBukti->dokumen_id = $model->id;
                    if ($modelMonevBukti->save()) {
                        $transcation->commit();
                        Yii::$app->session->setFlash('success', "Data berhasil ditambahkan");
                        return $this->redirect(['/monev-unit-kerja-objek/' . $modelMonevBukti->monevUnitKerjaObjek->jenis_objek, 'id' => $modelMonevBukti->monev_unit_kerja_id]);
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    public function actionAjaxGetDokumenAsesmen()
    {

        $q = $_GET['term'];
        $keywords = explode(" ", $q);
        $out = [];

        $query = Dokumen::find()->joinWith(['unitKerja as uk']);

        foreach ($keywords as $keyword) {
            $query->andWhere([
                'or',
                ['like', 'dokumen.nama', $keyword],
                ['like', 'uk.singkatan', $keyword],
            ]);
        }

        $query->andWhere([
            'or',
            ['unit_kerja_id' => Yii::$app->user->identity->unit_kerja_id],
            ['dokumen.status' => 1],
        ]);

        $data = $query->all();



        if (!empty($data)) {
            foreach ($data as $d) {
                $out[] = [
                    'id' => $d->id,
                    'label' => '[ ' . $d->unitKerja->singkatan . ' ] ' . $d->nama,
                ];
            }
        } else {
            $out[] = [
                'id' => 0,
                'label' => 'Dokumen tidak ditemukan',

            ];
        }

        echo \yii\helpers\Json::encode($out);

        die();
    }

    /**
     * Finds the Dokumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dokumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dokumen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
