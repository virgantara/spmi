<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\CapaianHasil;
use app\models\CapaianHasilSearch;
use app\models\CapaianKinerja;
use app\models\CapaianKinerjaSearch;
use app\models\Periode;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CapaianHasilController implements the CRUD actions for CapaianHasil model.
 */
class CapaianHasilController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CapaianHasil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CapaianHasilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInput()
    {
        $dataPeriode = Periode::find()->orderBy(['tahun' => SORT_DESC])->all();

        return $this->render('input', [
            'dataPeriode' => $dataPeriode,
        ]);
    }

    public function actionInputCapaian($periode_id)
    {
        $periode        = Periode::findOne($periode_id);
        $searchModel    = new CapaianKinerjaSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('input_capaian', [
            'periode'       => $periode,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionMasterInputCapaian($periode_id)
    {
        $periode        = Periode::findOne($periode_id);
        $searchModel    = new CapaianKinerjaSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('master_input_capaian', [
            'periode'       => $periode,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }


    /**
     * Displays a single CapaianHasil model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($periode_id)
    {
        $periode = Periode::findOne($periode_id);

        $list_capaian = CapaianKinerja::find()->joinWith(['capaianPeriodes as cp'])->where(['cp.periode_id' => $periode_id])->orderBy(['no_indikator' => "SORT_DESC"])->all();

        $capaianHasil = CapaianHasil::find()->where([
            'periode_id' => $periode_id,
            'unit_kerja_id' => MyHelper::adminOnly(Yii::$app->user->identity->unit_kerja_id, 23),
        ])->all();

        foreach ($list_capaian as $capaian) {

            $capaianHasil = CapaianHasil::find()->where([
                'capaian_kinerja_id' => $capaian->id,
                'periode_id' => $periode_id,
                'unit_kerja_id' => MyHelper::adminOnly(Yii::$app->user->identity->unit_kerja_id, 23),
            ])->one();

            $data[$capaian->id] = [
                'capaian' => $capaian,
                'capaianHasil' => $capaianHasil,
            ];

            $results = $data;
        }

        return $this->render('view', [
            'periode' => $periode,
            'capaian' => $results,
        ]);
    }

    /**
     * Creates a new CapaianHasil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CapaianHasil();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxGetCapaianKinerja()
    {

        if (Yii::$app->request->isPost) {
            $data = $_POST['dataPost'];

            $list_capaian = CapaianKinerja::find()->joinWith(['capaianPeriodes as cp'])->where(['cp.periode_id' => $data['periode_id']])->orderBy(['no_indikator' => "SORT_DESC"])->all();

            $results = [];

            foreach ($list_capaian as $capaian) {

                $data_capaian = CapaianHasil::find()->where([
                    'periode_id' => $data['periode_id'],
                    'capaian_kinerja_id' => $capaian->id,
                    'unit_kerja_id' => MyHelper::adminOnly(Yii::$app->user->identity->unit_kerja_id, 23),
                ])->one();

                $results[$capaian->id] = [
                    'sasaran_strategis' => $capaian->sasaran_stategis,
                    'no_indikator' => $capaian->no_indikator,
                    'indikator_kinerja' => $capaian->indikator_kinerja,
                    'satuan' => $capaian->satuan,
                    'target_unida' => $capaian->target_unida,
                    'bidang_wr' => $capaian->bidang_wr,
                    'unit_kerja_id' => MyHelper::adminOnly(Yii::$app->user->identity->unit_kerja_id, 23),
                    'periode_id' => $data['periode_id'],
                    'realisasi_target' => isset($data_capaian) ? $data_capaian->realisasi_target : 0,
                    'capaian_target' => isset($data_capaian) ? $data_capaian->capaian_target : 0,
                ];
            }

            // header('Content-Type: application/json');
            echo json_encode($results);
            // echo '</pre>';
        }

        die();
    }

    public function actionAjaxRealisasiCapaian()
    {
        if (!empty($_POST['dataPost'])) {
            $dataPost = $_POST['dataPost'];
            $data = $dataPost[0];
            $statusData = $dataPost[1];
        }
        $transaction = \Yii::$app->db->beginTransaction();
        $errors = '';
        $results = [];
        $periode_aktif = Periode::getStatusAktif()->id;

        try {

            if (!empty($dataPost) && $data[0]['periode_id'] == $periode_aktif) {

                CapaianHasil::deleteAll([
                    'unit_kerja_id' => $statusData['master'] == 1 ? $data[0]['unit_kerja_id'] : MyHelper::adminOnly(Yii::$app->user->identity->unit_kerja_id, 23),
                    'periode_id' => $data[0]['periode_id'],
                ]);

                $batchCapaianHasil = Yii::$app->db->createCommand()->batchInsert('capaian_hasil', [
                    'unit_kerja_id',
                    'capaian_kinerja_id',
                    'periode_id',
                    'capaian_target',
                    'realisasi_target',
                ], $data);

                if ($batchCapaianHasil->execute()) {
                    $transaction->commit();
                    $results = [
                        'code'      => 200,
                        'message'   => 'Data successfully updated'
                    ];
                } else {
                    $errors .= MyHelper::logError($batchCapaianHasil);
                    throw new \Exception;
                }
            } else {

                $results = [
                    'code'      => 400,
                    'message'   => 'Periode tidak aktif'
                ];
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();

            $results = [
                'code' => 501,
                'message' => $errors
            ];
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors .= $e->getMessage();

            $results = [
                'code' => 502,
                'message' => $errors
            ];
            $transaction->rollBack();
        }

        echo json_encode($results);

        die();
    }

    public function actionAjaxGetMasterCapaian()
    {

        if (Yii::$app->request->isPost) {
            $data = $_POST['dataPost'];

            $list_capaian = CapaianKinerja::find()->joinWith(['capaianPeriodes as cp'])->where(['cp.periode_id' => $data['periode_id']])->orderBy(['no_indikator' => "SORT_DESC"])->all();

            $results = [];

            foreach ($list_capaian as $capaian) {

                $data_capaian = CapaianHasil::find()->where([
                    'periode_id' => $data['periode_id'],
                    'capaian_kinerja_id' => $capaian->id,
                    'unit_kerja_id' => $data['satker_id'],
                ])->one();

                $results[$capaian->id] = [
                    'sasaran_strategis' => $capaian->sasaran_stategis,
                    'no_indikator' => $capaian->no_indikator,
                    'indikator_kinerja' => $capaian->indikator_kinerja,
                    'satuan' => $capaian->satuan,
                    'target_unida' => $capaian->target_unida,
                    'bidang_wr' => $capaian->bidang_wr,
                    'unit_kerja_id' => $data['satker_id'],
                    'periode_id' => $data['periode_id'],
                    'realisasi_target' => isset($data_capaian) ? $data_capaian->realisasi_target : 0,
                    'capaian_target' => isset($data_capaian) ? $data_capaian->capaian_target : 0,
                ];
            }

            // header('Content-Type: application/json');
            echo json_encode($results);
            // echo '</pre>';
        }

        die();
    }

    /**
     * Updates an existing CapaianHasil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CapaianHasil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CapaianHasil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CapaianHasil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CapaianHasil::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
