<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\Pengelompokan;
use app\models\PengelompokanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PengelompokanController implements the CRUD actions for Pengelompokan model.
 */
class PengelompokanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengelompokan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengelompokanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengelompokan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengelompokan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pengelompokan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxAdd()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;

        $dataPost = $_POST;

        try {

            if (!empty($dataPost)) {

                $pengelompokan = Pengelompokan::findOne($dataPost['id']);

                if (!isset($pengelompokan)) {

                    $model = new Pengelompokan();
                    $model->unit_kerja_id   = Yii::$app->user->identity->unit_kerja_id;
                    $model->attributes      = $dataPost;

                    if ($model->save()) {

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                } else {
                    $model = $this->findModel($pengelompokan->id);
                    $model->attributes = $dataPost;

                    if ($model->save()) {

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully updated'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing Pengelompokan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pengelompokan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['dokumen/personal']);
    }

    /**
     * Finds the Pengelompokan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengelompokan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengelompokan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
