<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\AmiUnit;
use Yii;
use app\models\Persetujuan;
use app\models\PersetujuanCek;
use app\models\PersetujuanSearch;
use Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PersetujuanController implements the CRUD actions for Persetujuan model.
 */
class PersetujuanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Persetujuan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersetujuanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionAjaxAddPersetujuan()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $dataPost = $_POST;

        try {

            if (!empty($dataPost)) {

                if (!empty($dataPost['ami_unit_id']) && !empty($dataPost['unit_kerja_id'])) {

                    $persetujuan = Persetujuan::findOne([
                        'ami_unit_id' => $dataPost['ami_unit_id'],
                        'kode_dokumen' => $dataPost['kode_dokumen'],
                        'is_auditee' => 1,
                    ]);

                    if (empty($persetujuan)) {

                        $persetujuan = new Persetujuan();
                        $persetujuan->attributes = $dataPost;
                        $persetujuan->datetime = date("Y-m-d H:i:s");
                        $persetujuan->is_auditee = 1;

                        if ($persetujuan->save()) {

                            $amiUnit = AmiUnit::findOne($dataPost['ami_unit_id']);

                            PersetujuanCek::inputDokumenTervalidasi($amiUnit->id, 'AMI', $dataPost['kode_dokumen']);

                            $transaction->commit();

                            $results = [
                                'code' => 200,
                                'message' => 'Dokumen berhasil disetujui'
                            ];
                        } else {
                            $errors .= MyHelper::logError($persetujuan);
                            throw new \Exception;
                        }
                    }
                } elseif (!empty($dataPost['ami_unit_id']) && !empty($dataPost['auditor_id'])) {

                    $persetujuan = Persetujuan::findOne([
                        'ami_unit_id' => $dataPost['ami_unit_id'],
                        'auditor_id' => $dataPost['auditor_id'],
                        'kode_dokumen' => $dataPost['kode_dokumen'],
                    ]);

                    if (empty($persetujuan)) {

                        $persetujuan = new Persetujuan();
                        $persetujuan->attributes = $dataPost;
                        $persetujuan->datetime = date("Y-m-d H:i:s");

                        if ($persetujuan->save()) {

                            $amiUnit = AmiUnit::findOne($dataPost['ami_unit_id']);

                            PersetujuanCek::inputDokumenTervalidasi($amiUnit->id, 'AMI', $dataPost['kode_dokumen']);

                            $transaction->commit();

                            $results = [
                                'code' => 200,
                                'message' => 'Dokumen berhasil disetujui'
                            ];
                        } else {
                            $errors .= MyHelper::logError($persetujuan);
                            throw new \Exception;
                        }
                    } else {
                        $results = [
                            'code' => 400,
                            'message' => 'Dokumen ini sudah disetujui'
                        ];
                    }
                }
            } else {
                $errors .= 'Oops, anda tidak memiliki akses persetujuan..';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            // $errors .= 'Oops, anda tidak memiliki akses persetujuan..';
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }



    public function actionAjaxAuthApp()
    {
        $results = [];

        $dataPost = $_POST['dataPost'];
        // print_r($dataPost);exit;

        $persetujuan = Persetujuan::find()->where([
            'ami_unit_id' => $dataPost['ami_unit_id'],
            'auditor_id' => $dataPost['auditor_id'],
            'dokumen_id' => $dataPost['dokumen_id'],
        ])->one();

        if (empty($persetujuan)) {
            $persetujuan = new Persetujuan();
            $persetujuan->attributes = $dataPost;

            if ($persetujuan->save()) {
                $results = [
                    'code' => 200,
                    'message' => 'Dokumen Disetujui'
                ];
            } else {
                $error = \app\helpers\MyHelper::logError($persetujuan);
                $results = [
                    'code' => 500,
                    'message' => $error
                ];
            }
        } else {
            if ($dataPost['checked'] == '0') {
                $persetujuan->delete();
                $results = [
                    'code' => 200,
                    'message' => 'Persetujuan dihapus'
                ];
            }
        }

        echo json_encode($results);
        die();
    }


    /**
     * Displays a single Persetujuan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Persetujuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Persetujuan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Persetujuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Persetujuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Persetujuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Persetujuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Persetujuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
