<?php

namespace app\controllers;

use Yii;
use app\models\Akreditasi;
use app\models\AkreditasiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use yii\web\UploadedFile;

/**
 * AkreditasiController implements the CRUD actions for Akreditasi model.
 */
class AkreditasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create','update','delete','index','view','ajax-add','upload-sk'],
                'rules' => [
                    
                    [
                        'actions' => ['create','update','delete','index','view','ajax-add','upload-sk'],
                        'allow' => true,
                        'roles' => ['theCreator','admin']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDownloadSk($id)
    {
        $model = $this->findModel($id);

        if ($model && isset($model->file_path)) {
            $pdfContent = base64_decode($model->file_path_base64);
            return Yii::$app->response->sendContentAsFile($pdfContent, $model->id.'.pdf', [
                'mimeType' => 'application/pdf',
                'inline' => true
            ]);
        }

        throw new \yii\web\NotFoundHttpException('The requested document does not exist.');
    }

    public function actionAjaxAdd()
    {
        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        

        try 
        {
            $dataPost = $_POST;
            
            $model = new Akreditasi;
            $model->attributes = $dataPost;
            $model->id = MyHelper::gen_uuid();
            if(!$model->save()) {
                $errors .= MyHelper::logError($model);
                throw new \Exception;
            }

            $transaction->commit();
            $results = [
                'code' => 200,
                'message' => 'Data successfully added'
            ];
        } 

        catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
            
            
        } 
        echo json_encode($results);
        exit;
    }


    /**
     * Lists all Akreditasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AkreditasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Akreditasi model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Akreditasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Akreditasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Akreditasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUploadSk($id)
    {
        $model = $this->findModel($id);
        $list_lembaga_akreditasi = \app\models\LembagaAkreditasi::find()->where(['status_aktif' => '1'])->orderBy(['tingkat'=>SORT_ASC])->all();
        $file_path = $model->file_path;
        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();

            $s3config = Yii::$app->params['s3'];

            $s3 = new \Aws\S3\S3Client($s3config);
            $errors = '';

            try {
            
                $model->file_path = UploadedFile::getInstance($model, 'file_path');

                if ($model->file_path) {
                    $file_name = $model->id.'.' . $model->file_path->extension;
                    $s3_path = $model->file_path->tempName;
                    $mime_type = $model->file_path->type;
                    $key = 'akreditasi/sk/'.$file_name;

                    $insert = $s3->putObject([
                        'Bucket' => 'spmi',
                        'Key'    => $key,
                        'Body'   => 'This is the Body',
                        'SourceFile' => $s3_path,
                        'ContentType' => $mime_type
                    ]);


                    $plainUrl = $s3->getObjectUrl('spmi', $key);
                    $model->file_path = $plainUrl;

                    $base64 = base64_encode(file_get_contents($s3_path));
                    $model->file_path_base64 = $base64;
                }

                if(empty($model->file_path))
                    $model->file_path = $file_path;

                if ($model->save()) {
                    $transaction->commit();

                    Yii::$app->session->setFlash('success', "Data tersimpan");
                    return $this->redirect(['prodi/view', 'id' => $model->prodi_id]);
                }
                
            } catch (\Exception $e) {
                $transaction->rollBack();
                $errors .= $e->getMessage();
                Yii::$app->session->setFlash('danger', $errors);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'list_lembaga_akreditasi' => $list_lembaga_akreditasi
        ]);
    }


    /**
     * Deletes an existing Akreditasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        Yii::$app->session->setFlash('success', "Data successfully deleted");
        $this->findModel($id)->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Akreditasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Akreditasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Akreditasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
