<?php

namespace app\controllers;

use Yii;
use app\models\AmanahKinerja;
use app\models\AmanahKinerjaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\AmanahKinerjaMaster;
use app\models\AmanahKinerjaUnitKerja;
use app\models\UnitKerja;

/**
 * AmanahKinerjaController implements the CRUD actions for AmanahKinerja model.
 */
class AmanahKinerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete'],
                'rules' => [
                    [
                        'actions' => [
                            'index'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'view',
                            'index',
                            'delete',
                            'create',
                            'update'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmanahKinerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmanahKinerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        if (Yii::$app->request->post('hasEditable')) {

            $id = Yii::$app->request->post('editableKey');
            $model = AmanahKinerja::findOne($id);

            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['AmanahKinerja']);
            $post = ['AmanahKinerja' => $posted];

            if ($model->load($post)) {

                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmanahKinerja model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $unitKerja = UnitKerja::find()->where(['jenis' => ['satker', 'fakultas']])->orderBy(['nama' => SORT_ASC])->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'unitKerja' => $unitKerja,
        ]);
    }

    /**
     * Creates a new AmanahKinerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmanahKinerja();

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $model->id = MyHelper::gen_uuid();
                $model->save();

                $unitKerjas = UnitKerja::find()->where(['jenis' => ['satker', 'fakultas']])->orderBy(['nama' => SORT_ASC])->all();

                foreach ($unitKerjas as $unitKerja) {
                    $listAmanah = AmanahKinerjaMaster::find()->where(['jenis_amanah_kinerja' => $unitKerja->jenis, 'status_aktif' => 1]);

                    if ($unitKerja->jenis == 'satker') {
                        $listAmanah->andWhere(['unit_kerja_id' => $unitKerja->id]);
                    }

                    $listAmanah = $listAmanah->all();

                    foreach ($listAmanah as $amanah) {
                        $modelAmanahKinerja = new AmanahKinerjaUnitKerja();

                        $modelAmanahKinerja->id = MyHelper::gen_uuid();
                        $modelAmanahKinerja->unit_kerja_id = $unitKerja->id;
                        $modelAmanahKinerja->amanah_kinerja_id = $model->id;
                        $modelAmanahKinerja->amanah_kinerja_master_id = $amanah->id;
                        $modelAmanahKinerja->target = $amanah->target_umum;

                        if ($modelAmanahKinerja->save()) {
                        } else {
                            $error = MyHelper::logError($model);
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                }

                $transaction->commit();

                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (\Throwable $th) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AmanahKinerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSync()
    {
        $results = [];
        $dataPost = $_POST;
        $jumlahDelete = 0;
        $jumlahTambah = 0;

        $model = AmanahKinerja::findOne($dataPost['amanah_id']);

        if ($model->status_aktif ==  0) {
            $results = [
                'code' => 500,
                'message' => 'Periode amanah kinerja Non-Aktif'
            ];
            echo json_encode($results);
            exit;
        }

        $amanahKinerjas = AmanahKinerjaUnitKerja::findAll(['amanah_kinerja_id' => $dataPost['amanah_id']]);

        $amanahKinerjaUnit = [];
        $amanahSudahAda = [];

        foreach ($amanahKinerjas as $amanahKinerja) {
            $amanahKinerjaUnit[$amanahKinerja->id] = [
                'id' => $amanahKinerja->id,
                'amanah_kinerja_id' => $amanahKinerja->amanah_kinerja_id,
                'amanah_kinerja_master_id' => $amanahKinerja->amanah_kinerja_master_id
            ];
        }

        foreach ($amanahKinerjaUnit as $k => $v) {
            $kinerjaMaster = AmanahKinerjaMaster::findOne(['id' => $v['amanah_kinerja_master_id'], 'status_aktif' => 1]);

            if (!isset($kinerjaMaster)) {
                unset($amanahKinerjaUnit[$k]);
                $jumlahDelete++;

                $amanahUnitKerja = AmanahKinerjaUnitKerja::findOne($k);
                $amanahUnitKerja->delete();
            } else {
                array_push($amanahSudahAda, $v['amanah_kinerja_master_id']);
            }
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            if ($model) {

                $unitKerjas = UnitKerja::find()->where(['jenis' => ['satker', 'fakultas']])->orderBy(['nama' => SORT_ASC])->all();

                foreach ($unitKerjas as $unitKerja) {
                    $listAmanah = AmanahKinerjaMaster::find()->where(['jenis_amanah_kinerja' => $unitKerja->jenis, 'status_aktif' => 1]);
                    $listAmanah->andWhere(['not in', 'id', $amanahSudahAda]);

                    if ($unitKerja->jenis == 'satker') {
                        $listAmanah->andWhere(['unit_kerja_id' => $unitKerja->id]);
                    }

                    $listAmanah = $listAmanah->all();

                    foreach ($listAmanah as $amanah) {
                        $jumlahTambah++;
                        $modelAmanahKinerja = new AmanahKinerjaUnitKerja();

                        $modelAmanahKinerja->id = MyHelper::gen_uuid();
                        $modelAmanahKinerja->unit_kerja_id = $unitKerja->id;
                        $modelAmanahKinerja->amanah_kinerja_id = $model->id;
                        $modelAmanahKinerja->amanah_kinerja_master_id = $amanah->id;
                        $modelAmanahKinerja->target = $amanah->target_umum;

                        if ($modelAmanahKinerja->save()) {
                        } else {
                            $error = MyHelper::logError($model);
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                }

                $transaction->commit();
            }
        } catch (\Throwable $th) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
        }

        $results = [
            'code' => 200,
            'message' => $jumlahDelete + $jumlahTambah . ' data berhasil di singkonkan'
        ];

        echo json_encode($results);
        exit;
    }

    /**
     * Deletes an existing AmanahKinerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $amanahUnits = AmanahKinerjaUnitKerja::findAll(['amanah_kinerja_id' => $id]);
        $model = $this->findModel($id);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($amanahUnits as $amanahUnit) {
                if (!$amanahUnit->delete()) {
                    throw new \Exception('Failed to delete AmanahKinerjaUnitKerja');
                }
            }

            if (!$model->delete()) {
                throw new \Exception('Failed to delete AmanahKinerja');
            }

            $transaction->commit();
            $result = [
                'code' => 200,
                'message' => Yii::t('app', 'Data berhasil dihapus'),
            ];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $result = [
                'code' => 500,
                'message' => Yii::t('app', 'Data gagal dihapus'),
            ];
        }

        echo json_encode($result);
        exit();
    }

    /**
     * Finds the AmanahKinerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AmanahKinerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmanahKinerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
