<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use Yii;
use app\models\Jadwal;
use app\models\JadwalSearch;
use Throwable;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * JadwalController implements the CRUD actions for Jadwal model.
 */
class JadwalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jadwal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JadwalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jadwal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jadwal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jadwal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSetNormal()
    {
        $dataPost = $_POST;
        $errors = '';

        $data = [
            [
                'urutan' => 1,
                'waktu_mulai' => '08:45:00',
                'waktu_selesai' => '09:00:00',
                'pelaksana' => 'Auditor',
                'acara' => 'Auditor Hadir di Lokasi',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 2,
                'waktu_mulai' => '09:00:00',
                'waktu_selesai' => '09:15:00',
                'pelaksana' => 'Auditor, Prodi, UPPS, UPM, dan GPM',
                'acara' => 'Rapat Pembukaan',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 3,
                'waktu_mulai' => '09:15:00',
                'waktu_selesai' => '09:45:00',
                'pelaksana' => 'Auditor, Prodi, UPPS, UPM, dan GPM',
                'acara' => 'Pelaksanaan Audit: Konfirmasi Dokumen',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 4,
                'waktu_mulai' => '09:45:00',
                'waktu_selesai' => '10:30:00',
                'pelaksana' => 'Auditor, Prodi, dan GPM ',
                'acara' => 'Pelaksanaan Audit: Kriteria 6 dan 9',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 5,
                'waktu_mulai' => '10:30:00',
                'waktu_selesai' => '11:00:00',
                'pelaksana' => 'Auditor, UPPS, dan UPM',
                'acara' => 'Pelaksanaan Audit: Kriteria 1, 2, 3, 4, 5, 7, dan 8',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 6,
                'waktu_mulai' => '11:00:00',
                'waktu_selesai' => '11:15:00',
                'pelaksana' => 'Tim Auditor',
                'acara' => 'Musyawarah Tim Auditor',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 7,
                'waktu_mulai' => '11:15:00',
                'waktu_selesai' => '11:30:00',
                'pelaksana' => 'Auditor dan Prodi',
                'acara' => 'Peyampaian hasil/temuan AMI dan Penandatangan Berita Acara AMI',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 8,
                'waktu_mulai' => '11:30:00',
                'waktu_selesai' => '11:40:00',
                'pelaksana' => 'Auditor, Prodi, UPPS, UPM, dan GPM',
                'acara' => 'Rapat Penutupan',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
            [
                'urutan' => 9,
                'waktu_mulai' => '11:40:00',
                'waktu_selesai' => '12:00:00',
                'pelaksana' => 'Auditor, Prodi, UPPS, UPM, dan GPM',
                'acara' => 'Ramah Tamah',
                'ami_unit_id' => $dataPost['ami_unit_id'],
            ],
        ];

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            $batchJadwal = Yii::$app->db->createCommand()->batchInsert('jadwal', [
                'urutan',
                'waktu_mulai',
                'waktu_selesai',
                'pelaksana',
                'acara',
                'ami_unit_id',
            ], $data);

            if ($batchJadwal->execute()) {
                $transaction->commit();
                $results = [
                    'code'      => 200,
                    'message'   => 'Data successfully added'
                ];
            } else {
                $errors .= MyHelper::logError($batchJadwal);
                throw new \Exception;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }

        echo json_encode($results);
        die();
    }

    public function actionAjaxAddJadwal()
    {
        $results    = [];
        $errors     = '';
        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $model = new Jadwal();
                $model->attributes = $dataPost;

                if ($model->save()) {
                    $results    = [
                        'code'      => 200,
                        'message'   => "Yeay, Data berhasil ditambahkan",
                    ];
                } else {
                    $errors     = MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Opps, data yang dikirim kosong !';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'      => 500,
                'message'   => $errors,
            ];
        } catch (\Throwable $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'      => 500,
                'message'   => $errors,
            ];
        }

        echo json_encode($results);

        die();
    }

    /**
     * Updates an existing Jadwal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jadwal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteRencana($id)
    {
        $model = $this->findModel($id);
        $ami_unit_id = $model->ami_unit_id;
        $model->delete();
        return $this->redirect(['asesmen/rencana', 'id' => $ami_unit_id]);
    }

    /**
     * Finds the Jadwal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jadwal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jadwal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
