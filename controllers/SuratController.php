<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiAuditorSearch;
use app\models\AmiTujuanAudit;
use app\models\AmiUnit;
use app\models\Auditor;
use app\models\AuditorSearch;
use app\models\Bpm;
use app\models\Jadwal;
use app\models\JenjangMap;
use app\models\Kriteria;
use app\models\Persetujuan;
use app\models\RuangLingkup;
use app\models\Temuan;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * SuratController implements the CRUD actions for Surat model.
 */
class SuratController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Periode models.
     * @return mixed
     */

    public function actionIndex()
    {
        $list_auditor   = [];
        $ami_id         = [];

        $query          = AmiAuditor::find();
        $query->joinWith(['ami.ami as aa']);

        if (!empty($_GET['auditor_id']))
            $query->where(['auditor_id' => $_GET['auditor_id']]);

        if (!empty($_GET['ami_id']))
            $query->andWhere(['aa.id' => $_GET['ami_id']]);

        if (!empty($_GET['ami_id']) || !empty($_GET['ami_id']))
            $auditors   = $query->all();

        if (isset($auditors)) {
            foreach ($auditors as $auditor) {
                $auditor = [
                    'id'            => $auditor->ami->ami_id . $auditor->auditor_id,
                    'auditor_id'    => $auditor->auditor_id,
                    'ami_id'        => $auditor->ami->ami_id,
                ];
                // $test[] = $auditor;
                $list_auditor[$auditor['id']]   = $auditor;
                asort($list_auditor);
            }
        }



        return $this->render('index', [
            'list_auditor'  => $list_auditor,
        ]);
    }

    public function actionHasilAmi()
    {
        $list_auditor   = [];
        $ami_id         = [];
        $query          = Ami::find();
        $ami            = $query->one();


        return $this->render('hasil', [
            'ami'       => $ami,
        ]);
    }

    public function actionKehadiranAmi($ami_unit_id)
    {
        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit        = AmiUnit::findOne($ami_unit_id);
            $jumlahAuditor  = AmiAuditor::find()->where(['ami_id' => $ami_unit_id])->count();
            $jenjang        = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();


            echo $this->renderPartial('kehadiran_ami', [
                'amiUnit'       => $amiUnit,
                'jumlahAuditor' => $jumlahAuditor + 1,
                'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath   = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $logoOri    = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $fontreg    = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($logoOri, 20, 13, 14);

            $pdf->writeHTML($data);
            $pdf->Output('Kehadiran-ami-' . $amiUnit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    // public function actionKehadiranRtm($rtm_id)
    // {
    //     try {
    //         ob_start();
    //         $this->layout = '';
    //         ob_start();

    //         $ami_unit_id = 48;

    //         // SUMBER DATA
    //         $amiUnit = AmiUnit::findOne($ami_unit_id);
    //         $jumlahAuditor  = AmiAuditor::find()->where(['ami_id' => $ami_unit_id])->count();
    //         $jenjang = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();

    //         $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    //         $pdf->SetPrintHeader(false);
    //         $pdf->SetPrintFooter(false);
    //         $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
    //         $logoOri = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
    //         $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

    //         $pdf->SetFont($fontreg, '', 10);
    //         $pdf->AddPage();
    //         $pdf->Image($logoOri, 20, 13, 14);

    //         echo $this->renderPartial('kehadiran_rtm', [
    //             'amiUnit'       => $amiUnit,
    //             'jumlahAuditor' => $jumlahAuditor + 1,
    //             'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
    //         ]);

    //         // Import the existing PDF file
    //         $existingPdfUrl = 'http://s3.unida.gontor.ac.id:9000/spmi/dokumen/Kebijakan/Pedoman%20Audit%20Mutu%20Internal%20%28AMI%29-0';
    //         $pdf->setSourceFile($existingPdfUrl);
    //         $tplIdx = $pdf->importPage(1);
    //         $pdf->useTemplate($tplIdx);

    //         // Merge the imported page into the main PDF
    //         $pageCount = $pdf->setSourceFile($existingPdfUrl);
    //         for ($pageNo = 2; $pageNo <= $pageCount; $pageNo++) {
    //             $tplIdx = $pdf->importPage($pageNo);
    //             $pdf->AddPage();
    //             $pdf->useTemplate($tplIdx);
    //         }

    //         $pdf->Output('Kehadiran-rtm-' . $amiUnit->unit->nama . '.pdf');
    //     } catch (\Exception $e) {
    //         echo $e;
    //         exit;
    //     }
    //     die();
    // }

    public function actionSubAuditor()
    {

        $out = [];

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $ami_id = $parents[0];
                $query = AmiAuditor::find();
                $query->joinWith(['ami.ami as aa'])->andWhere(['aa.id' => $ami_id]);
                $query->select(['auditor_id'])->distinct();
                $list_auditor = $query->all();

                foreach ($list_auditor as $item) {
                    $item = Auditor::findOne($item->auditor_id);
                    $out[$item->id] = [
                        'id' => $item->id,
                        'name' => $item->nama
                    ];
                }

                echo Json::encode(['output' => $out, 'selected' => '']);
                die();
            }
        }
    }

    public function actionRencana($ami_unit_id)
    {
        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $auditor = AmiAuditor::find()->where(['ami_id' => $ami_unit_id])->orderBy(['status_id' => SORT_ASC]);
            $listSusunan = Jadwal::find()->where(['ami_unit_id' => $ami_unit_id])->orderBy(['urutan' => SORT_ASC])->all();
            $ruangLingkup = RuangLingkup::findAll(['ami_unit_id' => $ami_unit_id]);
            $tujuanAudit = AmiTujuanAudit::findAll(['ami_unit_id' => $ami_unit_id]);

            echo $this->renderPartial('cetak_rencana', [
                'tujuanAudit' => $tujuanAudit,
                'ruangLingkup' => $ruangLingkup,
                'amiUnit' => $amiUnit,
                'jumlahAuditor' => (int)$auditor->count() + 1,
                'listSusunan' => $listSusunan,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath   = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $logoOri    = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $fontreg    = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();

            $pdf->Image($logoOri, 20, 13, 14);

            $listPersetujuan = ArrayHelper::map(Persetujuan::findAll([
                'ami_unit_id' => $amiUnit->id,
                'kode_dokumen' => 'rencana-ami'
            ]), function ($data) {
                $auditorId = $data->auditor_id ?? 0;
                $isAuditee = $data->is_auditee ?? 0;
                return $auditorId . '-' . $isAuditee;
            }, 'ami_unit_id');

            $style = [
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => false,
            ];

            if (isset($listPersetujuan['0-1']))
                $pdf->write2DBarcode(
                    MyHelper::printQrIsi(
                        'AMI',
                        'temuan-ami',
                        $amiUnit->unit->id,
                        $amiUnit->ami->tahun,
                        '#auditee'
                    ),
                    'QRCODE,Q',
                    25,
                    128,
                    0,
                    22,
                    $style,
                    'N',
                    false
                );

            $mulai = 70;
            $jarak = 48;

            foreach ($auditor->all() as $key => $value) {
                $position = $mulai + $jarak;
                if (isset($listPersetujuan[$value->auditor_id . '-0']))
                    $pdf->write2DBarcode(
                        MyHelper::printQrIsi(
                            'AMI',
                            'temuan-ami',
                            $amiUnit->unit->id,
                            $amiUnit->ami->tahun,
                            '#auditor' . $value->status_id
                        ),
                        'QRCODE,Q',
                        $position,
                        128,
                        0,
                        22,
                        $style,
                        'N'
                    );
            }

            $pdf->SetXY(12, 10);

            // Menulis konten HTML dari tengah halaman
            $pdf->writeHTML($data);

            $pdf->Output('dokumen-rencana-ami' . $amiUnit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionTugasAmiunit($ami_unit_id)
    {
        try {
            ob_start();
            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $jumlahAuditor = AmiAuditor::find()->where(['ami_id' => $ami_unit_id])->count();
            echo $this->renderPartial('surat_tugas', [
                'amiUnit' => $amiUnit,
                'nomorSurat' => $amiUnit->ami->nomor_surat_tugas,
                'jumlahAuditor' => $jumlahAuditor,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $kopSurat = Yii::getAlias('@webroot') . '/gentela/images/logo-full.png';
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($kopSurat, 78, 10, 50);

            $pdf->writeHTML($data);

            $pdf->SetY(170);
            $ttdData = $this->renderPartial('surat_tugas_ttd', [
                'amiUnit' => $amiUnit,
            ]);
            $pdf->writeHTML($ttdData);

            // Menambahkan QR code
            $style = [
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => false,
            ];

            $pdf->write2DBarcode(MyHelper::printQrIsi('AMI', 'surat-tugas-ami', $amiUnit->unit->id, $amiUnit->ami->tahun), 'QRCODE,Q', 142, 190, 0, 20, $style, 'N');

            $pdf->Output('surat-tugas-' . preg_replace('/\s+/', '-', strtolower($amiUnit->unit->nama)) . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }



    public function actionTugasAuditor($auditor_id, $ami_id)
    {
        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $auditor = Auditor::findOne($auditor_id);
            $ami = Ami::findOne($ami_id);
            $unitAudit = AmiAuditor::find();
            $unitAudit->joinWith(['ami as a']);
            $unitAudit->andWhere(['a.ami_id' => $ami_id]);
            $unitAudit = $unitAudit->andWhere(['auditor_id' => $auditor_id])->all();
            $kopSurat = Yii::getAlias('@webroot') . '/gentela/images/logo-full.png';
            $kopBawah = Yii::getAlias('@webroot') . '/gentela/images/kop-bawah.png';

            echo $this->renderPartial('surat_tugas_auditor', [
                'ami' => $ami,
                'auditor' => $auditor,
                'unitAudit' => $unitAudit,
            ]);

            $data = ob_get_clean();
            ob_start();

            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 10);
            $pdf->AddPage();
            $pdf->Image($kopSurat, 78, 10, 50);

            $pdf->writeHTML($data);

            // BARCODE
            $style = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
            );
            $pdf->write2DBarcode($ami->ketua_bpm . ' NIY.' . $ami->nidn_ketua_bpm . ' ' . $ami->tanggal_ami_mulai, 'QRCODE,Q', 120, 222, 0, 18, $style, 'N');

            $pdf->Output('surat-tugas-' . strtolower(str_replace([' ', '.'], '-', $auditor->nama)) . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionCetakHasilAmi($ami_unit_id)
    {
        $ami_unit = AmiUnit::findOne($ami_unit_id);

        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit        = AmiUnit::findOne($ami_unit_id);
            $listTemuan     = Temuan::find()->where(['ami_unit_id' => $ami_unit_id])->all();
            $auditor        = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
            ])->andWhere([
                'not', ['status_id' => 1]
            ]);
            $jenjang        = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();
            $listAuditor    = $auditor->all();
            $jumlahAuditor  = $auditor->count();
            $ketuaAuditor   = AmiAuditor::find()->where([
                'ami_id'    => $ami_unit_id,
                'status_id' => 1,
            ])->one();

            $listKriteria   = Kriteria::find();
            $listKriteria->joinWith(['pembagianKriterias as pk']);

            if ($amiUnit->unit->jenis == "prodi") {
                $listKriteria->where([
                    'jenis_unit' => 2
                ])
                    ->orWhere([
                        'jenis_unit' => 1
                    ])
                    ->orderBy(['urutan' => SORT_ASC]);
            }

            echo $this->renderPartial('cetak_hasil_ami', [
                'listTemuan'    => $listTemuan,
                'amiUnit'       => $amiUnit,
                'listKriteria'  => $listKriteria->all(),
                'jumlahAuditor' => $jumlahAuditor + 1,
                'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            // echo '<pre>';print_r($fontpath);die;
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 9);
            $pdf->AddPage();
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $pdf->Image($imgdata, 20, 13, 14);

            $pdf->writeHTML($data);
            $style = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
            );

            #DATA
            $mulai = 70;
            $jarak = 48;
            $data = [
                'ami_unit_id' => $amiUnit->id,
                'dokumen_id'  => 1,
            ];

            ##BARCODE PENANGGUNGJAWAB
            $pj['is_auditee'] = true;
            $dataPj = $data + $pj;
            if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, 73, 0, 20, $style, 'N');

            ##BARCODE KETUA AUDITOR
            $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
            $kAuditor['is_auditee'] = false;
            $dataKetua = $data + $kAuditor;
            if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, 73, 0, 20, $style, 'N');

            ##BARCODE ANGGOTA AUDITOR
            if (isset($listAuditor)) {
                $position = $mulai + $jarak;
                foreach ($listAuditor as $auditor) {
                    $aAuditor['auditor_id'] = $auditor->auditor_id;
                    $aAuditor['is_auditee'] = false;
                    $dataAuditor = $data + $aAuditor;
                    if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, 73, 0, 20, $style, 'N');
                    $position = $position + $jarak;
                }
            }

            $pdf->Output('HasilAmi_' . $ami_unit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionCetakBerita($ami_unit_id)
    {
        $ami_unit = AmiUnit::findOne($ami_unit_id);

        try {

            ob_start();

            $this->layout = '';
            ob_start();

            // SUMBER DATA
            $amiUnit = AmiUnit::findOne($ami_unit_id);
            $listTemuan = Temuan::find()->where(['ami_unit_id' => $ami_unit_id])->all();
            $auditor = AmiAuditor::find()->where([
                'ami_id' => $ami_unit_id,
            ]);

            $listKriteria = Kriteria::find()->where(['is_khusus' => 0])->orderBy(['urutan' => SORT_ASC]);
            $listKriteria->joinWith(['pembagianKriterias as p'])->andWhere(['p.jenis_unit' => MyHelper::getJenisUnitCode($amiUnit->unit->jenis)]);
            $listKriteria = $listKriteria->all();

            $dataTemuan = [];
            $temuan = Temuan::find();
            $temuan->joinWith(['asesmen.indikator as ai']);
            foreach ($listKriteria as $key => $kriteria) {
                $data = $temuan->where([
                    'ai.kriteria_id' => $kriteria->id,
                    // 'ai.ami_id' => $amiUnit->ami_id,
                    'temuan.ami_unit_id' => $amiUnit->id,
                ])->all();
                $dataRekomendasi = [];
                foreach ($data as $key => $d) {
                    $dataRekomendasi[] = $d;
                }
                $dataTemuan[$kriteria->id] = $dataRekomendasi;
            }

            echo $this->renderPartial('cetak_berita', [
                'jumlahAuditor' => $auditor->count() + 1,
                'listKriteria' => $listKriteria,
                'dataTemuan' => $dataTemuan,
                'amiUnit' => $amiUnit,
            ]);

            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';

            // echo '<pre>';print_r($fontpath);die;
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);

            $pdf->SetFont($fontreg, '', 9);
            $pdf->AddPage();
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
            $pdf->Image($imgdata, 20, 13, 14);

            $listPersetujuan = ArrayHelper::map(Persetujuan::findAll([
                'ami_unit_id' => $amiUnit->id,
                'kode_dokumen' => 'berita-acara-ami'
            ]), function ($data) {
                $auditorId = $data->auditor_id ?? 0;
                $isAuditee = $data->is_auditee ?? 0;
                return $auditorId . '-' . $isAuditee;
            }, 'ami_unit_id');

            $style = [
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => false,
            ];

            if (isset($listPersetujuan['0-1']))
                $pdf->write2DBarcode(
                    MyHelper::printQrIsi(
                        'AMI',
                        'temuan-ami',
                        $amiUnit->unit->id,
                        $amiUnit->ami->tahun,
                        '#auditee'
                    ),
                    'QRCODE,Q',
                    25,
                    128,
                    0,
                    22,
                    $style,
                    'N',
                    false
                );

            $mulai = 70;
            $jarak = 48;

            foreach ($auditor->all() as $key => $value) {
                $position = $mulai + $jarak;
                if (isset($listPersetujuan[$value->auditor_id . '-0']))
                    $pdf->write2DBarcode(
                        MyHelper::printQrIsi(
                            'AMI',
                            'temuan-ami',
                            $amiUnit->unit->id,
                            $amiUnit->ami->tahun,
                            '#auditor' . $value->status_id
                        ),
                        'QRCODE,Q',
                        $position,
                        128,
                        0,
                        22,
                        $style,
                        'N'
                    );
            }

            $pdf->SetXY(12, 10);

            $pdf->writeHTML($data);

            $pdf->Output('Berita acara-' . $ami_unit->unit->nama . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }
}
