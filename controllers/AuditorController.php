<?php

namespace app\controllers;

use Yii;
use app\helpers\MyHelper;

use app\models\User;
use app\models\Auditor;
use app\models\AuditorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Client;

/**
 * AuditorController implements the CRUD actions for Auditor model.
 */
class AuditorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'ajax-add', 'delete-multiple', 'set-unsent', 'ajax-send'],
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['fakultas', 'tatausaha', 'prodi', 'operatorCabang']
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'view', 'ajax-add', 'delete-multiple', 'tilik', 'ajax-send'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                    [
                        'actions' => ['set-unsent'],
                        'allow' => true,
                        'roles' => ['theCreator']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDeleteMultiple()
    {
        $dataPost = $_POST['dataPost'];
        $errors = '';
        $results = [];
        $counter = 0;

        if (!empty($dataPost['keys'])) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($dataPost['keys'] as $id) {
                    $model = Auditor::findOne($id);
                    if (!empty($model)) {
                        $user = User::findOne([
                            'email' => $model->email
                        ]);

                        if (!empty($user)) {
                            $auth = Yii::$app->authManager;
                            $role = $auth->getRole('auditor');

                            $auth->revoke($role, $user->id);
                            if (!$user->delete()) {
                                throw new \Exception(MyHelper::logError($user));
                            }
                        }

                        if ($model->delete()) {
                            $counter++;
                        } else {
                            throw new \Exception(MyHelper::logError($model));
                        }
                    }
                }

                $transaction->commit();

                $results = [
                    'code' => 200,
                    'message' => $counter . ' data successfully removed'
                ];
            } catch (\Exception $e) {
                $errors .= $e->getMessage();
                $transaction->rollBack();
                $results = [
                    'code' => 500,
                    'message' => $errors
                ];
            }
        }

        echo json_encode($results);
        die();
    }

    /**
     * Lists all Auditor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSetUnsent()
    {
        $query = Yii::$app->db->createCommand()
            ->update('auditor', ['is_sent' => 0])
            ->execute();
        return $this->redirect(['index']);
    }

    public function actionAuditee()
    {
        $searchModel = new AuditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('auditee', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKecukupan()
    {
        return $this->render('kecukupan');
    }

    public function actionEvaluasiDiri()
    {
        return $this->render('evaluasi_diri');
    }
    public function actionTilik()
    {
        return $this->render('tilik');
    }

    public function actionAsesmen()
    {
        return $this->render('asesmen');
    }

    public function actionTemuan()
    {
        return $this->render('temuan');
    }

    public function actionDeskripsi()
    {
        return $this->render('deskripsi');
    }

    public function actionHasil()
    {
        return $this->render('hasil');
    }

    public function actionAjaxSend()
    {
        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $dataPost = $_POST;


        try {
            if (!empty($dataPost)) {

                $model = $this->findModel($dataPost['id']);

                $model->is_sent = 1;
                $model->save();

                // Add User Auditee...

                $user = User::findOne([
                    'auditor_id' => $model->id,
                ]);

                $is_user = false;
                if (empty($user)) {
                    $user = new User;
                    $is_user = true;
                }

                $password = MyHelper::getRandomString(8, 8, true, false, false);

                $user->username = $model->email;
                $user->email = $model->email;
                $user->nama = $model->nama;
                $user->auditor_id = $model->id;

                $user->setPassword($password);
                $user->status = User::STATUS_ACTIVE;
                $user->access_role = 'auditor';

                $auth = Yii::$app->authManager;
                $oldRole = $auth->getRole('auditor');

                $user->access_role = $oldRole->name;

                if ($user->save()) {

                    if ($is_user == true)
                        $auth->assign($oldRole, $user->id);

                    // $to = MyHelper::sendEmail($user->email);

                    // $emailTemplate = $this->renderPartial('email', [
                    //     'user' => $user,
                    //     'password' => $password
                    // ]);

                    // Yii::$app->mailer->compose()
                    //     ->setTo($to)
                    //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    //     ->setSubject('[SIMUDA] Account Information')
                    //     ->setHtmlBody($emailTemplate)
                    //     ->send();

                    // $toBpm = MyHelper::emailBpm();
                    // Yii::$app->mailer->compose()
                    //     ->setTo($toBpm)
                    //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    //     ->setSubject('[SIMUDA] Account Information')
                    //     ->setHtmlBody($emailTemplate)
                    //     ->send();
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }

                $transaction->commit();
                $results = [
                    'code' => 200,
                    'message' => 'Data successfully updated'
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            // $errors .= 'Oops, your data is incomplete, please complete the data';

            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }


    /**
     * Displays a single Auditor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Auditor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Auditor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionSyncNoReg()
    {
        $auditor = Auditor::find()->all();
        $succesUpdatedData = 0;
        $failedUpdatedData = 0;

        $noRegSementara = 1;
        foreach ($auditor as $auditor) {

            $nomorRegistrasiLama = $auditor->nomor_registrasi;
            $niy = $auditor->niy;

            if (!is_numeric($niy)) {
                $formattedNonNiy = sprintf('%06d', $noRegSementara);
                $niy = $formattedNonNiy;
                $noRegSementara++;
            }

            $nomorRegistrasiBaru = 'AMI' . date('y', strtotime($auditor->tanggal_sk)) . $niy;

            if ($nomorRegistrasiLama != $nomorRegistrasiBaru) {
                $auditor->nomor_registrasi = $nomorRegistrasiBaru;
                if ($auditor->save()) {
                    $succesUpdatedData++;
                } else {
                    $failedUpdatedData++;
                }
            }
        }
        if ($succesUpdatedData != 0) Yii::$app->session->setFlash('success', $succesUpdatedData . " data berhasil diupdate");
        if ($failedUpdatedData != 0) Yii::$app->session->setFlash('danger', $failedUpdatedData . " data gagal diupdate");
        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                if ($model->save()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Data tersimpan");

                    return $this->redirect(['index']);
                } else {
                    throw new \Exception(MyHelper::logError($model));
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger', $e->getMessage());
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

     
    public function actionUpdateProfil($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                if ($model->save()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', "Data berhasil diupdate");

                    return $this->redirect(['user/profil']);
                } else {
                    throw new \Exception(MyHelper::logError($model));
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('danger', $e->getMessage());
                return $this->redirect(['user/profil']);
            }
        }

        return $this->render('update_profil', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Auditor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $model = $this->findModel($id);
            $user = User::findOne([
                'email' => $model->email
            ]);

            if (!empty($user)) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole('auditor');

                $auth->revoke($role, $user->id);
            }

            if ($model->delete()) {
            } else {
                $error = \app\helpers\MyHelper::logError($model);
            }
            if ($user->delete()) {
            } else {
                $error = \app\helpers\MyHelper::logError($user);
            }
            $transaction->commit();

            Yii::$app->session->setFlash("success", "Data successfully deleted");
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash("danger", "Data successfully deleted");
        }


        return $this->redirect(['index']);
    }

    public function actionAjaxAdd()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {
                $model = new Auditor;
                $model->attributes = $dataPost;
                $curYear = date('y', strtotime($dataPost['tanggal_sk']));
                $niy = $model->niy;
                if (!is_numeric($niy)) {
                    $nonNiy = Auditor::find()
                        ->where(['not', ['niy' => null]])
                        ->andWhere(['not', ['niy' => '']])
                        ->andWhere(['not regexp', 'niy', '^[0-9]+$']) // Mencari string yang mengandung karakter bukan angka
                        ->count();

                    $formattedNonNiy = sprintf('%06d', $nonNiy);

                    $niy = $formattedNonNiy;
                }

                // AMI | tahun-sk (tt) | niy ?? 000001
                $model->nomor_registrasi = 'AMI' . $curYear . $niy;

                if ($model->save()) {
                    $user = User::findOne([
                        'auditor_id' => $model->id
                    ]);

                    if (empty($user)) {
                        $user = new User;
                        $user->auditor_id = $model->id;
                    }

                    $password = MyHelper::getRandomString(8, 8, true, false, false);

                    $user->username = $model->email;
                    $user->email = $model->email;
                    $user->nama = $model->nama;

                    $user->setPassword($password);
                    $user->status = User::STATUS_ACTIVE;
                    $user->access_role = 'auditor';

                    $auth = Yii::$app->authManager;

                    // if user has role, set oldRole to that role name, else offer 'member' as sensitive default
                    $oldRole = $auth->getRole('auditor');


                    // set property item_name of User object to this role name, so we can use it in our form
                    $user->access_role = $oldRole->name;
                    // echo '<pre>';print_r($model);die;

                    if ($user->save()) {
                        $auth->assign($oldRole, $user->id);
                        $to = MyHelper::sendEmail($user->email);

                        $emailTemplate = $this->renderPartial('email', [
                            'user' => $user,
                            'password' => $password
                        ]);

                        // Yii::$app->mailer->compose()
                        //     ->setTo($to)
                        //     ->setFrom([Yii::$app->params['supportEmail'] => 'SPMI UNIDA Gontor'])
                        //     ->setSubject('[SPMI] Account Information')
                        //     ->setHtmlBody($emailTemplate)
                        //     ->send();
                    } else {
                        throw new \Exception(MyHelper::logError($user));
                    }


                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' => 'Data successfully added'
                    ];
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxCariDosen()
    {

        $q = $_GET['term'];

        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token' => $client_token];
        $response = $client->get('/simpeg/dosen/list', ['nama' => $q], $headers)->send();

        $out = [];


        if ($response->isOk) {
            $result = $response->data['values'];

            if (!empty($result)) {
                foreach ($result as $d) {

                    $out[] = [

                        'id' => $d['kode_unik'],
                        'label' => $d['gd'] . ' ' . $d['nama'] . ', ' . $d['gb'],
                        'items' => $d
                    ];
                }
            } else {
                $out[] = [
                    'id' => 0,
                    'label' => 'Data dosen tidak ditemukan',

                ];
            }
        }


        echo \yii\helpers\Json::encode($out);

        die();
    }


    /**
     * Finds the Auditor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
