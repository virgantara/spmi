<?php

namespace app\controllers;

use Yii;
use app\models\Ami;
use app\models\AmiSearch;
use app\models\AmiUnit;
use app\models\Bpm;
use app\models\UnitKerja;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * AmiController implements the CRUD actions for Ami model.
 */
class AmiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete'],
                'rules' => [

                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ami models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            $id = Yii::$app->request->post('editableKey');
            $model = Ami::findOne($id);

            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Ami']);
            $post = ['Ami' => $posted];

            if (isset($post['Ami']['status_aktif'])) {
                if ($post['Ami']['status_aktif'] == 1) {
                    $allAmi = Ami::findAll(['status_aktif' => 1]);
                    foreach ($allAmi as $ami) {
                        $ami->status_aktif = 0;
                        $ami->save(false);
                    }
                }
            }

            if ($model->load($post)) {

                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionSubamiunit()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $ami_id = $parents[0];
                $query = AmiUnit::find();
                $query->andWhere([
                    'ami_id' => $ami_id,
                ]);
                $list_unit = $query->all();
                foreach ($list_unit as $item) {
                    $out[$item->id] = [
                        'id' => $item->id,
                        'unit_id' => $item->unit_id,
                        'name' => $item->ami->nama . ' - ' . $item->unit->nama
                    ];
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                die();
            }
        }
    }

    /**
     * Displays a single Ami model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $amiUnit = AmiUnit::findAll(['ami_id' => $id]);
        $ami = Ami::findOne($id);

        if ($ami->status_aktif != 1) $auditee = UnitKerja::find()->orderBy(['nama' => SORT_ASC])->joinWith(['amiUnits as au'])->where(['au.ami_id' => $id])->all();
        else $auditee = UnitKerja::find()->where(['NOT', ['jenis' => ['koordinator', 'universitas']]])->orderBy(['nama' => SORT_ASC])->all();

        $listAmiUnit = ArrayHelper::map($amiUnit, 'unit_id', 'status_ami');

        $checkedAll = 'checked';
        foreach ($auditee as $key => $a) {
            if (!isset($listAmiUnit[$a->id])) {
                $checkedAll = '';
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'amiUnit' => $amiUnit,
            'auditee' => $auditee,
            'checkedAll' => $checkedAll,
        ]);
    }

    public function actionCover($id)
    {
        $model = Ami::findOne($id);
        if (!empty($model->path_cover)) {
            try {
                $image = imagecreatefromstring($this->getImage($model->path_cover));

                header('Content-Type: image/png');
                imagepng($image);
            } catch (\Exception $e) {
            }
        }

        die();
    }




    public function actionUploadCover()
    {
        $errors = [];
        $connection = \Yii::$app->db;
        $transcation = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];

        $s3new = new \Aws\S3\S3Client($s3config);
        $errors = [];

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if (!empty($file)) {
                $ami = Ami::findOne($_POST['id']);

                $curdate = date('d-m-y');

                $file_name = $curdate . '-' . $file->name;
                $s3path = $file->tempName;
                $s3type = $file->type;
                $key = 'laporan' . '/' . 'laporan-umum' . '/cover-' . $ami->nama . '.png';

                $insert = $s3new->putObject([
                    'Bucket' => 'spmi',
                    'Key' => $key,
                    'Body' => 'Body',
                    'SourceFile' => $s3path,
                    'ContenType' => $s3type,
                ]);

                $plainUrl = $s3new->getObjectUrl('spmi', $key);
                $ami->path_cover = $plainUrl;


                if ($ami->save()) {
                    $transcation->commit();

                    Yii::$app->session->setFlash('success', "Data dipebarui");
                    return $this->redirect(['view', 'id' => $ami->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    /**
     * Creates a new Ami model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ami();

        if ($model->load(Yii::$app->request->post())) {

            $allAmi = Ami::findAll(['status_aktif' => 1]);
            foreach ($allAmi as $ami) {
                $ami->status_aktif = 0;
                $ami->save(false);
            }

            $bpm = Bpm::find()->one();
            $model->ketua_bpm = $bpm->kepala_bpm;
            $model->nidn_ketua_bpm = $bpm->niy;
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ami model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ami model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ami model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ami the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ami::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
