<?php

namespace app\controllers;

use Yii;
use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiAuditor;
use app\models\AmiUnit;
use app\models\AmiUnitSearch;
use app\models\Asesmen;
use app\models\AsesmenDokumen;
use app\models\Auditor;
use app\models\Dokumen;
use app\models\Indikator;
use app\models\IndikatorSearch;
use app\models\JenjangBobot;
use app\models\Kriteria;
use app\models\PembagianKriteria;
use app\models\Tilik;
use app\models\UnitKerja;
use app\models\UnitKerjaSearch;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Yaml\Dumper;
use yii\debug\panels\DumpPanel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * IndikatorController implements the CRUD actions for Indikator model.
 */
class IndikatorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'ajax-add', 'prodi'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'ajax-add', 'ami'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAjaxCloning()
    {
        $results = [];
        $errors = '';

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $amiAktif = Ami::find()->where(['status_aktif' => true])->one();
            $dataPost = Yii::$app->request->post();

            if (empty($dataPost)) {
                throw new \Exception('Oops, Tahun AMI belum dipilih');
            }

            // Hapus data AMI aktif sebelumnya jika ada
            $cekDataAmi = Indikator::findAll(['ami_id' => $amiAktif->id]);
            foreach ($cekDataAmi as $dataAmi) {
                JenjangBobot::deleteAll(['indikator_id' => $dataAmi->id]);
                if (!$dataAmi->delete()) {
                    throw new \Exception(MyHelper::logError($dataAmi));
                }
            }

            // Kloning data berdasarkan `ami_id` dari post
            $dataAmiRef = Indikator::findAll(['ami_id' => $dataPost['ami_id']]);
            foreach ($dataAmiRef as $dataRef) {
                $jenjangRef = JenjangBobot::findAll(['indikator_id' => $dataRef->id]);

                $model = new Indikator();
                $model->attributes = $dataRef->attributes;
                $model->ami_id = $amiAktif->id;
                $model->created_at = date('Y-m-d H:i:s');
                $model->updated_at = date('Y-m-d H:i:s');

                if (!$model->save()) {
                    throw new \Exception(MyHelper::logError($model));
                } else {
                    foreach ($jenjangRef as $key => $jr) {
                        $modeljr = new JenjangBobot();
                        $modeljr->attributes = $jr->attributes;
                        $modeljr->indikator_id = $model->id;

                        if (!$modeljr->save()) {
                            throw new \Exception(MyHelper::logError($modeljr));
                        }
                    }
                }
            }

            $transaction->commit();
            $results = [
                'code' => 200,
                'message' => 'Data berhasil dikloning',
            ];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors = $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors,
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors = $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors,
            ];
        }

        echo json_encode($results);
        exit;
    }


    public function actionAjaxAdd()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $update = false;
                if ($dataPost['id'] != null) {
                    $model = $this->findModel($dataPost['id']);
                    $update = true;
                } else {
                    $model = new Indikator;
                }

                $model->attributes = $dataPost;
                $jenis = Kriteria::findOne($dataPost['kriteria_id']);
                $model->jenis_indikator = $jenis->is_khusus;

                if ($model->save()) {

                    if (empty($dataPost['bobot'])) {
                        foreach ($dataPost['jenjang_id'] as $k => $v) {
                            $nilai = [
                                'indikator_id' => $model->id,
                                'jenjang_id' => $v,
                                // 'bobot' => $dataPost['nilai_bobot'][$v],
                                'bobot' => 1,
                            ];
                            $bobots[] = $nilai;
                        }
                    } else {
                        foreach ($dataPost['jenjang_id'] as $k => $v) {
                            $nilai = [
                                'indikator_id' => $model->id,
                                'jenjang_id' => $v,
                                'bobot' => $dataPost['bobot'],
                            ];
                            $bobots[] = $nilai;
                        }
                    }

                    if ($update) {

                        JenjangBobot::deleteAll(['indikator_id'  => $model->id]);

                        $batchBobots = Yii::$app->db->createCommand()->batchInsert('jenjang_bobot', [
                            'indikator_id',
                            'jenjang_id',
                            'bobot',
                        ], $bobots);
                    } else {
                        $batchBobots = Yii::$app->db->createCommand()->batchInsert('jenjang_bobot', [
                            'indikator_id',
                            'jenjang_id',
                            'bobot',
                        ], $bobots);
                    }

                    if ($batchBobots->execute()) {
                        $transaction->commit();
                    } else {
                        $errors .= MyHelper::logError($batchBobots);
                        throw new \Exception;
                    }

                    if ($update) {
                        $message = 'Data successfully updated';
                    } else {
                        $message = 'Data successfully added';
                    }
                    $results = [
                        'code' => 200,
                        'message' => $message
                    ];
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxGetData()
    {
        $dataPost = $_POST;
        $out = [];

        $indikator = Indikator::findOne($dataPost['id']);

        $jenjang = JenjangBobot::find()->where([
            'indikator_id' => $dataPost['id'],
        ])->all();

        $out['id'] = $indikator->id;
        $out['kriteria_id'] = $indikator->kriteria_id;
        $out['nama'] = $indikator->nama;
        $out['pernyataan'] = $indikator->pernyataan;
        $out['indikator'] = $indikator->indikator;
        $out['referensi'] = $indikator->referensi;
        $out['strategi'] = $indikator->strategi;
        $out['bobot'] = $indikator->bobot;

        foreach ($jenjang as $key => $value) {
            $jenjang = [
                'id'    => $value->jenjang_id,
                'bobot' => $value->bobot,
            ];
            $out['jenjang'][] = $jenjang;
        }

        echo \yii\helpers\Json::encode($out);

        die();
    }

    public function actionAjaxGetStandarByKriteria()
    {
        $request = Yii::$app->request;
        $dataPost = $request->post();

        $out = [];
        $amiUnit = AmiUnit::findOne($dataPost['ami_unit_id']);

        if (!empty($dataPost['filter_kriteria_id'])) {
            $kriteria = Kriteria::findOne($dataPost['filter_kriteria_id']);
            $indikators = Indikator::find()
                ->joinWith(['ami as a'])
                ->andWhere([
                    'kriteria_id' => $kriteria->id,
                    'jenis_indikator' => $kriteria->is_khusus == true ? 2 : 0,
                    'indikator.status_aktif' => 1,
                ]);

            if ($kriteria->is_khusus == true) {
                $indikators->andWhere([
                    'unit_id' => $amiUnit->unit_id,
                    'a.status_aktif' => 1
                ]);
            } else {
                $indikators->andWhere(['a.id' => $amiUnit->ami_id]);
            }

            if ($amiUnit->unit->jenis != 'satker') {
                $jenjangs = $amiUnit->unit->jenjangMaps;
                $jenjangData = [];
                foreach ($jenjangs  as $key => $jenjang) {
                    $jenjangData[] = $jenjang->jenjang_id;
                }
                $indikators->joinWith(['jenjangBobots as jb'])->andWhere(['jb.jenjang_id' => $jenjangData]);
            }

            $indikators = $indikators->all();
        }

        $out['kriteria'] = [
            'nama' => $kriteria->nama,
            'keterangan' => $kriteria->keterangan ?? '',
        ];

        foreach ($indikators as $key => $i) {
            $asesmen = Asesmen::findOne(['indikator_id' => $i->id, 'ami_unit_id' => $dataPost['ami_unit_id']]);

            $status_ak = 0;
            $status_al = 0;
            $statusTilik = 0;

            if ($asesmen !== null) {
                $status_ak = $this->calculateStatus($asesmen->skor_ak1, $asesmen->skor_ak2, $asesmen->skor_ak3);
                $status_al = $this->calculateStatus($asesmen->skor_al1, $asesmen->skor_al2, $asesmen->skor_al3);
                $asesmenDokumen = AsesmenDokumen::findOne(['asesmen_id' => $asesmen->id]);

                $tilik = Tilik::findOne(['asesmen_id' => $asesmen->id]);
                if ($tilik !== null) {
                    $statusTilik = 1;
                }
            }

            $out['standar'][] = [
                'id' => $i->id,
                'referensi' => $i->referensi,
                'indikator' => $i->indikator,
                'statusDokumen' => (empty($asesmen) ? 0 : (!empty($asesmenDokumen) ? 2 : 0)),
                'status_ed' => (!empty($asesmen->skor_ed) ? 2 : 0),
                'status_ak' => $status_ak,
                'status_al' => $status_al,
                'statusTilik' => $statusTilik,
            ];
        }

        echo \yii\helpers\Json::encode($out);

        die();
    }


    /**
     * Lists all Indikator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndikatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, $_GET);

        $kriteria = Kriteria::find()->orderBy(['urutan' => SORT_ASC]);
        $ami = Ami::find()->where(['status_aktif' => false])->all();
        $listAmi = ArrayHelper::map($ami, 'id', 'nama');
        $kriteria->andWhere(['status_aktif' => 1]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        $auditee = UnitKerja::find()->joinWith(['amiUnits.ami as aa'])
            ->andWhere([
                'aa.status_aktif' => 1,
                'jenis' => 'satker'
            ])->all();
        $listAuditee = ArrayHelper::map($auditee, 'id', function ($model) {
            return $model->nama;
        });

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Indikator::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'listAuditee' => $listAuditee,
            'dataProvider' => $dataProvider,
            'listAmi' => $listAmi,
        ]);
    }

    public function actionData($ami_id)
    {
        $searchModel = new IndikatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, $_GET, $ami_id);

        $kriteria = Kriteria::find()->orderBy(['urutan' => SORT_ASC]);
        $kriteria->andWhere(['status_aktif' => 1]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        $auditee = UnitKerja::find()->joinWith(['amiUnits.ami as aa'])
            ->andWhere([
                'aa.status_aktif' => 1,
                'jenis' => 'satker'
            ])->all();
        $listAuditee = ArrayHelper::map($auditee, 'id', function ($model) {
            return $model->nama;
        });

        return $this->render('data', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'listAuditee' => $listAuditee,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpload()
    {
        $errors = '';
        $counter = 0;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if ($file != null) {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    $inputFileType = IOFactory::identify($file->tempName);
                    $reader = IOFactory::createReader($inputFileType);
                    $spreadsheet = $reader->load($file->tempName);
                    $worksheet = $spreadsheet->getSheetByName('Penetapan Standar');
                    $highestRow = $worksheet->getHighestRow();

                    // Prepare arrays for mapping
                    $arrAmi = [];
                    $ami = ArrayHelper::map(Ami::find()->all(), 'tahun', 'id');
                    foreach ($ami as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAmi[$newKey] = $value;
                    }

                    $arrKriteria = [];
                    $kriteria = ArrayHelper::map(Kriteria::find()->all(), 'kode', 'id');
                    foreach ($kriteria as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrKriteria[$newKey] = $value;
                    }

                    $arrSatker = [];
                    $satker = ArrayHelper::map(UnitKerja::find()->where(['jenis' => 'satker'])->orderBy(['nama' => SORT_ASC])->all(), 'singkatan', 'id');
                    foreach ($satker as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrSatker[$newKey] = $value;
                    }

                    $dataJenjang = [];
                    for ($r = 6; $r <= $highestRow; $r++) {
                        $A = $worksheet->getCell('A' . $r)->getValue();
                        $K = $worksheet->getCell('K' . $r)->getValue();
                        $L = $worksheet->getCell('L' . $r)->getValue();

                        if (!empty($A) && !empty($K)) {
                            $indikator = new Indikator();
                            $indikator->kriteria_id = $arrKriteria[strtolower(trim($A))] ?? null;
                            $indikator->ami_id = $arrAmi[strtolower(trim($K))] ?? null;
                            $indikator->unit_id = !empty($L) ? $arrSatker[strtolower(trim($L))] : null;
                            $indikator->nama = $worksheet->getCell('C' . $r)->getValue();
                            $indikator->pernyataan = $worksheet->getCell('D' . $r)->getValue();
                            $indikator->indikator = $worksheet->getCell('E' . $r)->getValue();
                            $indikator->jenis_indikator = !empty($L) ? '2' : '0';
                            $indikator->referensi = $worksheet->getCell('F' . $r)->getValue();
                            $indikator->strategi = $worksheet->getCell('G' . $r)->getValue();

                            if ($indikator->save()) {
                                if (!empty($worksheet->getCell('H' . $r)->getValue())) {
                                    $dataJenjang[] = ['indikator_id' => $indikator->id, 'jenjang_id' => 2, 'bobot' => 1];
                                }
                                if (!empty($worksheet->getCell('I' . $r)->getValue())) {
                                    $dataJenjang[] = ['indikator_id' => $indikator->id, 'jenjang_id' => 3, 'bobot' => 1];
                                }
                                if (!empty($worksheet->getCell('J' . $r)->getValue())) {
                                    $dataJenjang[] = ['indikator_id' => $indikator->id, 'jenjang_id' => 4, 'bobot' => 1];
                                }
                            } else {

                                Yii::error("Error saving indikator at row $r: " . json_encode($indikator->getErrors()), __METHOD__);
                                throw new \Exception("Gagal menyimpan indikator pada baris $r. Periksa data input.");
                            }
                        }
                    }

                    if (!empty($dataJenjang)) {
                        Yii::$app->db->createCommand()->batchInsert('jenjang_bobot', [
                            'indikator_id',
                            'jenjang_id',
                            'bobot',
                        ], $dataJenjang)->execute();
                    }

                    Yii::$app->db->transaction->commit();
                    Yii::$app->session->setFlash('success', count($dataJenjang) + 1 . ' data berhasil diinput.');
                    return $this->redirect(['index']);
                } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
                    Yii::error("Spreadsheet error: " . $e->getMessage(), __METHOD__);
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Kesalahan pada file spreadsheet: ' . $e->getMessage());
                } catch (\Throwable $th) {
                    Yii::error("Unhandled error: " . $th->getMessage(), __METHOD__);
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    public function actionTemplateDownload()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $kriteria = Kriteria::find()->orderBy(['urutan' => SORT_ASC]);
        $satker = UnitKerja::find()->where(['jenis' => 'satker'])->orderBy(['nama' => SORT_ASC]);

        // Add the first sheet
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Penetapan Standar');
        $sheet1->setCellValue('A1', 'PENETAPAN STANDAR');
        $sheet1->setCellValue('A2', 'Kode Kriteria');
        $sheet1->setCellValue('B2', 'Nama Kriteria');
        $sheet1->setCellValue('C2', 'Nama Standar');
        $sheet1->setCellValue('D2', 'Pernyataan Standar');
        $sheet1->setCellValue('E2', 'Indikator');
        $sheet1->setCellValue('F2', 'Referensi');
        $sheet1->setCellValue('G2', 'Strategi');
        $sheet1->setCellValue('H2', 'S1');
        $sheet1->setCellValue('I2', 'S2');
        $sheet1->setCellValue('J2', 'S3');
        $sheet1->setCellValue('K2', 'Tahun');
        $sheet1->setCellValue('L2', 'Kode Satker');
        $sheet1->setCellValue('M2', 'Nama Satker');

        $sheet1->setCellValue('A3', 'Contoh:');
        $sheet1->setCellValue('A4', '1');
        $sheet1->setCellValue('B4', 'Kriteria 1');
        $sheet1->setCellValue('C4', 'VMTS');
        $sheet1->setCellValue('D4', 'visi misi dibentuk ');
        $sheet1->setCellValue('E4', 'Tracer study yang dilakukan UPPS telah mencakup 5 aspek.');
        $sheet1->setCellValue('F4', 'Permendikbud');
        $sheet1->setCellValue('G4', 'Strategi');
        $sheet1->setCellValue('H4', '1');
        $sheet1->setCellValue('I4', '');
        $sheet1->setCellValue('J4', '1');
        $sheet1->setCellValue('K4', '2023');
        $sheet1->setCellValue('L4', 'BPM');
        $sheet1->setCellValue('M4', 'Badan Penjaminan Mutu');

        $sheet1->setCellValue('A5', 'Mulai Tambahkan Data Dibawah ini:');

        $sheet1->mergeCells('A1:M1');

        $style = $sheet1->getStyle('A1:M1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet1->getStyle('A1:M2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        // Add the 'Data Kode' sheet
        $sheet2 = $spreadsheet->createSheet();
        $sheet2->setTitle('Data Kode');

        $sheet2->setCellValue('A1', 'KODE KRITERIA STANDAR');
        $sheet2->setCellValue('A2', 'kode');
        $sheet2->setCellValue('B2', 'Nama Kriteria');

        $sheet2->mergeCells('A1:B1');

        $colomInventaris = 3; // mulai colom inventaris
        $noInventaris = 1; // mulai no inventaris

        foreach ($kriteria->all() as $i) {
            $sheet2->setCellValue('A' . $colomInventaris, $i->kode);
            $sheet2->setCellValue('B' . $colomInventaris, $i->nama);
            $colomInventaris++;
            $noInventaris++;
        }

        $sheet2->setCellValue('D1', 'KODE INVENTARIS RUANGAN');
        $sheet2->setCellValue('D2', 'kode');
        $sheet2->setCellValue('E2', 'Nama Ruangan');

        $sheet2->mergeCells('D1:E1');

        $colomRuangan = 3; // mulai colom ruangan
        $noRuangan = 1; // mulai no ruangan

        foreach ($satker->all() as $i) {
            $sheet2->setCellValue('D' . $colomRuangan, $i->singkatan);
            $sheet2->setCellValue('E' . $colomRuangan, $i->nama);
            $colomRuangan++;
            $noRuangan++;
        }

        $style = $sheet2->getStyle('A1:C1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet2->getStyle('A1:K2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        //Vlookup Alat Inventaris
        for ($i = 6; $i < 100; $i++) {
            $sheet1->setCellValue('B' . $i, '=IFERROR(VLOOKUP(A' . $i . ',\'Data Kode\'!$A$3:$B$' . ($kriteria->count() + 3) . ',2,0), "-")');
            $sheet1->getCell('B' . $i)->setDataType(DataType::TYPE_FORMULA);

            $sheet1->setCellValue('M' . $i, '=IFERROR(VLOOKUP(L' . $i . ',\'Data Kode\'!$D$3:$E$' . ($satker->count() + 3) . ',2,0), "-")');
            $sheet1->getCell('M' . $i)->setDataType(DataType::TYPE_FORMULA);
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Configure the Excel output
        $filename = 'data-standar-umum-' . date('Y') . '.xlsx';

        // Render Excel to memory
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Set the response headers and send the file as a download
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        Yii::$app->response->headers->add('Content-Disposition', 'attachment;filename=' . $filename);
        Yii::$app->response->headers->add('Cache-Control', 'max-age=0');
        Yii::$app->response->content = $content;
        Yii::$app->response->send();
        Yii::$app->end();
    }

    public function actionFakultas()
    {

        $searchModel = new IndikatorSearch();
        $jenis = 1;

        if (!empty($_GET['filter_jenjang'])) $filters['jenjang_id'] = $_GET['filter_jenjang'];
        else $filters = '';

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis, $filters);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Indikator::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('fakultas', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'dataProvider' => $dataProvider,
            'total_d4' => MyHelper::getSumBobotIndikator($jenis, 1),
            'total_s1' => MyHelper::getSumBobotIndikator($jenis, 2),
            'total_s2' => MyHelper::getSumBobotIndikator($jenis, 3),
            'total_s3' => MyHelper::getSumBobotIndikator($jenis, 4),
        ]);
    }

    public function actionProgramStudi()
    {
        $searchModel = new IndikatorSearch();
        $jenis = 2;

        if (!empty($_GET['filter_jenjang'])) $filters['jenjang_id'] = $_GET['filter_jenjang'];
        else $filters = '';

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis, $filters);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Indikator::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('prodi', [
            'searchModel'   => $searchModel,
            'listKriteria'  => $listKriteria,
            'dataProvider'  => $dataProvider,
            'total_d4'      => MyHelper::getSumBobotIndikator($jenis, 1),
            'total_s1'      => MyHelper::getSumBobotIndikator($jenis, 2),
            'total_s2'      => MyHelper::getSumBobotIndikator($jenis, 3),
            'total_s3'      => MyHelper::getSumBobotIndikator($jenis, 4),
        ]);
    }

    public function actionSatuanKerja()
    {
        $searchModel = new IndikatorSearch();
        $jenis = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis);

        $kriteria = Kriteria::find()->where(['jenis' => 0])->orderBy('nama ASC');
        $kriteria->joinWith(['pembagianKriterias as p']);
        $kriteria->where(['p.jenis_unit' => $jenis]);
        $kriteria = $kriteria->all();
        $listKriteria = ArrayHelper::map($kriteria, 'id', 'nama');

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Indikator::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Indikator']);
            $post = ['Indikator' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('satker', [
            'searchModel' => $searchModel,
            'listKriteria' => $listKriteria,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAmi()
    {
        $searchModel = new UnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['jenis' => 'satker']);

        return $this->render('ami', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKhusus($id)
    {
        $indikator_khusus = Indikator::find()->where([
            'unit_id' => $id,
            'jenis_indikator' => '2',
        ]);
        $indikator_khusus->joinWith(['ami as a']);
        $indikator_khusus->andWhere(['a.status_aktif' => 1]);
        $indikator_khusus = $indikator_khusus->all();

        $list_unit = ArrayHelper::map(\app\models\UnitKerja::find()->orderBy(['nama' => SORT_ASC])->all(), 'id', 'nama');

        return $this->render('khusus', [
            'model' => $this->findModelUnit($id),
            'indikator_khusus' => $indikator_khusus,
            'list_unit' => $list_unit,
        ]);
    }

    /**
     * Displays a single Indikator model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionReview()
    {
        $indikator = Indikator::find();
        $indikator->joinWith(['ami as a']);
        $indikator->joinWith(['kriteria as k']);
        $indikator->andWhere(['a.status_aktif' => 1]);
        $indikator->andWhere(['k.jenis_standar' => 'standar-mutu']);

        // echo '<pre>';print_r($_GET);die;
        if (!empty($_GET['kriteria'])) {
            $indikator->andWhere(['kriteria_id' => $_GET['kriteria']]);
        }
        $indikator = $indikator->all();

        $listKriteria = Kriteria::find()->orderBy(['urutan' => SORT_ASC])->all();
        return $this->render('review', [
            'indikators' => $indikator,
            'listKriteria' => $listKriteria,
        ]);
    }

    /**
     * Creates a new Indikator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Indikator();

        if ($model->load(Yii::$app->request->post())) {
            // $model->bobot = 5;
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionTambah($id_ami_unit)
    {
        $model = new Indikator();

        if ($model->load(Yii::$app->request->post())) {
            $model->unit_id = $id_ami_unit;
            $model->ami_id = Ami::findOne(['status_aktif' => 1])->id ?? null;
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");
            // echo '<pre>';print_r($model->ami_id);die;
            return $this->redirect(['khusus', 'id' => $id_ami_unit]);
        }

        return $this->render('tambah', [
            'model' => $model,
            'ami_unit' => $id_ami_unit,
        ]);
    }

    /**
     * Updates an existing Indikator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Indikator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        JenjangBobot::deleteAll(['indikator_id'  => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletee($id, $ami_id)
    {
        // echo '<pre>';print_r('Tes');exit;
        $asesmen = Asesmen::find()->where(['indikator_id' => $id])->all();
        if (isset($asesmen)) {

            foreach ($asesmen as $a) {
                $asesmen = Asesmen::findOne($a->id);
                $asesmen->delete();
            }
        }
        // echo '<pre>';print_r($asesmen);die;

        $this->findModel($id)->delete();

        return $this->redirect(['indikator/khusus', 'id' => $ami_id]);
    }

    /**
     * Finds the Indikator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indikator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indikator::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelUnit($id)
    {
        if (($model = UnitKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function calculateStatus($score1, $score2, $score3)
    {
        if (!empty($score1) && !empty($score2) && !empty($score3)) {
            return 2;
        } elseif (!empty($score1) || !empty($score2) || !empty($score3)) {
            return 1;
        } else {
            return 0;
        }
    }
}
