<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use Yii;
use app\models\AmiAuditor;
use app\models\AmiAuditorSearch;
use app\models\AmiUnit;
use app\models\AmiUnitSearch;
use app\models\Auditor;
use app\models\Kriteria;
use app\models\StatusAuditor;
use app\models\UnitKerja;
use app\models\UnitKerjaSearch;
use app\models\User;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * AmiAuditorController implements the CRUD actions for AmiAuditor model.
 */
class AmiAuditorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'rekap', 'rekap-auditor', 'ajax-add', 'ajax-send', 'submitamiunit', 'deletee'],
                'rules' => [

                    [
                        'actions' => [
                            'create', 'update', 'view', 'index', 'delete', 'rekap', 'rekap-auditor', 'ajax-add', 'ajax-send', 'submitamiunit', 'deletee'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmiAuditor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $filters = [];

        if (!empty($_GET)) {
            $filters = $_GET;
        }

        $searchModel = new AmiAuditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRekap()
    {
        $searchModel = new AmiUnitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('rekap', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionRekapAuditor()
    {
        // $ami_id = [];
        $filters = [];

        if (!empty($_GET))
            $filters = $_GET;


        $searchModel = new AmiAuditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('rekap-auditor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmiAuditor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AmiAuditor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmiAuditor();

        if ($model->load(Yii::$app->request->post())) {
            $dataPost = $_POST;

            $auditor = Auditor::findOne($model->auditor_id);
            $status = StatusAuditor::findOne($model->status_id);
            $ami = AmiUnit::findOne(['ami_id' => $dataPost['periode_id'], 'unit_id' => $dataPost['unit_kerja_id']]);

            $check_auditor = AmiAuditor::findOne([
                'auditor_id' => $model->auditor_id,
                'ami_id' => $ami->id,
            ]);

            if (!empty($check_auditor)) {

                Yii::$app->session->setFlash('danger', "Oops.., $auditor->nama sudah terdaftar pada $ami sebagai $status->nama");
                return $this->redirect(['index']);
            } else {

                $check_status = AmiAuditor::find()->where([
                    'ami_id' => $ami->id,
                    'status_id' => $model->status_id,
                ])->one();

                if (!empty($check_status)) {

                    $status = StatusAuditor::findOne($model->status_id);

                    $auditor_isi = Auditor::findOne($check_status->auditor_id);

                    Yii::$app->session->setFlash('danger', "Oops.., Posisi petugas $status->nama pada $ami sudah diisi oleh $auditor_isi->nama");
                    return $this->redirect(['index']);
                } else {

                    $model->ami_id = $ami->id;
                    $model->save();
                    Yii::$app->session->setFlash('success', "Yeay.., Data tersimpan");
                    return $this->redirect(['index']);
                }
            }
        }
        // echo '<pre>';print_r($ami);exit;

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpload()
    {
        $errors = '';
        $counter = 0;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if ($file != null) {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    $inputFileType = IOFactory::identify($file->tempName);
                    $reader = IOFactory::createReader($inputFileType);
                    $spreadsheet = $reader->load($file->tempName);
                    $worksheet = $spreadsheet->getSheetByName('Penugasan Auditor');
                    $highestRow = $worksheet->getHighestRow();

                    // Prepare arrays for mapping

                    $arrAuditor = [];
                    $auditor = ArrayHelper::map(Auditor::find()->all(), 'nomor_registrasi', 'id');
                    foreach ($auditor as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAuditor[$newKey] = $value;
                    }
                    $ami = Ami::findOne(['status_aktif' => 1]);

                    $arrAmiUnit = [];
                    $amiUnit = ArrayHelper::map(AmiUnit::find()->where(['ami_id' => $ami->id])->all(), function ($data) {
                        return $data->unit->nama;
                    }, 'id');
                    foreach ($amiUnit as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAmiUnit[$newKey] = $value;
                    }

                    $berhasil = 0;
                    $update = 0;
                    $gagal = 0;

                    for ($r = 6; $r < ($highestRow + 1); $r++) {
                        $B = $worksheet->getCell('B' . $r)->getValue();
                        $C = $worksheet->getCell('C' . $r)->getValue();
                        $D = $worksheet->getCell('D' . $r)->getValue();
                        $F = $worksheet->getCell('F' . $r)->getValue();
                        $H = $worksheet->getCell('H' . $r)->getValue();

                        if (!empty($D)) {
                            $amiUnitAuditor = AmiAuditor::findOne([
                                'ami_id' => $arrAmiUnit[strtolower(trim($B))],
                                'status_id' => 1
                            ]);
                            if (empty($amiUnitAuditor)) {
                                $amiUnitAuditor = new AmiAuditor();
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($D))];
                                $amiUnitAuditor->status_id = 1;
                                if ($amiUnitAuditor->save()) $berhasil++;
                                else $gagal++;
                            } else {
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($D))];
                                $amiUnitAuditor->status_id = 1;
                                if ($amiUnitAuditor->save()) $update++;
                                else $gagal++;
                            }
                        }
                        if (!empty($F)) {
                            $amiUnitAuditor = AmiAuditor::findOne([
                                'ami_id' => $arrAmiUnit[strtolower(trim($B))],
                                'status_id' => 2
                            ]);
                            if (empty($amiUnitAuditor)) {
                                $amiUnitAuditor = new AmiAuditor();
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($F))];
                                $amiUnitAuditor->status_id = 2;
                                if ($amiUnitAuditor->save()) $berhasil++;
                                else $gagal++;
                            } else {
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($F))];
                                $amiUnitAuditor->status_id = 2;
                                if ($amiUnitAuditor->save()) $update++;
                                else $gagal++;
                            }
                        }
                        if (!empty($H)) {
                            $amiUnitAuditor = AmiAuditor::findOne([
                                'ami_id' => $arrAmiUnit[strtolower(trim($B))],
                                'status_id' => 3
                            ]);
                            if (empty($amiUnitAuditor)) {
                                $amiUnitAuditor = new AmiAuditor();
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($H))];
                                $amiUnitAuditor->status_id = 3;
                                if ($amiUnitAuditor->save()) $berhasil++;
                                else $gagal++;
                            } else {
                                $amiUnitAuditor->ami_id = $arrAmiUnit[strtolower(trim($B))];
                                $amiUnitAuditor->auditor_id = $arrAuditor[strtolower(trim($H))];
                                $amiUnitAuditor->status_id = 3;
                                if ($amiUnitAuditor->save()) $update++;
                                else $gagal++;
                            }
                        }
                    }

                    // Batch insert after the loop
                    if (!empty($dataJenjang)) {
                        Yii::$app->db->createCommand()->batchInsert('jenjang_bobot', [
                            'indikator_id',
                            'jenjang_id',
                            'bobot',
                        ], $dataJenjang)->execute();
                    }

                    // Committing the transaction after batch insert
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', $berhasil . ' data auditor berhasil diinput');
                    Yii::$app->session->setFlash('info', $update . ' data auditor berhasil diupdate');
                    Yii::$app->session->setFlash('danger', $gagal . ' data auditor gagal diinput/diupdate');
                    return $this->redirect(['index']);
                } catch (\Throwable $th) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    public function actionTemplateDownload()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Fetching data from models
        $auditorData = Auditor::findAll(['status_aktif' => 1]);
        $ami = Ami::findOne(['status_aktif' => 1]);
        $auditeeData = AmiUnit::find()->where(['ami_id' => $ami->id])->all();
        $amiAuditorData = AmiAuditor::find()
            ->joinWith(['ami as a'])
            ->where(['a.ami_id' => $ami->id])
            ->all();

        // Add the first sheet
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Penugasan Auditor');
        $sheet1->setCellValue('A1', 'Penugasan Auditor');
        $sheet1->setCellValue('B2', 'Periode');
        $sheet1->setCellValue('C2', $ami->nama);

        $sheet1->setCellValue('A3', 'Isilah data berikut:');

        $sheet1->setCellValue('A4', 'No');
        $sheet1->setCellValue('B4', 'Auditee');
        $sheet1->setCellValue('C4', 'Kategori Auditee');
        $sheet1->setCellValue('D4', 'NRA Auditor 1');
        $sheet1->setCellValue('E4', 'Nama Auditor 1');
        $sheet1->setCellValue('F4', 'NRA Auditor 2');
        $sheet1->setCellValue('G4', 'Nama Auditor 2');
        $sheet1->setCellValue('H4', 'NRA Auditor 3');
        $sheet1->setCellValue('I4', 'Nama Auditor 3');

        $sheet1->setCellValue('A5', 'Mulai Tambahkan Data Dibawah ini:');

        $sheet1->mergeCells('A1:I1');
        $sheet1->mergeCells('C2:H2');
        $sheet1->mergeCells('A3:B3');

        $style = $sheet1->getStyle('A1:H1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet1->getStyle('A4:I4')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $auditeeMulai = 5;
        $auditeeNo = 1;

        $dataAmiAuditor = [];
        foreach ($amiAuditorData as $auditor) {
            switch ($auditor->status_id) {
                case 1:
                    $dataAmiAuditor['1'][$auditor->ami->unit_id] = $auditor->auditor->nomor_registrasi;
                    break;
                case 2:
                    $dataAmiAuditor['2'][$auditor->ami->unit_id] = $auditor->auditor->nomor_registrasi;
                    break;
                case 3:
                    $dataAmiAuditor['3'][$auditor->ami->unit_id] = $auditor->auditor->nomor_registrasi;
                    break;
                default:
                    # code...
                    break;
            }
        }



        // Add the 'Data Kode' sheet
        $sheet2 = $spreadsheet->createSheet();
        $sheet2->setTitle('Data Auditor');

        $sheet2->setCellValue('A1', 'Data Auditor');
        $sheet2->setCellValue('A2', 'NRA');
        $sheet2->setCellValue('B2', 'Nama Auditor');
        $sheet2->setCellValue('C2', 'Jumlah Penugasan');
        $sheet2->setCellValue('C3', 'Satuan Kerja');
        $sheet2->setCellValue('D3', 'Fakultas');
        $sheet2->setCellValue('E3', 'Program Studi');
        $sheet2->setCellValue('F2', 'Total');

        $sheet2->mergeCells('A1:B1');
        $sheet2->mergeCells('A2:A3');
        $sheet2->mergeCells('B2:B3');
        $sheet2->mergeCells('F2:F3');

        $sheet2->mergeCells('C2:E2');

        $colomInventaris = 4;

        foreach ($auditorData as $auditor) {
            $sheet2->setCellValue('A' . $colomInventaris, $auditor->nomor_registrasi);
            $sheet2->setCellValue('B' . $colomInventaris, $auditor->nama);

            // Count jumlah penugasan auditor

            $formulaC = '=COUNTIFS(\'Penugasan Auditor\'!$D$' . $auditeeMulai . ':$D$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"satker")+COUNTIFS(\'Penugasan Auditor\'!$F$' . $auditeeMulai . ':$F$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"satker")+COUNTIFS(\'Penugasan Auditor\'!$H$' . $auditeeMulai . ':$H$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"satker")';
            $formulaD = '=COUNTIFS(\'Penugasan Auditor\'!$D$' . $auditeeMulai . ':$D$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"fakultas")+COUNTIFS(\'Penugasan Auditor\'!$F$' . $auditeeMulai . ':$F$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"fakultas")+COUNTIFS(\'Penugasan Auditor\'!$H$' . $auditeeMulai . ':$H$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"fakultas")';
            $formulaE = '=COUNTIFS(\'Penugasan Auditor\'!$D$' . $auditeeMulai . ':$D$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"prodi")+COUNTIFS(\'Penugasan Auditor\'!$F$' . $auditeeMulai . ':$F$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"prodi")+COUNTIFS(\'Penugasan Auditor\'!$H$' . $auditeeMulai . ':$H$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ',\'Penugasan Auditor\'!$C$' . $auditeeMulai . ':$C$' . ($auditeeMulai + count($auditeeData)) . ',"prodi")';

            $sheet2->setCellValue('C' . $colomInventaris, $formulaC);
            $sheet2->getCell('C' . $colomInventaris)->setDataType(DataType::TYPE_FORMULA);
            $sheet2->setCellValue('D' . $colomInventaris, $formulaD);
            $sheet2->getCell('D' . $colomInventaris)->setDataType(DataType::TYPE_FORMULA);
            $sheet2->setCellValue('E' . $colomInventaris, $formulaE);
            $sheet2->getCell('E' . $colomInventaris)->setDataType(DataType::TYPE_FORMULA);

            $sheet2->setCellValue('F' . $colomInventaris, '=COUNTIF(\'Penugasan Auditor\'!$D$' . $auditeeMulai . ':$I$' . ($auditeeMulai + count($auditeeData)) . ',A' . $colomInventaris . ')');
            $sheet2->getCell('F' . $colomInventaris)->setDataType(DataType::TYPE_FORMULA);

            $colomInventaris++;
        }


        foreach ($auditeeData as $auditee) {
            $sheet1->setCellValue('A' . $auditeeMulai, $auditeeNo);
            $sheet1->setCellValue('B' . $auditeeMulai, $auditee->unit->nama);
            $sheet1->setCellValue('C' . $auditeeMulai, $auditee->unit->jenis);

            $sheet1->setCellValue('D' . $auditeeMulai, $dataAmiAuditor['1'][$auditee->unit_id] ?? null);
            $sheet1->setCellValue('F' . $auditeeMulai, $dataAmiAuditor['2'][$auditee->unit_id] ?? null);
            $sheet1->setCellValue('H' . $auditeeMulai, $dataAmiAuditor['3'][$auditee->unit_id] ?? null);

            $auditeeNo++;
            $auditeeMulai++;
        }

        $style = $sheet2->getStyle('A1:C1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet2->getStyle('A1:K2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        // Vlookup nama auditor
        foreach ($auditeeData as $index => $auditee) {
            $row = $index + 5;
            $sheet1->setCellValue('E' . $row, '=IFERROR(VLOOKUP(D' . $row . ',\'Data Auditor\'!$A$4:$B$' . (count($auditorData) + 4) . ',2,0), "-")');
            $sheet1->getCell('E' . $row)->setDataType(DataType::TYPE_FORMULA);
            $sheet1->setCellValue('G' . $row, '=IFERROR(VLOOKUP(F' . $row . ',\'Data Auditor\'!$A$4:$B$' . (count($auditorData) + 4) . ',2,0), "-")');
            $sheet1->getCell('G' . $row)->setDataType(DataType::TYPE_FORMULA);
            $sheet1->setCellValue('I' . $row, '=IFERROR(VLOOKUP(H' . $row . ',\'Data Auditor\'!$A$4:$B$' . (count($auditorData) + 4) . ',2,0), "-")');
            $sheet1->getCell('I' . $row)->setDataType(DataType::TYPE_FORMULA);
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Configure the Excel output
        $filename = 'penugasan-auditor-' . $ami->tahun . '.xlsx';

        // Render Excel to memory
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Set the response headers and send the file as a download
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        Yii::$app->response->headers->add('Content-Disposition', 'attachment;filename=' . $filename);
        Yii::$app->response->headers->add('Cache-Control', 'max-age=0');
        Yii::$app->response->content = $content;
        Yii::$app->response->send();
        Yii::$app->end();
    }

    public function actionAjaxAdd()
    {
        // echo '<pre>';print_r($_POST);die;

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $dataPost = $_POST;
        // echo '<pre>';print_r($dataPost);die;
        try {

            if (!empty($dataPost)) {
                $model = new AmiAuditor();

                $ami_auditor = Auditor::find()->where([
                    'nidn' => $dataPost['nidn'],
                ])->one();

                if (!isset($dataPost['ami_unit_id'])) {

                    $ami_unit = AmiUnit::find()->where([
                        'unit_id' => $dataPost['unit_id'],
                    ]);
                    $ami_unit->joinWith(['ami as a']);
                    $ami_unit->andWhere(['a.status_aktif' => 1]);
                    $ami_unit = $ami_unit->one();

                    $model->ami_id = $ami_unit->id;
                    $model->attributes = $dataPost;
                } else {
                    $model->ami_id = $dataPost['ami_unit_id'];
                }

                if (isset($ami_auditor)) {
                    $model->auditor_id = $ami_auditor->id;
                    $auditor = Auditor::findOne($model->auditor_id);

                    // echo '<pre>';print_r('te');die;
                    if ($model->save()) {
                        # code...
                        $user = User::findOne([
                            'auditor_id' => $auditor->id
                        ]);

                        $is_user = false;
                        if (empty($user)) {
                            $user = new User;
                            $user->auditor_id = $auditor->id;
                            $is_user = true;
                        }

                        $password = MyHelper::getRandomString(8, 8, true, false, false);

                        $user->username = $auditor->email;
                        $user->email = $auditor->email;
                        $user->nama = $auditor->nama;

                        $user->setPassword($password);
                        $user->status = User::STATUS_ACTIVE;
                        $user->access_role = 'auditor';

                        $auth = Yii::$app->authManager;

                        // if user has role, set oldRole to that role name, else offer 'member' as sensitive default
                        $oldRole = $auth->getRole('auditor');


                        // set property item_name of User object to this role name, so we can use it in our form
                        $user->access_role = $oldRole->name;
                        // echo '<pre>';print_r($model);die;

                        if ($user->save()) {
                            // if (empty())
                            if ($is_user == true)
                                $auth->assign($oldRole, $user->id);

                            // $to = MyHelper::sendEmail($user->email);
                            // $emailTemplate = $this->renderPartial('email', [
                            //     'user' => $user,
                            //     'password' => $password
                            // ]);
                            if ($auditor->is_sent != 1) {
                                // Yii::$app->mailer->compose()
                                //     ->setTo($to)
                                //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                                //     ->setSubject('[SIMUDA] Account Information')
                                //     ->setHtmlBody($emailTemplate)
                                //     ->send();

                                // $toBpm = MyHelper::emailBpm();
                                // Yii::$app->mailer->compose()
                                //     ->setTo($toBpm)
                                //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                                //     ->setSubject('[SIMUDA] Account Information')
                                //     ->setHtmlBody($emailTemplate)
                                //     ->send();

                                $auditor->is_sent = 1;
                                $auditor->save();
                            }
                        } else {
                            throw new \Exception(MyHelper::logError($user));
                        }

                        $transaction->commit();

                        $results = [
                            'code' => 200,
                            'message' => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    public function actionAjaxSend()
    {
        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $dataPost = $_POST;


        try {
            if (!empty($dataPost)) {

                $ami_auditor = $this->findModel($dataPost['id']);

                $model = Auditor::findOne($ami_auditor->auditor_id);
                // echo '<pre>';print_r($model);die;

                $model->is_sent = 1;
                $model->save();

                // Add User Auditee...

                $user = User::findOne([
                    'unit_kerja_id' => $model->id,
                ]);

                $is_user = false;
                if (empty($user)) {
                    $user = new User;
                    $is_user = true;
                }

                $password = MyHelper::getRandomString(8, 8, true, false, false);

                $user->username = $model->email;
                $user->email = $model->email;
                $user->nama = $model->nama;
                $user->unit_kerja_id = $model->id;

                $user->setPassword($password);
                $user->status = User::STATUS_ACTIVE;
                $user->access_role = 'auditor';

                $auth = Yii::$app->authManager;
                $oldRole = $auth->getRole('auditor');

                $user->access_role = $oldRole->name;
                if ($user->save()) {

                    if ($is_user == true)
                        $auth->assign($oldRole, $user->id);

                    $to = MyHelper::sendEmail($user->email);

                    $emailTemplate = $this->renderPartial('email', [
                        'user' => $user,
                        'password' => $password
                    ]);

                    Yii::$app->mailer->compose()
                        ->setTo($to)
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                        ->setSubject('[SIMUDA] Account Information')
                        ->setHtmlBody($emailTemplate)
                        ->send();


                    $toBpm = MyHelper::emailBpm();
                    Yii::$app->mailer->compose()
                        ->setTo($toBpm)
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                        ->setSubject('[SIMUDA] Account Information')
                        ->setHtmlBody($emailTemplate)
                        ->send();
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }

                $transaction->commit();
                $results = [
                    'code' => 200,
                    'message' => 'Data successfully updated'
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            // $errors .= $e->getMessage();
            $errors .= 'Oops, your data is incomplete, please complete the data';

            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }


    /**
     * Updates an existing AmiAuditor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'id' => $id,
        ]);
    }

    /**
     * Deletes an existing AmiAuditor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletee($id, $ami_id)
    {
        // echo '<pre>';print_r('Tes');exit;
        $this->findModel($id)->delete();

        return $this->redirect(['ami-unit/view', 'id' => $ami_id]);
    }


    /**
     * Finds the AmiAuditor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AmiAuditor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmiAuditor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function sendMailAssignment($amiUnit, $auditor)
    {
        $to = MyHelper::sendEmail($auditor->email);

        $emailTemplate = $this->renderPartial('mail_assignment');

        Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
            ->setSubject('[SIMUDA] Assignment Information')
            ->setHtmlBody($emailTemplate)
            ->send();
    }
}
