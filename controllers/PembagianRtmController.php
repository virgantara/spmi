<?php

namespace app\controllers;

use Yii;
use app\models\PembagianRtm;
use app\models\PembagianRtmSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PembagianRtmController implements the CRUD actions for PembagianRtm model.
 */
class PembagianRtmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PembagianRtm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PembagianRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PembagianRtm model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PembagianRtm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PembagianRtm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PembagianRtm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionAjaxPembagianRtm()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $relation = PembagianRtm::find()->where([
            'unit_kerja_id'      => $dataPost['unit_kerja_id'],
        ])->one();
        
        #check data
        if (!empty($relation)) {
            $relation->attributes = $dataPost;
            if($relation->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Data berhasil di update'
                ];
            }
        }else {
            $relation = new PembagianRtm();
            $relation->attributes = $dataPost;
            if($relation->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Data berhasil di tambahkan'
                ];
            }

            else
            {
                $error = \app\helpers\MyHelper::logError($relation);
                $results = [
                    'code' => 500,
                    'message' => $error
                ];
            }
        }

        echo json_encode($results);
        
        exit;
    }

    /**
     * Deletes an existing PembagianRtm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PembagianRtm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PembagianRtm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PembagianRtm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
