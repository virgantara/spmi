<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Asesmen;
use Yii;
use app\models\AsesmenDokumen;
use app\models\AsesmenDokumenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AsesmenDokumenController implements the CRUD actions for AsesmenDokumen model.
 */
class AsesmenDokumenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AsesmenDokumen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AsesmenDokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AsesmenDokumen model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAjaxAddDokumen()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;
            if (!empty($dataPost)) {
                $asesmen = Asesmen::findOne(['indikator_id' => $dataPost['indikator_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);

                if (empty($asesmen)) {
                    $asesmen = new Asesmen();
                    $asesmen->indikator_id = $dataPost['indikator_id'];
                    $asesmen->ami_unit_id = $dataPost['ami_unit_id'];
                    $asesmen->save();
                }

                $dokumen = new AsesmenDokumen();
                $dokumen->asesmen_id = $asesmen->id;
                $dokumen->dokumen_id = $dataPost['dokumen_id'];

                if ($dokumen->save()) {
                    $transaction->commit();
                    $results = [
                        'code' => 200,
                        'message' => 'Dokumen berhasil ditambahkan',
                        'data' => [
                            'id' => $dataPost['dokumen_id'],
                            'nama' => $dokumen->dokumen->nama,
                            'asesmen_dokumen_id' => $dokumen->id,
                        ],
                    ];
                } else {
                    $errors .= MyHelper::logError($dokumen);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    /**
     * Creates a new AsesmenDokumen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AsesmenDokumen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AsesmenDokumen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AsesmenDokumen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteById()
    {
        $id = $_POST['id'];
        $model = $this->findModel($id);
        $asesmen = Asesmen::setPenilaianClear($model->asesmen_id);

        if ($model->delete() && $asesmen) {
            $results = [
                'code' => 200,
                'message' => 'Bukti berhasil dihapus'
            ];
        } else {
            $results = [
                'code' => 500,
                'message' => MyHelper::logError($model)
            ];
        }
        echo json_encode($results);
        exit;
    }

    /**
     * Finds the AsesmenDokumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AsesmenDokumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AsesmenDokumen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
