<?php

namespace app\controllers;

use Yii;
use app\models\PembagianKriteria;
use app\models\PembagianKriteriaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PembagianKriteriaController implements the CRUD actions for PembagianKriteria model.
 */
class PembagianKriteriaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PembagianKriteria models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PembagianKriteriaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PembagianKriteria model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PembagianKriteria model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PembagianKriteria();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxPembagianKriteria()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $pembagian = PembagianKriteria::find()->where([
            'kriteria_id' => $dataPost['kriteria_id'],
            'jenis_unit' => $dataPost['jenis_unit'],
        ])->one();
        
        if (empty($pembagian)) {
            $pembagian = new PembagianKriteria();
            $pembagian->attributes = $dataPost;
            if ($pembagian->save()) {
                $results = [
                    'code'      => 200,
                    'message'   => 'data berhasil ditambah',
                ];
            }else {
                $error = \app\helpers\MyHelper::logError($pembagian);
                $results = [
                    'code'      => 500,
                    'message'   => $error,
                ];
            }
        }else {
            if($dataPost['checked'] == '0')
            {
                $pembagian->delete();
                $results = [
                    'code' => 200,
                    'message' => 'Bagian berhasil dihapus'
                ];
            }else {
                $error = \app\helpers\MyHelper::logError($pembagian);
                $results = [
                    'code'      => 500,
                    'message'   => $error,
                ];
            }
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing PembagianKriteria model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PembagianKriteria model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PembagianKriteria model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PembagianKriteria the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PembagianKriteria::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}