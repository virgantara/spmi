<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\AmiUnit;
use Yii;
use app\models\BahanRtm;
use app\models\BahanRtmSearch;
use app\models\PenanggungJawabBahanRtm;
use app\models\Rtm;
use app\models\Temuan;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Console\Helper\Dumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * BahanRtmController implements the CRUD actions for BahanRtm model.
 */
class BahanRtmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BahanRtm models.
     * @return mixed
     */

    // public function actionIndex()
    // {
    //     $searchModel = new BahanRtmSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 1);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single BahanRtm model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    // public function actionAjaxGetAras()
    // {
    //     $q = Yii::$app->request->get('term');
    //     $list = ['Universitas', 'Satuan Kerja', 'UPPS', 'Program Studi'];
    //     $out = [];

    //     foreach ($list as $d) {
    //         if (stripos($d, $q) !== false) {
    //             $out[] = [
    //                 'id' => md5($d),
    //                 'label' => $d,
    //             ];
    //         }
    //     }


    //     echo \yii\helpers\Json::encode($out);

    //     die();
    // }

    public function actionCreate($rtm_id)
    {
        $model = new BahanRtm();

        $rtm = Rtm::findOne($rtm_id);

        $amiUnit = AmiUnit::findOne(['unit_id' => Yii::$app->user->identity->unit_kerja_id, 'ami_id' => $rtm->ami_id]);

        $dataTemuan = [];
        if (!empty($amiUnit)) {
            $dataTemuan = Temuan::find()->where(['ami_unit_id' => $amiUnit->id])->all();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['rtm/view', 'id' => $model->rtm_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'rtm' => $rtm,
            'dataTemuan' => $dataTemuan,
        ]);
    }

    /**
     * Updates an existing BahanRtm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $rtm = Rtm::findOne($model->rtm_id);

        $amiUnit = AmiUnit::findOne(['unit_id' => Yii::$app->user->identity->unit_kerja_id, 'ami_id' => $rtm->ami_id]);
        $dataTemuan = Temuan::find()->where(['ami_unit_id' => $amiUnit->id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['rtm/view', 'id' => $model->rtm_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'rtm' => $rtm,
            'dataTemuan' => $dataTemuan,
        ]);
    }


    public function actionTemplateDownload($id)
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $rtm = Rtm::findOne($id);
        // $kriteria = Kriteria::find()->orderBy(['urutan' => SORT_ASC]);
        // $satker = UnitKerja::find()->where(['jenis' => 'satker']);

        // Add the first sheet
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Bahan RTM');
        $sheet1->setCellValue('A1', 'Pemenuhan Bahan RTM');
        $sheet1->setCellValue('A2', 'Kode LBS');
        $sheet1->setCellValue('B2', 'Lingkup Bahan Standar');
        $sheet1->setCellValue('C2', 'Kode Aspek');
        $sheet1->setCellValue('D2', 'Aspek');
        $sheet1->setCellValue('E2', 'Deskripsi Kondisi');
        $sheet1->setCellValue('F2', 'Rencana Tindak Lanjut');
        $sheet1->setCellValue('G2', 'Target Waktu Selesai (yyyy-mm-dd)');
        $sheet1->setCellValue('H2', 'Output');
        $sheet1->setCellValue('I2', 'Penanggung Jawab');
        $sheet1->setCellValue('J2', 'Pelaksana');
        $sheet1->setCellValue('K2', 'Kode Aras');
        $sheet1->setCellValue('L2', 'Aras');
        $sheet1->setCellValue('M2', 'Kode Persetujuan');
        $sheet1->setCellValue('N2', 'Persetujuan');

        $sheet1->setCellValue('A3', 'Contoh:');
        $sheet1->setCellValue('A4', '1');
        $sheet1->setCellValue('B4', 'Kriteria 1');
        $sheet1->setCellValue('C4', 'VMTS');
        $sheet1->setCellValue('D4', 'visi misi dibentuk ');
        $sheet1->setCellValue('E4', 'Tracer study yang dilakukan UPPS telah mencakup 5 aspek.');
        $sheet1->setCellValue('F4', 'Permendikbud');
        $sheet1->setCellValue('G4', 'Strategi');
        $sheet1->setCellValue('H4', '1');
        $sheet1->setCellValue('I4', '');
        $sheet1->setCellValue('J4', '1');
        $sheet1->setCellValue('K4', '2023');
        $sheet1->setCellValue('L4', 'BPM');
        $sheet1->setCellValue('M4', 'Badan Penjaminan Mutu');

        $sheet1->setCellValue('A5', 'Mulai Tambahkan Data Dibawah ini:');

        $sheet1->mergeCells('A1:M1');

        $style = $sheet1->getStyle('A1:M1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet1->getStyle('A1:M2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        // Add the 'Data Kode' sheet
        $sheet2 = $spreadsheet->createSheet();
        $sheet2->setTitle('Data Kode');

        $sheet2->setCellValue('A1', 'KODE KRITERIA STANDAR');
        $sheet2->setCellValue('A2', 'kode');
        $sheet2->setCellValue('B2', 'Nama Kriteria');

        $sheet2->mergeCells('A1:B1');

        $colomInventaris = 3; // mulai colom inventaris
        $noInventaris = 1; // mulai no inventaris

        // foreach ($kriteria->all() as $i) {
        //     $sheet2->setCellValue('A' . $colomInventaris, $i->kode);
        //     $sheet2->setCellValue('B' . $colomInventaris, $i->nama);
        //     $colomInventaris++;
        //     $noInventaris++;
        // }

        $sheet2->setCellValue('D1', 'KODE INVENTARIS RUANGAN');
        $sheet2->setCellValue('D2', 'kode');
        $sheet2->setCellValue('E2', 'Nama Ruangan');

        $sheet2->mergeCells('D1:E1');

        $colomRuangan = 3; // mulai colom ruangan
        $noRuangan = 1; // mulai no ruangan

        // foreach ($satker->all() as $i) {
        //     $sheet2->setCellValue('D' . $colomRuangan, $i->singkatan);
        //     $sheet2->setCellValue('E' . $colomRuangan, $i->nama);
        //     $colomRuangan++;
        //     $noRuangan++;
        // }

        $style = $sheet2->getStyle('A1:C1');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet2->getStyle('A1:K2')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        //Vlookup Alat Inventaris
        // for ($i = 6; $i < 100; $i++) {
        //     $sheet1->setCellValue('B' . $i, '=IFERROR(VLOOKUP(A' . $i . ',\'Data Kode\'!$A$3:$B$' . ($kriteria->count() + 3) . ',2,0), "-")');
        //     $sheet1->getCell('B' . $i)->setDataType(DataType::TYPE_FORMULA);

        //     $sheet1->setCellValue('M' . $i, '=IFERROR(VLOOKUP(L' . $i . ',\'Data Kode\'!$D$3:$E$' . ($satker->count() + 3) . ',2,0), "-")');
        //     $sheet1->getCell('M' . $i)->setDataType(DataType::TYPE_FORMULA);
        // }

        $spreadsheet->setActiveSheetIndex(0);

        // Configure the Excel output
        $filename = 'input-bahan-rtm-' . strtolower($rtm->unitKerja->singkatan) . '-' . date('Y') . '.xlsx';

        // Render Excel to memory
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Set the response headers and send the file as a download
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        Yii::$app->response->headers->add('Content-Disposition', 'attachment;filename=' . $filename);
        Yii::$app->response->headers->add('Cache-Control', 'max-age=0');
        Yii::$app->response->content = $content;
        Yii::$app->response->send();
        Yii::$app->end();
    }

    public function actionHasilRapat($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 1, $rtmId = $id);

        return $this->render('hasil_rapat', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionSurvey($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 2, $rtmId = $id);

        return $this->render('survey', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionCapaian($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 3, $rtmId = $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = BahanRtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['BahanRtm']);
            $post = ['BahanRtm' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('capaian', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionTindakLanjut($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 4, $rtmId = $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = BahanRtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['BahanRtm']);
            $post = ['BahanRtm' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('tindak_lanjut', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionPerbaikanMutuInternal($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 5, $rtmId = $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = BahanRtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['BahanRtm']);
            $post = ['BahanRtm' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('perbaikan_mutu_internal', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionPerbaikanMutuEksternal($id)
    {
        $rtm = Rtm::findOne($id);
        $searchModel = new BahanRtmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $jenis = 6, $rtmId = $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = BahanRtm::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['BahanRtm']);
            $post = ['BahanRtm' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {

                // can save model or do something before saving model
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            // return ajax json encoded response and exit
            echo $out;
            exit;
        }

        return $this->render('perbaikan_mutu_eksternal', [
            'rtm'           => $rtm,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionAjaxAdd()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $model = new BahanRtm;
                $model->attributes = $dataPost;

                if (is_array($dataPost['penanggung_jawab'])) {
                    $model->penanggung_jawab = '';

                    if ($model->save()) {
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }


                    foreach ($dataPost['penanggung_jawab'] as $value) {
                        $pj = [
                            'bahan_rtm_id'              => $model->id,
                            'penanggung_jawab_rtm_id'   => $value,
                        ];
                        $datas[] = $pj;
                    }

                    $batchDatas = Yii::$app->db->createCommand()->batchInsert('penanggung_jawab_bahan_rtm', [
                        'bahan_rtm_id',
                        'penanggung_jawab_rtm_id',
                    ], $datas);

                    if ($batchDatas->execute()) {
                        $transaction->commit();
                        $results = [
                            'code'      => 200,
                            'message'   => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($batchDatas);
                        throw new \Exception;
                    }
                } else {
                    if ($model->save()) {
                        $transaction->commit();
                        $results = [
                            'code'      => 200,
                            'message'   => 'Data successfully added'
                        ];
                    } else {
                        $errors .= MyHelper::logError($model);
                        throw new \Exception;
                    }
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Deletes an existing BahanRtm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $model->delete();

        Yii::$app->session->setFlash('success', "Data bahan RTM berhasil dihapus");
        return $this->redirect(['rtm/view', 'id' => $model->rtm_id]);
    }

    /**
     * Finds the BahanRtm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BahanRtm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BahanRtm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
