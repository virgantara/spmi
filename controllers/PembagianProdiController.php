<?php

namespace app\controllers;

use Yii;
use app\models\PembagianProdi;
use app\models\PembagianProdiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PembagianProdiController implements the CRUD actions for PembagianProdi model.
 */
class PembagianProdiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PembagianProdi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PembagianProdiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PembagianProdi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PembagianProdi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PembagianProdi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxProdiFakultas()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $relation = PembagianProdi::find()->where([
            'prodi_id'      => $dataPost['prodi_id'],
        ])->one();
        
        #check data
        if (!empty($relation)) {
            $relation->attributes = $dataPost;
            if($relation->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Data berhasil di update'
                ];
            }
        }else {
            $relation = new PembagianProdi();
            $relation->attributes = $dataPost;
            if($relation->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Data berhasil di tambahkan'
                ];
            }

            else
            {
                $error = \app\helpers\MyHelper::logError($relation);
                $results = [
                    'code' => 500,
                    'message' => $error
                ];
            }
        }

        echo json_encode($results);
        
        exit;
    }

    /**
     * Updates an existing PembagianProdi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PembagianProdi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PembagianProdi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PembagianProdi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PembagianProdi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}