<?php

namespace app\controllers;

use Yii;
use app\models\AmanahKinerjaUnitKerja;
use app\models\AmanahKinerjaUnitKerjaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\AmanahKinerja;
use app\models\UnitKerja;

/**
 * AmanahKinerjaUnitKerjaController implements the CRUD actions for AmanahKinerjaUnitKerja model.
 */
class AmanahKinerjaUnitKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'detail'],
                'rules' => [
                    [
                        'actions' => [
                            'detail'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'create', 'update', 'view', 'index', 'delete', 'detail'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmanahKinerjaUnitKerja models.
     * @return mixed
     */
    public function actionIndex($unit_kerja_id, $amanah_kinerja_id)
    {
        $searchModel = new AmanahKinerjaUnitKerjaSearch();
        $filters = [
            'unit_kerja_id' => $unit_kerja_id,
            'amanah_kinerja_id' => $amanah_kinerja_id
        ];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionDetail($unit_kerja_id, $amanah_kinerja_id)
    {
        $unitKerja = UnitKerja::findOne($unit_kerja_id);
        $amanahKinerja = AmanahKinerja::findOne($amanah_kinerja_id);
        $searchModel = new AmanahKinerjaUnitKerjaSearch();
        $filters = [
            'unit_kerja_id' => $unit_kerja_id,
            'amanah_kinerja_id' => $amanah_kinerja_id
        ];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $filters);


        if (Yii::$app->request->post('hasEditable')) {

            $id = Yii::$app->request->post('editableKey');
            $model = AmanahKinerjaUnitKerja::findOne($id);

            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['AmanahKinerjaUnitKerja']);
            $post = ['AmanahKinerjaUnitKerja' => $posted];

            if ($model->load($post)) {

                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $error = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => 'Oops, ' . $error]);
                }
            }
            echo $out;
            exit;
        }

        return $this->render('detail', [
            'unitKerja' => $unitKerja,
            'amanahKinerja' => $amanahKinerja,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmanahKinerjaUnitKerja model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AmanahKinerjaUnitKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmanahKinerjaUnitKerja();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AmanahKinerjaUnitKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AmanahKinerjaUnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AmanahKinerjaUnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AmanahKinerjaUnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmanahKinerjaUnitKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
