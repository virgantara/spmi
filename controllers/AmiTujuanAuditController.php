<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\AmiTujuanAudit;
use app\models\AmiTujuanAuditSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AmiTujuanAuditController implements the CRUD actions for AmiTujuanAudit model.
 */
class AmiTujuanAuditController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AmiTujuanAudit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmiTujuanAuditSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmiTujuanAudit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AmiTujuanAudit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AmiTujuanAudit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSetTujuanAudit()
    {
        $results = [];
        $errors = [];

        try {
            $dataPost = Yii::$app->request->post();

            // Validasi data yang diterima
            if (!empty($dataPost['tujuan_audit_id']) && !empty($dataPost['ami_unit_id'])) {
                $amiTujuanAudit = AmiTujuanAudit::findOne(['tujuan_audit_id' => $dataPost['tujuan_audit_id'], 'ami_unit_id' => $dataPost['ami_unit_id']]);

                if ($amiTujuanAudit !== null) {
                    // Hapus data jika sudah ada
                    if ($amiTujuanAudit->delete()) {
                        $results = [
                            'code' => 200,
                            'message' => "Yeay, Data berhasil dihapus",
                        ];
                    } else {
                        $errors[] = MyHelper::logError($amiTujuanAudit);
                    }
                } else {
                    // Tambahkan data baru jika tidak ditemukan
                    $model = new AmiTujuanAudit();
                    $model->attributes = $dataPost;

                    if ($model->save()) {
                        $results = [
                            'code' => 200,
                            'message' => "Yeay, Data berhasil ditambahkan",
                        ];
                    } else {
                        $errors[] = MyHelper::logError($model);
                    }
                }
            } else {
                $errors[] = 'Opps, data yang dikirim kosong !';
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
        }

        if (!empty($errors)) {
            $results = [
                'code' => 500,
                'message' => implode("\n", $errors),
            ];
        }

        echo json_encode($results);
    }



    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AmiTujuanAudit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionRencanaDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['asesmen/rencana', 'id' => $model->ami_unit_id]);
    }

    /**
     * Finds the AmiTujuanAudit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AmiTujuanAudit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmiTujuanAudit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
