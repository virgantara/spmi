<?php

namespace app\controllers;

use Yii;
use app\models\MonevBukti;
use app\models\MonevBuktiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;
use app\models\MonevUnitKerjaObjek;

/**
 * MonevBuktiController implements the CRUD actions for MonevBukti model.
 */
class MonevBuktiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete', 'ajax-set-bukti'],
                'rules' => [
                    [
                        'actions' => [
                            'detail',
                            'ajax-set-bukti',
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'delete',
                            'detail',
                            'ajax-set-bukti'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MonevBukti models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonevBuktiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MonevBukti model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MonevBukti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MonevBukti();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSetBukti()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $monevObjek = MonevUnitKerjaObjek::findOne($dataPost['monev_unit_kerja_objek_id']);

                $model = new MonevBukti();

                $model->id = MyHelper::gen_uuid();
                $model->attributes = $dataPost;
                $model->monev_unit_kerja_id = $monevObjek->monev_unit_kerja_id;

                if ($model->save()) {
                    $transaction->commit();

                    $results = [
                        'code'      => 200,
                        'message'   => 'Data saved successfully'
                    ];
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing MonevBukti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MonevBukti model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        Yii::$app->session->setFlash('success', "Data berhasil dihapus");
        return $this->redirect(['/monev-unit-kerja-objek/' . $model->monevUnitKerjaObjek->jenis_objek, 'id' => $model->monev_unit_kerja_id]);
    }

    /**
     * Finds the MonevBukti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MonevBukti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MonevBukti::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
