<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Ami;
use app\models\AmiTujuanAudit;
use app\models\AmiUnit;
use Yii;
use app\models\TujuanAudit;
use app\models\TujuanAuditSearch;
use app\models\UnitKerja;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * TujuanAuditController implements the CRUD actions for TujuanAudit model.
 */
class TujuanAuditController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TujuanAudit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TujuanAuditSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpload()
    {
        $errors = '';
        $counter = 0;

        if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstanceByName('file');

            if ($file != null) {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                try {
                    $inputFileType = IOFactory::identify($file->tempName);
                    $reader = IOFactory::createReader($inputFileType);
                    $spreadsheet = $reader->load($file->tempName);
                    $worksheet = $spreadsheet->getSheetByName('Master Tujuan Audit');
                    $highestRow = $worksheet->getHighestRow();

                    // Prepare arrays for mapping

                    $arrTujuanAudit = [];
                    $tujuanAudit = ArrayHelper::map(TujuanAudit::find()->all(), 'tujuan_audit', 'id');
                    foreach ($tujuanAudit as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrTujuanAudit[$newKey] = $value;
                    }
                    $ami = Ami::findOne(['status_aktif' => 1]);

                    $arrAmiUnit = [];
                    $amiUnit = ArrayHelper::map(AmiUnit::find()->where(['ami_id' => $ami->id])->all(), function ($data) {
                        return $data->unit->singkatan;
                    }, 'id');
                    foreach ($amiUnit as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAmiUnit[$newKey] = $value;
                    }

                    $arrAmiTujuanAudit = [];
                    $amiTujuanAudit = ArrayHelper::map(AmiTujuanAudit::find()->joinWith(['amiUnit.ami as aa'])->where(['aa.status_aktif' => 1])->all(), function ($data) {
                        return $data->tujuan_audit_id . '-' . $data->ami_unit_id;
                    }, 'id');
                    foreach ($amiTujuanAudit as $key => $value) {
                        $newKey = strtolower(trim($key));
                        $newKey = preg_replace('/\s+/', ' ', $newKey);
                        $arrAmiTujuanAudit[$newKey] = $value;
                    }

                    $bTujuan = 0;
                    $gTujuan = 0;
                    $bAmiTujuan = 0;

                    // Cek Tujuan Audit *jika ada penambahan (tidak support pengurangan)
                    $jumlahTujuan = 0;
                    for ($r = 4; $r < ($highestRow + 1); $r++) {
                        $A = $worksheet->getCell('A' . $r)->getValue();
                        if (!isset($arrTujuanAudit[strtolower(trim($A))])) {
                            $modelTa = new TujuanAudit();
                            $modelTa->tujuan_audit = $A;
                            if ($modelTa->save()) {
                                $newKey = strtolower(trim($A));
                                $newKey = preg_replace('/\s+/', ' ', $newKey);
                                $arrTujuanAudit[$newKey] = $modelTa->id;
                                $bTujuan++;
                            } else $gTujuan++;
                        }
                        $jumlahTujuan++;
                    }

                    $mulaiKolom = 2;
                    $dataTujuanUnit = [];
                    foreach ($arrAmiUnit as $key => $value) {
                        $mulaiBaris = 4;
                        for ($jmlTujuan = 0; $jmlTujuan < $jumlahTujuan; $jmlTujuan++) {
                            $status = $worksheet->getCell(MyHelper::convertToLetters($mulaiKolom) . $mulaiBaris)->getValue();
                            $tujuan = $worksheet->getCell('A' . $mulaiBaris)->getValue();
                            $tujuanId = $arrTujuanAudit[strtolower(trim($tujuan))];
                            if (!empty($status) && !isset($arrAmiTujuanAudit[$tujuanId . '-' . $value])) {
                                $dataTujuanUnit[] = [
                                    'tujuan_audit_id' => $tujuanId,
                                    'ami_unit_id' => $value
                                ];
                                $bAmiTujuan++;
                            }
                            $mulaiBaris++;
                        }
                        $mulaiKolom++;
                    }
                    
                    // Batch insert after the loop
                    if (!empty($dataTujuanUnit)) {
                        Yii::$app->db->createCommand()->batchInsert('ami_tujuan_audit', [
                            'tujuan_audit_id',
                            'ami_unit_id',
                        ], $dataTujuanUnit)->execute();
                    }

                    // Committing the transaction after batch insert
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', $bTujuan . ' data tujuan audit berhasil diinput');
                    Yii::$app->session->setFlash('danger', $gTujuan . ' data tujuan audit gagal diinput');
                    Yii::$app->session->setFlash('info', $bAmiTujuan . ' data tujuan audit [audite] berhasil diinput');
                    return $this->redirect(['index']);
                } catch (\Throwable $th) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Terjadi kesalahan: ' . $th->getMessage());
                }
            } else {
                Yii::$app->session->setFlash('error', 'File tidak ditemukan.');
            }
        }
    }

    public function actionDownloadMaster()
    {
        // Create a new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $tujuanAudit = TujuanAudit::find();
        $ami = Ami::findOne(['status_aktif' => 1]);
        $amiUnit = AmiUnit::find()->joinWith(['ami as a']);
        $amiUnit->where(['a.status_aktif' => 1]);

        // Add the first sheet
        $sheet1 = $spreadsheet->getActiveSheet();
        $sheet1->setTitle('Master Tujuan Audit');
        $sheet1->setCellValue('A1', 'Master Tujuan Audit');

        $sheet1->setCellValue('A2', 'Periode');
        $sheet1->setCellValue('B2', $ami->nama);

        $sheet1->setCellValue('A3', 'Tujuan Audit');

        $mulai = 2;
        foreach ($amiUnit->all() as $au) {
            $sheet1->setCellValue(MyHelper::convertToLetters($mulai) . '3', $au->unit->singkatan);
            $mulai++;
        }

        $mulai = 4;
        foreach ($tujuanAudit->all() as $ta) {
            $sheet1->setCellValue('A' . $mulai, $ta->tujuan_audit);
            $mulai++;
        }


        $sheet1->mergeCells('A1:M1');

        $style = $sheet1->getStyle('A1:M3');
        $font = $style->getFont();

        $font->setBold(true);
        $sheet1->getStyle('A1:M3')->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        $spreadsheet->setActiveSheetIndex(0);

        // Configure the Excel output
        $filename = 'data-master-tujuan-ami-' . date('Y') . '.xlsx';

        // Render Excel to memory
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Set the response headers and send the file as a download
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        Yii::$app->response->headers->add('Content-Disposition', 'attachment;filename=' . $filename);
        Yii::$app->response->headers->add('Cache-Control', 'max-age=0');
        Yii::$app->response->content = $content;
        Yii::$app->response->send();
        Yii::$app->end();
    }

    /**
     * Displays a single TujuanAudit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionAjaxAdd()
    {
        $results   = [];
        $errors    = '';
        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                $model = new TujuanAudit();
                $model->attributes = $dataPost;

                if ($model->save()) {
                    $results   = [
                        'code'     => 200,
                        'message'  => "Yeay, Data berhasil ditambahkan",
                    ];
                } else {
                    $errors    = MyHelper::logError($model);
                    throw new \Exception;
                }
            } else {
                $errors .= 'Opps, data yang dikirim kosong !';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'     => 500,
                'message'  => $errors,
            ];
        } catch (\Throwable $e) {
            $errors .= $e->getMessage();
            $results = [
                'code'     => 500,
                'message'  => $errors,
            ];
        }

        echo json_encode($results);

        die();
    }

    /**
     * Creates a new TujuanAudit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TujuanAudit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TujuanAudit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TujuanAudit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TujuanAudit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TujuanAudit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TujuanAudit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
