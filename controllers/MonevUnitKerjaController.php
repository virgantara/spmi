<?php

namespace app\controllers;

use Yii;
use app\models\MonevUnitKerja;
use app\models\MonevUnitKerjaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;

/**
 * MonevUnitKerjaController implements the CRUD actions for MonevUnitKerja model.
 */
class MonevUnitKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'view', 'index', 'delete'],
                'rules' => [
                    [
                        'actions' => [
                            'view'
                        ],
                        'allow' => true,
                        'roles' => ['auditee'],
                    ],
                    [
                        'actions' => [
                            'view', 'index', 'delete'
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => [
                            'view', 'index', 'delete', 'create', 'update'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MonevUnitKerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MonevUnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MonevUnitKerja model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($monev_id)
    {
        return $this->render('view', [
            'model' => $this->findModelByUnitKerja($monev_id),
        ]);
    }

    public function actionToView($monev_id, $unit_kerja_id)
    {
        return $this->render('view', [
            'model' => $this->findModelByUnitKerja($monev_id, $unit_kerja_id),
        ]);
    }

    /**
     * Creates a new MonevUnitKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MonevUnitKerja();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionLaporanDownload()
    {

        try {
            ob_start();
            $this->layout = '';
            
            // Instansiasi TCPDF
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
        
            // Daftarkan font
            $pala = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
            $palaI = Yii::getAlias('@webroot') . '/gentela/fonts/palai.ttf';
            $palaB = Yii::getAlias('@webroot') . '/gentela/fonts/palab.ttf';
            $fontReg = \TCPDF_FONTS::addTTFfont($pala, 'TrueTypeUnicode', '', 86);
            $fontItalic = \TCPDF_FONTS::addTTFfont($palaI, 'TrueTypeUnicode', '', 86);
            $fontBold = \TCPDF_FONTS::addTTFfont($palaB, 'TrueTypeUnicode', '', 86);
        
            // Gambar logo
            $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
        
            // COVER 
            echo $this->renderPartial('laporan-template/01_cover', [
                'nomor_dokumen' => "123",
                'satuan_kerja' => "123",
                'kepala_satker' => "123",
                'koordinator' => "123",
                'nama_warek' => "123",
                'tahun' => "1445/2024",
            ]);
        
            $data = ob_get_clean();
            $pdf->SetFont($fontReg, '', 10);
            $pdf->AddPage();
            $pdf->Image($imgdata, 75, 70, 60);
            $pdf->writeHTML($data, true, false, true, false, '');
        
            // KATA PENGANTAR
            echo $this->renderPartial('laporan-template/02_kata_pengantar', []);
        
            $data = ob_get_clean();
            $pdf->SetFont($fontReg, '', 10);
            $pdf->AddPage();
            $pdf->writeHTML($data, true, false, true, false, '');

            // LEMBAR PENGESAHAN
            echo $this->renderPartial('laporan-template/03_lembar_pengesahan', [
                ''
            ]);
        
            $data = ob_get_clean();
            $pdf->SetFont($fontReg, '', 10);
            $pdf->AddPage();
            $pdf->writeHTML($data, true, false, true, false, '');
        
            // Output PDF
            $pdf->Output('Laporan_' . "Monev" . '.pdf');
        
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
        


        // try {

        //     ob_start();

        //     $this->layout = '';
        //     ob_start();

        //     // SUMBER DATA
        //     $amiUnit = AmiUnit::findOne($ami_unit_id);
        //     $listTemuan = Temuan::findAll(['ami_unit_id' => $ami_unit_id]);
        //     $auditor = AmiAuditor::find()->where([
        //         'ami_id' => $ami_unit_id,
        //     ])->andWhere([
        //         'not', ['status_id' => 1]
        //     ]);
        //     $listAuditor = $auditor->all();
        //     $jumlahAuditor = $auditor->count();
        //     $ketuaAuditor = AmiAuditor::find()->where([
        //         'ami_id' => $ami_unit_id,
        //         'status_id' => 1,
        //     ])->one();
        //     $jenjang = JenjangMap::find()->where(['unit_kerja_id' => $amiUnit->unit->id])->one();
        //     $listTemuan = Temuan::find()->where(['ami_unit_id' => $ami_unit_id])->all();
        //     $lembar = 1;
        //     $listKriteria = Kriteria::find();
        //     $listSusunan = Jadwal::find()->where(['ami_unit_id' => $ami_unit_id])->orderBy(['urutan' => SORT_ASC])->all();
        //     $listKriteria->joinWith(['pembagianKriterias as pk']);

        //     if ($amiUnit->unit->jenis == "prodi") {
        //         $listKriteria
        //             ->where(['jenis_unit' => 2])
        //             ->orWhere([
        //                 'jenis_unit' => 1
        //             ])
        //             ->orderBy(['urutan' => SORT_ASC]);
        //     }


        //     //-- SOURCE
        //     $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //     $pdf->SetPrintHeader(false);
        //     $pdf->SetPrintFooter(false);
        //     $fontpath = Yii::getAlias('@webroot') . '/gentela/fonts/pala.ttf';
        //     $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);
        //     $imgdata = Yii::getAlias('@webroot') . '/gentela/images/logo-ori.png';
        //     $logoFull = Yii::getAlias('@webroot') . '/gentela/images/logo-full.png';
        //     $style = array(
        //         'border' => false,
        //         'padding' => 0,
        //         'fgcolor' => array(0, 0, 0),
        //         'bgcolor' => false, //array(255,255,255)
        //     );

        //     //00. COVER
        //     echo $this->renderPartial('laporan_cover', [
        //         'ketuaAuditor' => $ketuaAuditor,
        //         'listTemuan' => $listTemuan,
        //         'amiUnit' => $amiUnit,
        //         'jenjang' => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
        //         'jumlahAuditor' => $jumlahAuditor + 1
        //     ]);

        //     $data = ob_get_clean();
        //     ob_start();
        //     $pdf->SetFont($fontreg, '', 10);
        //     $pdf->AddPage();
        //     $pdf->Image($imgdata, 80, 60, 45);
        //     $pdf->writeHTML($data);


        //     //01. PENGANTAR
        //     echo $this->renderPartial('laporan_pengantar', [
        //         'amiUnit' => $amiUnit,
        //     ]);

        //     $data = ob_get_clean();
        //     ob_start();
        //     $pdf->SetFont($fontreg, '', 10);
        //     $pdf->AddPage();
        //     $pdf->writeHTML($data);


        //     //02. SURAT TUGAS
        //     echo $this->renderPartial('//surat/surat_tugas', [
        //         'amiUnit' => $amiUnit,
        //         'jumlahAuditor' => $jumlahAuditor + 1
        //     ]);

        //     $data = ob_get_clean();
        //     ob_start();
        //     $pdf->SetFont($fontreg, '', 10);
        //     $pdf->AddPage();
        //     $pdf->Image($logoFull, 78, 10, 50);
        //     $pdf->writeHTML($data);


        //     //03. BERITA ACARA
        //     echo $this->renderPartial('//surat/cetak_berita', [
        //         'ketuaAuditor' => $ketuaAuditor,
        //         'listKriteria' => $listKriteria,
        //         'listTemuan' => $listTemuan,
        //         'amiUnit' => $amiUnit,
        //         'jumlahAuditor' => $jumlahAuditor + 1
        //     ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 10);
        //     // $pdf->AddPage();
        //     // $pdf->Image($imgdata, 20, 13, 14);
        //     // $pdf->writeHTML($data);
        //     // #DATA
        //     // $mulai  = 70;
        //     // $jarak  = 48;
        //     // $tinggi = 150;
        //     // $data = [
        //     //     'ami_unit_id' => $amiUnit->id,
        //     //     'dokumen_id'  => 0,
        //     // ];
        //     // ##BARCODE PENANGGUNGJAWAB
        //     // $pj['is_auditee'] = true;
        //     // $dataPj = $data + $pj;
        //     // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE KETUA AUDITOR
        //     // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
        //     // $kAuditor['is_auditee'] = false;
        //     // $dataKetua = $data + $kAuditor;
        //     // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE ANGGOTA AUDITOR
        //     // if (isset($listAuditor)) {
        //     //     $position = $mulai + $jarak;
        //     //     foreach ($listAuditor as $auditor) {
        //     //         $aAuditor['auditor_id'] = $auditor->auditor_id;
        //     //         $aAuditor['is_auditee'] = false;
        //     //         $dataAuditor = $data + $aAuditor;
        //     //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
        //     //         $position = $position + $jarak;
        //     //     }
        //     // }


        //     // //04. SUSUNAN ACARA ----
        //     // echo $this->renderPartial('//jadwal/susunan_acara', [
        //     //     'amiUnit'       => $amiUnit,
        //     //     'jumlahAuditor' => $jumlahAuditor + 2,
        //     //     'listSusunan'   => $listSusunan,
        //     // ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 10);
        //     // $pdf->AddPage();
        //     // $pdf->Image($imgdata, 20, 13, 14);
        //     // $pdf->writeHTML($data);


        //     // //05. DAFTAR HADIR ----
        //     // echo $this->renderPartial('//surat/kehadiran', [
        //     //     'amiUnit'       => $amiUnit,
        //     //     'jumlahAuditor' => $jumlahAuditor + 1,
        //     //     'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
        //     // ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 10);
        //     // $pdf->AddPage();
        //     // $pdf->Image($imgdata, 20, 13, 14);
        //     // $pdf->writeHTML($data);


        //     // //06. HASIL AMI ----
        //     // echo $this->renderPartial('//surat/cetak_hasil_ami', [
        //     //     'listTemuan'    => $listTemuan,
        //     //     'amiUnit'       => $amiUnit,
        //     //     'listKriteria'  => $listKriteria->all(),
        //     //     'jumlahAuditor' => $jumlahAuditor + 1,
        //     //     'jenjang'       => (isset($jenjang->jenjang->nama) ? $jenjang->jenjang->nama : "Satuan Kerja"),
        //     // ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 9);
        //     // $pdf->AddPage();
        //     // $pdf->Image($imgdata, 20, 13, 14);
        //     // $pdf->writeHTML($data);
        //     // #DATA
        //     // $mulai  = 70;
        //     // $jarak  = 48;
        //     // $tinggi = 73;
        //     // $data = [
        //     //     'ami_unit_id' => $amiUnit->id,
        //     //     'dokumen_id'  => 1,
        //     // ];
        //     // ##BARCODE PENANGGUNGJAWAB
        //     // $pj['is_auditee'] = true;
        //     // $dataPj = $data + $pj;
        //     // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE KETUA AUDITOR
        //     // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
        //     // $kAuditor['is_auditee'] = false;
        //     // $dataKetua = $data + $kAuditor;
        //     // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE ANGGOTA AUDITOR
        //     // if (isset($listAuditor)) {
        //     //     $position = $mulai + $jarak;
        //     //     foreach ($listAuditor as $auditor) {
        //     //         $aAuditor['auditor_id'] = $auditor->auditor_id;
        //     //         $aAuditor['is_auditee'] = false;
        //     //         $dataAuditor = $data + $aAuditor;
        //     //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
        //     //         $position = $position + $jarak;
        //     //     }
        //     // }


        //     // //07. PELUANG PERBAIKAN ----
        //     // echo $this->renderPartial('laporan_peluang_perbaikan', [
        //     //     'amiUnit'       => $amiUnit,
        //     //     'jumlahAuditor' => $jumlahAuditor + 1
        //     // ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 10);
        //     // $pdf->AddPage();
        //     // $pdf->writeHTML($data);


        //     // //08. DESKRIPSI TEMUAN ----
        //     // foreach ($listTemuan as $temuan) {

        //     //     echo $this->renderPartial('//temuan/cetak_temuan', [
        //     //         'listTemuan'    => $listTemuan,
        //     //         'lembar'        => $lembar,
        //     //         'temuan'        => $temuan,
        //     //         'amiUnit'       => $amiUnit,
        //     //         'jumlahAuditor' => $jumlahAuditor + 1
        //     //     ]);

        //     //     $data = ob_get_clean();
        //     //     ob_start();
        //     //     $pdf->AddPage();
        //     //     $pdf->Image($imgdata, 20, 13, 14);
        //     //     $pdf->writeHTML($data);
        //     //     $style = array(
        //     //         'border' => false,
        //     //         'padding' => 0,
        //     //         'fgcolor' => array(0, 0, 0),
        //     //         'bgcolor' => false, //array(255,255,255)
        //     //     );
        //     //     #DATA
        //     //     $mulai  = 70;
        //     //     $jarak  = 48;
        //     //     $tinggi = 105;
        //     //     $data = [
        //     //         'ami_unit_id' => $amiUnit->id,
        //     //         'dokumen_id'  => 2,
        //     //     ];
        //     //     ##BARCODE PENANGGUNGJAWAB
        //     //     $pj['is_auditee'] = true;
        //     //     $dataPj = $data + $pj;
        //     //     if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
        //     //     ##BARCODE KETUA AUDITOR
        //     //     $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
        //     //     $kAuditor['is_auditee'] = false;
        //     //     $dataKetua = $data + $kAuditor;
        //     //     if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
        //     //     ##BARCODE ANGGOTA AUDITOR
        //     //     if (isset($listAuditor)) {
        //     //         $position = $mulai + $jarak;
        //     //         foreach ($listAuditor as $auditor) {
        //     //             $aAuditor['auditor_id'] = $auditor->auditor_id;
        //     //             $aAuditor['is_auditee'] = false;
        //     //             $dataAuditor = $data + $aAuditor;
        //     //             if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
        //     //             $position = $position + $jarak;
        //     //         }
        //     //     }

        //     //     $lembar++;
        //     // }


        //     // //09. REKOMENDASI MUTU ----
        //     // echo $this->renderPartial('laporan_rekomendasi', [
        //     //     'amiUnit'       => $amiUnit,
        //     //     'listKriteria'  => $listKriteria->all(),
        //     //     'jumlahAuditor' => $jumlahAuditor + 2
        //     // ]);

        //     // $data = ob_get_clean();
        //     // ob_start();
        //     // $pdf->SetFont($fontreg, '', 10);
        //     // $pdf->AddPage();
        //     // $pdf->Image($imgdata, 20, 13, 14);
        //     // $pdf->writeHTML($data);
        //     // #DATA
        //     // $mulai  = 70;
        //     // $jarak  = 48;
        //     // $tinggi = 144;
        //     // $data = [
        //     //     'ami_unit_id' => $amiUnit->id,
        //     //     'dokumen_id'  => 3,
        //     // ];
        //     // ##BARCODE PENANGGUNGJAWAB
        //     // $pj['is_auditee'] = true;
        //     // $dataPj = $data + $pj;
        //     // if (MyHelper::printBarcode($dataPj)) $pdf->write2DBarcode(MyHelper::printBarcode($dataPj), 'QRCODE,Q', 23, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE KETUA AUDITOR
        //     // $kAuditor['auditor_id'] = $ketuaAuditor['auditor_id'];
        //     // $kAuditor['is_auditee'] = false;
        //     // $dataKetua = $data + $kAuditor;
        //     // if (MyHelper::printBarcode($dataKetua)) $pdf->write2DBarcode(MyHelper::printBarcode($dataKetua), 'QRCODE,Q', $mulai, $tinggi, 0, 20, $style, 'N');
        //     // ##BARCODE ANGGOTA AUDITOR
        //     // if (isset($listAuditor)) {
        //     //     $position = $mulai + $jarak;
        //     //     foreach ($listAuditor as $auditor) {
        //     //         $aAuditor['auditor_id'] = $auditor->auditor_id;
        //     //         $aAuditor['is_auditee'] = false;
        //     //         $dataAuditor = $data + $aAuditor;
        //     //         if (MyHelper::printBarcode($dataAuditor)) $pdf->write2DBarcode(MyHelper::printBarcode($dataAuditor), 'QRCODE,Q', $position, $tinggi, 0, 20, $style, 'N');
        //     //         $position = $position + $jarak;
        //     //     }
        //     // }


        //     // //09. REKOMENDASI MUTU (ISI) ----
        //     // echo $this->renderPartial('laporan_rekomendasi_isi', [
        //     //     'listKriteria'  => $listKriteria->all(),
        //     // ]);

        //     $data = ob_get_clean();
        //     ob_start();
        //     $pdf->SetFont($fontreg, '', 10);
        //     $pdf->AddPage();
        //     $pdf->writeHTML($data);


        //     $pdf->Output('LaporanAMI_' . $ami_unit->unit->nama . '.pdf');
        // } catch (\Exception $e) {
        //     echo $e;
        //     exit;
        // }
        // die();
    }

    /**
     * Updates an existing MonevUnitKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MonevUnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MonevUnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MonevUnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MonevUnitKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelByUnitKerja($monev_id, $unit_kerja_id = null)
    {
        if (($model = MonevUnitKerja::findOne([
            'monev_id' => $monev_id,
            'unit_kerja_id' => $unit_kerja_id ?? Yii::$app->user->identity->unit_kerja_id
        ])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
