<?php

namespace app\controllers;

use Yii;
use app\models\ProdiJenjang;
use app\models\ProdiJenjangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ProdiJenjangController implements the CRUD actions for ProdiJenjang model.
 */
class ProdiJenjangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProdiJenjang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProdiJenjangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProdiJenjang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProdiJenjang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProdiJenjang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxProdiJenjang()
    {
        $results = [];
        $dataPost = $_POST['dataPost'];
        
        $prodiJenjang = ProdiJenjang::find()->where([
            'prodi_id' => $dataPost['prodi_id']
        ])->one();
        
        #check data
        // echo '<pre>';print_r(!empty($prodiJenjang));die;
        if (!empty($prodiJenjang)) {
            $prodiJenjang->attributes = $dataPost;
            if($prodiJenjang->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'Jenjang Prodi Berhasil di Update'
                ];
            }
        }else {
            $prodiJenjang = new ProdiJenjang();
            $prodiJenjang->attributes = $dataPost;
            if($prodiJenjang->save())
            {
                $results = [
                    'code' => 200,
                    'message' => 'App Role Added'
                ];
            }

            else
            {
                $error = \app\helpers\MyHelper::logError($prodiJenjang);
                $results = [
                    'code' => 500,
                    'message' => $error
                ];
            }
        }

        echo json_encode($results);
        
        exit;
    }

    /**
     * Updates an existing ProdiJenjang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProdiJenjang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProdiJenjang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProdiJenjang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProdiJenjang::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}