<?php

use yii\db\Migration;

/**
 * Class m240129_081627_add_column
 */
class m240129_081627_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('{{%bahan_rtm}}', 'lingkup_bahasan_id', $this->integer());
        $this->addColumn('{{%bahan_rtm}}', 'aspek_id', $this->integer());

        $this->execute("ALTER TABLE `bahan_rtm` MODIFY COLUMN `lingkup_bahasan_id` INTEGER AFTER `rtm_id`");
        $this->execute("ALTER TABLE `bahan_rtm` MODIFY COLUMN `aspek_id` INTEGER AFTER `rtm_id`");


        $this->addForeignKey(
            'fk_bahan_rtm_lingkup_bahasan_id',
            '{{%bahan_rtm}}',
            'lingkup_bahasan_id',
            '{{%lingkup_bahasan}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_bahan_rtm_aspek_id',
            '{{%bahan_rtm}}',
            'aspek_id',
            '{{%aspek}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240129_081627_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240129_081627_add_column cannot be reverted.\n";

        return false;
    }
    */
}
