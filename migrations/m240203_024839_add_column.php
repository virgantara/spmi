<?php

use yii\db\Migration;

/**
 * Class m240203_024839_add_column
 */
class m240203_024839_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%peningkatan}}', 'master_jenis_id', $this->integer());

        $this->execute("ALTER TABLE `peningkatan` MODIFY COLUMN `master_jenis_id` INTEGER AFTER `dokumen_id`");

        $this->addForeignKey(
            'fk-peningkatan-master_jenis_id',
            '{{%peningkatan}}',
            'master_jenis_id',
            '{{%master_jenis}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-peningkatan-master_jenis_id', '{{%peningkatan}}');

        $this->dropColumn('{{%peningkatan}}', 'master_jenis_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240203_024839_add_column cannot be reverted.\n";

        return false;
    }
    */
}
