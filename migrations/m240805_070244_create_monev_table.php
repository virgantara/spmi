<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%monev}}`.
 */
class m240805_070244_create_monev_table extends Migration
{
    public function safeUp()
    {
        // Tabel monev
        $this->createTable('{{%monev}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'nama_monev' => $this->string(300),
            'tahun' => $this->string(5),
            'periode' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Tabel monev_unit_kerja
        $this->createTable('{{%monev_unit_kerja}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'monev_id' => $this->string(36),
            'unit_kerja_id' => $this->integer(),
            'dokumen_id' => $this->integer(),
            'pernyataan_peningkatan' => $this->text(),
            'persetujuan' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Menambahkan foreign key untuk monev_id di tabel monev_unit_kerja
        $this->addForeignKey(
            'fk-monev_unit_kerja-monev_id',
            '{{%monev_unit_kerja}}',
            'monev_id',
            '{{%monev}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-monev_unit_kerja-unit_kerja',
            '{{%monev_unit_kerja}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'CASCADE'
        );


        // Tabel monev_unit_kerja_objek
        $this->createTable('{{%monev_unit_kerja_objek}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'monev_unit_kerja_id' => $this->string(36),
            'jenis_objek' => $this->string(50),
            'import_id' => $this->string(36),
            'kode' => $this->string(200),
            'nama_kode' => $this->string(200),
            'nama_sub_kode' => $this->string(200),
            'nama_program' => $this->text(),
            'sumber_dana' => $this->string(20),
            'biaya' => $this->float(),
            'waktu' => $this->string(50),
            'status_biaya' => $this->string(5),
            'status_waktu' => $this->string(5),
            'status_program' => $this->string(5),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Menambahkan foreign key untuk monev_unit_kerja_id di tabel monev_unit_kerja_objek
        $this->addForeignKey(
            'fk-monev_unit_kerja_objek-monev_unit_kerja_id',
            '{{%monev_unit_kerja_objek}}',
            'monev_unit_kerja_id',
            '{{%monev_unit_kerja}}',
            'id',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        // Menghapus foreign key monev_unit_kerja_objek
        $this->dropForeignKey('fk-monev_unit_kerja_objek-monev_unit_kerja_id', '{{%monev_unit_kerja_objek}}');

        // Menghapus foreign key monev_unit_kerja
        $this->dropForeignKey('fk-monev_unit_kerja-monev_id', '{{%monev_unit_kerja}}');

        // Menghapus tabel
        $this->dropTable('{{%monev_unit_kerja_objek}}');
        $this->dropTable('{{%monev_unit_kerja}}');
        $this->dropTable('{{%monev}}');
    }
}
