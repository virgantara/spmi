<?php

use yii\db\Migration;

/**
 * Class m240107_053437_add_column
 */
class m240107_053437_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ami_unit', 'tanggal_ami', $this->date());

        // Memindahkan kolom 'tanggal_ami' ke posisi sebelum kolom 'created_at' menggunakan SQL
        $this->execute("ALTER TABLE `ami_unit` MODIFY COLUMN `tanggal_ami` DATE AFTER `penanggung_jawab`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ami_unit', 'tanggal_ami');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240107_053437_add_column cannot be reverted.\n";

        return false;
    }
    */
}
