<?php

use yii\db\Migration;

/**
 * Class m240123_040759_change_column
 */
class m240123_040759_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_rtm_periode', '{{%rtm}}');
        $this->dropColumn('{{%rtm}}', 'periode_id'); // Corrected dropColumn usage

        $this->addColumn('{{%rtm}}', 'ami_id', $this->integer());
        $this->addColumn('{{%rtm}}', 'unit_kerja_id', $this->integer());
        $this->renameColumn('{{%rtm}}', 'jenis_rtm', 'tingkatan');
        $this->alterColumn('{{%rtm}}', 'tingkatan', $this->string(50));
        $this->addColumn('{{%rtm}}', 'pengantar', $this->text());

        $this->execute("ALTER TABLE `rtm` MODIFY COLUMN `ami_id` INTEGER AFTER `id`");
        $this->execute("ALTER TABLE `rtm` MODIFY COLUMN `unit_kerja_id` INTEGER AFTER `ami_id`");

        $this->addForeignKey(
            'fk_rtm_ami',
            '{{%rtm}}',
            'ami_id',
            '{{%ami}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_rtm_unit_kerja',
            '{{%rtm}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropColumn('{{%rtm}}', 'tahun');
        $this->dropColumn('{{%rtm}}', 'bidang');

        $this->addColumn('{{%rtm}}', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('{{%rtm}}', 'updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240123_040759_change_column cannot be reverted.\n";

        return false;
    }
    */
}
