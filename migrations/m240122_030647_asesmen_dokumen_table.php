<?php

use yii\db\Migration;

/**
 * Class m240122_030647_asesmen_dokumen_table
 */
class m240122_030647_asesmen_dokumen_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%doc_induk}}');
        $this->dropTable('{{%doc_lkps}}');
        $this->dropTable('{{%jadwalasesmen}}');

        $this->dropForeignKey('kelompok_dokumen_dokumen', '{{%kelompok_dokumen}}');
        $this->dropForeignKey('kelompok_dokumen_pengelompokan', '{{%kelompok_dokumen}}');

        $this->dropTable('{{%kelompok_dokumen}}');
        $this->dropTable('{{%kts}}');
        $this->dropTable('{{%lkps}}');
        $this->dropTable('{{%master_lkps}}');
        $this->dropTable('{{%master_table}}');

        $this->dropForeignKey('pengelompokan_unit_kerja', '{{%pengelompokan}}');
        $this->dropForeignKey('asesmen_pengelompokan', '{{%asesmen}}');

        $this->dropTable('{{%pengelompokan}}');
        $this->dropTable('{{%skor_nilai}}');
        $this->dropTable('{{%table_auth}}');
        $this->dropTable('{{%variable}}');
        $this->dropTable('{{%variable_kategori}}');


        $this->createTable('{{%asesmen_dokumen}}', [
            'id' => $this->primaryKey(),
            'asesmen_id' => $this->integer(),
            'dokumen_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
        $this->addForeignKey('fk-asesmen_dokumen-asesmen_id', '{{%asesmen_dokumen}}', 'asesmen_id', '{{%asesmen}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-asesmen_dokumen-dokumen_id', '{{%asesmen_dokumen}}', 'dokumen_id', '{{%dokumen}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240122_030647_asesmen_dokumen_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240122_030647_asesmen_dokumen_table cannot be reverted.\n";

        return false;
    }
    */
}
