<?php

use yii\db\Migration;

/**
 * Class m231224_162044_change_column
 */
class m231224_162044_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%ami}}', 'nomor_surat_tugas', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231224_162044_change_column cannot be reverted.\n";

        return false;
    }
    */
}
