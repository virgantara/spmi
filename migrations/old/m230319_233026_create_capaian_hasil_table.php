<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%capaian_hasil}}`.
 */
class m230319_233026_create_capaian_hasil_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%capaian_hasil}}', [
            'id' => $this->primaryKey(),
            'unit_kerja_id' => $this->integer(),
            'capaian_kinerja_id' => $this->integer(),
            'periode_id' => $this->integer(),
            'capaian_target' => $this->string(100),
        ]);

        $this->addForeignKey('capaian_hasil_unit_kerja', 'capaian_hasil', 'unit_kerja_id', 'unit_kerja', 'id');
        $this->addForeignKey('capaian_hasil_capaian_kinerja', 'capaian_hasil', 'capaian_kinerja_id', 'capaian_kinerja', 'id');
        $this->addForeignKey('capaian_hasil_periode', 'capaian_hasil', 'periode_id', 'periode', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%capaian_hasil}}');
    }
}
