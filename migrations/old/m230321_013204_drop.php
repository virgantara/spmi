<?php

use yii\db\Migration;

/**
 * Class m230321_013204_drop
 */
class m230321_013204_drop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('capaian_kinerja', 'target_fakultas');
        $this->dropColumn('capaian_kinerja', 'realisasi_kinerja');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230321_013204_drop cannot be reverted.\n";

        return false;
    }
    */
}
