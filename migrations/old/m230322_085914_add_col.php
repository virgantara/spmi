<?php

use yii\db\Migration;

/**
 * Class m230322_085914_add_col
 */
class m230322_085914_add_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('asesmen', 'id_prodi');
        $this->addColumn('asesmen', 'dokumen_id', $this->integer());
        $this->addColumn('asesmen', 'pengelompokan_id', $this->integer());
        $this->addColumn('asesmen', 'jenis_dokumen', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230322_085914_add_col cannot be reverted.\n";

        return false;
    }
    */
}
