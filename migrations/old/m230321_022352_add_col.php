<?php

use yii\db\Migration;

/**
 * Class m230321_022352_add_col
 */
class m230321_022352_add_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('capaian_kinerja', 'status_aktif', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230321_022352_add_col cannot be reverted.\n";

        return false;
    }
    */
}
