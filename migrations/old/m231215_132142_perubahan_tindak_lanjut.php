<?php

use yii\db\Migration;

/**
 * Class m231215_132142_perubahan_tindak_lanjut
 */
class m231215_132142_perubahan_tindak_lanjut extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-audit_tindak_lanjut-temuan_id', '{{%audit_tindak_lanjut}}');
        $this->dropTable('{{%audit_tindak_lanjut}}');
        $this->dropColumn('temuan', 'kriteria');
        $this->dropColumn('temuan', 'indikator');
        $this->dropColumn('temuan', 'temuan');
        $this->dropColumn('temuan', 'auditi');
        $this->dropColumn('temuan', 'kriteria_temuan');
        $this->dropColumn('temuan', 'lokasi');
        $this->dropColumn('temuan', 'ruang_lingkup');
        $this->dropColumn('temuan', 'tanggal_audit');
        $this->dropColumn('temuan', 'wakil_auditi');

        $this->addColumn('temuan', 'rencana_tindak_lanjut', $this->string(500));
        $this->addColumn('temuan', 'jadwal_rtl', $this->string(100));
        $this->addColumn('temuan', 'realisasi_tindak_lanjut', $this->string(500));
        $this->addColumn('temuan', 'efektifitas', $this->string(100));
        $this->addColumn('temuan', 'status', $this->string(50));

        $this->renameColumn('{{%ami}}', 'tanggal_mulai_ami', 'tanggal_ami_mulai');
        $this->renameColumn('{{%ami}}', 'tanggal_selesai_ami', 'tanggal_ami_selesai');
        $this->renameColumn('{{%ami}}', 'tanggal_mulai_visitasi', 'tanggal_visitasi_mulai');
        $this->renameColumn('{{%ami}}', 'tanggal_akhir_visitasi', 'tanggal_visitasi_selesai');
        $this->renameColumn('{{%ami}}', 'tanggal_mulai_penilaian', 'tanggal_penilaian_mulai');
        $this->renameColumn('{{%ami}}', 'tanggal_akhir_penilaian', 'tanggal_penilaian_selesai');

        $this->addColumn('{{%ami}}', 'form_rencana_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_rencana_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_rencana_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_rencana_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_tilik_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_tilik_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_tilik_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_tilik_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_temuan_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_temuan_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_temuan_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_temuan_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_stemuan_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_stemuan_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_stemuan_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_stemuan_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_berita_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_berita_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_berita_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_berita_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_atl_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_atl_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_atl_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_atl_tanggal_efektif', $this->string(20));

        $this->addColumn('{{%ami}}', 'form_kehadiran_kode', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_kehadiran_tanggal_pembuatan', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_kehadiran_tanggal_revisi', $this->string(20));
        $this->addColumn('{{%ami}}', 'form_kehadiran_tanggal_efektif', $this->string(20));

        $this->alterColumn('{{%ami}}', 'keterangan', $this->text());

        $this->dropColumn('bpm', 'surat_tugas');
        $this->dropColumn('bpm', 'berita_acara');
        $this->dropColumn('bpm', 'susunan_acara');
        $this->dropColumn('bpm', 'hasil_audit_fakultas_prodi');
        $this->dropColumn('bpm', 'hasil_audit_satuan_kerja');
        $this->dropColumn('bpm', 'temuan');
        $this->dropColumn('bpm', 'rekomendasi_mutu');
        $this->dropColumn('bpm', 'daftar_hadir_rtm');

        $this->dropColumn('ami', 'tanggal_berlaku');
        $this->dropColumn('ami', 'tanggal_revisi');
        $this->dropColumn('ami', 'no_revisi');
        $this->dropColumn('ami', 'nomor_surat_tugas');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231215_132142_perubahan_tindak_lanjut cannot be reverted.\n";

        return false;
    }
    */
}
