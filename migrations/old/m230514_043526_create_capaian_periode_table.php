<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%capaian_periode}}`.
 */
class m230514_043526_create_capaian_periode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%capaian_periode}}', [
            'id' => $this->primaryKey(),
            'capaian_kinerja_id' => $this->integer(),
            'periode_id' => $this->integer(),
        ]);
        
        $this->addForeignKey('capaianPeriode_capaianKinerja', 'capaian_periode', 'capaian_kinerja_id', 'capaian_kinerja', 'id');
        $this->addForeignKey('capaianPeriode_periode', 'capaian_periode', 'periode_id', 'periode', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%capaian_periode}}');
    }
}
