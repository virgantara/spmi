<?php

use yii\db\Migration;

/**
 * Class m230527_040910_change_bpm_table
 */
class m230527_040910_change_bpm_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('bpm', 'daftar_hadir', 'daftar_hadir_ami');
        $this->addColumn('bpm', 'daftar_hadir_rtm', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230527_040910_change_bpm_table cannot be reverted.\n";

        return false;
    }
    */
}
