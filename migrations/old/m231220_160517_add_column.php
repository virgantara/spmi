<?php

use yii\db\Migration;

/**
 * Class m231220_160517_add_column
 */
class m231220_160517_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('kriteria', 'kode', $this->string(5));
        $this->addColumn('indikator', 'pernyataan', $this->string(500));
        $this->addColumn('indikator', 'indikator', $this->string(500));
        $this->addColumn('indikator', 'strategi', $this->string(500));
        $this->addColumn('indikator', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('indikator', 'updated_at', $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('kriteria', 'kode');
        $this->dropColumn('indikator', 'pernyataan');
        $this->dropColumn('indikator', 'indikator');
        $this->dropColumn('indikator', 'strategi');
        $this->dropColumn('indikator', 'created_at');
        $this->dropColumn('indikator', 'updated_at');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231220_160517_add_column cannot be reverted.\n";

        return false;
    }
    */
}
