<?php

use yii\db\Migration;

/**
 * Class m231224_023346_change_column
 */
class m231224_023346_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->renameColumn('tujuan_audit', 'ruang_lingkup', 'tujuan_audit');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231224_023346_change_column cannot be reverted.\n";

        return false;
    }
    */
}
