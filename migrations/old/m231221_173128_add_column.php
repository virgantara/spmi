<?php

use yii\db\Migration;

/**
 * Class m231221_173128_add_column
 */
class m231221_173128_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('indikator', 'referensi', $this->string(500));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('indikator', 'referensi');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231221_173128_add_column cannot be reverted.\n";

        return false;
    }
    */
}
