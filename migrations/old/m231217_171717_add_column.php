<?php

use yii\db\Migration;

/**
 * Class m231217_171717_add_column
 */
class m231217_171717_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('indikator', 'ami_id', $this->integer());

        $this->addForeignKey(
            'fk-indikator-ami',
            '{{%indikator}}',
            'ami_id',
            '{{%ami}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-indikator-ami', '{{%indikator}}');
        $this->dropColumn('indikator', 'ami_id');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231217_171717_add_column cannot be reverted.\n";

        return false;
    }
    */
}
