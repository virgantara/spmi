<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ruang_lingkup}}`.
 */
class m231216_111351_create_ruang_lingkup_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ruang_lingkup}}', [
            'id' => $this->primaryKey(),
            'ruang_lingkup' => $this->string(250),
            'keterangan' => $this->string(250),
            'status_aktif' => $this->boolean(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%ami_ruang_lingkup}}', [
            'id' => $this->primaryKey(),
            'ruang_lingkup_id' => $this->integer(),
            'ami_unit_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%tujuan_audit}}', [
            'id' => $this->primaryKey(),
            'tujuan_audit' => $this->string(150),
            'ami_unit_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Menambahkan foreign key untuk membuat relasi antara ruang_lingkup_id di ami_ruang_lingkup dengan id di ruang_lingkup
        $this->addForeignKey(
            'fk-ami_ruang_lingkup-ruang_lingkup',
            '{{%ami_ruang_lingkup}}',
            'ruang_lingkup_id',
            '{{%ruang_lingkup}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ami_ruang_lingkup-ami_unit',
            '{{%ami_ruang_lingkup}}',
            'ami_unit_id',
            '{{%ami_unit}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-tujuan_audit-ami_unit',
            '{{%tujuan_audit}}',
            'ami_unit_id',
            '{{%ami_unit}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // Menghapus foreign key terlebih dahulu untuk menghapus relasi sebelum menghapus tabel
        $this->dropForeignKey('fk-tujuan_audit-ami_unit', '{{%tujuan_audit}}');
        $this->dropForeignKey('fk-ami_ruang_lingkup-ami_unit', '{{%ami_ruang_lingkup}}');
        $this->dropForeignKey('fk-ami_ruang_lingkup-ruang_lingkup', '{{%ami_ruang_lingkup}}');

        $this->dropTable('{{%tujuan_audit}}');
        $this->dropTable('{{%ruang_lingkup}}');
        $this->dropTable('{{%ami_ruang_lingkup}}');
    }
}
