<?php

use yii\db\Migration;

/**
 * Class m230516_154530_add_column
 */
class m230516_154530_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('capaian_kinerja', 'ket_jawaban', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230516_154530_add_column cannot be reverted.\n";

        return false;
    }
    */
}
