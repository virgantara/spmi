<?php

use yii\db\Migration;

/**
 * Class m230516_124213_alter_column
 */
class m230516_124213_alter_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {        
        $this->alterColumn('capaian_hasil', 'realisasi_target', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230516_124213_alter_column cannot be reverted.\n";

        return false;
    }
    */
}
