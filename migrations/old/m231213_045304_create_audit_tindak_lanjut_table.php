<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%audit_tindak_lanjut}}`.
 */
class m231213_045304_create_audit_tindak_lanjut_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%audit_tindak_lanjut}}', [
            'id' => $this->primaryKey(),
            'temuan_id' => $this->integer(),
            'rencana_tindak_lanjut' => $this->string(500),
            'jadwal_rtl' => $this->string(100),
            'realisasi_tindak_lanjut' => $this->string(500),
            'efektifitas' => $this->string(255),
            'status' => $this->string(50),
        ]);

        $this->addForeignKey(
            'fk-audit_tindak_lanjut-temuan_id',
            '{{%audit_tindak_lanjut}}',
            'temuan_id',
            '{{%temuan}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-audit_tindak_lanjut-temuan_id', '{{%audit_tindak_lanjut}}');

        $this->dropTable('{{%audit_tindak_lanjut}}');
    }
}
