<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ami_unit}}`.
 */
class m231227_184417_create_ami_unit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Drop foreign keys
        $this->dropForeignKey('FK_ami_auditor_ami', '{{%ami_auditor}}');
        $this->dropForeignKey('fk-ami_ruang_lingkup-ami_unit', '{{%ami_tujuan_audit}}');

        $this->dropTable('{{%ami_unit_auditor}}');

        $this->dropForeignKey('asesmen_ibfk_1', '{{%asesmen}}');
        $this->dropForeignKey('fk_jadwal_amiunit', '{{%jadwal}}');
        $this->dropForeignKey('fk_persetujuan_amiunit', '{{%persetujuan}}');
        $this->dropForeignKey('fk-tujuan_audit-ami_unit', '{{%ruang_lingkup}}');
        $this->dropForeignKey('fk_temuan_amiunit', '{{%temuan}}');
        $this->dropForeignKey('fk-ami_laporan-ami_unit', '{{%ami_laporan}}');

        // Change id column to primary key
        $this->alterColumn('{{%ami_unit}}', 'id', $this->primaryKey());

        $this->addForeignKey('FK_ami_auditor_ami', '{{%ami_auditor}}', 'ami_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk-ami_ruang_lingkup-ami_unit', '{{%ami_tujuan_audit}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('asesmen_ibfk_1', '{{%asesmen}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk_jadwal_amiunit', '{{%jadwal}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk_persetujuan_amiunit', '{{%persetujuan}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk-tujuan_audit-ami_unit', '{{%ruang_lingkup}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk_temuan_amiunit', '{{%temuan}}', 'ami_unit_id', '{{%ami_unit}}', 'id');
        $this->addForeignKey('fk-ami_laporan-ami_unit', '{{%ami_laporan}}', 'ami_unit_id', '{{%ami_unit}}', 'id');

        $this->dropColumn('{{%ami_unit}}', 'dokumen_tilik');
        $this->dropColumn('{{%ami_unit}}', 'dokumen_temuan');
        $this->dropColumn('{{%ami_unit}}', 'dokumen_berita_acara');

        $this->addColumn('{{%ami_unit}}', 'penanggung_jawab', $this->string(150));
        $this->addColumn('{{%ami_unit}}', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('{{%ami_unit}}', 'updated_at', $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP'));

        $this->addColumn('{{%ami}}', 'path_cover', $this->string(500));
    }
}
