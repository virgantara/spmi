<?php

use yii\db\Migration;

/**
 * Class m240102_062300_change_column
 */
class m240102_062300_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%persetujuan_cek}}', 'acara', 'unit_kerja_id');
        $this->alterColumn('{{%persetujuan_cek}}', 'unit_kerja_id', $this->integer());
        
        $this->addForeignKey('fk-persetujuan_cek-unit_kerja', '{{%persetujuan_cek}}', 'unit_kerja_id', '{{%unit_kerja}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240102_062300_change_column cannot be reverted.\n";

        return false;
    }
    */
}
