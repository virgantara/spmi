<?php

use yii\db\Migration;

/**
 * Class m230320_043754_ubah_column
 */
class m230320_043754_ubah_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('periode', 'periode', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230320_043754_ubah_column cannot be reverted.\n";

        return false;
    }
    */
}
