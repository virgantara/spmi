<?php

use yii\db\Migration;

/**
 * Class m230320_083338_add_column
 */
class m230320_083338_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('periode', 'status_aktif', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230320_083338_add_column cannot be reverted.\n";

        return false;
    }
    */
}
