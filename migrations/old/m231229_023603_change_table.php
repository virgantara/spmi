<?php

use yii\db\Migration;

/**
 * Class m231229_023603_change_table
 */
class m231229_023603_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%indikator}}', 'nama', $this->text());
        $this->alterColumn('{{%indikator}}', 'pernyataan', $this->text());
        $this->alterColumn('{{%indikator}}', 'indikator', $this->text());
        $this->alterColumn('{{%indikator}}', 'strategi', $this->text());
        $this->alterColumn('{{%indikator}}', 'referensi', $this->text());
        $this->alterColumn('{{%indikator}}', 'skor1', $this->text());
        $this->alterColumn('{{%indikator}}', 'skor2', $this->text());
        $this->alterColumn('{{%indikator}}', 'skor3', $this->text());
        $this->alterColumn('{{%indikator}}', 'skor4', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m231229_023603_change_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231229_023603_change_table cannot be reverted.\n";

        return false;
    }
    */
}
