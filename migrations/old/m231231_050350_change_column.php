<?php

use yii\db\Migration;

/**
 * Class m231231_050350_change_column
 */
class m231231_050350_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%persetujuan}}', 'dokumen_id');
        $this->addColumn('{{%persetujuan}}', 'nama_dokumen', $this->string(50));
        $this->addColumn('{{%persetujuan}}', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('{{%persetujuan}}', 'updated_at', $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231231_050350_change_column cannot be reverted.\n";

        return false;
    }
    */
}
