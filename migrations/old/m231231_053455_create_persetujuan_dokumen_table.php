<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%persetujuan_dokumen}}`.
 */
class m231231_053455_create_persetujuan_dokumen_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%persetujuan_cek}}', [
            'id' => $this->primaryKey(),
            'kode_dokumen' => $this->string(100),
            'nama_kegiatan' => $this->string(100),
            'nama_dokumen' => $this->string(100),
            'acara' => $this->string(100),
            'tahun' => $this->string(100),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%persetujuan_cek}}');
    }
}
