<?php

use yii\db\Migration;

/**
 * Class m230321_013535_add_coll
 */
class m230321_013535_add_coll extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('capaian_kinerja', 'bidang_wr', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230321_013535_add_coll cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230321_013535_add_coll cannot be reverted.\n";

        return false;
    }
    */
}
