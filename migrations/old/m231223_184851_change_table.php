<?php

use yii\db\Migration;

/**
 * Class m231223_184851_change_table
 */
class m231223_184851_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ami', 'nomor_surat_tugas', $this->string(20));
        $this->addColumn('ami', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('ami', 'updated_at', $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP'));

        $this->dropForeignKey('fk-ami_ruang_lingkup-ruang_lingkup', 'ami_ruang_lingkup');

        $this->renameTable('ami_ruang_lingkup', 'ami_tujuan_audit');
        $this->renameTable('tujuan_audit', 'ruang_lingkupp');
        $this->renameTable('ruang_lingkup', 'tujuan_audit');
        $this->renameTable('ruang_lingkupp', 'ruang_lingkup');

        $this->renameColumn('ami_tujuan_audit', 'ruang_lingkup_id', 'tujuan_audit_id');
        $this->addForeignKey('fk-ami_ami_tujuan_audit-tujuan_audit', 'ami_tujuan_audit', 'tujuan_audit_id', 'tujuan_audit', 'id');
        
        $this->renameColumn('ruang_lingkup', 'tujuan_audit', 'ruang_lingkup');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231223_184851_change_table cannot be reverted.\n";

        return false;
    }
    */
}
