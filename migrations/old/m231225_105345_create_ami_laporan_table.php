<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ami_laporan}}`.
 */
class m231225_105345_create_ami_laporan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ami_laporan}}', [
            'id' => $this->primaryKey(),
            'ami_id' => $this->integer(),
            'ami_unit_id' => $this->integer(),
            'urutan' => $this->integer(),
            'path_gambar' => $this->string(500),
            'keterangan' => $this->string(500),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);

        $this->addForeignKey(
            'fk-ami_laporan-ami',
            '{{%ami_laporan}}',
            'ami_id',
            '{{%ami}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ami_laporan-ami_unit',
            '{{%ami_laporan}}',
            'ami_unit_id',
            '{{%ami_unit}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ami_laporan-ami_unit', '{{%ami_laporan}}');
        $this->dropForeignKey('fk-ami_laporan-ami', '{{%ami_laporan}}');
        $this->dropTable('{{%ami_laporan}}');
    }
}
