<?php

use yii\db\Migration;

/**
 * Class m231231_165939_change_column
 */
class m231231_165939_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('persetujuan', 'nama_dokumen', 'kode_dokumen');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231231_165939_change_column cannot be reverted.\n";

        return false;
    }
    */
}
