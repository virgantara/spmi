<?php

use yii\db\Migration;

/**
 * Class m231129_034433_add_column
 */
class m231129_034433_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $this->addColumn('temuan', 'indikator_id', $this->integer());
        $this->addColumn('temuan', 'rencana_perbaikan', $this->string(500));
        // $this->addForeignKey('temuan_indikator', 'temuan', 'indikator_id', 'indikator', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropForeignKey('temuan_indikator', 'temuan');
        // $this->dropColumn('temuan', 'indikator_id');
        $this->dropColumn('temuan', 'rencana_perbaikan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231129_034433_add_column cannot be reverted.\n";

        return false;
    }
    */
}
