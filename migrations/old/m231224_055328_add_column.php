<?php

use yii\db\Migration;

/**
 * Class m231224_055328_add_column
 */
class m231224_055328_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('ami', 'ketua_bpm', $this->string(500));
        $this->addColumn('ami', 'nidn_ketua_bpm', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231224_055328_add_column cannot be reverted.\n";

        return false;
    }
    */
}
