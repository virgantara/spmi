<?php

use yii\db\Migration;

/**
 * Class m231212_212523_add_column
 */
class m231212_212523_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tilik', 'referensi', $this->string(255));
        $this->addColumn('tilik', 'bukti', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m231212_212523_add_column cannot be reverted.\n";

        return false;
    }
    */
}
