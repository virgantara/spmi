<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%capaian_kinerja}}`.
 */
class m230319_233019_create_capaian_kinerja_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%capaian_kinerja}}', [
            'id' => $this->primaryKey(),
            'sasaran_stategis' => $this->string(500),
            'no_indikator' => $this->integer(),
            'indikator_kinerja' => $this->string(500),
            'satuan' => $this->string(100),
            'target_unida' => $this->string(100),
            'target_fakultas' => $this->string(100),
            'realisasi_kinerja' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%capaian_kinerja}}');
    }
}
