<?php

use yii\db\Migration;

/**
 * Class m230322_090230_add_rell
 */
class m230322_090230_add_rell extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('asesmen_pengelompokan', 'asesmen', 'pengelompokan_id', 'pengelompokan', 'id');
        $this->addForeignKey('asesmen_dokumen', 'asesmen', 'dokumen_id', 'dokumen', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230322_090230_add_rell cannot be reverted.\n";

        return false;
    }
    */
}
