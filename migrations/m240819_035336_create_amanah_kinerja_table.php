<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%amanah_kinerja}}`.
 */
class m240819_035336_create_amanah_kinerja_table extends Migration
{
    public function safeUp()
    {
        // Membuat tabel amanah_kinerja_master
        $this->createTable('{{%amanah_kinerja_master}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'unit_kerja_id' => $this->integer(),
            'jenis_amanah_kinerja' => $this->string(50),
            'amanah_kinerja' => $this->text(),
            'target_umum' => $this->text(),
            'status_aktif' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%amanah_kinerja}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'nama' => $this->string(500),
            'tahun' => $this->string(5),
            'status_aktif' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%amanah_kinerja_unit_kerja}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'amanah_kinerja_id' => $this->char(36), // Sesuaikan dengan CHAR(36)
            'amanah_kinerja_master_id' => $this->char(36), // Sesuaikan dengan CHAR(36)
            'unit_kerja_id' => $this->integer(),
            'target' => $this->string(250),
            'capaian' => $this->string(250),
            'status' => $this->string(50),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Menambahkan foreign key
        $this->addForeignKey(
            'fk-amanah_kinerja_unit_kerja-amanah_kinerja_id',
            '{{%amanah_kinerja_unit_kerja}}',
            'amanah_kinerja_id',
            '{{%amanah_kinerja}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-amanah_kinerja_unit_kerja-amanah_kinerja_master_id',
            '{{%amanah_kinerja_unit_kerja}}',
            'amanah_kinerja_master_id',
            '{{%amanah_kinerja_master}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-amanah_kinerja_master-unit_kerja_id',
            '{{%amanah_kinerja_master}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-amanah_kinerja_unit_kerja-unit_kerja_id',
            '{{%amanah_kinerja_unit_kerja}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        // Urutan penghapusan foreign key
        $this->dropForeignKey(
            'fk-amanah_kinerja_unit_kerja-unit_kerja_id',
            '{{%amanah_kinerja_unit_kerja}}'
        );

        $this->dropForeignKey(
            'fk-amanah_kinerja_master-unit_kerja_id',
            '{{%amanah_kinerja_master}}'
        );

        $this->dropForeignKey(
            'fk-amanah_kinerja_unit_kerja-amanah_kinerja_master_id',
            '{{%amanah_kinerja_unit_kerja}}'
        );

        $this->dropForeignKey(
            'fk-amanah_kinerja_unit_kerja-amanah_kinerja_id',
            '{{%amanah_kinerja_unit_kerja}}'
        );

        // Menghapus tabel
        $this->dropTable('{{%amanah_kinerja}}');
        $this->dropTable('{{%amanah_kinerja_unit_kerja}}');
        $this->dropTable('{{%amanah_kinerja_master}}');
    }
}
