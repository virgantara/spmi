<?php

use yii\db\Migration;

/**
 * Class m240831_090011_add_column
 */
class m240831_090011_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'evaluasi', $this->string(1000));
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'perbaikan', $this->string(1000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'evaluasi');
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'perbaikan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240831_090011_add_column cannot be reverted.\n";

        return false;
    }
    */
}
