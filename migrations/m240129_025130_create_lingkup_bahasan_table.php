<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lingkup_bahasan}}`.
 */
class m240129_025130_create_lingkup_bahasan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lingkup_bahasan}}', [
            'id' => $this->primaryKey(),
            'lingkup_pembahasan' => $this->string(150),
            'status_aktif' => $this->boolean(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%aspek}}', [
            'id' => $this->primaryKey(),
            'aspek' => $this->string(150),
            'status_aktif' => $this->boolean(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->addColumn('{{%bahan_rtm}}', 'persetujuan', $this->integer());
        $this->execute("ALTER TABLE `bahan_rtm` MODIFY COLUMN `persetujuan` INTEGER AFTER `aras`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lingkup_bahasan}}');
    }
}
