<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%peningkatan}}`.
 */
class m240131_035951_create_peningkatan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%peningkatan}}', [
            'id' => $this->primaryKey(),
            'nama_dokumen' => $this->text(),
            'indikator_id' => $this->integer(),
            'unit_kerja_id' => $this->integer(),
            'dokumen_id' => $this->integer(),
            'pernyataan_peningkatan' => $this->text(),
            'persetujuan' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->createTable('{{%peningkatan_riwayat}}', [
            'id' => $this->primaryKey(),
            'peningkatan_id' => $this->integer(),
            'nama_dokumen' => $this->string(150),
            'indikator_id' => $this->integer(),
            'unit_kerja_id' => $this->integer(),
            'dokumen_id' => $this->integer(),
            'pernyataan_peningkatan' => $this->text(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);
        
        $this->addForeignKey(
            'fk-peningkatan-indikator_id',
            '{{%peningkatan}}',
            'indikator_id',
            '{{%indikator}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-peningkatan-unit_kerja_id',
            '{{%peningkatan}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-peningkatan-dokumen_id',
            '{{%peningkatan}}',
            'dokumen_id',
            '{{%dokumen}}',
            'id',
            'CASCADE',
            'CASCADE'
        );



        $this->addForeignKey(
            'fk-peningkatan_riwayat-peningkatan_id',
            '{{%peningkatan_riwayat}}',
            'peningkatan_id',
            '{{%peningkatan}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-peningkatan_riwayat-indikator_id',
            '{{%peningkatan_riwayat}}',
            'indikator_id',
            '{{%indikator}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-peningkatan_riwayat-unit_kerja_id',
            '{{%peningkatan_riwayat}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-peningkatan_riwayat-dokumen_id',
            '{{%peningkatan_riwayat}}',
            'dokumen_id',
            '{{%dokumen}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-peningkatan-indikator_id', '{{%peningkatan}}');
        $this->dropForeignKey('fk-peningkatan-unit_kerja_id', '{{%peningkatan}}');
        $this->dropForeignKey('fk-peningkatan-dokumen_id', '{{%peningkatan}}');

        $this->dropForeignKey('fk-peningkatan_riwayat-peningkatan_id', '{{%peningkatan_riwayat}}');
        $this->dropForeignKey('fk-peningkatan_riwayat-indikator_id', '{{%peningkatan_riwayat}}');
        $this->dropForeignKey('fk-peningkatan_riwayat-unit_kerja_id', '{{%peningkatan_riwayat}}');
        $this->dropForeignKey('fk-peningkatan_riwayat-dokumen_id', '{{%peningkatan_riwayat}}');

        $this->dropTable('{{%peningkatan}}');
        $this->dropTable('{{%peningkatan_riwayat}}');
    }
}
