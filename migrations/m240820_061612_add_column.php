<?php

use yii\db\Migration;

/**
 * Class m240820_061612_add_column
 */
class m240820_061612_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'target', $this->text());
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'capaian', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'target');
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'capaian');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240820_061612_add_column cannot be reverted.\n";

        return false;
    }
    */
}
