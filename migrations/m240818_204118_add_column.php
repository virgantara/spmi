<?php

use yii\db\Migration;

/**
 * Class m240818_204118_add_column
 */
class m240818_204118_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'ketidaksesuaian', $this->text());
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'referensi', $this->text());
        $this->addColumn('{{%monev_unit_kerja_objek}}', 'rencana_tindak_lanjut', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'ketidaksesuaian');
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'referensi');
        $this->dropColumn('{{%monev_unit_kerja_objek}}', 'rencana_tindak_lanjut');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240818_204118_add_column cannot be reverted.\n";

        return false;
    }
    */
}
