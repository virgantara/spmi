<?php

use yii\db\Migration;

/**
 * Class m240114_181735_add_column
 */
class m240114_181735_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('kriteria', 'jenis_standar', $this->string(50));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('kriteria', 'jenis_standar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240114_181735_add_column cannot be reverted.\n";

        return false;
    }
    */
}
