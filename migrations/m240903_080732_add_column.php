<?php

use yii\db\Migration;

/**
 * Class m240903_080732_add_column
 */
class m240903_080732_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%akreditasi}}', 'urutan', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%akreditasi}}', 'urutan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240903_080732_add_column cannot be reverted.\n";

        return false;
    }
    */
}
