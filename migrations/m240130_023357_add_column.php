<?php

use yii\db\Migration;

/**
 * Class m240130_023357_add_column
 */
class m240130_023357_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%master_jenis}}', 'set_to', $this->string(50));
        $this->addColumn('{{%master_jenis}}', 'status_aktif', $this->integer());

        $this->addColumn('{{%master_jenis}}', 'created_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->addColumn('{{%master_jenis}}', 'updated_at', $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240130_023357_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240130_023357_add_column cannot be reverted.\n";

        return false;
    }
    */
}
