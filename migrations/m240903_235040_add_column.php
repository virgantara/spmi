<?php

use yii\db\Migration;

/**
 * Class m240903_235040_add_column
 */
class m240903_235040_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%unit_kerja}}', 'kode_unit', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%unit_kerja}}', 'kode_unit');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240903_235040_add_column cannot be reverted.\n";

        return false;
    }
    */
}
