<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%monev_bukti}}`.
 */
class m240818_142600_create_monev_bukti_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%monev_bukti}}', [
            'id' => 'CHAR(36) NOT NULL PRIMARY KEY',
            'monev_unit_kerja_id' => $this->string(36),
            'monev_unit_kerja_objek_id' => $this->string(36),
            'dokumen_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey(
            'fk_monev_bukti_unit_kerja',
            '{{%monev_bukti}}',
            'monev_unit_kerja_id',
            '{{%monev_unit_kerja}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_monev_bukti_unit_kerja_objek',
            '{{%monev_bukti}}',
            'monev_unit_kerja_objek_id',
            '{{%monev_unit_kerja_objek}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_monev_bukti_dokumen',
            '{{%monev_bukti}}',
            'dokumen_id',
            '{{%dokumen}}',
            'id',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_monev_bukti_unit_kerja',
            '{{%monev_bukti}}'
        );

        $this->dropForeignKey(
            'fk_monev_bukti_unit_kerja_objek',
            '{{%monev_bukti}}'
        );

        $this->dropForeignKey(
            'fk_monev_bukti_dokumen',
            '{{%monev_bukti}}'
        );

        // Hapus tabel
        $this->dropTable('{{%monev_bukti}}');
    }
}
