<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\assets\DashboardAsset;
use app\assets\HighchartAsset;
use app\models\ErpKamar;
use app\models\SimakPilihan;
use app\models\SimakMasterprogramstudi;

DashboardAsset::register($this);
HighchartAsset::register($this);

$this->title = Yii::t('app', Yii::$app->name);


?>
<style type="text/css">

.lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 8px;
  border-radius: 50%;
  border: 2px solid #000;
  border-color: #000 transparent #000 transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>
<h3 class="page-title">Selamat Datang di Sistem Informasi Akademik (SIAKAD) Universitas Darussalam Gontor</h3>

<?php


if(Yii::$app->user->can('baak') || Yii::$app->user->can('sekretearis') || Yii::$app->user->can('akpam') || Yii::$app->user->can('asesor') || Yii::$app->user->can('warek1'))
{
 ?>
 <p>
<?php
if (Yii::$app->user->can('warek1'))
{
  echo Html::a('<i class="lnr lnr-apartment"></i> Display Dashboard Akademik',['site/index'],['class'=>'btn btn-primary btn-lg']);

  echo '&nbsp;';
  echo Html::a('<i class="lnr lnr-book"></i> Display Dashboard Tahfidz',['site/tahfidz'],['class'=>'btn btn-primary btn-lg']);
}
?>
</p>
<div class="row">
  <div class="col-md-4">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekapitulasi Kelulusan AKPAM</h1>
        <?=Html::dropDownList('tahun_akpam',null,ArrayHelper::map($list_tahun,'tahun_id','nama_tahun'),['id'=>'tahun_akpam','prompt'=>'- Semua -']);?>
        
        <!-- <div id="loading_ipk" style="display: none">Loading...</div> -->
      </div>
      <div class="panel-body">
        <div class="lds-dual-ring" id="loading_akpam_lulus" style="display: none"></div>
        <div class="chart-container">
          <div id="container-akpam-lulus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-4">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekapitulasi Kelulusan AKPAM Per Fakultas</h1>
      </div>
      <div class="panel-body">
        <div class="chart-container">
          <div id="container-akpam-lulus-fakultas" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekapitulasi Kelulusan AKPAM Per Prodi</h1>
      </div>
      <div class="panel-body">
        <div class="chart-container">
          <div id="container-akpam-lulus-prodi" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekap Penginputan AKPAM</h1>
        *NB: Rekap ini hanya menghitung yang sudah input AKPAM dan tidak menghitung jumlah kelulusan
      </div>
      <div class="panel-body">
        <p>
          <?=Html::dropDownList('tahun_akademik','',ArrayHelper::map($list_tahun,'tahun_id','nama_tahun'),['id'=>'tahun_akademik','prompt'=>'- Pilih Tahun Akademik -']);?>
          <button type="button" class="btn btn-primary" id="btnShow"><i class="fa fa-search"></i> Tampilkan Data</button>
          <span id="loading" style="display: none"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Loading. . .</span>
        </p>
        <div class="table-responsive">
         
            <table class="table table-bordered table-hover" id="tabel_rekap">
              <thead>
              <tr>
                <th rowspan="3">Fakultas</th>
                <th rowspan="3">Prodi</th>
                <th colspan="<?=count($list_kampus) * 3;?>" class="text-center">Kelas</th>
              </tr>
              <tr>
                <?php 
                foreach($list_kampus as $k)
                {
                ?>
                <th colspan="3" class="text-center"><?=$k->nama_kampus;?></th>
                <?php 
                }
                ?>
              </tr>
              <tr>
                <?php 
                foreach($list_kampus as $k)
                {
                ?>
                <th class="text-center" title="Sudah Input AKPAM" width="5%">S</th>
                <th class="text-center" title="Belum Input AKPAM"  width="5%">B</th>
                <th class="text-center" title="Total MHS"  width="5%">T</th>
                <?php 
                }
                ?>
                
                
              </tr>
               
            </thead>
            <tbody>
             
              
            </tbody>
            <tfoot>
              <tr>
                <td colspan="26">
                  NB: 
                  <ul>
                    <li>S: Jumlah Mahasiswa Sudah Input</li>
                    <li>B: Jumlah Mahasiswa Belum Input</li>
                    <li>T: Total Mahasiswa</li>
                  </ul>
                </td>
              </tr>
            </tfoot>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>


<?php
}

$jsScript = '


var results = [];


$("#btnShow").click(function(){

  getMahasiswa($("#tahun_akademik").val());
});

$("#tahun_akpam").change(function(){

  getKelulusanAkpam($(this).val());
  getKelulusanAkpamFakultas($(this).val())
});

 function getRandColor(same, darkness) {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }

function getKelulusanAkpamProdi(tahun_akademik, fakultas){
    
    var obj = new Object;
    obj.tahun_akademik = tahun_akademik;
    obj.fakultas = fakultas
   
    $.ajax({

        type : "POST",
        url : "'.Url::to(['/simak-mastermahasiswa/ajax-rekap-kelulusan-akpam-prodi']).'",
        data : {
          dataPost : obj
        },
        async : true,
        error : function(e){
          $("#loading_akpam_lulus").hide();
        },
        beforeSend : function(){
          $("#loading_akpam_lulus").show(); 

        },
        success: function(hasil){
          var hasil = $.parseJSON(hasil);
          $("#loading_akpam_lulus").hide();
                
          var kategori = [];

          var chartData = [];
          var chartDataBelum = [];

          $.each(hasil,function(i,obj){
            kategori.push(obj.prodi)
            
            chartData.push(obj.count_lulus);
            chartDataBelum.push(obj.count_belum);
          });

          $("#container-akpam-lulus-prodi").highcharts({
            chart: {
              type: "column"
          },
            title: {
                text: "Persentase Kelulusan AKPAM Per Prodi"
            },

            xAxis: {
              categories: kategori,
              crosshair: true
            },
            yAxis: {
                title: {
                    text: "Jumlah"
                },

                startOnTick: false,
                endOnTick: false
            },
            tooltip: {
                headerFormat: "<span style=\"font-size:10px\">{point.key}</span><table>",
                pointFormat: "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
                    "<td style=\"padding:0\"><b>{point.y}</b></td></tr>",
                footerFormat: "</table>",
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                },
                series: {
                  cursor: \'pointer\',
                  point: {
                      events: {
                          click: function (tes) {
                            
                          }
                      }
                  }
              }
                
              
            },

            series: [
            {
                  name: "Jumlah Mahasiswa Lulus AKPAM",
                  data: chartData,
                  color: "rgb(0,200,0)"
            },
            {
                  name: "Jumlah Mahasiswa Belum Lulus AKPAM",
                  data: chartDataBelum,
                  color: "rgb(200,0,0)"
            },
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: "horizontal",
                            align: "center",
                            verticalAlign: "bottom"
                        }
                    }
                }]
            }
          });
        }
    });
}

function getKelulusanAkpamFakultas(tahun_akademik){
    
    var obj = new Object;
    obj.tahun_akademik = tahun_akademik;
   
    $.ajax({

        type : "POST",
        url : "'.Url::to(['/simak-mastermahasiswa/ajax-rekap-kelulusan-akpam-fakultas']).'",
        data : {
          dataPost : obj
        },
        async : true,
        error : function(e){
          $("#loading_akpam_lulus").hide();
        },
        beforeSend : function(){
          $("#loading_akpam_lulus").show(); 

        },
        success: function(hasil){
          var hasil = $.parseJSON(hasil);
          $("#loading_akpam_lulus").hide();
                
          var kategori = [];

          var chartData = [];
          var chartDataBelum = [];

          $.each(hasil,function(i,obj){
            kategori.push(obj.fakultas)
            
            chartData.push(obj.count_lulus);
            chartDataBelum.push(obj.count_belum);
          });

          $("#container-akpam-lulus-fakultas").highcharts({
            chart: {
              type: "column"
          },
            title: {
                text: "Persentase Kelulusan AKPAM Per Fakultas"
            },

            xAxis: {
              categories: kategori,
              crosshair: true
            },
            yAxis: {
                title: {
                    text: "Jumlah"
                },

                startOnTick: false,
                endOnTick: false
            },
            tooltip: {
                headerFormat: "<span style=\"font-size:10px\">{point.key}</span><table>",
                pointFormat: "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
                    "<td style=\"padding:0\"><b>{point.y}</b></td></tr>",
                footerFormat: "</table>",
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                },
                series: {
                  cursor: \'pointer\',
                  point: {
                      events: {
                          click: function (tes) {
                            getKelulusanAkpamProdi($("#tahun_akpam").val(),tes.point.category)
                          }
                      }
                  }
              }
                
              
            },

            series: [
            {
                  name: "Jumlah Mahasiswa Lulus AKPAM",
                  data: chartData,
                  color: "rgb(0,200,0)"
            },
            {
                  name: "Jumlah Mahasiswa Belum Lulus AKPAM",
                  data: chartDataBelum,
                  color: "rgb(200,0,0)"
            },
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: "horizontal",
                            align: "center",
                            verticalAlign: "bottom"
                        }
                    }
                }]
            }
          });
        }
    });
}

function getKelulusanAkpam(tahun_akademik){
    
    var obj = new Object;
    obj.tahun_akademik = tahun_akademik;
   
    $.ajax({

        type : "POST",
        url : "'.Url::to(['/simak-mastermahasiswa/ajax-rekap-kelulusan-akpam']).'",
        data : {
          dataPost : obj
        },
        async : true,
        error : function(e){
          $("#loading_akpam_lulus").hide();
        },
        beforeSend : function(){
          $("#loading_akpam_lulus").show(); 

        },
        success: function(hasil){
          var hasil = $.parseJSON(hasil);
          $("#loading_akpam_lulus").hide();
                     var kategori = ["LULUS","BELUM"];

          var chartData = [
            {
              y:hasil.count_lulus,
              name:"Lulus"
            },
            {
              y:hasil.count_belum,
              name:"Belum Lulus"
            }
          ];


          $("#container-akpam-lulus").highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: "pie"
            },
            title: {
                text: "Persentase Kelulusan AKPAM UNIDA Gontor"
            },

            xAxis: {
              categories: kategori,
              crosshair: true
            },
            yAxis: {
                title: {
                    text: "Jumlah"
                },
                min: 0,
                max: 4,
                startOnTick: false,
                endOnTick: false
            },
            tooltip: {
                headerFormat: "<span style=\"font-size:10px\">{point.key}</span><table>",
                pointFormat: "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
                    "<td style=\"padding:0\"><b>{point.y}</b></td></tr>",
                footerFormat: "</table>",
                shared: true,
                useHTML: true
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: true,
                        format: "<b>{point.name}</b>: {point.percentage:.1f} %"
                    }
                }
                
                
              
            },

            series: [{
                name: "Data Kelulusan AKPAM ",
                data: chartData,
                colorByPoint: true,
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: "horizontal",
                            align: "center",
                            verticalAlign: "bottom"
                        }
                    }
                }]
            }
          });
        }
    });
}


function getMahasiswa(tahun_akademik){
    
    var obj = new Object;
    obj.tahun_akademik = tahun_akademik;
   
    $.ajax({

        type : "POST",
        url : "'.Url::to(['/simak-mastermahasiswa/ajax-rekap-akpam-mahasiswa']).'",
        data : {
          dataPost : obj
        },
        async : true,
        error : function(e){
          $("#loading").hide();
        },
        beforeSend : function(){
          $("#loading").show(); 

        },
        success: function(hasil){
          var hasil = $.parseJSON(hasil);
          $("#loading").hide();
          $("#tabel_rekap > tbody").empty();
          var row = "";
          ';

          $labels = [];
          foreach($fakultas as $q => $f)
          {
            foreach($f->simakMasterprogramstudis as $p)
            {

              
              if(!in_array($f->nama_fakultas, $labels))
              {
                $labels[] = $f->nama_fakultas;
                $jsScript .= '
                  row += "<tr>"
                  row += "<td>'.$f->nama_fakultas.'</td>"
                  row += "<td>'.$p->singkatan.'</td>"
                ';
              }
              else{
                $jsScript .= '
                  row += "<tr>"
                  row += "<td></td>"
                  row += "<td>'.$p->singkatan.'</td>"
                ';
              }
              $jsScript .= PHP_EOL;
              foreach($list_kampus as $k)
              {
              
                
                $jsScript .= 'var count = hasil['.$f->kode_fakultas.']['.$p->kode_prodi.']['.$k->kode_kampus.']; ';
                $jsScript .= 'var belum = count[\'count_belum\'];';
                $jsScript .= 'var sudah = count[\'count_sudah\'];';
                $jsScript .= 'var total = count[\'count_total\'];';
                $jsScript .= PHP_EOL;
                $jsScript .= 'var linkBelum = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-akpam']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'&is_status=NOT">\'+belum+\'</a>\' ';
                $jsScript .= PHP_EOL;
                $jsScript .= 'var linkSudah = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-akpam']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'&is_status=IN">\'+sudah+\'</a>\' ';
                  $jsScript .= PHP_EOL;
                  $jsScript .= 'var linkTotal = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-akpam']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'">\'+total+\'</a>\' ';
                $jsScript .= PHP_EOL;
                $jsScript .= '
                  var label = belum > 0 ? \'style="color:red"\' : \'\';
                  row += "<td class=\'text-center\'>"+linkSudah+"</td>"
                  row += "<td "+label+" class=\'text-center\'>"+linkBelum+"</td>"
                  row += "<td class=\'text-center\'>"+linkTotal+"</td>"
                ';
                $jsScript .= PHP_EOL;
                
              }

              $jsScript .= '
                row += "</tr>"
              ';
              $jsScript .= PHP_EOL;
               
            }
          }
          $jsScript .= '

          $("#tabel_rekap > tbody").append(row);

        }
    });
}


';


$this->registerJs($jsScript, \yii\web\View::POS_READY);

?>
