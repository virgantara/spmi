<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .vertical-align-wrap {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
        width: 100%;
    }

    .auth-box {
        display: flex;
        max-width: 900px;
        background: #fff;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        border-radius: 8px;
        overflow: hidden;
    }

    .auth-box .left,
    .auth-box .right {
        width: 50%;
        padding: 30px;
    }

    .auth-box .left {
        background-color: #fff;
    }

    .auth-box .right {
        background: linear-gradient(to right, #3b5998, #192f6a);
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }

    .auth-box .right .content {
        text-align: center;
    }

    .auth-box .header .logo img {
        width: 100px;
        margin-bottom: 20px;
    }

    .auth-box .content .form-auth-small {
        margin-top: 20px;
    }

    .auth-box .content .btn-primary {
        width: 100%;
        font-weight: bold;
        font-size: 16px;
    }

    .auth-box .content .btn-sso {
        width: 100%;
        font-weight: bold;
        font-size: 16px;
        margin-top: 10px;
        background-color: #4caf50;
        color: #fff;
        border: none;
    }

    .auth-box .content .form-group .form-control {
        font-size: 14px;
    }

    .auth-box .content .lead {
        font-weight: bold;
        font-size: 18px;
    }

    .auth-box .right .heading {
        font-weight: bold;
        font-size: 24px;
    }

    .auth-box .right p {
        font-weight: bold;
        font-size: 16px;
    }
</style>

<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="<?= Yii::getAlias('@web') . Yii::$app->view->theme->baseUrl; ?>/images/logounida.png" alt="Logo"></div>
                        <p class="lead text-center">Login to your account</p>
                    </div>

                    <a href="<?= Url::to(['site/auth-sso']) ?>" class="btn btn-sso btn-lg btn-block"><i class="fa fa-globe"></i> CENTRAL SSO </a>

                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">SISTEM INFORMASI MUTU UNIDA GONTOR - SIMUDA</h1>
                    <p>Badan Penjaminan Mutu<br>Universitas Darussalam Gontor</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>