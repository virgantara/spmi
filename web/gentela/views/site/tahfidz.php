<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\assets\DashboardAsset;
use app\assets\HighchartAsset;
use app\models\ErpKamar;
use app\models\SimakPilihan;
use app\models\SimakMasterprogramstudi;

DashboardAsset::register($this);
// HighchartAsset::register($this);

$this->title = Yii::t('app', Yii::$app->name);


?>

<h3 class="page-title">Selamat Datang di AMI</h3>

<?php


if(Yii::$app->user->can('baak') || Yii::$app->user->can('sekretearis') || Yii::$app->user->can('akpam') || Yii::$app->user->can('asesor') || Yii::$app->user->can('warek1'))
{
 ?>
 <p>
<?php
if (Yii::$app->user->can('warek1'))
{

  echo Html::a('<i class="lnr lnr-apartment"></i> Display Dashboard Akademik',['site/index'],['class'=>'btn btn-primary btn-lg']);
  echo '&nbsp;';
  echo Html::a('<i class="lnr lnr-cloud"></i> Display Dashboard Kepengasuhan',['site/kp'],['class'=>'btn btn-primary btn-lg']);
  
}
?>
</p>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekap Tahfidz</h1>
        
      </div>
      <div class="panel-body">
        <p>
          <?=Html::dropDownList('tahun_akademik','',ArrayHelper::map($list_tahun,'tahun_id','nama_tahun'),['id'=>'tahun_akademik','prompt'=>'- Pilih Tahun Akademik -']);?>
          <button type="button" class="btn btn-primary" id="btnShow"><i class="fa fa-search"></i> Tampilkan Data</button>
          <span id="loading" style="display: none"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> Loading. . .</span>
        </p>
        <div class="table-responsive">
         
            <table class="table table-bordered table-hover" id="tabel_rekap">
              <thead>
              <tr>
                <th rowspan="3">Fakultas</th>
                <th rowspan="3">Prodi</th>
                <th colspan="<?=count($list_kampus) * 3;?>" class="text-center">Kelas</th>
              </tr>
              <tr>
                <?php 
                foreach($list_kampus as $k)
                {
                ?>
                <th colspan="3" class="text-center"><?=$k->nama_kampus;?></th>
                <?php 
                }
                ?>
              </tr>
              <tr>
                <?php 
                foreach($list_kampus as $k)
                {
                ?>
                <th class="text-center"  width="5%" title="Sudah Lulus">S</th>
                <th class="text-center"  width="5%" title="Belum Lulus/Belum diinput">B</th>
                <th class="text-center"  width="5%"  title="Total Mhs">T</th>
                <?php 
                }
                ?>
                
                
              </tr>
               
            </thead>
            <tbody>
             
              
            </tbody>
            <tfoot>
              <tr>
                <td colspan="26">
                  NB: 
                  <ul>
                    <li>S: Jumlah Mahasiswa Sudah Lulus</li>
                    <li>B: Jumlah Mahasiswa Belum Lulus/Belum Diinput</li>
                    <li>T: Total Mahasiswa</li>
                  </ul>
                </td>
              </tr>
            </tfoot>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>


<?php
}

$jsScript = '



var results = [];

function getMahasiswa(tahun_akademik){
    
    var obj = new Object;
    obj.tahun_akademik = tahun_akademik;
   
    $.ajax({

        type : "POST",
        url : "'.Url::to(['/simak-mastermahasiswa/ajax-rekap-tahfidz-mahasiswa']).'",
        data : {
          dataPost : obj
        },
        async : true,
        error : function(e){
          $("#loading").hide();
        },
        beforeSend : function(){
          $("#loading").show(); 

        },
        success: function(hasil){
          var hasil = $.parseJSON(hasil);
          $("#loading").hide();
          $("#tabel_rekap > tbody").empty();
          var row = "";
          ';

          $labels = [];
          foreach($fakultas as $q => $f)
          {
            foreach($f->simakMasterprogramstudis as $p)
            {

              
              if(!in_array($f->nama_fakultas, $labels))
              {
                $labels[] = $f->nama_fakultas;
                $jsScript .= '
                  row += "<tr>"
                  row += "<td>'.$f->nama_fakultas.'</td>"
                  row += "<td>'.$p->singkatan.'</td>"
                ';
              }
              else{
                $jsScript .= '
                  row += "<tr>"
                  row += "<td></td>"
                  row += "<td>'.$p->singkatan.'</td>"
                ';
              }
              $jsScript .= PHP_EOL;
              foreach($list_kampus as $k)
              {
              
                
                $jsScript .= 'var count = hasil['.$f->kode_fakultas.']['.$p->kode_prodi.']['.$k->kode_kampus.']; ';
                $jsScript .= 'var belum = count[\'count_belum\'];';
                $jsScript .= 'var sudah = count[\'count_sudah\'];';
                $jsScript .= 'var total = count[\'count_total\'];';
                $jsScript .= PHP_EOL;
                $jsScript .= 'var linkBelum = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-tahfidz']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'&is_lulus=N">\'+belum+\'</a>\' ';
                $jsScript .= PHP_EOL;
                $jsScript .= 'var linkSudah = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-tahfidz']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'&is_lulus=Y">\'+sudah+\'</a>\' ';
                  $jsScript .= PHP_EOL;
                  $jsScript .= 'var linkTotal = \'<a target="_blank" href="'.Url::to(['simak-mastermahasiswa/list-tahfidz']).'?kampus='.$k->kode_kampus.'&prodi='.$p->kode_prodi.'&tahun_akademik=\'+tahun_akademik+\'">\'+total+\'</a>\' ';
                $jsScript .= PHP_EOL;
                $jsScript .= '
                  
                  var label = belum > 0 ? \'style="color:red"\' : \'\';
                  row += "<td class=\'text-center\'>"+linkSudah+"</td>"
                  row += "<td "+label+" class=\'text-center\'>"+linkBelum+"</td>"
                  row += "<td class=\'text-center\'>"+linkTotal+"</td>"
                ';
                $jsScript .= PHP_EOL;
                
              }

              $jsScript .= '
                row += "</tr>"
              ';
              $jsScript .= PHP_EOL;
               
            }
          }
          $jsScript .= '

          $("#tabel_rekap > tbody").append(row);

        }
    });
}

$("#btnShow").click(function(){

  getMahasiswa($("#tahun_akademik").val());
});

';


$this->registerJs($jsScript, \yii\web\View::POS_READY);

?>
