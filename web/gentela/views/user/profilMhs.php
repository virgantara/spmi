<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\helpers\MyHelper;
use kartik\grid\GridView;
use app\models\ErpKamar;
/* @var $this yii\web\View */
/* @var $model app\models\SimakMastermahasiswa */

$this->title = $model->nama_mahasiswa;
$this->params['breadcrumbs'][] = ['label' => 'Simak Mastermahasiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// $kamar = ErpKamar::findOne($mhs->kamar_id);

$list_status_aktif = MyHelper::getStatusAktivitas();
$nama_kabupaten = '';
$kabupaten = \app\models\SimakKabupaten::findOne(['id'=>$model->kabupaten]);
// print_r(($kabupaten));exit;
?>

<style>
.lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 8px;
  border-radius: 50%;
  border: 6px solid #000;
  border-color: #000 transparent #000 transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

</style>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel">
            <div class="panel-heading">
                <h2>
                    <?= Html::encode($this->title) ?>
                </h2>
            </div>

        <div class="panel-body">
            <p>
            


            <?= Html::a('<i class="fa fa-edit"></i> Update Profil', ['simak-mastermahasiswa/update-profil'], ['class' => 'btn btn-primary']) ?>

            <?= Html::a('<i class="fa fa-upload"></i> Upload Foto', ['simak-mastermahasiswa/upload-foto', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
            </p>
        

          
            <div class="row">
                <div class="col-xs-12 text-center">
                    <img width="200px" height="300px" src="<?=\yii\helpers\Url::to(['simak-mastermahasiswa/foto','id'=>$model->id]);?>"/>
                </div>
            </div>
            <div class="help-block"></div>
            <div class="row">
                <div class="col-lg-6">
                    <h3>Data Pribadi</h3>
                    <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'nim_mhs',
                            'nama_mahasiswa',
                            'tempat_lahir',
                            'tgl_lahir:date',
                            'jenis_kelamin',
                            'telepon',
                            'hp',
                            'email:email',
                            
                            // 'kode_jenjang_studi',
                        ]
                    ]);?>
                    </div>
                     <h3>Data Akademik</h3>
                     <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'kampus0.nama_kampus',
                            'kodeProdi.kodeFakultas.nama_fakultas',
                            'kodeProdi.nama_prodi',
                            'tahun_masuk',
                            // 'semester_awal',
                          
                            'tgl_masuk:date',
                            
                            [
                                'attribute' => 'status_aktivitas',
                                'label' => 'Status Aktif',
                                'value' => function($data) use ($list_status_aktif){
                                    return !empty($list_status_aktif[$data->status_aktivitas]) ? $list_status_aktif[$data->status_aktivitas] : null;
                                }
                            ],
                            'namaPembimbingAkademik',
                            // 'nip_co_promotor1',
                            'semester',
                            // 'kode_jenjang_studi',
                        ]
                    ]);?>
                </div>
                     <h3>Alamat</h3>
                     <div class="table-responsive">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'alamat',
                           
                            'ktp',
                            'rt',
                            'rw',
                            'dusun',
                            'kode_pos',
                            'desa',
                            'kecamatan',
                            [
                                'label' => 'Kota/Kabupaten',
                                'attribute' => 'kabupaten',
                                'value' => function($data) use ($kabupaten){
                                    return !empty($kabupaten) ? $kabupaten->kab : null;
                                }
                            ],
                            [
                                'label' => 'Provinsi',
                                'attribute' => 'provinsi',
                                'value' => function($data) use ($kabupaten){
                                    return !empty($kabupaten) && !empty($kabupaten->provinsi) ? $kabupaten->provinsi->prov : null;
                                }
                            ],
                            
                        ]
                    ]);?>
                </div>
                     <h3>Lain-lain</h3>
                     <div class="table-responsive">
                    <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    // 'jenis_tinggal',
                                    // 'penerima_kps',
                                    // 'no_kps',
                                    
                                    'status_warga',
                                    'warga_negara',
                                    // 'status_sipil',
                                    'tgl_lulus',
                                    'no_sk_yudisium',
                                    'tgl_sk_yudisium',
                                    'no_ijazah',
                                    // 'status_mahasiswa',
                                    [
                                        'attribute' => 'kamar_id',
                                        'label' => 'Kamar / Asrama',
                                        'value' => function($data){
                                            return !empty($data->kamar->asrama) && !empty($data->kamar->asrama) ? $data->kamar->nama.' / '.$data->kamar->asrama->nama : '';
                                        }
                                    ],
                                ]
                            ]);?>
                        </div>
                </div>
                <div class="col-xs-12 col-lg-6" >

                    <h3 class="text-center">Data PDDIKTI</h3>

                    <div class="col-xs-12">
                    <div class="lds-dual-ring" id="loading" style="display: none"></div>
                    <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td colspan="3" class="text-center"><h4>BIODATA DIRI</h4></td>
                        </tr>
                        <tr>
                            <td width="20%">Nama</td>
                            <td  width="4%">:</td>
                            <td><span id="nm_pd"></span></td>
                            
                            
                                
                        </tr>
                        <tr>
                            <td>NIM</td>
                            <td>:</td>
                           
                            <td><span id="nipd"></span></td>
                           
                                
                        </tr>
                        <tr>
                            <td>JK</td>
                            <td>:</td>
                            <td><span id="jk"></span></td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td>:</td>
                            <td><span id="nik"></span></td>
                        </tr>
                        <tr>
                            <td>TEMPAT LAHIR</td>
                            <td>:</td>

                            <td><span id="tmpt_lahir"></span></td>
                        </tr>
                        <tr>
                            <td>TGL LAHIR</td>
                            <td>:</td>

                            <td><span id="tgl_lahir"></span></td>
                        </tr>
                        
                        <tr>
                            <td>JALAN</td>
                            <td>:</td>

                            <td><span id="jln"></span></td>
                        </tr>
                        <tr>
                            <td>RT/RW</td>
                            <td>:</td>

                            <td><span id="rt"></span> / <span id="rw"></span></td>
                        </tr>
                        <tr>
                            <td>DUSUN</td>
                            <td>:</td>

                            <td><span id="nm_dsn"></span></td>
                        </tr>
                        <tr>
                            <td>Desa</td>
                            <td>:</td>

                            <td><span id="ds_kel"></span></td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td>:</td>

                            <td><span id="fk__wil"></span></td>
                        </tr>
                        <tr>
                            <td>Kabupaten</td>
                            <td>:</td>

                            <td><span id="nm_kab"></span></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>:</td>

                            <td><span id="nm_prov"></span></td>
                        </tr>
                        <tr>
                            <td>Kodepos</td>
                            <td>:</td>

                            <td><span id="kode_pos"></span></td>
                        </tr>
                        <tr>
                            <td>No Telp Rumah</td>
                            <td>:</td>

                            <td><span id="no_tel_rmh"></span></td>
                        </tr>
                        <tr>
                            <td>HP</td>
                            <td>:</td>

                            <td><span id="hp"></span></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>

                            <td><span id="email"></span></td>
                        </tr>
                        <tr>
                            <td>NPWP</td>
                            <td>:</td>

                            <td><span id="npwp"></span></td>
                        </tr>
                    </table>
                </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tr>
                            <td colspan="3" class="text-center"><h4>IBU</h4></td>
                        </tr>
                        <tr>
                            <td width="20%">Nama Ibu Kandung</td>
                            <td  width="4%">:</td>
                            <td><span id="nm_ibu_kandung"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Tgl Lahir Ibu</td>
                            <td  width="4%">:</td>
                            <td><span id="tgl_lahir_ibu"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">NIK Ibu</td>
                            <td  width="4%">:</td>
                            <td><span id="nik_ibu"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Pendidikan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_jenjang_pendidikan_ibu"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Pekerjaan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_pekerjaan_ibu"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Penghasilan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_penghasilan_ibu"></span></td>
                               
                        </tr>
                        <tr>
                            <td colspan="3" class="text-center"><h4>AYAH</h4></td>
                        </tr>
                        <tr>
                            <td width="20%">Nama Ayah</td>
                            <td  width="4%">:</td>
                            <td><span id="nm_ayah"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Tgl Lahir Ayah</td>
                            <td  width="4%">:</td>
                            <td><span id="tgl_lahir_ayah"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">NIK Ayah</td>
                            <td  width="4%">:</td>
                            <td><span id="nik_ayah"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Pendidikan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_jenjang_pendidikan_ayah"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Pekerjaan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_pekerjaan_ayah"></span></td>
                               
                        </tr>
                        <tr>
                            <td width="20%">Penghasilan</td>
                            <td  width="4%">:</td>
                            <td><span id="id_penghasilan_ayah"></span></td>
                               
                        </tr>
                    </table>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php


/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakMahasiswaOrtuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode('Data Ortu') ?></h3>
            </div>
<div class="panel-body ">

    <?php
    $gridColumns = [
    [
        'class'=>'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'pageSummary'=>'Total',
        'pageSummaryOptions' => ['colspan' => 6],
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
        [
            'attribute' => 'hubungan',
            'filter' => ['AYAH' => 'AYAH','IBU'=>'IBU','WALI' =>'WALI'],
            'value'=>function($data){
                return $data->hubungan;
            }
        ],
        'nama',
        // [
        //     'label'=>'Agama',
        //     'value'=>function($data){
        //         return $data->agama0->label;
        //     }
        // ],
        [
            'attribute'=>'pendidikan',
            'filter' => $searchModel->pendidikanList,
            'value'=>function($data){
                return !empty($data->pendidikan0) ? $data->pendidikan0->label : '<span style="color:red">Data pendidikan belum diisi</span';
            }
        ],
        [
            'attribute'=>'pekerjaan',
            'filter' => $searchModel->pekerjaanList,
            'value'=>function($data){
                return !empty($data->pekerjaan0) ? $data->pekerjaan0->label : '<span style="color:red">Data pekerjaan belum diisi</span';
            }
        ],
        [
            'attribute'=>'penghasilan',
            'filter' => $searchModel->penghasilanList,
            'value'=>function($data){
                return !empty($data->penghasilan0) ? $data->penghasilan0->label : '<span style="color:red">Data penghasilan belum diisi</span';
            }
        ],
        // 'hidup',
        'alamat',
        // 'kota',
        // 'propinsi',
        //'negara',
        //'pos',
        // 'telepon',
        'hp',
            //'email:email',
            //'is_synced',
            //'created_at',
            //'updated_at',
    
];?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'responsiveWrap' => false,
        'containerOptions' => ['style' => 'overflow: auto'], 
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'beforeHeader'=>[
            [
                'columns'=>[
                    ['content'=> $this->title, 'options'=>['colspan'=>14, 'class'=>'text-center warning']], //cuma satu 
                ], 
                'options'=>['class'=>'skip-export'] 
            ]
        ],
        'exportConfig' => [
              GridView::PDF => ['label' => 'Save as PDF'],
              GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
              GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
              GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
          ],
          
        'toolbar' =>  [
            '{export}', 

           '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        // 'condensed' => false,
        // 'responsive' => false,
        'hover' => true,
        // 'floatHeader' => true,
        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>

</div>
        </div>
    </div>

</div>

<?php
$this->registerJs('

function getFeeder(nim){

    var obj = new Object;
    obj.nim = nim;

    $.ajax({
        type : \'POST\',
        url : \''.Url::to(['api/ajax-profil-feeder']).'\',
        data : {
            dataPost : obj
        },
        async: true,
        error: function(e){
            $("#loading").hide();
        },
        beforeSend : function(){
            $("#loading").show();
        },
        success: function(res){
            $("#loading").hide();
            
            var hasil = $.parseJSON(res);
            if(hasil.code == 200){
                var res = hasil.data

                $("#nm_pd").html(res.nama_mahasiswa);
                $("#nipd").html(res.nim);
                $("#jk").html(res.jenis_kelamin);
                $("#nik").html(res.nik);
                $("#tmpt_lahir").html(res.tempat_lahir);
                $("#tgl_lahir").html(res.tanggal_lahir);
                $("#jln").html(res.jalan);
                $("#nm_dsn").html(res.dusun);
                $("#rt").html(res.rt);
                $("#rw").html(res.rw);
                $("#ds_kel").html(res.kelurahan);
                $("#fk__wil").html(res.fk__wil);
                $("#kode_pos").html(res.kode_pos);
                $("#no_tel_rmh").html(res.telepon);
                $("#hp").html(res.handphone);
                $("#email").html(res.email);
                $("#npwp").html(res.npwp);
                $("#nm_ibu_kandung").html(res.nama_ibu_kandung);
                $("#tgl_lahir_ibu").html(res.tanggal_lahir_ibu);
                $("#nm_ayah").html(res.nama_ayah);
                $("#tgl_lahir_ayah").html(res.tanggal_lahir_ayah);
                $("#nik_ayah").html(res.nik_ayah);
                $("#nik_ibu").html(res.nik_ibu);
                
                $("#id_jenjang_pendidikan_ibu").html(res.nama_pendidikan_ibu);
                $("#id_jenjang_pendidikan_ayah").html(res.nama_pendidikan_ayah);
                $("#id_penghasilan_ibu").html(res.nama_penghasilan_ibu);
                $("#id_penghasilan_ayah").html(res.nama_penghasilan_ayah);
                $("#id_pekerjaan_ibu").html(res.nama_pekerjaan_ibu);
                $("#id_pekerjaan_ayah").html(res.nama_pekerjaan_ayah);
                // $("#nm_kab").html(hsl2.nm_wil);
                // console.log(res)
                // getWilayah(res.id_wilayah,function(err, hsl){
                //     getWilayah(hsl.id_induk_wilayah, function(err, hsl2){
                //         getWilayah(hsl2.id_induk_wilayah, function(err, hsl3){
                //             $("#nm_kab").html(hsl2.nm_wil);
                //             $("#nm_prov").html(hsl3.nm_wil);
                //         });
                //     });
                // });
            }
        }

    });
}

getFeeder("'.$model->nim_mhs.'");

function getWilayah(id_wil, callback){

    $.ajax({
        type : \'POST\',
        data : \'id_wil=\'+id_wil,
        url : \''.Url::to(['api/ajax-wilayah-feeder']).'\',
        async: true,
        beforeSend : function(){

        },
        success: function(res){
            var res = $.parseJSON(res);
            callback(null,res)
        }

    });
}

function getJenjang(id_jenjang_didik, callback){

    $.ajax({
        type : \'POST\',
        data : \'id_jenjang_didik=\'+id_jenjang_didik,
        url : \''.Url::to(['api/ajax-pendidikan-feeder']).'\',
        async: true,
        beforeSend : function(){

        },
        success: function(res){
            var res = $.parseJSON(res);
            callback(null,res)
        }

    });
}

function getPekerjaan(id, callback){

    $.ajax({
        type : \'POST\',
        data : \'id_pekerjaan=\'+id,
        url : \''.Url::to(['api/ajax-pekerjaan-feeder']).'\',
        async: true,
        beforeSend : function(){

        },
        success: function(res){
            var res = $.parseJSON(res);
            callback(null,res)
        }

    });
}

function getPenghasilan(id, callback){

    $.ajax({
        type : \'POST\',
        data : \'id_penghasilan=\'+id,
        url : \''.Url::to(['api/ajax-penghasilan-feeder']).'\',
        async: true,
        beforeSend : function(){

        },
        success: function(res){
            var res = $.parseJSON(res);
            callback(null,res)
        }

    });
}

', \yii\web\View::POS_READY);
?>