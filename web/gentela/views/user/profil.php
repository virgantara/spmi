<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$this->title = Yii::t('app', 'Profil User') . ': ' . $model->username;
?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }
    ?>
<div class="page-title">
              <div class="title_left">
                <h3>User Profile</h3>
              </div>

    
            </div>
<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profile</h2>
          
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-6 col-sm-6 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="<?=Yii::$app->view->theme->baseUrl?>/images/user.png" alt="Foto profil" title="Foto profil">
                        </div>
                      </div>
                      <h3><?=$dosen->nama_dosen;?></h3>

                      <ul class="list-unstyled user_data">
                        <li> <?=$dosen->tempat_lahir_dosen;?>, <?=date('d-m-Y',strtotime($dosen->tgl_lahir_dosen));?>
                        </li>
                         <li><span><?=$dosen->no_hp_dosen;?></span></li>
                        <li><span><?=$model->email;?></span></li>
                        <li><span><?=$dosen->nidn_asli;?></span></li>
                        
                      </ul>
                       <a href="<?=Url::to(['simak-masterdosen/update-profil']);?>" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a>
                        <a href="<?=Url::to(['user/ubah-akun']);?>" class="btn btn-primary"><i class="fa fa-lock"></i> Akun</a>
                      

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <!-- end of user-activity-graph -->

                    </div>
                  </div>
                </div>
              </div>
            </div>

