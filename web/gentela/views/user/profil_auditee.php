<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */


$this->title = 'Update Auditor: ' . $user->nama;
$this->params['breadcrumbs'][] = ['label' => 'Profil', 'url' => ['profil']];
?>

<?php

use app\models\UnitKerja;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */

$listJenis = [
  'prodi' => 'Program Studi',
  'fakultas' => 'Fakultas',
  'satker' => 'Satuan Kerja',
  'koordinator' => 'Koordinator RTM',
]
?>

<style>
  .profile_img {
    text-align: center;
  }

  #crop-avatar img {
    width: 150px;
    height: 150px;
    object-fit: cover;
    border-radius: 50%;
    /* Membuat gambar menjadi lingkaran */
  }

  table tr {
    margin-bottom: 10px;
    /* Ubah nilai ini sesuai dengan kebutuhan jarak antar baris */
  }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Profile</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="col-md-6 col-sm-6 col-xs-12">
          <h3>Profil auditee</h3>
          <hr>
          <table width="100%">
            <tr>
              <td width="20%">Nama Auditee</td>
              <td width="2%">:</td>
              <td><?= $auditee->nama; ?></td>
            </tr>
            <tr>
              <td>Singkatan</td>
              <td>:</td>
              <td><?= $auditee->singkatan; ?></td>
            </tr>
            <tr>
              <td>Jenis Auditee</td>
              <td>:</td>
              <td><?= $listJenis[$auditee->jenis]; ?></td>
            </tr>
            <tr>
              <td>Dibawah</td>
              <td>:</td>
              <td><?= UnitKerja::findOne($auditee->parent_id)->nama; ?></td>
            </tr>
            <tr>
              <td>Email</td>
              <td>:</td>
              <td><?= $auditee->email; ?></td>
            </tr>
            <tr>
              <td>Penanggung Jawab</td>
              <td>:</td>
              <td><?= $auditee->penanggung_jawab; ?></td>
            </tr>
          </table>
          <br>

          <a href="<?= Url::to(['unit-kerja/update-profil']); ?>" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a>


        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">

          <h3>Profil akun</h3>
          <hr>

          <table width="100%">
            <tr>
              <td width="20%">Nama</td>
              <td width="2%">:</td>
              <td><?= $user->nama; ?></td>
            </tr>
            <tr>
              <td>username</td>
              <td>:</td>
              <td><?= $user->username; ?></td>
            </tr>
            <tr>
              <td>Email</td>
              <td>:</td>
              <td><?= $user->email; ?></td>
            </tr>
          </table>
          <br>
          <a href="<?= Url::to(['user/update-akun', 'id' => $user->id]); ?>" class="btn btn-primary"><i class="fa fa-lock"></i> Akun</a>


          <!-- end of user-activity-graph -->

        </div>

      </div>
    </div>
  </div>
</div>