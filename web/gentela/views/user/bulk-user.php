<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Create/Update Bulk Users';

$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Bulk-User', 'url' => ['bulk-user']];
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : '';
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$nim = !empty($_GET['nim']) ? $_GET['nim'] : '';
$nim_mhs = !empty($_GET['nim_mhs']) ? $_GET['nim_mhs'] : '';
$nama_mahasiswa = !empty($_GET['nama_mahasiswa']) ? $_GET['nama_mahasiswa'] : '';
$kampus = !empty($_GET['kampus']) ? $_GET['kampus'] : '';
$tahun_masuk = !empty($_GET['tahun_masuk']) ? $_GET['tahun_masuk'] : '';
$status_aktivitas = !empty($_GET['status_aktivitas']) ? $_GET['status_aktivitas'] : '';

$listKampus = \app\models\SimakKampus::find();
if(Yii::$app->user->identity->access_role == 'sub baak')
{
  $listKampus->andWhere(['kode_kampus' => Yii::$app->user->identity->kampus]);
}

$listKampus = $listKampus->all();

// $list_kampus = ArrayHelper::map($listKampus, 'kode_kampus','nama_kampus');
?>
<?php
    $gridColumns = [
    ['class' => '\kartik\grid\CheckboxColumn'],
    [
        'class'=>'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'pageSummary'=>'Total',
        'pageSummaryOptions' => ['colspan' => 6],
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
            'nim_mhs',
            'nama_mahasiswa',
            [
                'attribute' => 'tgl_lahir',
                'header' => 'Password'
            ],
            'email',
            'jenis_kelamin',
            [
                'attribute'=>'kampus',

                'value' => function ($data) {
                    return !empty($data->kampus0) ? $data->kampus0->nama_kampus : '-';
                },
                
            ],
            'tahun_masuk',
            
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{modify} ',
        'buttons' => [
            'modify' => function ($url, $model){
                return Html::a('<i class="fa fa-refresh"></i> Modify SSO Account', ['user/generate-account','nim'=>$model->nim_mhs], [
                    'title' => Yii::t('app', 'Modify '),
                    'data-pjax' =>0,
                    'data-item' => $model->id,
                    'class' => 'modify-sso btn-success btn'
                ]);
            },    
            'sync' => function ($url, $model){
                return Html::a('<i class="fa fa-google"></i> Modify GMail Account', ['user/modify-gmail-account','nim' => $model->nim_mhs], [
                    'title' => Yii::t('app', 'Sync '),
                    'data-pjax' =>0,
                    'data-item' => $model->id,
                    'class' => 'link-sync btn btn-primary'
                ]);
            },
            
          ],
        'visibleButtons' => [
            
            'sync' => function($data){
                return Yii::$app->user->can('theCreator');
            }
        ]
        
    ]
];?>    
<div class="row">
  <div class="col-md-3">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$this->title;?></h3>
      </div>
      <div class="panel-body">

         <?php $form = ActiveForm::begin([
          'method' => 'GET',
          'action' => ['user/bulk-user'],
          'options' => [
                'id' => 'form_validation',
          ]
        ]); ?>
       
          <?php 
         
            $tmp = \app\models\SimakMasterprogramstudi::find();
          

            $listProdi = $tmp->all();
          ?>
        <div class="form-group">
              <label class="control-label ">Kampus</label>
              <?php 
                echo Select2::widget([
                    'name' => 'kampus',
                    'value' => $kampus,
                    'data' => \yii\helpers\ArrayHelper::map($listKampus,'kode_kampus','nama_kampus'),
                    'options'=>['id'=>'kampus','placeholder'=>Yii::t('app','- Pilih Kampus -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])
               ?>
             
          </div>
          <div class="form-group">
              <label class="control-label ">Prodi</label>
              <?php 
                echo Select2::widget([
                    'name' => 'prodi',
                    'value' => $prodi,
                    'data' => \yii\helpers\ArrayHelper::map($listProdi,'kode_prodi','nama_prodi'),
                    'options'=>['id'=>'prodi','placeholder'=>Yii::t('app','- Pilih Prodi -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])
               ?>
             
          </div>

          <div class="form-group">
              <label class="control-label ">Tahun Angkatan / Masuk</label>
               <?php 
                echo Select2::widget([
                    'name' => 'tahun_masuk',
                    'value' => $tahun_masuk,
                    'data' => $list_tahun,
                    'options'=>['id'=>'tahun_masuk','placeholder'=>Yii::t('app','- Pilih Angkatan/Tahun Masuk -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])
               ?>
             
          </div>

          <div class="form-group">
              <label class="control-label ">Status Mhs</label>
              <?php 
                echo Select2::widget([
                    'name' => 'status_aktivitas',
                    'value' => $status_aktivitas,
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\SimakPilihan::find()->where(['kode'=>'05'])->all(),'value','label'),
                    'options'=>['id'=>'status_aktivitas','placeholder'=>Yii::t('app','- Pilih Status Mhs -')],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])
               ?>
          </div>

          <div class="form-group">
            <label class="control-label ">NIM / Nama Mahasiswa</label>          
            <?=Html::textInput('nim',$nim,['id'=>'nama_mahasiswa','class'=>'form-control','placeholder'=>'..Ketik NIM / Nama Mahasiswa..']);?>
            <input type="hidden" id="nim" name="nim_mhs" value="<?=$nim_mhs?>">
                      
          </div>
          <div class="form-group clearfix">
            <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Preview </button>
            <button type="submit" class="btn btn-success" name="btn-generate" id="btn-generate" value="1"><i class="fa fa-refresh"></i> Modify All Accounts </button>
          </div>
        
         <?php ActiveForm::end(); ?>
      </div>
      
    </div>
  </div>
  <div class="col-md-9">
        <div class="panel">
            <div class="panel-body">
                <p>
                    <?php 
                    // Renders a export dropdown menu
                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns,
                        'clearBuffers' => true, //optional
                    ]);
                    ?>
                    &nbsp;<button class="sync-selected btn btn-primary"><i class="fa fa-refresh"></i> Sync Gmail on selected items</button>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'columns' => $gridColumns,
                        'containerOptions' => ['style' => 'overflow: auto'], 
                        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                        'containerOptions' => ['style'=>'overflow: auto'], 
                        'beforeHeader'=>[
                            [
                                'columns'=>[
                                    ['content'=> $this->title, 'options'=>['colspan'=>17, 'class'=>'text-center warning']], //cuma satu 
                                ], 
                                'options'=>['class'=>'skip-export'] 
                            ]
                        ],
                        'exportConfig' => [
                              GridView::PDF => ['label' => 'Save as PDF'],
                              GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                              GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                              GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                          ],
                          
                        'toolbar' =>  [
                            // '{export}', 

                           '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                        ],
                        'toggleDataContainer' => ['class' => 'btn-group mr-2'],

                        'export' => [
                            'fontAwesome' => true
                        ],
                        'pjax' => true,
                        'pjaxSettings' =>[
                            'neverTimeout'=>true,
                            'options'=>[
                                'id'=>'pjax-container',
                            ]
                        ],  
                        'id' => 'grid-mhs',
                        'bordered' => true,
                        'striped' => true,
                        // 'condensed' => false,
                        // 'responsive' => false,
                        'hover' => true,
                        // 'floatHeader' => true,
                        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY
                        ],
                    ]); ?>
            </div>
        </div>
    </div>
</div>
    
<div class="row">
    
</div>
        

       
<?php 

$this->registerJs(' 


$(\'.sync-selected\').click(function(e){
    var keys = $("#grid-mhs").yiiGridView("getSelectedRows");
    e.preventDefault();
    
    Swal.fire({
      title: "Pembuatan Akun Google Mail Mahasiswa!",
      text: \'Proses ini juga berdampak pada password akun CENTRAL SSO. Apakah Anda ingin melanjutkan?\',
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, lanjut!"
    }).then((result) => {
        if(result.isConfirmed){
            var obj = new Object;
            obj.keys = keys;
            $.ajax({

                type : "POST",
                url : "/user/ajax-create-gmail-multiple",
                data : {
                    dataPost : obj
                },
               
                beforeSend: function(){
                   Swal.fire({
                        title : \'Please wait\',
                        html: \'Processing your request...\',
                        
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                        
                    })
                },
                error: function(e){
                    Swal.close()
                },
                success: function(data){
                    Swal.close()
                    var data = $.parseJSON(data)
                    

                    if(data.code == 200){
                        Swal.fire({
                            title: "Yeay!",
                            icon: "success",
                            text: data.message
                        });

                        $.pjax.reload({container: "#pjax-container", async: true});
                    }
                    
                    else{
                        Swal.fire({
                            title: "Oops!",
                            icon: "error",
                            text: data.message
                        });

                    }
                }
            })
        }
    });
});

$(document).bind("keyup.autocomplete",function(){
  $(\'#nama_mahasiswa\').autocomplete({
      minLength:1,
      select:function(event, ui){
       
        $(\'#nim\').val(ui.item.nim);
                
      },
      
      focus: function (event, ui) {
        $(\'#nim\').val(ui.item.nim);
       
      },
      source:function(request, response) {
        $.ajax({
                url: "'.Url::to(['simak-mastermahasiswa/ajax-cari-mahasiswa']).'",
                dataType: "json",
                data: {
                    term: request.term,
                    prodi : $("#prodi").val(),
                    kampus : $("#kampus").val(),
                    semester : $("#semester").val(),
                    status : $("#status_aktivitas").val()
                },
                success: function (data) {
                    response(data);
                }
            })
        },
       
  }); 

});

$(document).on("click","#btn-generate",function(e){
    e.preventDefault()
    var obj = new Object;

    obj.prodi = $("#prodi").val();
    obj.tahun_masuk = $("#tahun_masuk").val();
    obj.status_aktivitas = $("#status_aktivitas").val();
    
    var ajax_url= "'.Url::to(['user/ajax-generate-bulk-accounts']).'";

    $.ajax({
        type: "POST",
        url: ajax_url,
        data: {
            dataPost : obj
        },
        async: true,
        error : function(e){
            Swal.close()
            console.log(e.responseText)
        },
        beforeSend: function(){
            Swal.fire({
                title : "Please wait",
                html: "Processing your request...",
                showConfirmButton: false,
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
                
            })
        },
        success: function (data) {
            Swal.close()
            var hasil = $.parseJSON(data)
            if(hasil.code==200){
                Swal.fire({
                  icon: \'success\',
                  title: \'Yeay\',

                  text: hasil.message
                })
                
            }

            else{
                Swal.fire({
                  icon: \'error\',
                  title: \'Oops...\',
                  text: hasil.message,
                })
            }

        }
    })
})
', \yii\web\View::POS_READY);

?>