<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */


$this->title = 'Update Auditor: ' . $user->nama;
$this->params['breadcrumbs'][] = ['label' => 'Profil', 'url' => ['profil']];
?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
  .profile_img {
    text-align: center;
  }

  #crop-avatar img {
    width: 150px;
    height: 150px;
    object-fit: cover;
    border-radius: 50%;
    /* Membuat gambar menjadi lingkaran */
  }

  table tr {
    margin-bottom: 10px;
    /* Ubah nilai ini sesuai dengan kebutuhan jarak antar baris */
  }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Profile</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="col-md-6 col-sm-6 col-xs-12">
          <h3>Profil auditor</h3>
          <hr>
          <table width="100%">
            <tr>
              <td width="20%">Nama</td>
              <td width="2%">:</td>
              <td><?= $auditor->nama; ?></td>
            </tr>
            <tr>
              <td>NIDN</td>
              <td>:</td>
              <td><?= $auditor->nidn; ?></td>
            </tr>
            <tr>
              <td>NIY</td>
              <td>:</td>
              <td><?= $auditor->niy; ?></td>
            </tr>
            <tr>
              <td>Email</td>
              <td>:</td>
              <td><?= $auditor->email; ?></td>
            </tr>
            <tr>
              <td>Nomor Registrasi</td>
              <td>:</td>
              <td><?= $auditor->nomor_registrasi; ?></td>
            </tr>
            <tr>
              <td>Nomor SK</td>
              <td>:</td>
              <td><?= $auditor->nomor_sk; ?></td>
            </tr>
            <tr>
              <td>Program Studi</td>
              <td>:</td>
              <td><?= $auditor->prodi; ?></td>
            </tr>
          </table>
          <br>

          <a href="<?= Url::to(['auditor/update-profil', 'id' => $auditor->id]); ?>" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a>


        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">

          <h3>Profil akun</h3>
          <hr>

          <table width="100%">
            <tr>
              <td width="20%">Nama</td>
              <td width="2%">:</td>
              <td><?= $user->nama; ?></td>
            </tr>
            <tr>
              <td>username</td>
              <td>:</td>
              <td><?= $user->username; ?></td>
            </tr>
            <tr>
              <td>Email</td>
              <td>:</td>
              <td><?= $user->email; ?></td>
            </tr>
          </table>
          <br>
          <a href="<?= Url::to(['user/update-akun', 'id' => $user->id]); ?>" class="btn btn-primary"><i class="fa fa-lock"></i> Akun</a>


          <!-- end of user-activity-graph -->

        </div>

      </div>
    </div>
  </div>
</div>