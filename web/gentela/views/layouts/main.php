<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\SweetalertAsset;
use app\assets\AppAsset;
use app\widgets\Alert;
use richardfan\widget\JSRegister;
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

AppAsset::register($this);
SweetalertAsset::register($this);
$theme = Yii::$app->view->theme->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>

  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head(); ?>

</head>
<style>
  .swal2-popup {
    font-size: 1.6rem !important;
  }

  .ui-autocomplete {
    z-index: 2147483647;
  }
</style>

<body class="nav-md">
  <noscript>
    <div style="color:red">Javascript di browser tidak aktif.<br />Mohon aktifkan Javascript di setting browser anda.</div>
  </noscript>
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="<?= Url::home() ?>" class="site_title"><img src="<?= $theme ?>/images/logo.png" alt=""> <span><?= Yii::$app->name ?></span></a>
          </div>

          <div class="clearfix"></div>
          <?php

          if (!Yii::$app->user->isGuest) {

          ?>
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?= $theme ?>/images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Logged in as</span>
                <h2><?= Yii::$app->user->identity->access_role; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
          <?php } ?>
          <br />
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">


              <?php

              $menuItems = \app\helpers\MenuHelper::getMenuItems();

              echo Menu::widget([
                'options' => array('class' => 'nav side-menu'),
                'itemOptions' => array('class' => 'hover'),

                // 'itemCssClass'=>'hover',
                'encodeLabels' => false,
                'items' => $menuItems
              ]);


              ?>
            </div>
          </div>
          <!-- <div class="sidebar-footer hidden-small">
                      <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                      </a>
                      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                      </a>
                      <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                      </a>
                      <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                      </a>
                    </div> -->
        </div>
      </div>
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <?php

            if (!Yii::$app->user->isGuest) {
            ?>
              <?php

              $menuItems = \app\helpers\MenuHelper::getTopMenus();

              echo Menu::widget([
                'options' => array('class' => 'nav navbar-nav navbar-right'),
                'itemOptions' => array('class' => 'dropdown'),

                // 'itemCssClass'=>'hover',
                'encodeLabels' => false,
                'items' => $menuItems
              ]); ?>

            <?php } ?>
          </nav>
        </div>
      </div>
      <div class="right_col" role="main">
        <?=
        Breadcrumbs::widget([
          'homeLink' => [
            'label' => Yii::t('yii', 'Dashboard'),
            'url' => Yii::$app->homeUrl,
          ],
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
        <?php
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
        } ?>
        <?= $content; ?>
      </div>
      <footer>
        <div class="row">
          <div class="col-md-6">
            Nomor Registrasi Hak Cipta: EC00202307063<br>
            No Pencatatan: 000439985
          </div>
          <div class="col-md-6">
            <div class="pull-right">
              UPT PPTIK UNIDA Gontor &copy; 2022 - <?= date('Y') ?></a>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </footer>
    </div>
  </div>


  <?php $this->endBody() ?>
</body>

<?php JSRegister::begin() ?>
<script>
  $(document).on('click', '.language', function() {
    var lang = $(this).attr('id')

    $.post('/site/language', {
      'lang': lang
    }, function(data) {
      location.reload();
    });
  });
</script>
<?php JSRegister::end() ?>

</html>
<?php $this->endPage() ?>