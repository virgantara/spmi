<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\LoginAsset;
use app\widgets\Alert;
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\nav\NavX;

LoginAsset::register($this);

$theme = $this->theme;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=Yii::$app->view->theme->baseUrl;?>/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=Yii::$app->view->theme->baseUrl;?>/assets/img/favicon.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title.' '.Yii::$app->name) ?></title>

    <?php $this->head(); ?>
</head>
<body>
	<div id="wrapper"> 
  <?=$content;?>
  <?php $this->endBody() ?>
</div>
</body>

</html>
<?php $this->endPage() ?>
