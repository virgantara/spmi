<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;

use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presensi';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : '';
// $dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';

$tmp = \app\models\SimakMasterprogramstudi::find();

if (Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('theCreator')) {
  $tmp->andWhere(['kode_prodi' => Yii::$app->user->identity->prodi]);
}

$semester = !empty($_GET['semester']) ? $_GET['semester'] : '';
$listProdi = $tmp->all();
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';
$jadwal_id = !empty($_GET['jadwal_id']) ? $_GET['jadwal_id'] : '';

$list_absensi = \app\helpers\MyHelper::getListAbsensi();
?>

<h3 class="page-title"><?= $this->title; ?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
    <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/presensi', 'kode' => $kode],
      'options' => [
        'id' => 'form_validation',
      ]
    ]); ?>
    <div class="form-group">
      <label class="control-label ">Tahun Akademik</label>
      <?= Html::dropDownList('tahun_id', $tahun_id, \yii\helpers\ArrayHelper::map($listTahun, 'tahun_id', 'nama_tahun'), ['class' => 'form-control', 'id' => 'tahun_id']) ?>
    </div>

    <div class="form-group">
      <label class="control-label ">Prodi</label>
      <?= Select2::widget([
        'name' => 'prodi',
        'value' => $prodi,
        'data' => \yii\helpers\ArrayHelper::map($listProdi, 'kode_prodi', 'nama_prodi'),
        'options' => [
          'id' => 'prodi_id',
          'placeholder' => Yii::t('app', '- Pilih Prodi -'),

        ],
        'pluginOptions' => [
          'initialize' => true,
          'allowClear' => true,
        ],
      ]) ?>

    </div>
    <div class="form-group">
      <label class="control-label ">Semester</label>
      <?= Html::dropDownList('semester', $semester, \app\helpers\MyHelper::getListSemester(), ['id' => 'semester', 'class' => 'form-control', 'prompt' => '- Pilih Semester -']) ?>
    </div>
    <div class="form-group">
      <label class="control-label ">Mata kuliah</label>
      <?= DepDrop::widget([
        'name' => 'kode_mk',
        'value' => $kode_mk,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'kode_mk'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester'],
          'initialize' => true,
          'placeholder' => '- Pilih Mata Kuliah -',
          'url' => Url::to(['/simak-mastermatakuliah/submatkul'])
        ]
      ]) ?>


    </div>
    <div class="form-group">
      <label class="control-label ">Kelas</label>
      <?= DepDrop::widget([
        'name' => 'jadwal_id',
        'value' => $jadwal_id,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'jadwal_id'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester', 'kode_mk'],
          'initialize' => true,
          'placeholder' => '- Pilih Kelas -',
          'url' => Url::to(['/simak-jadwal/subjadwal'])
        ]
      ]) ?>


    </div>
    <div class="form-group clearfix">
      <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
      <a target="_blank" class="btn btn-success" href="<?= Url::to(['simak-jadwal/export-presensi', 'id' => $jadwal_id, 'kode' => $kode]); ?>"><i class="fa fa-file-pdf-o"></i> Download Presensi Harian</a>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">

        <h3 class="panel-title">Daftar Presensi Harian Mata Kuliah <strong><?= !empty($mk) ? '[' . $mk->kode_mk . '] ' . $mk->nama_mk : ''; ?></strong> dengan dosen pengampu <strong><?= !empty($dosen) ? $dosen->nama_dosen : ''; ?></strong></h3>
        <div class="alert alert-danger">Data yang ada di sini hanya yang sudah disetujui KRSnya.</div>

      </div>
      <div class="panel-body ">

        <div class="table-responsive">
          <table class="table table-hover table-bordered table-striped">
            <thead>
              <tr>
                <th rowspan="2">#</th>
                <th rowspan="2">NIM</th>
                <th rowspan="2">Mahasiswa</th>
                <th rowspan="2">Semester</th>
                <th colspan="<?= $jadwal->jumlah_tatap_muka; ?>" class="text-center">Pertemuan Ke-</th>
              </tr>
              <tr>
                <?php
                for ($i = 1; $i <= $jadwal->jumlah_tatap_muka; $i++) {
                  echo '<th>' . $i . '</th>';
                }
                ?>

              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($results as $q => $m) {
                if (empty($m->mahasiswa0)) continue;
              ?>
                <tr>
                  <td><?= $q + 1; ?></td>
                  <td><?= $m->mahasiswa0->nim_mhs; ?></td>
                  <td><?= $m->mahasiswa0->nama_mahasiswa; ?></td>
                  <td><?= $m->mahasiswa0->semester; ?></td>
                  <?php
                  for ($i = 0; $i < $jadwal->jumlah_tatap_muka; $i++) {
                    $absensi = \app\models\SimakAbsenHarian::getAbsenMahasiswa($m->mahasiswa, $i + 1, $jadwal->id);
                    $status_kehadiran = !empty($absensi) ? $absensi->status_kehadiran : null;
                    $status_kehadiran = !empty($list_absensi[$status_kehadiran]) ? $list_absensi[$status_kehadiran] : null;
                    echo '<td class="text-center">' . $status_kehadiran . '</td>';
                  }
                  ?>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>