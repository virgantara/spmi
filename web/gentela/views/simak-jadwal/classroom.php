<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use app\assets\IntroAsset;
IntroAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Info mata kuliah '.(!empty($mk) ? '['.$mk->kode_mk.'] '.$mk->nama_mk : '-');
$this->params['breadcrumbs'][] = $this->title;
$url_tugas = ['simak-tugas/tugasku','jadwal_id'=>$jadwal->id];
$url_silabus = ['simak-jadwal/view-silabus','id'=>$jadwal->id];

if(Yii::$app->user->can('Dosen') || Yii::$app->user->can('sekretearis')) {
    $url_tugas = ['simak-tugas/index','jadwal_id'=>$jadwal->id];
    $url_silabus = ['simak-jadwal/silabus','id'=>$jadwal->id];
}  
?>
<p>
    <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', 'javascript:history.back()', ['class' => 'btn btn-danger']) ?>
</p>
<h3><?= Html::encode($this->title) ?></h3>

<div class="panel panel-profile">
	<div class="custom-tabs-line tabs-line-bottom left-aligned">
                <ul class="nav nav-pills" role="tablist">
                    <li class="active">
                      <?=Html::a('Course Info',['simak-jadwal/classroom','id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Pengumuman',['simak-announcement/index','jadwal_id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Model Pembelajaran',['simak-jadwal/silabus','id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Kategori',['simak-topik/index','jadwal_id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Materi',['simak-materi/index','jadwal_id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Tugas',$url_tugas);?>
                    </li>
                   
                    <?php 
                    if(Yii::$app->user->can('Dosen') || Yii::$app->user->can('sekretearis'))
                    {
                     ?>
                    <li class="">
                      <?=Html::a('Capaian',['simak-cpmk/capaian','jadwal_id'=>$jadwal->id],['aria-expanded'=>'true']);?>
                    </li>
                <?php } ?>
                </ul>
            </div>
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
		<div class="row">
		<!-- LEFT COLUMN -->
			<div class="col-md-5">
				<section class="panel">

                    <div class="x_title">
                      <h2>Course Description</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                      <h3 class="green" id="course_name"></h3>

                      <p id="course_description"></p>
                      <br>

                      <div class="project_detail">
                      	
                        <p class="title">Section</p>
                        <p id="course_section"></p>
                        <p class="title">Room</p>
                        <p id="course_room"></p>
						<p class="title">Link</p>
                        <p id="course_link"></p>
                        <p class="title">Enrollment Code</p>
                        <p id="enrollment_code" style="font-size:2em"></p>
                      </div>

                      
                      <br>

                      
                    </div>

                  </section>

				
				<!-- END PROFILE DETAIL -->
			</div>
			<!-- END LEFT COLUMN -->
			<!-- RIGHT COLUMN -->
			<div class="col-md-7">
				<h4 class="heading">Course Detail</h4>
				
				<!-- END AWARDS -->
				<!-- TABBED CONTENT -->
				<div class="custom-tabs-line tabs-line-bottom left-aligned">
					<ul class="nav nav-tabs" role="tablist">
						<li id="tab_classwork" class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Classwork</a></li>
						<!-- <li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Teachers <span class="badge" id="count_teachers"></span></a></li> -->
						<li id="tab_students" ><a href="#tab-bottom-left3" role="tab" data-toggle="tab">Students <span class="badge" id="count_students"></span></a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab-bottom-left1">
						<ul class="list-unstyled activity-timeline" id="course_works">
							
							
						</ul>
						<div class="margin-top-30 text-center"><a href="#" class="btn btn-default">See all activity</a></div>
					</div>
				
					<div class="tab-pane fade" id="tab-bottom-left3">
						<div class="table-responsive">
							<table class="table project-table" id="table-students">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Email</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END TABBED CONTENT -->
			</div>
			<!-- END RIGHT COLUMN -->
		</div>	
	</div>
	
</div>
<?php


yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-lg',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<h1 style="text-align:center;"><span id="enrollment_code_big" style="color: rgb(25,103,210);font-size: 3em;font-family: Roboto,Arial,sans-serif;"></span>
</h1>
<div style="font-size: 1.2em;text-align:center;border-top: .125rem solid #fff;border-color: rgb(66, 133, 244);margin-bottom: -1rem;padding-top:.5rem">

	<a href="javascript:void(0)" title="Click to Copy" id="btn-copy" style="">Copy invite link <i class="fa fa-copy"></i></a>
</div>

<?php
yii\bootstrap\Modal::end();
?>
<?php 

$this->registerJs(' 

document.querySelector(\'#btn-copy\').addEventListener(\'click\' , ()=> {
	let cjc = $("#enrollment_code_big").text()
	let urllink = $("#link_to_classroom").attr("href")
  	navigator.clipboard.writeText(urllink+\'?cjc=\'+cjc).then(
	    function() {
	      	Swal.fire({
		      icon: \'success\',
		      title: \'Yeay\',
		      timer : 1000,
		      text: \'Copied to clipboard\'
		    })
	    }, 
	    function() {
	      /* clipboard write failed */
	      window.alert(\'Opps! Your browser does not support the Clipboard API\')
	    }
  	)
})

$(document).on("click", "#btn-show-enrollment-code", function(e){
    e.preventDefault();
    $("#modal").modal("show")
    
});


getStudents("'.$courseId.'")
getCourseWorks("'.$courseId.'")
getCourseDetail("'.$courseId.'")


function getStudents(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-students']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        

	      },
	      beforeSend: function(){

	      },
	      success: function (data) {
	      	$("#table-students > tbody").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		
	      		var row = "";
	      		$("#count_students").html(hasil.items.length)
	      		$.each(hasil.items, function(i,obj){
	      			row += "<tr>"
	      			row += "<td>"+eval(i+1)+"</td>"
	      			row += "<td>"+obj.name+"</td>"
	      			row += "<td>"+obj.email+"</td>"
	      			
	      			row += "</tr>"
	      		})
	      		$("#table-students > tbody").append(row);
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {
                	
                }
              });
            }


	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {

	            }
	          });
	        }
	      }
	  })
}

function getCourseDetail(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-detail']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        
	        $("#loading").hide()
	      },
	      beforeSend: function(){
	      	$("#loading").show()
	      	
	      },
	      success: function (data) {

	      	$("#loading").hide()
	      	$("#course_works").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		$("#course_description").html(hasil.items.descriptionHeading)
	        	$("#course_name_header").html(hasil.items.name)
	      		$("#enrollment_code_big").html(hasil.items.enrollmentCode)
	      		$("#course_name").html(hasil.items.name)
	      		$("#course_section").html(hasil.items.section)
	      		$("#course_room").html(hasil.items.room)
	      		$("#course_link").html("<a id=\'link_to_classroom\' target=\'_blank\' href=\'"+hasil.items.alternateLink+"\'><i class=\'fa fa-share\'></i> link to classroom</a>")
	      		$("#enrollment_code").html("<a id=\'btn-show-enrollment-code\' href=\'javascript:void(0)\'><i class=\'fa fa-expand\'></i> </a>" +hasil.items.enrollmentCode)
	      		
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {

                }
              });
            }

	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {
	               
	            }
	          });
	        }
	      }
	  })
}

function getCourseWorks(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-works']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        

	      },
	      beforeSend: function(){
	      	Swal.fire({
                title : "Please wait",
                html: "Processing your request...",
                
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
                
            })
	      },
	      success: function (data) {
	      	Swal.close()
	      	$("#course_works").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		
	      		var row = "";
	      		
	      		$.each(hasil.items, function(i,obj){
	      			row += "<li><p><i class=\'fa fa-tasks activity-icon\'></i> &nbsp;"
	      			row += "New "+obj.workType+" "+obj.title+" <a href=\'"+obj.alternateLink+"\'>see assignment here</a> <span class=\'timestamp\'>Due : "+obj.dueDate.day+"-"+obj.dueDate.month+"-"+obj.dueDate.year+" "+obj.dueTime.hours+":"+obj.dueTime.minutes+"</span></p>"
	      			
	      			row += "</li>"
	      		})
	      		$("#course_works").append(row);
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {

                }
              });
            }

	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {
	            	
	            }
	          });
	        }
	      }
	  })
}


var introguide = introJs();
introguide.setOptions({
    exitOnOverlayClick: false,
    steps : [
        {
            intro: "Ini adalah tampilan dari course Classroom Anda. Saat ini, fitur Classroom di SIAKAD hanya untuk membuat course. Sedangkan kegiatan di Classroom, silakan membuka aplikasi Google Classroom.",
            title: "Fitur Google Classroom",
            element : ".panel-profile"
        },
        {
            intro: "Kode Enrollment dipakai oleh mahasiswa untuk masuk ke Classroom.",
            title: "Kode Enrollment Google Classroom",
            element : "#enrollment_code"
        },
        {
            intro: "Ini adalah informasi mengenai data course Classroom yang telah dibuat",
            title: "Basic Info",
            element : "#basic_info"
        },
        {
            intro: "Klik link ini untuk menuju Google Classroom",
            title: "Link to Classroom",
            element : "#link_to_classroom",
           
        },
        {
            intro: "Tab ini berisi daftar Tugas, Materi, atau Diskusi yang dibuat di Classroom",
            title: "Classwork",
            element : "#tab_classwork"
        },
        {
            intro: "Tab ini berisi daftar mahasiswa yang sudah enroll di Classroom. Ingat <strong>mahasiswa hanya bisa enroll ke Classroom dengan email @unida.gontor.ac.id</strong>",
            title: "Students",
            element : "#tab_students"
        },
    ]
});

var doneTour = localStorage.getItem(\'evt_classroom_detail\') === \'Completed\';

if(!doneTour) {
    introguide.start()

    introguide.oncomplete(function () {
        localStorage.setItem(\'evt_classroom_detail\', \'Completed\');
        Swal.fire({
          title: \'Ulangi Langkah Fitur ini ?\',
          text: "",
          icon: \'warning\',
          showCancelButton: true,
          width:\'35%\',
          confirmButtonColor: \'#3085d6\',
          cancelButtonColor: \'#d33\',
          confirmButtonText: \'Ya, ulangi lagi!\',
          cancelButtonText: \'Tidak, sudah cukup\'
        }).then((result) => {
          if (result.value) {
            introguide.start();
            localStorage.removeItem(\'evt_classroom_detail\');
          }

        });
    });

}
', \yii\web\View::POS_READY);

?>