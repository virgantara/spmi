<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presensi Ujian ' . ($kode == 2 ? 'Tengah' : 'Akhir') . ' Semester';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : '';
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';

$tmp = \app\models\SimakMasterprogramstudi::find();

if (Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('theCreator')) {
  $tmp->andWhere(['kode_prodi' => Yii::$app->user->identity->prodi]);
}

$semester = !empty($_GET['semester']) ? $_GET['semester'] : '';
$listProdi = $tmp->all();
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';
$jadwal_id = !empty($_GET['jadwal_id']) ? $_GET['jadwal_id'] : '';
?>

<h3 class="page-title"><?= $this->title; ?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
    <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/presensi', 'kode' => $kode],
      'options' => [
        'id' => 'form_validation',
      ]
    ]); ?>
    <div class="form-group">
      <label class="control-label ">Tahun Akademik</label>
      <?= Html::dropDownList('tahun_id', $tahun_id, \yii\helpers\ArrayHelper::map($listTahun, 'tahun_id', 'nama_tahun'), ['class' => 'form-control', 'id' => 'tahun_id']) ?>
    </div>

    <div class="form-group">
      <label class="control-label ">Prodi</label>
      <?= Select2::widget([
        'name' => 'prodi',
        'value' => $prodi,
        'data' => \yii\helpers\ArrayHelper::map($listProdi, 'kode_prodi', 'nama_prodi'),
        'options' => [
          'id' => 'prodi_id',
          'placeholder' => Yii::t('app', '- Pilih Prodi -'),

        ],
        'pluginOptions' => [
          'initialize' => true,
          'allowClear' => true,
        ],
      ]) ?>

    </div>
    <div class="form-group">
      <label class="control-label ">Semester</label>
      <?= Html::dropDownList('semester', $semester, \app\helpers\MyHelper::getListSemester(), ['id' => 'semester', 'class' => 'form-control', 'prompt' => '- Pilih Semester -']) ?>
    </div>
    <div class="form-group">
      <label class="control-label ">Mata kuliah</label>
      <?= DepDrop::widget([
        'name' => 'kode_mk',
        'value' => $kode_mk,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'kode_mk'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester'],
          'initialize' => true,
          'placeholder' => '- Pilih Mata Kuliah -',
          'url' => Url::to(['/simak-mastermatakuliah/submatkul'])
        ]
      ]) ?>


    </div>
    <div class="form-group">
      <label class="control-label ">Kelas</label>
      <?= DepDrop::widget([
        'name' => 'jadwal_id',
        'value' => $jadwal_id,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'jadwal_id'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester', 'kode_mk'],
          'initialize' => true,
          'placeholder' => '- Pilih Kelas -',
          'url' => Url::to(['/simak-jadwal/subjadwal'])
        ]
      ]) ?>


    </div>
    <div class="form-group clearfix">
      <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
      <a target="_blank" class="btn btn-success" href="<?= Url::to(['simak-jadwal/export-presensi', 'id' => $jadwal_id, 'kode' => $kode]); ?>"><i class="fa fa-file-pdf-o"></i> Download Presensi Ujian</a>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar Presensi Ujian</h3>

      </div>
      <div class="panel-body ">

        <div class="table-responsive">
          <table class="table table-hover table-bordered table-striped">
            <thead>
              <tr>
                <th rowspan="2">#</th>
                <th rowspan="2">NIM</th>
                <th rowspan="2">Mahasiswa</th>
                <th colspan="5" class="text-center">Nilai</th>
                <th rowspan="2" class="text-center">TTD</th>

              </tr>
              <tr>
                <th>Harian</th>
                <th>Tugas</th>
                <th>UTS</th>
                <th>UAS</th>
                <th>Nilai<br>Akhir</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($results as $q => $m) {

                $hadir = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 1])->count();

                $sakit = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 2])->count();

                $izin = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 3])->count();

                $remaining = $sakit + $izin >= 3 ? 3 : $sakit + $izin;

                $absens = $hadir + $remaining;

                $persen = 0;
                if (!empty($m->kodeJadwal) && $m->kodeJadwal->jumlah_tatap_muka > 0)
                  $persen = round($absens / $m->kodeJadwal->jumlah_tatap_muka * 100, 2);

                $is_tercekal_akademik = $persen < 75 && $kode == 3;



                $tahfidz = \app\models\SimakTahfidzNilai::find()->where([
                  'tahun_id' => $m->tahun_akademik,
                  'nim' => $m->mahasiswa
                ])->one();

                $is_tercekal_tahfidz = empty($tahfidz) || (!empty($tahfidz) && $tahfidz->nilai_angka <= 2);
                $label_tercekal = [];

                if ($is_tercekal_akademik)
                  $label_tercekal[] = 'Akademik';

                if ($is_tercekal_tahfidz)
                  $label_tercekal[] = 'Tahfidz';

                $is_tercekal_adm = \app\models\SimakMastermahasiswa::isTercekalADM($m->mahasiswa);
                if ($is_tercekal_adm)
                  $label_tercekal[] = 'ADM';


                $subtotal = 0;
                foreach ($indukKegiatan as $induk) {
                  foreach ($induk->simakJenisKegiatans as $jk) {
                    $km = \app\models\SimakKegiatanMahasiswa::find()->where([
                      'id_jenis_kegiatan' => $jk->id,
                      'nim' => $m->mahasiswa,
                      'tahun_akademik' => $m->tahun_akademik,
                      'is_approved' => 1
                    ]);
                    $sub = $km->sum('nilai');
                    if ($sub >= $jk->nilai_maximal) {
                      $subtotal += $jk->nilai_maximal;
                    } else {
                      $subtotal += $sub;
                    }
                  }
                }

                $is_tercekal_akpam = $subtotal < $tahun_akademik_aktif->nilai_lulus_akpam;

                if ($is_tercekal_akpam)
                  $label_tercekal[] = 'AKPAM';
              ?>
                <tr>
                  <td><?= $q + 1; ?></td>
                  <td><?= $m->mahasiswa0->nim_mhs; ?></td>
                  <td><?= $m->mahasiswa0->nama_mahasiswa; ?></td>
                  <?php


                  if (count($label_tercekal) > 0) {
                    echo '<td colspan="6" class="text-center" style="color:red">Tercekal ';
                    echo ': ' . implode(', ', $label_tercekal);
                    echo '</td>';
                  } else {
                  ?>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  <?php
                  }
                  ?>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>