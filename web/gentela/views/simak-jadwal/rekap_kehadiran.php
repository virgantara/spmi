<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\assets\IntroAsset;
IntroAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Jumlah Pertemuan Per Dosen ';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';
// $listDosen = \app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all();
?>
<style type="text/css">
  .ui-autocomplete { z-index:2147483647; }

  .modal-dialog{
      top: 50%;
      margin-top: -250px; 
  }

</style>
<h3 class="page-title">Jadwal</h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/rekap-kehadiran'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>

          
          
      </div>
      <?php 

      if(Yii::$app->user->can('sekretearis') || Yii::$app->user->can('sub baak'))
      {
        if(Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('baak'))
        {
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->where(['kode_prodi' => Yii::$app->user->identity->prodi])->all(),'kode_prodi','nama_prodi');
        }

        else{
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->all(),'kode_prodi','nama_prodi');
          // $listDosen = \yii\helpers\ArrayHelper::map($listDosen,'id','nama_dosen');
        }
        
      ?>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => !empty($_GET['prodi']) ? $_GET['prodi'] : '',
            'data' => $listProdi,
            'options'=>['id'=>'prodi_id','placeholder'=>Yii::t('app','- Pilih Prodi -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

      </div>
      <div class="form-group">
          <label class="control-label ">Dosen</label>
          
          <?php
          echo DepDrop::widget([
              'name' => 'dosen',      
              'data' => [],
              'value' => !empty($_GET['dosen']) ? $_GET['dosen'] : '',
              'type'=>DepDrop::TYPE_SELECT2,
              'options'=>['id'=>'dosen'],
              'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
              'pluginOptions'=>[
                  'depends'=>['tahun_id','prodi_id'],
                  'initialize' => true,
                  'placeholder'=>'- Pilih Dosen -',
                  'url'=>Url::to(['/simak-masterdosen/subdosen'])
              ]
            ]);
          ?>
          
      </div>
      <?php 
    }


      ?>
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
        
     
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>
        
      </div>
      <div class="panel-body ">

        <?php 

        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }

?>
    


   
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                
                <th>Mata Kuliah</th>
                <th>SKS</th>
                <th><strong>Dosen</strong><br>Dosen (Ber-NIDN)</th>
                
                <th>Kelas</th>
                <th>Waktu</th>
                <th>Prodi</th>
                <th>Jml Pertemuan</th>
              
                
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 0;

              $prodi = Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen') ? Yii::$app->user->identity->prodi : '';
              foreach($results as $q => $m)
              {
                $total_pertemuan = $m['total_pertemuan'];
                $m = $m['item'];

               
                $mk = \app\models\SimakMatakuliah::find()->where([
                  'kode_mk' => $m->kode_mk,

                ])->one();
               

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m->kampus])->one();

                $dosen = SimakMasterdosen::find()->where([
                  'nidn' => $m->kode_dosen,
                ])->one();

                $dosenNidn = SimakMasterdosen::find()->where([
                  'nidn' => $m->kode_pengampu_nidn,
                ])->one();
                $i++;
              ?>
              <tr>
                <td><?=$i;?></td>
                
                
                <td>[<?=$m->kode_mk;?>] <?=!empty($mk) ? $mk->nama_mk: '-';?></td>
                <td><?=!empty($mk) ? $mk->sks_mk : '-';?></td>
                <td>
                  <strong><?=$dosen->nama_dosen;?></strong>
                 
                </td>
                
                <td><?=!empty($kampus) ? $kampus->nama_kampus : '-';?> - <?=$m->kelas;?></td>
                <td><?=ucwords(strtolower($m->hari));?>, <?=$m->jam;?></td>
                
                <td><?=!empty($mk) ? $mk->prodi0->nama_prodi : '-';?></td>
                <td>
                  <span style="color:<?=$total_pertemuan < 7 ? 'red' : 'black';?>"><?=$total_pertemuan;?></span>
                </td>
                
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>

      </div>
    </div>
   </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Pengampu Ber-NIDN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <input type="hidden" id="jadwal_id" >
       <input type="hidden" id="dosen_id" >
<?= Html::textInput('nama_pejabat','',['class'=>'form-control','id'=>'nama_pejabat','placeholder'=>'Ketik Nama Dosen']) ?>
      <?php 
      AutoComplete::widget([
      'name' => 'nama_pejabat',
      'id' => 'nama_pejabat',
      'clientOptions' => [
      'source' => Url::to(['simak-masterdosen/ajax-cari-dosen']),
      'autoFill'=>true,
      'minLength'=>'1',
      'select' => new JsExpression("function( event, ui ) {
          $('#dosen_id').val(ui.item.nidn);
          
       }")],
      'options' => [
          // 'size' => '40'
      ]
   ]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-save">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<?php 

$this->registerJs(' 
$(document).on("click",".btn-edit-dosen",function(e){
  e.preventDefault()

  $("#exampleModal").modal("show");

  $("#jadwal_id").val($(this).data("item"));


})


$(document).on("click",".link-sync-classroom",function(e){
  e.preventDefault()
  var obj = new Object;
  obj.id = $(this).data("item")
  $.ajax({
      type: \'POST\',
      url: "'.Url::to(['simak-jadwal/ajax-classroom']).'",
      data: {
          dataPost : obj
      },
      async: true,
      error : function(e){
        Swal.hideLoading();
        

      },
      beforeSend: function(){
        Swal.showLoading();
      },
      success: function (data) {
        var hasil = $.parseJSON(data)
        if(hasil.code == 200){
      
          Swal.fire({
            title: \'Yeay!\',
            icon: \'success\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }

        else{
          Swal.fire({
            title: \'Oops!\',
            icon: \'error\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }
      }
  })
})

$(document).on("click","#btn-sync-classroom",function(e){
  e.preventDefault()

  var obj = new Object
  obj.tahun_id = $("#tahun_id").val()
  $.ajax({
      type: \'POST\',
      url: "'.Url::to(['simak-jadwal/ajax-sync-classroom']).'",
      data: {
          dataPost : obj
      },
      async: true,
      error : function(e){
        Swal.close();
        

      },
      beforeSend: function(){
        Swal.showLoading();
      },
      success: function (data) {
        Swal.close();
        var hasil = $.parseJSON(data)
        if(hasil.code == 200){
      
          Swal.fire({
            title: \'Yeay!\',
            icon: \'success\',
            html: "<div class=\'text-left\'>"+hasil.message+"</div>"
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }

        else{
          Swal.fire({
            title: \'Oops!\',
            icon: \'error\',
            html: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }
      }
  })
})

$(document).on("click","#btn-save",function(e){
  e.preventDefault()

  var obj = new Object;
  obj.kode_pengampu_nidn = $("#dosen_id").val();
  obj.jid = $("#jadwal_id").val();
  
  $.ajax({
      type: \'POST\',
      url: "'.Url::to(['simak-jadwal/ajax-update-pengampu']).'",
      data: {
          dataPost : obj
      },
      async: true,
      error : function(e){
        Swal.hideLoading();
        

      },
      beforeSend: function(){
        Swal.showLoading();
      },
      success: function (data) {
        var hasil = $.parseJSON(data)
        if(hasil.code == 200){
      
          Swal.fire({
            title: \'Yeay!\',
            icon: \'success\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }

        else{
          Swal.fire({
            title: \'Oops!\',
            icon: \'error\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }
      }
  })
})


var introguide = introJs();
introguide.setOptions({
    exitOnOverlayClick: false,
    steps : [
        {
            intro: "Assalamualaikum. Saat ini, SIAKAD ada fitur baru, yaitu sinkronisasi dengan Google Classroom. Anda bisa membuat course secara langsung dan banyak tanpa harus membuka aplikasi Googlel Classroom. Yuk ikuti tutorialnya.",
            title: "Fitur Google Classroom",
            element : "#form_validation"
        },
        {
            intro: "Tombol ini akan menjalankan proses pembuatan course di Classroom secara otomatis. Cukup menekan tombol ini sekali saja, maka semua course di jadwal Anda akan masuk ke Classroom. Tapi ingat, <strong>syaratnya email yang Anda registrasikan di SIAKAD adalah harus email @unida.gontor.ac.id</strong>",
            title: "Tombol Sync to Google Classroom",
            element : "#btn-sync-classroom"
        },
        {
            intro: "Tombol ini berfungsi untuk melihat course yang sudah diarsipkan di Classroom",
            title: "Archived Classroom",
            element : "#btn-archived"
        },
        {
            intro: "Klik ini untuk melihat menu",
            title: "Actions",
            element : "#btn-action-group",
            onbeforechange: function() {
              $("#btn-action-group").trigger("click")
            }
        },
        {
            intro: "Klik ini untuk melihat Classroom Anda",
            title: "Classroom",
            element : ".classroom_li"
        },
    ]
});
var doneTour = localStorage.getItem(\'evt_classroom\') === \'Completed\';

if(!doneTour) {
    introguide.start()

    introguide.oncomplete(function () {
        localStorage.setItem(\'evt_classroom\', \'Completed\');
        Swal.fire({
          title: \'Ulangi Langkah Fitur ini ?\',
          text: "",
          icon: \'warning\',
          showCancelButton: true,
          width:\'35%\',
          confirmButtonColor: \'#3085d6\',
          cancelButtonColor: \'#d33\',
          confirmButtonText: \'Ya, ulangi lagi!\',
          cancelButtonText: \'Tidak, sudah cukup\'
        }).then((result) => {
          if (result.value) {
            introguide.start();
            localStorage.removeItem(\'evt_classroom\');
          }

        });
    });

}
', \yii\web\View::POS_READY);

?>