<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */
/* @var $form yii\widgets\ActiveForm */



?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>

<?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
?>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Hari</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'hari',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jam</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'jam',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kode mk</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kode_mk',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>

                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kode dosen</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kode_dosen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kode Dosen Ber-NIDN</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kode_dosen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>
            <?= Html::textInput('nama_pejabat',$pejabat,['class'=>'form-control','id'=>'nama_pejabat','placeholder'=>'Ketik Nama Dosen']) ?>
            <?php 
            AutoComplete::widget([
            'name' => 'nama_pejabat',
            'id' => 'nama_pejabat',
            'clientOptions' => [
            'source' => Url::to(['simak-masterdosen/ajax-cari-dosen']),
            'autoFill'=>true,
            'minLength'=>'1',
            'select' => new JsExpression("function( event, ui ) {
                $('#kode_dosen').val(ui.item.nidn);
                
             }")],
            'options' => [
                // 'size' => '40'
            ]
         ]); ?>

            <?= $form->field($model, 'kode_dosen',['options' => ['tag' => false]])->hiddenInput(['id'=>'kode_dosen'])->label(false) ?>
            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Semester</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'semester',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kelas</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kelas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
             <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Fakultas</label>
            <div class="col-sm-9">
            
            <?= $form->field($model, 'fakultas',['options' => ['tag' => false]])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\app\models\SimakMasterfakultas::find()->orderBy(['nama_fakultas'=>SORT_ASC])->all(),'kode_fakultas','nama_fakultas'),

            'options'=>['id'=>'fakultas_id','placeholder'=>Yii::t('app','- Pilih Fakultas -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Prodi</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'prodi',['options' => ['tag' => false]])->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'subcat-id'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['fakultas_id'],
                        'initialize' => true,
                        'placeholder'=>'- Pilih Prodi -',
                        'url'=>Url::to(['/simak-masterprogramstudi/subprodi'])
                    ]
                ])->label(false) ?>

            </div>
        </div>
            
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kd ruangan</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kd_ruangan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tahun akademik</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'tahun_akademik',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kuota kelas</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kuota_kelas',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kelas</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'kampus',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Presensi</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'presensi',['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Materi</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'materi',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot formatif</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_formatif',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot uts</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_uts',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot uas</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_uas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot harian1</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_harian1',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot harian</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_harian',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jadwal temp</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'jadwal_temp_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            
            </div>
        </div>
             
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
