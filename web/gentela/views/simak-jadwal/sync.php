<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sinkronisasi Kelas Kuliah';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : $dosen;
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
?>

<h3 class="page-title"><?=$this->title;?></h3>
<div class="alert alert-danger">
  <h1><i class="fa fa-warning"></i>  Perhatian</h1>
  <ul>
  <li>Data dosen di sini hanyalah dosen yang sudah ber-NIDN dan SUDAH tersinkron dengan PDDIKTI. Apabila data tersebut tidak muncul, silakan diubah dulu data pengampu Jadwal pada kolom Dosen Pengampu Ber-NIDN dan sinkronkan dengan PDDIKTI dari SIMPEG.
    </li>
    <li>
      Sebelum sinkronisasi, <strong>PASTIKAN</strong> warna merah pada kolom Keterangan sudah hilang
    </li>
    <li>
      Tombol <button class="btn btn-info"><i class="fa fa-refresh"></i> Sync</button> tidak akan muncul jika warna merah pada kolom Keterangan belum hilang.
    </li>
  </ul>
</div>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/sync'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); 

    

    ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>

          
          
      </div>
      <?php 

      $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->orderBy(['kode_fakultas'=>SORT_ASC,'nama_prodi'=>SORT_ASC])->all(),'kode_prodi',function($data){
        return $data->nama_prodi.' - ['.$data->kode_prodi.']';
      });
          $listDosen = \yii\helpers\ArrayHelper::map(\app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all(),'id','nama_dosen');
        
      ?>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => $prodi,
            'data' => $listProdi,
            'options'=>['id'=>'prodi_id','placeholder'=>Yii::t('app','- Pilih Prodi -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

      </div>
      <div class="form-group">
          <label class="control-label ">Dosen</label>
          
          <?php
          echo DepDrop::widget([
              'name' => 'dosen',      
              'data' => $listDosen,
              'value' => !empty($_GET['dosen']) ? $_GET['dosen'] : '',
              'type'=>DepDrop::TYPE_SELECT2,
              'options'=>['id'=>'dosen'],
              'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
              'pluginOptions'=>[
                  'depends'=>['tahun_id','prodi_id'],
                  'initialize' => true,
                  'placeholder'=>'- Pilih Dosen -',
                  'url'=>Url::to(['/simak-masterdosen/subdosen-bernidn'])
              ]
            ]);
          ?>
          
      </div>
      <?php 
    


      ?>
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
       
        <?php 
        if(Yii::$app->user->can('baak'))
        {
        echo Html::a('<i class="fa fa-refresh"></i> Bulk Sync','javascript:void(0)',['class'=>'btn btn-danger','id'=>'btn-bulk-sync']);
        }
        ?>
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>
      </div>
      <div class="panel-body ">

       <?php 
        foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
          echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
        }
        ?>
        <div class="table-responsive">
          <table class="table table-hover text-nowrap table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Data Mata Kuliah</th>
                <th>Prodi</th>
                <th>Jml Mhs</th>
                <th>Status</th>
                <th>Keterangan</th>
                <th>Aksi</th>
                
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 0;

              // $prodi = Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen') ? Yii::$app->user->identity->prodi : '-';
              foreach($results as $q => $m)
              {
                $mk = \app\models\SimakMatakuliah::find()->where([
                  'prodi' => $m->prodi,
                  'kode_mk' => $m->kode_mk
                ])->one();

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m->kampus])->one();
                $i++;
              ?>
              <tr>
                <td><?=$i;?></td>
                <td>
                  [<?=$m->kode_mk;?>] <strong><?=!empty($mk) ? $mk->nama_mk: '-';?></strong> - <?=!empty($mk) ? $mk->sks_mk : '-';?> sks
                  <br>
                  <?=!empty($kampus) ? $kampus->nama_kampus : '-';?> - <?=$m->kelas;?>
                  <br><strong style="color:green"><?=ucwords(strtolower($m->hari));?>, <?=$m->jam;?></strong>
                </td>
                <td><?=!empty($mk) ? $mk->prodi0->nama_prodi : '-';?></td>
                <td>
                  <?=$total_mhs[$m->id];?>
                </td>
                <td><?=strlen($m->kode_feeder) > 10 ? '<span title="'.$m->kode_feeder.'" class="label label-success"><i class="fa fa-check"></i> Synced</span>' : '<span class="label label-danger"><i class="fa fa-ban"></i> Not Synced</span>';?></td>
                <td>
                 <?php 

                 $counter_invalid = 0;
                echo '<ul>';
                $list_vars = ['a_selenggara_pditt','bahasan_case','tgl_mulai_koas','tgl_selesai_koas','mode_kuliah'];
                foreach($m->attributes as $q => $attr){
                    if(in_array($q,$list_vars)){
                        if(!isset($attr)){
                            echo '<li style="color:red">'.$m->getAttributeLabel($q). ' belum diisi';
                            echo '. Klik di '.Html::a('sini',['simak-jadwal/silabus','id'=>$m->id]).' untuk mengisi';
                            echo '</li>';
                            $counter_invalid++;
                        }
                    }
                }

                $counter_invalid_mhs = 0;

                $list_mhsvars = ['id_reg_pd','kode_pd'];
                foreach($m->listPeserta as $pst){
                    $mhs = $pst->mahasiswa0;
                    if(!empty($mhs->invalidAttributes))
                      $counter_invalid_mhs++;


                    // if($this->apakah_4_tahun == '2'){
                    //     $result .= "<li style=\"color:red;font-style:italic\"><strong>Mahasiswa ini TIDAK melanjutkan hingga S1</strong></li>";
                    // }
                }

                if($counter_invalid_mhs > 0){
                    echo '<li style="color:red">Ada '.$counter_invalid_mhs. ' mhs belum lengkap datanya';
                    echo '. Klik di '.Html::a('sini',['simak-jadwal/view','id'=>$m->id]).' untuk mengecek';
                    echo '</li>';
                }



                echo '</ul>';
                  ?>
                  
                  
                </td>
                <td>

                  <?php
                  if($counter_invalid == 0 && $counter_invalid_mhs == 0)
                    echo Html::a('<i class="fa fa-refresh"></i> Sync','#',['data-item'=>$m->id,'class'=>'btn btn-info btn-sync-manual']);

                  ?>
                  <?=Html::a('<i class="fa fa-list"></i> View',['simak-jadwal/view','id'=>$m->id],['class'=>'btn btn-success']);?>
                </td>
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
   </div>
</div>


<?php 

$this->registerJs(' 


$(document).on(\'click\',\'.btn-sync-manual\',function(e){
    e.preventDefault();
    var obj = new Object;
    obj.id = $(this).data("item");
    
    $.ajax({
        type: \'POST\',
        url: "'.Url::to(['simak-jadwal/ajax-sync-manual']).'",
        data: {
            dataPost : obj
        },
        async: true,
        error : function(e){
          Swal.close();
          

        },
        beforeSend: function(){
          Swal.showLoading();
        },
        success: function (data) {
          Swal.close();
          var hasil = $.parseJSON(data)
          if(hasil.code == 200){
        
            Swal.fire({
              title: \'Yeay!\',
              icon: \'success\',
              text: hasil.message
            }).then((result) => {
              if (result.value) {
                location.reload(); 
              }
            });
          }

          else{
            Swal.fire({
              title: \'Oops!\',
              icon: \'error\',
              text: hasil.message
            });
          }
        }
    })
    
    
});


$(document).on(\'click\',\'#btn-bulk-sync\',function(e){
    e.preventDefault();
    var obj = new Object;
    obj.prodi = $("#prodi_id").val();
    obj.tahun_akademik = $("#tahun_id").val();
    obj.dosen = $("#dosen").val();
    $.ajax({
        type: \'POST\',
        url: "'.Url::to(['sync/ajax-bulk-sync-kelas']).'",
        data: {
            dataPost : obj
        },
        async: true,
        error : function(e){
          Swal.hideLoading();
          

        },
        beforeSend: function(){
          Swal.showLoading();
        },
        success: function (data) {
          var hasil = $.parseJSON(data)
          if(hasil.code == 200){
        
            Swal.fire({
              title: \'Yeay!\',
              icon: \'success\',
              text: hasil.message
            }).then((result) => {
              if (result.value) {
                location.reload(); 
              }
            });
          }

          else{
            Swal.fire({
              title: \'Oops!\',
              icon: \'error\',
              text: hasil.message
            }).then((result) => {
              if (result.value) {
                location.reload(); 
              }
            });
          }
        }
    })
    
    
});


', \yii\web\View::POS_READY);

?>