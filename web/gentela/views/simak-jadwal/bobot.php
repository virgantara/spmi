<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SimakJam;
/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Bobot Materi';

?>
<h3><?= Html::encode($this->title) ?></h3>
 <?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'form_validation',
    ]
]); ?>

<?=$form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);?>

<div class="row">
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                <div class="custom-tabs-line tabs-line-bottom left-aligned">
          <ul class="nav" role="tablist">
            <li class="">
              <?=Html::a('Presensi',['simak-datakrs/absensi','id'=>$model->id,'tahun'=>$model->tahun_akademik],['aria-expanded'=>'false']);?>
            </li>
            <li class="">
              <?=Html::a('Jurnal',['simak-jurnal/index','jid'=>$model->id,'tahun'=>$model->tahun_akademik],['aria-expanded'=>'true']);?>
            </li>
            <li class="active">
              <?=Html::a('Bobot',['simak-jadwal/bobot','id'=>$model->id,'tahun'=>$model->tahun_akademik],['aria-expanded'=>'true']);?>
            </li>
            <li class="">
              <?=Html::a('Nilai',['simak-datakrs/nilai','id'=>$model->id,'tahun'=>$model->tahun_akademik],['aria-expanded'=>'true']);?>
            </li>
            <li class="">
              <?= Html::a('Riwayat perizinan', ['simak-datakrs/mahasiswa-izin', 'id' => $model->id, 'tahun' => $model->tahun_akademik], ['aria-expanded' => 'true']); ?>
          </ul>
        </div>
            </div>
            <div class="panel-body ">
        
         
        <div class="form-group">
            <label class="control-label no-padding-right">Hari</label>
            <div class="">
            <?= $form->field($model, 'hari',['options' => ['tag' => false]])->dropDownList([
                'SENIN'=>'SENIN','SELASA'=>'SELASA','RABU'=>'RABU','KAMIS'=>'KAMIS','SABTU'=>'SABTU','AHAD'=>'AHAD'
            ],['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
        <div class="form-group">
            <label class="control-label no-padding-right">Jam </label>
            <div class="">
            <?= $form->field($model, 'jam',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(SimakJam::find()->all(),'jam_mulai','jam_mulai'),['class'=>'form-control','maxlength' => true])->label(false) ?>
            <small>Format Jam hh:mm
            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Kode mk</label>
            <div class="">
            <?= $form->field($model, 'kode_mk',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Kode dosen</label>
            <div class="">
            <?= $form->field($model, 'kode_dosen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Semester</label>
            <div class="">
            <?= $form->field($model, 'semester',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Kelas</label>
            <div class="">
            <?= $form->field($model, 'kelas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Fakultas</label>
            <div class="">
            <?= $form->field($model, 'fakultas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Prodi</label>
            <div class="">
            <?= $form->field($model, 'prodi',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="control-label no-padding-right">Kd ruangan</label>
            <div class="">
            <?= $form->field($model, 'kd_ruangan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
         <div class="form-group">
            <label class="control-label no-padding-right">Tahun akademik</label>
            <div class="">
            <?= $form->field($model, 'tahun_akademik',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required','readonly'=>'readonly'])->label(false) ?>

            
            </div>
        </div>
    
   
    
                

           </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body ">
        
       
        <?= $form->field($model, 'id',['options' => ['tag' => false]])->hiddenInput()->label(false) ?>
        <div class="form-group">
            <?= $form->field($model, 'jumlah_tatap_muka',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required']) ?>
            
        </div>
        <div class="form-group">
            <?= $form->field($model, 'bobot_harian',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required']) ?>

            
            
        </div>
                <div class="form-group">
            <?= $form->field($model, 'bobot_formatif',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required']) ?>

            
        </div>
        <div class="form-group">
            <?= $form->field($model, 'bobot_uts',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required']) ?>

            
        </div>
                <div class="form-group">
            <?= $form->field($model, 'bobot_uas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required']) ?>

        </div>
    
                


                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
       
           </div>
        </div>
    </div>
</div>

 <?php ActiveForm::end(); ?>
