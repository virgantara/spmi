<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Google Classroom';
$this->params['breadcrumbs'][] = $this->title;
// print_r($course);exit;
// $listDosen = \app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all();
?>

<div class="panel panel-profile">
	<div class="panel-heading">
		<h3 class="panel-title"><?=$this->title;?></h3>		
	</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>No</th>
					<th>ID</th>
					<th>Name</th>
					<th>Section</th>
					<th>Room</th>
					<th>Course State</th>
					<th>Alternate Link</th>
				</tr>
			</thead>
			<tbody>
				<?php 
                foreach($courses as $q=>$c)
                {

				?>

				<tr>
					<td><?=$q+1;?></td>
					<td><?=$c->id;?></td>
					<td><?=$c->name;?></td>
					<td><?=$c->section;?></td>
					<td><?=$c->room;?></td>
					<td><?=$c->courseState;?></td>
					<td><?=Html::a('link',$c->alternateLink);?></td>
				</tr>
				<?php 
                  }
				?>
			</tbody>
		</table>
	</div>
</div>