<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use app\assets\IntroAsset;
IntroAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Info mata kuliah '.(!empty($mk) ? '['.$mk->kode_mk.'] '.$mk->nama_mk : '-');
$this->params['breadcrumbs'][] = $this->title;
$url_tugas = ['simak-tugas/tugasku','jadwal_id'=>$jadwal->id];
$url_silabus = ['simak-jadwal/view-silabus','id'=>$jadwal->id];

if(Yii::$app->user->can('Dosen') || Yii::$app->user->can('sekretearis')) {
    $url_tugas = ['simak-tugas/index','jadwal_id'=>$jadwal->id];
    $url_silabus = ['simak-jadwal/silabus','id'=>$jadwal->id];
}  
?>
<p>
    <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', 'javascript:history.back()', ['class' => 'btn btn-danger']) ?>
</p>
<h3><?= Html::encode($this->title) ?></h3>

<div class="panel panel-profile">
	<div class="custom-tabs-line tabs-line-bottom left-aligned">
                <ul class="nav nav-pills" role="tablist">
                    <li class="active">
                      <?=Html::a('Course Info',['simak-jadwal/course-info','id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Pengumuman',['simak-announcement/index','jadwal_id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Model Pembelajaran',['simak-jadwal/view-silabus','id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Materi',['simak-materi/index','jadwal_id'=>$jadwal->id]);?>
                    </li>
                    <li class="">
                      <?=Html::a('Tugas',$url_tugas);?>
                    </li>
                    <li class="">
                      <?=Html::a('Diskusi','#',['aria-expanded'=>'true']);?>
                    </li>
                    <?php 
                    if(Yii::$app->user->can('Dosen') || Yii::$app->user->can('sekretearis'))
                    {
                     ?>
                    <li class="">
                      <?=Html::a('CPMK',['simak-cpmk/index','matkul_id'=>$mk->id,'jadwal_id' => $jadwal->id],['aria-expanded'=>'true']);?>
                    </li>
                    <li class="">
                      <?=Html::a('Capaian',['simak-cpmk/capaian','jadwal_id'=>$jadwal->id],['aria-expanded'=>'true']);?>
                    </li>
                <?php } ?>
                </ul>
            </div>
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
		<div class="row">

		<!-- LEFT COLUMN -->
			<div class="col-md-5">
				<div id="loading" style="display:none" class="text-center">
	                <img width="40px" src="<?=Yii::$app->view->theme->baseUrl;?>/assets/img/loading.gif">
	                <strong style="color:blue;">Sedang mengambil data Course dari Google Classroom...</strong>
	             </div>
				<!-- PROFILE HEADER -->
				<div class="profile-header">
					<div class="overlay"></div>
					<div class="profile-main">
						<h3 class="name" id="course_name_header"></h3>
						<span class="online-status status-available"></span>
					</div>
					<div class="profile-stat">
						<div class="row">
							<div class="col-md-12 stat-item">
								Enrollment Code<span id="enrollment_code" style="font-family: Roboto,Arial,sans-serif;"></span>
							</div>
							
						</div>
					</div>
				</div>
				<!-- END PROFILE HEADER -->
				<!-- PROFILE DETAIL -->
				<div class="profile-detail" id="basic_info">
					<div class="profile-info">
						
						<h4 class="heading">Basic Info </h4>

						<ul class="list-unstyled list-justify">
							<li>Course name <span id="course_name"></span></li>
							<li>Section <span id="course_section"></span></li>
							<li>Room <span id="course_room"></span></li>
							<li>Link <span id="course_link"></span></li>
							<li>CID <span id="course_id"><?=$jadwal->classroom_id;?></span></li>
						</ul>
					</div>
					
					<div class="profile-info">
						<h4 class="heading">About</h4>
						<p id="course_description"></p>
					</div>
				</div>
				<!-- END PROFILE DETAIL -->
			</div>
			<!-- END LEFT COLUMN -->
			<!-- RIGHT COLUMN -->
			<div class="col-md-7">
				<h4 class="heading">Course Detail</h4>
				
				<!-- END AWARDS -->
				<!-- TABBED CONTENT -->
				<div class="custom-tabs-line tabs-line-bottom left-aligned">
					<ul class="nav" role="tablist">
						<li id="tab_classwork" class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Classwork</a></li>
						<!-- <li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Teachers <span class="badge" id="count_teachers"></span></a></li> -->
						<li id="tab_students" ><a href="#tab-bottom-left3" role="tab" data-toggle="tab">Classmates <span class="badge" id="count_students"></span></a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab-bottom-left1">
						<ul class="list-unstyled activity-timeline" id="course_works">
							
							
						</ul>
						<div class="margin-top-30 text-center"><a href="#" class="btn btn-default">See all activity</a></div>
					</div>
				
					<div class="tab-pane fade" id="tab-bottom-left3">
						<div class="table-responsive">
							<table class="table project-table" id="table-students">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Email</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END TABBED CONTENT -->
			</div>
			<!-- END RIGHT COLUMN -->
		</div>	
	</div>
	
</div>

<?php 

$this->registerJs(' 


getCourseDetail("'.$courseId.'")
getStudents("'.$courseId.'")
getCourseWorks("'.$courseId.'")

function getCourseDetail(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-detail']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        
	        $("#loading").hide()
	      },
	      beforeSend: function(){
	      	$("#loading").show()
	      },
	      success: function (data) {
	      	$("#loading").hide()
	      	$("#course_works").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		$("#course_description").html(hasil.items.descriptionHeading)
	        	$("#course_name_header").html(hasil.items.name)
	      		$("#enrollment_code").html(hasil.items.enrollmentCode)
	      		$("#course_name").html(hasil.items.name)
	      		$("#course_section").html(hasil.items.section)
	      		$("#course_room").html(hasil.items.room)
	      		$("#course_link").html("<a id=\'link_to_classroom\' target=\'_blank\' href=\'"+hasil.items.alternateLink+"\'><i class=\'fa fa-share\'></i> link to classroom</a>")
	      		
	      		
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {
                }
              });
            }

	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {
	            }
	          });
	        }
	      }
	  })
}


function getStudents(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-students']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        

	      },
	      beforeSend: function(){

	      },
	      success: function (data) {
	      	$("#table-students > tbody").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		
	      		var row = "";
	      		$("#count_students").html(hasil.items.length)
	      		$.each(hasil.items, function(i,obj){
	      			row += "<tr>"
	      			row += "<td>"+eval(i+1)+"</td>"
	      			row += "<td>"+obj.name+"</td>"
	      			row += "<td>"+obj.email+"</td>"
	      			
	      			row += "</tr>"
	      		})
	      		$("#table-students > tbody").append(row);
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {
                   
                }
              });
            }

	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {
	            }
	          });
	        }
	      }
	  })
}


function getCourseWorks(courseId){
	var obj = new Object;
	  obj.courseId = courseId
	  $.ajax({
	      type: \'POST\',
	      url: "'.Url::to(['simak-jadwal/ajax-classroom-course-works']).'",
	      data: {
	          dataPost : obj
	      },
	      async: true,
	      error : function(e){
	        Swal.hideLoading();
	        

	      },
	      beforeSend: function(){

	      },
	      success: function (data) {
	      	$("#course_works").empty();
	        var hasil = $.parseJSON(data)
	        if(hasil.code == 200){
	      		
	      		var row = "";
	      		
	      		$.each(hasil.items, function(i,obj){
	      			row += "<li><i class=\'fa fa-tasks activity-icon\'></i>"
	      			row += "<p>New "+obj.workType+" "+obj.title+" <a href=\'"+obj.alternateLink+"\'>see assignment here</a> <span class=\'timestamp\'>Due : "+obj.dueDate.day+"-"+obj.dueDate.month+"-"+obj.dueDate.year+" "+obj.dueTime.hours+":"+obj.dueTime.minutes+"</span></p>"
	      			
	      			row += "</li>"
	      		})
	      		$("#course_works").append(row);
	          
	        }

	        else if(hasil.code == 5403){
      
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                html: hasil.message+" Click <a href=\'"+hasil.authUrl+"\'>here</a> for authorization"
              }).then((result) => {
                if (result.value) {
                	
                }
              });
            }

	        else{
	          Swal.fire({
	            title: \'Oops!\',
	            icon: \'error\',
	            text: hasil.message
	          }).then((result) => {
	            if (result.value) {
	            }
	          });
	        }
	      }
	  })
}

var introguide = introJs();
introguide.setOptions({
    exitOnOverlayClick: false,
    steps : [
        {
            intro: "Ini adalah tampilan dari course Classroom Anda. Saat ini, fitur Classroom di SIAKAD hanya untuk membuat course. Sedangkan kegiatan di Classroom, silakan membuka aplikasi Google Classroom.",
            title: "Fitur Google Classroom",
            element : ".panel-profile"
        },
        {
            intro: "Kode Enrollment dipakai oleh mahasiswa untuk masuk ke Classroom.",
            title: "Kode Enrollment Google Classroom",
            element : "#enrollment_code"
        },
        {
            intro: "Ini adalah informasi mengenai data course Classroom yang telah dibuat",
            title: "Basic Info",
            element : "#basic_info"
        },
        {
            intro: "Klik link ini untuk menuju Google Classroom",
            title: "Link to Classroom",
            element : "#link_to_classroom",
           
        },
        {
            intro: "Tab ini berisi daftar Tugas, Materi, atau Diskusi yang dibuat di Classroom",
            title: "Classwork",
            element : "#tab_classwork"
        },
        {
            intro: "Tab ini berisi daftar mahasiswa yang sudah enroll di Classroom. Ingat <strong>mahasiswa hanya bisa enroll ke Classroom dengan email @unida.gontor.ac.id</strong>",
            title: "Students",
            element : "#tab_students"
        },
    ]
});

var doneTour = localStorage.getItem(\'evt_classroom_detail\') === \'Completed\';

if(!doneTour) {
    introguide.start()

    introguide.oncomplete(function () {
        localStorage.setItem(\'evt_classroom_detail\', \'Completed\');
        Swal.fire({
          title: \'Ulangi Langkah Fitur ini ?\',
          text: "",
          icon: \'warning\',
          showCancelButton: true,
          width:\'35%\',
          confirmButtonColor: \'#3085d6\',
          cancelButtonColor: \'#d33\',
          confirmButtonText: \'Ya, ulangi lagi!\',
          cancelButtonText: \'Tidak, sudah cukup\'
        }).then((result) => {
          if (result.value) {
            introguide.start();
            localStorage.removeItem(\'evt_classroom_detail\');
          }

        });
    });

}
', \yii\web\View::POS_READY);

?>