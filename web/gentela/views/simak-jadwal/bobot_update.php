<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Bobot Nilai Kode kuliah ';
?>
<h2><?=$this->title;?></h2>
<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>

<?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
?>
    <h3>NB: Total bobot tidak boleh melebihi 100 %</h3>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot Harian (%)</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_harian',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot Formatif (%)</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_formatif',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot UTS (%)</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_uts',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Bobot UAS (%)</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'bobot_uas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            
            </div>
        </div>
             
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>
