<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\assets\IntroAsset;
IntroAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Nilai';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';
// $listDosen = \app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all();
?>
<style type="text/css">
  .ui-autocomplete { z-index:2147483647; }

  .modal-dialog{
      top: 50%;
      margin-top: -250px; 
  }

</style>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$this->title;?></h3>
        
      </div>
      <div class="panel-body ">

  
<?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/rekap-nilai'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>

          
          
      </div>
      <?php 

      if(Yii::$app->user->can('sekretearis') || Yii::$app->user->can('sub baak')|| Yii::$app->user->can('akademik'))
      {
        if(Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('baak'))
        {
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->where(['kode_prodi' => Yii::$app->user->identity->prodi])->all(),'kode_prodi','nama_prodi');
        }

        else{
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->all(),'kode_prodi','nama_prodi');
          // $listDosen = \yii\helpers\ArrayHelper::map($listDosen,'id','nama_dosen');
        }
        
      ?>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => !empty($_GET['prodi']) ? $_GET['prodi'] : '',
            'data' => $listProdi,
            'options'=>['id'=>'prodi_id','placeholder'=>Yii::t('app','- Pilih Prodi -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

      </div>

      <?php 
    }


      ?>
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
        
      </div>
    
     <?php ActiveForm::end(); ?>
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th  width="3%" rowspan="2">#</th>
                <th width="17%" rowspan="2"><strong>Dosen Pengampu <sup>a</sup></strong><br>Dosen (Ber-NIDN) <sup>b</sup></th>
                <th width="13%"  rowspan="2">Mata Kuliah</th>
                <th width="10%"  rowspan="2" class="text-center">Mahasiswa (lulus/total)</th>  
                <th colspan="10" width="50%" class="text-center">Nilai</th>
              </tr>
              <tr>
                <th width="5%" class="text-center">A</th>
                <th width="5%" class="text-center">A-</th>
                <th width="5%" class="text-center">B+</th>
                <th width="5%" class="text-center">B</th>
                <th width="5%" class="text-center">B-</th>
                <th width="5%" class="text-center">C+</th>
                <th width="5%" class="text-center">C</th>
                <th width="5%" class="text-center">D</th>
                <th width="5%" class="text-center">E</th>
                <th width="5%" class="text-center">Kosong/Belum input</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 0;

              $list_nama = [];
              foreach($results as $q => $m)
              {
                // $mk = \app\models\SimakMastermatakuliah::find()->where([
                //   'kode_prodi' => $m->prodi,
                //   'tahun_akademik' => $tahun_id,
                //   'kode_mata_kuliah' => $m->kode_mk
                // ])->one();

                // if(empty($mk))
                // {
                    $mk = \app\models\SimakMatakuliah::find()->where([
                      'kode_mk' => $m['kode_mk'],

                    ])->one();
                // }

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m['kampus']])->one();

                $dosen = SimakMasterdosen::find()->where([
                  'nidn' => $m['kode_dosen'],
                ])->one();
                
                

                $dosenNidn = SimakMasterdosen::find()->where([
                  'nidn' => $m['kode_pengampu_nidn'],
                ])->one();

                $jml_mhs = $m['total_mhs'];
                $jml_lulus = $m['total_lulus'];
                $total_nilai = $m['total_nilai'];
                $i++;
              ?>
              <tr>
                <td><?=$i;?></td>
                
                <td>
                    <?php
                    if(!in_array($dosen->nama_dosen,$list_nama)){
                        $list_nama[] = $dosen->nama_dosen;
                    ?>
                    <sup>a</sup> <strong><?=$dosen->nama_dosen;?></strong>
                  <br><sup>b</sup> <?=!empty($dosenNidn) ? $dosenNidn->nama_dosen : '<i>nama dosen ber-NIDN belum diisi</i>';?>
                    <?php
                    }
                    ?>
                </td>
                
                <td>
                  [<?=$m['kode_mk'];?>] <strong><?=!empty($mk) ? $mk->nama_mk: '-';?></strong>
                  <br>
                  <?=!empty($kampus) ? $kampus->nama_kampus : '-';?> - <?=$m['kelas'];?>
                  <br><strong style="color:green"><?=ucwords(strtolower($m['hari']));?>, <?=$m['jam'];?></strong>
                </td>
                <td class="text-center"><?=$jml_lulus?> / <?=$jml_mhs;?></td>
                <td class="text-center"><span style="color:green;font-weight: bold;"><?=$total_nilai['A'];?></span></td>
                <td class="text-center"><?=$total_nilai['A-'];?></td>
                <td class="text-center"><?=$total_nilai['B+'];?></td>
                <td class="text-center"><?=$total_nilai['B'];?></td>
                <td class="text-center"><?=$total_nilai['B-'];?></td>
                <td class="text-center"><?=$total_nilai['C+'];?></td>
                <td class="text-center"><?=$total_nilai['C'];?></td>
                <td class="text-center"><span title="Gagal" style="color:red"><?=$total_nilai['D'];?></span></td>
                <td class="text-center"><span title="Gagal" style="color:red"><?=$total_nilai['E'];?></span></td>
                <td class="text-center"><span title="Belum diinput/kosong" style="color:red;font-weight: bold"><?=$total_nilai['empty'];?></span></td>
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>

       
      </div>
    </div>
   </div>
</div>

<?php 

$this->registerJs(' 

', \yii\web\View::POS_READY);

?>