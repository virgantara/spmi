<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai per Kelas';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';

$tmp = \app\models\SimakMasterprogramstudi::find();

if (Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('theCreator')) {
  $tmp->andWhere(['kode_prodi' => Yii::$app->user->identity->prodi]);
}

$semester = !empty($_GET['semester']) ? $_GET['semester'] : '';
$listProdi = $tmp->all();
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';
$jadwal_id = !empty($_GET['jadwal_id']) ? $_GET['jadwal_id'] : '';

$disabled = Yii::$app->user->identity->access_role == 'sekretearis' && $tahun_id != $tahun_akademik_selected->tahun_id;

?>

<h3 class="page-title"><?= $this->title; ?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
    <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/nilai'],
      'options' => [
        'id' => 'form_validation',
      ]
    ]); ?>
    <div class="form-group">
      <label class="control-label ">Tahun Akademik</label>
      <?= Html::dropDownList('tahun_id', $tahun_id, \yii\helpers\ArrayHelper::map($listTahun, 'tahun_id', 'nama_tahun'), ['class' => 'form-control', 'id' => 'tahun_id']) ?>
    </div>

    <div class="form-group">
      <label class="control-label ">Prodi</label>
      <?= Select2::widget([
        'name' => 'prodi',
        'value' => $prodi,
        'data' => \yii\helpers\ArrayHelper::map($listProdi, 'kode_prodi', 'nama_prodi'),
        'options' => [
          'id' => 'prodi_id',
          'placeholder' => Yii::t('app', '- Pilih Prodi -'),

        ],
        'pluginOptions' => [
          'initialize' => true,
          'allowClear' => true,
        ],
      ]) ?>

    </div>
    <div class="form-group">
      <label class="control-label ">Semester Mata kuliah</label>
      <?= Html::dropDownList('semester', $semester, \app\helpers\MyHelper::getListSemester(), ['id' => 'semester', 'class' => 'form-control', 'prompt' => '- Pilih Semester -']) ?>
    </div>
    <div class="form-group">
      <label class="control-label ">Mata kuliah</label>
      <?= DepDrop::widget([
        'name' => 'kode_mk',
        'value' => $kode_mk,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'kode_mk'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester'],
          'initialize' => true,
          'placeholder' => '- Pilih Mata Kuliah -',
          'url' => Url::to(['/simak-mastermatakuliah/submatkul'])
        ]
      ]) ?>


    </div>
    <div class="form-group">
      <label class="control-label ">Kelas</label>
      <?= DepDrop::widget([
        'name' => 'jadwal_id',
        'value' => $jadwal_id,
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'jadwal_id'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
          'depends' => ['prodi_id', 'tahun_id', 'semester', 'kode_mk'],
          'initialize' => true,
          'placeholder' => '- Pilih Kelas -',
          'url' => Url::to(['/simak-jadwal/subjadwal'])
        ]
      ]) ?>


    </div>
    <div class="form-group clearfix">
      <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
      <a target="_blank" class="btn btn-success" href="<?= Url::to(['simak-jadwal/export-nilai', 'id' => $jadwal_id]); ?>"><i class="fa fa-file-pdf-o"></i> Download Rekap Nilai per Kelas</a>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>

      </div>
      <div class="panel-body ">

        <?php
        $tglnow = date('Y-m-d');
        $is_allowed = ($tglnow >= $tahun_akademik_selected->nilai_mulai . ' 00:00:00' && $tglnow <= $tahun_akademik_selected->nilai_selesai . ' 23:59:59');
        $is_locked = false;
        if ($is_allowed) {
          echo Html::a('<i class="fa fa-edit"></i> Update Bobot', ['simak-jadwal/bobot', 'id' => $jadwal->id], ['class' => 'btn btn-info', 'target' => '_blank']);
        }


        $bobot_harian = is_numeric($jadwal->bobot_harian) ? $jadwal->bobot_harian : 0;
        $bobot_normatif = is_numeric($jadwal->bobot_formatif) ? $jadwal->bobot_formatif : 0;
        $bobot_uts = is_numeric($jadwal->bobot_uts) ? $jadwal->bobot_uts : 0;
        $bobot_uas = is_numeric($jadwal->bobot_uas) ? $jadwal->bobot_uas : 0;
        $total_bobot = $bobot_harian + $bobot_normatif + $bobot_uts + $bobot_uas;

        if ($total_bobot < 100) {
        ?>
          <div class="alert alert-danger"><i class="fa fa-alert"></i> Oops, Total Bobot tidak sampai 100%. Mohon cek kembali dan update bobot mata kuliah ini dengan klik tombol Update Bobot. Jika sudah yakin mengisi bobot, silakan refresh halaman ini dengan menekan tombol F5.</div>
        <?php
        }
        if (!$is_allowed) {
        ?>
          <div class="alert alert-info">
            <i class="fa fa-warning"></i> Mohon maaf, jadwal input nilai sudah ditutup
          </div>
        <?php } ?>
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                <th>NIM</th>
                <th>Mahasiswa</th>
                <th>Semester</th>
                <th>Harian<br>(<?= $jadwal->bobot_harian; ?> %)</th>
                <th>Normatif<br>(<?= $jadwal->bobot_formatif; ?> %)</th>
                <th>UTS<br>(<?= $jadwal->bobot_uts; ?> %)</th>
                <th>UAS<br>(<?= $jadwal->bobot_uas; ?> %)</th>
                <th>NA</th>
                <th>NH</th>
                <?php
                if ($tglnow >= $tahun_akademik_selected->nilai_mulai && $tglnow <= $tahun_akademik_selected->nilai_selesai) {
                ?>
                  <th>Opsi</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
              <?php
              $datacekal = [];
              foreach ($results as $q => $m) {
                if (empty($m->mahasiswa0)) continue;
                $presisi = 0.00001;
                $nilai_angka = round($m->nilai_angka, 2);
                $nilai_huruf = "";
                for ($i = 0; $i < count($range_nilai); $i++) {
                  $rn = $range_nilai[$i];

                  if (($nilai_angka - $rn["min"]) > $presisi && ($nilai_angka - $rn["max"]) <= $presisi) {
                    $nilai_huruf = $rn["val"];
                    break;
                  }
                }

                $hadir = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 1])->count();

                $sakit = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 2])->count();

                $izin = \app\models\SimakAbsenHarian::find()->where(['kode_jadwal' => $m->kode_jadwal, 'mhs' => $m->mahasiswa, 'status_kehadiran' => 3])->count();

                $remaining = $sakit + $izin >= 3 ? 3 : $sakit + $izin;

                $absens = $hadir + $remaining;

                $persen = 0;
                if (!empty($m->kodeJadwal) && $m->kodeJadwal->jumlah_tatap_muka > 0) {
                  $persen = round($absens / $m->kodeJadwal->jumlah_tatap_muka * 100, 2);
                }

                $is_tercekal_akademik = $persen < 75;

                $tahfidz = \app\models\SimakTahfidzNilai::find()->where([
                  'tahun_id' => $tahun_akademik_selected->tahun_id,
                  'nim' => $m->mahasiswa
                ])->one();

                $is_tercekal_tahfidz = empty($tahfidz) || (!empty($tahfidz) && $tahfidz->nilai_angka <= 2);

                $label_tercekal = [];

                if ($is_tercekal_akademik)
                  $label_tercekal[] = 'Akademik';

                if ($is_tercekal_tahfidz)
                  $label_tercekal[] = 'Tahfidz';


                $subtotal = 0;
                foreach ($indukKegiatan as $induk) {
                  foreach ($induk->simakJenisKegiatans as $jk) {
                    $km = \app\models\SimakKegiatanMahasiswa::find()->where([
                      'id_jenis_kegiatan' => $jk->id,
                      'nim' => $m->mahasiswa,
                      'tahun_akademik' => $tahun_akademik_selected->tahun_id,
                      'is_approved' => 1
                    ]);

                    $sub = $km->sum('nilai');
                    if ($sub >= $jk->nilai_maximal) {
                      $subtotal += $jk->nilai_maximal;
                    } else {
                      $subtotal += $sub;
                    }
                  }
                }

                $is_tercekal_akpam = $subtotal < $tahun_akademik_selected->nilai_lulus_akpam;

                if ($is_tercekal_akpam)
                  $label_tercekal[] = 'AKPAM';

                $is_tercekal_adm = \app\models\SimakMastermahasiswa::isTercekalADM($m->mahasiswa);
                if ($is_tercekal_adm)
                  $label_tercekal[] = 'ADM';

                if (count($label_tercekal) > 0) {
              ?>
                  <tr>
                    <td><?= $q + 1; ?></td>
                    <td><?= $m->mahasiswa; ?></td>
                    <td><?= !empty($m->mahasiswa0) ? $m->mahasiswa0->nama_mahasiswa : 'not exist'; ?></td>
                    <td><?= !empty($m->mahasiswa0) ? $m->mahasiswa0->semester : '-'; ?></td>
                    <td colspan="7" class="alert alert-danger text-center">
                      Tercekal : <?= implode(', ', $label_tercekal); ?>
                    </td>
                  </tr>
                <?php
                } else {

                ?>

                  <tr>
                    <td><?= $q + 1; ?></td>
                    <td><?= $m->mahasiswa; ?></td>
                    <td><?= !empty($m->mahasiswa0) ? $m->mahasiswa0->nama_mahasiswa : 'not exist'; ?></td>
                    <td><?= !empty($m->mahasiswa0) ? $m->mahasiswa0->semester : '-'; ?></td>
                    <td><?= $m->harian; ?></td>
                    <td><?= $m->normatif; ?></td>
                    <td><?= $m->uts; ?></td>
                    <td><?= $m->uas; ?></td>
                    <td><?= $m->nilai_angka; ?></td>
                    <td><?= $m->nilai_huruf; ?></td>
                    <?php
                    if ($is_allowed) {
                    ?>
                      <td><?= Html::a('<i class="fa fa-edit"></i> Update Nilai', ['simak-datakrs/update-nilai', 'id' => $m->id], ['class' => 'btn btn-info']); ?></td>
                    <?php
                    } else {
                      echo '<td>-</td>';
                    }

                    ?>
                  </tr>
              <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>