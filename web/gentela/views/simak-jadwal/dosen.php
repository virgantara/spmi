<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\assets\IntroAsset;

IntroAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';
// $listDosen = \app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all();
?>
<style type="text/css">
  .ui-autocomplete {
    z-index: 2147483647;
  }

  .modal-dialog {
    top: 50%;
    margin-top: -250px;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>

      </div>
      <div class="panel-body ">

        <?php

        
        $tglnow = date('Y-m-d');
        $is_allowed = $tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai;
        if (!$is_allowed) {
        ?>
          <div class="alert alert-info">
            <i class="fa fa-warning"></i> Mohon maaf, jadwal input nilai belum dibuka
          </div>


        <?php } ?>

        <?php $form = ActiveForm::begin([
          'method' => 'GET',
          'action' => ['simak-jadwal/dosen'],
          'options' => [
            'id' => 'form_validation',
          ]
        ]); ?>
        <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id', $tahun_id, \yii\helpers\ArrayHelper::map($listTahun, 'tahun_id', 'nama_tahun'), ['class' => 'form-control', 'id' => 'tahun_id']) ?>



        </div>
        <?php

        if (Yii::$app->user->can('kaprodi') || Yii::$app->user->can('sekretearis') || Yii::$app->user->can('sub baak') || Yii::$app->user->can('akademik')) {

          $allowed = ['sekretearis','kurikulum_prodi','kaprodi','Dosen'];
          if(in_array(Yii::$app->user->identity->access_role, $allowed)){  
            $listProdi = ArrayHelper::map(SimakMasterprogramstudi::find()->where(['kode_prodi' => Yii::$app->user->identity->prodi])->all(), 'kode_prodi', 'nama_prodi');
          } else {
            $listProdi = ArrayHelper::map(SimakMasterprogramstudi::find()->all(), 'kode_prodi', 'nama_prodi');
            // $listDosen = \yii\helpers\ArrayHelper::map($listDosen,'id','nama_dosen');
          }

        ?>
          <div class="form-group">
            <label class="control-label ">Prodi</label>
            <?= Select2::widget([
              'name' => 'prodi',
              'value' => !empty($_GET['prodi']) ? $_GET['prodi'] : '',
              'data' => $listProdi,
              'options' => ['id' => 'prodi_id', 'placeholder' => Yii::t('app', '- Pilih Prodi -')],
              'pluginOptions' => [
                'allowClear' => true,
              ],
            ]); ?>

          </div>
          <div class="form-group">
            <label class="control-label ">Dosen</label>

            <?php
            echo DepDrop::widget([
              'name' => 'dosen',
              'data' => [],
              'value' => !empty($_GET['dosen']) ? $_GET['dosen'] : '',
              'type' => DepDrop::TYPE_SELECT2,
              'options' => ['id' => 'dosen'],
              'select2Options' => ['pluginOptions' => ['allowClear' => true]],
              'pluginOptions' => [
                'depends' => ['tahun_id', 'prodi_id'],
                'initialize' => true,
                'placeholder' => '- Pilih Dosen -',
                'url' => Url::to(['/simak-masterdosen/subdosen'])
              ]
            ]);
            ?>

          </div>
        <?php
        }


        ?>
        <div class="form-group clearfix">
          <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
          <a target="_blank" class="btn btn-success" href="<?= Url::to(['simak-jadwal/export', 'tahun' => $tahun_id, 'did' => $dosen]); ?>"><i class="fa fa-file-pdf-o"></i> Download Jadwal</a>
          <?php
          if (Yii::$app->user->can('baak')) {
            echo Html::a('<i class="fa fa-refresh"></i> Bulk Sync', 'javascript:void(0)', ['class' => 'btn btn-danger', 'id' => 'btn-bulk-sync']);
          }
          ?>


        </div>

        <?php ActiveForm::end(); ?>
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>

                <th>Mata Kuliah</th>
                <th>SKS</th>
                <th><strong>Dosen</strong><br>Dosen (Ber-NIDN)</th>

                <!-- <th>Kelas</th> -->
                <!-- <th>Waktu</th> -->
                <th>Prodi</th>
                <th>Jml Mhs/Kuota</th>
                <th>RPS/RPKPS</th>
                <th>Jurnal Ajar</th>

                <th>Keterangan</th>
                <th class="text-center">Opsi</th>

              </tr>
            </thead>
            <tbody>
              <?php
              $i = 0;

              $prodi = Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen') ? Yii::$app->user->identity->prodi : '';
              // echo '<pre>';
              // print_r($results);
              // die;
              foreach ($results as $q => $m) {


                $jumlah_tatap_muka = $m->jumlah_tatap_muka;
                $jumlah_isian_jurnal = count($m->simakJurnals);
                // $mk = \app\models\SimakMastermatakuliah::find()->where([
                //   'kode_prodi' => $m->prodi,
                //   'tahun_akademik' => $tahun_id,
                //   'kode_mata_kuliah' => $m->kode_mk
                // ])->one();

                // if(empty($mk))
                // {
                $mk = \app\models\SimakMatakuliah::find()->where([
                  'kode_mk' => $m->kode_mk,

                ])->one();
                // }

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m->kampus])->one();

                $dosen = SimakMasterdosen::find()->where([
                  'nidn' => $m->kode_dosen,
                ])->one();

                $dosenNidn = SimakMasterdosen::find()->where([
                  'nidn' => $m->kode_pengampu_nidn,
                ])->one();
                $i++;
              ?>
                <tr>
                  <td><?= $i; ?></td>


                  <td>
                    [<?= $m->kode_mk; ?>] <strong><?= !empty($mk) ? $mk->nama_mk : '-'; ?></strong>
                    <br>
                    <?= !empty($kampus) ? $kampus->nama_kampus : '-'; ?> - <?= $m->kelas; ?>
                    <br><strong style="color:green"><?= ucwords(strtolower($m->hari)); ?>, <?= $m->jam; ?></strong>
                  </td>
                  <td><?= !empty($mk) ? $mk->sks_mk : '-'; ?></td>
                  <td>
                    <strong><?= $dosen->nama_dosen; ?></strong>
                    <br><?= !empty($dosenNidn) ? $dosenNidn->nama_dosen : '<i>nama dosen ber-NIDN belum diisi</i>'; ?> <?= Html::a('<i class="fa fa-edit"></i>', 'javascript:void(0)', ['class' => 'btn-edit-dosen', 'title' => 'Ubah Dosen Pengampu Ber-NIDN', 'data-item' => $m->id]); ?>
                  </td>


                  <td><?= !empty($mk) ? $mk->prodi0->nama_prodi : '-'; ?></td>

                  <td>
                    <?= $total_mhs[$m->id]; ?> / <?= $m->kuota_kelas; ?>
                  </td>
                  <td>
                    <?php 
                    if(!empty($mk) && count($mk->simakRps) > 0){

                      foreach($mk->simakRps as $rps){
                        echo '<p>'.Html::a('<i class="fa fa-download"></i> Unduh RPS',['simak-rps/preview','id' => $rps->id],['class' => 'btn btn-success']).'</p>';
                      }
                    }
                    else{
                      echo '<i>RPS belum dibuat. Klik di '.Html::a('sini',['simak-rps/create']).' untuk membuat RPS</i>';
                    }
                    ?>
                  </td>
                  <td>
                    <?php
                    $label_color = 'green';
                    if ($jumlah_isian_jurnal < $jumlah_tatap_muka) {
                      $label_color = 'red';
                    }

                    echo '<span style="color:' . $label_color . '">' . $jumlah_isian_jurnal . ' dari ' . $jumlah_tatap_muka . ' pertemuan</span>';
                    echo '<br>';
                    if ($jumlah_tatap_muka < 16) {
                      echo 'Silakan ubah jumlah pertemuan menjadi 16 kali. Klik di ' . Html::a('sini', ['simak-jadwal/bobot', 'id' => $m->id]) . ' untuk mengubah';
                    }
                    ?>
                  </td>
                  <td>
                    <?php
                    $valid_counter = 0;
                    echo '<ul>';
                    $list_vars = ['a_selenggara_pditt', 'bahasan_case', 'tgl_mulai_koas', 'tgl_selesai_koas', 'mode_kuliah'];
                    foreach ($m->attributes as $q => $attr) {
                      if (in_array($q, $list_vars)) {
                        if (!isset($attr)) {
                          echo '<li style="color:red">' . $m->getAttributeLabel($q) . ' belum diisi';
                          echo '. Klik di ' . Html::a('sini', ['simak-jadwal/silabus', 'id' => $m->id]) . ' untuk mengisi';
                          echo '</li>';
                          $valid_counter++;
                        }
                      }
                    }
                    // print_r($m->attributes);
                    echo '</ul>';
                    ?>


                  </td>
                  <!-- <td> -->

                  <?php
                  // echo \kartik\editable\Editable::widget([
                  // 'name'=>'classroom_id', 
                  // 'value' => $m->classroom_id,
                  // 'beforeInput' => Html::hiddenInput('editableKey',$m->id),
                  // 'asPopover' => true,
                  // 'size'=>'md',
                  // 'options' => ['class'=>'form-control', 'placeholder'=>'Enter Classroom ID...']
                  // ]);
                  ?>
                  <!-- </td> -->
                  <td>

                    <div class="btn-group">
                      <button type="button" <?= $valid_counter > 0 ? 'disabled title="Silakan memperbaiki warna merah di kolom Keterangan"' : ''; ?> id="btn-action-group" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Actions
                        <span class="caret"></span>
                      </button>

                      <ul class="dropdown-menu">
                        <li><?= Html::a('<i class="fa fa-users"></i> Absensi', ['simak-datakrs/absensi', 'id' => $m->id, 'tahun' => $tahun_id]) ?></li>
                        <?php
                        if (Yii::$app->user->can('Dosen') || Yii::$app->user->can('sekretearis')) {

                          if (!empty($m->classroom_id)) {
                        ?>

                            <li class="classroom_li">
                              <?= Html::a('<i class="fa fa-file"></i> Classroom', ['simak-jadwal/classroom', 'id' => $m->id], ['data-item' => $m->id, 'target' => '_blank']) ?>
                            </li>
                          <?php
                          }
                          ?>
                        <?php
                        }
                        ?>

                        <li><?= Html::a('<i class="fa fa-file"></i> Jurnal', ['simak-jurnal/index', 'jid' => $m->id]) ?>

                        </li>


                        <li>

                          <?php
                          $tglNow = date('Y-m-d');
                          if (Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen')) {
                            if ($tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai) {
                              echo Html::a('<i class="fa fa-file"></i> Bobot Materi', ['simak-jadwal/bobot', 'id' => $m->id, 'tahun' => $tahun_id]);
                            }
                          }
                          ?>

                        </li>
                        <li>
                          <?php $idmatkul = \app\models\SimakJadwal::find()->where([
                            'id' => $m->id,
                          ])->one();
                          $mk_id = $idmatkul->id;
                          // echo '<pre>';
                          // print_r($m);
                          // die;
                          ?>

                        <li><?= Html::a('<i class="fa fa-file"></i> Buat Soal', ['soal/index', 'jid' => $m->id]) ?>

                        </li>

                        </li>
                        <li>

                          <?php
                          if (Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen'))
                            echo Html::a('<i class="fa fa-edit"></i> Nilai', ['simak-datakrs/nilai', 'id' => $m->id, 'tahun' => $tahun_id]);

                          if (Yii::$app->user->can('baak'))
                            echo Html::a('<i class="fa fa-file-pdf-o"></i> Cetak Nilai', ['simak-datakrs/export', 'id' => $m->id, 'tahun' => $tahun_id]);
                          ?>
                        </li>
                      </ul>
                    </div>


                  </td>
                </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>

        <div class="alert alert-info">NB: Untuk menggunakan fitur Sync to Google Classroom, pastikan email user Anda sudah menggunakan email unida.gontor.ac.id. Klik di <?= Html::a('sini', ['user/ubah-akun']); ?> untuk mengecek.</div>
        <div class="alert alert-danger">

          <h3><i class="fa fa-warning"></i> Perhatian</h3>
          Pastikan tulisan merah pada kolom Keterangan hilang semua agar bisa mengisi presensi dan jurnal perkuliahan
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Pengampu Ber-NIDN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="jadwal_id">
        <input type="hidden" id="dosen_id">
        <?= Html::textInput('nama_pejabat', '', ['class' => 'form-control', 'id' => 'nama_pejabat', 'placeholder' => 'Ketik Nama Dosen']) ?>
        <?php
        AutoComplete::widget([
          'name' => 'nama_pejabat',
          'id' => 'nama_pejabat',
          'clientOptions' => [
            'source' => Url::to(['simak-masterdosen/ajax-cari-dosen']),
            'autoFill' => true,
            'minLength' => '1',
            'select' => new JsExpression("function( event, ui ) {
          $('#dosen_id').val(ui.item.nidn);
          
       }")
          ],
          'options' => [
            // 'size' => '40'
          ]
        ]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-save">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
<?php

$this->registerJs(' 
$(document).on("click",".btn-edit-dosen",function(e){
  e.preventDefault()

  $("#exampleModal").modal("show");

  $("#jadwal_id").val($(this).data("item"));


})


$(document).on("click",".link-sync-classroom",function(e){
  e.preventDefault()
  var obj = new Object;
  obj.id = $(this).data("item")
  $.ajax({
      type: \'POST\',
      url: "' . Url::to(['simak-jadwal/ajax-classroom']) . '",
      data: {
          dataPost : obj
      },
      async: true,
      error : function(e){
        Swal.hideLoading();
        

      },
      beforeSend: function(){
        Swal.showLoading();
      },
      success: function (data) {
        var hasil = $.parseJSON(data)
        if(hasil.code == 200){
      
          Swal.fire({
            title: \'Yeay!\',
            icon: \'success\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }

        else{
          Swal.fire({
            title: \'Oops!\',
            icon: \'error\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }
      }
  })
})

$(document).on("click","#btn-save",function(e){
  e.preventDefault()

  var obj = new Object;
  obj.kode_pengampu_nidn = $("#dosen_id").val();
  obj.jid = $("#jadwal_id").val();
  
  $.ajax({
      type: \'POST\',
      url: "' . Url::to(['simak-jadwal/ajax-update-pengampu']) . '",
      data: {
          dataPost : obj
      },
      async: true,
      error : function(e){
        Swal.hideLoading();
        

      },
      beforeSend: function(){
        Swal.showLoading();
      },
      success: function (data) {
        var hasil = $.parseJSON(data)
        if(hasil.code == 200){
      
          Swal.fire({
            title: \'Yeay!\',
            icon: \'success\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }

        else{
          Swal.fire({
            title: \'Oops!\',
            icon: \'error\',
            text: hasil.message
          }).then((result) => {
            if (result.value) {
              location.reload(); 
            }
          });
        }
      }
  })
})

', \yii\web\View::POS_READY);

?>