<table width="100%">
  <tr>
    
    <td style="text-align: center;border-bottom: 1px solid black">
      <span style="font-size: 1.35em">UNIVERSITAS DARUSSALAM GONTOR</span><br>
      <span style="font-size: 1.15em">Terakreditasi APT<br>
        Nomor: 1035/SK/BAN-PT/Akred/PT/XII/2020 <br> Kualifikasi BAIK SEKALI</span><br>
      <span style="font-size: 1.15em">Website: https://unida.gontor.ac.id</span>
 
    </td>
  </tr>
</table><br><br>
<table width="50%">
  <tr>
    <td width="25%">Nama Dosen</td>
    <td width="5%">:</td>
    <td width="70%"><?=$dosen->nama_dosen;?></td>
  </tr>
  <tr>
    <td width="25%">NIDN</td>
    <td width="5%">:</td>
    <td width="70%"><?=$dosen->nidn_asli;?></td>
  </tr>
</table>
<br><br>
<table border="1" width="100%" cellpadding="3" cellspacing="0">
    <tr>
      <th style="text-align: center" width="5%">#</th>
      <th style="text-align: center" width="7%">Kelas</th>
      <th style="text-align: center" width="10%">Kampus</th>
      <th style="text-align: center" width="35%">Mata Kuliah</th>
      <th style="text-align: center" width="5%">SKS</th>
      <th style="text-align: center" width="8%">Hari</th>
      <th style="text-align: center" width="10%">Jam</th>
      <th style="text-align: center" width="15%">Prodi</th>
      <th style="text-align: center" width="5%">Jml Mhs</th>
      
    </tr>
  
    <?php 
    $sks = 0;
    foreach($results as $q => $m)
    {
      $sks += $m['sks_mk'];
    ?>
    <tr>
        <td style="text-align: center"><?=$q+1;?></td>
        <td style="text-align: center"><?=$m['kelas'];?></td>
        <td style="text-align: center"><?=$m['kampus'];?></td>
        <td>[<?=$m['kode_mk'];?>]<br><?=$m['nama_mk'];?></td>
        <td style="text-align: center"><?=$m['sks_mk'];?></td>
        <td style="text-align: center"><?=$m['hari'];?></td>
        <td style="text-align: center"><?=$m['jam'];?></td>
        <td><?=$m['nama_prodi'];?></td>
        <td>
          <?=$total_mhs[$m['jadwalid']];?>
        </td>
        
      </tr>
    <?php 
    }
    ?>
    <tr>
      <td colspan="4" style="text-align: right;font-weight: bold;">Total SKS</td>
      <td style="text-align: center"><?=$sks;?></td>
    </tr>
</table>
<br><br>
<table width="100%" border="0" >
  <tr>
    <td width="60%">
      &nbsp;
    </td>
    <td style="text-align: left" width="40%">
      <br><br>
      Mengetahui
      <br><br><br><br><br><br><br><br>
      <u><strong><?=$dosen->nama_dosen;?></strong></u>
      <br>&nbsp;&nbsp;<?=$dosen->nidn_asli;?>

    </td>
  </tr>
</table>