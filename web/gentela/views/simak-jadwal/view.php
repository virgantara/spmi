<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

use yii\jui\AutoComplete;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */

$this->title = 'Kelas Kuliah Matkul: '.$model->namaMk.' - T.A. '.$model->tahun_akademik.' - '.$model->kelas;
$this->params['breadcrumbs'][] = ['label' => 'Simak Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$list_kelas = ArrayHelper::map($list_kelas,'id','kelas');
$list_status = \app\helpers\MyHelper::getStatusAktivitas();
// print_r($list_kelas);exit;
?>
<style>
    .ui-autocomplete { z-index:2147483647; }
</style>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row clearfix">
    <div class="col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', 'javascript:history.back()', ['class' => 'btn btn-default']) ?>
               <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        
                        'hari',
                        'jam',
                        'kode_mk',
                        'namaMk',
                        'namaDosen',
                        'namaDosenNIDN',
                        'semester',
                        
                        
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-heading">
                <h3></h3>
            </div>

            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'kelas',
                        [
                            'attribute' => 'prodi',
                            'value' => function($data){
                                return !empty($data->prodi0) ? $data->prodi0->nama_prodi : null;
                            }
                        ],
                        'tahun_akademik',
                        'kuota_kelas',
                        [
                            'attribute' => 'kampus',
                            'value' => function($data){
                                return !empty($data->kampus0) ? $data->kampus0->nama_kampus : null;
                            }
                        ],
                        'jumlah_tatap_muka'
                     
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
   <div class="col-md-12">
        <div class="panel">
            
            <div class="panel-body">
                <div class="custom-tabs-line tabs-line-bottom left-aligned">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab" aria-expanded="true">Dosen Pengajar</a></li>
                        <li class=""><a href="#tab-bottom-left2" role="tab" data-toggle="tab" aria-expanded="false">Mahasiswa KRS / Peserta Kelas </a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab-bottom-left1">
                        <p>
                                
                                <?=Html::a('<i class="fa fa-plus"></i> DOSEN PENGAJAR','#',['id'=>'btn-tambah-dosen','class'=>'btn btn-primary ']);?>
                            
                        </p>
                        <?php
                            $gridColumns = [
                            [
                                'class'=>'kartik\grid\SerialColumn',
                                'contentOptions'=>['class'=>'kartik-sheet-style'],
                                'width'=>'36px',
                                'pageSummary'=>'Total',
                                'pageSummaryOptions' => ['colspan' => 6],
                                'header'=>'',
                                'headerOptions'=>['class'=>'kartik-sheet-style']
                            ],
                            [
                                'header'=>'Prodi',
                                'value' => function ($data) {
                                    return !empty($data->dosen) && ($data->dosen->kodeProdi) ? $data->dosen->kodeProdi->nama_prodi : null;
                                },
                                
                            ],
                            //'kode_jenjang_studi',
                            //'no_ktp_dosen',
                            [
                                'header'=>'Dosen',
                                'value' => function ($data) {
                                    return !empty($data->dosen)? $data->dosen->nama_dosen : null;
                                },
                                
                            ],
                            // 'niy',
                            [
                                'header'=>'NIDN',
                                'value' => function ($data) {
                                    return !empty($data->dosen)? $data->dosen->nidn_asli : null;
                                },
                                
                            ],
                            [
                                'header'=>'Kode Unik',
                                'value' => function ($data) {
                                    return !empty($data->dosen)? $data->dosen->nidn : null;
                                },
                                
                            ],
                            [
                                'header' => 'Aksi',
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{remove}',
                                'buttons' => [
                                    
                                    'remove' => function ($url, $model){
                                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:void(0)', [
                                                'title' => Yii::t('app', 'Remove Dosen Pengajar'),
                                                'data-pjax' =>0,
                                                'data-item' => $model->id,
                                                'class' => 'remove'
                                      ]);
                                    },
                                    
                                  ],
                                'visibleButtons' => [

                                    'remove' => function($data){
                                        return Yii::$app->user->can('baakdata');
                                    }
                                ]
                            ]    
                       
                        ];?>    
                        <?= GridView::widget([
                                'pager' => [
                                    'firstPageLabel' => 'First',
                                    'lastPageLabel' => 'Last',

                                ],
                                'dataProvider' => $dataProviderDosen,
                                // 'filterModel' => $searchModelPembimbing,
                                'columns' => $gridColumns,
                                'containerOptions' => ['style' => 'overflow: auto'], 
                                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                                'containerOptions' => ['style'=>'overflow: auto'], 
                                'beforeHeader'=>[
                                    [
                                        'columns'=>[
                                            ['content'=> $this->title, 'options'=>['colspan'=>14, 'class'=>'text-center warning']], //cuma satu 
                                        ], 
                                        'options'=>['class'=>'skip-export'] 
                                    ]
                                ],
                                'exportConfig' => [
                                      GridView::PDF => ['label' => 'Save as PDF'],
                                      GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                                      GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                                      GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                                  ],
                                  
                                'toolbar' =>  [
                                    '{export}', 

                                   '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                                ],
                                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                            // set export properties
                                'export' => [
                                    'fontAwesome' => true
                                ],
                                'pjax' => true,
                                'pjaxSettings' =>[
                                    'neverTimeout'=>true,
                                    'options'=>[
                                        'id'=>'pjax-container-dosen-pengajar',
                                    ]
                                ], 
                                'bordered' => true,
                                'striped' => true,
                                // 'condensed' => false,
                                // 'responsive' => false,
                                'hover' => true,
                                // 'floatHeader' => true,
                                // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY
                                ],
                            ]); ?>

                    </div>
                    <div class="tab-pane fade in" id="tab-bottom-left2">
                        <div class="alert alert-danger">
                            <h3><i class="fa fa-warning"></i> Perhatian</h3>
                                Data yang muncul di sini hanya mahasiswa yang sudah diapprove KRS-nya
                        </div>
                        <?php
                        $gridColumns = [
                        [
                            'class'=>'kartik\grid\SerialColumn',
                            'contentOptions'=>['class'=>'kartik-sheet-style'],
                            'width'=>'36px',
                            'pageSummary'=>'Total',
                            'pageSummaryOptions' => ['colspan' => 6],
                            'header'=>'',
                            'headerOptions'=>['class'=>'kartik-sheet-style']
                        ],
                                [
                                    'attribute' => 'namaProdi',
                                    'label' => 'Prodi',
                                    'format' => 'raw',
                                    'filter'=>\yii\helpers\ArrayHelper::map(\app\models\SimakMasterprogramstudi::getListProdi(),'kode_prodi','nama_prodi'),
                                    'value'=>function($model,$url){
                                        return !empty($model->mahasiswa0->kodeProdi) ? $model->mahasiswa0->kodeProdi->nama_prodi : '-';
                                        
                                    },
                                ],
                                'mahasiswa',
                                [
                                    'attribute' => 'namaMahasiswa',
                                    'label' => 'Mahasiswa',
                                    'format' => 'raw',
                                    'value'=>function($model,$url){
                                        return !empty($model->mahasiswa0) ? $model->mahasiswa0->nama_mahasiswa : '-';
                                        
                                    },
                                ],
                                [
                                    'attribute' =>'nilai_angka',
                                    'contentOptions' => ['class' => 'col-lg-1'],
                                    'value'=>function($model,$url){
                                        return round($model->nilai_angka,2);
                                        
                                    },
                                ],
                                'nilai_huruf',
                                [
                                    'header' => 'Angkatan',
                                    'format' => 'raw',
                                    'value'=>function($model,$url){
                                        return !empty($model->mahasiswa0) ? $model->mahasiswa0->tahun_masuk : '-';
                                        
                                    },
                                ],
                                [
                                    'class' => 'kartik\grid\EditableColumn',
                                    'header' => 'Pindah Kelas',
                                    'attribute' => 'kode_jadwal',
                                    'filter' => $list_kelas,
                                    'refreshGrid'=>true,
                                    'readonly' => !Yii::$app->user->can('baak'),
                                    'editableOptions' => [
                                        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                                        'asPopover' => false,
                                        'data' => $list_kelas
                                    ],
                                    'value' => function($data) use ($list_kelas){
                                        return $data->kelas;
                                    }   
                                ],
                                [
                                    'header' => 'Status Registrasi',
                                    'format' => 'raw',
                                    'value'=>function($model,$url){
                                        return !empty($model->mahasiswa0) && !empty($model->mahasiswa0->id_reg_pd) ? '<span title="'.$model->mahasiswa0->id_reg_pd.'" class="label label-success">Registered</span>' : '<span class="label label-danger">Not-Registered</span>';
                                        
                                    },
                                ],
                                [
                                    'attribute' => 'apakah_4_tahun',
                                    'label' => 'Lama Rencana Studi',
                                    'format' => 'raw',
                                    'value'=>function($model,$url){
                                        $list = ['1' => '4 Tahun','2' => '1 Tahun'];
                                        $label = '';
                                        if(!empty($list[$model->mahasiswa0->apakah_4_tahun])) {

                                            if($model->mahasiswa0->apakah_4_tahun == '2')
                                                $label = '<span class="label label-danger">'.$list[$model->mahasiswa0->apakah_4_tahun].'</span>';
                                            else
                                                $label = $list[$model->mahasiswa0->apakah_4_tahun];
                                        }
                                        
                                        return $label;
                                        
                                    },
                                ],
                                [
                                    'attribute' => 'status_aktivitas',
                                    'format' => 'raw',
                                    'value'=>function($model,$url) use ($list_status){
                                        $label = '';
                                        if(!empty($model->mahasiswa0) && !empty($list_status[$model->mahasiswa0->status_aktivitas])) {

                                            if($model->mahasiswa0->status_aktivitas == 'A')
                                                $label = '<span class="label label-success">'.$list_status[$model->mahasiswa0->status_aktivitas].'</span>';
                                            else if(in_array($model->mahasiswa0->status_aktivitas,['K','N']))
                                                $label = '<span class="label label-danger">'.$list_status[$model->mahasiswa0->status_aktivitas].'</span>';
                                            else
                                                $label = $list_status[$model->mahasiswa0->status_aktivitas];
                                        }

                                        return $label;
                                    },
                                ],
                                [
                                    'header' => 'Keterangan',
                                    'format' => 'raw',
                                    'value'=>function($model,$url){
                                        return !empty($model->mahasiswa0) ? $model->mahasiswa0->invalidAttributes : '';
                                        
                                    },
                                ],
                                [
                                    'header' => 'Aksi',
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{sync}',
                                    'buttons' => [
                                        
                                        'sync' => function ($url, $model){
                                          return Html::a('<span class="glyphicon glyphicon-refresh"></span>', 'javascript:void(0)', [
                                                    'title' => Yii::t('app', 'Sync to FEEDER'),
                                                    'data-pjax' =>0,
                                                    'data-item' => $model->mahasiswa0->nim_mhs,
                                                    'class' => 'sync'
                                          ]);
                                        },
                                        
                                      ],
                                    'visibleButtons' => [

                                        'sync' => function($data){
                                            return Yii::$app->user->can('baakdata') && $data->mahasiswa0->apakah_4_tahun == '1';
                                        }
                                    ]
                                ]
                                //'updated_at',
                                //'created_at',
                        
                    ];?>    
                    <?= GridView::widget([
                            'pager' => [
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last',

                            ],
                            'dataProvider' => $dataProviderPeserta,
                            // 'filterModel' => $searchModel,
                            'columns' => $gridColumns,
                            'containerOptions' => ['style' => 'overflow: auto'], 
                            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                            'containerOptions' => ['style'=>'overflow: auto'], 
                            'beforeHeader'=>[
                                [
                                    'columns'=>[
                                        ['content'=> $this->title, 'options'=>['colspan'=>14, 'class'=>'text-center warning']], //cuma satu 
                                    ], 
                                    'options'=>['class'=>'skip-export'] 
                                ]
                            ],
                            'exportConfig' => [
                                  GridView::PDF => ['label' => 'Save as PDF'],
                                  GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                                  GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                                  GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                              ],
                              
                            'toolbar' =>  [
                                '{export}', 

                               '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                            ],
                            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                        // set export properties
                            'export' => [
                                'fontAwesome' => true
                            ],
                            'pjax' => true,
                            'pjaxSettings' =>[
                                'neverTimeout'=>true,
                                'options'=>[
                                    'id'=>'pjax-container-mahasiswa',
                                ]
                            ],  
                            'bordered' => true,
                            'striped' => true,
                            // 'condensed' => false,
                            // 'responsive' => false,
                            'hover' => true,
                            // 'floatHeader' => true,
                            // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                            'panel' => [
                                'type' => GridView::TYPE_PRIMARY
                            ],
                        ]); ?>

                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<?php


yii\bootstrap\Modal::begin([
'headerOptions' => ['id' => 'modalHeader'],
'id' => 'modal',
'size' => 'modal-lg',
'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
?>

<form action="" id="form-dosen-pengajar">
    <div class="form-group">
        <label for="">Dosen*</label>
        <input type="hidden" name="jadwal_id" id="jadwal_id" value="<?=$model->id;?>">
        <input type="hidden" id="dosen_id" name="dosen_id">
        <?= Html::textInput('nama_dosen_pembimbing','',['class'=>'form-control','id'=>'nama_dosen_pembimbing','placeholder'=>'Ketik Nama Dosen']) ?>
        <?php 
        AutoComplete::widget([
            'name' => 'nama_dosen_pembimbing',
            'id' => 'nama_dosen_pembimbing',
            'clientOptions' => [
            'source' => Url::to(['simak-masterdosen/ajax-cari-dosen']),
            'autoFill'=>true,
            'minLength'=>'1',
            'select' => new JsExpression("function( event, ui ) {
                $('#dosen_id').val(ui.item.id);
                
             }")],
            'options' => [
                // 'size' => '40'
            ]
         ]); ?>
         <div class="help-block"></div>
    </div>
    <div class="form-group">
        <label for="">Jenis Evaluasi*</label>
        <?= Html::dropDownList('id_jenis_evaluasi','',['1' => 'Evaluasi Akademik'],['class'=>'form-control','id'=>'id_jenis_evaluasi']) ?>
    </div>
    <div class="form-group">
        <button id="btn-add-dosen-pengajar" class="btn btn-info">
            <i class="fa fa-save"></i> Simpan
        </button>
    </div>
</form>

<?php
yii\bootstrap\Modal::end();
?>
<?php 

$this->registerJs(' 



$(document).on(\'click\',\'.remove\',function(e){
    e.preventDefault();
    var obj = new Object;
    obj.kelas_id = $(this).data("item");
    Swal.fire({
      title: \'Penghapusan Dosen Pengajar FEEDER!\',
      text: "Hapus data dosen pengajar kelas ini dari FEEDER? Ketik email Anda untuk konfirmasi hapus",
      icon: \'info\',
      input: "text",
      showCancelButton: true,
      confirmButtonColor: \'#d33\',
      cancelButtonColor: \'#3085d6\',
      confirmButtonText: \'Ya, hapus sekarang!\'
    }).then((result) => {
      if (result.value) {
        obj.email = result.value
        $.ajax({
          type: \'POST\',
          url: "'.Url::to(['simak-jadwal-pengajar/ajax-remove']).'",
          data: {
              dataPost : obj
          },
          async: true,
          error : function(e){
            callback(e.responseText,null)
            
          },
          beforeSend: function(){
            Swal.showLoading();
          },
          success: function (data) {
            Swal.close()
            var hasil = $.parseJSON(data)
            if(hasil.code == 200){
          
              Swal.fire({
                title: \'Yeay!\',
                icon: \'success\',
                text: hasil.message
              });
              $.pjax.reload({container: "#pjax-container-dosen-pengajar"})
            }

            else{
              Swal.fire({
                title: \'Oops!\',
                icon: \'error\',
                text: hasil.message
              });
            }
          }
      })
      }
    });
    
    
    
});


$(document).on(\'click\',\'.sync\',function(e){
    e.preventDefault();
    var obj = new Object;
    obj.nim = $(this).data("item");
    $.ajax({
        type: \'POST\',
        url: "'.Url::to(['simak-mastermahasiswa/ajax-get-kode-registrasi']).'",
        data: {
            dataPost : obj
        },
        async: true,
        error : function(e){
          Swal.close()
          callback(e.responseText,null)
        },
        beforeSend: function(){
          Swal.showLoading();
        },
        success: function (data) {
          Swal.close()
          var hasil = $.parseJSON(data)
          if(hasil.code == 200){
        
            Swal.fire({
                title: \'Yeay!\',
                icon: \'success\',
                text: hasil.message,
                showConfirmButton: false,
                timer: 1000
            })

            $.pjax.reload({container: "#pjax-container-mahasiswa"})
              

            
            
          }

          else{
            Swal.fire({
              title: \'Oops!\',
              icon: \'error\',
              text: hasil.message
            });
          }
        }
    })
    
    
});

$("#modal").on("shown.bs.modal", function (e) {
    $("#nama_dosen_pembimbing").focus()
})

$(document).on("click", "#btn-tambah-dosen", function(e){
    e.preventDefault();
    $("#modal").modal("show")
    
});


$(document).on("click", "#btn-add-dosen-pengajar", function(e){
    e.preventDefault();
    
    var obj = $("#form-dosen-pengajar").serialize()
    
    $.ajax({
        url: "'.Url::to(["simak-jadwal-pengajar/ajax-add"]).'",
        type : "POST",

        data: obj,
        success: function (data) {
            var hasil = $.parseJSON(data)
            if(hasil.code == 200){
                Swal.fire({
                    title: \'Yeay!\',
                    icon: \'success\',
                    text: hasil.message
                });
                
                $.pjax.reload({container: \'#pjax-container-dosen-pengajar\'});
            }

            else{
                Swal.fire({
                    title: \'Oops!\',
                    icon: \'error\',
                    text: hasil.message
                })
            }
        }
    })
});

', \yii\web\View::POS_READY);

?>