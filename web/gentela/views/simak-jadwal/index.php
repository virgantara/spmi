<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h4 ><?=$this->title;?></h4>
    </div>
    <div class="card-body">
   
    <?php
    $gridColumns = [
    [
        'class'=>'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'pageSummary'=>'Total',
        'pageSummaryOptions' => ['colspan' => 6],
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
            [
                'attribute'=>'prodi',
                'filter' => \app\models\SimakMasterdosenSearch::getProdiList(),
                'value' => function ($data) {
                    return !empty($data->prodi0) ? $data->prodi0->nama_prodi : 'not set';
                },
                // 'contentOptions'=>function($model, $key, $index, $column) {
                //     return ['class'=>\app\helpers\CssHelper::roleCss($model->kode_prodi)];
                // }
            ],
            'hari',
            'jam',
            'kode_mk',
            'namaMk',
            // [
            //     'attribute' => 'kode_dosen',
            //     'label' => 'Dosen',
            //     'format' => 'raw',
            //     'value' => function($data){
            //         return $data->namaProgram;
            //     }
            // ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_dosen',
                'refreshGrid' => true,
                'readonly' => !Yii::$app->user->can('theCreator'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    
                ],
            ],
            'namaDosen',
             [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_pengampu_nidn',
                'refreshGrid' => true,
                'readonly' => !Yii::$app->user->can('theCreator'),
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    
                ],
            ],
            'namaDosenNIDN',
            'semester',
            'kelas',
            //'fakultas',
            
            //'kd_ruangan',
            'tahun_akademik',
            //'kuota_kelas',
            [
                'attribute' => 'kampus',
                'value' => function($data){
                    return !empty($data->kampus0) ? $data->kampus0->nama_kampus : 'not set';
                }
            ],
            //'presensi:ntext',
            //'materi',
            //'bobot_formatif',
            //'bobot_uts',
            //'bobot_uas',
            //'bobot_harian1',
            //'bobot_harian',
            //'jadwal_temp_id',
            //'created_at',
            //'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {update-bobot} {delete}',
                'buttons'=>[
                    'update-bobot'=>function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', ['simak-jadwal/update-bobot','id'=>$model->id], ['aria-label' => 'Update Bobot', 'data-pjax'=>'0','title'=>'Ubah Bobot']);
                    },
                    
                ],
                'visibleButtons' => [
                    'update-bobot' => function ($model, $key, $index) {
                        return Yii::$app->user->can('baak');
                    }
                ]
            ]
];?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'], 
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'beforeHeader'=>[
            [
                'columns'=>[
                    ['content'=> $this->title, 'options'=>['colspan'=>14, 'class'=>'text-center warning']], //cuma satu 
                ], 
                'options'=>['class'=>'skip-export'] 
            ]
        ],
        'exportConfig' => [
              GridView::PDF => ['label' => 'Save as PDF'],
              GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
              GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
              GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
          ],
          
        'toolbar' =>  [
            '{export}', 

           '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        // 'condensed' => false,
        // 'responsive' => false,
        'hover' => true,
        'pager' => [
            'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
            'activePageCssClass' => 'active paginate_button page-item',
            'disabledPageCssClass' => 'disabled paginate_button',
            'prevPageLabel' => 'Previous',   // Set the label for the "previous" page button
            'nextPageLabel' => 'Next',   // Set the label for the "next" page button
            'firstPageLabel'=>'First',   // Set the label for the "first" page button
            'lastPageLabel'=>'Last',    // Set the label for the "last" page button
            'nextPageCssClass'=>'paginate_button next page-item',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'paginate_button previous page-item',    // Set CSS class for the "previous" page button
            'firstPageCssClass'=>'first paginate_button page-item',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'last paginate_button page-item',    // Set CSS class for the "last" page button
            'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
            'linkOptions' => [
                'class' => 'page-link'
            ]
        ],      
        // 'floatHeader' => true,
        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>

    </div>

</div>

