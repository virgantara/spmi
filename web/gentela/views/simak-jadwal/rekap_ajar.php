<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\assets\IntroAsset;
IntroAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Jurnal Pengajaran';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : '';
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : '';
// $listDosen = \app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all();
?>
<style type="text/css">
  .ui-autocomplete { z-index:2147483647; }

  .modal-dialog{
      top: 50%;
      margin-top: -250px; 
  }

</style>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$this->title;?></h3>
        
      </div>
      <div class="panel-body ">

  
<?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/rekap-ajar'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>

          
          
      </div>
      <?php 

      if(Yii::$app->user->can('sekretearis') || Yii::$app->user->can('sub baak')|| Yii::$app->user->can('akademik'))
      {
        if(Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('baak'))
        {
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->where(['kode_prodi' => Yii::$app->user->identity->prodi])->all(),'kode_prodi','nama_prodi');
        }

        else{
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->all(),'kode_prodi','nama_prodi');
          // $listDosen = \yii\helpers\ArrayHelper::map($listDosen,'id','nama_dosen');
        }
        
      ?>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => !empty($_GET['prodi']) ? $_GET['prodi'] : '',
            'data' => $listProdi,
            'options'=>['id'=>'prodi_id','placeholder'=>Yii::t('app','- Pilih Prodi -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

      </div>

      <?php 
    }


      ?>
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
        
      </div>
    
     <?php ActiveForm::end(); ?>
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                <th><strong>Dosen</strong><br>Dosen (Ber-NIDN)</th>
                <th>Mata Kuliah</th>
                <th>SKS</th>
                
                
                <!-- <th>Kelas</th> -->
                <!-- <th>Waktu</th> -->
                <th>Prodi</th>
              
                <th>Jurnal Perkuliahan</th>
               
                
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 0;

              $list_nama = [];
              foreach($results as $q => $m)
              {
                // $mk = \app\models\SimakMastermatakuliah::find()->where([
                //   'kode_prodi' => $m->prodi,
                //   'tahun_akademik' => $tahun_id,
                //   'kode_mata_kuliah' => $m->kode_mk
                // ])->one();

                // if(empty($mk))
                // {
                    $mk = \app\models\SimakMatakuliah::find()->where([
                      'kode_mk' => $m['kode_mk'],

                    ])->one();
                // }

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m['kampus']])->one();

                $dosen = SimakMasterdosen::find()->where([
                  'nidn' => $m['kode_dosen'],
                ])->one();
                
                

                $dosenNidn = SimakMasterdosen::find()->where([
                  'nidn' => $m['kode_pengampu_nidn'],
                ])->one();
                $i++;
              ?>
              <tr>
                <td><?=$i;?></td>
                
                <td>
                    <?php
                    if(!in_array($dosen->nama_dosen,$list_nama)){
                        $list_nama[] = $dosen->nama_dosen;
                    ?>
                    <strong><?=$dosen->nama_dosen;?></strong>
                  <br><?=!empty($dosenNidn) ? $dosenNidn->nama_dosen : '<i>nama dosen ber-NIDN belum diisi</i>';?>
                    <?php
                    }
                    ?>
                </td>
                
                <td>
                  [<?=$m['kode_mk'];?>] <strong><?=!empty($mk) ? $mk->nama_mk: '-';?></strong>
                  <br>
                  <?=!empty($kampus) ? $kampus->nama_kampus : '-';?> - <?=$m['kelas'];?>
                  <br><strong style="color:green"><?=ucwords(strtolower($m['hari']));?>, <?=$m['jam'];?></strong>
                </td>
                <td><?=!empty($mk) ? $mk->sks_mk : '-';?></td>
                
                
                <td><?=!empty($mk) ? $mk->prodi0->nama_prodi : '-';?></td>
               
                <td>
                    <?php
                    $label_color = 'green';
                    if($m['jumlah_isian_jurnal'] < $m['jumlah_tatap_muka']){
                        $label_color = 'red';
                    }   

                    echo '<span style="color:'.$label_color.'">'.$m['jumlah_isian_jurnal'].' dari '.$m['jumlah_tatap_muka'].' pertemuan</span>';
                    echo '<br>';
                    if($m['jumlah_tatap_muka'] < 16){
                        echo 'Jumlah pertemuan belum disetting menjadi 16 kali.';
                    }
                    ?>
                  
                </td>
               
              
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>

       
      </div>
    </div>
   </div>
</div>

<?php 

$this->registerJs(' 

', \yii\web\View::POS_READY);

?>