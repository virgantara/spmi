<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Presensi Per Prodi';
$this->params['breadcrumbs'][] = $this->title;


$tmp = \app\models\SimakMasterprogramstudi::find();

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$semester = !empty($_GET['semester']) ? $_GET['semester'] : $semester;
$listProdi = $tmp->all();
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';

?>

<h3 class="page-title"><?=$this->title;?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/repres','kode'=>$kode],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>
      </div>

      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => $prodi,
            'data' => \yii\helpers\ArrayHelper::map($listProdi,'kode_prodi','nama_prodi'),
            'options'=>[
              'id'=>'prodi_id',
              'placeholder'=>Yii::t('app','- Pilih Prodi -'),

            ],
            'pluginOptions' => [
              'initialize' => true,
                'allowClear' => true,
            ],
        ]) ?>
        </div>
      <div class="form-group">
          <label class="control-label ">Semester</label>
          <?= Html::dropDownList('semester',$semester,\app\helpers\MyHelper::getListSemester(),['id'=>'semester','class'=>'form-control','prompt'=>'- Pilih Semester -']) ?>
      </div>
         <div class="form-group clearfix">

        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
       
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>

    </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar Rekap Absensi</h3>

      </div>
      <div class="panel-body ">

        <div class="table-responsive">
          <table class="table table-striped table-hover table-bordered"> 
          <thead>
            <tr>
                <th>No.</th>
                <th class="text-center">NIM</th>
                <th class="text-center">NIMA</th>
                <?php
                $lmk = [];
                 foreach($mks as $qmk=>$vmk){
                    $lmk[]=$vmk['kode_mata_kuliah'];
                    echo "<th class='text-center'>".$vmk->kode_mata_kuliah."<br>".$vmk->nama_mata_kuliah."</th>";
            }
                ?>
            </tr>
          </thead>     
                <tbody>
                <tr>
            <?php
            $a = 1;
                foreach ($absen as $q => $kcb){
                echo "<tr><td align='center'>".$a++.".</td>";
                echo "<td>".$kcb['mhs1']."</td>";
                echo "<td>".$kcb['mhs2']."</td>";
                foreach($mks as $mk){
                    echo "<td align='center'>".$kcb[''.$mk->kode_mata_kuliah.'']."</td>";
                    }
                echo "</tr>";
            }
            
            ?>
              
            </tr>
        </body>
        </table>
         
        </div>
      </div>
    </div>
   </div>
</div>
