<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Presensi Per Prodi';
$this->params['breadcrumbs'][] = $this->title;

$kampus = !empty($_GET['kampus']) ? $_GET['kampus'] : '';
$tmp = \app\models\SimakMasterprogramstudi::find();

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : '';
$semester = !empty($_GET['semester']) ? $_GET['semester'] : '';

$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';

$status_aktivitas = !empty($_GET['status_aktivitas']) ? $_GET['status_aktivitas'] : '';
$listKampus = [];
$query = \app\models\SimakKampus::find();
if(Yii::$app->user->identity->access_role =='akpam'){
    $query->andWhere(['kode_kampus'=>Yii::$app->user->identity->kampus]);
} 

if(Yii::$app->user->identity->access_role == 'sekretearis'){
    $tmp->andWhere(['kode_prodi'=>Yii::$app->user->identity->prodi]);
}

$listProdi = $tmp->all();
$btn_cari = !empty($_GET['btn-cari']) ? $_GET['btn-cari'] : '';

$listKampus = $query->all();

?>

<h3 class="page-title"><?=$this->title;?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/rekap'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>
      </div>
      <div class="form-group">
          <label class="control-label ">Kelas</label>
          <?= Html::dropDownList('kampus',$kampus,\yii\helpers\ArrayHelper::map($listKampus,'kode_kampus','nama_kampus'),['id'=>'kampus','class'=>'form-control','prompt'=>'- Pilih Kelas -']) ?>

          
          
      </div>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => $prodi,
            'data' => \yii\helpers\ArrayHelper::map($listProdi,'kode_prodi','nama_prodi'),
            'options'=>[
              'id'=>'prodi_id',
              'placeholder'=>Yii::t('app','- Pilih Prodi -'),

            ],
            'pluginOptions' => [
              'initialize' => true,
                'allowClear' => true,
            ],
        ]) ?>
        </div>
        <div class="form-group">
          <label class="control-label ">Status</label>
          <?= Html::dropDownList('status_aktivitas',$status_aktivitas,\app\helpers\MyHelper::getStatusAktivitas(),['id'=>'status_aktivitas','class'=>'form-control','prompt'=>'- Pilih status_aktivitas -']) ?>

          
          
      </div>
      <div class="form-group">
          <label class="control-label ">Semester</label>
          <?= Html::dropDownList('semester',$semester,\app\helpers\MyHelper::getListSemester(),['id'=>'semester','class'=>'form-control','prompt'=>'- Pilih Semester -']) ?>
      </div>
         <div class="form-group clearfix">

        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
         <a target="_blank" class="btn btn-success" href="<?=Url::to(['simak-jadwal/export-rekap','prodi'=>$prodi,'kampus' => $kampus, 'semester'=>$semester,'tahun_id'=>$tahun_id,'status_aktivitas'=>$status_aktivitas,'btn_cari'=>$btn_cari]);?>"><i class="fa fa-file-excel-o"></i> Download Rekap</a>
       
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar Rekap Presensi</h3>

      </div>
      <div class="panel-body ">

        <div class="table-responsive">
          <table class="table table-striped table-hover table-bordered"> 
          <thead>
            <tr>
                <th>No.</th>
                <th class="text-center">NIM</th>
                <th class="text-center">Nama Mahasiswa</th>
                <th class="text-center">Semester</th>
                <?php
                $lmk = [];
                 foreach($list_mk as $qmk=>$mk)
                 {
                  $mat = \app\models\SimakMatakuliah::findOne([
                    'prodi' => $prodi,
                    'kode_mk' => $mk->kode_mk
                  ]);
                    echo '<th class="text-center">['.$mk->kode_mk.']<br>'.(!empty($mat) ? $mat->nama_mk : null).'<br>Kelas '.$mk->kelas.'</th>';
                    // echo '<th>a</th>';
                  }
                ?>
            </tr>
          </thead>     
              <tbody>
                <?php 

                
                $counter = 0;
                foreach($listMhs as $q => $mhs)
                {
                  
                  $counter++;
                ?>
                <tr>
                  <td><?=$counter;?></td>
                  <td><?=$mhs->nim_mhs;?></td>
                  <td><?=$mhs->nama_mahasiswa;?></td>
                  <td><?=$mhs->semester;?></td>
                  <?php 
                  foreach($list_mk as $qmk=>$mk)
                  {

                    $persen = $results[$q][$mk->id]['absen'];

                    $label = $persen < 75 ? 'danger' : 'success';
                    
                    if($persen < 0){
                      $label = 'default';
                      $persen = '-';
                    }

                    else{
                      $persen .= ' %';
                    }

                    echo '<td class="text-center alert alert-'.$label.'">'.$persen.'</td>';
                  }
                  ?>
                </tr>
                <?php 
              }
                ?>
        </body>
        </table>
         
        </div>
      </div>
    </div>
   </div>
</div>
