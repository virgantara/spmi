<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'assets/bootstrap/dist/css/bootstrap.min.css',
        'assets/jquery/dist/jquery-ui.css',
        'assets/font-awesome/css/font-awesome.min.css',
        'assets/nprogress/nprogress.css',
        'assets/iCheck/skins/flat/green.css',
        // 'assets/fullcalendar/dist/fullcalendar.min.css',
        // 'assets/fullcalendar/dist/fullcalendar.print.css',
        'build/css/custom.css',
    ];
    public $js = [
        'assets/bootstrap/dist/js/bootstrap.min.js',
        'assets/jquery/dist/jquery-ui.min.js',
        'assets/fastclick/lib/fastclick.js',
        'assets/nprogress/nprogress.js',
        'assets/DateJS/build/date.js',
        'assets/moment/min/moment.min.js',
        // 'assets/fullcalendar/dist/fullcalendar.min.js',
        'build/js/custom.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',

    ];
}
