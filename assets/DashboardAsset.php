<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'assets/bootstrap/dist/css/bootstrap.min.css',
        'assets/font-awesome/css/font-awesome.min.css',
        'assets/nprogress/nprogress.css',
        'assets/iCheck/skins/flat/green.css',
        'build/css/custom.min.css',
    ];
    public $js = [
        'assets/bootstrap/dist/js/bootstrap.min.js',
        'assets/fastclick/lib/fastclick.js',
        'assets/nprogress/nprogress.js',
        'assets/DateJS/build/date.js',
        'build/js/custom.js'
    ];
    
    // public $css = [
    //     'assets/vendor/bootstrap/css/bootstrap.min.css',
    //     'assets/vendor/font-awesome/css/font-awesome.min.css',
    //     'assets/vendor/linearicons/style.css',
    //     'assets/vendor/chartist/css/chartist-custom.css',
    //     'assets/css/source_sans_pro.css',
    //     'assets/css/main.css',

    // ];

    // public $js = [
    //     'assets/vendor/jquery/jquery.min.js',
    //     'assets/vendor/bootstrap/js/bootstrap.min.js',
    //     'assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js',
    //     'assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js',
    //     'assets/vendor/chartist/js/chartist.min.js',
    //     'assets/scripts/klorofil-common.js'
    // ];

    public $depends = [
        'yii\web\YiiAsset',

    ];
}
