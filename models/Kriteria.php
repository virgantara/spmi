<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kriteria".
 *
 * @property int $id
 * @property string $nama
 * @property string|null $status_aktif 1=aktif,0=tidak aktif
 * @property int $urutan
 * @property int|null $jenis
 * @property int|null $is_khusus
 * @property string|null $keterangan
 * @property string|null $kode
 * @property string|null $jenis_standar
 *
 * @property Indikator[] $indikators
 * @property PembagianKriteria[] $pembagianKriterias
 */
class Kriteria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kriteria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'urutan'], 'required'],
            [['urutan', 'jenis', 'is_khusus'], 'integer'],
            [['nama'], 'string', 'max' => 255],
            [['status_aktif'], 'string', 'max' => 1],
            [['keterangan', 'jenis_standar'], 'string', 'max' => 50],
            [['kode'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'urutan' => Yii::t('app', 'Urutan'),
            'jenis' => Yii::t('app', 'Jenis'),
            'is_khusus' => Yii::t('app', 'Is Khusus'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'kode' => Yii::t('app', 'Kode'),
            'jenis_standar' => Yii::t('app', 'Jenis Standar'),
        ];
    }

    /**
     * Gets query for [[Indikators]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikators()
    {
        return $this->hasMany(Indikator::class, ['kriteria_id' => 'id']);
    }

    /**
     * Gets query for [[PembagianKriterias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembagianKriterias()
    {
        return $this->hasMany(PembagianKriteria::class, ['kriteria_id' => 'id']);
    }
}
