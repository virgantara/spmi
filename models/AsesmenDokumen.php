<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asesmen_dokumen".
 *
 * @property int $id
 * @property int|null $asesmen_id
 * @property int|null $dokumen_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Asesmen $asesmen
 * @property Dokumen $dokumen
 */
class AsesmenDokumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asesmen_dokumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asesmen_id', 'dokumen_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['asesmen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asesmen::class, 'targetAttribute' => ['asesmen_id' => 'id']],
            [['dokumen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dokumen::class, 'targetAttribute' => ['dokumen_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'asesmen_id' => Yii::t('app', 'Asesmen ID'),
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Asesmen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesmen()
    {
        return $this->hasOne(Asesmen::class, ['id' => 'asesmen_id']);
    }

    /**
     * Gets query for [[Dokumen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumen()
    {
        return $this->hasOne(Dokumen::class, ['id' => 'dokumen_id']);
    }
}
