<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami_unit".
 *
 * @property int $id
 * @property int|null $unit_id
 * @property int|null $ami_id
 * @property int|null $status_ami
 * @property int|null $jenis_unit
 * @property string|null $auditee
 * @property string|null $email_auditee
 * @property string|null $lokasi
 * @property string|null $wakil_auditee
 * @property string|null $penanggung_jawab
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Ami $ami
 * @property AmiAuditor[] $amiAuditors
 * @property AmiLaporan[] $amiLaporans
 * @property AmiTujuanAudit[] $amiTujuanAudits
 * @property Asesmen[] $asesmens
 * @property Jadwal[] $jadwals
 * @property Persetujuan[] $persetujuans
 * @property RuangLingkup[] $ruangLingkups
 * @property Temuan[] $temuans
 * @property UnitKerja $unit
 */
class AmiUnit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami_unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_id', 'ami_id', 'status_ami', 'jenis_unit'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['auditee', 'email_auditee'], 'string', 'max' => 250],
            [['lokasi'], 'string', 'max' => 50],
            [['wakil_auditee'], 'string', 'max' => 100],
            [['penanggung_jawab'], 'string', 'max' => 150],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_id' => 'id']],
            [['ami_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ami::class, 'targetAttribute' => ['ami_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'ami_id' => Yii::t('app', 'Ami ID'),
            'status_ami' => Yii::t('app', 'Status Ami'),
            'jenis_unit' => Yii::t('app', 'Jenis Unit'),
            'auditee' => Yii::t('app', 'Auditee'),
            'email_auditee' => Yii::t('app', 'Email Auditee'),
            'lokasi' => Yii::t('app', 'Lokasi'),
            'wakil_auditee' => Yii::t('app', 'Wakil Auditee'),
            'penanggung_jawab' => Yii::t('app', 'Penanggung Jawab'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Ami]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmi()
    {
        return $this->hasOne(Ami::class, ['id' => 'ami_id']);
    }

    /**
     * Gets query for [[AmiAuditors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiAuditors()
    {
        return $this->hasMany(AmiAuditor::class, ['ami_id' => 'id']);
    }

    /**
     * Gets query for [[AmiLaporans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiLaporans()
    {
        return $this->hasMany(AmiLaporan::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[AmiTujuanAudits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiTujuanAudits()
    {
        return $this->hasMany(AmiTujuanAudit::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[Asesmens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesmens()
    {
        return $this->hasMany(Asesmen::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[Jadwals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[Persetujuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersetujuans()
    {
        return $this->hasMany(Persetujuan::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[RuangLingkups]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuangLingkups()
    {
        return $this->hasMany(RuangLingkup::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[Temuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemuans()
    {
        return $this->hasMany(Temuan::class, ['ami_unit_id' => 'id']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_id']);
    }
}
