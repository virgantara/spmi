<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akreditasi".
 *
 * @property string $id
 * @property string $lembaga_akreditasi_id
 * @property int $prodi_id
 * @property string $status_akreditasi
 * @property string $tanggal_sk
 * @property string $tanggal_kadaluarsa
 * @property string $nomor_sk
 *
 * @property LembagaAkreditasi $lembagaAkreditasi
 * @property Prodi $prodi
 */
class Akreditasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'akreditasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'lembaga_akreditasi_id', 'prodi_id', 'status_akreditasi', 'tanggal_sk', 'tanggal_kadaluarsa', 'nomor_sk'], 'required'],
            [['prodi_id', 'urutan'], 'integer'],
            [['tanggal_sk', 'tanggal_kadaluarsa'], 'safe'],
            [['id', 'lembaga_akreditasi_id', 'nomor_sk'], 'string', 'max' => 100],
            [['status_akreditasi'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['lembaga_akreditasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => LembagaAkreditasi::class, 'targetAttribute' => ['lembaga_akreditasi_id' => 'id']],
            [['prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prodi::class, 'targetAttribute' => ['prodi_id' => 'id']],
            [['file_path'], 'file', 'skipOnEmpty' => true, 'extensions' => ['pdf'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lembaga_akreditasi_id' => Yii::t('app', 'Lembaga Akreditasi ID'),
            'prodi_id' => Yii::t('app', 'Prodi ID'),
            'status_akreditasi' => Yii::t('app', 'Status Akreditasi'),
            'tanggal_sk' => Yii::t('app', 'Tanggal Sk'),
            'tanggal_kadaluarsa' => Yii::t('app', 'Tanggal Kadaluarsa'),
            'nomor_sk' => Yii::t('app', 'Nomor Sk'),
            'file_path' => Yii::t('app', 'File Path'),
            'file_path_base64' => Yii::t('app', 'File Path Base64'),
            'urutan' => Yii::t('app', 'Urutan'),
        ];
    }

    public function getNamaLembagaAkreditasi()
    {
        return (!empty($this->lembagaAkreditasi) ? $this->lembagaAkreditasi->singkatan_lembaga : null);
    }

    public function getNamaProdi()
    {
        return (!empty($this->prodi) ? $this->prodi->nama_prodi : null);
    }

    /**
     * Gets query for [[LembagaAkreditasi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLembagaAkreditasi()
    {
        return $this->hasOne(LembagaAkreditasi::class, ['id' => 'lembaga_akreditasi_id']);
    }

    /**
     * Gets query for [[Prodi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(Prodi::class, ['id' => 'prodi_id']);
    }
}
