<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruang_lingkup".
 *
 * @property int $id
 * @property string|null $ruang_lingkup
 * @property int|null $ami_unit_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AmiUnit $amiUnit
 */
class RuangLingkup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruang_lingkup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ruang_lingkup'], 'string'],
            [['ami_unit_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ruang_lingkup' => Yii::t('app', 'Ruang Lingkup'),
            'ami_unit_id' => Yii::t('app', 'Ami Unit ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }
}
