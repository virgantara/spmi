<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenjang_bobot".
 *
 * @property int $id
 * @property int|null $indikator_id
 * @property int|null $jenjang_id
 * @property int|null $bobot
 *
 * @property Indikator $indikator
 * @property Jenjang $jenjang
 */
class JenjangBobot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenjang_bobot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['indikator_id', 'jenjang_id', 'bobot'], 'integer'],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::class, 'targetAttribute' => ['indikator_id' => 'id']],
            [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::class, 'targetAttribute' => ['jenjang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indikator_id' => 'Indikator ID',
            'jenjang_id' => 'Jenjang ID',
            'bobot' => 'Bobot',
        ];
    }

    /**
     * Gets query for [[Indikator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::class, ['id' => 'indikator_id']);
    }

    /**
     * Gets query for [[Jenjang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::class, ['id' => 'jenjang_id']);
    }
}
