<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami".
 *
 * @property int $id
 * @property string $nama
 * @property int $tahun
 * @property string|null $tanggal_ami_mulai
 * @property string|null $tanggal_ami_selesai
 * @property string|null $tanggal_visitasi_mulai
 * @property string|null $tanggal_visitasi_selesai
 * @property string|null $tanggal_penilaian_mulai
 * @property string|null $tanggal_penilaian_selesai
 * @property string|null $keterangan
 * @property string|null $status_aktif
 * @property int|null $periode_id
 * @property string|null $form_rencana_kode
 * @property string|null $form_rencana_tanggal_pembuatan
 * @property string|null $form_rencana_tanggal_revisi
 * @property string|null $form_rencana_tanggal_efektif
 * @property string|null $form_tilik_kode
 * @property string|null $form_tilik_tanggal_pembuatan
 * @property string|null $form_tilik_tanggal_revisi
 * @property string|null $form_tilik_tanggal_efektif
 * @property string|null $form_temuan_kode
 * @property string|null $form_temuan_tanggal_pembuatan
 * @property string|null $form_temuan_tanggal_revisi
 * @property string|null $form_temuan_tanggal_efektif
 * @property string|null $form_stemuan_kode
 * @property string|null $form_stemuan_tanggal_pembuatan
 * @property string|null $form_stemuan_tanggal_revisi
 * @property string|null $form_stemuan_tanggal_efektif
 * @property string|null $form_berita_kode
 * @property string|null $form_berita_tanggal_pembuatan
 * @property string|null $form_berita_tanggal_revisi
 * @property string|null $form_berita_tanggal_efektif
 * @property string|null $form_atl_kode
 * @property string|null $form_atl_tanggal_pembuatan
 * @property string|null $form_atl_tanggal_revisi
 * @property string|null $form_atl_tanggal_efektif
 * @property string|null $form_kehadiran_kode
 * @property string|null $form_kehadiran_tanggal_pembuatan
 * @property string|null $form_kehadiran_tanggal_revisi
 * @property string|null $form_kehadiran_tanggal_efektif
 * @property string|null $nomor_surat_tugas
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $ketua_bpm
 * @property string|null $nidn_ketua_bpm
 * @property string|null $path_cover
 *
 * @property AmiLaporan[] $amiLaporans
 * @property AmiUnit[] $amiUnits
 * @property Indikator[] $indikators
 * @property Periode $periode
 */
class Ami extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tahun'], 'required'],
            [['tahun', 'periode_id'], 'integer'],
            [['tanggal_ami_mulai', 'tanggal_ami_selesai', 'tanggal_visitasi_mulai', 'tanggal_visitasi_selesai', 'tanggal_penilaian_mulai', 'tanggal_penilaian_selesai', 'created_at', 'updated_at'], 'safe'],
            [['keterangan'], 'string'],
            [['nama'], 'string', 'max' => 255],
            [['status_aktif'], 'string', 'max' => 1],
            [['form_rencana_kode', 'form_rencana_tanggal_pembuatan', 'form_rencana_tanggal_revisi', 'form_rencana_tanggal_efektif', 'form_tilik_kode', 'form_tilik_tanggal_pembuatan', 'form_tilik_tanggal_revisi', 'form_tilik_tanggal_efektif', 'form_temuan_kode', 'form_temuan_tanggal_pembuatan', 'form_temuan_tanggal_revisi', 'form_temuan_tanggal_efektif', 'form_stemuan_kode', 'form_stemuan_tanggal_pembuatan', 'form_stemuan_tanggal_revisi', 'form_stemuan_tanggal_efektif', 'form_berita_kode', 'form_berita_tanggal_pembuatan', 'form_berita_tanggal_revisi', 'form_berita_tanggal_efektif', 'form_atl_kode', 'form_atl_tanggal_pembuatan', 'form_atl_tanggal_revisi', 'form_atl_tanggal_efektif', 'form_kehadiran_kode', 'form_kehadiran_tanggal_pembuatan', 'form_kehadiran_tanggal_revisi', 'form_kehadiran_tanggal_efektif', 'nomor_surat_tugas'], 'string', 'max' => 50],
            [['ketua_bpm', 'path_cover'], 'string', 'max' => 500],
            [['nidn_ketua_bpm'], 'string', 'max' => 20],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::class, 'targetAttribute' => ['periode_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'tahun' => Yii::t('app', 'Tahun'),
            'tanggal_ami_mulai' => Yii::t('app', 'Tanggal Ami Mulai'),
            'tanggal_ami_selesai' => Yii::t('app', 'Tanggal Ami Selesai'),
            'tanggal_visitasi_mulai' => Yii::t('app', 'Tanggal Visitasi Mulai'),
            'tanggal_visitasi_selesai' => Yii::t('app', 'Tanggal Visitasi Selesai'),
            'tanggal_penilaian_mulai' => Yii::t('app', 'Tanggal Penilaian Mulai'),
            'tanggal_penilaian_selesai' => Yii::t('app', 'Tanggal Penilaian Selesai'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'periode_id' => Yii::t('app', 'Periode ID'),
            'form_rencana_kode' => Yii::t('app', 'Form Rencana Kode'),
            'form_rencana_tanggal_pembuatan' => Yii::t('app', 'Form Rencana Tanggal Pembuatan'),
            'form_rencana_tanggal_revisi' => Yii::t('app', 'Form Rencana Tanggal Revisi'),
            'form_rencana_tanggal_efektif' => Yii::t('app', 'Form Rencana Tanggal Efektif'),
            'form_tilik_kode' => Yii::t('app', 'Form Tilik Kode'),
            'form_tilik_tanggal_pembuatan' => Yii::t('app', 'Form Tilik Tanggal Pembuatan'),
            'form_tilik_tanggal_revisi' => Yii::t('app', 'Form Tilik Tanggal Revisi'),
            'form_tilik_tanggal_efektif' => Yii::t('app', 'Form Tilik Tanggal Efektif'),
            'form_temuan_kode' => Yii::t('app', 'Form Temuan Kode'),
            'form_temuan_tanggal_pembuatan' => Yii::t('app', 'Form Temuan Tanggal Pembuatan'),
            'form_temuan_tanggal_revisi' => Yii::t('app', 'Form Temuan Tanggal Revisi'),
            'form_temuan_tanggal_efektif' => Yii::t('app', 'Form Temuan Tanggal Efektif'),
            'form_stemuan_kode' => Yii::t('app', 'Form Stemuan Kode'),
            'form_stemuan_tanggal_pembuatan' => Yii::t('app', 'Form Stemuan Tanggal Pembuatan'),
            'form_stemuan_tanggal_revisi' => Yii::t('app', 'Form Stemuan Tanggal Revisi'),
            'form_stemuan_tanggal_efektif' => Yii::t('app', 'Form Stemuan Tanggal Efektif'),
            'form_berita_kode' => Yii::t('app', 'Form Berita Kode'),
            'form_berita_tanggal_pembuatan' => Yii::t('app', 'Form Berita Tanggal Pembuatan'),
            'form_berita_tanggal_revisi' => Yii::t('app', 'Form Berita Tanggal Revisi'),
            'form_berita_tanggal_efektif' => Yii::t('app', 'Form Berita Tanggal Efektif'),
            'form_atl_kode' => Yii::t('app', 'Form Atl Kode'),
            'form_atl_tanggal_pembuatan' => Yii::t('app', 'Form Atl Tanggal Pembuatan'),
            'form_atl_tanggal_revisi' => Yii::t('app', 'Form Atl Tanggal Revisi'),
            'form_atl_tanggal_efektif' => Yii::t('app', 'Form Atl Tanggal Efektif'),
            'form_kehadiran_kode' => Yii::t('app', 'Form Kehadiran Kode'),
            'form_kehadiran_tanggal_pembuatan' => Yii::t('app', 'Form Kehadiran Tanggal Pembuatan'),
            'form_kehadiran_tanggal_revisi' => Yii::t('app', 'Form Kehadiran Tanggal Revisi'),
            'form_kehadiran_tanggal_efektif' => Yii::t('app', 'Form Kehadiran Tanggal Efektif'),
            'nomor_surat_tugas' => Yii::t('app', 'Nomor Surat Tugas'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'ketua_bpm' => Yii::t('app', 'Ketua Bpm'),
            'nidn_ketua_bpm' => Yii::t('app', 'Nidn Ketua Bpm'),
            'path_cover' => Yii::t('app', 'Path Cover'),
        ];
    }

    /**
     * Gets query for [[AmiLaporans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiLaporans()
    {
        return $this->hasMany(AmiLaporan::class, ['ami_id' => 'id']);
    }

    /**
     * Gets query for [[AmiUnits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnits()
    {
        return $this->hasMany(AmiUnit::class, ['ami_id' => 'id']);
    }

    /**
     * Gets query for [[Indikators]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikators()
    {
        return $this->hasMany(Indikator::class, ['ami_id' => 'id']);
    }

    /**
     * Gets query for [[Periode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::class, ['id' => 'periode_id']);
    }
}
