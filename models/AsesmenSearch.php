<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Asesmen;

/**
 * AsesmenSearch represents the model behind the search form of `app\models\Asesmen`.
 */
class AsesmenSearch extends Asesmen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'indikator_id', 'skor_ed', 'skor_ak1', 'skor_ak2', 'skor_ak3', 'skor_al1', 'skor_al2', 'skor_al3', 'ami_unit_id', 'dokumen_id', 'pengelompokan_id', 'jenis_dokumen'], 'integer'],
            [['link_bukti', 't_auditor', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $amiUnitId = null, $amiId = null)
    {
        $query = Asesmen::find();

        if ($amiUnitId != null)
            $query->joinWith(['amiUnit as a'])->andWhere(['a.id' => $amiUnitId]);

        if ($amiId != null)
            $query->joinWith(['amiUnit.ami as aa'])->andWhere(['aa.id' => $amiId]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // if(Yii::$app->user->identity->access_role == ('satker'))
        // {
        // }


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'indikator_id' => $this->indikator_id,
            'skor_ed' => $this->skor_ed,
            'skor_ak1' => $this->skor_ak1,
            'skor_ak2' => $this->skor_ak2,
            'skor_ak3' => $this->skor_ak3,
            'skor_al1' => $this->skor_al1,
            'skor_al2' => $this->skor_al2,
            'skor_al3' => $this->skor_al3,
            'ami_unit_id' => $this->ami_unit_id, 
            'created_at' => $this->created_at,
            'dokumen_id' => $this->dokumen_id,
            'pengelompokan_id' => $this->pengelompokan_id,
            'jenis_dokumen' => $this->jenis_dokumen,
        ]);

        $query->andFilterWhere(['like', 'link_bukti', $this->link_bukti])
            ->andFilterWhere(['like', 't_auditor', $this->t_auditor]);

        return $dataProvider;
    }
}
