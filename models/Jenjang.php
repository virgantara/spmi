<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenjang".
 *
 * @property int $id
 * @property string|null $nama
 * @property int|null $urutan
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property JenjangBobot[] $jenjangBobots
 * @property ProdiJenjang[] $prodiJenjangs
 */
class Jenjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenjang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urutan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'urutan' => 'Urutan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[JenjangBobots]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangBobots()
    {
        return $this->hasMany(JenjangBobot::class, ['jenjang_id' => 'id']);
    }

    /**
     * Gets query for [[ProdiJenjangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdiJenjangs()
    {
        return $this->hasMany(ProdiJenjang::class, ['jenjang_id' => 'id']);
    }
}
