<?php

namespace app\models;

use app\helpers\MyHelper;
use Yii;

/**
 * This is the model class for table "persetujuan_cek".
 *
 * @property int $id
 * @property string|null $kode_dokumen
 * @property string|null $nama_kegiatan
 * @property string|null $nama_dokumen
 * @property int|null $unit_kerja_id
 * @property string|null $tahun
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property UnitKerja $unitKerja
 */
class PersetujuanCek extends \yii\db\ActiveRecord
{
    public $captcha;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persetujuan_cek';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['captcha', 'captcha', 'on' => 'captchaRequired'],
            [['unit_kerja_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_dokumen', 'nama_kegiatan', 'nama_dokumen', 'tahun'], 'string', 'max' => 100],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode_dokumen' => Yii::t('app', 'Kode Dokumen'),
            'nama_kegiatan' => Yii::t('app', 'Nama Kegiatan'),
            'nama_dokumen' => Yii::t('app', 'Nama Dokumen'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'tahun' => Yii::t('app', 'Tahun'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }


    public static function inputDokumenTervalidasi($id, $kodeKegiatan = 'AMI', $namaDokumen = 'surat-tugas-ami')
    {
        if ($kodeKegiatan == "AMI") {
            $acara = AmiUnit::findOne($id);
        } elseif ($kodeKegiatan == "RTM") {
            $acara = Rtm::findOne($id);
        }

        $kodeTahun =  $acara->ami->tahun ?? $acara->tahun;
        $unitKerjaId = $acara->unit_id;
        $DokumenId = array_column(MyHelper::listDokumenAmi(), 'id', 'kode')[$namaDokumen];
        $kodeUnit = trim($unitKerjaId);
        $kodeUnit = str_pad($kodeUnit, 4, '0', STR_PAD_LEFT);
        $kodeDokumen = $kodeKegiatan . $DokumenId . $kodeUnit . $kodeTahun;

        $cekDokumen = PersetujuanCek::findOne(['kode_dokumen' => $kodeDokumen]);
        if (empty($cekDokumen)) {
            
            $validateDokumen = new PersetujuanCek();
            $validateDokumen->kode_dokumen = (string)$kodeDokumen;
            $validateDokumen->nama_kegiatan = $kodeKegiatan;
            $validateDokumen->nama_dokumen = $namaDokumen;
            $validateDokumen->unit_kerja_id = $unitKerjaId;
            $validateDokumen->tahun = (string)$kodeTahun;

            if ($validateDokumen->save()) {
                return 'berhasil';
            } else {
                $error = MyHelper::logError($validateDokumen);
                echo $error;
                return 'gagal';
            }
        } else
            return 'skip';
    }
}
