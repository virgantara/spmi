<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmiUnit;

/**
 * AmiUnitSearch represents the model behind the search form of `app\models\AmiUnit`.
 */
class AmiUnitSearch extends AmiUnit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'ami_id', 'status_ami', 'jenis_unit'], 'integer'],
            [['auditee', 'email_auditee', 'lokasi', 'wakil_auditee', 'penanggung_jawab', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {

        $query = AmiUnit::find();

        // add conditions that should always apply here

        if (!empty($filters['unit_id'])) $query->andWhere(['unit_id' => $filters['unit_id']]);
        if (!empty($filters['ami_id'])) $query->andWhere(['ami_unit.ami_id' => $filters['ami_id']]);

        $query->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (Yii::$app->user->identity->access_role == 'auditee') {
            $query->andWhere(['unit_id' => Yii::$app->user->identity->unit_kerja_id]);
        }

        if (Yii::$app->user->identity->access_role == 'auditor') {
            $query->joinWith(['amiAuditors as aa']);
            $query->andWhere(['aa.auditor_id' => Yii::$app->user->identity->auditor_id]);
        }


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
            'ami_id' => $this->ami_id,
            'status_ami' => $this->status_ami,
            'jenis_unit' => $this->jenis_unit,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'auditee', $this->auditee])
            ->andFilterWhere(['like', 'email_auditee', $this->email_auditee])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'wakil_auditee', $this->wakil_auditee])
            ->andFilterWhere(['like', 'penanggung_jawab', $this->penanggung_jawab]);

        return $dataProvider;
    }
}
