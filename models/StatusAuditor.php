<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status_auditor".
 *
 * @property int $id
 * @property string|null $nama
 *
 * @property AmiAuditor[] $amiAuditors
 */
class StatusAuditor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_auditor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[AmiAuditors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiAuditors()
    {
        return $this->hasMany(AmiAuditor::class, ['status_id' => 'id']);
    }
}
