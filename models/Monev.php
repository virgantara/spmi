<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monev".
 *
 * @property string $id
 * @property string|null $nama_monev
 * @property string|null $tahun
 * @property int|null $periode
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property MonevUnitKerja[] $monevUnitKerjas
 */
class Monev extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monev';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['periode', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['nama_monev'], 'string', 'max' => 300],
            [['tahun'], 'string', 'max' => 5],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_monev' => Yii::t('app', 'Nama Monev'),
            'tahun' => Yii::t('app', 'Tahun'),
            'periode' => Yii::t('app', 'Periode'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[MonevUnitKerjas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerjas()
    {
        return $this->hasMany(MonevUnitKerja::class, ['monev_id' => 'id']);
    }
}
