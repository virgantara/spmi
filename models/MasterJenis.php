<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_jenis".
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $value
 * @property int|null $urutan
 * @property int|null $is_khusus
 * @property string|null $set_to
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Dokumen[] $dokumens
 * @property Peningkatan[] $peningkatans
 */
class MasterJenis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_jenis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urutan', 'is_khusus', 'status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 250],
            [['value'], 'string', 'max' => 100],
            [['set_to'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'value' => Yii::t('app', 'Value'),
            'urutan' => Yii::t('app', 'Urutan'),
            'is_khusus' => Yii::t('app', 'Is Khusus'),
            'set_to' => Yii::t('app', 'Set To'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Dokumens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumens()
    {
        return $this->hasMany(Dokumen::class, ['jenis_id' => 'id']);
    }

    /**
     * Gets query for [[Peningkatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeningkatans()
    {
        return $this->hasMany(Peningkatan::class, ['master_jenis_id' => 'id']);
    }
}
