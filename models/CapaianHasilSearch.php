<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CapaianHasil;

/**
 * CapaianHasilSearch represents the model behind the search form of `app\models\CapaianHasil`.
 */
class CapaianHasilSearch extends CapaianHasil
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'unit_kerja_id', 'capaian_kinerja_id', 'periode_id', 'realisasi_target'], 'integer'],
            [['capaian_target'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CapaianHasil::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'unit_kerja_id' => $this->unit_kerja_id,
            'capaian_kinerja_id' => $this->capaian_kinerja_id,
            'periode_id' => $this->periode_id,
            'realisasi_target' => $this->realisasi_target,
        ]);

        $query->andFilterWhere(['like', 'capaian_target', $this->capaian_target]);

        return $dataProvider;
    }
}
