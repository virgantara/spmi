<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lembaga_akreditasi".
 *
 * @property string $id
 * @property string $tingkat
 * @property string $nama_lembaga
 */
class LembagaAkreditasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lembaga_akreditasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tingkat', 'nama_lembaga'], 'required'],
            [['id','singkatan_lembaga'], 'string', 'max' => 100],
            [['tingkat','status_aktif'], 'string', 'max' => 1],
            [['nama_lembaga'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tingkat' => 'Tingkat',
            'nama_lembaga' => 'Nama Lembaga',
        ];
    }
}
