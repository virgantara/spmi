<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PeningkatanRiwayat;

/**
 * PeningkatanRiwayatSearch represents the model behind the search form of `app\models\PeningkatanRiwayat`.
 */
class PeningkatanRiwayatSearch extends PeningkatanRiwayat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'peningkatan_id', 'indikator_id', 'unit_kerja_id', 'dokumen_id'], 'integer'],
            [['nama_dokumen', 'pernyataan_peningkatan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PeningkatanRiwayat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'peningkatan_id' => $this->peningkatan_id,
            'indikator_id' => $this->indikator_id,
            'unit_kerja_id' => $this->unit_kerja_id,
            'dokumen_id' => $this->dokumen_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_dokumen', $this->nama_dokumen])
            ->andFilterWhere(['like', 'pernyataan_peningkatan', $this->pernyataan_peningkatan]);

        return $dataProvider;
    }
}
