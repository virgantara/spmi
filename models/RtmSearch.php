<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rtm;

/**
 * RtmSearch represents the model behind the search form of `app\models\Rtm`.
 */
class RtmSearch extends Rtm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_id', 'unit_kerja_id', 'tingkatan'], 'integer'],
            [['nama_kepala', 'ketua_panitia', 'tema', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rtm::find();

        $query->orderBy(['id' => SORT_DESC]);
        $identity = Yii::$app->user->identity;

        if ($identity->access_role == 'auditee') {

            $pengecualian = ['bpm', 'yptd', 'senat'];

            if ($identity->unitKerja->jenis == 'satker') {
                $unit = $identity->unitKerja->singkatan;

                if (in_array(strtolower($unit), $pengecualian)) {
                    $query->andWhere(['unit_kerja_id' => $identity->unit_kerja_id]);
                } else {
                    $query->andWhere(['unit_kerja_id' => $identity->unitKerja->parent_id]);
                }
            } elseif ($identity->unitKerja->jenis == 'prodi') {
                $query->andWhere(['unit_kerja_id' => $identity->unitKerja->parent_id]);
            } elseif ($identity->unitKerja->jenis == 'fakultas') {
                $query->andWhere(['unit_kerja_id' => $identity->unit_kerja_id]);
            } elseif ($identity->unitKerja->jenis == 'koordinator') {
                $query->andWhere(['unit_kerja_id' => $identity->unit_kerja_id]);
            }
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ami_id' => $this->ami_id,
            'unit_kerja_id' => $this->unit_kerja_id,
            'tingkatan' => $this->tingkatan,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_kepala', $this->nama_kepala])
            ->andFilterWhere(['like', 'ketua_panitia', $this->ketua_panitia])
            ->andFilterWhere(['like', 'tema', $this->tema]);

        return $dataProvider;
    }
}
