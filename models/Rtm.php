<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rtm".
 *
 * @property int $id
 * @property int|null $ami_id
 * @property int|null $unit_kerja_id
 * @property string|null $nama_kepala
 * @property string|null $ketua_panitia
 * @property string|null $tema
 * @property string|null $tingkatan
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $pendahuluan
 *
 * @property Ami $ami
 * @property BahanRtm[] $bahanRtms
 * @property Jadwal[] $jadwals
 * @property UnitKerja $unitKerja
 */
class Rtm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rtm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ami_id', 'unit_kerja_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['pendahuluan'], 'string'],
            [['nama_kepala', 'ketua_panitia'], 'string', 'max' => 150],
            [['tema'], 'string', 'max' => 300],
            [['tingkatan'], 'string', 'max' => 50],
            [['ami_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ami::class, 'targetAttribute' => ['ami_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ami_id' => Yii::t('app', 'Unit'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja'),
            'nama_kepala' => Yii::t('app', 'Nama Kepala'),
            'ketua_panitia' => Yii::t('app', 'Ketua Panitia'),
            'tema' => Yii::t('app', 'Tema'),
            'tingkatan' => Yii::t('app', 'Tingkatan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'pendahuluan' => Yii::t('app', 'Pendahuluan'),
        ];
    }

    /**
     * Gets query for [[Ami]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmi()
    {
        return $this->hasOne(Ami::class, ['id' => 'ami_id']);
    }

    /**
     * Gets query for [[BahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtms()
    {
        return $this->hasMany(BahanRtm::class, ['rtm_id' => 'id']);
    }

    /**
     * Gets query for [[Jadwals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::class, ['rtm_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
