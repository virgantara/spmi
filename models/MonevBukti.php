<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monev_bukti".
 *
 * @property string $id
 * @property string|null $monev_unit_kerja_id
 * @property string|null $monev_unit_kerja_objek_id
 * @property int|null $dokumen_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Dokumen $dokumen
 * @property MonevUnitKerja $monevUnitKerja
 * @property MonevUnitKerjaObjek $monevUnitKerjaObjek
 */
class MonevBukti extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monev_bukti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['dokumen_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'monev_unit_kerja_id', 'monev_unit_kerja_objek_id'], 'string', 'max' => 36],
            [['id'], 'unique'],
            [['dokumen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dokumen::class, 'targetAttribute' => ['dokumen_id' => 'id']],
            [['monev_unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => MonevUnitKerja::class, 'targetAttribute' => ['monev_unit_kerja_id' => 'id']],
            [['monev_unit_kerja_objek_id'], 'exist', 'skipOnError' => true, 'targetClass' => MonevUnitKerjaObjek::class, 'targetAttribute' => ['monev_unit_kerja_objek_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'monev_unit_kerja_id' => Yii::t('app', 'Monev Unit Kerja ID'),
            'monev_unit_kerja_objek_id' => Yii::t('app', 'Monev Unit Kerja Objek ID'),
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Dokumen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumen()
    {
        return $this->hasOne(Dokumen::class, ['id' => 'dokumen_id']);
    }

    /**
     * Gets query for [[MonevUnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerja()
    {
        return $this->hasOne(MonevUnitKerja::class, ['id' => 'monev_unit_kerja_id']);
    }

    /**
     * Gets query for [[MonevUnitKerjaObjek]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerjaObjek()
    {
        return $this->hasOne(MonevUnitKerjaObjek::class, ['id' => 'monev_unit_kerja_objek_id']);
    }
}
