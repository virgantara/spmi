<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_kerja".
 *
 * @property int $id
 * @property string|null $jenis
 * @property string $nama
 * @property string|null $singkatan
 * @property string|null $surat
 * @property string|null $kode_prodi
 * @property int|null $parent_id
 * @property int|null $jenjang_id
 * @property int|null $variable_id
 * @property int|null $is_sent
 * @property string|null $email
 * @property string|null $penanggung_jawab
 * @property string|null $kode_unit
 *
 * @property AmanahKinerjaMaster[] $amanahKinerjaMasters
 * @property AmanahKinerjaUnitKerja[] $amanahKinerjaUnitKerjas
 * @property AmiUnit[] $amiUnits
 * @property BahanRtm[] $bahanRtms
 * @property CapaianHasil[] $capaianHasils
 * @property Dokumen[] $dokumens
 * @property Indikator[] $indikators
 * @property JenjangMap[] $jenjangMaps
 * @property MonevUnitKerja[] $monevUnitKerjas
 * @property PembagianProdi[] $pembagianProdis
 * @property PembagianProdi[] $pembagianProdis0
 * @property PembagianRtm[] $pembagianRtms
 * @property PeningkatanRiwayat[] $peningkatanRiwayats
 * @property Peningkatan[] $peningkatans
 * @property PersetujuanCek[] $persetujuanCeks
 * @property Rtm[] $rtms
 * @property UnitKerjaProdi[] $unitKerjaProdis
 * @property User[] $users
 */
class UnitKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit_kerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['parent_id', 'jenjang_id', 'variable_id', 'is_sent'], 'integer'],
            [['jenis', 'nama'], 'string', 'max' => 100],
            [['singkatan'], 'string', 'max' => 20],
            [['surat'], 'string', 'max' => 5],
            [['kode_prodi'], 'string', 'max' => 10],
            [['email', 'penanggung_jawab'], 'string', 'max' => 150],
            [['kode_unit'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jenis' => Yii::t('app', 'Jenis'),
            'nama' => Yii::t('app', 'Nama'),
            'singkatan' => Yii::t('app', 'Singkatan'),
            'surat' => Yii::t('app', 'Surat'),
            'kode_prodi' => Yii::t('app', 'Kode Prodi'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'jenjang_id' => Yii::t('app', 'Jenjang ID'),
            'variable_id' => Yii::t('app', 'Variable ID'),
            'is_sent' => Yii::t('app', 'Is Sent'),
            'email' => Yii::t('app', 'Email'),
            'penanggung_jawab' => Yii::t('app', 'Penanggung Jawab'),
            'kode_unit' => Yii::t('app', 'Kode Unit'),
        ];
    }

    /**
     * Gets query for [[AmanahKinerjaMasters]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerjaMasters()
    {
        return $this->hasMany(AmanahKinerjaMaster::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[AmanahKinerjaUnitKerjas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerjaUnitKerjas()
    {
        return $this->hasMany(AmanahKinerjaUnitKerja::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[AmiUnits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnits()
    {
        return $this->hasMany(AmiUnit::class, ['unit_id' => 'id']);
    }

    /**
     * Gets query for [[BahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtms()
    {
        return $this->hasMany(BahanRtm::class, ['unit_id' => 'id']);
    }

    /**
     * Gets query for [[CapaianHasils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianHasils()
    {
        return $this->hasMany(CapaianHasil::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[Dokumens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumens()
    {
        return $this->hasMany(Dokumen::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[Indikators]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikators()
    {
        return $this->hasMany(Indikator::class, ['unit_id' => 'id']);
    }

    /**
     * Gets query for [[JenjangMaps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangMaps()
    {
        return $this->hasMany(JenjangMap::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[MonevUnitKerjas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerjas()
    {
        return $this->hasMany(MonevUnitKerja::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[PembagianProdis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembagianProdis()
    {
        return $this->hasMany(PembagianProdi::class, ['fakultas_id' => 'id']);
    }

    /**
     * Gets query for [[PembagianProdis0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembagianProdis0()
    {
        return $this->hasMany(PembagianProdi::class, ['prodi_id' => 'id']);
    }

    /**
     * Gets query for [[PembagianRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembagianRtms()
    {
        return $this->hasMany(PembagianRtm::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[PeningkatanRiwayats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeningkatanRiwayats()
    {
        return $this->hasMany(PeningkatanRiwayat::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[Peningkatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeningkatans()
    {
        return $this->hasMany(Peningkatan::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[PersetujuanCeks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersetujuanCeks()
    {
        return $this->hasMany(PersetujuanCek::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[Rtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRtms()
    {
        return $this->hasMany(Rtm::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerjaProdis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjaProdis()
    {
        return $this->hasMany(UnitKerjaProdi::class, ['unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['unit_kerja_id' => 'id']);
    }
}
