<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temuan".
 *
 * @property int $id
 * @property string|null $uraian_k
 * @property string|null $referensi
 * @property string|null $akar_penyebab
 * @property string|null $rekomendasi
 * @property string|null $kategori
 * @property string|null $jadwal_perbaikan
 * @property string|null $penanggung_jawab
 * @property string|null $created_at
 * @property string $updated_at
 * @property int|null $ami_unit_id
 * @property string|null $jabatan_pj
 * @property int|null $asesmen_id
 * @property int|null $indikator_id
 * @property string|null $rencana_perbaikan
 * @property string|null $rencana_tindak_lanjut
 * @property string|null $jadwal_rtl
 * @property string|null $realisasi_tindak_lanjut
 * @property string|null $efektifitas
 * @property string|null $status
 *
 * @property AmiUnit $amiUnit
 * @property Asesmen $asesmen
 * @property BahanRtm[] $bahanRtms
 * @property Indikator $indikator
 */
class Temuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['ami_unit_id', 'asesmen_id', 'indikator_id'], 'integer'],
            [['uraian_k', 'referensi', 'akar_penyebab', 'rekomendasi', 'kategori', 'jadwal_perbaikan', 'penanggung_jawab'], 'string', 'max' => 255],
            [['jabatan_pj', 'jadwal_rtl', 'efektifitas'], 'string', 'max' => 100],
            [['rencana_perbaikan', 'rencana_tindak_lanjut', 'realisasi_tindak_lanjut'], 'string', 'max' => 500],
            [['status'], 'string', 'max' => 50],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
            [['asesmen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asesmen::class, 'targetAttribute' => ['asesmen_id' => 'id']],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::class, 'targetAttribute' => ['indikator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uraian_k' => Yii::t('app', 'Uraian K'),
            'referensi' => Yii::t('app', 'Referensi'),
            'akar_penyebab' => Yii::t('app', 'Akar Penyebab'),
            'rekomendasi' => Yii::t('app', 'Rekomendasi'),
            'kategori' => Yii::t('app', 'Kategori'),
            'jadwal_perbaikan' => Yii::t('app', 'Jadwal Perbaikan'),
            'penanggung_jawab' => Yii::t('app', 'Penanggung Jawab'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'ami_unit_id' => Yii::t('app', 'Ami Unit ID'),
            'jabatan_pj' => Yii::t('app', 'Jabatan Pj'),
            'asesmen_id' => Yii::t('app', 'Asesmen ID'),
            'indikator_id' => Yii::t('app', 'Indikator ID'),
            'rencana_perbaikan' => Yii::t('app', 'Rencana Perbaikan'),
            'rencana_tindak_lanjut' => Yii::t('app', 'Rencana Tindak Lanjut'),
            'jadwal_rtl' => Yii::t('app', 'Jadwal Rtl'),
            'realisasi_tindak_lanjut' => Yii::t('app', 'Realisasi Tindak Lanjut'),
            'efektifitas' => Yii::t('app', 'Efektifitas'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[Asesmen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesmen()
    {
        return $this->hasOne(Asesmen::class, ['id' => 'asesmen_id']);
    }

    /**
     * Gets query for [[BahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtms()
    {
        return $this->hasMany(BahanRtm::class, ['temuan_id' => 'id']);
    }

    /**
     * Gets query for [[Indikator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::class, ['id' => 'indikator_id']);
    }
}
