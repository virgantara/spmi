<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Persetujuan;

/**
 * PersetujuanSearch represents the model behind the search form of `app\models\Persetujuan`.
 */
class PersetujuanSearch extends Persetujuan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_unit_id', 'auditor_id', 'is_auditee'], 'integer'],
            [['datetime', 'kode_dokumen', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Persetujuan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ami_unit_id' => $this->ami_unit_id,
            'auditor_id' => $this->auditor_id,
            'datetime' => $this->datetime,
            'is_auditee' => $this->is_auditee,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kode_dokumen', $this->kode_dokumen]);

        return $dataProvider;
    }
}
