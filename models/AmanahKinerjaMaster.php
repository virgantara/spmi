<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "amanah_kinerja_master".
 *
 * @property string $id
 * @property int|null $unit_kerja_id
 * @property string|null $jenis_amanah_kinerja
 * @property string|null $amanah_kinerja
 * @property string|null $target_umum
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmanahKinerjaUnitKerja[] $amanahKinerjaUnitKerjas
 * @property UnitKerja $unitKerja
 */
class AmanahKinerjaMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amanah_kinerja_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','jenis_amanah_kinerja'], 'required'],
            [['unit_kerja_id', 'status_aktif'], 'integer'],
            [['amanah_kinerja', 'target_umum'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['jenis_amanah_kinerja'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'jenis_amanah_kinerja' => Yii::t('app', 'Jenis Amanah Kinerja'),
            'amanah_kinerja' => Yii::t('app', 'Amanah Kinerja'),
            'target_umum' => Yii::t('app', 'Target Umum'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmanahKinerjaUnitKerjas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerjaUnitKerjas()
    {
        return $this->hasMany(AmanahKinerjaUnitKerja::class, ['amanah_kinerja_master_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
