<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bagian".
 *
 * @property int $id
 * @property int|null $id_auditor
 * @property int|null $bagian
 * @property int|null $id_prodi
 * @property string|null $created_at
 */
class Bagian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bagian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_auditor', 'bagian', 'id_prodi'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_auditor' => 'Id Auditor',
            'bagian' => 'Bagian',
            'id_prodi' => 'Id Prodi',
            'created_at' => 'Created At',
        ];
    }
}
