<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembagian_prodi".
 *
 * @property int $id
 * @property int|null $fakultas_id
 * @property int|null $prodi_id
 *
 * @property UnitKerja $fakultas
 * @property UnitKerja $prodi
 */
class PembagianProdi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembagian_prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fakultas_id', 'prodi_id'], 'integer'],
            [['fakultas_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['fakultas_id' => 'id']],
            [['prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['prodi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fakultas_id' => 'Fakultas ID',
            'prodi_id' => 'Prodi ID',
        ];
    }

    /**
     * Gets query for [[Fakultas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFakultas()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'fakultas_id']);
    }

    /**
     * Gets query for [[Prodi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'prodi_id']);
    }
}
