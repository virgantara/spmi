<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monev_unit_kerja".
 *
 * @property string $id
 * @property string|null $monev_id
 * @property int|null $unit_kerja_id
 * @property int|null $dokumen_id
 * @property string|null $pernyataan_peningkatan
 * @property int|null $persetujuan
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Monev $monev
 * @property MonevUnitKerjaObjek[] $monevUnitKerjaObjeks
 * @property UnitKerja $unitKerja
 */
class MonevUnitKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monev_unit_kerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['unit_kerja_id', 'dokumen_id', 'persetujuan', 'status'], 'integer'],
            [['pernyataan_peningkatan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'monev_id'], 'string', 'max' => 36],
            [['id'], 'unique'],
            [['monev_id'], 'exist', 'skipOnError' => true, 'targetClass' => Monev::class, 'targetAttribute' => ['monev_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'monev_id' => Yii::t('app', 'Monev ID'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'pernyataan_peningkatan' => Yii::t('app', 'Pernyataan Peningkatan'),
            'persetujuan' => Yii::t('app', 'Persetujuan'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Monev]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonev()
    {
        return $this->hasOne(Monev::class, ['id' => 'monev_id']);
    }

    /**
     * Gets query for [[MonevUnitKerjaObjeks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerjaObjeks()
    {
        return $this->hasMany(MonevUnitKerjaObjek::class, ['monev_unit_kerja_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
