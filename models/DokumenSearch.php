<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dokumen;

/**
 * DokumenSearch represents the model behind the search form of `app\models\Dokumen`.
 */
class DokumenSearch extends Dokumen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'jenis_id', 'unit_kerja_id'], 'integer'],
            [['nama', 'link', 's3', 'keterangan', 'created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $condition = null, $filters = [])
    {

        $query = Dokumen::find();

        if (!empty($filters)) {
            if (empty($filters['nama'])) {
                $query->andWhere($filters);
            } else {
                $query->andWhere(['like', 'nama', $filters['nama']]);

                if (!empty($filters['jenis_id'])) $query->andWhere(['jenis_id' =>  $filters['jenis_id']]);
                if (!empty($filters['unit_kerja_id'])) $query->andWhere(['unit_kerja_id' => $filters['unit_kerja_id']]);
            }
        }

        if (isset(Yii::$app->user->identity->unit_kerja_id) && $condition == 'personal') {
            $query->andWhere([
                'unit_kerja_id' => Yii::$app->user->identity->unit_kerja_id
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'jenis_id' => $this->jenis_id,
            'unit_kerja_id' => $this->unit_kerja_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 's3', $this->s3])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
