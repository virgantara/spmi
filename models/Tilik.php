<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tilik".
 *
 * @property int $id
 * @property string|null $kriteria
 * @property string|null $pertanyaan
 * @property int|null $asesmen_id
 * @property int|null $auditor_id
 * @property int|null $status
 * @property string|null $catatan
 * @property string|null $created_at
 * @property string $updated_at
 * @property string|null $referensi 
 * @property string|null $bukti
 *
 * @property Asesmen $asesmen
 * @property Auditor $auditor
 */
class Tilik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tilik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asesmen_id', 'auditor_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kriteria', 'referensi', 'bukti'], 'string', 'max' => 255],
            [['pertanyaan', 'catatan'], 'string', 'max' => 500],
            [['asesmen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asesmen::class, 'targetAttribute' => ['asesmen_id' => 'id']],
            [['auditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auditor::class, 'targetAttribute' => ['auditor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kriteria' => Yii::t('app', 'Kriteria'),
            'pertanyaan' => Yii::t('app', 'Pertanyaan'),
            'asesmen_id' => Yii::t('app', 'Asesmen ID'),
            'auditor_id' => Yii::t('app', 'Auditor ID'),
            'status' => Yii::t('app', 'Status'),
            'catatan' => Yii::t('app', 'Catatan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'referensi' => Yii::t('app', 'Referensi'),
            'bukti' => Yii::t('app', 'Bukti'),
        ];
    }

    /**
     * Gets query for [[Asesmen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesmen()
    {
        return $this->hasOne(Asesmen::class, ['id' => 'asesmen_id']);
    }

    /**
     * Gets query for [[Auditor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuditor()
    {
        return $this->hasOne(Auditor::class, ['id' => 'auditor_id']);
    }
}
