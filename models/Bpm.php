<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bpm".
 *
 * @property int $id
 * @property string|null $kepala_bpm
 * @property string|null $niy
 * @property string|null $daftar_hadir_ami
 */
class Bpm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bpm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kepala_bpm'], 'string', 'max' => 150],
            [['niy', 'daftar_hadir_ami'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kepala_bpm' => Yii::t('app', 'Kepala Bpm'),
            'niy' => Yii::t('app', 'Niy'),
            'daftar_hadir_ami' => Yii::t('app', 'Daftar Hadir Ami'),
        ];
    }
}
