<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterJenis;

/**
 * MasterJenisSearch represents the model behind the search form of `app\models\MasterJenis`.
 */
class MasterJenisSearch extends MasterJenis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'urutan', 'is_khusus', 'status_aktif'], 'integer'],
            [['nama', 'value', 'set_to', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = MasterJenis::find();
        
        if (!empty($filters)){
            $query->andWhere($filters);
            $query->orderBy(['urutan' => SORT_ASC]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'urutan' => $this->urutan,
            'is_khusus' => $this->is_khusus,
            'status_aktif' => $this->status_aktif,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'set_to', $this->set_to]);

        return $dataProvider;
    }
}
