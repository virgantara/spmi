<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "indikator".
 *
 * @property int $id
 * @property int $kriteria_id
 * @property string|null $nama
 * @property string|null $skor1
 * @property string|null $skor2
 * @property string|null $skor3
 * @property string|null $skor4
 * @property float|null $bobot
 * @property string|null $status_aktif
 * @property string|null $jenis_indikator
 * @property int|null $unit_id
 * @property int|null $ami_id
 * @property string|null $pernyataan
 * @property string|null $indikator
 * @property string|null $strategi
 * @property string $created_at
 * @property string|null $referensi
 * @property string|null $updated_at
 *
 * @property Ami $ami
 * @property Asesmen[] $asesmens
 * @property JenjangBobot[] $jenjangBobots
 * @property Kriteria $kriteria
 * @property Temuan[] $temuans
 * @property Tilik[] $tiliks
 * @property UnitKerja $unit
 */
class Indikator extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'indikator';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kriteria_id'], 'required'],
            [['kriteria_id', 'unit_id', 'ami_id'], 'integer'],
            [['nama', 'skor1', 'skor2', 'skor3', 'skor4', 'pernyataan', 'indikator', 'strategi', 'referensi'], 'string'],
            [['bobot'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['status_aktif'], 'string', 'max' => 1],
            [['jenis_indikator'], 'string', 'max' => 50],
            [['ami_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ami::class, 'targetAttribute' => ['ami_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_id' => 'id']],
            [['kriteria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kriteria::class, 'targetAttribute' => ['kriteria_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kriteria_id' => Yii::t('app', 'Kriteria'),
            'nama' => Yii::t('app', 'Nama'),
            'skor1' => Yii::t('app', 'Skor1'),
            'skor2' => Yii::t('app', 'Skor2'),
            'skor3' => Yii::t('app', 'Skor3'),
            'skor4' => Yii::t('app', 'Skor4'),
            'bobot' => Yii::t('app', 'Bobot'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'jenis_indikator' => Yii::t('app', 'Jenis Indikator'),
            'unit_id' => Yii::t('app', 'Auditee'),
            'ami_id' => Yii::t('app', 'Periode Ami'),
            'pernyataan' => Yii::t('app', 'Pernyataan'),
            'indikator' => Yii::t('app', 'Indikator'),
            'strategi' => Yii::t('app', 'Strategi'),
            'created_at' => Yii::t('app', 'Created At'),
            'referensi' => Yii::t('app', 'Referensi'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Ami]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmi()
    {
        return $this->hasOne(Ami::class, ['id' => 'ami_id']);
    }

    /**
     * Gets query for [[Asesmens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesmens()
    {
        return $this->hasMany(Asesmen::class, ['indikator_id' => 'id']);
    }

    /**
     * Gets query for [[JenjangBobots]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangBobots()
    {
        return $this->hasMany(JenjangBobot::class, ['indikator_id' => 'id']);
    }

    /**
     * Gets query for [[Kriteria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriteria()
    {
        return $this->hasOne(Kriteria::class, ['id' => 'kriteria_id']);
    }

    /**
     * Gets query for [[Temuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemuans()
    {
        return $this->hasMany(Temuan::class, ['indikator_id' => 'id']);
    }

    /**
     * Gets query for [[Tiliks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiliks()
    {
        return $this->hasMany(Tilik::class, ['indikator_id' => 'id']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_id']);
    }
}
