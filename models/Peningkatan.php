<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peningkatan".
 *
 * @property int $id
 * @property string|null $nama_dokumen
 * @property int|null $indikator_id
 * @property int|null $unit_kerja_id
 * @property int|null $dokumen_id
 * @property int|null $master_jenis_id
 * @property string|null $pernyataan_peningkatan
 * @property int|null $persetujuan
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Dokumen $dokumen
 * @property Indikator $indikator
 * @property MasterJenis $masterJenis
 * @property PeningkatanRiwayat[] $peningkatanRiwayats
 * @property UnitKerja $unitKerja
 */
class Peningkatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peningkatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_dokumen', 'pernyataan_peningkatan'], 'string'],
            [['indikator_id', 'unit_kerja_id', 'dokumen_id', 'master_jenis_id', 'persetujuan', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['dokumen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dokumen::class, 'targetAttribute' => ['dokumen_id' => 'id']],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::class, 'targetAttribute' => ['indikator_id' => 'id']],
            [['master_jenis_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenis::class, 'targetAttribute' => ['master_jenis_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_dokumen' => Yii::t('app', 'Nama Dokumen'),
            'indikator_id' => Yii::t('app', 'Indikator ID'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'master_jenis_id' => Yii::t('app', 'Master Jenis ID'),
            'pernyataan_peningkatan' => Yii::t('app', 'Pernyataan Peningkatan'),
            'persetujuan' => Yii::t('app', 'Persetujuan'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Dokumen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumen()
    {
        return $this->hasOne(Dokumen::class, ['id' => 'dokumen_id']);
    }

    /**
     * Gets query for [[Indikator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::class, ['id' => 'indikator_id']);
    }

    /**
     * Gets query for [[MasterJenis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMasterJenis()
    {
        return $this->hasOne(MasterJenis::class, ['id' => 'master_jenis_id']);
    }

    /**
     * Gets query for [[PeningkatanRiwayats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeningkatanRiwayats()
    {
        return $this->hasMany(PeningkatanRiwayat::class, ['peningkatan_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
