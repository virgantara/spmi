<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penanggung_jawab_rtm".
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $penanggung_jawab
 */
class PenanggungJawabRtm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penanggung_jawab_rtm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 100],
            [['penanggung_jawab'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'penanggung_jawab' => 'Penanggung Jawab',
        ];
    }
}
