<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BahanRtm;

/**
 * BahanRtmSearch represents the model behind the search form of `app\models\BahanRtm`.
 */
class BahanRtmSearch extends BahanRtm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'unit_id', 'temuan_id', 'rtm_id', 'aspek_id', 'lingkup_bahasan_id', 'jenis_bahan', 'persetujuan'], 'integer'],
            [['deskripsi', 'tindakan', 'target_waktu', 'output', 'penanggung_jawab', 'pelaksana', 'aras', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = BahanRtm::find();

        if (!empty($filters)) {
            $query->where($filters);
        }
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'unit_id' => $this->unit_id,
            'temuan_id' => $this->temuan_id,
            'rtm_id' => $this->rtm_id,
            'aspek_id' => $this->aspek_id,
            'lingkup_bahasan_id' => $this->lingkup_bahasan_id,
            'jenis_bahan' => $this->jenis_bahan,
            'persetujuan' => $this->persetujuan,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'tindakan', $this->tindakan])
            ->andFilterWhere(['like', 'target_waktu', $this->target_waktu])
            ->andFilterWhere(['like', 'output', $this->output])
            ->andFilterWhere(['like', 'penanggung_jawab', $this->penanggung_jawab])
            ->andFilterWhere(['like', 'pelaksana', $this->pelaksana])
            ->andFilterWhere(['like', 'aras', $this->aras]);

        return $dataProvider;
    }
}
