<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ami;

/**
 * AmiSearch represents the model behind the search form of `app\models\Ami`.
 */
class AmiSearch extends Ami
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tahun', 'periode_id'], 'integer'],
            [['nama', 'tanggal_ami_mulai', 'tanggal_ami_selesai', 'tanggal_visitasi_mulai', 'tanggal_visitasi_selesai', 'tanggal_penilaian_mulai', 'tanggal_penilaian_selesai', 'keterangan', 'status_aktif', 'form_rencana_kode', 'form_rencana_tanggal_pembuatan', 'form_rencana_tanggal_revisi', 'form_rencana_tanggal_efektif', 'form_tilik_kode', 'form_tilik_tanggal_pembuatan', 'form_tilik_tanggal_revisi', 'form_tilik_tanggal_efektif', 'form_temuan_kode', 'form_temuan_tanggal_pembuatan', 'form_temuan_tanggal_revisi', 'form_temuan_tanggal_efektif', 'form_stemuan_kode', 'form_stemuan_tanggal_pembuatan', 'form_stemuan_tanggal_revisi', 'form_stemuan_tanggal_efektif', 'form_berita_kode', 'form_berita_tanggal_pembuatan', 'form_berita_tanggal_revisi', 'form_berita_tanggal_efektif', 'form_atl_kode', 'form_atl_tanggal_pembuatan', 'form_atl_tanggal_revisi', 'form_atl_tanggal_efektif', 'form_kehadiran_kode', 'form_kehadiran_tanggal_pembuatan', 'form_kehadiran_tanggal_revisi', 'form_kehadiran_tanggal_efektif', 'nomor_surat_tugas', 'created_at', 'updated_at', 'ketua_bpm', 'nidn_ketua_bpm', 'path_cover'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ami::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tahun' => $this->tahun,
            'tanggal_ami_mulai' => $this->tanggal_ami_mulai,
            'tanggal_ami_selesai' => $this->tanggal_ami_selesai,
            'tanggal_visitasi_mulai' => $this->tanggal_visitasi_mulai,
            'tanggal_visitasi_selesai' => $this->tanggal_visitasi_selesai,
            'tanggal_penilaian_mulai' => $this->tanggal_penilaian_mulai,
            'tanggal_penilaian_selesai' => $this->tanggal_penilaian_selesai,
            'periode_id' => $this->periode_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'status_aktif', $this->status_aktif])
            ->andFilterWhere(['like', 'form_rencana_kode', $this->form_rencana_kode])
            ->andFilterWhere(['like', 'form_rencana_tanggal_pembuatan', $this->form_rencana_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_rencana_tanggal_revisi', $this->form_rencana_tanggal_revisi])
            ->andFilterWhere(['like', 'form_rencana_tanggal_efektif', $this->form_rencana_tanggal_efektif])
            ->andFilterWhere(['like', 'form_tilik_kode', $this->form_tilik_kode])
            ->andFilterWhere(['like', 'form_tilik_tanggal_pembuatan', $this->form_tilik_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_tilik_tanggal_revisi', $this->form_tilik_tanggal_revisi])
            ->andFilterWhere(['like', 'form_tilik_tanggal_efektif', $this->form_tilik_tanggal_efektif])
            ->andFilterWhere(['like', 'form_temuan_kode', $this->form_temuan_kode])
            ->andFilterWhere(['like', 'form_temuan_tanggal_pembuatan', $this->form_temuan_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_temuan_tanggal_revisi', $this->form_temuan_tanggal_revisi])
            ->andFilterWhere(['like', 'form_temuan_tanggal_efektif', $this->form_temuan_tanggal_efektif])
            ->andFilterWhere(['like', 'form_stemuan_kode', $this->form_stemuan_kode])
            ->andFilterWhere(['like', 'form_stemuan_tanggal_pembuatan', $this->form_stemuan_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_stemuan_tanggal_revisi', $this->form_stemuan_tanggal_revisi])
            ->andFilterWhere(['like', 'form_stemuan_tanggal_efektif', $this->form_stemuan_tanggal_efektif])
            ->andFilterWhere(['like', 'form_berita_kode', $this->form_berita_kode])
            ->andFilterWhere(['like', 'form_berita_tanggal_pembuatan', $this->form_berita_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_berita_tanggal_revisi', $this->form_berita_tanggal_revisi])
            ->andFilterWhere(['like', 'form_berita_tanggal_efektif', $this->form_berita_tanggal_efektif])
            ->andFilterWhere(['like', 'form_atl_kode', $this->form_atl_kode])
            ->andFilterWhere(['like', 'form_atl_tanggal_pembuatan', $this->form_atl_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_atl_tanggal_revisi', $this->form_atl_tanggal_revisi])
            ->andFilterWhere(['like', 'form_atl_tanggal_efektif', $this->form_atl_tanggal_efektif])
            ->andFilterWhere(['like', 'form_kehadiran_kode', $this->form_kehadiran_kode])
            ->andFilterWhere(['like', 'form_kehadiran_tanggal_pembuatan', $this->form_kehadiran_tanggal_pembuatan])
            ->andFilterWhere(['like', 'form_kehadiran_tanggal_revisi', $this->form_kehadiran_tanggal_revisi])
            ->andFilterWhere(['like', 'form_kehadiran_tanggal_efektif', $this->form_kehadiran_tanggal_efektif])
            ->andFilterWhere(['like', 'nomor_surat_tugas', $this->nomor_surat_tugas])
            ->andFilterWhere(['like', 'ketua_bpm', $this->ketua_bpm])
            ->andFilterWhere(['like', 'nidn_ketua_bpm', $this->nidn_ketua_bpm])
            ->andFilterWhere(['like', 'path_cover', $this->path_cover]);

        return $dataProvider;
    }
}
