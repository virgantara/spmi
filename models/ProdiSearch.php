<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prodi;

/**
 * ProdiSearch represents the model behind the search form of `app\models\Prodi`.
 */
class ProdiSearch extends Prodi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kode_prodi', 'nama_prodi', 'kode_jenjang', 'akreditasi', 'tanggal_sk', 'nomor_sk', 'tanggal_kadaluarsa', 'updated_at', 'created_at','lembaga_akreditasi_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prodi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lembaga_akreditasi_id' => $this->lembaga_akreditasi_id,
            'tanggal_sk' => $this->tanggal_sk,
            'tanggal_kadaluarsa' => $this->tanggal_kadaluarsa,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'kode_prodi', $this->kode_prodi])
            ->andFilterWhere(['like', 'nama_prodi', $this->nama_prodi])
            ->andFilterWhere(['like', 'kode_jenjang', $this->kode_jenjang])
            ->andFilterWhere(['like', 'akreditasi', $this->akreditasi])
            ->andFilterWhere(['like', 'nomor_sk', $this->nomor_sk]);

        return $dataProvider;
    }
}
