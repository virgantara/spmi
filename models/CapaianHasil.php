<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "capaian_hasil".
 *
 * @property int $id
 * @property int|null $unit_kerja_id
 * @property int|null $capaian_kinerja_id
 * @property int|null $periode_id
 * @property string|null $capaian_target
 * @property string|null $realisasi_target
 *
 * @property CapaianKinerja $capaianKinerja
 * @property Periode $periode
 * @property UnitKerja $unitKerja
 */
class CapaianHasil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'capaian_hasil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_kerja_id', 'capaian_kinerja_id', 'periode_id'], 'integer'],
            [['capaian_target', 'realisasi_target'], 'string', 'max' => 100],
            [['capaian_kinerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => CapaianKinerja::class, 'targetAttribute' => ['capaian_kinerja_id' => 'id']],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::class, 'targetAttribute' => ['periode_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_kerja_id' => 'Unit Kerja ID',
            'capaian_kinerja_id' => 'Capaian Kinerja ID',
            'periode_id' => 'Periode ID',
            'capaian_target' => 'Capaian Target',
            'realisasi_target' => 'Realisasi Target',
        ];
    }

    /**
     * Gets query for [[CapaianKinerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianKinerja()
    {
        return $this->hasOne(CapaianKinerja::class, ['id' => 'capaian_kinerja_id']);
    }

    /**
     * Gets query for [[Periode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::class, ['id' => 'periode_id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
