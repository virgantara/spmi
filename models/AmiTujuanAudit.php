<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami_tujuan_audit".
 *
 * @property int $id
 * @property int|null $tujuan_audit_id
 * @property int|null $ami_unit_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmiUnit $amiUnit
 * @property TujuanAudit $tujuanAudit
 */
class AmiTujuanAudit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami_tujuan_audit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tujuan_audit_id', 'ami_unit_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['tujuan_audit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TujuanAudit::class, 'targetAttribute' => ['tujuan_audit_id' => 'id']],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tujuan_audit_id' => Yii::t('app', 'Tujuan Audit ID'),
            'ami_unit_id' => Yii::t('app', 'Ami Unit ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[TujuanAudit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTujuanAudit()
    {
        return $this->hasOne(TujuanAudit::class, ['id' => 'tujuan_audit_id']);
    }
}
