<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditor".
 *
 * @property int $id
 * @property string|null $nomor_registrasi
 * @property string $nama
 * @property string $kode_unik
 * @property string|null $nomor_sk
 * @property string|null $tanggal_sk
 * @property string|null $email
 * @property string|null $niy
 * @property string|null $prodi
 * @property string|null $nidn
 * @property string|null $pangkat
 * @property string|null $jabfung
 * @property string|null $status_aktif
 * @property int|null $is_sent
 *
 * @property AmiAuditor[] $amiAuditors
 * @property Persetujuan[] $persetujuans
 * @property Tilik[] $tiliks
 * @property User[] $users
 */
class Auditor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auditor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'kode_unik'], 'required'],
            [['is_sent'], 'integer'],
            [['nomor_registrasi', 'nomor_sk', 'tanggal_sk', 'pangkat', 'jabfung'], 'string', 'max' => 100],
            [['nama', 'kode_unik', 'prodi'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 200],
            [['niy'], 'string', 'max' => 20],
            [['nidn'], 'string', 'max' => 12],
            [['status_aktif'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_registrasi' => 'Nomor Registrasi',
            'nama' => 'Nama',
            'kode_unik' => 'Kode Unik',
            'nomor_sk' => 'Nomor SK',
            'tanggal_sk' => 'Tanggal SK',
            'email' => 'Email',
            'niy' => 'NIY',
            'prodi' => 'Prodi',
            'nidn' => 'NIDN',
            'pangkat' => 'Pangkat',
            'jabfung' => 'Jabfung',
            'status_aktif' => 'Status Aktif',
            'is_sent' => 'Is Sent',
        ];
    }

    /**
     * Gets query for [[AmiAuditors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiAuditors()
    {
        return $this->hasMany(AmiAuditor::class, ['auditor_id' => 'id']);
    }

    /**
     * Gets query for [[Persetujuans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersetujuans()
    {
        return $this->hasMany(Persetujuan::class, ['auditor_id' => 'id']);
    }

    /**
     * Gets query for [[Tiliks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiliks()
    {
        return $this->hasMany(Tilik::class, ['auditor_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['auditor_id' => 'id']);
    }
}
