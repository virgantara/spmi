<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tilik;

/**
 * TilikSearch represents the model behind the search form of `app\models\Tilik`.
 */
class TilikSearch extends Tilik
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'asesmen_id', 'auditor_id', 'status'], 'integer'],
            [['kriteria', 'pertanyaan', 'catatan', 'bukti', 'referensi', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ami_unit_id = null)
    {
        $query = Tilik::find();

        if (isset($ami_unit_id)) {
            $query->joinWith(['asesmen as a']);
            $query->where([
                'a.ami_unit_id' => $ami_unit_id,
            ]);
            # code...
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);




        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'asesmen_id' => $this->asesmen_id,
            'auditor_id' => $this->auditor_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kriteria', $this->kriteria])
            ->andFilterWhere(['like', 'pertanyaan', $this->pertanyaan])
            ->andFilterWhere(['like', 'bukti', $this->bukti])
            ->andFilterWhere(['like', 'referensi', $this->referensi])
            ->andFilterWhere(['like', 'catatan', $this->catatan]);

        return $dataProvider;
    }
}
