<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dokumen".
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $link
 * @property string|null $s3
 * @property string|null $keterangan
 * @property int|null $jenis_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property MasterJenis $jenis
 */
class Dokumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dokumen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keterangan'], 'string'],
            [['jenis_id'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
            [['nama', 'link', 's3'], 'string', 'max' => 250],
            [['jenis_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterJenis::class, 'targetAttribute' => ['jenis_id' => 'id']],
            [['unit_kerja_id'],'exist','skipOnError'=>true,'targetClass'=>UnitKerja::class,'targetAttribute'=>['unit_kerja_id'=>'id']],
            [['s3'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf','maxSize' => 1024 * 1024 * 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'link' => 'Link',
            's3' => 'S3',
            'keterangan' => 'Keterangan',
            'unit_kerja_id' => 'Unit Kerja',
            'jenis_id' => 'Jenis Dokumen',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Visibilitas'
        ];
    }

    /**
     * Gets query for [[Jenis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasOne(MasterJenis::class, ['id' => 'jenis_id']);
    }

    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
