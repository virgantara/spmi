<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Temuan;

/**
 * TemuanSearch represents the model behind the search form of `app\models\Temuan`.
 */
class TemuanSearch extends Temuan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_unit_id', 'asesmen_id', 'indikator_id'], 'integer'],
            [['uraian_k', 'referensi', 'akar_penyebab', 'rekomendasi', 'kategori', 'jadwal_perbaikan', 'penanggung_jawab', 'created_at', 'updated_at', 'jabatan_pj', 'rencana_perbaikan', 'rencana_tindak_lanjut', 'jadwal_rtl', 'realisasi_tindak_lanjut', 'efektifitas', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = Temuan::find();

        // add conditions that should always apply here

        if (!empty($filters)) {

            $query->joinWith(['amiUnit as a', 'amiUnit.unit as au', 'asesmen.indikator as ai']);

            if (!empty($filters['kategori'])) $query->andWhere(['kategori' => $filters['kategori']]);
            if (!empty($filters['ami_id'])) $query->andWhere(['a.ami_id' => $filters['ami_id']]);
            if (!empty($filters['ami_unit_id'])) $query->andWhere(['temuan.ami_unit_id' => $filters['ami_unit_id']]);
            if (!empty($filters['kriteria_id'])) $query->andWhere(['ai.kriteria_id' => $filters['kriteria_id']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'ami_unit_id' => $this->ami_unit_id,
            'asesmen_id' => $this->asesmen_id,
            'indikator_id' => $this->indikator_id,
        ]);

        $query->andFilterWhere(['like', 'uraian_k', $this->uraian_k])
            ->andFilterWhere(['like', 'referensi', $this->referensi])
            ->andFilterWhere(['like', 'akar_penyebab', $this->akar_penyebab])
            ->andFilterWhere(['like', 'rekomendasi', $this->rekomendasi])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'jadwal_perbaikan', $this->jadwal_perbaikan])
            ->andFilterWhere(['like', 'penanggung_jawab', $this->penanggung_jawab])
            ->andFilterWhere(['like', 'jabatan_pj', $this->jabatan_pj])
            ->andFilterWhere(['like', 'rencana_perbaikan', $this->rencana_perbaikan])
            ->andFilterWhere(['like', 'rencana_tindak_lanjut', $this->rencana_tindak_lanjut])
            ->andFilterWhere(['like', 'jadwal_rtl', $this->jadwal_rtl])
            ->andFilterWhere(['like', 'realisasi_tindak_lanjut', $this->realisasi_tindak_lanjut])
            ->andFilterWhere(['like', 'efektifitas', $this->efektifitas])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
