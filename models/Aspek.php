<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aspek".
 *
 * @property int $id
 * @property string|null $aspek
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BahanRtm[] $bahanRtms
 */
class Aspek extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aspek';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['aspek'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'aspek' => Yii::t('app', 'Aspek'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtms()
    {
        return $this->hasMany(BahanRtm::class, ['aspek_id' => 'id']);
    }
}
