<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CapaianKinerja;

/**
 * CapaianKinerjaSearch represents the model behind the search form of `app\models\CapaianKinerja`.
 */
class CapaianKinerjaSearch extends CapaianKinerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'no_indikator', 'bidang_wr', 'status_aktif', 'ket_jawaban'], 'integer'],
            [['sasaran_stategis', 'indikator_kinerja', 'satuan', 'target_unida'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CapaianKinerja::find();

        // add conditions that should always apply here
        $query->orderBy(['no_indikator' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_indikator' => $this->no_indikator,
            'bidang_wr' => $this->bidang_wr,
            'status_aktif' => $this->status_aktif, 
            'ket_jawaban' => $this->ket_jawaban,
        ]);

        $query->andFilterWhere(['like', 'sasaran_stategis', $this->sasaran_stategis])
            ->andFilterWhere(['like', 'indikator_kinerja', $this->indikator_kinerja])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'target_unida', $this->target_unida]);

        return $dataProvider;
    }
}
