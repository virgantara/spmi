<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonevUnitKerjaObjek;

/**
 * MonevUnitKerjaObjekSearch represents the model behind the search form of `app\models\MonevUnitKerjaObjek`.
 */
class MonevUnitKerjaObjekSearch extends MonevUnitKerjaObjek
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'monev_unit_kerja_id', 'jenis_objek', 'import_id', 'kode', 'nama_kode', 'nama_sub_kode', 'nama_program', 'sumber_dana', 'waktu', 'status_biaya', 'status_waktu', 'status_program', 'created_at', 'updated_at', 'ketidaksesuaian', 'referensi', 'rencana_tindak_lanjut', 'target', 'capaian', 'evaluasi', 'perbaikan'], 'safe'],
            [['biaya'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = MonevUnitKerjaObjek::find();

        if (!empty($filters)) {
            $query->andWhere([
                'monev_unit_kerja_id' => $filters['monev_unit_kerja_id'],
                'jenis_objek' => $filters['jenis_objek']
            ]);
        }

        $query->orderBy(['kode' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'monev_unit_kerja_id', $this->monev_unit_kerja_id])
            ->andFilterWhere(['like', 'jenis_objek', $this->jenis_objek])
            ->andFilterWhere(['like', 'import_id', $this->import_id])
            ->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'nama_kode', $this->nama_kode])
            ->andFilterWhere(['like', 'nama_sub_kode', $this->nama_sub_kode])
            ->andFilterWhere(['like', 'nama_program', $this->nama_program])
            ->andFilterWhere(['like', 'sumber_dana', $this->sumber_dana])
            ->andFilterWhere(['like', 'biaya', $this->biaya])
            ->andFilterWhere(['like', 'waktu', $this->waktu])
            ->andFilterWhere(['like', 'status_biaya', $this->status_biaya])
            ->andFilterWhere(['like', 'status_waktu', $this->status_waktu])
            ->andFilterWhere(['like', 'status_program', $this->status_program])
            ->andFilterWhere(['like', 'ketidaksesuaian', $this->ketidaksesuaian])
            ->andFilterWhere(['like', 'referensi', $this->referensi])
            ->andFilterWhere(['like', 'rencana_tindak_lanjut', $this->rencana_tindak_lanjut])
            ->andFilterWhere(['like', 'target', $this->target])
            ->andFilterWhere(['like', 'capaian', $this->capaian])
            ->andFilterWhere(['like', 'evaluasi', $this->evaluasi])
            ->andFilterWhere(['like', 'perbaikan', $this->perbaikan]);

        return $dataProvider;
    }
}
