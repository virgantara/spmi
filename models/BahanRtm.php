<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bahan_rtm".
 *
 * @property int $id
 * @property int|null $unit_id
 * @property int|null $temuan_id
 * @property int|null $rtm_id
 * @property int|null $aspek_id
 * @property int|null $lingkup_bahasan_id
 * @property int|null $jenis_bahan
 * @property string|null $topik
 * @property string|null $tindakan
 * @property string|null $target_waktu
 * @property string|null $output
 * @property string|null $penanggung_jawab
 * @property string|null $pelaksana
 * @property string|null $aras
 * @property int|null $persetujuan
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Aspek $aspek
 * @property LingkupBahasan $lingkupBahasan
 * @property PenanggungJawabBahanRtm[] $penanggungJawabBahanRtms
 * @property Rtm $rtm
 * @property Temuan $temuan
 * @property UnitKerja $unit
 */
class BahanRtm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bahan_rtm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_id', 'temuan_id', 'rtm_id', 'aspek_id', 'lingkup_bahasan_id', 'jenis_bahan', 'persetujuan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['deskripsi', 'tindakan', 'output'], 'string', 'max' => 1000],
            [['target_waktu', 'aras'], 'string', 'max' => 50],
            [['penanggung_jawab'], 'string', 'max' => 300],
            [['pelaksana'], 'string', 'max' => 200],
            [['aspek_id'], 'exist', 'skipOnError' => true, 'targetClass' => Aspek::class, 'targetAttribute' => ['aspek_id' => 'id']],
            [['lingkup_bahasan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LingkupBahasan::class, 'targetAttribute' => ['lingkup_bahasan_id' => 'id']],
            [['rtm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rtm::class, 'targetAttribute' => ['rtm_id' => 'id']],
            [['temuan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temuan::class, 'targetAttribute' => ['temuan_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit_id' => Yii::t('app', 'Unit ID'),
            'temuan_id' => Yii::t('app', 'Temuan ID'),
            'rtm_id' => Yii::t('app', 'Rtm ID'),
            'aspek_id' => Yii::t('app', 'Aspek ID'),
            'lingkup_bahasan_id' => Yii::t('app', 'Lingkup Bahasan ID'),
            'jenis_bahan' => Yii::t('app', 'Jenis Bahan'),
            'deskripsi' => Yii::t('app', 'deskripsi'),
            'tindakan' => Yii::t('app', 'Tindakan'),
            'target_waktu' => Yii::t('app', 'Target Waktu'),
            'output' => Yii::t('app', 'Output'),
            'penanggung_jawab' => Yii::t('app', 'Penanggung Jawab'),
            'pelaksana' => Yii::t('app', 'Pelaksana'),
            'aras' => Yii::t('app', 'Aras'),
            'persetujuan' => Yii::t('app', 'Persetujuan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Aspek]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAspek()
    {
        return $this->hasOne(Aspek::class, ['id' => 'aspek_id']);
    }

    /**
     * Gets query for [[LingkupBahasan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLingkupBahasan()
    {
        return $this->hasOne(LingkupBahasan::class, ['id' => 'lingkup_bahasan_id']);
    }

    /**
     * Gets query for [[PenanggungJawabBahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenanggungJawabBahanRtms()
    {
        return $this->hasMany(PenanggungJawabBahanRtm::class, ['bahan_rtm_id' => 'id']);
    }

    /**
     * Gets query for [[Rtm]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRtm()
    {
        return $this->hasOne(Rtm::class, ['id' => 'rtm_id']);
    }

    /**
     * Gets query for [[Temuan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemuan()
    {
        return $this->hasOne(Temuan::class, ['id' => 'temuan_id']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_id']);
    }
}
