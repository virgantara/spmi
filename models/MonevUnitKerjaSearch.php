<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonevUnitKerja;

/**
 * MonevUnitKerjaSearch represents the model behind the search form of `app\models\MonevUnitKerja`.
 */
class MonevUnitKerjaSearch extends MonevUnitKerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'monev_id', 'pernyataan_peningkatan', 'created_at', 'updated_at'], 'safe'],
            [['unit_kerja_id', 'dokumen_id', 'persetujuan', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MonevUnitKerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'unit_kerja_id' => $this->unit_kerja_id,
            'dokumen_id' => $this->dokumen_id,
            'persetujuan' => $this->persetujuan,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'monev_id', $this->monev_id])
            ->andFilterWhere(['like', 'pernyataan_peningkatan', $this->pernyataan_peningkatan]);

        return $dataProvider;
    }
}
