<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami_laporan".
 *
 * @property int $id
 * @property int|null $ami_id
 * @property int|null $ami_unit_id
 * @property int|null $urutan
 * @property string|null $path_gambar
 * @property string|null $keterangan
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Ami $ami
 * @property AmiUnit $amiUnit
 */
class AmiLaporan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami_laporan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ami_id', 'ami_unit_id', 'urutan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['path_gambar', 'keterangan'], 'string', 'max' => 500],
            [['ami_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ami::class, 'targetAttribute' => ['ami_id' => 'id']],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
            [['path_gambar'], 'file', 'skipOnEmpty' => true, 'extensions' => ['jpg', 'png'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ami_id' => Yii::t('app', 'Ami ID'),
            'ami_unit_id' => Yii::t('app', 'Ami Unit ID'),
            'urutan' => Yii::t('app', 'Urutan'),
            'path_gambar' => Yii::t('app', 'Path Gambar'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Ami]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmi()
    {
        return $this->hasOne(Ami::class, ['id' => 'ami_id']);
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }
}
