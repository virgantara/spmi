<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Indikator;
use yii\helpers\ArrayHelper;

/**
 * IndikatorSearch represents the model behind the search form of `app\models\Indikator`.
 */
class IndikatorSearch extends Indikator
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kriteria_id', 'unit_id', 'ami_id'], 'integer'],
            [['nama', 'skor1', 'skor2', 'skor3', 'skor4', 'status_aktif', 'jenis_indikator', 'pernyataan', 'indikator', 'strategi', 'created_at', 'referensi', 'updated_at'], 'safe'],
            [['bobot'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $condition = null, $filters = [], $ami_id = null)
    {
        $query = Indikator::find();

        // Retrieve PembagianKriteria based on condition or all
        $pembagianQuery = PembagianKriteria::find();
        if (!empty($condition)) {
            $pembagianQuery->where(['jenis_unit' => $condition]);
        }
        $pembagian = $pembagianQuery->select('kriteria_id')->column();

        // Apply kriteria_id filter
        if (!empty($pembagian)) {
            $query->andWhere(['in', 'kriteria_id', $pembagian]);
        }

        // Join with related tables
        $query->joinWith(['ami as a']);

        if (!empty($ami_id)) {
            $query->andWhere(['a.id' => $ami_id]);
        } else {
            $query->andWhere(['a.status_aktif' => 1]);
        }
        
        $query->joinWith(['jenjangBobots as j']);

        // Apply additional filters if provided
        if (!empty($filters['jenjang_id'])) {
            $query->andWhere(['j.jenjang_id' => $filters['jenjang_id']]);
        }
        if (!empty($filters['kriteria_id'])) {
            $query->andWhere(['kriteria_id' => $filters['kriteria_id']]);
        }
        if (!empty($filters['unit_id'])) {
            $query->andWhere(['unit_id' => $filters['unit_id']]);
        }
        if (!empty($filters['ami_id'])) {
            $query->andWhere(['ami_id' => $filters['ami_id']]);
        }

        // Load parameters and validate
        $this->load($params);
        // if (!$this->validate()) {
        //     // Return empty dataProvider if validation fails
        //     return new ActiveDataProvider([
        //         'query' => $query,
        //     ]);
        // }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kriteria_id' => $this->kriteria_id,
            'bobot' => $this->bobot,
            'unit_id' => $this->unit_id,
            'ami_id' => $this->ami_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'skor1', $this->skor1])
            ->andFilterWhere(['like', 'skor2', $this->skor2])
            ->andFilterWhere(['like', 'skor3', $this->skor3])
            ->andFilterWhere(['like', 'skor4', $this->skor4])
            ->andFilterWhere(['like', 'status_aktif', $this->status_aktif])
            ->andFilterWhere(['like', 'jenis_indikator', $this->jenis_indikator])
            ->andFilterWhere(['like', 'pernyataan', $this->pernyataan])
            ->andFilterWhere(['like', 'indikator', $this->indikator])
            ->andFilterWhere(['like', 'strategi', $this->strategi])
            ->andFilterWhere(['like', 'referensi', $this->referensi]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
