<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmanahKinerjaMaster;

/**
 * AmanahKinerjaMasterSearch represents the model behind the search form of `app\models\AmanahKinerjaMaster`.
 */
class AmanahKinerjaMasterSearch extends AmanahKinerjaMaster
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'jenis_amanah_kinerja', 'amanah_kinerja', 'target_umum', 'created_at', 'updated_at'], 'safe'],
            [['unit_kerja_id', 'status_aktif'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmanahKinerjaMaster::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'unit_kerja_id' => $this->unit_kerja_id,
            'status_aktif' => $this->status_aktif,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'jenis_amanah_kinerja', $this->jenis_amanah_kinerja])
            ->andFilterWhere(['like', 'amanah_kinerja', $this->amanah_kinerja])
            ->andFilterWhere(['like', 'target_umum', $this->target_umum]);

        return $dataProvider;
    }
}
