<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami_auditor".
 *
 * @property int $id
 * @property int|null $ami_id
 * @property int|null $auditor_id
 * @property int|null $status_id
 *
 * @property AmiUnit $ami
 * @property Auditor $auditor
 * @property StatusAuditor $status
 */
class AmiAuditor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami_auditor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ami_id', 'auditor_id', 'status_id'], 'integer'],
            [['ami_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_id' => 'id']],
            [['auditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auditor::class, 'targetAttribute' => ['auditor_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusAuditor::class, 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ami_id' => 'AMI',
            'auditor_id' => 'Auditor ID',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * Gets query for [[Ami]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmi()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_id']);
    }

    /**
     * Gets query for [[Auditor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuditor()
    {
        return $this->hasOne(Auditor::class, ['id' => 'auditor_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusAuditor::class, ['id' => 'status_id']);
    }
}
