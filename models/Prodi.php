<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prodi".
 *
 * @property int $id
 * @property string $kode_prodi
 * @property string $nama_prodi
 * @property string|null $fakultas
 * @property string $kode_jenjang
 * @property string|null $akreditasi
 * @property string|null $tanggal_sk
 * @property string|null $nomor_sk
 * @property string|null $tanggal_kadaluarsa
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property ProdiJenjang[] $prodiJenjangs
 * @property UnitKerjaProdi[] $unitKerjaProdis
 */
class Prodi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_prodi', 'nama_prodi', 'kode_jenjang'], 'required'],
            [['tanggal_sk', 'tanggal_kadaluarsa', 'updated_at', 'created_at','lembaga_akreditasi_id'], 'safe'],
            [['kode_prodi'], 'string', 'max' => 20],
            [['nama_prodi'], 'string', 'max' => 255],
            [['fakultas', 'nomor_sk'], 'string', 'max' => 100],
            [['kode_jenjang'], 'string', 'max' => 10],
            [['akreditasi'], 'string', 'max' => 10],
            [['lembaga_akreditasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => LembagaAkreditasi::class, 'targetAttribute' => ['lembaga_akreditasi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_prodi' => 'Kode Prodi',
            'nama_prodi' => 'Nama Prodi',
            'fakultas' => 'Fakultas',
            'kode_jenjang' => 'Kode Jenjang',
            'akreditasi' => 'Akreditasi',
            'tanggal_sk' => 'Tanggal Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_kadaluarsa' => 'Tanggal Kadaluarsa',
            'lembaga_akreditasi_id' => 'Lembaga Akreditasi',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getListAkreditasi()
    {
        $query = new \yii\db\Query();
        $query->select(['a.id','a.file_path','a.nomor_sk', 'a.tanggal_sk', 'la.tingkat', 'la.singkatan_lembaga', 'sa.nama_id as status_akreditasi']);
        $query->from(['prodi p']);
        $query->join('JOIN','akreditasi a','a.prodi_id = p.id');
        $query->join('JOIN','lembaga_akreditasi la','la.id = a.lembaga_akreditasi_id');
        $query->join('JOIN','status_akreditasi sa','sa.kode = a.status_akreditasi');
        $query->where('a.tanggal_sk <= NOW() AND a.tanggal_kadaluarsa >= NOW()');
        $query->andWhere(['p.kode_prodi' => $this->kode_prodi]);
        $query->orderBy(['a.tanggal_sk' => SORT_ASC]);

        $list_akreditasi_nasional = [];
        $list_akreditasi_internasional = [];
        foreach($query->all() as $akr){
            if($akr['tingkat'] == 3 || $akr['tingkat'] == 2){
                $list_akreditasi_internasional[$akr['singkatan_lembaga']] = [
                    'nomor_sk' => $akr['nomor_sk'],
                    'link_download' => (!empty($akr['file_path']) ? '/akreditasi/download-sk?id='.$akr['id'] : null),
                    'tanggal_sk' => $akr['tanggal_sk'],
                    'lembaga' => $akr['singkatan_lembaga'],
                    'status_akreditasi' => $akr['status_akreditasi'],
                ];
            }

            else{

                /*
                Akreditasi dari lembaga yang sama akan replace akreditasi sebelumnya
                */
                $list_akreditasi_nasional[$akr['singkatan_lembaga']] = [
                    'nomor_sk' => $akr['nomor_sk'],
                    'link_download' => (!empty($akr['file_path']) ? '/akreditasi/download-sk?id='.$akr['id'] : null),
                    'tanggal_sk' => $akr['tanggal_sk'],
                    'lembaga' => $akr['singkatan_lembaga'],
                    'status_akreditasi' => $akr['status_akreditasi']
                ];
            }
        }

        return [
            'list_akreditasi_nasional' => $list_akreditasi_nasional,
            'list_akreditasi_internasional' => $list_akreditasi_internasional
        ];
    }

    public function getAkreditasis()
    {
        return $this->hasMany(Akreditasi::class, ['prodi_id' => 'id']);
    }

    public function getLembagaAkreditasi()
    {
        return $this->hasOne(LembagaAkreditasi::class, ['id' => 'lembaga_akreditasi_id']);
    }

    /**
     * Gets query for [[ProdiJenjangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdiJenjang()
    {
        return $this->hasOne(ProdiJenjang::class, ['prodi_id' => 'id']);
    }

    /**
     * Gets query for [[UnitKerjaProdis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjaProdi()
    {
        return $this->hasOne(UnitKerjaProdi::class, ['prodi_id' => 'id']);
    }
}
