<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tujuan_audit".
 *
 * @property int $id
 * @property string|null $tujuan_audit
 * @property string|null $keterangan
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmiTujuanAudit[] $amiTujuanAudits
 */
class TujuanAudit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tujuan_audit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['tujuan_audit', 'keterangan'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tujuan_audit' => Yii::t('app', 'Tujuan Audit'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmiTujuanAudits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiTujuanAudits()
    {
        return $this->hasMany(AmiTujuanAudit::class, ['tujuan_audit_id' => 'id']);
    }
}
