<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status_akreditasi".
 *
 * @property string $id
 * @property string $kode
 * @property string $nama_id
 * @property string $nama_en
 */
class StatusAkreditasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_akreditasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kode', 'nama_id', 'nama_en'], 'required'],
            [['id', 'nama_id', 'nama_en'], 'string', 'max' => 100],
            [['kode'], 'string', 'max' => 20],
            [['id','kode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama_id' => 'Nama ID',
            'nama_en' => 'Nama En',
        ];
    }
}
