<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penanggung_jawab_bahan_rtm".
 *
 * @property int $id
 * @property int|null $bahan_rtm_id
 * @property int|null $penanggung_jawab_rtm_id
 *
 * @property BahanRtm $bahanRtm
 * @property PenanggungJawabRtm $penanggungJawabRtm
 */
class PenanggungJawabBahanRtm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penanggung_jawab_bahan_rtm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bahan_rtm_id', 'penanggung_jawab_rtm_id'], 'integer'],
            [['bahan_rtm_id'], 'exist', 'skipOnError' => true, 'targetClass' => BahanRtm::class, 'targetAttribute' => ['bahan_rtm_id' => 'id']],
            [['penanggung_jawab_rtm_id'], 'exist', 'skipOnError' => true, 'targetClass' => PenanggungJawabRtm::class, 'targetAttribute' => ['penanggung_jawab_rtm_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bahan_rtm_id' => 'Bahan Rtm ID',
            'penanggung_jawab_rtm_id' => 'Penanggung Jawab Rtm ID',
        ];
    }

    /**
     * Gets query for [[BahanRtm]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtm()
    {
        return $this->hasOne(BahanRtm::class, ['id' => 'bahan_rtm_id']);
    }

    /**
     * Gets query for [[PenanggungJawabRtm]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenanggungJawabRtm()
    {
        return $this->hasOne(PenanggungJawabRtm::class, ['id' => 'penanggung_jawab_rtm_id']);
    }
}
