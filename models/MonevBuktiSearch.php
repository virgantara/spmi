<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonevBukti;

/**
 * MonevBuktiSearch represents the model behind the search form of `app\models\MonevBukti`.
 */
class MonevBuktiSearch extends MonevBukti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'monev_unit_kerja_id', 'monev_unit_kerja_objek_id', 'created_at', 'updated_at'], 'safe'],
            [['dokumen_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MonevBukti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dokumen_id' => $this->dokumen_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'monev_unit_kerja_id', $this->monev_unit_kerja_id])
            ->andFilterWhere(['like', 'monev_unit_kerja_objek_id', $this->monev_unit_kerja_objek_id]);

        return $dataProvider;
    }
}
