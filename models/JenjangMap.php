<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenjang_map".
 *
 * @property int $id
 * @property int|null $unit_kerja_id
 * @property int|null $jenjang_id
 *
 * @property Jenjang $jenjang
 * @property UnitKerja $unitKerja
 */
class JenjangMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenjang_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_kerja_id', 'jenjang_id'], 'integer'],
            [['jenjang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenjang::class, 'targetAttribute' => ['jenjang_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_kerja_id' => 'Unit Kerja ID',
            'jenjang_id' => 'Jenjang ID',
        ];
    }

    /**
     * Gets query for [[Jenjang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenjang()
    {
        return $this->hasOne(Jenjang::class, ['id' => 'jenjang_id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
