<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "capaian_periode".
 *
 * @property int $id
 * @property int|null $capaian_kinerja_id
 * @property int|null $periode_id
 *
 * @property CapaianKinerja $capaianKinerja
 * @property Periode $periode
 */
class CapaianPeriode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'capaian_periode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['capaian_kinerja_id', 'periode_id'], 'integer'],
            [['capaian_kinerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => CapaianKinerja::class, 'targetAttribute' => ['capaian_kinerja_id' => 'id']],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Periode::class, 'targetAttribute' => ['periode_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'capaian_kinerja_id' => 'Capaian Kinerja ID',
            'periode_id' => 'Periode ID',
        ];
    }

    /**
     * Gets query for [[CapaianKinerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianKinerja()
    {
        return $this->hasOne(CapaianKinerja::class, ['id' => 'capaian_kinerja_id']);
    }

    /**
     * Gets query for [[Periode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(Periode::class, ['id' => 'periode_id']);
    }
}
