<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "periode".
 *
 * @property int $id
 * @property int|null $tahun
 * @property string|null $periode
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $status_aktif
 *
 * @property Ami[] $amis
 * @property CapaianHasil[] $capaianHasils
 * @property Rtm[] $rtms
 */
class Periode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'periode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahun', 'status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['periode'], 'string', 'max' => 100],
            [['tahun', 'periode', 'status_aktif'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tahun' => 'Tahun',
            'periode' => 'Periode',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status_aktif' => 'Status Aktif',
        ];
    }

    public static function getStatusAktif()
    {
        $model = Periode::findOne(['status_aktif' => true]);

        return !empty($model) ? $model : null;
    }

    /**
     * Gets query for [[Amis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmis()
    {
        return $this->hasMany(Ami::class, ['periode_id' => 'id']);
    }

    /**
     * Gets query for [[CapaianHasils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianHasils()
    {
        return $this->hasMany(CapaianHasil::class, ['periode_id' => 'id']);
    }

    /**
     * Gets query for [[Rtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRtms()
    {
        return $this->hasMany(Rtm::class, ['periode_id' => 'id']);
    }
}
