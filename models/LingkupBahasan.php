<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lingkup_bahasan".
 *
 * @property int $id
 * @property string|null $lingkup_pembahasan
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property BahanRtm[] $bahanRtms
 */
class LingkupBahasan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lingkup_bahasan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lingkup_pembahasan'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lingkup_pembahasan' => Yii::t('app', 'Lingkup Pembahasan'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[BahanRtms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBahanRtms()
    {
        return $this->hasMany(BahanRtm::class, ['lingkup_bahasan_id' => 'id']);
    }
}
