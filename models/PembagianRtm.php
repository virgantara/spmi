<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembagian_rtm".
 *
 * @property int $id
 * @property int|null $unit_kerja_id
 * @property int|null $jabatan_id
 *
 * @property JabatanRtm $jabatan
 * @property UnitKerja $unitKerja
 */
class PembagianRtm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembagian_rtm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_kerja_id', 'jabatan_id'], 'integer'],
            [['jabatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JabatanRtm::class, 'targetAttribute' => ['jabatan_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit_kerja_id' => 'Unit Kerja ID',
            'jabatan_id' => 'Jabatan ID',
        ];
    }

    /**
     * Gets query for [[Jabatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJabatan()
    {
        return $this->hasOne(JabatanRtm::class, ['id' => 'jabatan_id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
