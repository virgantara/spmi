<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monev_unit_kerja_objek".
 *
 * @property string $id
 * @property string|null $monev_unit_kerja_id
 * @property string|null $jenis_objek
 * @property string|null $import_id
 * @property string|null $kode
 * @property string|null $nama_kode
 * @property string|null $nama_sub_kode
 * @property string|null $nama_program
 * @property string|null $sumber_dana
 * @property float|null $biaya
 * @property string|null $waktu
 * @property string|null $status_biaya
 * @property string|null $status_waktu
 * @property string|null $status_program
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $ketidaksesuaian
 * @property string|null $referensi
 * @property string|null $rencana_tindak_lanjut
 * @property string|null $target
 * @property string|null $capaian
 * @property string|null $evaluasi
 * @property string|null $perbaikan
 *
 * @property MonevBukti[] $monevBuktis
 * @property MonevUnitKerja $monevUnitKerja
 */
class MonevUnitKerjaObjek extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monev_unit_kerja_objek';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['nama_program', 'ketidaksesuaian', 'referensi', 'rencana_tindak_lanjut', 'target', 'capaian'], 'string'],
            [['biaya'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'monev_unit_kerja_id', 'import_id'], 'string', 'max' => 36],
            [['jenis_objek', 'waktu'], 'string', 'max' => 50],
            [['kode', 'nama_kode', 'nama_sub_kode'], 'string', 'max' => 2000],
            [['sumber_dana'], 'string', 'max' => 20],
            [['status_biaya', 'status_waktu', 'status_program'], 'string', 'max' => 5],
            [['evaluasi', 'perbaikan'], 'string', 'max' => 1000],
            [['id'], 'unique'],
            [['monev_unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => MonevUnitKerja::class, 'targetAttribute' => ['monev_unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'monev_unit_kerja_id' => Yii::t('app', 'Monev Unit Kerja ID'),
            'jenis_objek' => Yii::t('app', 'Jenis Objek'),
            'import_id' => Yii::t('app', 'Import ID'),
            'kode' => Yii::t('app', 'Kode'),
            'nama_kode' => Yii::t('app', 'Nama Kode'),
            'nama_sub_kode' => Yii::t('app', 'Nama Sub Kode'),
            'nama_program' => Yii::t('app', 'Nama Program'),
            'sumber_dana' => Yii::t('app', 'Sumber Dana'),
            'biaya' => Yii::t('app', 'Anggaran'),
            'waktu' => Yii::t('app', 'Waktu'),
            'status_biaya' => Yii::t('app', 'Status Biaya'),
            'status_waktu' => Yii::t('app', 'Status Waktu'),
            'status_program' => Yii::t('app', 'Status Program'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'ketidaksesuaian' => Yii::t('app', 'Ketidaksesuaian'),
            'referensi' => Yii::t('app', 'Referensi'),
            'rencana_tindak_lanjut' => Yii::t('app', 'Rencana Tindak Lanjut'),
            'target' => Yii::t('app', 'Target'),
            'capaian' => Yii::t('app', 'Capaian'),
            'evaluasi' => Yii::t('app', 'Evaluasi'),
            'perbaikan' => Yii::t('app', 'Perbaikan'),
        ];
    }

    /**
     * Gets query for [[MonevBuktis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevBuktis()
    {
        return $this->hasMany(MonevBukti::class, ['monev_unit_kerja_objek_id' => 'id']);
    }

    /**
     * Gets query for [[MonevUnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMonevUnitKerja()
    {
        return $this->hasOne(MonevUnitKerja::class, ['id' => 'monev_unit_kerja_id']);
    }
    
    public function getBukti()
    {
        return MonevBukti::findAll(['monev_unit_kerja_objek_id' => $this->id]);
    }
}
