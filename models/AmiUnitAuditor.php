<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ami_unit_auditor".
 *
 * @property int $id
 * @property int $ami_unit_id
 * @property int|null $asesor_id
 * @property int|null $asesor_ke
 *
 * @property AmiUnit $amiUnit
 * @property Auditor $asesor
 */
class AmiUnitAuditor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ami_unit_auditor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_unit_id'], 'required'],
            [['id', 'ami_unit_id', 'asesor_id', 'asesor_ke'], 'integer'],
            [['id'], 'unique'],
            [['asesor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auditor::class, 'targetAttribute' => ['asesor_id' => 'id']],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ami_unit_id' => 'Ami Unit ID',
            'asesor_id' => 'Asesor ID',
            'asesor_ke' => 'Asesor Ke',
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[Asesor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsesor()
    {
        return $this->hasOne(Auditor::class, ['id' => 'asesor_id']);
    }
}
