<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "amanah_kinerja_unit_kerja".
 *
 * @property string $id
 * @property string|null $amanah_kinerja_id
 * @property string|null $amanah_kinerja_master_id
 * @property int|null $unit_kerja_id
 * @property string|null $target
 * @property string|null $capaian
 * @property string|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmanahKinerja $amanahKinerja
 * @property AmanahKinerjaMaster $amanahKinerjaMaster
 * @property UnitKerja $unitKerja
 */
class AmanahKinerjaUnitKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amanah_kinerja_unit_kerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['unit_kerja_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'amanah_kinerja_id', 'amanah_kinerja_master_id'], 'string', 'max' => 36],
            [['target', 'capaian'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['amanah_kinerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmanahKinerja::class, 'targetAttribute' => ['amanah_kinerja_id' => 'id']],
            [['amanah_kinerja_master_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmanahKinerjaMaster::class, 'targetAttribute' => ['amanah_kinerja_master_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'amanah_kinerja_id' => Yii::t('app', 'Amanah Kinerja ID'),
            'amanah_kinerja_master_id' => Yii::t('app', 'Amanah Kinerja Master ID'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'target' => Yii::t('app', 'Target'),
            'capaian' => Yii::t('app', 'Capaian'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmanahKinerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerja()
    {
        return $this->hasOne(AmanahKinerja::class, ['id' => 'amanah_kinerja_id']);
    }

    /**
     * Gets query for [[AmanahKinerjaMaster]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerjaMaster()
    {
        return $this->hasOne(AmanahKinerjaMaster::class, ['id' => 'amanah_kinerja_master_id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
