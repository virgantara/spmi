<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmiUnitAuditor;

/**
 * AmiUnitAuditorSearch represents the model behind the search form of `app\models\AmiUnitAuditor`.
 */
class AmiUnitAuditorSearch extends AmiUnitAuditor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_unit_id', 'asesor_id', 'asesor_ke'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmiUnitAuditor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ami_unit_id' => $this->ami_unit_id,
            'asesor_id' => $this->asesor_id,
            'asesor_ke' => $this->asesor_ke,
        ]);

        return $dataProvider;
    }
}
