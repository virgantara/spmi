<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "capaian_kinerja".
 *
 * @property int $id
 * @property string|null $sasaran_stategis
 * @property int|null $no_indikator
 * @property string|null $indikator_kinerja
 * @property string|null $satuan
 * @property string|null $target_unida
 * @property int|null $bidang_wr
 * @property int|null $status_aktif
 * @property int|null $ket_jawaban
 *
 * @property CapaianHasil[] $capaianHasils
 * @property CapaianPeriode[] $capaianPeriodes
 */
class CapaianKinerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'capaian_kinerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_indikator', 'bidang_wr', 'status_aktif', 'ket_jawaban'], 'integer'],
            [['sasaran_stategis', 'indikator_kinerja'], 'string', 'max' => 500],
            [['satuan', 'target_unida'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sasaran_stategis' => 'Sasaran Stategis',
            'no_indikator' => 'No Indikator',
            'indikator_kinerja' => 'Indikator Kinerja',
            'satuan' => 'Satuan',
            'target_unida' => 'Target Unida',
            'bidang_wr' => 'Bidang Wr',
            'status_aktif' => 'Status Aktif',
            'ket_jawaban' => 'Ket Jawaban',
        ];
    }

    /**
     * Gets query for [[CapaianHasils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianHasils()
    {
        return $this->hasMany(CapaianHasil::class, ['capaian_kinerja_id' => 'id']);
    }

    /**
     * Gets query for [[CapaianPeriodes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapaianPeriodes()
    {
        return $this->hasMany(CapaianPeriode::class, ['capaian_kinerja_id' => 'id']);
    }
}
