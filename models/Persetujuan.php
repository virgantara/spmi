<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persetujuan".
 *
 * @property int $id
 * @property int|null $ami_unit_id
 * @property int|null $auditor_id
 * @property string|null $datetime
 * @property int|null $is_auditee
 * @property string|null $kode_dokumen
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmiUnit $amiUnit
 * @property Auditor $auditor
 */
class Persetujuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persetujuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ami_unit_id', 'auditor_id', 'is_auditee'], 'integer'],
            [['datetime', 'created_at', 'updated_at'], 'safe'],
            [['kode_dokumen'], 'string', 'max' => 50],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
            [['auditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auditor::class, 'targetAttribute' => ['auditor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ami_unit_id' => Yii::t('app', 'Ami Unit ID'),
            'auditor_id' => Yii::t('app', 'Auditor ID'),
            'datetime' => Yii::t('app', 'Datetime'),
            'is_auditee' => Yii::t('app', 'Is Auditee'),
            'kode_dokumen' => Yii::t('app', 'Kode Dokumen'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[Auditor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuditor()
    {
        return $this->hasOne(Auditor::class, ['id' => 'auditor_id']);
    }
}
