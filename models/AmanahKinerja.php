<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "amanah_kinerja".
 *
 * @property string $id
 * @property string|null $nama
 * @property string|null $tahun
 * @property int|null $status_aktif
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property AmanahKinerjaUnitKerja[] $amanahKinerjaUnitKerjas
 */
class AmanahKinerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amanah_kinerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['status_aktif'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'string', 'max' => 36],
            [['nama'], 'string', 'max' => 500],
            [['tahun'], 'string', 'max' => 5],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'tahun' => Yii::t('app', 'Tahun'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AmanahKinerjaUnitKerjas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmanahKinerjaUnitKerjas()
    {
        return $this->hasMany(AmanahKinerjaUnitKerja::class, ['amanah_kinerja_id' => 'id']);
    }
}
