<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asesmen".
 *
 * @property int $id
 * @property int|null $indikator_id
 * @property string|null $link_bukti
 * @property int|null $skor_ed
 * @property int|null $skor_ak1
 * @property int|null $skor_ak2
 * @property int|null $skor_ak3
 * @property int|null $skor_al1
 * @property int|null $skor_al2
 * @property int|null $skor_al3
 * @property string|null $t_auditor
 * @property int|null $ami_unit_id
 * @property string|null $created_at
 * @property int|null $dokumen_id
 * @property int|null $pengelompokan_id
 * @property int|null $jenis_dokumen
 *
 * @property AmiUnit $amiUnit
 * @property Dokumen $dokumen
 * @property Indikator $indikator
 * @property Pengelompokan $pengelompokan
 * @property Tilik[] $tiliks
 */
class Asesmen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asesmen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['indikator_id', 'skor_ed', 'skor_ak1', 'skor_ak2', 'skor_ak3', 'skor_al1', 'skor_al2', 'skor_al3', 'ami_unit_id', 'dokumen_id', 'pengelompokan_id', 'jenis_dokumen'], 'integer'],
            [['t_auditor'], 'string'],
            [['created_at'], 'safe'],
            [['link_bukti'], 'string', 'max' => 500],
            [['dokumen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dokumen::class, 'targetAttribute' => ['dokumen_id' => 'id']],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::class, 'targetAttribute' => ['indikator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indikator_id' => 'Indikator ID',
            'link_bukti' => 'Link Bukti',
            'skor_ed' => 'Skor Ed',
            'skor_ak1' => 'Skor Ak1',
            'skor_ak2' => 'Skor Ak2',
            'skor_ak3' => 'Skor Ak3',
            'skor_al1' => 'Skor Al1',
            'skor_al2' => 'Skor Al2',
            'skor_al3' => 'Skor Al3',
            't_auditor' => 'T Auditor',
            'ami_unit_id' => 'Ami Unit ID',
            'created_at' => 'Created At',
            'dokumen_id' => 'Dokumen ID',
            'pengelompokan_id' => 'Pengelompokan ID',
            'jenis_dokumen' => 'Jenis Dokumen',
        ];
    }

    public static function setPenilaianClear($id)
    {
        $asesmen = Self::findOne($id);
        $asesmen->skor_ak1 = null;
        $asesmen->skor_ak2 = null;
        $asesmen->skor_ak3 = null;
        $asesmen->skor_al1 = null;
        $asesmen->skor_al2 = null;
        $asesmen->skor_al3 = null;
        $asesmen->save();
        return $asesmen;
    }

    /**
     * Gets query for [[Temuans]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getTemuans()
    {
        return $this->hasMany(Temuan::class, ['asesmen_id' => 'id']);
    }

    /** 
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[Indikator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::class, ['id' => 'indikator_id']);
    }

    /**
     * Gets query for [[Tiliks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiliks()
    {
        return $this->hasMany(Tilik::class, ['asesmen_id' => 'id']);
    }

    /** 
     * Gets query for [[Dokumen]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getDokumen()
    {
        return $this->hasOne(Dokumen::class, ['id' => 'dokumen_id']);
    }

    /**
     * Gets query for [[Pengelompokan]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
}
