<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property int $id
 * @property int|null $rtm_id
 * @property string|null $waktu_mulai
 * @property string|null $waktu_selesai
 * @property string|null $pelaksana
 * @property string|null $acara
 * @property int|null $urutan
 * @property int|null $ami_unit_id
 *
 * @property AmiUnit $amiUnit
 * @property Rtm $rtm
 */
class Jadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rtm_id', 'urutan', 'ami_unit_id'], 'integer'],
            [['waktu_mulai', 'waktu_selesai'], 'safe'],
            [['pelaksana', 'acara'], 'string', 'max' => 100],
            [['ami_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmiUnit::class, 'targetAttribute' => ['ami_unit_id' => 'id']],
            [['rtm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rtm::class, 'targetAttribute' => ['rtm_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rtm_id' => 'Rtm ID',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'pelaksana' => 'Pelaksana',
            'acara' => 'Acara',
            'urutan' => 'No',
            'ami_unit_id' => 'Ami Unit ID',
        ];
    }

    /**
     * Gets query for [[AmiUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmiUnit()
    {
        return $this->hasOne(AmiUnit::class, ['id' => 'ami_unit_id']);
    }

    /**
     * Gets query for [[Rtm]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRtm()
    {
        return $this->hasOne(Rtm::class, ['id' => 'rtm_id']);
    }
}
