<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmiAuditor;

/**
 * AmiAuditorSearch represents the model behind the search form of `app\models\AmiAuditor`.
 */
class AmiAuditorSearch extends AmiAuditor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ami_id', 'auditor_id', 'status_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = AmiAuditor::find();
        $query->orderBy(['id' => SORT_DESC]);
        $query->joinWith(['ami as a']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if (!empty($filters['auditor_id'])) $query->andWhere(['auditor_id' => $filters['auditor_id']]);
        if (!empty($filters['ami_id'])) {
            $query->andWhere(['a.ami_id' => $filters['ami_id']]);
        }
        if (!empty($filters['unit_id'])) {
            $query->andWhere(['a.unit_id' => $filters['unit_id']]);
        }


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ami_id' => $this->ami_id,
            'auditor_id' => $this->auditor_id,
            'status_id' => $this->status_id,
        ]);

        return $dataProvider;
    }
}
