<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmanahKinerjaUnitKerja;

/**
 * AmanahKinerjaUnitKerjaSearch represents the model behind the search form of `app\models\AmanahKinerjaUnitKerja`.
 */
class AmanahKinerjaUnitKerjaSearch extends AmanahKinerjaUnitKerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'amanah_kinerja_id', 'amanah_kinerja_master_id', 'target', 'capaian', 'status', 'created_at', 'updated_at'], 'safe'],
            [['unit_kerja_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $filters = [])
    {
        $query = AmanahKinerjaUnitKerja::find();

        if (!empty($filters)) {
            $query->andWhere($filters);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'unit_kerja_id' => $this->unit_kerja_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'amanah_kinerja_id', $this->amanah_kinerja_id])
            ->andFilterWhere(['like', 'amanah_kinerja_master_id', $this->amanah_kinerja_master_id])
            ->andFilterWhere(['like', 'target', $this->target])
            ->andFilterWhere(['like', 'capaian', $this->capaian])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
