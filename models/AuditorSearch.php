<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Auditor;

/**
 * AuditorSearch represents the model behind the search form of `app\models\Auditor`.
 */
class AuditorSearch extends Auditor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_sent'], 'integer'],
            [['nomor_registrasi', 'nama', 'kode_unik', 'nomor_sk', 'tanggal_sk', 'email', 'niy', 'prodi', 'nidn', 'pangkat', 'jabfung', 'status_aktif'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_sent' => $this->is_sent,
        ]);

        $query->andFilterWhere(['like', 'nomor_registrasi', $this->nomor_registrasi])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kode_unik', $this->kode_unik])
            ->andFilterWhere(['like', 'nomor_sk', $this->nomor_sk])
            ->andFilterWhere(['like', 'tanggal_sk', $this->tanggal_sk])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'niy', $this->niy])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'nidn', $this->nidn])
            ->andFilterWhere(['like', 'pangkat', $this->pangkat])
            ->andFilterWhere(['like', 'jabfung', $this->jabfung])
            ->andFilterWhere(['like', 'status_aktif', $this->status_aktif]);

        return $dataProvider;
    }
}
