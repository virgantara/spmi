<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembagian_kriteria".
 *
 * @property int $id
 * @property int|null $kriteria_id
 * @property int|null $jenis_unit
 *
 * @property Kriteria $kriteria
 */
class PembagianKriteria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pembagian_kriteria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kriteria_id', 'jenis_unit'], 'integer'],
            [['kriteria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kriteria::class, 'targetAttribute' => ['kriteria_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kriteria_id' => 'Kriteria ID',
            'jenis_unit' => 'Jenis Unit',
        ];
    }

    /**
     * Gets query for [[Kriteria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriteria()
    {
        return $this->hasOne(Kriteria::class, ['id' => 'kriteria_id']);
    }
}
