<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peningkatan_riwayat".
 *
 * @property int $id
 * @property int|null $peningkatan_id
 * @property string|null $nama_dokumen
 * @property int|null $indikator_id
 * @property int|null $unit_kerja_id
 * @property int|null $dokumen_id
 * @property string|null $pernyataan_peningkatan
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Dokumen $dokumen
 * @property Indikator $indikator
 * @property Peningkatan $peningkatan
 * @property UnitKerja $unitKerja
 */
class PeningkatanRiwayat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peningkatan_riwayat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peningkatan_id', 'indikator_id', 'unit_kerja_id', 'dokumen_id'], 'integer'],
            [['pernyataan_peningkatan'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_dokumen'], 'string', 'max' => 150],
            [['dokumen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dokumen::class, 'targetAttribute' => ['dokumen_id' => 'id']],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indikator::class, 'targetAttribute' => ['indikator_id' => 'id']],
            [['peningkatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Peningkatan::class, 'targetAttribute' => ['peningkatan_id' => 'id']],
            [['unit_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::class, 'targetAttribute' => ['unit_kerja_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'peningkatan_id' => Yii::t('app', 'Peningkatan ID'),
            'nama_dokumen' => Yii::t('app', 'Nama Dokumen'),
            'indikator_id' => Yii::t('app', 'Indikator ID'),
            'unit_kerja_id' => Yii::t('app', 'Unit Kerja ID'),
            'dokumen_id' => Yii::t('app', 'Dokumen ID'),
            'pernyataan_peningkatan' => Yii::t('app', 'Pernyataan Peningkatan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Dokumen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokumen()
    {
        return $this->hasOne(Dokumen::class, ['id' => 'dokumen_id']);
    }

    /**
     * Gets query for [[Indikator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(Indikator::class, ['id' => 'indikator_id']);
    }

    /**
     * Gets query for [[Peningkatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeningkatan()
    {
        return $this->hasOne(Peningkatan::class, ['id' => 'peningkatan_id']);
    }

    /**
     * Gets query for [[UnitKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::class, ['id' => 'unit_kerja_id']);
    }
}
