<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akreditasi;

/**
 * AkreditasiSearch represents the model behind the search form of `app\models\Akreditasi`.
 */
class AkreditasiSearch extends Akreditasi
{
    public $namaLembagaAkreditasi;
    public $namaProdi;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'urutan', 'lembaga_akreditasi_id', 'status_akreditasi', 'tanggal_sk', 'tanggal_kadaluarsa', 'nomor_sk','namaLembagaAkreditasi','namaProdi'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akreditasi::find();

        // add conditions that should always apply here

        $query->orderBy(['urutan' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prodi_id' => $this->namaProdi,
            'tanggal_sk' => $this->tanggal_sk,
            'tanggal_kadaluarsa' => $this->tanggal_kadaluarsa,
            'urutan' => $this->urutan,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'lembaga_akreditasi_id', $this->namaLembagaAkreditasi])
            ->andFilterWhere(['like', 'status_akreditasi', $this->status_akreditasi])
            ->andFilterWhere(['like', 'nomor_sk', $this->nomor_sk]);

        return $dataProvider;
    }
}
