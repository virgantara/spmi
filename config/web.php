<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'SIMUDA UNIDA Gontor',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\components\Aliases'],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@app/node_modules',
    ],
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'aplikasi' => [
            'class' => 'virgantara\components\AplikasiAuth',
            'baseurl' => $params['oauth']['baseurl'],
        ],
        'tokenManager' => [
            'class' => 'virgantara\components\TokenManager',
        ],
        'oauth2' => [
            'class' => 'virgantara\components\OAuth2Client',
            'tokenValidationUrl' => $params['oauth']['baseurl'], // Endpoint for token validation
            'tokenRefreshUrl' => $params['oauth']['baseurl'],
            'client_id' => $params['oauth']['client_id'],
            'client_secret' => $params['oauth']['client_secret'],
        ],
        'tokenService' => [
            'class' => 'virgantara\components\TokenService',
        ],


        'jui' => [
            'class' => 'yii\jui\AutoComplete',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_E8sKHo80hcdxqd6Df536k7wppFz0IOI',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\UserIdentity',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'session' => [
            'class' => 'yii\web\DbSession',
            'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60],
            'timeout' => 60 * 60 * 24, //session expire
            'useCookies' => true,

        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // 'cache' => 'cache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@webroot/gentela/views'
                ],
                'baseUrl' => '@web/gentela',
                'basePath' => '@webroot/gentela',
            ],
        ],
        'assetManager' => [
            'assetMap' => [
                'jquery.js' => '@web/gentela/assets/jquery/dist/jquery.min.js',
                // 'jquery.ui.js' => '@web/themes/klorofil/js/jquery-ui.min.js',
                'bootstrap.js' => '@web/gentela/assets/bootstrap/dist/js/bootstrap.min.js'
            ],
            'bundles' => [
                // we will use bootstrap css from our theme
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [], // do not use yii default one
                ],
            ],
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'newFileMode' => 0644,
        'newDirMode' => 0755,
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*'],
        'generators' => [ //here
            'crud' => [ // generator name
                'class' => 'app\template\crud\Generator', // generator class
                'templates' => [ //setting for out templates
                    'myCrud' => '@app/template/crud/default', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
